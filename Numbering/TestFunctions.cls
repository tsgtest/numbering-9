VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Test"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Function zzmpEditScheme() As Long
    Dim dlgEditScheme As New frmEditScheme
    dlgEditScheme.Show vbModal
End Function

Function zzmpSetDocVarCell() As Long
    bRet = lSetDocVarCell("Standard_L", 5, 3, "Doug")
End Function

Function zzmpNewScheme() As Long
    bAppInitialize
    Dim dlgNewScheme As New frmNewScheme
    dlgNewScheme.Show vbModal
End Function
