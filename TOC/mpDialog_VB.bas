Attribute VB_Name = "mpDialog_VB"
Option Explicit

Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
    Dim xarP As xArray
    Dim iRows As Integer
    On Error Resume Next
    With tdbCBX
        Set xarP = tdbCBX.Array
        iRows = mpMin(xarP.Count(1), CDbl(iMaxRows))
        .DropdownHeight = (iRows * .RowHeight)
        If Err.Number = 6 Then
 '---use hard value if there's a processor problem -- err 6 is overflow
            .DropdownHeight = (iRows * 239.811)
        End If
    End With
End Sub

Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, App.Title
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Sub CorrectTDBComboMismatch(oTDBCombo As TrueDBList60.TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    Dim oNum As CNumbers
    
    Set oNum = New CNumbers
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = mpMax(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function GetTDBListCount(oTDBCombo As TrueDBList60.TDBCombo) As Long
    Dim xarP As xArray
    Dim iCount As Integer
    
    If Not (oTDBCombo.Array Is Nothing) Then
        Set xarP = oTDBCombo.Array
        
    '   count rows in xarray
        iCount = xarP.Count(1)
    End If
    
    GetTDBListCount = iCount
End Function

Public Sub MatchInXArrayList(KeyCode As Integer, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'first character is KeyCode
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As xArray
    Dim lStartRow As Long
    Dim xChr As String
    
'   get starting row
    lStartRow = ctlP.Bookmark
    
'   start with next row
    lCurRow = lStartRow + 1
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1)
    
'   get the char that we're searching for
    xChr = UCase(Chr(KeyCode))
    
'   loop through xarray until match with first letter is found
    While (xChr <> UCase(Left(xarP.Value(lCurRow, 1), 1)))
'       if at end of xarray, start from beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        Else
            lCurRow = 0
        End If
        
    Wend
    
'   select the matched item
    With ctlP
        .Bookmark = lCurRow
        .SelBookmarks.Remove 0
        .SelBookmarks.Add lCurRow
        .SetFocus
    End With
End Sub

Public Sub MatchCompleteInXArrayList(xString As String, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'full string is xString
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As xArray
    Dim lStartRow As Long
    Dim xChrs As String
    
'   start at the top
    lStartRow = 0
    
'   start with next row
    lCurRow = lStartRow + 1
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1)
    
'   get the char that we're searching for
    xChrs = UCase(xString)
    
'   loop through xarray until match with first letter is found
    While (xChrs <> UCase(xarP.Value(lCurRow, 0)))
'       if at end of xarray, start from beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        Else
            lCurRow = 0
        End If
        
    Wend
    
'   select the matched item
    ctlP.Bookmark = lCurRow
    
End Sub

Public Sub AddItemToTDBCombo(oCtl As TrueDBList60.TDBCombo, _
                             xItem As String, _
                             Optional iIndex As Integer = -1, _
                             Optional lColValue As Long = -1)
                             
'   Adds new row to TDBCombo list
    Dim lRows As Long
    Dim xarP As xArray
    
    Set xarP = oCtl.Array
    
'   count rows in xarray
    lRows = xarP.UpperBound(1)
    
'   add new row at iIndex
    If iIndex >= 0 Then
        xarP.Insert 1, iIndex
    Else    'add as last item
        xarP.Insert 1, lRows + 1
    End If
    
'   set DisplayName column
    xarP(xarP.UpperBound(1), 0) = xItem
    
'   set Value is specified
    If lColValue >= 0 Then
        xarP(xarP.UpperBound(1), 1) = CStr(lColValue)
    Else
        xarP(xarP.UpperBound(1), 1) = xItem
    End If
    
    oCtl.Array = xarP
    
End Sub

Public Sub RemoveItemFromTDBCombo(oCtl As TrueDBList60.TDBCombo, _
                                  iIndex As Integer)
                             
'   removes row at iIndex
    Dim xarP As xArray
    
    Set xarP = oCtl.Array
    
    If Not (xarP Is Nothing) Then
        xarP.Delete 1, iIndex
        oCtl.Array = xarP
    End If
    
End Sub


