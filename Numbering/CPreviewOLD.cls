VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CPreview"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**********************************************************
'   CPreview Collection Class
'   created 3/01/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   CPreview object - the numbering Preview - the preview
'   is the visual representation of the numbering scheme
'   that is displayed when the edit scheme dialog is active.
'   numbering bitmaps are created by using bit block transfers
'   of the screen representation of the preview.
'**********************************************************
Private Const GW_CHILD = 5
Private Const GW_HWNDFIRST = 0
Private Const GW_HWNDLAST = 1
Private Const GW_HWNDNEXT = 2
Private Const GW_HWNDPREV = 3
Private Const GW_MAX = 5

Private Declare Function GetWindow Lib "user32" (ByVal hwnd As Long, ByVal wCmd As Long) As Long
Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" ( _
    ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function IsWindowVisible Lib "user32" (ByVal hwnd As Long) As Long

Private Type udtWindowProps
    iZoom As Integer
    bShowAll As Boolean
    bShowTableGrids As Boolean
    iView As Integer
End Type
    
Public Function ShowPreview(ByVal xScheme As String, ByVal iSchemeType As mpSchemeTypes) As IPictureDisp
    Dim rngLocation As Word.Range
    Dim xAutoText As String
    Dim oAutoText As Word.AutoTextEntry
    Dim WinProps As udtWindowProps
    
    Word.Application.ScreenUpdating = False
    
'   set window props for preview
    SetPreviewWindow
    
    With Word.ActiveDocument
'       set margins/pageheight only if necessary - there's a Word quirk
'       such that setting page setup programmatically the first time
'       in a session takes an inordinate amount of time - this solution
'       is the best we can do.
        With .PageSetup
            If (.LeftMargin <> 90) Then
                .LeftMargin = 90
            End If
            If (.RightMargin <> 90) Then
                .RightMargin = 90
            End If
            If .PageHeight < (22 * 72) Then
                .PageHeight = (22 * 72)
            End If
        End With
        
        Set rngLocation = .Content
        rngLocation.StartOf
        
        Dim xSource As String
        If iSchemeType = mpSchemeType_Personal Then
'           insert preview autotext
            xAutoText = "zzmp" & xScheme & "Preview"
            On Error Resume Next
            Set oAutoText = Templates(g_xPNumSty).AutoTextEntries(xAutoText)
            On Error GoTo 0
            If Not (oAutoText Is Nothing) Then
                oAutoText.Insert rngLocation, True
            Else
                InsertTable rngLocation, xScheme
            End If
        Else
            InsertTable rngLocation, xScheme
        End If
        
        Set rngLocation = Word.ActiveDocument.Content
        With rngLocation
            .EndOf
            .InsertAfter String(80, vbCr)
            .Style = "Normal"
            .EndOf
            .Select
        End With
        
        Word.ActiveWindow.VerticalPercentScrolled = 0
        
        With ActiveDocument.Content.ParagraphFormat
            .LineSpacingRule = wdLineSpaceExactly
            .LineSpacing = 19
            .SpaceAfter = 12
            .SpaceBefore = 0
            .TabStops.ClearAll
        End With

        ActiveDocument.Content.Font.Size = 20
    End With
        
    Word.Application.ScreenUpdating = True
    Word.Application.StatusBar = ""
End Function

Private Sub InsertTable(rngLocation As Word.Range, xScheme As String)
    Dim i As Integer
    Dim xStyle As String
    Dim rngParaStart As Word.Range
    Dim tblPreview As Word.Table
    Dim rngLevel As Word.Range
    Dim rngHeading As Word.Range
    
    Application.StatusBar = "Updating Preview..."
    
    With Word.ActiveDocument
'       add a table at the beginning of document
        Set tblPreview = rngLocation.Tables.Add(rngLocation, 9, 1)
        
        tblPreview.Borders.Enable = False
        tblPreview.Rows.HeightRule = wdRowHeightExactly

'       insert paras for 1-9 levels
        For i = 1 To 9
            On Error Resume Next
            xStyle = xScheme & "_L" & i
            If .Styles(xStyle) Is Nothing Then
                Exit For
            End If
            On Error GoTo 0
            Set rngLevel = InsertLevel(tblPreview, xScheme, i)
        Next i
    End With
    
    Application.StatusBar = ""
End Sub

Sub RefreshPreviewLevel(xScheme As String, iLevel As Integer)
    Dim rngP As Word.Range
    Dim rngParaStart As Word.Range
    Dim i As Integer
    
    Application.ScreenUpdating = False
    On Error Resume Next
    Set rngP = Word.ActiveDocument.Tables(1).Range.Cells(iLevel).Range
    If rngP Is Nothing Then
        Exit Sub
    End If
    
'   remove number
    rngP.Delete
    rngP.Style = "Normal"
'
'    rngP.ListFormat.RemoveNumbers
    InsertLevel Word.ActiveDocument.Tables(1), xScheme, iLevel
    
    With rngP.ParagraphFormat
'        .SpaceBefore = 6
        .LineSpacingRule = wdLineSpaceExactly
        .LineSpacing = 19
        If Not .TabStops(1).CustomTab Then
            .TabStops.ClearAll
        End If
    End With
    
    Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Function InsertLevel(tblPreview As Word.Table, _
                             ByVal xScheme As String, _
                             ByVal iLevel As Integer) As Word.Range
    Dim xStyle As String
    Dim rngParaStart As Word.Range
    Dim fldTC As Word.Field
    Dim lNumReturns As Long
    Dim i As Integer
    Dim styTCHeading As Word.Style
    Dim fntTCHeading As Word.Font
    Dim bHeadingIsPara  As Boolean
    Dim bLinesInserted As Boolean
    Dim llCur As Word.ListLevel
    Dim iHeadingFormat As Integer
                        
    xStyle = xScheme & "_L" & iLevel
    Set rngParaStart = tblPreview.Range.Cells(iLevel) _
                       .Range.Paragraphs(1).Range

'   insert number
    Set rngParaStart = rngInsertNumWordNum(rngParaStart, _
                                           xScheme, _
                                           iLevel)
    Set llCur = rngParaStart.ListFormat _
                .ListTemplate.ListLevels(iLevel)
    
    With rngParaStart
        With .ParagraphFormat
            .LeftIndent = llCur.TextPosition
            .FirstLineIndent = llCur.NumberPosition - _
                               llCur.TextPosition
            On Error Resume Next
            .TabStops(1).Position = llCur.TabPosition
        End With
        .StartOf
        .InsertAfter g_xPreviewLevelText & iLevel
            
'       format info is held as bits
'       in an integer - get integer
        iHeadingFormat = xGetLevelProp(xScheme, _
                                  iLevel, _
                                  mpNumLevelProp_HeadingFormat, _
                                  mpSchemeType_Document)
        With rngParaStart.Font
'           test bits for format value
            .Bold = bHasTCFormat(iHeadingFormat, mpTCFormatField_Bold)
            .Italic = bHasTCFormat(iHeadingFormat, mpTCFormatField_Italic)
            .AllCaps = bHasTCFormat(iHeadingFormat, mpTCFormatField_AllCaps)
            .Underline = bHasTCFormat(iHeadingFormat, mpTCFormatField_Underline)
            .SmallCaps = bHasTCFormat(iHeadingFormat, mpTCFormatField_SmallCaps)
            .ColorIndex = wdBlue
        End With

        .EndOf
            
'       insert lines in all cases but centered paras with shift returns
        If Not ((.ParagraphFormat.Alignment = wdAlignParagraphCenter) And _
                    (InStr(.Paragraphs(1).Range, Chr(11)) > 0 Or _
                    .ListFormat.ListTemplate _
                    .ListLevels(iLevel).NumberFormat = "")) Then
            .InsertAfter "_   _  _   _  _     _   _  _     _   _  _   " & _
                         "_  _   _  _   _  _    " & _
                         "_  _    _ _   _  _   _  _" & _
                         "_  _   _  _    _  _   _  _   " & _
                         "_  _     _     _    " & _
                         "_      _     _     _     _     _     _     "

            .Underline = wdUnderlineSingle
            
            bHeadingIsPara = (iHeadingFormat And _
                              mpTCFormatField_Type)
                              
            If bHeadingIsPara Then
                .Font.ColorIndex = wdBlue
            Else
                .Font.ColorIndex = wdAuto
            End If
            bLinesInserted = True
        End If

'       set font size
        Set rngParaStart = tblPreview.Range.Cells(iLevel) _
                            .Range.Paragraphs(1).Range
        rngParaStart.Font.Size = 20

'       set row height to 48 points plus
'       24 points for each soft return
        lNumReturns = lCountChrs(.Paragraphs(1).Range, Chr(11))
        If bLinesInserted Or lNumReturns Then
            tblPreview.Rows(iLevel).Height = 45 + _
                (mpMax(lNumReturns - 1, 0) * 24)
        Else
            tblPreview.Rows(iLevel).Height = 32
        End If
        .EndOf
        Set InsertLevel = rngParaStart
    End With
End Function

Private Function CapturePreviewWindow() As Picture
    Dim r As Long

'   Get a handle to the active/foreground window.
    Dim xText As String
    Dim lText As Long
    Dim hWndActive As Long
    Dim hWndTop As Long
    Dim oBitmap As CBitmap
    Dim iCharsRet As Integer

    xText = String(100, " ")
    lText = Len(xText)

    DoEvents

'   remove any highlighting that may be present
    On Error Resume Next
    Word.ActiveDocument.Tables(1) _
        .Range.HighlightColorIndex = wdNoHighlight
    On Error GoTo 0
    
'   get Word app window
    hWndTop = FindWindow(vbNullString, _
                         Word.Application.Caption)

'   get first Word child window
    hWndActive = GetWindow(hWndTop, GW_CHILD)
    
'   get preview doc window, which is the first
'   visible child window that has no window text
    iCharsRet = GetWindowText(hWndActive, xText, lText)
    
    While iCharsRet Or Not (IsWindowVisible(hWndActive) = 1)
        hWndActive = GetWindow(hWndActive, GW_HWNDNEXT)
        iCharsRet = GetWindowText(hWndActive, xText, lText)
    Wend

'   capture the active window given its handle
'   and return the Resulting Picture object.
    Set oBitmap = New CBitmap
    Set CapturePreviewWindow = _
        oBitmap.CaptureWindow(hWndActive, True, 19, 0, 275, 275)
End Function

Sub CreateBitmap(xSchemeDisplayName As String)
    Dim oPicture As Picture
    Dim rngP As Word.Range
    Dim i As Long
    
    With Word.Application
        Set rngP = .ActiveDocument.Tables(1).Range
        
'       remove shading
        rngP.Shading.Texture = wdTextureNone
        
'       store preview table as autotext
        .Templates(g_xPNumSty).AutoTextEntries.Add _
                    "zzmp" & xSchemeDisplayName & "Preview", rngP
                    
        DoEvents
    End With
    
    Application.ScreenUpdating = True
    
'   create picture from preview window
    Set oPicture = CapturePreviewWindow()

'   save as mpb
    SavePicture oPicture, g_xUserPath & "\" & xSchemeDisplayName & ".mpb"
End Sub

Sub LoadBitmap(xScheme As String, iSchemeType As mpSchemeTypes)
    Dim xPreviewFilePath As String
        
'   get appropriate preview file
    Select Case iSchemeType
        Case mpSchemeType_Document
            xPreviewFilePath = App.Path & "\DocScheme" & ".mpb"
        Case mpSchemeType_Personal
            xPreviewFilePath = g_xUserPath & "\" & xScheme & ".mpb"
            
            If Dir(xPreviewFilePath) = "" Then
                xPreviewFilePath = App.Path & "\NoPreview.mpb"
            End If
        Case mpSchemeType_Firm
            xPreviewFilePath = App.Path & "\" & xScheme & ".mpb"
            
            If Dir(xPreviewFilePath) = "" Then
                xPreviewFilePath = App.Path & "\NoPreview.mpb"
            End If
        Case Else
                Select Case g_dlgScheme.tvwSchemes.SelectedItem.Index
                    Case mpSchemeType_Personal
                        xPreviewFilePath = App.Path & "\PersonalSchemes.mpb"
                    Case mpSchemeType_Firm
                        xPreviewFilePath = App.Path & "\FirmSchemes.mpb"
                    Case mpSchemeType_Document
                        xPreviewFilePath = App.Path & "\DocumentSchemes.mpb"
                    Case Else
                        xPreviewFilePath = App.Path & "\Blank.mpb"
                End Select
    End Select
    g_dlgScheme.imgPreview.Picture = LoadPicture(xPreviewFilePath)
End Sub

Sub HighlightLevel(iLevel As Integer)
'Highlights the cell containing the specified level -
'removes highlight from all other cells
    Dim rngP As Word.Range
    Dim rngQ As Word.Range
    
'    Application.ScreenUpdating = False
    On Error Resume Next
    Set rngP = Word.ActiveDocument _
        .Tables(1).Range.Cells(iLevel).Range
    On Error GoTo 0
    If rngP Is Nothing Then
        Exit Sub
    End If
    Word.ActiveDocument.Tables(1) _
        .Range.Shading.Texture = wdTextureNone
    rngP.Shading.Texture = wdTexture5Percent
    DoEvents
    
    Set rngQ = ActiveDocument.Content
    rngQ.EndOf
    ActiveWindow.VerticalPercentScrolled = 100
    ActiveWindow.VerticalPercentScrolled = 0
End Sub

Public Function BitmapExists(xSchemeDisplayName As String, _
                             iSchemeType As mpSchemeTypes) As Boolean
'returns TRUE if the bitmap for the specified scheme name exists
    Select Case iSchemeType
        Case mpSchemeType_Personal
            BitmapExists = (Dir(g_xUserPath & "\" & _
                        xSchemeDisplayName) <> "")
        Case mpSchemeType_Firm
            BitmapExists = (Dir(App.Path & "\" & _
                        xSchemeDisplayName) <> "")
        Case mpSchemeType_Document
            BitmapExists = False
    End Select
End Function

Private Function GetCurWindowProps() As udtWindowProps
    Dim WinProps As udtWindowProps
    With Word.ActiveWindow.View
        WinProps.bShowAll = .ShowAll
        WinProps.bShowTableGrids = .TableGridlines
        WinProps.iView = .Type
        WinProps.iZoom = .Zoom
    End With
End Function

Private Sub SetCurWindowProps(WinProps As udtWindowProps)
    With Word.ActiveWindow.View
        .ShowAll = WinProps.bShowAll
        .TableGridlines = WinProps.bShowTableGrids
        .Type = WinProps.iView
        .Zoom = WinProps.iZoom
    End With
End Sub

Private Sub SetPreviewWindow()
    With Word.ActiveWindow.View
        .ShowAll = False
        .TableGridlines = False
        .Type = wdNormalView
        .Zoom = 41
    End With
    
    With Word.ActiveWindow
        .VerticalPercentScrolled = 0
        .Caption = "Scheme Preview"
        .WindowState = wdWindowStateMaximize
    End With
    
    With Word.Application
'        EchoOn
'        .ScreenUpdating = True
'        .ScreenRefresh
'        DoEvents
'        .ScreenUpdating = False
    End With
End Sub


