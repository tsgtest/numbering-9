Attribute VB_Name = "mdlWord14"
Option Explicit

Public Function GetCompatibilityMode(ByVal oDocument As Word.Document) As Integer
    Dim oWord14 As cWord14
    Set oWord14 = New cWord14
    GetCompatibilityMode = oWord14.GetCompatibilityMode(oDocument)
End Function

Public Sub Convert(ByVal oDoc As Word.Document)
    Dim oWord14 As cWord14
    Set oWord14 = New cWord14
    oWord14.Convert oDoc
End Sub
