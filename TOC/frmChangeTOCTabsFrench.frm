VERSION 5.00
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmChangeTOCTabsFrench 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Augmenter/r�duire tabulations TM "
   ClientHeight    =   1665
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4740
   Icon            =   "frmChangeTOCTabsFrench.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1665
   ScaleWidth      =   4740
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin mpControls3.SpinTextInternational spnTabs 
      Height          =   330
      Left            =   2300
      TabIndex        =   1
      Top             =   405
      Width           =   1050
      _ExtentX        =   1852
      _ExtentY        =   582
      Appearance      =   1
      IncrementValue  =   0.05
      MinValue        =   -288
      MaxValue        =   288
      AppendSymbol    =   -1  'True
      DisplayUnit     =   0
   End
   Begin VB.CommandButton btnReset 
      Caption         =   "&R�tablir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      TabIndex        =   6
      ToolTipText     =   "Reset tabs to original settings"
      Top             =   1125
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      TabIndex        =   5
      Top             =   660
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      TabIndex        =   4
      Top             =   210
      Width           =   1100
   End
   Begin TrueDBList60.TDBCombo cbxTabLevels 
      Height          =   585
      Left            =   1230
      OleObjectBlob   =   "frmChangeTOCTabsFrench.frx":058A
      TabIndex        =   3
      Top             =   930
      Width           =   2115
   End
   Begin VB.Label lblTabLevels 
      Caption         =   "&Appliquer � :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   215
      TabIndex        =   2
      Top             =   960
      Width           =   925
   End
   Begin VB.Label lblTabs 
      Caption         =   "Augmenter &tabulations par:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   215
      TabIndex        =   0
      Top             =   450
      Width           =   2045
   End
End
Attribute VB_Name = "frmChangeTOCTabsFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public m_bFinished As Boolean

Private Sub btnCancel_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnCancel_Click
End Sub

Private Sub btnOK_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnOK_Click
End Sub

Private Sub btnReset_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnReset_Click
End Sub

Private Sub btnCancel_Click()
    Me.Hide
    Unload Me
End Sub

Private Sub btnOK_Click()
    Me.Hide
    DoEvents
    bRet = bSetTOCTabs(Me.spnTabs.Value, Me.cbxTabLevels.BoundText)
    Unload Me
End Sub

Private Sub btnReset_Click()
    Dim rngTOC As Word.Range
    Dim rngStart As Word.Range
    
    Me.Hide
    DoEvents
    With ActiveDocument
        If .Bookmarks.Exists("mpTableOfContents") Then
            Set rngTOC = .Bookmarks("mpTableOfContents").Range
        Else
            Set rngTOC = ActiveDocument.Content
            With rngTOC.Find
                .ClearFormatting
                .Text = ""
                .Style = wdStyleTOC1
                .Execute
                If Not .Found Then
                    MsgBox "Aucune table des mati�res n'a �t� trouv� dans ce document.", vbExclamation, "Num�rotation MacPac"
                    Exit Sub
                Else
                    rngTOC.MoveEnd wdSection
                    If rngTOC.Characters.Last = Chr(12) Then _
                        rngTOC.MoveEnd wdCharacter, -1
                End If
            End With
        End If
    End With
    Application.ScreenUpdating = False
    
    Set rngStart = Selection.Range
    bResetTOCTabs rngTOC
    Application.ScreenUpdating = True
    rngStart.Select
    Application.ScreenRefresh
End Sub

Private Sub cbxTabLevels_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxTabLevels, Reposition
    Exit Sub
ProcError:
    RaiseError "frmChangeTOCTabs.cbxTabLevels_Mismatch"
    Exit Sub
End Sub

Private Sub Form_Activate()
    Dim lUnits As Long
    
    'initialize to first in list
    Me.cbxTabLevels.SelectedItem = 0
        
    lUnits = Word.Options.MeasurementUnit
    With Me.spnTabs
        .DisplayUnit = lUnits
        .IncrementValue = GetStandardIncrement(lUnits)
    End With

End Sub

Private Sub spnTabs_LostFocus()
    With Me
        If .spnTabs.Value < 0 Then
            .lblTabs.Caption = "R�duire &Tabulations par:"
        Else
            .lblTabs.Caption = "Augmenter &Tabulations par:"
        End If
    End With
End Sub
Private Sub Form_Load()
    Dim xApplyTo(0 To 9) As String
    Dim i As Integer
    Dim xarTabLevels As xArray
    
    Me.m_bFinished = False

'   fill Tab Levels array
    Set xarTabLevels = New xArray
    xarTabLevels.ReDim 0, 9, 0, 1
    xarTabLevels(0, 0) = "Tous niveaux"
    xarTabLevels(0, 1) = "0"
    For i = 1 To 9
        xarTabLevels(i, 0) = "Niveau " & i
        xarTabLevels(i, 1) = i
    Next i
    
    Me.cbxTabLevels.Array = xarTabLevels
    ResizeTDBCombo Me.cbxTabLevels, 10

End Sub

Private Sub spnTabs_SpinDown()
    If Me.spnTabs.Value < 0 Then _
        Me.lblTabs.Caption = "R�duire &Tabulations par:"
End Sub

Private Sub spnTabs_SpinUp()
    If Me.spnTabs.Value >= 0 Then _
        Me.lblTabs.Caption = "Augmenter &Tabulations par:"
End Sub

Private Sub spnTabs_Validate(Cancel As Boolean)
    If Not spnTabs.IsValid Then _
        Cancel = True
End Sub
