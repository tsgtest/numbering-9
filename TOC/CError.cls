VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Enum mpnErrors
    mpTOCError_NoTOCFound = vbError + 512 + 1
End Enum

Sub Raise(CurError As ErrObject)
    Dim xDescription As String
    Dim iSeverity As Integer
    
    EchoOn
    Word.Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    Select Case CurError.Number
        Case mpTOCError_NoTOCFound
            If g_lUILanguage = wdFrenchCanadian Then
                Err.Description = "Aucune table des mati�res n'a �t� trouv� dans ce document."
            Else
                Err.Description = "Could not find a Table Of Contents in this document."
            End If
        Case Else
            If g_lUILanguage = wdFrenchCanadian Then
                xDescription = "Une erreur inattendue s'est produite dans la proc�dure " & _
                    Err.Source & "." & vbCr & "Erreur #" & CurError.Number & ": " & _
                    CurError.Description
            Else
                xDescription = "An unexpected error occurred in procedure " & _
                    Err.Source & "." & vbCr & "Error #" & CurError.Number & ": " & _
                    CurError.Description
            End If
            iSeverity = vbCritical
    End Select
    If g_lUILanguage = wdFrenchCanadian Then
        MsgBox xDescription, iSeverity, "Table des mati�res MacPac"
    Else
        MsgBox xDescription, iSeverity, App.Title
    End If
End Sub
