VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private m_frmStatus As frmStatus
Public Property Let Picture(ByVal xFile As String)
    With m_frmStatus.imgP
        If Len(xFile) Then
            .Picture = LoadPicture(xFile)
            .Visible = True
        Else
            .Picture = Nothing
            .Visible = False
        End If
    End With
    
    With m_frmStatus.lblStatus
        If Len(xFile) Then
            .Left = 825
            .Width = 4140
        Else
            .Left = 285
            .Width = 4585
        End If
    End With
End Property

Public Property Let Title(ByVal xNew As String)
    m_frmStatus.Label1 = xNew
End Property

Public Property Get Title() As String
    Title = m_frmStatus.Label1
End Property

Public Property Let ProgressBarVisible(bNew As Boolean)
    With m_frmStatus
        .pbStatus.Visible = bNew
        If bNew Then
            .Height = 1890
        Else
            .Height = 1350
        End If
    End With
End Property

Public Property Get ProgressBarVisible() As Boolean
    ProgressBarVisible = m_frmStatus.pbStatus.Visible
End Property
Public Function Show(Optional ByVal sPercent As Single = -1, _
                     Optional ByVal xMsg As String)
    
    Screen.MousePointer = vbHourglass
    With m_frmStatus
        .MousePointer = vbHourglass
        If sPercent <> -1 Then
            .pbStatus = sPercent
        End If
        If Len(xMsg) Then
            .lblStatus = xMsg
        End If
        
        If .Visible = False Then
            .Show
        Else
            .Refresh
        End If
    End With
    Screen.MousePointer = vbDefault
End Function
Public Function Hide()
    If Not m_frmStatus Is Nothing Then
        m_frmStatus.Visible = False
        m_frmStatus.MousePointer = vbDefault
        Unload m_frmStatus
        Set m_frmStatus = Nothing
    End If
End Function

Private Sub Class_Initialize()
    If Not m_frmStatus Is Nothing Then _
        Set m_frmStatus = Nothing
    Set m_frmStatus = New frmStatus
End Sub

Private Sub Class_Terminate()
    Hide
End Sub
