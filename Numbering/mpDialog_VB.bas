Attribute VB_Name = "mdlDialogVB"
Option Explicit
Type POINTAPI
        X As Long
        Y As Long
End Type

Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer
Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
Public Declare Sub mouse_event Lib "user32" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)
Public Declare Function MapVirtualKey Lib "user32" Alias "MapVirtualKeyA" (ByVal wCode As Long, ByVal wMapType As Long) As Long
Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long
Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long

Public Const KEYEVENTF_KEYUP = &H2
Public Const VK_SHIFT = &H10
Public Const VK_CONTROL = &H11
Public Const SM_CXSCREEN = 0
Public Const SM_CYSCREEN = 1
Public Const MOUSEEVENTF_MOVE = &H1 '  mouse move
Public Const MOUSEEVENTF_LEFTDOWN = &H2 '  left button down
Public Const MOUSEEVENTF_LEFTUP = &H4 '  left button up
Public Const MOUSEEVENTF_RIGHTDOWN = &H8 '  right button down
Public Const MOUSEEVENTF_RIGHTUP = &H10 '  right button up
Public Const MOUSEEVENTF_MIDDLEDOWN = &H20 '  middle button down
Public Const MOUSEEVENTF_MIDDLEUP = &H40 '  middle button up
Public Const MOUSEEVENTF_ABSOLUTE = &H8000 '  absolute move

Function SelectFirstCtlOnTab(ctlTab As VB.Control, xControls() As String)
'selects the first control on tab iTabIndex
'on active form
    Dim iTabIndex As Integer
    On Error Resume Next
    iTabIndex = GetFirstCtlOnTab(ctlTab.Tab)
    SelectTabIndex iTabIndex, xControls()
End Function

Function GetLastCtlOnTab(iTab As Integer) As Integer
'returns the index of the last control on tab iTab
    Dim xCycleCtl As String
    Dim iLastTabIndex As Integer
    
    xCycleCtl = "picCycleTab" & (iTab + 1)
    GetLastCtlOnTab = Screen.ActiveForm _
                    .Controls(xCycleCtl).TabIndex - 1
    Exit Function
    
ProcError:
    MsgBox Error(Err), vbExclamation, g_xAppName
    Exit Function
End Function

Function GetFirstCtlOnTab(iTab As Integer) As Integer
'returns the index of the first control on tab iTab
    Dim xCycleCtl As String
    Dim iLastTabIndex As Integer
    
    If iTab = 0 Then
        GetFirstCtlOnTab = 0
    Else
        xCycleCtl = "picCycleTab" & iTab
        GetFirstCtlOnTab = Screen.ActiveForm _
                        .Controls(xCycleCtl).TabIndex + 1
    End If
    Exit Function
    
ProcError:
    MsgBox Error(Err), vbExclamation, g_xAppName
    Exit Function
End Function

Function SelectTabIndex(iTabIndex As Integer, _
                        xControls() As String, _
                        Optional iIncr As Integer = 1)
'sets focus on control whose tab index = itabIndex.
'if control can't take focus, next/prev control is
'attempted (iIncr determines whether it's next or prev)
    Dim bCanGetFocus As Boolean
    Dim ctlFocus As VB.Control
    Dim xCtl As String
    Dim frmP As VB.Form
    
    Set frmP = Screen.ActiveForm
    
    Do
'       get control of previous tab index
        xCtl = xControls(iTabIndex)
        Set ctlFocus = frmP.Controls(xCtl)
            
'       test for enabled, visible, tabstop
        bCanGetFocus = False
        On Error Resume Next
        With ctlFocus
            bCanGetFocus = .Enabled And _
                           .Visible And _
                           .TabStop
        End With
        On Error GoTo 0
        DoEvents
        iTabIndex = iTabIndex + iIncr
    Loop Until (bCanGetFocus)
    ctlFocus.SetFocus
End Function

Function CycleTabs(iCurTab As Integer, vbControls() As String)
'either activates next or previous tab
'depending on value of iCurTab and relative
'value of cycle control
    With Screen.ActiveForm.MultiPage1
        If .Tab = iCurTab - 1 Then
            ActivateNextTab vbControls()
        Else
            ActivatePrevTab vbControls()
        End If
    End With
End Function

Function VBACtlLostFocus(ctlP As VB.Control, _
                         xControls() As String, _
                         Optional bShiftRelease As Boolean = True)
'this is a patch necessary to handle shift tab
'with VBA controls - YUK! - use in conjuntion with
'SelectCtlIfShiftTabbed - call only from VBA controls
'lost focus event procedure

'---release virtual shift down if present
        If g_VShiftDown And bShiftRelease Then
            Dim altscan%
            altscan% = MapVirtualKey(VK_SHIFT, 0)
            keybd_event VK_SHIFT, altscan, KEYEVENTF_KEYUP, 0
            g_VShiftDown = False
        End If

'   ensure that this only runs one at a time
    If Not g_bInVBALostFocus Then
        
        g_bInVBALostFocus = True
        SelectCtlIfShiftTabbed ctlP, xControls()
'        bEnsureSelectedContent Screen.ActiveControl
        g_bInVBALostFocus = False
    End If
End Function
Function ActivateNextTab(xControls() As String) As Long
'selects next tab -
'skips disabled tabs-
'if last tab, goes to tab 1
    Dim iStartTabIndex As Integer
    Dim iTabState As Integer
    Dim iTab As Integer
    Dim iFirstCtl As Integer
    Dim iShiftState As Integer
    Dim bIsShiftTabbing As Boolean
    
'   get state of tab/shift tab
    iTabState = GetKeyState(&H9)
    iShiftState = GetKeyState(&H10)
    
'   set global flag
    g_ActivatingTabProgrammatically = True
    
    bIsShiftTabbing = iTabState < 0 And _
                      iShiftState < 0
                      
    If Not bIsShiftTabbing Then
        With Screen.ActiveForm.MultiPage1
            iStartTabIndex = .Tab
            'On Error Resume Next
            
'           force to first tab if currently on last tab
            If iStartTabIndex = (.Tabs - 1) Then
                iTab = 0
            Else
                '---bug fix if last tab disabled as in notary
                If .TabEnabled(.Tabs - 1) = False And _
                        iStartTabIndex <> 0 Then
                    iTab = 0
                Else
                    iTab = iStartTabIndex + 1
                End If
            End If
            
            While Not .TabEnabled(iTab)
'               if current disabled tab is also last tab
'               select first tab and leave
                If iTab >= .Tabs - 1 Then
'                   set to Finish button - questionable line
                    Screen.ActiveForm.cmdFinish.SetFocus
                    Exit Function
                    iTab = 0
                    GoTo SelectTab
                End If
'               force to first tab if iTab is last tab
                If iTab = (.Tabs - 1) Then
                    iTab = 0
                Else
                    iTab = iTab + 1
                End If
            Wend
SelectTab:
            
'           select tab
            .Tab = iTab
            
            DisableInactiveTabs Screen.ActiveForm
           
'           get index of first ctl on tab
            iFirstCtl = GetFirstCtlOnTab(iTab)
            
        End With

        
'       select first control on selected tab
        SelectTabIndex iFirstCtl, _
                       xControls()
    End If
    g_ActivatingTabProgrammatically = False
End Function

Function ActivatePrevTab(xControls() As String) As Long
'selects next tab -
'skips disabled tabs-
'if last tab, goes to tab 1
    Dim iStartTabIndex As Integer
    Dim iTab As Integer
    Dim iLastControl As Integer
    
    Dim ctlP As VB.Control
    g_ActivatingTabProgrammatically = True
    With Screen.ActiveForm.MultiPage1
        iTab = .Tab
        If .Tab > 0 Then
            iStartTabIndex = iTab
            On Error Resume Next
            iTab = iTab - 1
            While Not .TabEnabled(iTab)
                iTab = mpMax(iTab - 1, 0)
            Wend
        End If
        
'       select tab
        .Tab = iTab
        
        DisableInactiveTabs Screen.ActiveForm
           
'       get tab index of last control
'       on selected tab
        iLastControl = GetLastCtlOnTab(.Tab)
    End With
    
'   select last control on selected tab
    SelectTabIndex iLastControl, xControls(), -1
    g_ActivatingTabProgrammatically = False
End Function

Function PreventInvisibleFocus(iTab As Integer, _
                               Optional xMultiPageName As String = "MultiPage1", _
                               Optional xControlTag) As Boolean
''returns to previous control if the
''tab containing the control (iTab) is not visible -
''returns True if focus is returned to previous
'    Dim iTabState As Integer
'    Dim bIsTabbing As Boolean
'
'    On Error GoTo ProcError
''   get state of tab/shift tab
'    bIsTabbing = (GetKeyState(&H9) < 0)
'
'Debug.Print Screen.ActiveControl.Name & ":" & Screen.ActiveControl.Tag & ":" & bIsTabbing
'
'    If Not bIsTabbing Then
'        With Screen.ActiveForm
'            If .Controls(xMultiPageName).Tab <> iTab Then
'                DoEvents
'                SendKeys "%" & .ActiveControl.Tag
'                DoEvents
'                PreventInvisibleFocus = True
'            End If
'        End With
'    End If
'ProcError:
End Function

Function objShowGlobalForm(xFormName As String, _
    Optional xCaption As String) As Object
    MsgBox "Hi! You've called objShowGlobalForm AGAIN!"
End Function

Function bEnsureSelectedContent(ctlP As VB.Control, _
    Optional ByVal bLastCharOnly As Boolean = False, _
    Optional ByVal bAlways As Boolean = False) As Boolean
    Dim ctlData As VB.Data
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   select content only if tabbed
'   or hotkeyed into
    If IsPressed(KEY_TAB) Or IsPressed(KEY_MENU) Or bAlways Then
        With ctlP
            If TypeOf ctlP Is ListBox Then
                If .ListIndex < 0 Then _
                    .ListIndex = 0
            Else
                If bLastCharOnly = False Then
                    .SelStart = 0
                    .SelLength = Len(.Text)
                Else
                    .SelStart = Len(.Text)
                    .SelLength = 1
                End If
            End If
        End With
    End If
    Exit Function
ProcError:
    Exit Function
End Function

Sub SelectCtlIfShiftTabbed(ctlP As VB.Control, _
                           xTabOrder() As String)
'selects ctlTab/ctlShiftTab if
'user has tab/shift tabbed

    Dim iTabState As Integer
    Dim iShiftState As Integer
    Dim bIsShiftTabbing As Boolean
    
'   get state of tab/shift tab
    iTabState = GetKeyState(&H9)
    iShiftState = GetKeyState(&H10)
    
    bIsShiftTabbing = iTabState < 0 And _
                      iShiftState < 0
                      
    If bIsShiftTabbing Then
        SelNextPrevCtl ctlP, xTabOrder(), False
    End If
End Sub
Sub GetTabOrder(frmP As VB.Form, xControls() As String)
    Dim ctlP As VB.Control
    
    ReDim xControls(frmP.Controls.Count)
    On Error Resume Next
    For Each ctlP In frmP.Controls
        xControls(ctlP.TabIndex) = ctlP.Name
    Next ctlP
End Sub
Sub SelNextPrevCtl(ctlP As VB.Control, _
                   xControls() As String, _
                   Optional bNext As Boolean = True)
'selects the first enabled control possible
'with the smallest tab index greater than the
'tab index of ctlP.  Useful with "cycler"
'controls in dlgs. xControls() is an array of all
'control names whose array index is each controls
'tab index.
    Dim i As Integer
    Dim iIncr As Integer
    Dim iLastTabIndex As Integer
    Dim iTabIndex As Integer
    Dim xCtl As String
    Dim ctlFocus As VB.Control
    Dim bCanGetFocus As Boolean
    
    iTabIndex = ctlP.TabIndex

'   next/previous ctl incrementer
    If bNext Then
        iIncr = 1
    Else
        iIncr = -1
    End If
    
    DoEvents
    
'   get last control index to test
    If bNext Then
        iLastTabIndex = ctlP.Parent.Controls.Count - 1
    Else
        iLastTabIndex = 0
    End If
Debug.Print "Source: " & ctlP.Name
    
    Do
        On Error Resume Next
'       get control of previous tab index
        iTabIndex = iTabIndex + iIncr
        If iTabIndex < 0 Then iTabIndex = 0
        xCtl = xControls(iTabIndex)
        Set ctlFocus = ctlP.Parent(xCtl)
        
'       test for enabled, visible, tabstop
        bCanGetFocus = False
        With ctlFocus
            bCanGetFocus = .Enabled And _
                           .Visible And _
                           (.TabStop Or Left(.Name, 11) = "picCycleTab")
        End With
        On Error GoTo 0
        DoEvents
        Loop Until (bCanGetFocus Or iTabIndex = iLastTabIndex)
Debug.Print "Dest: " & ctlFocus.Name & ":" & bCanGetFocus
    
'   if the correct control has been found
'   move focus, else keep focus on same control -
'   also prevent focus from going from tab 1 to tab 3
'   (ie recycling backwards)
    If bCanGetFocus Then
        ctlFocus.SetFocus
        DoEvents
    Else
        ctlP.SetFocus
    End If
End Sub


Public Function ToggleCheckBox()
''toggles checkbox - use in GotFocus procedure of
''checkboxes whose hotkeys are also used by another
''control in the dialog - allows user to toggle control
''when hotkeying to that control
'    Dim bAltIsDown As Boolean
'
''   ensure that toggle does not occur
''   when tabbing toor arrowing to
'    bAltIsDown = (GetKeyState(mpKey_Hex_Alt) < 0)
'
'    If bAltIsDown Then
'        With Screen.ActiveControl
'            On Error Resume Next
'            .Value = Abs(.Value - 1)
'        End With
'    End If
End Function
Public Function PushButton()
''pushes command button - use in GotFocus procedure of
''buttons whose hotkeys are also used by another
''control in the dialog - allows user to push button
''when hotkeying to that control
'    Dim bAltIsDown As Boolean
'
''   ensure that toggle does not occur
''   when tabbing toor arrowing to
'    bAltIsDown = (GetKeyState(mpKey_Hex_Alt) < 0)
'
'    If bAltIsDown Then
'        Screen.ActiveControl.Value = 1
'    End If
End Function


Public Function DisableInactiveTabs(frmP As VB.Form)
    Dim ctlP As VB.Control
    On Error Resume Next
    With frmP
        For Each ctlP In .Controls
            If (ctlP.Container Is .MultiPage1) Then
               ctlP.Enabled = (ctlP.Left >= 0)
            End If
        Next
    End With
End Function

Public Function bReleaseVShift() As Boolean
    Dim altscan As Integer
    altscan% = MapVirtualKey(VK_SHIFT, 0)
    If g_VShiftDown = True Then
        keybd_event VK_SHIFT, altscan, KEYEVENTF_KEYUP, 0
        g_VShiftDown = False
    End If
End Function
Function bVMouseClick() As Boolean
    Dim pt As POINTAPI
    Dim curx&, cury&
    Dim distx&, disty&, dl&
    Dim screenx&, screeny&
    Dim ptsperx&, ptspery&
    Dim iIndex As Integer
    Dim iNewIndex As Integer
    Dim recips() As String
    'iIndex = lstList.listindex
    
''   retrieve data from data object - not used in this context
''---we're using global recips type to carry data
''---you could use it, though...
'    xNewDPhrase = Data.GetText
'
''   cancel drop
'    Cancel = True
    
'---capture screen coords in mouse pixels of target row
    screenx& = GetSystemMetrics(SM_CXSCREEN)
    screeny& = GetSystemMetrics(SM_CYSCREEN)
    
'   About how many mouse points per pixel
    ptsperx& = &HFFFF& / screenx&
    ptspery& = &HFFFF& / screeny&
    
    dl& = GetCursorPos(pt)
    curx& = pt.X * ptsperx& '&HFFFF& / screenx&
    cury& = pt.Y * ptspery& '&HFFFF& / screeny&
    
'---virtual click sets list index for drop target row
    mouse_event MOUSEEVENTF_ABSOLUTE Or MOUSEEVENTF_LEFTDOWN, curx, cury, 0, 0
    mouse_event MOUSEEVENTF_ABSOLUTE Or MOUSEEVENTF_LEFTUP, curx, cury, 0, 0

End Function

Public Sub IncrementBuddyCtl(ctlP As UpDown, _
                             Optional ByVal sOffset As Single, _
                             Optional ByVal sMajorTick As Single = 0.25)
    Dim xFormat As String
    Dim dNew As Double
    Dim sCur As Single
    Dim dNextMajorTick As Double
    Dim xTag As String
    Dim xDecimal As String
    
'   regional setting for PC may use something
'   other than period (e.g. comma) as decimal symbol
    xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
    xTag = xSubstitute(ctlP.Tag, ".", xDecimal)
    
'   get offset from tag
'   prop if not supplied
    If sOffset = 0 Then
        sOffset = CSng(xTag)
    End If
    
'   set format based on int/sng
    If InStr(sOffset, xDecimal) Then
        xFormat = "#,##0.00"
    Else
        xFormat = "#,##0"
    End If
    
    With Screen.ActiveForm.Controls(ctlP.BuddyControl)
        If .Text = "" Then
            .Text = 0
        End If
'       get current value of control
        sCur = CSng(.Text)
''       new value is current plus offset, or the nearest greater
''       major tick, whichever is less
'        dNextMajorTick = sCur + (sMajorTick - (((sCur * 100) Mod (sMajorTick * 100)) / 100))
'        dNew = mpMin(sCur + sOffset, dNextMajorTick)
        dNew = sCur + sOffset
'       ensure that value is <= maximum acceptable value
        dNew = mpMin(dNew, ctlP.Max)
        .SetFocus
        .Text = Format(dNew, xFormat)
    End With
End Sub

Public Sub DecrementBuddyCtl(ctlP As UpDown, _
                             Optional ByVal sOffset As Single, _
                             Optional ByVal sMajorTick As Single = 0.25)
    Dim xFormat As String
    Dim dNew As Double
    Dim sCur As Single
    Dim dPrevMajorTick As Double
    Dim xTag As String
    Dim xDecimal As String
    
'   regional setting for PC may use something
'   other than period (e.g. comma) as decimal symbol
    xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
    xTag = xSubstitute(ctlP.Tag, ".", xDecimal)
    
'   get offset from tag
'   prop if not supplied
    If sOffset = 0 Then
        sOffset = CSng(xTag)
    End If
    
'   set format based on int/sng
    If InStr(sOffset, xDecimal) Then
        xFormat = "#,##0.00"
    Else
        xFormat = "#,##0"
    End If
    
    With Screen.ActiveForm.Controls.Item(ctlP.BuddyControl)
'       get current value of control
        sCur = CSng(.Text)
''       new value is current minus offset, or the nearest lower
''       major tick, whichever is greater
'        dPrevMajorTick = sCur - (((sCur * 100) Mod (sMajorTick * 100)) / 100)
'        dNew = mpMax(sCur - sOffset, dPrevMajorTick)
        dNew = sCur - sOffset
'       ensure that value is >= mininum acceptable value
        dNew = mpMax(dNew, ctlP.Min)
        .SetFocus
        .Text = Format(dNew, xFormat)
    End With
End Sub

Public Function bIsDigit(ByVal iKey As Integer, ctlP As VB.TextBox, Optional ByVal bAllowDecimal As Boolean = True) As Boolean
    Dim bIsNumeric As Boolean
    Dim bIsDecimal As Boolean
    Dim bIsGeneralFunction As Boolean
    Dim bShiftPressed As Boolean
    Dim xDecimal As String
    
'   regional setting for PC may use something
'   other than period (e.g. comma) as decimal symbol
    xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
    
'   test ascii id for integer val
    bIsNumeric = (iKey >= 48) And (iKey <= 57)
    bIsDecimal = (iKey = Asc(xDecimal))
    bShiftPressed = IsPressed(KEY_SHIFT)
    bIsGeneralFunction = (iKey = vbKeyLeft) Or _
                         (iKey = vbKeyRight) Or _
                         (iKey = vbKeyHome) Or _
                         (iKey = vbKeyEnd) Or _
                         (iKey = 1000) Or _
                         (iKey = vbKeyBack)
                         
    If bShiftPressed Then
        bIsDigit = False
    ElseIf (bAllowDecimal And bIsDecimal) Then
'       allow only 1 decimal place
        With Screen.ActiveControl
            bIsDigit = (InStr(ctlP.Text, xDecimal) = 0) Or _
                .SelLength = Len(.Text)
        End With
    Else
        bIsDigit = bIsNumeric Or _
                   (bAllowDecimal And bIsDecimal) Or _
                   bIsGeneralFunction
    End If
    
End Function

Public Function AllowDigitsOnly(ByVal KeyAscii As Integer, _
            ctlP As VB.TextBox, _
            Optional ByVal bAllowDecimal As Boolean = True) As Integer
'Returns 0 if keyAscii is not an acceptable
'character for a numeric field
    If Not bIsDigit(KeyAscii, ctlP, bAllowDecimal) Then
        KeyAscii = 0
    End If
    AllowDigitsOnly = KeyAscii
End Function

Public Function bValidatePositionInput(ctlText As VB.TextBox, _
                                       Optional sMin As Single = 0) As Boolean
'alerts to invalid numeric input, else formats
'to correct number of significant digits
    Dim xFormat As String
    
    With ctlText
        If Not bPositionIsValid(.Text, sMin) Then
            On Error Resume Next
            .SetFocus
            .Text = Format(.Text, "#,##0.00")
        Else
            .Text = Format(.Text, "#,##0.00")
            bValidatePositionInput = True
        End If
    End With
End Function

Public Function bValidateSpaceInput(ctlText As VB.TextBox) As Boolean
'alerts to invalid numeric input, else formats
'to correct number of significant digits
    With ctlText
        If Not bPointsAreValid(.Text) Then
            On Error Resume Next
            .SetFocus
            On Error GoTo 0
            .Text = Format(.Text, "#,##0")
        Else
            .Text = Format(.Text, "#,##0")
            bValidateSpaceInput = True
        End If
    End With
End Function

Sub ChangeUpDown(udP As ComCtl2.UpDown, _
                 Optional ByVal bUp As Boolean = True, _
                 Optional ByVal sSmall As Single = 1, _
                 Optional ByVal sMed As Single = 10, _
                 Optional ByVal sLarge As Single = 100)
'changes the value of UpDown ctl by iOffset
    Dim iOffset As Integer
    With udP
        If IsPressed(KEY_CTL) And _
            IsPressed(KEY_MENU) Then
            iOffset = sLarge
        ElseIf IsPressed(KEY_CTL) Then
            iOffset = sMed
        Else
            iOffset = sSmall
        End If
        
        If Not bUp Then
            iOffset = iOffset * -1
        End If
        
        .Value = mpMin(mpMax(.Value + iOffset, .Min), .Max)
    End With
End Sub

Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
    Dim xarP As xArray
    Dim iRows As Integer
    On Error Resume Next
    With tdbCBX
        Set xarP = tdbCBX.Array
        iRows = mpMin(xarP.Count(1), CDbl(iMaxRows))
        .DropdownHeight = (iRows * .RowHeight)
        If Err.Number = 6 Then
 '---use hard value if there's a processor problem -- err 6 is overflow
            .DropdownHeight = (iRows * 239.811)
        End If
    End With
End Sub

Sub CorrectTDBComboMismatch(oTDBCombo As TrueDBList60.TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    Dim oNum As CNumbers
    
    Set oNum = New CNumbers
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = mpMax(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub


Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, App.Title
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Function GetTDBListCount(oTDBCombo As TrueDBList60.TDBCombo) As Long
    Dim xarP As xArray
    Dim iCount As Integer
    
    If Not (oTDBCombo.Array Is Nothing) Then
        Set xarP = oTDBCombo.Array
        
    '   count rows in xarray
        iCount = xarP.Count(1)
    End If
    
    GetTDBListCount = iCount
End Function

Public Sub MatchInXArrayList(KeyCode As Integer, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'first character is KeyCode
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As xArray
    Dim lStartRow As Long
    Dim xChr As String
    
'   get starting row
    lStartRow = ctlP.Bookmark
    
'   start with next row
    lCurRow = lStartRow + 1
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1)
    
'   get the char that we're searching for
    xChr = UCase(Chr(KeyCode))
    
'   loop through xarray until match with first letter is found
    While (xChr <> UCase(Left(xarP.Value(lCurRow, 1), 1)))
'       if at end of xarray, start from beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        Else
            lCurRow = 0
        End If
        
    Wend
    
'   select the matched item
    With ctlP
        .Bookmark = lCurRow
        .SelBookmarks.Remove 0
        .SelBookmarks.Add lCurRow
        .SetFocus
    End With
End Sub

Public Sub MatchCompleteInXArrayList(xString As String, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'full string is xString
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As xArray
    Dim lStartRow As Long
    Dim xChrs As String
    
'   start at the top
    lStartRow = 0
    
'   start with next row
    lCurRow = lStartRow + 1
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1)
    
'   get the char that we're searching for
    xChrs = UCase(xString)
    
'   loop through xarray until match with first letter is found
    While (xChrs <> UCase(xarP.Value(lCurRow, 0)))
'       if at end of xarray, start from beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        Else
            lCurRow = 0
        End If
        
    Wend
    
'   select the matched item
    ctlP.Bookmark = lCurRow
    
End Sub

Public Sub AddItemToTDBCombo(oCtl As TrueDBList60.TDBCombo, _
                             xItem As String, _
                             Optional iIndex As Integer = -1, _
                             Optional lColValue As Long = -1)
                             
'   Adds new row to TDBCombo list
    Dim lRows As Long
    Dim xarP As xArray
    
    Set xarP = oCtl.Array
    
'   count rows in xarray
    lRows = xarP.UpperBound(1)
    
'   add new row at iIndex
    If iIndex >= 0 Then
        xarP.Insert 1, iIndex
    Else    'add as last item
        xarP.Insert 1, lRows + 1
    End If
    
'   set DisplayName column
    xarP(xarP.UpperBound(1), 0) = xItem
    
'   set Value is specified
    If lColValue >= 0 Then
        xarP(xarP.UpperBound(1), 1) = CStr(lColValue)
    Else
        xarP(xarP.UpperBound(1), 1) = xItem
    End If
    
    oCtl.Array = xarP
    
End Sub

Public Sub RemoveItemFromTDBCombo(oCtl As TrueDBList60.TDBCombo, _
                                  iIndex As Integer)
                             
'   removes row at iIndex
    Dim xarP As xArray
    
    Set xarP = oCtl.Array
    
    If Not (xarP Is Nothing) Then
        xarP.Delete 1, iIndex
        oCtl.Array = xarP
    End If
    
End Sub




