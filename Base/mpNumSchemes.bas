Attribute VB_Name = "mpNumSchemes"
Option Explicit

Public g_iTCEntryColor As Integer
Public g_bIsAdmin As Boolean
Public g_xAdminPath As String
Public g_xUserPath As String
Public g_sSession As Single
Public g_xAppPath As String
Public g_lUILanguage As Long
Public g_xAppName As String
Public g_oPNumSty As Word.Template

Public Function GetNumAppPaths() As Long
    On Error Resume Next
    
'   get admin schemes directory
    g_xAdminPath = Word.System.PrivateProfileString(GetAppPath & "\mpN90.ini", _
        "Numbering", "AdminDirectory")
    If g_xAdminPath = "" Then
        g_xAdminPath = GetAppPath & "\Admin Schemes"
    ElseIf (InStr(UCase(g_xAdminPath), "<USERNAME>") > 0) Or _
            (InStr(UCase(g_xAdminPath), "<USER>") > 0) Then
'       use API to get Windows user name
        g_xAdminPath = GetUserVarPath(g_xAdminPath)
    Else
'       use environmental variable
        g_xAdminPath = GetEnvironVarPath(g_xAdminPath)
    End If
    
'   strip trailing slash if necessary
    If Right(g_xAdminPath, 1) = "\" Then
        g_xAdminPath = Left(g_xAdminPath, Len(g_xAdminPath) - 1)
    End If
    
'   get personal directory
    g_xUserPath = Word.System.PrivateProfileString(GetAppPath & "\mpN90.ini", _
        "General", "UserDir")
    If (InStr(UCase(g_xUserPath), "<USERNAME>") > 0) Or _
            (InStr(UCase(g_xUserPath), "<USER>") > 0) Then
'       use API to get Windows user name
        g_xUserPath = GetUserVarPath(g_xUserPath)
    Else
'       use environmental variable
        g_xUserPath = GetEnvironVarPath(g_xUserPath)
    End If
    If Dir(g_xUserPath & "\mpNumbers.sty") = "" Then
        g_xUserPath = Word.Application.Options _
            .DefaultFilePath(wdUserTemplatesPath)
    ElseIf Right(g_xUserPath, 1) = "\" Then
        g_xUserPath = Left(g_xUserPath, Len(g_xUserPath) - 1)
    End If
End Function

Public Function oSchemeSource(ByVal iType As mpSchemeTypes) As Object
'returns the source file of scheme type iType
    If g_xAdminPath = "" Or _
            g_xUserPath = "" Then
        GetNumAppPaths
    End If
    
    Select Case iType
        Case mpSchemeType_Private
            If g_bIsAdmin Then
                Set oSchemeSource = GetTemplate(g_xAdminPath & "\mpNumbers.sty")
            Else
                '9.9.3006 - we're now holding onto the personal mpNumbers.sty to
                'avoid having to cycle through the templates collection each time
                'to get it from a network location in Word 2007
                If g_oPNumSty Is Nothing Then _
                    Set g_oPNumSty = GetTemplate(g_xUserPath & "\mpNumbers.sty")
                Set oSchemeSource = g_oPNumSty
            End If
        Case mpSchemeType_Document
                Set oSchemeSource = ActiveDocument
        Case mpSchemeType_Public
            If g_bIsAdmin Then
                Set oSchemeSource = GetTemplate(GetAppPath & "\mpNumbers.sty.clean")
            Else
                Set oSchemeSource = GetTemplate(GetAppPath & "\mpNumbers.sty")
            End If
        Case mpSchemeType_TOC
            Set oSchemeSource = GetTemplate(GetAppPath & "\mpTOC.sty")
    End Select
End Function

Public Function xGetPropFld(ltScheme As Word.ListTemplate, _
                                iLevel As Integer) As String
    Dim oSource As Object
    Dim bIsSourceFile As Boolean
    Dim iPos As Integer
    Dim iNext As Integer
    Dim xName As String
    Dim i As Integer
    
    Set oSource = ltScheme.Parent.Parent
    
    On Error Resume Next
    bIsSourceFile = Not (oSource _
        .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
    On Error GoTo 0
    
    xName = ltScheme.Name
    
    If bIsSourceFile Then
        iPos = InStr(xName, "|")
        If iPos > 0 Then _
            xName = Left(xName, iPos - 1)
        xGetPropFld = oSource.CustomDocumentProperties(xName & iLevel)
    Else
'       find starting point of level prop
        For i = 1 To iLevel
            iPos = InStr(iPos + 1, xName, "||")
        Next i
    
'       find starting point of next level prop
        iNext = InStr(iPos + 1, xName, "||")
    
'       parse between start and end points -
'       include starting and end pipe
        xGetPropFld = Mid(xName, iPos + 1, _
                     iNext - iPos)
    End If
End Function

Public Function SetPropFld(ltScheme As Word.ListTemplate, _
                            iLevel As Integer, _
                            ByVal xValue As String) As Long
    Dim oSource As Object
    Dim bIsSourceFile As Boolean
    Dim iPos As Integer
    Dim iNext As Integer
    Dim xName As String
    Dim i As Integer
    
    Set oSource = ltScheme.Parent.Parent
    On Error Resume Next
    bIsSourceFile = Not (oSource _
        .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
    On Error GoTo 0
    
    xName = ltScheme.Name
    
    If bIsSourceFile Then
        iPos = InStr(xName, "|")
        If iPos > 0 Then _
            xName = Left(xName, iPos - 1)
        On Error Resume Next
        oSource.CustomDocumentProperties( _
            xName & iLevel) = xValue
        If Err.Number Then
            oSource.CustomDocumentProperties.Add _
                xName & iLevel, False, 4, xValue
        End If
    Else
'       only edit list template name if props are already there
        If InStr(xName, "|") = 0 Then _
            Exit Function
            
'       find starting point of level prop
        For i = 1 To iLevel
            iPos = InStr(iPos + 1, xName, "||")
        Next i
    
'       find starting point of next level prop
        iNext = InStr(iPos + 1, xName, "||")
    
'       insert between start and end points -
'       include starting and end pipe
        ltScheme.Name = Left(xName, iPos) & xValue & _
            Mid(xName, iNext + 1)
    End If
End Function
Function GetLTRoot(xLT As String) As String
    Dim iPos As Integer
    
    iPos = InStr(xLT, "|")
    If iPos = 0 Then
        GetLTRoot = xLT
    Else
        GetLTRoot = Left(xLT, iPos - 1)
    End If
End Function

Function GetFullLTName(xScheme As String, _
                        Optional xSource As String = "") As String
    Dim ltScheme As Word.ListTemplate
    Dim oSource As Object
    
    If xSource = "" Then
        Set oSource = ActiveDocument
    ElseIf InStr(UCase(xSource), "MPNUMBERS.STY") Then
        GetFullLTName = xScheme
        Exit Function
    Else
        Set oSource = Documents(xSource)
    End If
    
    For Each ltScheme In oSource.ListTemplates
        On Error Resume Next
        If Left(ltScheme.Name, Len(xScheme) + 1) = _
                xScheme & "|" Then
            GetFullLTName = ltScheme.Name
            Exit Function
        End If
    Next ltScheme
    
'   if list template isn't in source, return string unchanged
    GetFullLTName = xScheme
End Function

Public Function GetAppPath() As String
'Substitute for App.Path call: first looks in the registry for Public documents location
'If not found, then uses regular App.Path command
    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    
    If g_xAppPath = "" Then
        On Error GoTo ProcError
        Set oReg = New CRegistry
        
        'look for new registry key first
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\Numbering", _
            "DataDirectory", vPath)
        vPath = RTrim(vPath)
        
        If Len(vPath) = 0 Then
            'look for old registry key value
            vPath = String(255, Chr(32))
            lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\Legal MacPac\9.x", _
                "NumberingData", vPath)
            vPath = RTrim(vPath)
        End If
        
        'exists, so set path to value
        If Len(vPath) > 0 Then
            'substitute real path for user var if it exists in string
            If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
                'use API to get Windows user name
                xTemp = GetUserVarPath(CStr(vPath))
            ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
                xTemp = GetCommonDocumentsPath(CStr(vPath))
            Else
                'use environmental variable
                xTemp = GetEnvironVarPath(CStr(vPath))
            End If
            
            'trim trailing slash
            If Right$(xTemp, 1) = "\" Then _
                xTemp = Left$(xTemp, Len(xTemp) - 1)
        
            'if ini is not in specified path, use App.Path
            If Dir(xTemp & "\mpN90.INI") = "" Then
                xTemp = App.Path
            End If
        Else
            'registry value empty, so set path to App.Path
            xTemp = App.Path
        End If
        
        g_xAppPath = xTemp
    End If
        
    'get ui language if we don't already have it
    If g_lUILanguage = 0 Then
        On Error Resume Next
        g_lUILanguage = CLng(Word.System.PrivateProfileString(g_xAppPath & _
            "\mpN90.INI", "General", "UILanguage"))
        On Error GoTo ProcError
        If g_lUILanguage = 0 Then _
            g_lUILanguage = wdEnglishUS
    End If

    GetAppPath = g_xAppPath
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpNumSchemes.GetAppPath"
    Exit Function
End Function

Public Function GetCommonDocumentsPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    
'   get common documents folder
    xBuf = String(255, " ")
    
    SHGetFolderPath 0, CSIDL_COMMON_DOCUMENTS Or CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, xBuf

    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise mpErrInvalidNull, , _
            "Common_Documents is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute common documents at specified point in path
    Set oStr = New CStrings
    GetCommonDocumentsPath = oStr.xSubstitute(xPath, "<Common_Documents>", xBuf)
        
    Exit Function
ProcError:
    Err.Raise Err.Number, _
        "mpNumSchemes.GetCommonDocumentsPath", Err.Source, _
        Err.Description
End Function

Public Function GetUserVarPath(ByVal xPath As String) As String
'substitutes user path for <xToken>
    Dim xBuf As String
    Dim xToken As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    
'   get token - we now accept either "USER" or "USERNAME"
    If InStr(UCase(xPath), "<USER>") > 0 Then
        xToken = "<User>"
    Else
        xToken = "<UserName>"
    End If
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise Err.Number, "mpNumSchemes.GetUserVarPath", _
            "UserName is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute user name at specified point in path
    Set oStr = New CStrings
    GetUserVarPath = oStr.xSubstitute(xPath, xToken, xBuf)
        
    Exit Function
ProcError:
    Err.Raise Err.Number, _
        "mpNumSchemes.GetUserVarPath", Err.Source, _
        Err.Description
End Function

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        xValue = Environ(xToken)
    End If
    
    If xValue <> "" Then
        Set oStr = New CStrings
        GetEnvironVarPath = oStr.xSubstitute(xPath, "<" & xToken & ">", xValue)
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpNumSchemes.GetEnvironPath"
    Exit Function
End Function

Public Function GetMacPacAppPath() As String
'Substitute for App.Path call: first looks in the registry for Public documents location
'If not found, then uses regular App.Path command
    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    
    'GLOG 5056 (2/1/12) - look for new registry key first
    vPath = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\MacPac 9.0", _
        "DataDirectory", vPath)
    vPath = RTrim(vPath)
    
    If Len(vPath) = 0 Then
        'look for old registry key value
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\Legal MacPac\9.x", _
            "MacPacData", vPath)
        vPath = RTrim(vPath)
    End If
    
    'exists, so set path to value
    If Len(vPath) > 0 Then
        'substitute real path for user var if it exists in string
        If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
            'use API to get Windows user name
            xTemp = GetUserVarPath(CStr(vPath))
        ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(CStr(vPath))
        Else
            'use environmental variable
            xTemp = GetEnvironVarPath(CStr(vPath))
        End If
    
        If Dir(xTemp & "\MacPac.ini") = "" Then
            'ini is not in specified location, so set path to App.Path
            xTemp = oReg.GetComponentPath("MPO.CApplication")
        End If
    Else
        'registry value empty, so set path to App.Path
        xTemp = oReg.GetComponentPath("MPO.CApplication")
    End If
        
    'GLOG 5056 - we assume a trailing backslash
    If xTemp <> "" Then
        If Right$(xTemp, 1) <> "\" Then _
            xTemp = xTemp & "\"
    End If
    
    GetMacPacAppPath = xTemp
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpNumSchemes.GetMacPacAppPath"
    Exit Function
End Function

Public Function GetTemplate(xFile As String) As Word.Template
'added in 9.9.3006 to workaround native Word 2007 SP2 bug whereby
'Word.Templates() no longer takes a network path
    Dim oTemplate As Word.Template
    Dim i As Integer
    Dim xName As String
    Dim lPos As Long
    
    On Error Resume Next
    Set oTemplate = Word.Templates(xFile)
    On Error GoTo ProcError
    
    If oTemplate Is Nothing Then
        lPos = InStrRev(xFile, "\")
        xName = Mid$(xFile, lPos + 1)
        For i = 1 To Word.Templates.Count
            If Word.Templates(i).Name = xName Then
                Set oTemplate = Word.Templates(i)
                Exit For
            End If
        Next i
    End If
    
    Set GetTemplate = oTemplate
    Exit Function
    
ProcError:
    Err.Raise Err.Number
End Function

