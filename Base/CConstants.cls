VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConstants"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'item locations
Public Enum DocPosition
     mpAtBOF = 0
     mpAtEOF = 1
     mpAtInsertion = 2
     mpAboveTOA = 3
End Enum

'User Operations
Public Enum mpUserActions
     mpFinish = -1
     mpDlgCancelled = 0
     mpPrint = 1
     mpAddToDoc = 2
End Enum

'keyboard
Public Enum mpKey
     mpKey_UpArrow = 38
     mpKey_DownArrow = 40
     mpKey_Hex_LeftArrow = &H25
     mpKey_Hex_UpArrow = &H26
     mpKey_Hex_RightArrow = &H27
     mpKey_Hex_DownArrow = &H28
     mpKey_Hex_Tab = &H9
     mpKey_Hex_Alt = &H12
     mpKey_Hex_Ctl = &H11
End Enum

Public Enum mpPleadingFirmNameTypes
     mpPleadingFirmNameNone = 0
     mpPleadingFirmNameInFooter = 1
     mpPleadingFirmNameLandscaped = 2
End Enum

'pleading paper types
Public Enum mpPleadingPaperTypes
     mpPleadingPaperNone = 0
     mpPleadingPaper26Line = 26
     mpPleadingPaper28Line = 28
     mpPleadingPaperNoneFNPort = 100
     mpPleadingPaper26LineFNPort = 126
     mpPleadingPaper28LineFNPort = 128
     mpPleadingPaperNoneFNLand = 200
     mpPleadingPaper26LineFNLand = 226
     mpPleadingPaper28LineFNLand = 228
End Enum

'error values
Public Enum mpErrors
     mpCancel = 0
     mpInitializeRetry = 3
     mpTCEntryStylesNotLoaded = 9
     mpTypeMismatch = 13
     mpBlockNotSet = 91
     mpErrInvalidNull = 94
     mpWB_CommandFailed = 102  'off wbasic object
     mpNoListArrays = 380
     mpInvalidMacPacApplicationPath = 612
     mpErrDupePrimaryKey = 3022
     mpErrFileNotFound = 3044
     mpErrDBOpenFailed = 3051
     mpErrDBSQLTooFewParameters = 3061
     mpDBTableDoesNotExist = 3078
     mpItemNotInCollection = 3265
     mpBoilerPlateMissing = 4198
     mpParagraphIsNotNumbered = 4608
     mpBookmarkMissing = 5101
     mpNotValidListLevel = 5148
     mpIncorrectPassword = 5485
     mpStyleDoesNotExist = 5608
     mpItemNameNotFound = 5834
     mpErrListNumNameInUse = 5890
     mpCollectionMemberDoesNotExist = 5941
End Enum

Public Property Get PropertyNotAvailable() As String
    PropertyNotAvailable = mpPropNotAvailable
End Property

Public Property Get AppName() As String
    If g_lUILanguage = wdFrenchCanadian Then
        AppName = "Numérotation MacPac"
    Else
        AppName = mpAppName
    End If
End Property

Public Property Get Slogan() As String
    Slogan = mpSlogan
End Property

Public Property Get DisplayBenchmarks() As String
    DisplayBenchmarks = mpDisplayBenchmarks
End Property

Public Property Get PointsPerIn() As String
    PointsPerIn = mpPointsPerIn
End Property

Public Property Get TwipsPerPoint() As String
    TwipsPerPoint = mpTwipsPerPoint
End Property

Public Property Get TemplateBusiness() As String
    TemplateBusiness = mpTemplateBusiness
End Property

Public Property Get TemplateFax() As String
    TemplateFax = mpTemplateFax
End Property

Public Property Get TemplateDPhrase() As String
    TemplateDPhrase = mpTemplateDPhrase
End Property

Public Property Get TemplateLetter() As String
    TemplateLetter = mpTemplateLetter
End Property

Public Property Get TemplateMemo() As String
    TemplateMemo = mpTemplateMemo
End Property

Public Property Get TemplatePleading() As String
    TemplatePleading = mpTemplatePleading
End Property

Public Property Get TemplatePOS() As String
    TemplatePOS = mpTemplatePOS
End Property

Public Property Get POSTitle() As String
    POSTitle = mpPOSTitle
End Property

Public Property Get TemplateVer() As String
    TemplateVer = mpTemplateVer
End Property

Public Property Get TemplateDepo() As String
    TemplateDepo = mpTemplateDepo
End Property

Public Property Get TemplatePleadingIL() As String
    TemplatePleadingIL = mpTemplatePleadingIL
End Property

Public Property Get PleadingILTitle() As String
    PleadingILTitle = mpPleadingILTitle
End Property

Public Property Get TemplateCert() As String
    TemplateCert = mpTemplateCert
End Property

Public Property Get CertTitle() As String
    CertTitle = mpCertTitle
End Property

Public Property Get TemplateAppellate() As String
    TemplateAppellate = mpTemplateAppellate
End Property

Public Property Get AppellateTitle() As String
    AppellateTitle = mpAppellateTitle
End Property

Public Property Get TemplateNotary() As String
    TemplateNotary = mpTemplateNotary
End Property

Public Property Get TopMarginDefault() As Single
    TopMarginDefault = mpTopMarginDefault
End Property

Public Property Get BottomMarginDefault() As Single
    BottomMarginDefault = mpBottomMarginDefault
End Property

Public Property Get LeftMarginDefault() As Single
    LeftMarginDefault = mpLeftMarginDefault
End Property

Public Property Get RightMarginDefault() As Single
    RightMarginDefault = mpRightMarginDefault
End Property

Public Property Get HeaderDistDefault() As Single
    HeaderDistDefault = mpHeaderDistDefault
End Property

Public Property Get FooterDistDefault() As Single
    FooterDistDefault = mpFooterDistDefault
End Property

Public Property Get PleadingHeaderDistance() As String
    PleadingHeaderDistance = mpPleadingHeaderDistance
End Property

Public Property Get PleadingFooterDistance() As String
    PleadingFooterDistance = mpPleadingFooterDistance
End Property

Public Property Get Pleading26TopMargin() As String
    Pleading26TopMargin = mpPleading26TopMargin
End Property

Public Property Get Pleading26BotMargin() As String
    Pleading26BotMargin = mpPleading26BotMargin
End Property

Public Property Get Pleading26LeftMargin() As String
    Pleading26LeftMargin = mpPleading26LeftMargin
End Property

Public Property Get Pleading26RightMargin() As String
    Pleading26RightMargin = mpPleading26RightMargin
End Property

Public Property Get Pleading28LeftMargin() As String
    Pleading28LeftMargin = mpPleading28LeftMargin
End Property

Public Property Get Pleading28RightMargin() As String
    Pleading28RightMargin = mpPleading28RightMargin
End Property

Public Property Get Pleading28TopMargin() As String
    Pleading28TopMargin = mpPleading28TopMargin
End Property

Public Property Get Pleading28BotMargin() As String
    Pleading28BotMargin = mpPleading28BotMargin
End Property

Public Property Get MsgAppInitializing() As String
    MsgAppInitializing = mpMsgAppInitializing
End Property

Public Property Get MsgInitializing() As String
    MsgInitializing = mpMsgInitializing
End Property

Public Property Get MsgReady() As String
    MsgReady = mpMsgReady
End Property

Public Property Get MsgEditingDPhrase() As String
    MsgEditingDPhrase = mpMsgEditingDPhrase
End Property

Public Property Get MsgEditingBSig() As String
    MsgEditingBSig = mpMsgEditingBSig
End Property

Public Property Get MsgInsertingRogs() As String
    MsgInsertingRogs = mpMsgInsertingRogs
End Property

Public Property Get MsgCreatingDoc() As String
    MsgCreatingDoc = mpMsgCreatingDoc
End Property

Public Property Get MsgWritingTrailer() As String
    MsgWritingTrailer = mpMsgWritingTrailer
End Property

Public Property Get MsgBeginTyping() As String
    MsgBeginTyping = mpMsgBeginTyping
End Property

Public Property Get MsgDefAuthorSet() As String
    MsgDefAuthorSet = mpMsgDefAuthorSet
End Property

Public Property Get MsgBeginInput() As String
    MsgBeginInput = mpMsgBeginInput
End Property

Public Property Get MsgUpdatingDBTables() As String
    MsgUpdatingDBTables = mpMsgUpdatingDBTables
End Property

Public Property Get MsgEditingDBTables() As String
    MsgEditingDBTables = mpMsgEditingDBTables
End Property

Public Property Get MsgRequiredField() As String
    MsgRequiredField = mpMsgRequiredField
End Property

Public Property Get MsgFinished() As String
    MsgFinished = mpMsgFinished
End Property

Public Property Get MsgLSigCreate() As String
    MsgLSigCreate = mpMsgLSigCreate
End Property

Public Property Get MsgInsertingPPaper() As String
    MsgInsertingPPaper = mpMsgInsertingPPaper
End Property

Public Property Get MsgRemovingPPaper() As String
    MsgRemovingPPaper = mpMsgRemovingPPaper
End Property

Public Property Get PageNumFormatTOC() As String
    PageNumFormatTOC = mpPageNumFormatTOC
End Property

Public Property Get PageNumIntroChrDefault() As String
    PageNumIntroChrDefault = mpPageNumIntroChrDefault
End Property

Public Property Get PageNumTrailChrDefault() As String
    PageNumTrailChrDefault = mpPageNumTrailChrDefault
End Property


