VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmSchemeFontsFrench 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   2955
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6210
   Icon            =   "frmSchemeFontsFrench.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2955
   ScaleWidth      =   6210
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   385
      Left            =   3450
      TabIndex        =   10
      Top             =   2490
      Width           =   1100
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      Height          =   385
      Left            =   4590
      TabIndex        =   11
      Top             =   2490
      Width           =   1100
   End
   Begin VB.Frame fraNumber 
      Caption         =   "Police pour num�ros du th�me"
      Height          =   2280
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2890
      Begin TrueDBList60.TDBCombo cmbNumFontName 
         Height          =   585
         Left            =   135
         OleObjectBlob   =   "frmSchemeFontsFrench.frx":058A
         TabIndex        =   2
         Top             =   615
         Width           =   2650
      End
      Begin TrueDBList60.TDBCombo cmbNumFontSize 
         Height          =   585
         Left            =   135
         OleObjectBlob   =   "frmSchemeFontsFrench.frx":24B4
         TabIndex        =   4
         Top             =   1410
         Width           =   2650
      End
      Begin VB.Label lblNumFontSize 
         Caption         =   "Ta&ille:"
         Height          =   240
         Left            =   180
         TabIndex        =   3
         Top             =   1170
         Width           =   750
      End
      Begin VB.Label lblNumFontName 
         Caption         =   "&Nom:"
         Height          =   240
         Left            =   180
         TabIndex        =   1
         Top             =   375
         Width           =   1395
      End
   End
   Begin VB.Frame fraParagraph 
      Caption         =   "Police des paragraphes du th�me"
      Height          =   2280
      Left            =   3160
      TabIndex        =   5
      Top             =   120
      Width           =   2890
      Begin TrueDBList60.TDBCombo cmbFontName 
         Height          =   585
         Left            =   135
         OleObjectBlob   =   "frmSchemeFontsFrench.frx":43DE
         TabIndex        =   7
         Top             =   615
         Width           =   2650
      End
      Begin TrueDBList60.TDBCombo cmbFontSize 
         Height          =   585
         Left            =   135
         OleObjectBlob   =   "frmSchemeFontsFrench.frx":6305
         TabIndex        =   9
         Top             =   1410
         Width           =   2650
      End
      Begin VB.Label lblFontName 
         Caption         =   "No&m:"
         Height          =   240
         Left            =   180
         TabIndex        =   6
         Top             =   375
         Width           =   1395
      End
      Begin VB.Label lblFontSize 
         Caption         =   "&Taille:"
         Height          =   240
         Left            =   180
         TabIndex        =   8
         Top             =   1170
         Width           =   585
      End
   End
End
Attribute VB_Name = "frmSchemeFontsFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_xScheme As String
Private m_SchemeProps As CSchemeProps

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub cmbFontSize_Validate(Cancel As Boolean)
    If (cmbFontSize.Text <> "-Utiliser taille de police courante-") And _
            (cmbFontSize.Text <> "-Utiliser taille de la police du style Normal-") Then
        Cancel = Not bIsValidFontSize(cmbFontSize.Text)
    End If
End Sub

Private Sub cmbNumFontSize_Validate(Cancel As Boolean)
    If (cmbNumFontSize.Text <> "-Utiliser taille de police courante-") And _
            (cmbNumFontSize.Text <> "-Utiliser taille de la police du style Normal-") Then
        Cancel = Not bIsValidFontSize(cmbNumFontSize.Text)
    End If
End Sub

Private Sub cmdCancel_Click()
    m_bCancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub cmdOK_Click()
    m_bCancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Activate()
'   select first item in lists
    Me.cmbFontName.SelectedItem = 0
    Me.cmbFontSize.SelectedItem = 0
    Me.cmbNumFontName.SelectedItem = 0
    Me.cmbNumFontSize.SelectedItem = 0
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim xName As String
    Dim oFont As Font
    Dim aFont As Variant
    Dim iNumFonts As Integer
    Dim xFonts() As String
    Dim bRestrictFonts As Boolean
    Dim xarFonts As xArray
    Dim xarSize As xArray
    
    Set m_SchemeProps = New CSchemeProps
    Set xarFonts = New xArray
    Set xarSize = New xArray
    
'   get fonts
    On Error Resume Next
    bRestrictFonts = xGetAppIni("Numbering", "RestrictFontList")
    On Error GoTo 0
    
    If bRestrictFonts Then
'       load list from ini
        If g_xPermittedFonts(0) <> "" Then
            xarFonts.ReDim 0, UBound(g_xPermittedFonts) + 2, 0, 1
            xarFonts(0, 0) = "-Utiliser police courante-"
            xarFonts(0, 1) = "-Utiliser police courante-"
            xarFonts(1, 0) = "-Utiliser police du style Normal-"
            xarFonts(1, 1) = "-Utiliser police du style Normal-"
            For i = LBound(g_xPermittedFonts) To UBound(g_xPermittedFonts)
                xarFonts(i + 2, 0) = g_xPermittedFonts(i)
                xarFonts(i + 2, 1) = g_xPermittedFonts(i)
            Next i
        End If
    Else
'       load system fonts
        'GLOG 4839 - omit vertical fonts (names start with '@')
        For Each aFont In Word.FontNames
            If Left$(aFont, 1) <> "@" Then
                iNumFonts = iNumFonts + 1
            End If
        Next aFont
        ReDim xFonts(iNumFonts - 1)
    
'       stick in variant array
        For Each aFont In Word.FontNames
            If Left$(aFont, 1) <> "@" Then
                xFonts(i) = aFont
                i = i + 1
            End If
        Next aFont
    
'       sort array
        WordBasic.SortArray xFonts
    
'       add array items to lists
        xarFonts.ReDim 0, UBound(xFonts) + 2, 0, 1
        xarFonts(0, 0) = "-Utiliser police courante-"
        xarFonts(0, 1) = "-Utiliser police courante-"
        xarFonts(1, 0) = "-Utiliser police du style Normal-"
        xarFonts(1, 1) = "-Utiliser police du style Normal-"
        For i = 0 To iNumFonts - 1
            xarFonts(i + 2, 0) = xFonts(i)
            xarFonts(i + 2, 1) = xFonts(i)
        Next i
    End If
    
    Me.cmbFontName.Array = xarFonts
    ResizeTDBCombo Me.cmbFontName, 22
    Me.cmbNumFontName.Array = xarFonts
    ResizeTDBCombo Me.cmbNumFontName, 22
    
'   get font sizes
    xarSize.ReDim 0, 21, 0, 1
    xarSize(0, 0) = "-Utiliser taille de police courante-"
    xarSize(0, 1) = "-Utiliser taille de police courante-"
    xarSize(1, 0) = "-Utiliser taille de la police du style Normal-"
    xarSize(1, 1) = "-Utiliser taille de la police du style Normal-"
    For i = 7 To 26
        xarSize(i - 5, 0) = CStr(i)
        xarSize(i - 5, 1) = CStr(i)
    Next i
    Me.cmbFontSize.Array = xarSize
    ResizeTDBCombo Me.cmbFontSize, 22
    Me.cmbNumFontSize.Array = xarSize
    ResizeTDBCombo Me.cmbNumFontSize, 22
    
End Sub

Private Sub cmbNumFontName_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNumFontName, Reposition
    Exit Sub
ProcError:
    RaiseError "frmSchemeFonts.cmbNumFontName_Mismatch"
    Exit Sub
End Sub

Private Sub cmbFontName_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbFontName, Reposition
    Exit Sub
ProcError:
    RaiseError "frmSchemeFonts.cmbFontName_Mismatch"
    Exit Sub
End Sub

Private Sub cmbNumFontSize_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNumFontSize, Reposition
    Exit Sub
ProcError:
    RaiseError "frmSchemeFonts.cmbNumFontSize_Mismatch"
    Exit Sub
End Sub

Private Sub cmbFontSize_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbFontSize, Reposition
    Exit Sub
ProcError:
    RaiseError "frmSchemeFonts.cmbFontSize_Mismatch"
    Exit Sub
End Sub
