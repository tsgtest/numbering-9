VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmChangeScheme 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Change Scheme"
   ClientHeight    =   4530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5880
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4530
   ScaleWidth      =   5880
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton rdCurrentDoc 
      Caption         =   "Document"
      Height          =   210
      Left            =   4185
      TabIndex        =   8
      Top             =   6015
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.OptionButton rdNumbers 
      Caption         =   "mpNumbers.sty"
      Height          =   210
      Left            =   3510
      TabIndex        =   7
      Top             =   5925
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.PictureBox imgPreview 
      Enabled         =   0   'False
      Height          =   855
      Left            =   4275
      ScaleHeight     =   795
      ScaleWidth      =   900
      TabIndex        =   4
      Top             =   5985
      Width           =   960
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   385
      Left            =   4650
      TabIndex        =   3
      Top             =   3990
      Width           =   1100
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   385
      Left            =   3510
      TabIndex        =   2
      Top             =   3990
      Width           =   1100
   End
   Begin ComctlLib.TreeView tvwSchemes 
      Height          =   4050
      Left            =   150
      TabIndex        =   1
      Top             =   345
      Width           =   2325
      _ExtentX        =   4101
      _ExtentY        =   7144
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   573
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblMsg 
      Caption         =   "###"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1710
      Left            =   2610
      TabIndex        =   10
      Top             =   345
      Width           =   3150
   End
   Begin MSForms.ComboBox lstSchemes 
      Height          =   315
      Left            =   3360
      TabIndex        =   9
      Top             =   5625
      Width           =   2505
      VariousPropertyBits=   746604571
      DisplayStyle    =   3
      Size            =   "4410;556"
      ColumnCount     =   2
      cColumnInfo     =   1
      MatchEntry      =   1
      ShowDropButtonWhen=   2
      FontHeight      =   165
      FontCharSet     =   0
      FontPitchAndFamily=   2
      Object.Width           =   "0"
   End
   Begin VB.Label lblView 
      Caption         =   "View Schemes in"
      Height          =   210
      Left            =   3870
      TabIndex        =   6
      Top             =   5940
      Visible         =   0   'False
      Width           =   1305
   End
   Begin MSForms.ListBox lstSchemesOLD 
      Height          =   600
      Left            =   3735
      TabIndex        =   5
      Top             =   6075
      Width           =   510
      VariousPropertyBits=   746586139
      ScrollBars      =   3
      DisplayStyle    =   2
      Size            =   "900;1058"
      ColumnCount     =   2
      cColumnInfo     =   1
      MatchEntry      =   0
      FontName        =   "Tahoma"
      FontHeight      =   165
      FontPitchAndFamily=   2
      Object.Width           =   "0"
   End
   Begin VB.Label lblBasedOn 
      Caption         =   "&Change To Scheme:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   165
      TabIndex        =   0
      Top             =   135
      Width           =   1905
   End
End
Attribute VB_Name = "frmChangeScheme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bOK As Boolean
Public Cancelled As Boolean
Private m_xDefScheme As String
Private m_iDefSchemeType As Integer

Public Property Get DefaultScheme() As String
    DefaultScheme = m_xDefScheme
End Property

Public Property Let DefaultScheme(xNew As String)
    m_xDefScheme = xNew
End Property

Public Property Get DefaultSchemeType() As mpSchemeTypes
    DefaultSchemeType = m_iDefSchemeType
End Property

Public Property Let DefaultSchemeType(iNew As mpSchemeTypes)
    m_iDefSchemeType = iNew
End Property

Public Property Get Scheme() As String
    On Error Resume Next
    Scheme = Mid(Me.tvwSchemes.SelectedItem.Key, 2)
End Property

Public Property Get SchemeDisplayName() As String
    On Error Resume Next
    SchemeDisplayName = Me.tvwSchemes.SelectedItem.Text
End Property

Public Property Get SchemeType() As mpSchemeTypes
    On Error Resume Next
    SchemeType = Me.tvwSchemes.SelectedItem.Parent.Index
End Property

Private Sub cmdCancel_Click()
    Me.Hide
    Cancelled = True
    Application.ScreenRefresh
End Sub

Private Sub cmdOK_Click()
    If Me.SchemeType <> mpSchemeType_Category Then
        Me.Hide
        Cancelled = False
        Application.ScreenRefresh
    End If
End Sub

Private Sub Form_Activate()
    On Error GoTo ProcError
    Me.tvwSchemes.Nodes(Me.DefaultSchemeType & _
            Me.DefaultScheme).Selected = True
    Exit Sub
    
ProcError:
    If Err.Number = 35601 Then 'ElementNotFound
        Me.tvwSchemes.Nodes(mpSchemeType_Public & _
            g_xFSchemes(0, 0)).Selected = True
    Else
        Err.Raise Err.Number, _
                  "frmChangeScheme.Form_Activate", _
                  Err.Description
    End If
    Exit Sub
End Sub

Private Sub Form_Load()
    iGetSchemes g_xDSchemes(), mpSchemeType_Document
    RefreshSchemesList Me.tvwSchemes, , , g_bShowPersonalSchemes
    Me.Cancelled = True
End Sub

Private Sub tvwSchemes_NodeClick(ByVal Node As ComctlLib.Node)
    Me.cmdOK.Enabled = (Me.SchemeType <> mpSchemeType_Category)
End Sub
