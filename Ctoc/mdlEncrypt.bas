Attribute VB_Name = "mdlEncrypt"
Option Explicit

'******************************************************
'THESE ARE THE MACPAC 9.3.1 FUNCTIONS
'******************************************************

Public Function iGetSeed(ByVal xSeed As String) As Integer
    Dim iSeedLen As Integer
    Dim xChar As String
    Dim iDigit As Integer
    Dim iTemp As Integer
    
'   uppercase seed string and get length
    xSeed = UCase(xSeed)
    iSeedLen = Len(xSeed)
    
    If iSeedLen > 0 Then
'       cycle through seed string and turn into number 'iDigit'
        While Len(xSeed)
            xChar = Mid(xSeed, 1, 1)
            iDigit = iDigit + Asc(xChar)
            xSeed = Mid(xSeed, 2)
        Wend
        iTemp = iDigit / iSeedLen
    Else
        iTemp = iSeedLen
    End If
    
'   return seed number
    iGetSeed = iTemp
    
End Function

Public Function EncryptText(xText As String)
    Dim i As Integer
    Dim iChar As Integer
    Dim vScram As Variant
    Dim vTemp As Variant
    Dim iSeed As Integer
    Dim iRnd As Integer
    
'this function will encrypt supplied xText
'unless it already is encrypted

'   Initialize random-number generator.
    Randomize

'   Generate random value between 145 and 164.
    iRnd = CInt((20 * Rnd) + 145)

'   convert to number between 1 and 20
    iSeed = iRnd - 144
    
    If Not Left(xText, 2) = "~}" Then
        For i = 1 To Len(xText)
            iChar = Asc(Mid(xText, i, 1))
            ' Encrypted character may be above ASCII 255 so use ChrW
            vScram = vScram + ChrW(((iChar + g_iSeed) + (i Mod 5)) - iSeed)
        Next i
'*************************************************
'       add encryption markers:
'       - 1st and 2nd characters are constants
'       - 3rd character is random seed
'       - 4th character is value returned by iGetSeed() + 144
'       - vScram is actual variable value
'*************************************************
        vTemp = "~}" & Chr(iRnd) & Chr(g_iSeed + 144) & vScram
    Else
        vTemp = vScram
    End If

    EncryptText = vTemp

End Function

Public Function DecryptText(ByVal vText As Variant)
    Dim i As Integer
    Dim iChar As Integer
    Dim vScram As Variant
    Dim iSeed As Integer
    Dim iRnd As Integer
    
'this function will decrypt string
'encrypted by EncryptText function

'   first 2 characters are encryption markers
    If Left(vText, 2) = "~}" Then
'       3rd character is variable seed
        iRnd = Asc(Mid(vText, 3, 1))
        iSeed = iRnd - 144
        
'       stri((iChar - g_iSeed) - (i Mod 5)) + iSeedng to decrypt starts at 5th character
        vText = Mid(vText, 5)
        For i = 1 To Len(vText)
            On Error Resume Next
            If Asc(Mid(vText, i, 1)) <> AscW(Mid(vText, i, 1)) Then
                ' Use AscW because encrypted character may be beyond ASCII 255 limit
                ' However, it shouldn't be higher than
                ' ((255 + g_iSeed) + (i Mod 5)) - iSeed
                ' So use regular Asc function for those characters (Unicode)
                If AscW(Mid(vText, i, 1)) <= ((255 + g_iSeed) + (i Mod 5)) - iSeed Then
                    iChar = AscW(Mid(vText, i, 1))
                Else
                    iChar = Asc(Mid(vText, i, 1))
                End If
            Else
                iChar = Asc(Mid(vText, i, 1))
            End If
            On Error GoTo 0
            vScram = vScram & Chr(((iChar - g_iSeed) - (i Mod 5)) + iSeed)
        Next i
    Else
        vScram = vText
    End If
    
    DecryptText = vScram
End Function
