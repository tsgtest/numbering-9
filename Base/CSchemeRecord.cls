VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSchemeRecords"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CSchemeRecords Collection Class
'   created 6/3/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   CSchemeRecords Class - this class manages the
'   "SchemeX" doc props in a source file
'**********************************************************

Option Explicit

'types of numbering schemes
Public Enum mpSchemeTypes
    mpSchemeType_Category = 0
    mpSchemeType_Document = 1
    mpSchemeType_Public = 2
    mpSchemeType_Private = 3
    mpSchemeType_TOC = 4
End Enum

Public Enum mpRecordFields
    mpRecField_Alias = 1
    mpRecField_Origin = 2
    mpRecField_TOCScheme = 3
    mpRecField_DynamicFonts = 4
    mpRecField_Description = 5
End Enum

Public Enum mpTOCRecordFields
    mpTOCRecField_Name = 1
    mpTOCRecField_Alias = 2
End Enum

'the dynamic fonts field is now also being used to
'store the dynamic spacing property (9.8.0)
Public Enum mpDynamicFieldValues
    mpDynamic_SpacingOnly = 0
    mpDynamic_All = 1
    mpDynamic_None = 2
    mpDynamic_FontsOnly = 3
End Enum

'**********************************************************
'Methods
'**********************************************************
Public Function Source(iType As mpSchemeTypes) As Object
'returns the source file of the scheme
    Set Source = oSchemeSource(iType)
End Function

Public Function DeleteRecord(ByVal xScheme As String) As Long
'deletes doc props
    Dim i As Integer
    Dim tmpPNumSty As Word.Template
    Dim oDescription As Object
    
    Set tmpPNumSty = oSchemeSource(mpSchemeType_Private)
    
'   delete level props
    For i = 1 To 9
        tmpPNumSty.CustomDocumentProperties( _
                        xScheme & i).Delete
    Next i
    
    'delete description (added in 9.9.5001, so usually won't be there)
    On Error Resume Next
    tmpPNumSty.CustomDocumentProperties(xScheme & "Description").Delete
End Function

Public Function AddRecord(oScheme As CNumScheme) As Long
'adds scheme properties string to level property
'of List Level 1 of specified scheme
    Dim iNumSchemes As Integer
    Dim i As Integer
    Dim oSource As Object
    
    On Error GoTo ProcError
    
'   fill new doc prop with values in oSchemes
    SetRecord oScheme, _
              oScheme.SchemeType
              
    Exit Function
ProcError:
    AddRecord = Err.Number
End Function

Public Function SchemeCount(Optional ByVal iType As mpSchemeTypes = mpSchemeType_Private, _
                            Optional ByVal oSource As Object) As Integer
'returns the number of records in oSource
    Dim i As Integer
    Dim ltP As Word.ListTemplate
    Dim oNum As CNumTOC
    Dim xScheme As String
    
'   get source from Type if
'   source is not supplied
    If (oSource Is Nothing) Then
        Set oSource = Source(iType)
    End If
    
    If oSource.Name = "mpTOC.sty" Then
'       cycle through doc props,
'       counting until empty
        Do
            i = i + 1
            xScheme = "Scheme" & i
            On Error Resume Next
        Loop Until (oSource.CustomDocumentProperties(xScheme) Is Nothing)
        i = i - 1
    Else
'       cycle through listtemplates,
'       counting MacPac ones
        Set oNum = New CNumTOC
        For Each ltP In oSource.ListTemplates
            If ltP.Name <> "" Then
'                If oNum.IsMacPacScheme(ltP.Name, , oSource) Then
                If oNum.bIsMPListTemplate(ltP) Then
                    i = i + 1
                End If
            End If
        Next ltP
    End If
    SchemeCount = i
End Function

Public Function GetRecord(ByVal xScheme As String, _
                Optional ByVal iType As mpSchemeTypes = mpSchemeType_Private, _
                Optional ByVal oSource As Object) As CNumScheme
'returns a mpScheme var with sub vars
'filled from doc prop with name xName
'in source template/document oSource
    Dim oScheme As CNumScheme
    Dim iID As Integer
    Dim iDynamic As mpDynamicFieldValues
    
'   get source from Type if
'   source is not supplied
    If (oSource Is Nothing) Then
        Set oSource = Source(iType)
    End If
    
    Set oScheme = New CNumScheme
    
'   fill props of scheme
    With oScheme
        .Name = xScheme
        .Alias = GetField(xScheme, _
                          mpRecField_Alias, _
                          , _
                          oSource)
        .Origin = GetField(xScheme, _
                           mpRecField_Origin, _
                           , _
                           oSource)
        .TOCScheme = GetField(xScheme, _
                              mpRecField_TOCScheme, _
                              , _
                              oSource)
                              
'       this field now does double duty (9.8.0)
        iDynamic = GetField(xScheme, _
                              mpRecField_DynamicFonts, _
                              , _
                              oSource)
        .DynamicFonts = Abs((iDynamic = mpDynamic_All) Or _
            (iDynamic = mpDynamic_FontsOnly))
        .DynamicSpacing = Abs((iDynamic = mpDynamic_All) Or _
            (iDynamic = mpDynamic_SpacingOnly))
            
        .SchemeType = iType
        
        '9.9.5001- description
        .Description = GetField(xScheme, mpRecField_Description, , oSource)
    End With
    
    Set GetRecord = oScheme
End Function

Public Function SetRecord(oScheme As CNumScheme, _
                          Optional ByVal iType As mpSchemeTypes, _
                          Optional ByVal oSource As Object) As String
'sets the value of the level property
'of list level 1 of the specified Scheme
    Dim iDynamic As mpDynamicFieldValues
    
'   get source from Type if
'   source is not supplied
    If (oSource Is Nothing) Then
        Set oSource = Source(iType)
    End If
        
    With oScheme
'       set alias
        SetField .Name, _
                 mpRecField_Alias, _
                 .Alias, , _
                 oSource
'       set origin
        SetField .Name, _
                 mpRecField_Origin, _
                 .Origin, , _
                 oSource
'       set TOCScheme
        SetField .Name, _
                 mpRecField_TOCScheme, _
                 .TOCScheme, , _
                 oSource
                 
'       set dynamic fonts/spacing
        iDynamic = GetDynamicFieldValue(.DynamicFonts, .DynamicSpacing)
        SetField .Name, _
                 mpRecField_DynamicFonts, _
                 iDynamic, , _
                 oSource
                 
        '9.9.5001- description
        SetField .Name, mpRecField_Description, .Description, , oSource
    End With
End Function

Public Function GetField(ByVal xScheme As String, _
                         ByVal iField As mpRecordFields, _
                         Optional ByVal iType As mpSchemeTypes, _
                         Optional ByVal oSource As Object) As String
'returns specified field from level property with
'name xName in template/document oSource
    Dim xTemp As String
    Dim iPos1 As Integer
    Dim iPos2 As Integer
    Dim i As Integer
    Dim oTempSource As Object
    Dim bIsSourceFile As Boolean
    Dim xLT As String
    Dim xPropName As String
    
'   get source from type if not supplied
    If (oSource Is Nothing) Then
        Set oTempSource = Source(iType)
    Else
        Set oTempSource = oSource
    End If
    
    On Error Resume Next
    bIsSourceFile = Not (oTempSource _
        .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
    On Error GoTo 0

    If bIsSourceFile Then
        xLT = GetLTRoot(xScheme)
    Else
        xLT = GetFullLTName(xScheme)
    End If
        
'   get value of doc prop
    If iField <> mpRecField_Description Then
        On Error Resume Next
        xTemp = xGetPropFld(oTempSource.ListTemplates(xLT), 1)
        On Error GoTo 0
        Set oTempSource = Nothing
        
        If Len(xTemp) Then
            'get pipe that precedes the requested field
            For i = 1 To iField
                iPos1 = InStr(iPos1 + 1, xTemp, "|")
            Next i
        
            'get pipe that succeeds the requested field
            iPos2 = InStr(iPos1 + 1, xTemp, "|")
        
            If iPos2 Then
                'return the contents in between the 2 pipes
                GetField = Mid(xTemp, iPos1 + 1, iPos2 - iPos1 - 1)
            Else
                'return the contents from pipe to end of string
                GetField = Mid(xTemp, iPos1 + 1)
            End If
        End If
    Else
        'description is in a different place
        If bIsSourceFile Then
            'doc prop
            xPropName = "Description"
            If UCase$(Right$(CStr(oTempSource), 4)) <> ".MPN" Then
                'sty file - add scheme identifier
                xPropName = xScheme & xPropName
            End If
            On Error Resume Next
            GetField = oTempSource.CustomDocumentProperties(xPropName)
        End If
    End If
End Function

Public Sub SetField(ByVal xScheme As String, _
                    ByVal iField As mpRecordFields, _
                    ByVal xValue As String, _
                    Optional ByVal iType As mpSchemeTypes, _
                    Optional ByVal oSource As Object)
'sets field iField in level property with
'name xName in template/document oSource to
'value xValue
    Dim xTemp As String
    Dim iPos1 As Integer
    Dim iPos2 As Integer
    Dim i As Integer
    Dim bIsSourceFile As Boolean
    Dim xLT As String
    
'   get source from type if not supplied
    If (oSource Is Nothing) Then
        Set oSource = Source(iType)
    End If
        
    On Error Resume Next
    bIsSourceFile = Not (oSource _
        .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
    On Error GoTo 0

    If bIsSourceFile Then
        xLT = GetLTRoot(xScheme)
    Else
        xLT = GetFullLTName(xScheme)
    End If
        
    If iField <> mpRecField_Description Then
        'get value of doc prop
        xTemp = xGetPropFld(oSource.ListTemplates(xLT), 1)
        
        'get pipe that precedes the requested field
        For i = 1 To iField
            iPos1 = InStr(iPos1 + 1, xTemp, "|")
        Next i
        
        'get pipe that succeeds the requested field
        iPos2 = InStr(iPos1 + 1, xTemp, "|")
        
        If iPos2 Then
            'return the contents in between the 2 pipes
            xValue = Left(xTemp, iPos1) & xValue & Mid(xTemp, iPos2)
        Else
            xValue = Left(xTemp, iPos1) & xValue & "|"
        End If
        
        'set level prop to new value
        SetPropFld oSource.ListTemplates(xLT), 1, xValue
    Else
        'description is in a different place
        If bIsSourceFile Then
            'doc prop
            On Error Resume Next
            oSource.CustomDocumentProperties(xScheme & "Description") = xValue
            If Err.Number Then
                oSource.CustomDocumentProperties.Add _
                    xScheme & "Description", False, 4, xValue
            End If
        End If
    End If
    
    Set oSource = Nothing
End Sub

Public Function GetDynamicFieldValue(ByVal iFonts As Integer, _
                                     ByVal iSpacing As Integer) _
                                     As mpDynamicFieldValues
'gets appropriate value for new combined field (9.8.0)
    If iFonts Then
        If iSpacing Then
            GetDynamicFieldValue = mpDynamic_All
        Else
            GetDynamicFieldValue = mpDynamic_FontsOnly
        End If
    Else
        If iSpacing Then
            GetDynamicFieldValue = mpDynamic_SpacingOnly
        Else
            GetDynamicFieldValue = mpDynamic_None
        End If
    End If
End Function

