Attribute VB_Name = "mpVariables"
'global environment vars
Public envMpApp As mpAppEnvironment
Public envMpDoc As mpDocEnvironment

'paths
Public xStartPath As String
Public xWorkgroupPath As String
Public xUserPath As String
Public xLitigationPath As String

'files
Public xBP As String
Public xUserIni As String
Public xFirmIni As String
Public xTOCSTY As String

'misc
Public bRet As Variant
Public lRet As Long
Public xMsg As String
Public iUserChoice  As Integer
Public xFirmName As String
Public g_oStatus As mpSCR.CStatus
Public g_bIsXP As Boolean
Public g_xSmartOne As String
Public g_xSmartTwo As String
Public g_xBullet As String
Public g_bIsMP10 As Boolean
Public g_iWordVersion As mpWordVersions
Public g_bXMLSupport As Boolean
Public g_bOrganizerSavePrompt As Boolean

'options
Public g_xTOCLocations() As String
Public g_xBoilerplates() As String
Public g_bAllowHeaderFooterEdit As Boolean
Public g_xPageNoIntroChar As String
Public g_xPageNoTrailingChar As String
Public g_iPageNoStyle As Integer
Public g_bApplyHeadingColor As Boolean
Public g_xDSchemes() As String
Public g_xAbbreviations() As String
Public g_xSentenceDefs() As String
Public g_bIncludeSchedule As Boolean
Public g_xScheduleStyles() As String
Public g_xScheduleLevels() As String
Public g_bAllowTOCAsField As Boolean
Public g_bInsertTOCAsField As Boolean
Public g_bForceTCEntries As Boolean
Public g_bForceLevels As Boolean
Public g_iDefaultLevelStart As Integer
Public g_iDefaultLevelEnd As Integer
Public g_bApplyTOC9StyleDefault As Boolean
Public g_vPreserveLineBreaks As Variant
Public g_bPreserveUndoList As Boolean
Public g_bPreserveUndoListTOC As Boolean
Public g_bUseTOCStyleLimitWorkaround As Boolean
Public g_bMarkWithStyleSeparators As Boolean
Public g_bUseNewTabAdjustmentRules As Boolean
Public g_iTOCDialogStyle As mpTOCDialogStyles
Public g_iIncludeInTOCDefault As Integer
Public g_bVariablesInBoilerplate As Boolean
Public g_bDisplayBenchmarks As Boolean

'forms
Public oCustTOC As mpctoc.CCustomTOC

'language
Public g_xTCPrefix As String
Public g_lUILanguage As Long
Public g_xTOCStatusMsg As String
