Attribute VB_Name = "mpRange"
Option Explicit
Function bIsPageBreak(rngPointer As Word.Range) As Boolean
'returns true if rngPointer is a page break -
'necessary because Word Library has no property/method
'to distinguish among chr(12) types
    Dim iPageNum1 As Integer
    Dim iPageNum2 As Integer
    Dim iSecNum1 As Integer
    Dim iSecNum2 As Integer
    Dim rngTemp As Word.Range
    
    Set rngTemp = rngPointer.Duplicate
    
    With rngTemp
        If .Text <> Chr(12) Then
            bIsPageBreak = False
        Else
            iPageNum1 = .Information(wdActiveEndPageNumber)
            iSecNum1 = .Information(wdActiveEndSectionNumber)
            .Move wdCharacter, -1
            iPageNum2 = .Information(wdActiveEndPageNumber)
            iSecNum2 = .Information(wdActiveEndSectionNumber)
            If (iSecNum1 = iSecNum2) And _
                    (iPageNum1 <> iPageNum2) Then
                bIsPageBreak = True
            End If
            .Move wdCharacter, 1
        End If
    End With
End Function

Function bUnderlineToLengthOfLongest(rngLocation As Word.Range, _
                                     Optional sDefaultTabStop As Single = 36) As Boolean
    
    Const mpHPos = wdHorizontalPositionRelativeToTextBoundary
    Const mpVPos = wdVerticalPositionRelativeToPage
    
    Dim iPosition
    Dim iLongestLine
    Dim iLeftEdge
    Dim iRightEdge
    Dim iTabStops As Integer
    Dim sRightIndentPos As Single
    Dim lTabPos As Long
    Dim iNumTabs As Integer
    Dim bIsHangingIndent As Boolean
    Dim i As Integer
    Dim tsExisting As Word.TabStop
    Dim iListLevel As Integer
    Dim rngLineEnd As Word.Range
    Dim iVPosition
    
    Application.ScreenUpdating = False
    
    ActiveWindow.View.ShowAll = False
    
    rngLocation.Select
    With Selection
        lTabPos = InStr(.Text, vbTab)
        iNumTabs = lCountChrs(.Text, vbTab)
        .Collapse wdCollapseStart
        .EndKey wdLine, wdMove
        If .Text = vbCr Then
            If lTabPos And .ParagraphFormat.FirstLineIndent < 0 Then
                rngLocation.MoveStart wdCharacter, lTabPos
            End If
            rngLocation.Underline = wdUnderlineSingle
            Exit Function
        End If
        
        If .ParagraphFormat.Alignment = wdAlignParagraphCenter Then
            iRightEdge = .Information(mpHPos)
            .HomeKey wdLine, wdMove
            iLeftEdge = .Information(mpHPos)
            .MoveDown wdLine, 1, wdMove
            .EndKey wdLine, wdMove
            While Selection.InRange(rngLocation)
                iPosition = .Information(mpHPos)
                If iPosition > iRightEdge Then _
                    iRightEdge = iPosition
                .HomeKey wdLine, wdMove
                iPosition = .Information(mpHPos)
                If iPosition < iLeftEdge Then _
                    iLeftEdge = iPosition
                .MoveDown wdLine, 1, wdMove
                .EndKey wdLine, wdMove
            Wend
            .HomeKey wdLine, wdExtend
            Set rngLineEnd = ActiveDocument.Range(.End, .End)
            
            If .Next(wdCharacter).Information(mpHPos) = iRightEdge Then
'           lines are the same length - add no spaces
                GoTo skipHardSpaces
            End If
            
            iVPosition = .Information(mpVPos)
            While (.Information(mpHPos) < iRightEdge) And _
                    (.Information(mpVPos) = iVPosition)
                .InsertAfter Chr(160)
                .EndKey wdLine, wdMove
                If .Information(mpHPos) < iRightEdge Then
                    .HomeKey wdLine, wdMove
                    .InsertAfter Chr(160)
                    .EndKey wdLine, wdMove
                End If
            Wend
            
            If .Information(mpVPos) > iVPosition Then
'               last line wrapped - delete responsible spaces
                .HomeKey wdLine, wdMove
                While .Characters(1) = Chr(160)
                    .Delete
                Wend
                .EndKey wdLine, wdMove
                While .Information(mpVPos) > iVPosition
                    .Move wdCharacter, -1
                    If .Characters(1) = Chr(160) Then _
                        .Delete
                    .EndKey wdLine, wdMove
                Wend
            End If
skipHardSpaces:
            i = i
        Else
            iLongestLine = .Information(mpHPos)
            .MoveDown wdLine, 1, wdMove
            .EndKey wdLine, wdMove
            While Selection.InRange(rngLocation)
                iPosition = .Information(mpHPos)
                If iPosition > iLongestLine Then _
                    iLongestLine = iPosition
                .MoveDown wdLine, 1, wdMove
                .EndKey wdLine, wdMove
            Wend
            iPosition = .Information(mpHPos)
            
            If iPosition < iLongestLine Then
'               add tab stops every x points if hanging indent
'               and >1 tab or not hanging and > 0 tabs.
                With .ParagraphFormat
                    bIsHangingIndent = (.FirstLineIndent < .LeftIndent)
                    With rngLocation
                        iNumTabs = lCountChrs(.Text, vbTab)
                        With .ListFormat
                            If .ListType <> wdListNoNumbering Then
                                iListLevel = .ListLevelNumber
                                If .ListTemplate.ListLevels(iListLevel).TrailingCharacter = wdTrailingTab Then
                                    iNumTabs = iNumTabs + 1
                                End If
                            End If
                        End With
                    End With
                    If bIsHangingIndent + iNumTabs Then
                        With ActiveDocument.PageSetup
                            sRightIndentPos = .PageWidth - _
                                (.LeftMargin + .RightMargin)
                        End With
                        
'                       get position of 1st custom tab stop,
'                       then specify next half -inch
                        For Each tsExisting In .TabStops
                            If tsExisting.CustomTab Then
                                i = tsExisting.Position
'                               specify next half inch
                                i = i + (36 - (i Mod 36))
                                Exit For
                            End If
                        Next tsExisting
                        
                        If i = Empty Then _
                            i = sDefaultTabStop
                            
                        While i <= iLongestLine
                            .TabStops.Add i
                            i = i + sDefaultTabStop
                        Wend
                    End If
                End With
                With .ParagraphFormat.TabStops
                    .Add iLongestLine, wdAlignTabRight
                End With
                While .Information(mpHPos) < Int(iLongestLine)
                    .InsertAfter vbTab
                    .EndKey wdLine, wdMove
                Wend
            End If
        End If
        .HomeKey wdLine, wdExtend
        .Font.Underline = wdUnderlineSingle
    End With
    
End Function


Public Function bUnderLineHeadingToggle() As Boolean
' converted from WordBasic code in MacPac 6/7.
' eventually should be rewritten -
' partially rewritten 1/11/98 - df

    Dim ShowAllStart As Boolean
    Dim IsUnderlined As Boolean
    Dim CurPosInInches As Single
    Dim bUnderlined As Boolean
    Dim bSelectEOPara As Boolean
    Dim rngLocation As Word.Range
    Dim bIsOneLineHanging As Boolean
    Dim iTabPosition As Integer
    Dim iFirstTabStop As Integer
    Dim iNumTabStops As Integer
    Dim tabExisting As TabStop
    Dim rngStart As Word.Range
    Dim i As Integer
     
    Application.ScreenUpdating = False
    
    Set rngStart = Selection.Range
    With rngStart.Paragraphs(1).Range
        If rngStart.Start = .End - 1 And _
            rngStart.End = .End - 1 Then
            bSelectEOPara = True
        End If
    End With
    
    With WordBasic
    '   get showall
        ShowAllStart = .ShowAll()
    
    '   test for current underlining -
    '   remove if it exists
        .WW7_EditGoTo "\Para"
    
    '   trim trailing para from selection if necessary
        If Right(Selection, 1) = vbCr Then _
            .CharLeft 1, 1
    
    '   select last line
        .CharRight
        .StartOfLine 1
        
    '   check for one-line hanging indent (e.g. reline)
        With Selection
            If .Characters.Last <> vbTab And _
                    .ParagraphFormat.FirstLineIndent < 0 And _
                    iTabPosition > 0 And Left(.Text, 1) = vbTab Then
                bIsOneLineHanging = True
                .MoveStart wdCharacter, iTabPosition
            End If
        End With
        
    '   remove underlining, hard spaces,
    '   trailing tab and last tab marker'  if they exist - then exit
        IsUnderlined = Selection.Range.Underline
        If IsUnderlined Then
            bEditFindReset
            .EditReplace Find:="^s", Replace:="", ReplaceAll:=1
            .EndOfLine
            .CharLeft 1, 1
            If Selection = vbTab Then
                .WW6_EditClear
                CurPosInInches = (.SelInfo(7) / 1440)
                Selection.Move wdCharacter, -1
                
                While Right(Selection, 1) = vbTab
                    Selection.Delete
                    Selection.Move wdCharacter, -1
                Wend

'--             clear last tab the VBA way
                With Selection.ParagraphFormat
                    For i = .TabStops.Count To 1 Step -1
                        With .TabStops(i)
                            If .CustomTab And .Alignment = wdAlignTabRight Then
                                .Clear
                                Exit For
                            End If
                        End With
                    Next i
                    
'                   check for numbering - if trailing tab
'                   for number, skip first tab stop
                    iFirstTabStop = 1
                    With Selection.Range.ListFormat
                        If .ListType <> wdListNoNumbering Then
                            If .ListTemplate.ListLevels(.ListLevelNumber) _
                                    .TrailingCharacter = wdTrailingTab Then
                                
                                iFirstTabStop = 2
                            End If
                        End If
                    End With
                    
                    iNumTabStops = .TabStops.Count
                    On Error Resume Next
                    For i = iNumTabStops To iFirstTabStop Step -1
                        If (.TabStops(i).Position Mod 36 = 0) And _
                                .TabStops(i).Position <> .LeftIndent Then
                            .TabStops(i).Clear
                        End If
                    Next i
                    On Error GoTo 0
                End With
            End If
            Selection.Paragraphs(1).Range.Underline = wdUnderlineNone
        Else
            If bIsOneLineHanging Then
                Set rngLocation = Selection.Range
            Else
                Set rngLocation = Selection.Paragraphs(1).Range
                rngLocation.MoveEnd wdCharacter, -1
            End If
            bUnderlined = bUnderlineToLengthOfLongest(rngLocation)
        End If
    
    '   ensure insertion point is at end of paragraph
        .WW7_EditGoTo "\Para"
    
    '   trim trailing para from selection if necessary
        If Right(Selection, 1) = vbCr Then _
            .CharLeft 1, 1
        .CharRight
        .Underline 0
    
    '   set environment
        .ShowAll ShowAllStart
        bEditFindReset
        
'       redefine selection range if eo para
'       should be selected - see top
        If bSelectEOPara Then
            Set rngStart = rngStart.Paragraphs(1).Range
            If rngStart.Characters.Last = vbCr Then _
                rngStart.MoveEnd wdCharacter, -1
            rngStart.EndOf
        End If
        rngStart.Select
    End With
End Function

Function bInsertUnderlinedLine(rngRange As Word.Range) As Boolean
'inserts an underline to the end
'of the line
    Dim sTabStopPos As Single
    
    With rngRange
'       get position of tab stop
        With .PageSetup
            sTabStopPos = .PageWidth - _
            (.RightMargin + .LeftMargin + rngRange.ParagraphFormat.RightIndent) - 12
        End With
        
'       add tab stop at end of line
        With .ParagraphFormat.TabStops
            .ClearAll
            .Add sTabStopPos, wdAlignTabRight
        End With
        
'       add tab stop
        .Text = vbTab
        
'       underline
        .Font.Underline = wdUnderlineSingle
    End With
End Function


Function rngGetField(rngFieldCode As Word.Range) As Word.Range
'returns the range of the field
'whose code is rngFieldCode

    Dim lStartPos As Long
    Dim lEndPos As Long
    
    With rngFieldCode
        lStartPos = .Start
        lEndPos = .End
    End With
    
    rngFieldCode.SetRange lStartPos - 1, lEndPos + 1
    Set rngGetField = rngFieldCode
End Function



