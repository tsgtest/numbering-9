Attribute VB_Name = "mdlDocument"
Option Explicit

Function envGetDocEnvironment() As mpDocEnvironment
'fills type mpDocEnvironment with
'current doc environment settings -
'g_envMpDoc below is a global var

'   some properties are not available
'   in preview mode - this will cause
'   error - hence turn off trapping
    On Error Resume Next
    
    g_envMpDoc.iProtectionType = ActiveDocument.ProtectionType
    
'   get selection parameters
    With Selection
        g_envMpDoc.iSelectionStory = .StoryType
        g_envMpDoc.lSelectionStartPos = .Start
        g_envMpDoc.lSelectionEndPos = .End
    End With
        
    With ActiveDocument.ActiveWindow.ActivePane
        g_envMpDoc.sVScroll = .VerticalPercentScrolled
        g_envMpDoc.sHScroll = .HorizontalPercentScrolled
        With .View
            g_envMpDoc.bShowAll = .ShowAll
            g_envMpDoc.bFieldCodes = .ShowFieldCodes
            g_envMpDoc.bBookmarks = .ShowBookmarks
            g_envMpDoc.bTabs = .ShowTabs
            g_envMpDoc.bSpaces = .ShowSpaces
            g_envMpDoc.bParagraphs = .ShowParagraphs
            g_envMpDoc.bHyphens = .ShowHyphens
            g_envMpDoc.bHiddenText = .ShowHiddenText
            g_envMpDoc.bTextBoundaries = .ShowTextBoundaries
            g_envMpDoc.iView = .Type
        End With
    
    End With
    
    envGetDocEnvironment = g_envMpDoc
    
End Function

Function bSetDocEnvironment(envCur As mpDocEnvironment, _
                            Optional bScroll As Boolean = False, _
                            Optional bSelect As Boolean = False) As Boolean
'   sets the doc/window related vars
'   based on type mpDocEnvironment var (global)

    Dim rngStory As Word.Range
    
'   some properties are not available
'   in preview mode - this will cause
'   error - hence turn off trapping
    On Error Resume Next
    With ActiveWindow.ActivePane
        With .View
            .ShowAll = envCur.bShowAll
            .ShowFieldCodes = envCur.bFieldCodes
            .ShowBookmarks = envCur.bBookmarks
            .ShowTabs = envCur.bTabs
            .ShowSpaces = envCur.bSpaces
            .ShowParagraphs = envCur.bParagraphs
            .ShowHyphens = envCur.bHyphens
            .ShowHiddenText = envCur.bHiddenText
            .ShowTextBoundaries = envCur.bTextBoundaries
            .Type = envCur.iView
        End With
        
        If bSelect Then
            With envCur
                Set rngStory = ActiveDocument.StoryRanges(.iSelectionStory)
                rngStory.SetRange .lSelectionStartPos, .lSelectionEndPos
                rngStory.Select
'               this needs to happen again here
'               because selecting the headers/footers
'               automatically puts you in normal view.
                ActiveWindow.View.Type = .iView
            End With
        End If
        
        If bScroll Then
            .VerticalPercentScrolled = envCur.sVScroll
            .HorizontalPercentScrolled = envCur.sHScroll
        End If
        
    End With
    
    bSetDocEnvironment = True

End Function

Function FullParagraphSelected() As Boolean
'returns TRUE if selection is a single, entire paragraph
    With Selection
        If (.Type <> wdSelectionIP) And _
                (.Paragraphs.Count = 1) Then
            If .Text = .Paragraphs(1).Range.Text Then
                FullParagraphSelected = True
            End If
        End If
    End With
End Function
