VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CMnuTbr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CSchemeProps Collection Class
'   created 3/19/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   CMnuTbr object - Menus and toolbars - this class
'   was created from Numbering version 8 VBA code
'**********************************************************

Private Const mpNumberingToolbar As String = "Legal MacPac Numbering"
Private Const mpNumberingMenu As String = "Numbering"
Private Const mpGalleryItem As String = "Gallery..."

Public Sub DocumentChange()
    Dim ctlToolbar As CommandBarControl
    Dim xScheme As String
    Dim varScheme As Variable
    Dim ctlMenu As CommandBarControl
    
    Word.Application.ScreenUpdating = False
    
    On Error Resume Next
    xScheme = ActiveDocument.Variables("zzmpFixedCurScheme_9.0")

'   enable/disable numbering menu
    With Word.Application.CommandBars("Menu Bar")
        If Word.Application.Windows.Count Then
            If ActiveWindow.View <> wdPrintPreview Then
                bSetMenuCtlState True, xScheme
            End If
        Else
            bSetMenuCtlState False
        End If
    End With

'   enable/disable numbering toolbar controls
    With CommandBars(mpNumberingToolbar)
        If Word.Application.Windows.Count = 0 Then
            bSetNumberingTbrEnable False
        Else
            .Enabled = True
            
            If Len(xScheme) Then
'               trim origin prefix from scheme id
                xScheme = Mid(xScheme, 2)
            End If
            On Error GoTo evnApp_DocumentChange_Error
            
            bSetNumberingTbrEnable True, xScheme
'           force colors to print as black on non-color printers
            On Error Resume Next
            ActiveDocument.Compatibility(wdPrintColBlack) = True
        End If
    End With

    MarkStartupSaved
    Word.Application.ScreenUpdating = True
    
    Exit Sub
    
evnApp_DocumentChange_Error:
    Select Case Err
        Case Else
            RaiseError "CMnuTbr.DocumentChange"
    End Select
    Exit Sub
End Sub

Private Function bSetMenuCtlState(bEnable As Boolean, _
                                  Optional xScheme As String) As Boolean
'enable/disable all menu items based on bEnable-
'enable/disable Mark and Format item based on
'whether there is a macpac scheme in the doc
    Dim ctlMenu As CommandBarControl
    Dim tmpActive As Template
    
    On Error Resume Next
    
    With Word.Application.CommandBars( _
            "Menu Bar").Controls("Numbering")
        For Each ctlMenu In .Controls
            ctlMenu.Enabled = bEnable
        Next ctlMenu
    End With

    Set tmpActive = ActiveDocument.AttachedTemplate
    tmpActive.Saved = True
    
    MarkStartupSaved
End Function

Private Function bSetNumberingTbrEnable(bEnable As Boolean, _
                                Optional xScheme As String) As Boolean
'enable/disable all toolbar btns based on bEnable-
'enable/disable Mark and Format btn based on
'whether there is a macpac scheme in the doc
    Dim ctlP As CommandBarControl
    
    For Each ctlP In CommandBars(mpNumberingToolbar).Controls
        ctlP.Enabled = bEnable
    Next
    
'   set numbering insertion buttons
    bSetNumberingBtnsEnable xScheme, ActiveDocument

    MarkStartupSaved
End Function

Friend Function bSetNumberingBtnsEnable(xScheme As String, docDoc As Document) As Boolean
'enables/disables appropriate
'"Insert Number" toolbar btns

    Dim xStyle As String
    Dim i As Integer
    Dim styNumbering As Style
    Dim xStyleRoot As String
    
    On Error Resume Next
    
    xStyleRoot = xGetStyleRoot(xScheme)
        
    With Word.Application
        If xScheme = Empty Then
'           disable all insertion btns
            For i = 1 To 9
                .CommandBars(mpNumberingToolbar) _
                    .Controls(1 + i).Enabled = False
                 CommandBars("Menu Bar").Controls("Numbering").Controls("Insert Level") _
                    .Controls(i).Enabled = False
           Next i
        ElseIf xScheme = mpGalleryItem Then
'           enable all insertion buttons
            For i = 1 To 9
                .CommandBars(mpNumberingToolbar) _
                    .Controls(1 + i).Enabled = True
                CommandBars("Menu Bar").Controls("Numbering") _
                    .Controls("Insert Level") _
                    .Controls(i).Enabled = True
            Next i
        Else
'           enable all appropriate insertion buttons
            For i = 1 To 9
                xStyle = xStyleRoot & "_L" & i
                Set styNumbering = Nothing
                
                On Error Resume Next
                Set styNumbering = docDoc.Styles(xStyle)
                
                With .CommandBars(mpNumberingToolbar).Controls(1 + i)
                    If styNumbering Is Nothing Then
                        .Enabled = False
                        CommandBars("Menu Bar").Controls("Numbering") _
                            .Controls("Insert Level") _
                            .Controls(i).Enabled = False
                    Else
                        .Enabled = True
                        CommandBars("Menu Bar").Controls("Numbering") _
                            .Controls("Insert Level") _
                            .Controls(i).Enabled = True
                    End If
                End With
            Next i
        End If
        
'       mark startup templates as saved
        MarkStartupSaved
    End With
End Function




