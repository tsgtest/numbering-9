VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CDialog"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Function bEnsureSelectedContent(ctlP As Object, _
                                Optional bLastCharOnly As Boolean = False) As Boolean
    Dim ctlData As VB.Data
    On Error Resume Next
    With ctlP
        If TypeOf ctlP Is ListBox Then
            If .ListIndex < 0 Then _
                .ListIndex = 0
        Else
            If bLastCharOnly = False Then
                .SelStart = 0
                .SelLength = Len(.Text)
            Else
                .SelStart = Len(.Text)
                .SelLength = 1
            End If
        End If
    End With
End Function







