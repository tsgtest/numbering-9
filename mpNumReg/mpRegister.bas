Attribute VB_Name = "mpRegister"
Option Explicit

'Register vars
Private Const mpnBase As String = "\mpBase.dll"
Private Const mpnNum As String = "\mpN90.dll"
Private Const mpnTOC As String = "\mpTOC90.dll"
Private Const mpnCTOC As String = "\mpCTOC.dll"
Private Const mpWDXX As String = "\mpWDXX.dll"
Private Const mpSCR As String = "\mpScr.exe"

Type OSVERSIONINFO   '  148 Bytes
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
End Type

Declare Function GetVersionEx& Lib "kernel32" Alias "GetVersionExA" ( _
lpVersionInformation As OSVERSIONINFO)

Public Const VER_PLATFORM_WIN32_NT = 2
Public Const VER_PLATFORM_WIN32_WINDOWS = 1
Public Const VER_PLATFORM_WIN32s = 0

Public mpRegSvr As String
Private oFSO As New FileSystemObject

Sub Main()
    bRegister
End Sub

Function bRegister()
    Dim dl&
    Dim osinfo As OSVERSIONINFO
    Dim xFile As String
    Dim xAppPath As String
    Dim iPos As Integer
    Dim iStart As Integer
    Dim iEnd As Integer
    Dim i As Integer
    Dim xCommonPath As String
    Dim xCommand As String
    Dim xText As String
    
    xCommand = Command() '9.9.6006
    xAppPath = App.Path
    
'   get operating system
    osinfo.dwOSVersionInfoSize = Len(osinfo)
    dl& = GetVersionEx(osinfo)
    
    '9.9.6006 - prevent errors when regsrvr32 isn't on C drive - note that
    'silent switch also wasn't working prior to this version
    mpRegSvr = oFSO.GetSpecialFolder(SystemFolder) & "\regsvr32.exe"
    If Dir(mpRegSvr) = "" Then
        mpRegSvr = oFSO.GetSpecialFolder(WindowsFolder) & "\Regsvr32.exe"
        If Dir(mpRegSvr) = "" Then
            MsgBox "Unable to register MacPac components, Regsvr32.exe not found" & vbCr & _
                   "Please contact your system administrator", vbExclamation, App.Title
            Set oFSO = Nothing
            End
        End If
    End If
    mpRegSvr = mpRegSvr & " /s "
    
''*****(/s is 'silent' switch, ie no dialog)*******
'    If osinfo.dwPlatformId = VER_PLATFORM_WIN32_NT Then
'        If bFileExists("C:\Winnt\System32\Regsvr32.exe") Then   'Windows NT
'            mpRegSvr = "C:\Winnt\System32\regsvr32.exe /s "
'        Else    'Windows 2000
'            mpRegSvr = "C:\Windows\System32\regsvr32.exe /s "
'        End If
'    Else
'        mpRegSvr = "C:\Windows\System\regsvr32.exe /s "
'    End If
    
    On Error Resume Next
    
'   register files in app directory
    xFile = Dir(xAppPath & "\*")
    While xFile <> ""
        If (Right(UCase(xFile), 4) = ".OCX" Or _
                Right(UCase(xFile), 4) = ".DLL" Or _
                Right(UCase(xFile), 4) = ".EXE") And _
                (UCase(xFile) <> "MPWD80.DLL" And _
                UCase(xFile) <> "MPWD90.DLL") Then
            ReRegisterServer xAppPath & "\" & xFile
        End If
        xFile = Dir()
    Wend
    
'   if it exists, register files in MacPac\Common directory
    iPos = lCountChrs(xAppPath, "\")
    If iPos > 2 Then
        For i = 1 To iPos - 2
            iStart = InStr(iStart + 1, xAppPath, "\")
        Next i
        iEnd = InStr(iStart + 1, xAppPath, "\")
        xCommonPath = UCase(Mid(xAppPath, iStart + 1, iEnd - iStart - 1))
        If (InStr(xCommonPath, "MACPAC") <> 0) And _
                (InStr(xCommonPath, "NUMBERING") = 0) Then
            xCommonPath = Left(xAppPath, iEnd) & "Common"
            If Dir(xCommonPath, vbDirectory) <> "" Then
                xFile = Dir(xCommonPath & "\*")
                While xFile <> ""
                    If (Right(UCase(xFile), 4) = ".OCX" Or _
                            Right(UCase(xFile), 4) = ".DLL" Or _
                            Right(UCase(xFile), 4) = ".EXE") Then
                        ReRegisterServer xCommonPath & "\" & xFile
                    End If
                    xFile = Dir()
                Wend
            End If
        End If
    End If
    
    '9.9.6006 - handle switches
    If InStr(UCase(xCommand), "/S") = 0 Then
        If InStr(UCase(xCommand), "/U") = 0 Then
            xText = "registered"
        Else
            xText = "unregistered"
        End If
        MsgBox "All of the MacPac Numbering components were " & xText & ".", vbInformation, _
            "MacPac Numbering"
    End If
bRegister_ERROR:

End Function

Private Function bFileExists(xPath As String) As Boolean
    On Error GoTo Function_ERROR
    If Dir(xPath) <> "" Then
        bFileExists = True
    End If
    Exit Function
Function_ERROR:
End Function

Private Sub ReRegisterServer(xFile As String)
    Dim bUnregisterOnly As Boolean
    
    On Error Resume Next
    
    '9.9.6006
    bUnregisterOnly = (InStr(UCase(Command()), "/U") > 0)
    
    If Right(UCase(xFile), 4) = ".EXE" Then
'       unregister
        Shell xFile & " /unregserver"
        
'       register
        If Not bUnregisterOnly Then '9.9.6006
            Shell xFile & " /regserver"
        End If
    Else
'       unregister
        Shell mpRegSvr & "/u " & Chr(34) & xFile & Chr(34)
        
'       register
        If Not bUnregisterOnly Then '9.9.6006
            Shell mpRegSvr & Chr(34) & xFile & Chr(34)
        End If
    End If
End Sub

Private Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Integer
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
    lCountChrs = l
End Function

