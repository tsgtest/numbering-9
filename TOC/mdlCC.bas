Attribute VB_Name = "mdlCC"
Option Explicit

Public Function BeforeBlockLevelCC() As Boolean
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    BeforeBlockLevelCC = oCC.BeforeBlockLevelCC
End Function

Public Function AfterBlockLevelCC() As Boolean
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    AfterBlockLevelCC = oCC.AfterBlockLevelCC
End Function

Public Function GetCCSafeParagraphStart(ByVal oInsertionRange As Word.Range) As Long
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    GetCCSafeParagraphStart = oCC.GetCCSafeParagraphStart(oInsertionRange)
End Function

Public Function GetCCSafeParagraphEnd(ByVal oInsertionRange As Word.Range) As Long
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    GetCCSafeParagraphEnd = oCC.GetCCSafeParagraphEnd(oInsertionRange)
End Function

Public Function CountCCsEndingInRange(ByVal oRange As Word.Range) As Integer
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    CountCCsEndingInRange = oCC.CountCCsEndingInRange(oRange)
End Function

Public Function GetPositionAfterInlineCC(ByVal oRange As Word.Range) As Long
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    GetPositionAfterInlineCC = oCC.GetPositionAfterInlineCC(oRange)
End Function


