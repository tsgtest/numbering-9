VERSION 5.00
Begin VB.Form frmRenameScheme 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Rename Numbering Scheme"
   ClientHeight    =   1920
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3660
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1920
   ScaleWidth      =   3660
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtNewDisplayName 
      Height          =   315
      Left            =   90
      MaxLength       =   10
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   960
      Width           =   3450
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   385
      Left            =   2445
      TabIndex        =   3
      Top             =   1440
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "OK"
      Height          =   385
      Left            =   1290
      TabIndex        =   2
      Top             =   1440
      Width           =   1100
   End
   Begin VB.TextBox txtNewName 
      Height          =   315
      Left            =   105
      MaxLength       =   10
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   345
      Width           =   3450
   End
   Begin VB.Label Label1 
      Caption         =   "&New Display Name:"
      Height          =   285
      Left            =   105
      TabIndex        =   5
      Top             =   750
      Width           =   1920
   End
   Begin VB.Label lblName 
      Caption         =   "&New Name:"
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1920
   End
End
Attribute VB_Name = "frmRenameScheme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
End Sub

Private Sub btnOK_Click()
    If bNameIsValid() Then
        Me.Hide
    End If
End Sub

Private Function bNameIsValid() As Boolean
'criteria are not sufficient yet
    bNameIsValid = True
'    bNameIsValid = (Me.txtNewName <> "") And _
'                                  (InStr(Me.txtName, " ") = 0) And _
'                                  (Me.txtNewDisplayName <> "")
End Function

Private Sub txtNewName_Change()
    Me.txtNewDisplayName = Me.txtNewName
End Sub
