Attribute VB_Name = "mdlNumLock"
Option Explicit

' Declare Type for API call:
Private Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128   '  Maintenance string for PSS usage
End Type

' API declarations:
Private Declare Function GetVersionEx Lib "kernel32" _
    Alias "GetVersionExA" _
    (lpVersionInformation As OSVERSIONINFO) As Long

Private Declare Sub keybd_event Lib "user32" _
    (ByVal bVk As Byte, _
    ByVal bScan As Byte, _
    ByVal dwFlags As Long, ByVal dwExtraInfo As Long)

Private Declare Function GetKeyboardState Lib "user32" _
    (pbKeyState As Byte) As Long

Private Declare Function SetKeyboardState Lib "user32" _
    (lppbKeyState As Byte) As Long

' Constant declarations:
Const VK_NUMLOCK = &H90
Const VK_SCROLL = &H91
Const VK_CAPITAL = &H14
Const KEYEVENTF_EXTENDEDKEY = &H1
Const KEYEVENTF_KEYUP = &H2
Const VER_PLATFORM_WIN32_NT = 2
Const VER_PLATFORM_WIN32_WINDOWS = 1

Function bGetNumLockState() As Boolean
    Dim keys(0 To 255) As Byte
    GetKeyboardState keys(0)
    bGetNumLockState = keys(VK_NUMLOCK)
End Function

Function lSetNumLockState(bCurrent As Boolean, bSet As Boolean) As Long
    Dim o As OSVERSIONINFO
    Dim keys(0 To 255) As Byte
    Dim iState As Integer
    
    o.dwOSVersionInfoSize = Len(o)
    GetVersionEx o
    
    If bSet = True And bCurrent = False Then
'       turn on
        iState = 1
    ElseIf bSet = False And bCurrent = True Then
'       turn off
        iState = 0
    Else
        Exit Function
    End If
    
'   different methods for different operating systems
    If o.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS Then  '=== Win95/98
        keys(VK_NUMLOCK) = iState
        SetKeyboardState keys(0)
    ElseIf o.dwPlatformId = VER_PLATFORM_WIN32_NT Then   '=== WinNT
'       Simulate Key Press
        keybd_event VK_NUMLOCK, &H45, KEYEVENTF_EXTENDEDKEY Or 0, 0
'       Simulate Key Release
        keybd_event VK_NUMLOCK, &H45, KEYEVENTF_EXTENDEDKEY _
            Or KEYEVENTF_KEYUP, 0
    End If
    
End Function

