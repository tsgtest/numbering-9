Attribute VB_Name = "mdlN90"
Option Explicit

Public Enum mpDPTableActions
    mpDPTableAction_Add = 0
    mpDPTableAction_Remove = 1
    mpDPTableAction_Edit = 2
End Enum

Public Const mpModeSchemeDone = 0
Public Const mpModeSchemeMain = 1
Public Const mpModeSchemeNew = 2
Public Const mpModeSchemeNewB = 3
Public Const mpModeSchemeEdit = 4
Public Const mpModeSchemeEditB = 5

Private bTemplateSaved As Boolean
Private m_bContStyles(8) As Boolean '9.9.6001
Private m_xQATStyle As String

Function EditSchemeProperties(ByVal xScheme As String, _
                              ByVal iSchemeType As mpSchemeTypes, _
                              ByVal bReadOnly As Boolean)
'allows user to edit the properties of a scheme -
'e.g. Name, dynamic fonts, default TOC scheme
'10/2/12 - added bReadOnly parameter for favorites schemes
    Dim tplSource As Word.Template
    Dim oScheme As CNumScheme
    Dim xOrigAlias As String
    Dim oForm As VB.Form
    
    If g_lUILanguage = wdFrenchCanadian Then
        Set oForm = New frmSchemePropertiesFrench
    Else
        Set oForm = New frmSchemeProperties
    End If
    
    With oForm
'       get current scheme properties
        Set oScheme = GetRecord(xScheme, iSchemeType)
        .Scheme = oScheme
        .ReadOnly = bReadOnly
        
'       store alias for future comparison
        xOrigAlias = oScheme.Alias
        
        .Show vbModal
    
'       do only if dlg was not cancelled
        If Not .Cancelled Then
'           set scheme record with dlg info
            oScheme.DynamicFonts = .chkBaseOnNormal
            oScheme.DynamicSpacing = .chkAdjustSpacing
'            oScheme.TOCScheme = .cmbTOCScheme.ListIndex + 1
            oScheme.TOCScheme = iGetTOCSchemeIndex(.cmbTOCScheme.Text)
            oScheme.Description = .txtDescription
        
'           modify scheme record
            SetRecord oScheme, iSchemeType
            
            'GLOG 5686 (9.9.6014)
            If oScheme.SchemeType = mpSchemeType_Private Then _
                g_oPNumSty.Saved = False
            
'           refresh list if alias has changed
            If .txtSchemeName <> xOrigAlias Then
                lRet = lRenameScheme(oScheme.Name, _
                                     .txtSchemeName, _
                                     xOrigAlias)
                If lRet Then
                    Err.Raise lRet
                End If
                g_oCurScheme.Alias = .txtSchemeName
            End If
            
            'update global scheme with new description
            g_oCurScheme.Description = oScheme.Description
            
'           save source if is scheme is personal scheme
            'remmed for GLOG 5539 (9.9.6006) - we need to hide the form
            'during the save to avoid error with FileSite
'            If oScheme.SchemeType = mpSchemeType_Private Then
'                If g_iWordVersion >= mpWordVersion_2010 Then
'                    'workaround for Word 2010 to avoid error 5986 -
'                    '"this command is not available in an unsaved document"
'                    g_oPNumSty.Saved = False 'GLOG 5123 (10/3/12)
'                    g_oPNumSty.OpenAsDocument.Save
'                    ActiveDocument.Close False
'                Else
'                    g_oPNumSty.Save
'                End If
'            End If
                
        End If
    End With
    Unload oForm
End Function

Function xGetListNumberWithFormat(xScheme As String, _
                                    iLevel As Integer) _
                                    As String
    Dim xNumberFormat As String
    Dim i As Integer
    Dim iLoc As Integer
    Dim xEntireNumber As String
    Dim xNumber As String
    
    xNumberFormat = ActiveDocument.ListTemplates(xGetFullLTName(xScheme)) _
        .ListLevels(iLevel).NumberFormat
    xNumber = xGetListNumber(xScheme, iLevel)
    iLoc = InStr(xNumberFormat, "%" & iLevel)
    xEntireNumber = Left(xNumberFormat, iLoc - 1) & xNumber & _
        Right(xNumberFormat, Len(xNumberFormat) - iLoc - 1)
        
    For i = 1 To iLevel - 1
        xNumber = xGetListNumber(xScheme, i)
        iLoc = InStr(xEntireNumber, "%" & i)
        If iLoc Then
            xEntireNumber = Left(xEntireNumber, iLoc - 1) & xNumber & _
                Right(xEntireNumber, Len(xEntireNumber) - iLoc - 1)
        End If
    Next i
    
    xGetListNumberWithFormat = xEntireNumber
End Function
Function xGetListNumber(xScheme As String, iLevel As Integer, Optional bLegal As Boolean = False) As String
    Dim iNumberStyle As Integer
    Dim iStartAt As Integer
    Dim xLT As String
    
    xLT = xGetFullLTName(xScheme)
    If iLevel Then
        With ActiveDocument.ListTemplates(xLT).ListLevels(iLevel)
            iNumberStyle = .NumberStyle
            iStartAt = .StartAt
        End With
    
' THIS OPTION NOT READY FOR PRIME TIME
'        If bLegal And iNumberStyle <> wdListNumberStyleLegalLZ _
'            Then iNumberStyle = wdListNumberStyleLegal
        xGetListNumber = xIntToListNumStyle(iStartAt, iNumberStyle)
    End If
End Function

Function iListNumStyleToInt(iNumberStyle As Integer, _
                                xNumber As String) As Integer
    Dim iNum As Integer

    Select Case iNumberStyle
        Case wdListNumberStyleUppercaseRoman, _
                wdListNumberStyleLowercaseRoman
            If Left(xNumber, 2) = "xl" Then
                iNum = 40
                xNumber = Right(xNumber, Len(xNumber) - 2)
            End If
            While Left(xNumber, 1) = "x"
                iNum = iNum + 10
                xNumber = Right(xNumber, Len(xNumber) - 1)
            Wend
            Select Case xNumber
                Case ""
                Case "i"
                    iNum = iNum + 1
                Case "ii"
                    iNum = iNum + 2
                Case "iii"
                    iNum = iNum + 3
                Case "iv"
                    iNum = iNum + 4
                Case "v"
                    iNum = iNum + 5
                Case "vi"
                    iNum = iNum + 6
                Case "vii"
                    iNum = iNum + 7
                Case "viii"
                    iNum = iNum + 8
                Case "ix"
                    iNum = iNum + 9
                Case Else   'number is greater than 49
                    iNum = 1
            End Select
        Case wdListNumberStyleUppercaseLetter
            iNum = Asc(Left(xNumber, 1)) + _
                ((Len(xNumber) - 1) * 26) - 64
        Case wdListNumberStyleLowercaseLetter
            iNum = Asc(Left(xNumber, 1)) + _
                ((Len(xNumber) - 1) * 26) - 96
        Case wdListNumberStyleArabicLZ, _
                wdListNumberStyleLegalLZ
            iNum = Val(Right(xNumber, Len(xNumber) - 1))
        Case Else
            iNum = Val(xNumber)
    End Select
    
    iListNumStyleToInt = iNum
End Function

Function xGetNumFormat(ByVal xCode As String, _
                       ByVal xDisplay As String, _
                       bNonbreakingSpaces As Boolean) As String
'gets list level NumberFormat property from
'string in rich text box and corresponding code
    Dim xNumFormat As String
    Dim xNextChar As String
    
'   do if not a bullet
    If xCode <> ChrW(&HB7) Then
        While Len(xCode) And (xCode <> ChrW(&HB7))
            xNextChar = Left(xCode, 1)
            If xNextChar = mpTextCode Then
                xNumFormat = xNumFormat & Left(xDisplay, 1)
                xCode = Right(xCode, Len(xCode) - 1)
                xDisplay = Right(xDisplay, Len(xDisplay) - 1)
            Else
                xNumFormat = xNumFormat & "%" & xNextChar
                While Len(xCode) And _
                        (Left(xCode, 1) = xNextChar)
                    xCode = Right(xCode, Len(xCode) - 1)
                    xDisplay = Right(xDisplay, Len(xDisplay) - 1)
                Wend
            End If
        Wend
    Else
        xNumFormat = xCode
    End If
    
'   if specified, make spaces non-breaking
    If bNonbreakingSpaces Then
        xNumFormat = xSubstitute(xNumFormat, Chr(32), ChrW(&HA0))
    Else
        xNumFormat = xSubstitute(xNumFormat, ChrW(&HA0), Chr(32))
    End If
    
    xGetNumFormat = xNumFormat
End Function

Function xGetDocVarCell(xTable As String, _
                        iRow As Integer, _
                        iColumn As Integer, _
                        Optional xSep As String = "|") As String
    Dim i As Integer
    Dim iPrevious As Integer
    Dim iNext As Integer
    Dim xValue As String
    
    On Error Resume Next
    
    xValue = ActiveDocument.Variables(xTable & iRow)
    
    For i = 1 To iColumn
        iPrevious = InStr(iPrevious + 1, xValue, xSep)
    Next i
    iNext = InStr(iPrevious + 1, xValue, xSep)
    
    xGetDocVarCell = Mid(xValue, iPrevious + 1, _
        iNext - iPrevious - 1)
        
End Function
                        
Function lSetDocVarCell(xTable As String, _
                        iRow As Integer, _
                        iColumn As Integer, _
                        xValue As String, _
                        Optional xSep As String = "|") As Long
    Dim i As Integer
    Dim iPrevious As Integer
    Dim iNext As Integer
    Dim xOldValue As String
    
    On Error Resume Next
    
    xOldValue = ActiveDocument.Variables(xTable & iRow)
    
    For i = 1 To iColumn
        iPrevious = InStr(iPrevious + 1, xOldValue, xSep)
    Next i
    iNext = InStr(iPrevious + 1, xOldValue, xSep)
    
    If iPrevious = 0 Or iNext = 0 Then _
        Exit Function
        
    ActiveDocument.Variables(xTable & iRow) = _
        Left(xOldValue, iPrevious) & xValue & _
        Right(xOldValue, Len(xOldValue) - iNext + 1)
        
End Function

Function lDocVarsToArray(xTable As String, _
                            xArray() As String, _
                            Optional xSep As String = "|") As Long
'fills xArray with items in doc var "table"
'returns number of rows, i.e. variables
    Dim i As Integer
    Dim j As Integer
    Dim iPos As Integer
    Dim iNumRows As Integer
    Dim iNumColumns As Integer
    Dim xValue As String
    
    With ActiveDocument.Variables
'       get number of rows
        For i = 1 To 1000
            On Error GoTo EOTReached
            xValue = .Item(xTable & i).Value
        Next
EOTReached:
        iNumRows = i - 1
        If iNumRows = 0 Then Exit Function
        
'       get number of columns
        iNumColumns = lCountChrs(.Item(xTable & "1").Value, _
            xSep) - 1
        If iNumColumns = 0 Then Exit Function
        
'       fill array
        ReDim xArray(iNumRows - 1, iNumColumns - 1)
        For i = 0 To iNumRows - 1
            xValue = .Item(xTable & (i + 1)).Value
            xValue = Right(xValue, Len(xValue) - 1)
            For j = 0 To iNumColumns - 1
                iPos = InStr(xValue, xSep)
                If iPos = 0 Then Exit For
                xArray(i, j) = Left(xValue, iPos - 1)
                xValue = Right(xValue, Len(xValue) - iPos)
            Next j
        Next i
    
    End With
        
    lDocVarsToArray = iNumRows
End Function

Function bEditScheme(xScheme As String, _
                      Optional iSchemeType As mpSchemeTypes, _
                      Optional Cancelled As Boolean = False) As Boolean
'modifies scheme - returns TRUE if scheme was modified
    Dim oPreview As CPreview
    Dim xAlias As String
    Dim i As Integer
    Dim iLevels As Integer
    Dim xPreview As String

    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If

'   keep DocumentChange code from running
'    g_bPreventDocChangeEvent = True

'   hide schemes dlg
'    frmSchemes.Visible = False
    Application.ScreenRefresh
    Application.ScreenUpdating = False
    Set g_docStart = Word.ActiveDocument

'   get whether attached template is dirty - used below
    bTemplateSaved = Word.ActiveDocument _
                    .AttachedTemplate.Saved

    'determine whether this is mp10 design mode
    g_bIsMP10DesignMode = (InStr(ActiveWindow.Caption, " - [Document Design]") > 0)
    
    'force a save if necessary
    If (iSchemeType = mpSchemeType_Document) And _
            (WordBasic.FileNameFromWindow$() = "") And _
            ((g_bGenerateBufferFromCopy And Not g_bIsMP10DesignMode) Or _
            g_bOrganizerSavePrompt) Then
        If g_lUILanguage = wdFrenchCanadian Then
            If g_bGenerateBufferFromCopy And Not g_bIsMP10DesignMode Then
                If (g_iDMS = mpDMSIManage5x) Or (g_iDMS = mpDMSNetDocs) Then
                    MsgBox "Veuillez enregistrer le document avant d'utiliser cette fonction.", _
                        vbInformation, g_xAppName
                Else
                    lRet = MsgBox("MacPac ne peut pas editer le th�me de num�rotation dans un document qui n'a jamais �t�  enregistr� D�sirez-vous enregistrer le document ?", vbYesNo + vbQuestion, g_xAppName)
                End If
            Else
                MsgBox mpOrganizerSavePromptFrench, vbInformation, g_xAppName
            End If
        Else
            If g_bGenerateBufferFromCopy And Not g_bIsMP10DesignMode Then
                If (g_iDMS = mpDMSIManage5x) Or (g_iDMS = mpDMSNetDocs) Then
                    MsgBox "Please save the document before running this function.", _
                        vbInformation, g_xAppName
                Else
                    lRet = MsgBox("MacPac cannot edit numbering schemes in " & _
                        "documents that have" & vbCr & "never been saved.  Would you like " & _
                        "to save this document now?", vbYesNo + vbQuestion, g_xAppName)
                End If
            Else
                MsgBox mpOrganizerSavePrompt, vbInformation, g_xAppName
            End If
        End If
        
        If lRet = vbYes Then
            Application.ScreenRefresh
    
'           mark dirty to ensure that we can later
'           check to see if doc was actually saved
            Word.ActiveDocument.Saved = False

            On Error Resume Next
'           we run the FileSave macro, as opposed
'           to activedocument.save to ensure that
'           any doc management, trailer prompting occurs.
            Word.Application.Run "FileSave"
            
            If Not Word.ActiveDocument.Saved Then
                Err.Clear
                Cancelled = True
                Exit Function
            End If
        
            #If compHandleErrors Then
                On Error GoTo ProcError
            #End If
        Else
            Cancelled = True
            Exit Function
        End If

'        g_xCurDest = WordBasic.filename$()
'        WordBasic.DisableAutoMacros 1
'        Set g_docPreview = Documents.Add(g_xCurDest)
'        WordBasic.DisableAutoMacros 0
    End If
    
'   Word XP only - ensure that number and text positions match style indents
'   for each level - we'll need to later relink in iCopySchemeFromActiveDoc and
'   bCreateLevel will only synchronize if list level has actually been edited
    If g_bIsXP And (iSchemeType = mpSchemeType_Document) Then
        lSynchIndents xScheme
    End If

'   create buffer doc
    If iSchemeType = mpSchemeType_Private Then
        g_xCurDest = g_xPNumSty
    ElseIf iSchemeType = mpSchemeType_Public Then
        g_xCurDest = g_xFNumSty
    Else
        g_xCurDest = WordBasic.FileName$()
    End If

'   create new doc - prevent auto macros from running
    WordBasic.DisableAutoMacros 1

'   9.9.6001 - track which cont styles are already in document
    If iSchemeType = mpSchemeType_Document Then _
        StoreContStyles xScheme
        
    If (iSchemeType = mpSchemeType_Document) And _
            ((g_bGenerateBufferFromCopy = False) Or g_bIsMP10DesignMode) Then
        '9.9.1003 - base buffer doc on Normal instead of on active doc
        If g_bIsXP Then
            'this new method of generating the buffer doc will
            'disable the mouse if ShowWindowsInTaskbar=false (GLOG 4774)
            g_bForceWindowsInTaskbar = Not ActiveDocument.Parent.ShowWindowsInTaskbar
            If g_bForceWindowsInTaskbar Then _
                ActiveDocument.Parent.ShowWindowsInTaskbar = True
        End If
    
        iLevels = iGetLevels(xScheme, mpSchemeType_Document)
        Application.ScreenUpdating = False
        If g_xBufferTemplate = "" Then
            Set g_docPreview = Documents.Add()
        Else
            '9.9.3 - a template other than Normal has been specified in the ini
            On Error Resume Next
            Set g_docPreview = Documents.Add(g_xBufferTemplate)
            If Err > 0 Then
                If g_lUILanguage = wdFrenchCanadian Then
                    MsgBox "Le mod�le precise dans mpn90.ini (" & g_xBufferTemplate & _
                        ") est invalide.  Veuillez contacter votre administrateur syst�me.", vbCritical, g_xAppName
                Else
                    MsgBox "The template specified in mpn90.ini (" & g_xBufferTemplate & _
                        ") is invalid.  Please contact your system administrator.", vbCritical, g_xAppName
                End If
                Err.Clear
                WordBasic.DisableAutoMacros 0
                Exit Function
            End If
            On Error GoTo ProcError
        End If
        g_docStart.Activate
        
        'message if doc contains fixed digit numbering and buffer doesn't support it
        If g_iWordVersion >= mpWordVersion_2010 Then
            If mdlWord14.GetCompatibilityMode(g_docPreview) < 14 Then
                If ContainsWord2010NumberStyle(xScheme, mpSchemeType_Document, _
                        g_docStart) Then
                    With g_docPreview
                        .Saved = True
                        .Close
                    End With
                    RemoveBufferFromList
                    g_docStart.Activate
                    Application.ScreenUpdating = True
                    Application.ScreenRefresh
                    If g_lUILanguage = wdFrenchCanadian Then
                        MsgBox "Le th�me sp�cifi� ne peut �tre modifi� car il contient un style de num�rotation qui est disponible uniquement dans Word 2010 et le mod�le �dition n'est pas disponible en mode de compatibilit� dans Word 2010.", vbInformation, g_xAppName
                    Else
                        MsgBox "The specified scheme cannot be edited because it contains a " & _
                            "numbering style that is only available in Word 2010 and the Edit " & _
                            "template is not in Word 2010 compatibility mode.", vbInformation, g_xAppName
                    End If
                    WordBasic.DisableAutoMacros 0
                    Exit Function
                End If
            End If
        End If
        
        xPreview = g_xUserPath & "\Refresh.mpf"
        g_docPreview.SaveAs xPreview, , , , False
        lRet = iCopySchemeFromActiveDoc(xScheme, xPreview, iLevels, _
            iLevels, , , g_bCurSchemeIsHeading, , , True)
    Else
    '   next line will err if active doc has no extension
        On Error Resume Next
        Set g_docPreview = Documents.Add(g_xCurDest)
        If Err = 5151 Then
            Set g_docPreview = Documents.Add(Chr(34) & g_xCurDest & Chr(34))
        End If
        On Error GoTo ProcError
    End If
    
    If g_docPreview.ProtectionType <> wdNoProtection Then
        g_docPreview.Unprotect
    End If
    WordBasic.DisableAutoMacros 0
    
'remmed in 9.9.6009 - was putting focus in document instead of preview -
'could limit to private scheme edit, but I'm not sure that status is ever
'necessary on edit other than as part of new scheme
'    If UBound(g_xPSchemes()) > 15 Then
'        xAlias = GetField(xScheme, mpRecField_Alias, iSchemeType)
'        Set g_oStatus = New CStatus
'
'        With g_oStatus
'            .ProgressBarVisible = False 'GLOG 5590
'            If g_lUILanguage = wdFrenchCanadian Then
'                .Title = "Editer th�me " & xAlias
'            Else
'                .Title = "Editing " & xAlias & " scheme"
'            End If
'        End With
'    End If

    SetPreviewWindow
'    Set m_oTimer = New CTimer
'    m_oTimer.Start 50, AddressOf bEditSchemeB
    
    bEditScheme = True
    Exit Function
ProcError:
    RaiseError "numFunctions.bEditScheme"
    Exit Function
End Function

Function SetPreviewWindow()
    With g_docPreview.ActiveWindow
        If g_lUILanguage = wdFrenchCanadian Then
            .Caption = "Aper�u Th�me"
        Else
            .Caption = "Scheme Preview"
        End If
        On Error Resume Next
        g_docPreview.TrackRevisions = False
        g_docPreview.Content.Delete
        .WindowState = wdWindowStateMaximize
        .DisplayVerticalScrollBar = True
        g_docPreview.Activate
        
        '9.9.6006 (8/27/15) - in Word 2013, we need to explicitly
        'force initial zoom to 100%
        If g_iWordVersion >= mpWordVersion_2013 Then
            Word.ActiveWindow.View.Zoom.Percentage = 100
        End If
        
        Application.ScreenUpdating = True
        Application.ScreenRefresh
    End With
End Function

Function bEditSchemeB(ByVal lRulerDisplayDelay As Long, ByVal bResetQAT As Boolean) As Boolean
'second part of ModifyScheme - if running
'Word 2000, this function is called by the
'windows timer. this is the only way to
'show the ruler in the preview window, which
'only appears in a new document after all
'code that runs when the doc is created
'is finished executing
    Dim oPreview As CPreview
    Dim xAlias As String
    Dim xScheme As String
    Dim iSchemeType As mpSchemeTypes
    Dim bCopyHeadings As Boolean
    Dim bCopyStyleAttributesOnly As Boolean
    Dim frmEdit As VB.Form
    Dim i As Integer
    Dim j As Integer
    Dim bSchemeIsDirty As Boolean
    Dim oReg As CRegistry
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If

'   stop timer
'    Set m_oTimer = Nothing

'   get detail of edited scheme
    With g_oCurScheme
        xAlias = .Alias
        xScheme = .Name
        iSchemeType = .SchemeType
    End With
    
    'GLOG 5292 - open QAT for read access - this will prevent VBA macros from
    'getting removed by this function
    Dim oQAT As Scripting.File
    Dim oTS As Scripting.TextStream
    If (g_iWordVersion = mpWordVersion_2010) And (iSchemeType = mpSchemeType_Private) Then
        Set oQAT = GetQATFile()
        If Not oQAT Is Nothing Then
            Set oTS = oQAT.OpenAsTextStream(ForReading)
        End If
    End If

    If g_lUILanguage = wdFrenchCanadian Then
        Set frmEdit = New frmEditSchemeFrench
    Else
        Set frmEdit = New frmEditScheme
    End If
    
'   show form
ShowForm:
    With frmEdit
        .RulerDisplayDelay = lRulerDisplayDelay '9.9.6012
        .Show vbModal

'       store zoom state for next time
        bSetUserIni "Numbering", "Zoom", .chkZoom
        
        If .ZoomClicked Then
            'GLOG 5549 - set zoom with dialog hidden as workaround for
            'ruler not reappearing in Word 2013 with dialog showing
            Application.ScreenUpdating = False
            .ZoomClicked = False
            SetPreviewZoom (.chkZoom = vbChecked)
            Application.ScreenUpdating = True
            Application.ScreenRefresh
            GoTo ShowForm
        ElseIf Not .Cancelled Then
            SetActiveWindow FindWindow("OpusApp", 0&)
            
'           create preview only for private schemes
            If iSchemeType = mpSchemeType_Private Then
                Set oPreview = New CPreview
                oPreview.CreateBitmap xScheme, xAlias
                Set oPreview = Nothing
            End If
        End If

'       list template font sizes have been changed for preview/bitmap
        ResetListNumberFonts
        
'       copy scheme from buffer
        If Not .Cancelled Then
            bCopyHeadings = ((iSchemeType = mpSchemeType_Document) And _
                g_bCurSchemeIsHeading)
            bCopyStyleAttributesOnly = ((iSchemeType = mpSchemeType_Document) And _
                ((g_bGenerateBufferFromCopy = False) Or g_bIsMP10DesignMode))
            lRet = iCopySchemeFromActiveDoc(xScheme, _
                                            g_xCurDest, _
                                            .InitialLevels, _
                                            .CurrentLevels, _
                                            .xarDirty, _
                                            iSchemeType = mpSchemeType_Private, _
                                            bCopyHeadings, , _
                                            bCopyStyleAttributesOnly)
                                           
'GLOG 5462 - determine whether changes were made in the dialog
            'to avoid unnecessary processing
            'GLOG 5607 - if scheme was using Word heading styles,
            'the styles need to relinked in any case
            If Not g_bCurSchemeIsHeading Then
                For i = 1 To .CurrentLevels
                    For j = 1 To 6
                        bSchemeIsDirty = (.xarDirty(i, j) = "True")
                        If bSchemeIsDirty Then Exit For 'GLOG 5607
                    Next j
                    If bSchemeIsDirty Then Exit For
                Next i
            Else
                bSchemeIsDirty = True
            End If
'        Else
''           delete contents of preview file - removed 9.8.1005
'            ActiveDocument.Content.Delete
        End If
        
'       save edits
        SavePNumSty
    End With

'9.9.6012 - remmed out, as screen updating was already on here
'and needs to turned be off below in Word 2013 higher
''   close buffer doc - turn on screen updating only if EditScheme
''   was launched via the Schemes dialog
'    If g_iSchemeEditMode = mpModeSchemeEditB Then _
'        Application.ScreenUpdating = True

    'GLOG 5643 - restore the QAT setting if necessary
    If bResetQAT Then
        Set oReg = New CRegistry
        If InStr(Application.Version, "16.") <> 0 Then
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\16.0\Common\Toolbars\Word", _
                "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        ElseIf g_iWordVersion = mpWordVersion_2013 Then
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\15.0\Common\Toolbars\Word", _
                "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        ElseIf g_iWordVersion = mpWordVersion_2010 Then
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\14.0\Common\Toolbars\Word", _
                "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        Else
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\12.0\Common\Toolbars\Word", _
                "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        End If
    End If
    
'   iManage COM workaround
'    g_docPreview.Close wdDoNotSaveChanges
    With g_docPreview
        'GLOG 5624: Zoom percentage is saved separately for Normal and Print views
        If g_iWordVersion >= mpWordVersion_2013 Then
            Application.ScreenUpdating = False
            .ActiveWindow.View.Zoom.Percentage = g_lDefaultZoomPercentage
            .ActiveWindow.View.Type = wdPageView
            .ActiveWindow.View.Zoom.Percentage = g_lDefaultZoomPercentage
            .ActiveWindow.View.Type = wdNormalView
            Application.ScreenUpdating = True
        End If

        .Saved = True
        .Close
    End With
    
    RemoveBufferFromList
    
    If bSchemeIsDirty Then 'GLOG 5462
'       if scheme was doc scheme, back up changes to doc var
        If iSchemeType = mpSchemeType_Document Then
'           backup scheme properties in case the user
'           writes over them using the Word UI
            BackupProps xScheme
            
'           set toolbar
            If xActiveScheme(ActiveDocument) = xScheme Then
                bSetNumberingBtnsEnable xScheme
            End If
        ElseIf (iSchemeType = mpSchemeType_Private) And _
                bSchemeExists(xScheme, g_oCurScheme.Alias, mpSchemeType_Document) And _
                Not (g_bOrganizerSavePrompt And (WordBasic.FileNameFromWindow$() = "")) Then
'           edited scheme is a private scheme,
'           and document scheme of same name exists.
'           prompt to update doc scheme
'           with changes made to private scheme
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "D�sirez-vous appliquer ces changements au th�me du document '" & _
                       g_oCurScheme.Alias & "' ?" & vbCr & _
                       "Si vous s�lectionnez 'Non' maintenant, vous pourrez mettre � jour le th�me � un autre moment en cliquant sur le bouton R�tablir."
            Else
                xMsg = "Would you like to apply these changes to the '" & _
                       g_oCurScheme.Alias & "' document scheme?" & vbCr & _
                       "If you choose 'No' now , you can update the " & _
                       "scheme later by clicking the Reset button."
            End If

            iUserChoice = MsgBox(xMsg, vbQuestion + vbYesNo, g_xAppName)

            If iUserChoice = vbYes Then
'               keep DocumentChange code from running
'                g_bPreventDocChangeEvent = False

'               reset scheme in document
                iLoadScheme ActiveDocument, _
                            xScheme, _
                            mpSchemeType_Private

'               backup scheme properties in case the user
'               writes over them using the Word UI
                BackupProps xScheme

'               set toolbar
                If xActiveScheme(ActiveDocument) = xScheme Then
                    bSetNumberingBtnsEnable xScheme
                End If
                
                If g_bCurSchemeIsHeading Then
'                   9.7.3 - convert before reapplying scheme, now that
'                   ModifyParaFormatsToNormal will correctly act on
'                   Heading 1-9, not proprietary styles, when appropriate
                    bRet = bConvertToHeadingStyles(xScheme, False)
                End If
                    
'               reapply scheme
                UpdateParaStyles xScheme
                ApplyModifiedScheme xScheme
            End If
        End If

'       if modifying document scheme, change
'       to new version and unload form - this
'       is necessary to implement any changes to
'       trailing chars
        If iSchemeType = mpSchemeType_Document Then
'           9.8.1005
            Application.ScreenUpdating = False
            
'           Word 10 patch - always relink
'            If g_bCurSchemeIsHeading Or g_bIsXP Then
                bRelinkScheme xScheme, mpSchemeType_Document, _
                    g_bCurSchemeIsHeading, False
'            End If
            AdjustAlignmentToNormal xScheme
            UpdateParaStyles xScheme
            ApplyModifiedScheme xScheme
        End If

'       update screen with changes
        With Word.Application
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    End If

'   9.9.6001 - delete unused cont styles
    If iSchemeType = mpSchemeType_Document Then _
        RestoreContStyles xScheme
            
    Unload frmEdit
    Set frmEdit = Nothing
    
    Set g_docPreview = Nothing
    Set g_docStart = Nothing

    'reset WindowsInTaskbar if necessary
    If g_bForceWindowsInTaskbar Then _
        ActiveDocument.Parent.ShowWindowsInTaskbar = False

'   return template dirt to starting value
    Word.ActiveDocument _
        .AttachedTemplate.Saved = bTemplateSaved

'   restore the ability of DocChange to run -
'   set to TRUE in bNewScheme to prevent
'   the DocChange code from running during
'   creation of scheme
'    g_bPreventDocChangeEvent = False
    
    bEditSchemeB = True
    
    'GLOG 5292 - close QAT text stream
    If Not oTS Is Nothing Then _
        oTS.Close
    
'    frmSchemes.Visible = True
    Exit Function
ProcError:
    'GLOG 5643 - restore the QAT setting if necessary
    If bResetQAT Then
        Set oReg = New CRegistry
        If InStr(Application.Version, "16.") <> 0 Then
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\16.0\Common\Toolbars\Word", _
                "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        ElseIf g_iWordVersion = mpWordVersion_2013 Then
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\15.0\Common\Toolbars\Word", _
                "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        ElseIf g_iWordVersion = mpWordVersion_2010 Then
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\14.0\Common\Toolbars\Word", _
                "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        Else
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\12.0\Common\Toolbars\Word", _
                "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        End If
    End If
    
    'GLOG 5292 - close QAT text stream
    If Not oTS Is Nothing Then _
        oTS.Close
    
    Set frmEdit = Nothing
    RaiseError "numFunctions.bEditSchemeB"
    Exit Function
End Function

Function bSchemeExists(ByVal xScheme As String, _
                       ByVal xDisplay As String, _
                       ByVal iSchemeType As mpSchemeTypes) As Boolean
'returns true if there exists a scheme
'by name xScheme and of type iSchemeType
    Dim i As Integer
    Select Case iSchemeType
        Case mpSchemeType_Public
            For i = LBound(g_xFSchemes) To UBound(g_xFSchemes)
                If (g_xFSchemes(i, 0) = xScheme) And _
                    (g_xFSchemes(i, 1) = xDisplay) Then
                    bSchemeExists = True
                    Exit For
                End If
            Next i
        Case mpSchemeType_Private
            For i = LBound(g_xPSchemes) To UBound(g_xPSchemes)
                If (g_xPSchemes(i, 0) = xScheme) And _
                    (g_xPSchemes(i, 1) = xDisplay) Then
                    bSchemeExists = True
                    Exit For
                End If
            Next i
        Case mpSchemeType_Document
            For i = LBound(g_xDSchemes) To UBound(g_xDSchemes)
                If (g_xDSchemes(i, 0) = xScheme) And _
                    (g_xDSchemes(i, 1) = xDisplay) Then
                    bSchemeExists = True
                    Exit For
                End If
            Next i
    End Select
End Function

Function iCopySchemeFromActiveDoc(ByVal xScheme As String, _
                                  ByVal xDest As String, _
                                  ByVal iInitLevels As Integer, _
                                  ByVal iCurLevels As Integer, _
                                  Optional ByVal xIsDirty As xArray, _
                                  Optional ByVal bSave As Boolean = True, _
                                  Optional bCopyWordHeadings As Boolean = False, _
                                  Optional xBasedOn As String, _
                                  Optional bCopyStyleAttributesOnly As Boolean = False, _
                                  Optional bCopyCustomNextParaStyles As Boolean = False) _
                                  As Integer
    Dim i As Integer
    Dim xStyle As String
    Dim xNextStyle As String
    Dim xPrevStyle As String
    Dim xStyleRoot As String
    Dim styStyle As Word.Style
    Dim ltScheme As Word.ListTemplate
    Dim tDest As Variant
    Dim bCopyAll As Boolean
    Dim bIsDirty As Boolean
    Dim xUCScheme As String
    Dim xLT As String
    Dim xLTScheme As String
    Dim bLTFound As Boolean
    Dim ltDest As Word.ListTemplate
    Dim styCont As Word.Style
    Dim ltBasedOn As Word.ListTemplate
    Dim j As Integer
    Dim lBold As Long
    Dim lItalic As Long
    Dim lUnderline As Long
    Dim lSize As Long
    Dim xName As String
    Dim xCont As String
    Dim stySource As Word.Style
    Dim styCustomNext As Word.Style
    Dim xCustomNext As String
    
    Application.ScreenUpdating = False
        
'   if no dirty flag array was passed,
'   then copy everything
    bCopyAll = (xIsDirty Is Nothing)
    
    xStyleRoot = xGetStyleRoot(xScheme)
    xLTScheme = xGetFullLTName(xScheme)

'   set destination document/template
    If xDest = g_xPNumSty Then
        Set tDest = g_oPNumSty
    Else
        Set tDest = Documents(xDest)
    End If
    
'   Word 10 patch - store prototype list template for renaming
'   after Organizer Copy
    If g_bIsXP And _
            (xBasedOn <> "") And _
            tDest.ListTemplates.Count Then
        On Error Resume Next
        Set ltBasedOn = tDest.ListTemplates(xBasedOn)
        On Error GoTo 0
    End If
    
'   copy Word Heading Styles
    If bCopyWordHeadings Then
'       Word 10 patch - unlink to avoid creating a new list
'       template in destination
        If g_bIsXP Then
'            For i = 1 To 9
'                ActiveDocument.ListTemplates(xLTScheme) _
'                    .ListLevels(i).LinkedStyle = ""
'            Next i
'           7/15/02 - added optional bUnlinkOnly argument to RelinkPreserveIndents;
'           unlinking here was causing styles to lose their indents.
            RelinkPreserveIndents ActiveDocument, ActiveDocument.ListTemplates(xLTScheme), _
                xScheme, 1, iInitLevels, True, True
        End If
        
        On Error Resume Next
        For i = 1 To 9
            Application.OrganizerCopy WordBasic.FileName$(), _
                                        xDest, _
                                        xTranslateHeadingStyle(i), _
                                        wdOrganizerObjectStyles
        Next i
        On Error GoTo 0
        
'       Word 10 - relink
        If g_bIsXP Then
            RelinkPreserveIndents ActiveDocument, ActiveDocument.ListTemplates(xLTScheme), _
                xScheme, 1, iInitLevels, True
        End If
    End If
    
    'copy custom next paragraph styles (9.9.1013)
    If bCopyCustomNextParaStyles Then
        If g_xNextParaStyles(0) <> "" Then
            For i = 0 To UBound(g_xNextParaStyles)
                'GLOG 4875 - skip all built-in styles, not just Normal and Body Text
                On Error Resume Next
                Set styCustomNext = ActiveDocument.Styles(g_xNextParaStyles(i))
                On Error GoTo 0
                If Not styCustomNext Is Nothing Then
                    If Not styCustomNext.BuiltIn Then
                        Application.OrganizerCopy WordBasic.FileName$(), xDest, _
                            g_xNextParaStyles(i), wdOrganizerObjectStyles
                    End If
                    Set styCustomNext = Nothing
                End If
            Next i
        End If
    End If
    
'   add/delete levels in target
    If iInitLevels > 0 Then
        If iInitLevels > iCurLevels Then
            For i = iCurLevels + 1 To iInitLevels
                xStyle = xGetStyleName(xScheme, i, xStyleRoot)
                xCont = xStyleRoot & " Cont " & i
                If xDest = g_xPNumSty Then
'                   add "x" to deleted levels
                    Application.OrganizerRename xDest, _
                                                xStyle, _
                                                xStyle & "x", _
                                                wdOrganizerObjectStyles
                Else
'                   delete style fom document
                    On Error Resume Next
                    Application.OrganizerDelete xDest, _
                                                xStyle, _
                                                wdOrganizerObjectStyles
'                   delete cont style from document
                    Application.OrganizerDelete xDest, _
                                                xCont, _
                                                wdOrganizerObjectStyles
                    On Error GoTo 0
                End If
            Next i
        ElseIf iCurLevels > iInitLevels Then
'           remove "x" from added levels
            For i = iInitLevels + 1 To iCurLevels
'               if level has "x" in target
'               style will also exist in buffer doc
                xStyle = xGetStyleName(xScheme, i, xStyleRoot)
                On Error Resume Next
                Set styStyle = ActiveDocument.Styles(xStyle & "x")
                On Error GoTo 0
                If Not styStyle Is Nothing Then
                    Application.OrganizerRename xDest, _
                                                xStyle & "x", _
                                                xStyle, _
                                                wdOrganizerObjectStyles
                End If
                Set styStyle = Nothing
            Next i
        End If
    End If
    
'   if necessary, create new list template in target;
'   although list template names are case sensitive,
'   Word will not allow differently cased list templates
'   of the same name; so recase old list template name,
'   rather than trying to add new one (which would cause error)
    xUCScheme = UCase(xScheme)
    For Each ltScheme In tDest.ListTemplates
        If ltScheme.Name <> "" Then
            xLT = xGetLTRoot(ltScheme.Name)
            If UCase(xLT) = xUCScheme Then
                xLT = ltScheme.Name
                bLTFound = True
                Exit For
            End If
        End If
    Next ltScheme
        
    If bLTFound Then
'       reuse old list template
        Set ltScheme = tDest.ListTemplates(xLT)
    Else
'       add new list template
        Set ltScheme = tDest.ListTemplates.Add(True)
    End If

'   rename list template, correctly cased
    If xDest = g_xPNumSty Then
'       in sty file, list template name is just root
        ltScheme.Name = xScheme
    Else
'       in doc, list template name includes props
        ltScheme.Name = xLTScheme
    End If
    
'   update existing list template in target
    For i = 1 To 9
        bIsDirty = False
        On Error Resume Next
        bIsDirty = CBool(xIsDirty(i, mpLevelIsDirty)) Or _
                   CBool(xIsDirty(i, mpTCIsDirty))
        On Error GoTo 0
        
        If bIsDirty Or bCopyAll Then
            iCopyListTemplateFromActiveDoc xScheme, xDest, i
        End If
    Next i
    
'   update scheme props
    For i = 1 To 9
        bIsDirty = False
        On Error Resume Next
        bIsDirty = CBool(xIsDirty(i, mpLevelIsDirty)) Or _
                   CBool(xIsDirty(i, mpTCIsDirty)) Or _
                   CBool(xIsDirty(i, mpDocPropIsDirty))
        On Error GoTo 0
        
        If bIsDirty Or bCopyAll Then
            iCopyPropFromActiveDoc xScheme, i, xDest
        End If
    Next i

'   Word 10 patch - LT loses name after Organizer Copy
    xLTScheme = xGetFullLTName(xScheme, xDest)

    With Application
'       the following needs to be done twice because next
'       paragraph styles may not be in target initially and can't
'       add dummies in template without opening it as a document
        For j = 1 To 2
'           Next paragraph styles
            For i = 1 To 9
                bIsDirty = False
                On Error Resume Next
                bIsDirty = xIsDirty(i, mpNextParaIsDirty)
                On Error GoTo 0
                
                '9.9.6001 - retain cont styles that have been edited
                If bIsDirty And (xDest <> g_xPNumSty) Then
                    m_bContStyles(i - 1) = True
                End If
                
                If bIsDirty Or bCopyAll Then
                    xStyle = xStyleRoot & " Cont " & i
                    On Error Resume Next
                    .OrganizerCopy WordBasic.FileName$(), _
                                    xDest, _
                                    xStyle, _
                                    wdOrganizerObjectStyles
                    If Err.Number Then
                        Set styCont = Word.Documents(xDest).Styles(xStyle)
                        If (styCont Is Nothing) And (i <= iCurLevels) Then
                            '2nd condition added 6/29/08 to fix GLOG 4802
                            '9.9.6001 - copy cont styles from source
                            CreateContStyles xScheme, i, , bCopyCustomNextParaStyles
                        Else
                            Set styCont = Nothing
                        End If
                    End If
                    
                    If Not bCopyAll And i < iCurLevels Then
'                       do subsequent level next para
                        xStyle = xStyleRoot & " Cont " & i + 1
                        On Error Resume Next
                        .OrganizerCopy WordBasic.FileName$(), _
                                        xDest, _
                                        xStyle, _
                                        wdOrganizerObjectStyles
                        If Err.Number Then
                            Set styCont = Word.Documents(xDest).Styles(xStyle)
                            If styCont Is Nothing Then
'                                Word.Documents(xDest).Styles.Add xStyle
                                '9.9.6001 - copy cont styles from source
                                CreateContStyles xScheme, i + 1, , bCopyCustomNextParaStyles
                            Else
                                Set styCont = Nothing
                            End If
                        End If
                        
                        '9.9.2005 - the following code causes the next paragraph style as
                        'defined in Normal.dot to be copied back to the document when
                        'GenerateBufferFromCopy is False, because bCopyAll=True prevents the
                        'style from the doc from ever getting copied to the buffer -
                        'although I don't understand the purpose of this code (only cont styles
                        'are editable), I felt more comfortable conditionalizing it than
                        'removing it altogether
                        If g_bGenerateBufferFromCopy Then
                            xStyle = ActiveDocument.Styles(xStyleRoot & "_L" & i).NextParagraphStyle
                            If InStr(xStyle, " Cont ") = 0 Then
                                On Error Resume Next
                                .OrganizerCopy WordBasic.FileName$(), _
                                                xDest, _
                                                xStyle, _
                                                wdOrganizerObjectStyles
                                If Err.Number Then
                                    Set styCont = Word.Documents(xDest).Styles(xStyle)
                                    If styCont Is Nothing Then
                                        If g_bCreateUnlinkedStyles Then
                                            '9.9.4010
                                            mdlCC.AddUnlinkedParagraphStyle Word.Documents(xDest), _
                                                xStyle
                                        Else
                                            Word.Documents(xDest).Styles.Add xStyle
                                        End If
                                    Else
                                        Set styCont = Nothing
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Next i
        Next j
        
        On Error GoTo 0
        
        '9.9.1011 - if copying style attributes only, temporarily activate the
        'source document - Word will only allow you to set the base and next
        'paragraph styles in the active document
        If bCopyStyleAttributesOnly Then
            Documents(xDest).Activate
        End If
        
'       the following needs to be done twice because next
'       paragraph styles may not be in target initially and can't
'       add dummies in template without opening it as a document
        For j = 1 To 2
'           numbered paragraph styles
            For i = 1 To iCurLevels
                xStyle = xGetStyleName(xScheme, i, xStyleRoot)
                xNextStyle = xGetStyleName(xScheme, i + 1, xStyleRoot)
                bIsDirty = False
                On Error Resume Next
                bIsDirty = CBool(xIsDirty(i, mpLevelIsDirty)) Or _
                           CBool(xIsDirty(i, mpParaIsDirty))
'               also need to copy style if changed font attributes
'               of previous level, which it's based on
                bIsDirty = bIsDirty Or CBool(xIsDirty(i - 1, mpTCIsDirty))
                On Error GoTo 0
                
                If bIsDirty Or bCopyAll Then
                    On Error Resume Next
                    If bCopyStyleAttributesOnly Then
                        '9.9.1011 - if buffer is based on Normal.dot, copying the style
                        'back into the user's doc will add a new list template -
                        'to avoid this, copy just the attributes -
                        'the bCopyStyleAttributesOnly argument should only be set to true
                        'after editing a document scheme
                        Set styStyle = ActiveDocument.Styles(xStyle)
                        If styStyle Is Nothing Then
                            'level has been added in edit scheme
                            If g_bCreateUnlinkedStyles Then
                                '9.9.4010
                                Set styStyle = mdlCC.AddUnlinkedParagraphStyle(ActiveDocument, _
                                    xStyle)
                            Else
                                Set styStyle = ActiveDocument.Styles.Add(xStyle, _
                                    wdStyleTypeParagraph)
                            End If
                            xPrevStyle = xGetStyleName(xScheme, i - 1, xStyleRoot)
                            styStyle.BaseStyle = xPrevStyle
                        End If
                        
                        'copy font and paragraph format
                        Set stySource = g_docPreview.Styles(xStyle)
                        With styStyle
                            'GLOG 5402 - set font after setting next paragraph style
                            .ParagraphFormat = stySource.ParagraphFormat
                            .NextParagraphStyle = stySource.NextParagraphStyle
                            .Font = stySource.Font
                        End With
                        Set styStyle = Nothing
                    Else
                        .OrganizerCopy WordBasic.FileName$(), _
                                        xDest, _
                                        xStyle, _
                                        wdOrganizerObjectStyles
                    End If
                                
'                   do for all levels but last
                    If i < iCurLevels Then
'                       copy next level, as it will have been changed
'                       if new line spacing, space after, or alignment
'                       has been assigned to current level
                        On Error Resume Next
                        If bCopyStyleAttributesOnly Then
                            Set styStyle = ActiveDocument.Styles(xNextStyle)
                            If styStyle Is Nothing Then
                                'level has been added in edit scheme
                                If g_bCreateUnlinkedStyles Then
                                    '9.9.4010
                                    Set styStyle = mdlCC.AddUnlinkedParagraphStyle(ActiveDocument, _
                                        xNextStyle)
                                Else
                                    Set styStyle = ActiveDocument.Styles.Add(xNextStyle, _
                                        wdStyleTypeParagraph)
                                End If
                                styStyle.BaseStyle = xStyle
                            End If
                            
                            'copy font and paragraph format
                            Set stySource = g_docPreview.Styles(xNextStyle)
                            With styStyle
                                'GLOG 5402 - set font after setting next paragraph style
                                .ParagraphFormat = stySource.ParagraphFormat
                                .NextParagraphStyle = stySource.NextParagraphStyle
                                .Font = stySource.Font
                            End With
                            Set styStyle = Nothing
                        Else
                            .OrganizerCopy WordBasic.FileName$(), _
                                            xDest, _
                                            xNextStyle, _
                                            wdOrganizerObjectStyles
                        End If
                    End If
                    On Error GoTo 0
                End If
            
                If j = 2 Then
''                   link to appropriate level of list template
'                    If bIsDirty Or (iInitLevels = 0) Then
''                       tDest.ListTemplates(xLTScheme) _
''                           .ListLevels(i).LinkedStyle = xStyle
''                       Word 10
'                        With ltScheme
'                            If xDest = g_xPNumSty Then
'                                .ListLevels(i).LinkedStyle = xStyle
'                            Else
'                                RelinkPreserveIndents Documents(xDest), .ListLevels(i), xStyle
'                            End If
'                        End With
'                    End If
                    
'                   correct Word 97 cardinal/ordinal text AllCaps
'                   issue (e.g. "ARTICLE One")
                    If InStr(Word.Application.Version, "8.") <> 0 Then
                        With ltScheme.ListLevels(i).Font
                            If ActiveDocument.Styles(xStyle).Font.AllCaps And _
                                    .AllCaps Then
                                lBold = .Bold
                                lItalic = .Italic
                                lUnderline = .Underline
                                lSize = .Size
                                xName = .Name
                                .Reset
                                If lBold <> wdUndefined Then _
                                    .Bold = lBold
                                If lItalic <> wdUndefined Then _
                                    .Italic = lItalic
                                If lUnderline <> wdUndefined Then _
                                    .Underline = lUnderline
                                If lSize <> wdUndefined Then _
                                    .Size = lSize
                                If xName <> "" Then _
                                    .Name = xName
                            End If
                        End With
                    End If
                End If
            Next i
        Next j
        
        '9.9.1011 - reactivate buffer doc if necessary
        If bCopyStyleAttributesOnly Then
            g_docPreview.Activate
        End If
        
'       relink
        If (xDest = g_xPNumSty) Or g_bIsXP Then
'           we don't want to preserve style indents in XP, because
'           styles get unlinked by OrganizerCopy
            For i = 1 To iCurLevels
                If bCopyWordHeadings And (tDest.Name = "Refresh.mpf") Then
                    'added in 9.9.1011 (GLOG 4787)
                    xStyle = xTranslateHeadingStyle(i)
                Else
                    xStyle = xGetStyleName(xScheme, i, xStyleRoot)
                End If
                ltScheme.ListLevels(i).LinkedStyle = xStyle
            Next i
        Else
            RelinkPreserveIndents Documents(xDest), ltScheme, xScheme, _
                1, iCurLevels, bCopyWordHeadings
        End If
        
'       unlink unused levels
        If iCurLevels < 9 Then
            For i = iCurLevels + 1 To 9
                ltScheme.ListLevels(i).LinkedStyle = ""
            Next i
        End If
        
'       Word 10 patch - LT loses name after Organizer Copy
        If xDest = g_xPNumSty Then
'           in sty file, list template name is just root
            ltScheme.Name = xScheme
        Else
'           in doc, list template name includes props
            ltScheme.Name = xLTScheme
        End If
            
'       when using Word Heading styles, an additional list
'       template is added to mpNumbers.sty when the styles
'       are copied from the buffer - get rid of it
        If xDest = g_xPNumSty Then
            For Each ltDest In tDest.ListTemplates
                On Error Resume Next
                If Left(ltDest.Name, Len(xScheme) + 1) = _
                        xScheme & "|" Then
                    ltDest.Name = ""
                    Exit For
                End If
            Next ltDest
        End If
        
'       Word 10 patch - prototype LT also loses name during
'       Organizer Copy
        If Not ltBasedOn Is Nothing Then _
            ltBasedOn.Name = xBasedOn
            
'       Word can't copy autotext if source or dest is a document -
'       if active document is not a template then this line will
'       err.  skip over - preview will be created on the fly in
'       CPreview.ShowPreview.
        On Error Resume Next
        If ActiveDocument.SaveFormat = wdFormatTemplate Then
            .OrganizerCopy WordBasic.FileName$(), _
                           xDest, _
                           xScheme & "Preview", _
                           wdOrganizerObjectAutoText
        End If
        On Error GoTo 0
        
        If bSave Then
            If (g_iWordVersion >= mpWordVersion_2010) And (xDest = g_xPNumSty) Then
                'workaround for Word 2010 to avoid error 5986 -
                '"this command is not available in an unsaved document"
                tDest.OpenAsDocument.Save
                ActiveDocument.Close False
            Else
                tDest.Save
            End If
        End If
    End With    'Application
    
'    Application.StatusBar = ""
    
End Function

Function iCopyListTemplateFromActiveDoc(xScheme As String, _
                                        xDest As String, _
                                        Optional iLevel As Integer) _
                                        As Integer
    Dim ltSource As ListTemplate
    Dim llSource As ListLevel
    Dim ltDest As ListTemplate
    Dim llDest As ListLevel
    Dim i As Integer
    Dim xWindow As String
    Dim xStartDoc As String
    Dim xLinkedStyle As String
    Dim sWindowHeight As Single
    Dim sWindowWidth As Single
    Dim xLT As String
    Dim bIs97DocScheme As Boolean
    
    Set ltSource = ActiveDocument _
        .ListTemplates(xGetFullLTName(xScheme))
    If xDest = g_xPNumSty Then
        Set ltDest = g_oPNumSty.ListTemplates(xScheme)
    Else
        xLT = xGetFullLTName(xScheme, xDest)
        Set ltDest = Documents(xDest) _
            .ListTemplates(xLT)
'       since list template number and text positions edited in Word 97 will revert to
'       original settings when opened in Word 2k or XP, and the first line and left
'       indents of style will go with them if these are in sync with the LT positions,
'       we've decided to 1) stop editing the LT positions of 97 doc schemes and 2) prioritize
'       the style indents everywhere in Num2k; this should limit the number of schemes that
'       MacPac will need to artificially force out of sync in order to prevent reversion
'        bIs97DocScheme = (InStr(Word.Application.Version, "8.") <> 0)
    End If
    
    If IsMissing(iLevel) Then
'       copy all levels
        For i = 1 To 9
            Set llSource = ltSource.ListLevels(i)
            Set llDest = ltDest.ListLevels(i)
            iCopyListLevel xScheme, llSource, llDest, , bIs97DocScheme
        Next i
    Else
'       copy only specified level
        Set llSource = ltSource.ListLevels(iLevel)
        Set llDest = ltDest.ListLevels(iLevel)
        iCopyListLevel xScheme, llSource, llDest, , bIs97DocScheme
    End If
    
End Function

Function bNewScheme(xBasedOnDefault As String, iBasedOnDefaultType As mpSchemeTypes) As Boolean
'creates a new scheme - returns FALSE if an error occurred
'called from Scheme Menu
    Dim iLevels As Integer
    Dim iInitLevels As Integer
    Dim xWindow As String
    Dim xBuffer As String
    Dim xBasedOn As String
    Dim iBasedOnType As mpSchemeTypes
    Dim iSource As Integer
    Dim oPreview As New CPreview
    Dim oScheme As CNumScheme
    Dim bIsHScheme As Boolean
    Dim docP As Word.Document
    Dim docS As Word.Document
    Dim sWindowHeight As Single
    Dim sWindowWidth As Single
    Dim iWindowState As WdWindowState
    Dim bIs2000 As Boolean
    Dim docStart As Word.Document
    Dim oForm As VB.Form
    Dim xCaption As String
    Dim bAllow As Boolean

    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If

'   get starting doc
    Set docS = Word.ActiveDocument

'   get whether attached template is dirty - used below
    bTemplateSaved = Word.ActiveDocument _
                    .AttachedTemplate.Saved

    If g_lUILanguage = wdFrenchCanadian Then
        Set oForm = New frmNewSchemeFrench
    Else
        Set oForm = New frmNewScheme
    End If
    
'   show new scheme form
    With oForm
'       select same not in frmNewScheme
'       as is selected in frmSchemes
        If iBasedOnDefaultType Then
'           select 'based on' scheme
            .tvwSchemes.Nodes(iBasedOnDefaultType & _
                       xBasedOnDefault).Selected = True
        Else
'           select first public scheme
            .tvwSchemes.Nodes(2).Child.Selected = True
        End If

'        frmSchemes.Visible = False
        .Show vbModal

        If .Cancelled Then
            Unload oForm
            Set oForm = Nothing
'            frmSchemes.Visible = True
            bNewScheme = False
            Exit Function
        End If

'       DocChange event procedure will
'       run when a buffer doc is created below -
'       prevent DocChange from running
'        g_bPreventDocChangeEvent = True

        Screen.MousePointer = vbHourglass
        Application.ScreenUpdating = False

        Set oScheme = New CNumScheme
        iLevels = .cmbLevels
        xBasedOn = .BasedOnScheme
'       bNewSchemeB will need this again for Word XP
        g_xBasedOn = xBasedOn
        oScheme.Alias = .txtSchemeName
        oScheme.Name = mpPrefix & .txtStyleName
        oScheme.SchemeType = mpSchemeType_Private
'        oScheme.TOCScheme = .cmbTOCScheme.ListIndex + 1
        oScheme.TOCScheme = iGetTOCSchemeIndex(.cmbTOCScheme.Text)
        oScheme.Origin = .BasedOnSchemeType
        oScheme.DynamicFonts = .chkBaseOnNormal
        oScheme.DynamicSpacing = .chkAdjustSpacing
        oScheme.Description = .txtDescription

        Unload oForm
        Set oForm = Nothing
        DoEvents

    End With

    bIs2000 = (InStr(Word.Application.Version, "8.") = 0) 'tweaked for Word 10

'   With Word 2000 under NT, showing CStatus before Preview
'   Doc is created will prevent correct document being displayed
'   behind Edit Form most of the time
    If Not bIs2000 Then
        Set g_oStatus = New CStatus

        With g_oStatus
            .ProgressBarVisible = False 'GLOG 5590
            If g_lUILanguage = wdFrenchCanadian Then
                .Title = "Cr�er " & oScheme.Alias & " Th�me"
                .Show , "Cr�ation du Th�me.  Veuillez patienter�"
            Else
                .Title = "Creating " & oScheme.Alias & " Scheme"
                .Show , "Creating scheme.  Please wait..."
            End If
        End With
    End If

'   get if heading scheme for future use
    bIsHScheme = bIsHeadingScheme(xBasedOn)

    Set docStart = ActiveDocument
    xWindow = ActiveWindow.Caption
    
'   create buffer doc - this will hold the
'   scheme that is being used as the "based on"
'   scheme.  if "based on" scheme is firm or personal,
'   create buffer from the source sty file.
'   if "based on" scheme is doc scheme, we'll copy
'   the scheme from the source to the buffer.
    WordBasic.DisableAutoMacros 1
    If oScheme.Origin = mpSchemeType_Public Then
        Set docP = Documents.Add(g_xFNumSty, True)
    Else
        Set docP = Documents.Add(g_xPNumSty, True)
    End If

    'message if scheme contains fixed digit numbering and destination doesn't support it
    If g_iWordVersion >= mpWordVersion_2010 Then
        bAllow = True
        If (oScheme.Origin = mpSchemeType_Document) And _
                (mdlWord14.GetCompatibilityMode(docP) < 14) Then
            bAllow = Not ContainsWord2010NumberStyle(xBasedOn, mpSchemeType_Document, docStart)
        ElseIf oScheme.Origin = mpSchemeType_Public Then
            If ContainsWord2010NumberStyle(xBasedOn, mpSchemeType_Public) Then
                bAllow = (mdlWord14.GetCompatibilityMode(g_oPNumSty.OpenAsDocument) >= 14)
                ActiveDocument.Close False
            End If
        End If
        If Not bAllow Then
            With docP
                .Saved = True
                .Close
            End With
            RemoveBufferFromList
            docStart.Activate
            Application.ScreenUpdating = True
            Application.ScreenRefresh
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Le nouveau th�me ne peut �tre cr�� car le prototype contient un style de num�rotation qui est disponible uniquement dans Word 2010 et le mod�le de destination n'est pas disponible en mode de compatibilit� dans Word 2010.", _
                    vbInformation, g_xAppName
            Else
                MsgBox "The new scheme cannot be created because the prototype " & _
                    "contains a numbering style that is only available in Word 2010 and " & _
                    "the destination template is not in Word 2010 compatibility mode.", _
                    vbInformation, g_xAppName
            End If
            WordBasic.DisableAutoMacros 0
            Screen.MousePointer = vbDefault
            Exit Function
        End If
    End If
        
    WordBasic.DisableAutoMacros 0
    DoEvents
    
    If g_lUILanguage = wdFrenchCanadian Then
        xCaption = "Aper�u Th�me"
    Else
        xCaption = "Scheme Preview"
    End If
    
    Set g_docPreview = docP
    With g_docPreview.ActiveWindow
        .Caption = xCaption
        If .WindowState <> wdWindowStateMaximize Then
            .WindowState = wdWindowStateMaximize
        End If
        g_docPreview.Activate
    End With

'   save buffer doc - prior to 9.9.2009, we only did this when basing on a doc scheme
    xBuffer = g_xUserPath & "\buffer.mpf"
    ActiveDocument.SaveAs xBuffer, , , , False
    ActiveDocument.ActiveWindow.Caption = xCaption
    Set g_docPreview = ActiveDocument

'   copy scheme to buffer
    If oScheme.Origin = mpSchemeType_Document Then
'       switch back to source file
        docStart.ActiveWindow.Activate
'        Application.Windows(xWindow).Activate
        DoEvents
        
        iInitLevels = iGetLevels(xBasedOn, mpSchemeType_Document)
        
        If bIsHScheme Then
'           convert to MacPac Styles
            bConvertToMacPacStyles xBasedOn, True, False
        End If

'       9.9.6001 - track which cont styles are already in document
        StoreContStyles xBasedOn
        
'       copy scheme from source to buffer
        lRet = iCopySchemeFromActiveDoc(xBasedOn, _
                                        xBuffer, _
                                        iInitLevels, _
                                        iInitLevels)
                                        
        '9.9.6001 - delete appropriate cont styles
        RestoreContStyles xBasedOn
        
        If bIsHScheme Then
'           convert back to Heading styles
            bConvertToHeadingStyles xBasedOn, False
        End If

        g_docPreview.ActiveWindow.Activate
        DoEvents
        
'       convert to inexact spacing if necessary
        If oScheme.DynamicSpacing Then
            With docStart.Styles(wdStyleNormal).ParagraphFormat
                If .LineSpacingRule = wdLineSpaceExactly Then
                    ConvertToInexactSpacing xBasedOn, .LineSpacing
                End If
            End With
        End If
        
'       style indents now control in documents, so LT positions are no longer credible;
'       set number and text positions before copying to sty file
        lSynchIndents xBasedOn
    End If

    DoEvents
    If bIs2000 Then
        Set g_oStatus = New CStatus

        With g_oStatus
            .ProgressBarVisible = False 'GLOG 5590
            If g_lUILanguage = wdFrenchCanadian Then
                .Title = "Cr�er " & oScheme.Alias & " Th�me"
                .Show , "Cr�ation du Th�me.  Veuillez patienter�"
            Else
                .Title = "Creating " & oScheme.Alias & " scheme"
                .Show , "Creating scheme.  Please wait..."
            End If
        End With
    End If

'   create continue styles
    CreateContStyles oScheme.Name

'   rename prototype scheme
    If oScheme.Name <> xBasedOn Then
        bRet = bRenameExistingScheme(oScheme.Name, _
                                    xBasedOn, _
                                    iLevels)
                                    
'       relink to fresh list template
        If g_bIsXP Then _
            LinkToNewListTemplate oScheme.Name
        
'       change scheme display name in buffer
        SetRecord oScheme, , Word.ActiveDocument

'       set origin in buffer as private
        SetField oScheme.Name, _
                 mpRecField_Origin, _
                 mpSchemeType_Private, , _
                 Word.ActiveDocument
    End If

    If g_lUILanguage = wdFrenchCanadian Then
        xMsg = "Enregistrement du th�me   Veuillez patienter..."
    Else
        xMsg = "Saving scheme.  Please wait..."
    End If
    g_oStatus.Show , xMsg

'   copy scheme to mpNumbers.sty
    lRet = iCopySchemeFromActiveDoc(oScheme.Name, _
                                    g_xPNumSty, _
                                    0, _
                                    iLevels, _
                                    xBasedOn:=xBasedOn)

'   add scheme record
    lRet = AddRecord(oScheme)

'   set origin to 'personal'
    SetField oScheme.Name, _
             mpRecField_Origin, _
             mpSchemeType_Private, _
             mpSchemeType_Private

'   refresh schemes tree
    iGetSchemes g_xPSchemes(), _
                 mpSchemeType_Private

    Set g_oCurScheme = oScheme

    Set oScheme = Nothing

'   set destination of new scheme
'   as personal numbers sty
    g_xCurDest = g_xPNumSty

    Screen.MousePointer = vbDefault
'    Application.ScreenUpdating = True
    SetPreviewWindow

    bNewScheme = True
'    bNewSchemeB

    Exit Function
ProcError:
'   restore the ability of DocChange to run
'    g_bPreventDocChangeEvent = False
    EchoOn
    Application.ScreenUpdating = True
    RaiseError "numFunctions.bNewScheme"
    bNewScheme = False
    Exit Function
End Function
Function bNewSchemeB() As Boolean
'second part of bNewScheme - this function is called by the
'windows timer. in Word 2000, this is the only way to
'show the ruler in the preview window, which
'only appears in a new document after all
'code that runs when the doc is created
'is finished executing
    Static iCount As Integer
    Dim oPreview As CPreview
    Dim xAlias As String
    Dim xScheme As String
    Dim iSchemeType As mpSchemeTypes
    Dim frmEdit As VB.Form
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    'GLOG 5292 - open QAT for read access - this will prevent VBA macros from
    'getting removed by this function
    Dim oQAT As Scripting.File
    Dim oTS As Scripting.TextStream
    If (g_iWordVersion = mpWordVersion_2010) And (iSchemeType = mpSchemeType_Private) Then
        Set oQAT = GetQATFile()
        If Not oQAT Is Nothing Then
            Set oTS = oQAT.OpenAsTextStream(ForReading)
        End If
    End If

'   get detail of edited scheme
    With g_oCurScheme
        xAlias = .Alias
        xScheme = .Name
        iSchemeType = .SchemeType
    End With
    
    If g_lUILanguage = wdFrenchCanadian Then
        Set frmEdit = New frmEditSchemeFrench
    Else
        Set frmEdit = New frmEditScheme
    End If
    
'   show form
ShowForm:
    With frmEdit
        .RulerDisplayDelay = g_lRulerDisplayDelay_New '9.9.6012
        .Show vbModal

'       store zoom state for next time
        bSetUserIni "Numbering", "Zoom", .chkZoom

        If .ZoomClicked Then
            'GLOG 5549 - set zoom with dialog hidden as workaround for
            'ruler not reappearing in Word 2013 with dialog showing
            Application.ScreenUpdating = False
            .ZoomClicked = False
            SetPreviewZoom (.chkZoom = vbChecked)
            Application.ScreenUpdating = True
            Application.ScreenRefresh
            GoTo ShowForm
        ElseIf (Not .Cancelled) Or (g_iWordVersion >= mpWordVersion_2013) Then
'           create preview only for private schemes
            '9.9.6006 - added Word 2013 condition, as initial bitmap is blank in 2013
            If iSchemeType = mpSchemeType_Private Then
                g_docPreview.ActiveWindow.Activate
                Set oPreview = New CPreview
                oPreview.CreateBitmap xScheme, xAlias
            End If
        End If

'       list template font sizes have been changed for preview/bitmap
        ResetListNumberFonts
        
'       copy scheme from buffer
        If Not .Cancelled Then
            lRet = iCopySchemeFromActiveDoc(xScheme, _
                                            g_xCurDest, _
                                            .InitialLevels, _
                                            .CurrentLevels, _
                                            .xarDirty, _
                                            xBasedOn:=g_xBasedOn)
            
            If iSchemeType = mpSchemeType_Document Then
                bSetNumberingBtnsEnable g_oCurScheme.Name
            End If
        Else
'           delete contents of preview file
            ActiveDocument.Content.Delete
        End If
    End With

'   save new scheme
    SavePNumSty
    
'9.9.6012 - remmed out, as screen updating was already on here
'and needs to turned be off below in Word 2013 higher
''   close buffer doc - turn on screen updating only if NewScheme
''   was launched via the Schemes dialog
'    If g_iSchemeEditMode = mpModeSchemeNewB Then _
'        Application.ScreenUpdating = True

'   iManage COM workaround
'    g_docPreview.Close wdDoNotSaveChanges
    With g_docPreview
        'GLOG 5624: Zoom percentage is saved separately for Normal and Print views
        If g_iWordVersion >= mpWordVersion_2013 Then
            Application.ScreenUpdating = False
            .ActiveWindow.View.Zoom.Percentage = g_lDefaultZoomPercentage
            .ActiveWindow.View.Type = wdPageView
            .ActiveWindow.View.Zoom.Percentage = g_lDefaultZoomPercentage
            .ActiveWindow.View.Type = wdNormalView
            Application.ScreenUpdating = True
        End If

        .Saved = True
        .Close
    End With
    RemoveBufferFromList

'   return template dirt to starting value
    Word.ActiveDocument _
        .AttachedTemplate.Saved = bTemplateSaved

    Unload frmEdit
    Set frmEdit = Nothing
    
    Set oPreview = Nothing
    Set g_docPreview = Nothing
    Set g_docStart = Nothing
    On Error Resume Next
    Kill g_xUserPath & "\buffer.mpf"

    bNewSchemeB = True
    
'   restore the ability of DocChange to run -
'   set to TRUE in bNewScheme to prevent
'   the DocChange code from running during
'   creation of scheme
'    g_bPreventDocChangeEvent = False

    'GLOG 5292 - close QAT text stream
    If Not oTS Is Nothing Then _
        oTS.Close
    
    Exit Function
    
ProcError:
'   restore the ability of DocChange to run -
'   set to TRUE in bNewScheme to prevent
'   the DocChange code from running during
'   creation of scheme

    'GLOG 5292 - close QAT text stream
    If Not oTS Is Nothing Then _
        oTS.Close
    
    Set frmEdit = Nothing
'    g_bPreventDocChangeEvent = False
    RaiseError "numFunctions.bNewSchemeB"
    Exit Function
End Function

Sub CreateContStyles(ByVal xScheme As String, _
                     Optional iLevel As Integer, _
                     Optional bUseListTabPos As Boolean, _
                     Optional bCopyFromSty As Boolean)
'create Cont 1-9 styles - mimic numbered para attributes
    Dim xStyleRoot As String
    Dim xStyleName As String
    Dim styCont As Word.Style
    Dim i As Integer
    Dim iStart As Integer
    Dim iEnd As Integer
    Dim styPrevCont As Word.Style
    Dim xPrevCont As String
    Dim xNumStyle As String
    Dim styNum As Word.Style
    Dim xNextLevel As String
    Dim styNextLevel As Word.Style
    Dim oLT As Word.ListTemplate
    Dim iAlignment As Integer
    Dim oNumPara As Word.ParagraphFormat
    Dim bUseDefTabStop As Boolean
    Dim sDef As Single
    Dim sMod As Single
    Dim iNextAlign As Integer
    Dim sNextSpacing As Single
    Dim iNextRule As Integer
    Dim sNextBefore As Single
    Dim sNextAfter As Single
    Dim xNextFontName As String
    Dim sNextFontSize As Single
    Dim sNextRight As Single
    Dim sNextLeft As Single
    Dim sNextFirstLine As Single
    Dim xNextCont As String
    Dim styNextCont As Word.Style
    Dim bNoFirstLineIndent As Boolean
    Dim iSchemes As Integer
    Dim oForm As VB.Form
    Dim iOrigin As mpSchemeTypes
    
    On Error GoTo ProcError
    
    '10/4/12 - user is no longer required to select scheme
    If xScheme = "" Then
        iSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
        If iSchemes = 0 Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Aucun th�me de num�rotation MacPac dans ce document.", vbInformation, g_xAppName
            Else
                MsgBox "There are no MacPac numbering schemes " & _
                    "in this document.", vbInformation, g_xAppName
            End If
        ElseIf iSchemes = 1 Then
            xScheme = g_xDSchemes(0, 0)
        Else
            Set oForm = New frmSchemeSelector
            oForm.Show vbModal
            If Not oForm.Cancelled Then
                xScheme = g_xDSchemes(oForm.lstSchemes.ListIndex, 0)
            End If
            Set oForm = Nothing
        End If
    End If
    
    If xScheme = "" Then _
        Exit Sub
        
    EchoOff

    xScheme = xGetLTRoot(xScheme)
    
    '9.9.6001 - new optional parameter to copy from sty
    If bCopyFromSty Then
        If bCopyContStylesFromSource(xScheme, iLevel, iOrigin) Then
            SetContStyleProperties xScheme, iLevel, iOrigin '9.9.6002
            EchoOn
            Exit Sub
        End If
    End If
    
    xStyleRoot = xGetStyleRoot(xScheme)
    
    With Word.ActiveDocument
        If iLevel = 0 Then
            iStart = 1
            
            If mpBase.bIsHeadingScheme(xScheme) Then
                'GLOG 5072 (5/7/12)
                iEnd = iGetLevels(xScheme, mpSchemeType_Document)
            Else
                iEnd = 9
            End If
        Else
            iStart = iLevel
            iEnd = iLevel
            
'           prevent changes from cascading to next level
            xNextCont = xStyleRoot & " Cont " & iLevel + 1
            
            On Error Resume Next
            Set styNextCont = .Styles(xNextCont)
            On Error GoTo ProcError
            
            If Not styNextCont Is Nothing Then
                With styNextCont
                    With .Font
                        xNextFontName = .Name
                        sNextFontSize = .Size
                    End With
                    With .ParagraphFormat
                        iNextAlign = .Alignment
                        iNextRule = .LineSpacingRule
                        sNextSpacing = .LineSpacing
                        sNextBefore = .SpaceBefore
                        sNextAfter = .SpaceAfter
                        sNextRight = .RightIndent
                        sNextLeft = .LeftIndent
                        sNextFirstLine = .FirstLineIndent
                    End With
                End With
            End If
        End If

        For i = iStart To iEnd
            xStyleName = xStyleRoot & " Cont " & i
            xPrevCont = xStyleRoot & " Cont " & i - 1
            xNumStyle = xGetStyleName(xScheme, i)
            
'           test for existence of styles
            On Error Resume Next
            Set styCont = .Styles(xStyleName)
            Set styPrevCont = .Styles(xPrevCont)
            Set styNum = .Styles(xNumStyle)
            On Error GoTo ProcError
            
            If (styCont Is Nothing) Then
                If g_bCreateUnlinkedStyles Then
                    '9.9.4010
                    Set styCont = mdlCC.AddUnlinkedParagraphStyle(ActiveDocument, _
                        xStyleName)
                Else
                    Set styCont = .Styles.Add(xStyleName)
                End If
                With styCont
                    If Not styPrevCont Is Nothing Then
                        .BaseStyle = xPrevCont
                    Else
                        .BaseStyle = wdStyleNormal
                    End If
                End With
            End If
                
            With styCont
'               this will guarantee no frame or borders when cursor
'               happens to be in a paragraph with one of these
                On Error Resume Next
                .Borders.Enable = False
                .Frame.Delete
                On Error GoTo ProcError
                
                If Not styNum Is Nothing Then
                    Set oNumPara = styNum.ParagraphFormat
'                    .Font = styNum.Font
'                    .ParagraphFormat = styNum.ParagraphFormat
'                   need to do properties individually; if you sync entire font and
'                   paragraph format, the link may inadvertently persist, so that (for
'                   example) later unlinking list level will impact cont style
                    With .Font
'                        .AllCaps = styNum.Font.AllCaps
'                        .Bold = styNum.Font.Bold
'                        .ColorIndex = styNum.Font.ColorIndex
'                        .Italic = styNum.Font.Italic
                        .Name = styNum.Font.Name
                        .Size = styNum.Font.Size
'                        .SmallCaps = styNum.Font.SmallCaps
'                        .Underline = styNum.Font.Underline
                    End With
                    
                    With .ParagraphFormat
'                       set alignment level prop
                        iAlignment = xGetLevelProp(xScheme, i, _
                            mpNumLevelProp_TrailUnderline, mpSchemeType_Document)
                        If ((oNumPara.Alignment = wdAlignParagraphRight) And _
                                (iAlignment < 2)) Or bBitwisePropIsTrue(iAlignment, _
                                mpTrailUnderlineField_AdjustToNormal) Then
'                           turn on adjustment
                            iAlignment = iAlignment Or _
                                mpTrailUnderlineField_AdjustContToNormal
                            .Alignment = wdAlignParagraphLeft
                        Else
'                           turn off adjustment
                            iAlignment = iAlignment And _
                                (Not mpTrailUnderlineField_AdjustContToNormal)
                            .Alignment = oNumPara.Alignment
                        End If
                        lSetLevelProp xScheme, i, mpNumLevelProp_TrailUnderline, _
                            CStr(iAlignment), mpSchemeType_Document
                    
'                        .TabStops.ClearAll
                        .LineSpacingRule = oNumPara.LineSpacingRule
                        .LineSpacing = oNumPara.LineSpacing
                        .SpaceAfter = oNumPara.SpaceAfter
                        .SpaceBefore = oNumPara.SpaceBefore
                        .OutlineLevel = wdOutlineLevelBodyText
                        
                        If .Alignment = wdAlignParagraphCenter Then
'                           numbered para is centered - use generic indents
                            xNextLevel = xGetStyleName(xScheme, i + 1)
                            On Error Resume Next
                            Set styNextLevel = ActiveDocument.Styles(xNextLevel)
                            On Error GoTo ProcError
                            If Not styNextLevel Is Nothing Then
                                .Alignment = styNextLevel.ParagraphFormat.Alignment
                                If .Alignment = wdAlignParagraphCenter Then
                                    .Alignment = ActiveDocument.Styles(wdStyleNormal) _
                                        .ParagraphFormat.Alignment
                                End If
                            End If
                            .LeftIndent = 0
                            .FirstLineIndent = 0
                            .RightIndent = 0
                        Else
'                           align with text, not number
                            .LeftIndent = oNumPara.LeftIndent
                            .RightIndent = oNumPara.RightIndent
                            
'                           first line indent
                            If xGetLevelProp(xScheme, i, mpNumLevelProp_TrailChr, _
                                    mpSchemeType_Document) = mpTrailingChar_Tab Then
                                With oNumPara
                                    If .TabStops.Count = 0 Then
'                                       converted schemes may not have tab stops
                                        bUseListTabPos = True
                                    ElseIf .TabStops(1).Position <= _
                                            .LeftIndent + .FirstLineIndent Then
'                                       first tab stop is left of number
                                        bUseListTabPos = True
                                    End If
                                
                                    If bUseListTabPos Then
                                        On Error Resume Next
                                        Set oLT = styNum.ListTemplate
                                        On Error GoTo ProcError
                                        
                                        If oLT Is Nothing Then
'                                           no list template
                                            bUseDefTabStop = True
                                        ElseIf oLT.ListLevels(i).TabPosition = wdUndefined Then
'                                           list level has no defined tab position
                                            If .FirstLineIndent < 0 Then
'                                               hanging indent - give cont style no first line indent
                                                bNoFirstLineIndent = True
                                            Else
'                                               non-hanging indent
                                                bUseDefTabStop = True
                                            End If
                                        ElseIf oLT.ListLevels(i).TabPosition <= _
                                                .LeftIndent + .FirstLineIndent Then
'                                           tab position is left of number
                                            bUseDefTabStop = True
                                        End If
                                    End If
                                End With
                                
                                If bNoFirstLineIndent Then
'                                   no first line indent
                                    .FirstLineIndent = 0
                                ElseIf bUseDefTabStop Then
'                                   use default tab stop
                                    sDef = ActiveDocument.DefaultTabStop
                                    sMod = Abs((oNumPara.LeftIndent + _
                                        oNumPara.FirstLineIndent + sDef) Mod sDef)
                                    If sMod = 0 Then
                                        .FirstLineIndent = _
                                            oNumPara.FirstLineIndent + sDef
                                    Else
                                        .FirstLineIndent = _
                                            oNumPara.FirstLineIndent + sMod
                                    End If
                                ElseIf bUseListTabPos Then
'                                   use list level tab position
                                    .FirstLineIndent = oLT.ListLevels(i).TabPosition - _
                                        .LeftIndent
                                Else
'                                   use first tab stop
                                    .FirstLineIndent = oNumPara.TabStops(1).Position - _
                                        .LeftIndent
                                End If
                            Else
                                .FirstLineIndent = oNumPara.FirstLineIndent
                            End If
                        End If
                    End With
                End If
            End With
            
            Set styCont = Nothing
            Set styPrevCont = Nothing
            Set styNum = Nothing
            Set styNextLevel = Nothing
        Next i
    
'       restore next level cont style
        If Not styNextCont Is Nothing Then
            With styNextCont
                With .Font
                    .Name = xNextFontName
                    .Size = sNextFontSize
                End With
                With .ParagraphFormat
                    .Alignment = iNextAlign
                    .LineSpacingRule = iNextRule
                    .LineSpacing = sNextSpacing
                    .SpaceBefore = sNextBefore
                    .SpaceAfter = sNextAfter
                    .RightIndent = sNextRight
                    .LeftIndent = sNextLeft
                    .FirstLineIndent = sNextFirstLine
                End With
            End With
        End If
    End With

    EchoOn
    Exit Sub
ProcError:
    EchoOn
    RaiseError "mdlN90.CreateContStyles"
    Exit Sub
End Sub

Function lAddLevel(xScheme As String, iLevel As Integer) As Long
    Dim xStyle As String
    Dim xStyleRoot As String
    Dim xPrevLevelStyle As String
    Dim styPrevLevel As Word.Style
    Dim xValue As String
    Dim styNew As Word.Style
    Dim pfP As Word.ParagraphFormat
    Dim iHeadingFormat As Integer
    Dim styCont As Word.Style
    Dim xNewCont As String
    Dim llNew As Word.ListLevel
    Dim xLT As String
    Dim bIsHScheme As Boolean
    Dim xPStyle As String
    Dim xPrevLevelPStyle As String
    Dim xPrevLevelCont As String
    Dim styPrevLevelCont As Word.Style
    Dim iAlignment As Integer
    
    xStyleRoot = xGetStyleRoot(xScheme)
    xStyle = xGetStyleName(xScheme, iLevel)
    xPStyle = xGetStyleName(xScheme, iLevel, xStyleRoot)
    xNewCont = xStyleRoot & " Cont " & iLevel
    xPrevLevelStyle = xGetStyleName(xScheme, iLevel - 1)
    xPrevLevelPStyle = xGetStyleName(xScheme, iLevel - 1, xStyleRoot)
    xPrevLevelCont = xStyleRoot & " Cont " & (iLevel - 1)
    xLT = xGetFullLTName(xScheme)
    bIsHScheme = bIsHeadingScheme(xScheme)
    
'   add new style based on previous level
    With ActiveDocument.Styles
        Set styPrevLevel = .Item(xPrevLevelStyle)
        If bIsHScheme Then
'           if scheme is "Heading", new style already exists
            Set styNew = .Item(xStyle)
'           add proprietary style to avoid problems when
'           scheme is later copied from buffer
            If xScheme <> "HeadingStyles" Then
                On Error Resume Next
                If g_bCreateUnlinkedStyles Then
                    '9.9.4010
                    mdlCC.AddUnlinkedParagraphStyle ActiveDocument, xPStyle
                Else
                    .Add xPStyle, wdStyleTypeParagraph
                End If
                On Error GoTo 0
                With .Item(xPStyle)
                    .BaseStyle = xPrevLevelPStyle
                    .ParagraphFormat.OutlineLevel = iLevel
                    .Font.Size = ActiveDocument _
                        .Styles(xPrevLevelPStyle).Font.Size
                End With
            End If
        Else
            On Error Resume Next
            If g_bCreateUnlinkedStyles Then
                '9.9.4010
                Set styNew = mdlCC.AddUnlinkedParagraphStyle(ActiveDocument, xStyle)
            Else
                Set styNew = .Add(xStyle, wdStyleTypeParagraph)
            End If
            If Err = 5173 Then
'               style already exists
                Set styNew = .Item(xStyle)
            End If
            On Error GoTo 0
        End If
        
'       add cont style if necessary
        On Error Resume Next
        Set styPrevLevelCont = .Item(xPrevLevelCont)
        Set styCont = .Item(xNewCont)
        On Error GoTo 0
        If styCont Is Nothing Then
            If g_bCreateUnlinkedStyles Then
                '9.9.4010
                Set styCont = mdlCC.AddUnlinkedParagraphStyle(ActiveDocument, xNewCont)
            Else
                Set styCont = .Add(xNewCont, wdStyleTypeParagraph)
            End If
        End If
    
''       add 'continue' style -
''       will err if already present
'        On Error Resume Next
'        Set styCont = .Add(xNewCont, wdStyleTypeParagraph)
'        Set styPrevLevelCont = .Item(xPrevLevelCont)
'        On Error GoTo 0
'
''       this is a patch - when adding a level to
''       a document scheme, font is 20 (due to preview)
'        If (Not styCont Is Nothing) And _
'                (Not styPrevLevelCont Is Nothing) Then
'            styCont.Font.Size = styPrevLevelCont.Font.Size
'        End If
    End With
    
    With styNew
        If bIsHScheme Then
            .Font = styPrevLevel.Font
            .ParagraphFormat = styPrevLevel.ParagraphFormat
        Else
            .BaseStyle = styPrevLevel
        End If
        If styPrevLevel.NextParagraphStyle = _
                xStyleRoot & " Cont " & (iLevel - 1) Then
            .NextParagraphStyle = xNewCont
        Else
            .NextParagraphStyle = _
                styPrevLevel.NextParagraphStyle
        End If
        
'       this is a patch - when adding a level to
'       a document scheme, font is 20 (due to preview)
        .Font.Size = styPrevLevel.Font.Size
        
        With .ParagraphFormat
            .OutlineLevel = iLevel
'           the following line spacing related lines are
'           necessary because the preview will cause the line
'           spacing of the style to be exactly 19 points.
            Set pfP = styPrevLevel.ParagraphFormat
            .LineSpacingRule = pfP.LineSpacingRule
            .LineSpacing = pfP.LineSpacing
            .SpaceAfter = pfP.SpaceAfter
            .SpaceBefore = pfP.SpaceBefore
            If pfP.Alignment <> _
                    wdAlignParagraphCenter Then
                .Alignment = pfP.Alignment
            Else
                .Alignment = wdAlignParagraphLeft
            End If
            
'           style indents now take priority over LT positions
            .LeftIndent = 0
            .FirstLineIndent = 0
        End With
    End With
    
'    With styCont
'        If Not styPrevLevelCont Is Nothing Then
''           hierarchical
'            .BaseStyle = xPrevLevelCont
'        End If
'        .Font = styNew.Font
'        With .ParagraphFormat
'            .LineSpacingRule = pfP.LineSpacingRule
'            .LineSpacing = pfP.LineSpacing
'            .SpaceAfter = pfP.SpaceAfter
'            .SpaceBefore = pfP.SpaceBefore
'            .Alignment = styNew.ParagraphFormat.Alignment
'            .LeftIndent = 0
'            .FirstLineIndent = InchesToPoints(0.5)
'        End With
'    End With
    
'   set generic list template properties for new level
    Set llNew = ActiveDocument.ListTemplates(xLT).ListLevels(iLevel)
    With llNew
        .NumberStyle = wdListNumberStyleArabic
        .NumberFormat = "%" & LTrim(Str(iLevel)) & "."
        .NumberPosition = 0
        .TextPosition = InchesToPoints(0)
        .NumberPosition = InchesToPoints(0)
        .TabPosition = InchesToPoints(0.5)
        .TrailingCharacter = wdTrailingTab
        .ResetOnHigher = True
        .StartAt = 1
        With .Font
            .Bold = False
            .Italic = False
            .Strikethrough = False
            .Subscript = False
            .Superscript = False
            .Shadow = False
            .Outline = False
            .Emboss = False
            .Engrave = False
            .AllCaps = False
            .SmallCaps = False
            .Hidden = False
            .Underline = False
            .ColorIndex = wdAuto
            .DoubleStrikeThrough = False
'           if ll size was previously defined, it's now
'           been forced to 20 pts for preview; undefining
'           is not an option
'            .Size = wdUndefined
            On Error Resume Next
            g_sNumberFontSizes(iLevel - 1) = _
                g_sNumberFontSizes(iLevel - 2)
            On Error GoTo 0
            .Animation = wdAnimationNone
            .Name = ""
            
'           set level properties for new level-
'           get alignment options from previous level
            iAlignment = xGetLevelProp(xScheme, _
                                       iLevel - 1, _
                                       mpNumLevelProp_TrailUnderline, _
                                       mpSchemeType_Document)
            xValue = "|1|" & Trim(Str(iAlignment)) & "|"
            
'           get heading type from previous level
            iHeadingFormat = xGetLevelProp(xScheme, _
                                   iLevel - 1, _
                                   mpNumLevelProp_HeadingFormat, _
                                   mpSchemeType_Document)
            iHeadingFormat = iHeadingFormat And mpTCFormatField_Type
            xValue = xValue & Trim(Str(iHeadingFormat)) & "|"
            
            lRet = lSetNumLevelProps(ActiveDocument.ListTemplates(xLT), _
                iLevel, xValue)
            If lRet Then
                Err.Raise lRet
            End If
        End With
        .LinkedStyle = xStyle
    End With
    
'   cont style - match numbered level attributes
    CreateContStyles xScheme, iLevel

'    Application.StatusBar = "Level added"
    g_bSchemeIsDirty = True
End Function

Function lDeleteLevel(xScheme As String, _
                      iCurLevels As Integer, _
                      iLevel As Integer) As Long
    Dim i As Integer
    Dim xStyle As String
    Dim xNextStyle As String
    Dim xNextLevelProp As String
    Dim llp As Word.ListLevel
    Dim llNext As Word.ListLevel
    Dim xStyleRoot As String
    Dim xLT As String
    Dim bIsHScheme As Boolean
    Dim oFont As Word.Font
    Dim styCont As Word.Style
    Dim styNextCont As Word.Style
    Dim xNextNextStyle As String
    Dim ltScheme As Word.ListTemplate
    
    xStyleRoot = xGetStyleRoot(xScheme)
    xLT = xGetFullLTName(xScheme)
    Set ltScheme = ActiveDocument.ListTemplates(xLT)

    With ActiveDocument
        If iLevel < iCurLevels Then
            For i = iLevel To iCurLevels - 1
'               promote subsequent list template levels
'                xLT = xGetFullLTName(xScheme)
                With ltScheme.ListLevels
                    Set llp = .Item(i)
                    Set llNext = .Item(i + 1)
                    With llNext
                        llp.Alignment = .Alignment
                        llp.Font.AllCaps = .Font.AllCaps
                        llp.Font.Bold = .Font.Bold
                        llp.Font.Italic = .Font.Italic
                        llp.Font.Underline = .Font.Underline
                        llp.Font.Name = .Font.Name
                        g_sNumberFontSizes(i - 1) = g_sNumberFontSizes(i)
                        
'                       clear out existing format/style to avoid
'                       incompatibilities - ie code errs when number format
'                       has a number placeholder and an attempt is made to
'                       set the style to bullets, etc.
                        llp.NumberFormat = ""
                        llp.NumberStyle = wdListNumberStyleArabic
                        llp.NumberStyle = .NumberStyle
                        llp.NumberFormat = _
                            xPromoteNumberFormat(.NumberFormat)
                        llp.NumberPosition = .NumberPosition
                        llp.TextPosition = .TextPosition
                        If i > 1 Then _
                            llp.ResetOnHigher = .ResetOnHigher
                        llp.StartAt = .StartAt
                        llp.TabPosition = .TabPosition
                        llp.TrailingCharacter = .TrailingCharacter
                        
                        If i = 1 Then
                            Dim oScheme As New CSchemeRecords
                            Dim o_FirstLevel As CNumScheme
                            Dim xLevelOneProps As String
                            Dim iDynamic As Integer
                            
                            Set o_FirstLevel = oScheme.GetRecord(xScheme, mpSchemeType_Document)
                            With o_FirstLevel
                                iDynamic = oScheme.GetDynamicFieldValue(.DynamicFonts, _
                                    .DynamicSpacing)
                                xLevelOneProps = "|" & .Alias & "|" & .Origin & "|" & _
                                    .TOCScheme & "|" & iDynamic
                            End With
                                
                            Set oScheme = Nothing
                            Set o_FirstLevel = Nothing
                        End If
                            
                                
'                       get next level property
                        xNextLevelProp = xGetLevelProps(xScheme, _
                                            i + 1, _
                                            mpSchemeType_Document)
                                            
                        If i = 1 Then _
                            xNextLevelProp = xLevelOneProps & xNextLevelProp
                            
'                       set current prop to next level prop
                        lSetLevelProps xScheme, _
                                       i, _
                                       mpSchemeType_Document, _
                                       xNextLevelProp
                                       
                    End With
                End With
            
'               promote subsequent level styles
                With .Styles
                    xStyle = xGetStyleName(xScheme, i)
                    xNextStyle = xGetStyleName(xScheme, i + 1)
                    xNextNextStyle = xGetStyleName(xScheme, i + 2)

'                   numbered paragraph styles
                    .Item(xStyle).ParagraphFormat = _
                        .Item(xNextStyle).ParagraphFormat

'                   this will undo any toggle effect resulting
'                   from hierarchical styles
                    Set oFont = .Item(xNextStyle).Font.Duplicate
                    .Item(xStyle).Font = _
                        .Item(xNextStyle).Font
                    With .Item(xNextStyle).Font
                        .Bold = oFont.Bold
                        .Italic = oFont.Italic
                        .AllCaps = oFont.AllCaps
                        .SmallCaps = oFont.SmallCaps
                        .Underline = oFont.Underline
                    End With
                        
'                   ensure that outline level remains the same
                    .Item(xStyle).ParagraphFormat.OutlineLevel = i
                                        
'                   reassign next paragraph style
                    Select Case .Item(xNextStyle).NextParagraphStyle
                        Case xStyleRoot & " Cont " & (i + 1)
                            .Item(xStyle).NextParagraphStyle = _
                                xStyleRoot & " Cont " & i
                        Case xNextStyle
                            .Item(xStyle).NextParagraphStyle = xStyle
                        Case xNextNextStyle
                            .Item(xStyle).NextParagraphStyle = xNextStyle
                        Case Else
                            .Item(xStyle).NextParagraphStyle = _
                                .Item(xNextStyle).NextParagraphStyle
                    End Select
                
'                   unique next paragraph styles
                    xStyle = xStyleRoot & " Cont " & i
                    xNextStyle = xStyleRoot & " Cont " & (i + 1)
                    On Error Resume Next
                    Set styCont = .Item(xStyle)
                    Set styNextCont = .Item(xNextStyle)
                    On Error GoTo 0
                    If (Not styCont Is Nothing) And _
                            (Not styNextCont Is Nothing) Then
                        styCont.ParagraphFormat = _
                            styNextCont.ParagraphFormat
                        styCont.Font = styNextCont.Font
                        If styNextCont.NextParagraphStyle = xNextStyle Then
                            styCont.NextParagraphStyle = xStyle
                        Else
                            styCont.NextParagraphStyle = _
                                styNextCont.NextParagraphStyle
                        End If
                    End If
                    Set styCont = Nothing
                    Set styNextCont = Nothing
                End With    'styles
            Next i  'level
        End If
            
'       delete last level style
        If xScheme <> "HeadingStyles" Then
            xStyle = xGetStyleName(xScheme, iCurLevels, xStyleRoot)
            .Styles(xStyle).Delete
        End If
        
        'GLOG 4981 (2/2/12) - set generic list template properties for deleted level
        Set llp = ltScheme.ListLevels(iCurLevels)
        With llp
            .NumberStyle = wdListNumberStyleNone
            .NumberFormat = ""
            .NumberPosition = 0
            .TextPosition = InchesToPoints(0)
            .NumberPosition = InchesToPoints(0)
            .TabPosition = InchesToPoints(0.5)
            .TrailingCharacter = wdTrailingNone
            .ResetOnHigher = True
            .StartAt = 1
            .Alignment = wdListLevelAlignLeft
            With .Font
                .Bold = False
                .Italic = False
                .Strikethrough = False
                .Subscript = False
                .Superscript = False
                .Shadow = False
                .Outline = False
                .Emboss = False
                .Engrave = False
                .AllCaps = False
                .SmallCaps = False
                .Hidden = False
                .Underline = False
                .ColorIndex = wdAuto
                .DoubleStrikeThrough = False
                .Name = ""
                If .Size <> wdUndefined Then _
                    .Size = ActiveDocument.Styles(wdStyleNormal).Font.Size
                .Animation = wdAnimationNone
            End With
        End With
        
'       delete property - must clear prop
'       because existence of prop with intro
'       pipe signifies that the level exists
        lSetLevelProps xScheme, _
                       iCurLevels, _
                       mpSchemeType_Document, _
                       PropertyNotAvailable
    
'       relink styles to levels
        bIsHScheme = bIsHeadingScheme(xScheme)
        RelinkPreserveIndents ActiveDocument, ltScheme, xScheme, _
            1, iCurLevels - 1, bIsHScheme
        ltScheme.ListLevels(iCurLevels).LinkedStyle = ""
    
    End With    'active document
    
'    Application.StatusBar = "Level deleted"
    g_bSchemeIsDirty = True
End Function

Function bRenameExistingScheme(xFrom As String, _
                               xTo As String, _
                               Optional iLevels As Integer) As Boolean
    Dim i As Integer
    Dim styStyle As Word.Style
    Dim xToStyle As String
    Dim xStyle As String
    Dim xStyleRoot As String
    Dim xToStyleRoot As String
    Dim iOriginalLevels As Integer
    Dim oProp As Office.DocumentProperty
    Dim ltP As Word.ListTemplate
    Dim xValue As String

    #If compHandleErrors Then
        On Error GoTo ProcError
    #Else
        On Error GoTo 0
    #End If
    
'   get number of levels in prototype
    iOriginalLevels = iGetLevels(xTo, _
                                 mpSchemeType_Document)
    If IsMissing(iLevels) Then _
        iLevels = iOriginalLevels
    
    With ActiveDocument
'       rename list template
'       if there's already an existing list template with the
'       new name, rename the old one first to prevent error;
'       this assumes that scheme will only be renamed once in
'       the active doc, as is the case with buffer doc
        On Error Resume Next
        Set ltP = .ListTemplates(xGetFullLTName(xFrom))
        On Error GoTo 0

'       rename if lt with name xFrom exists
        If Not ltP Is Nothing Then _
            ltP.Name = ltP.Name & "x"
        
        .ListTemplates(xGetFullLTName(xTo)).Name = xFrom
        Set ltP = .ListTemplates(xFrom)
        xStyleRoot = xGetStyleRoot(xFrom)
        xToStyleRoot = xGetStyleRoot(xTo)
        
'       rename docProps if they exist
        For i = 1 To 9
            Set oProp = Nothing
            On Error Resume Next
            xValue = .CustomDocumentProperties(xTo & i)
            On Error GoTo 0
            If xValue <> Empty Then _
                .CustomDocumentProperties(xTo & i).Name = xFrom & i
        Next i
    
'       rename next paragraph styles
        For i = 1 To 9
            xToStyle = xToStyleRoot & " Cont " & i
            xStyle = xStyleRoot & " Cont " & i
            
            Set styStyle = Nothing
            On Error Resume Next
            Set styStyle = .Styles(xStyle)
            On Error GoTo 0
            If Not styStyle Is Nothing Then _
                styStyle.Delete
            
            On Error Resume Next
            Application.OrganizerRename WordBasic.FileName$(), _
                                        xToStyle, _
                                        xStyle, _
                                        wdOrganizerObjectStyles
            On Error GoTo 0
        Next i
    
'       rename numbered paragraph styles
        For i = 1 To iLevels
            xToStyle = xGetStyleName(xTo, i, xToStyleRoot)
'           always create a scheme
'           using MacPac proprietary styles
            xStyle = xStyleRoot & "_L" & i
            
            Set styStyle = Nothing
            On Error Resume Next
            Set styStyle = .Styles(xStyle)
            If Not styStyle Is Nothing Then
                Application.OrganizerRename WordBasic.FileName$(), _
                                            xStyle, _
                                            xStyle & "x", _
                                            wdOrganizerObjectStyles
            End If
            On Error GoTo 0
            
            If i > iOriginalLevels Then
'               add new level
                lAddLevel xFrom, i
            Else
                If xToStyle = xTranslateHeadingStyle(i) Then
                    xToStyle = "Heading_L" & i
                End If
                
'               rename existing style
                Application.OrganizerRename WordBasic.FileName$(), _
                                            xToStyle, _
                                            xStyle, _
                                            wdOrganizerObjectStyles
            End If
        Next i
                                    
'       ensure link between styles and levels
        RelinkPreserveIndents ActiveDocument, ltP, xFrom, 1, iLevels
    
        If iLevels < 9 Then
            For i = iLevels + 1 To 9
'               delete level props
                lSetNumLevelProps ltP, i, _
                                  PropertyNotAvailable
'               unlink unused levels
                ltP.ListLevels(i).LinkedStyle = ""
            Next i
        End If
    End With    'active document
    Exit Function
ProcError:
    RaiseError "numFunctions.bRenameExistingScheme"
    Exit Function
End Function

Function xPromoteNumberFormat(xNumberFormat As String) As String
    Dim iPos As Integer
    
    iPos = InStr(xNumberFormat, "%")
    While iPos
        xNumberFormat = Left(xNumberFormat, iPos) & _
            (Val(Mid(xNumberFormat, iPos + 1, 1)) - 1) & _
            Right(xNumberFormat, Len(xNumberFormat) - iPos - 1)
        iPos = InStr(iPos + 1, xNumberFormat, "%")
    Wend

'   delete non-existent previous level
    iPos = InStr(xNumberFormat, "%0")
    If iPos Then
        ' Period may be in middle of string, so check after deleted level
        If Mid(xNumberFormat, iPos + 2, 1) = "." Then
            xNumberFormat = Left(xNumberFormat, iPos - 1) & Right(xNumberFormat, Len(xNumberFormat) - iPos - 2)
        Else
            xNumberFormat = Left(xNumberFormat, iPos - 1) & _
                Right(xNumberFormat, Len(xNumberFormat) - iPos - 1)
        End If
'       delete superfluous period
'        If Left(xNumberFormat, 1) = "." Then
'            xNumberFormat = Right(xNumberFormat, _
'                Len(xNumberFormat) - 1)
'        End If
    End If
    
    xPromoteNumberFormat = xNumberFormat
End Function

Function bDeleteScheme(ByVal xScheme As String, _
                        ByVal xAlias As String, _
                        iSchemeType As mpSchemeTypes) As Boolean
'deletes a private scheme -
'returns TRUE if successful, FALSE
'if user cancelled or Error occurred
    
    Dim xDefaultScheme As String
    Dim xStyle As String
    Dim i As Integer
    Dim j As Integer
    Dim iLevels As Integer
    Dim ltScheme As Word.ListTemplate
    Dim xStyleRoot As String
    Dim xProp As String
    Dim tDest As Variant
    Dim xDest As String
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
        
    With Application
'       get target
        If iSchemeType = mpSchemeType_Private Then
            xDest = g_xPNumSty
            Set tDest = g_oPNumSty
        Else
            xDest = WordBasic.FileName$()
            Set tDest = ActiveDocument
        End If
        
'       get list template
        Set ltScheme = tDest _
            .ListTemplates(xGetFullLTName(xScheme, xDest))
            
        With ltScheme
'           unlink before deleting - this has apparently not
'           been necessary with private schemes, so no reason
'           to risk a change
            If iSchemeType = mpSchemeType_Document Then
                For i = 1 To 9
                    .ListLevels(i).LinkedStyle = ""
                Next i
            End If
'           make the list template a non-macpac template
            .Name = ""
        End With
    
'       get style root
        xStyleRoot = xGetStyleRoot(xScheme)

'       delete next paragraph styles
        On Error Resume Next
        For i = 1 To 9
            xStyle = xStyleRoot & " Cont " & i
            .OrganizerDelete xDest, _
                             xStyle, _
                             wdOrganizerObjectStyles
        Next i
        
'       delete numbered paragraph styles
        #If compHandleErrors Then
            On Error GoTo ProcError
        #Else
            On Error GoTo 0
        #End If
    
        iLevels = iGetLevels(xScheme, iSchemeType)
        
        On Error Resume Next
        For i = 1 To iLevels
            xStyle = xStyleRoot & "_L" & i
            .OrganizerDelete xDest, _
                             xStyle, _
                             wdOrganizerObjectStyles
        Next i
        
'       delete old levels
        If iLevels < 9 Then
            For i = iLevels + 1 To 9
                xStyle = xStyleRoot & "_L" & i & "x"
                .OrganizerDelete xDest, _
                                 xStyle, _
                                 wdOrganizerObjectStyles
            Next i
        End If
        
        #If compHandleErrors Then
            On Error GoTo ProcError
        #Else
            On Error GoTo 0
        #End If
    
        If iSchemeType = mpSchemeType_Private Then
'           clear default scheme if xscheme is default scheme
            xDefaultScheme = xGetUserIni("Numbering", "DefaultScheme")
            If xScheme = xDefaultScheme Then
                bSetUserIni "Numbering", "DefaultScheme", ""
            End If
    
'           delete autotext entry
            On Error Resume Next
            g_oPNumSty.AutoTextEntries(xScheme & "Preview").Delete
            #If compHandleErrors Then
                On Error GoTo ProcError
            #Else
                On Error GoTo 0
            #End If
        
'           raise error if not "Member of collection
'           does not exist"- if autotext entry
'           doesn't exist, we don't have to worry
'           about deleting it.
            If Err.Number And Err.Number <> 5941 Then
                Err.Raise Err.Number
            End If
        
'           'delete' scheme property
            DeleteRecord xScheme
        
''           make the list template a non-macpac template
'            .Templates(g_xPNumSty) _
'                .ListTemplates(xScheme).Name = ""

'           relink schemes
'            bRelinkSchemes mpSchemeType_Private
        
'           save mpNumbers.sty
'            .Templates(g_xPNumSty).Save

            'remmed for GLOG 5539 (9.9.6006) - this has been moved to the end of
            'the process because we need to hide the form during save to avoid
            'error with FileSite
'            SavePNumSty
    
'           delete bitmap
            Dim xMPB As String
            xMPB = g_xMPBPath & "\" & xSubstitute(xAlias, _
                "/", "=") & ".mpb"
            
            On Error Resume Next
            Kill xMPB
            If Len(Dir(xMPB)) Then
'               error occurred while deleting preview
                If g_lUILanguage = wdFrenchCanadian Then
                    xMsg = "Impossible de supprimer l'Aper�u pour ce th�me. Veuillez contacter votre administrateur."
                Else
                    xMsg = "Could not delete the Preview file for this scheme." & _
                           "Please contact your administrator."
                End If
                MsgBox xMsg, vbCritical, g_xAppName
            End If
        Else
'           additional precaution with document schemes
            bRelinkSchemes mpSchemeType_Document
            
            On Error Resume Next
            With ActiveDocument.Variables
'               delete backup props
                .Item(xScheme).Delete
                
'               delete active scheme variables
                If xActiveScheme(ActiveDocument) = xScheme Then
                    .Item(mpActiveSchemeDocVar).Delete
                    .Item(mpActiveScheme80DocVar).Delete
                    
'                   disable toolbar buttons
                    bSetNumberingBtnsEnable ""
                End If
            End With
            On Error GoTo ProcError
        End If
    End With    'application

    bDeleteScheme = True
    Exit Function
    
ProcError:
    RaiseError "numFunctions.bDeleteScheme"
    bDeleteScheme = False
    Exit Function
End Function

Function iCopyListLevel(xScheme As String, _
                        llSource As ListLevel, _
                        llDest As ListLevel, _
                        Optional bCopyAllProps As Boolean = False, _
                        Optional bSkipIndents As Boolean = False) _
                        As Integer
    Dim fontSource As Word.Font
    
    With llDest
        ' Need to clear out format so that existing bullet style
        ' number will not cause error if format contains a level specifier
        .NumberFormat = ""
        .NumberStyle = llSource.NumberStyle
        .NumberFormat = llSource.NumberFormat
        .TrailingCharacter = llSource.TrailingCharacter
        If Not bSkipIndents Then
            If llSource.TextPosition <> 9999999 Then _
                .TextPosition = llSource.TextPosition
            If llSource.NumberPosition <> 9999999 Then _
                .NumberPosition = llSource.NumberPosition
        End If
        If llSource.TabPosition <> 9999999 Then _
            .TabPosition = llSource.TabPosition
        If .Index > 1 Then
            If g_iWordVersion = mpWordVersion_97 Then
                'set boolean property value
                .ResetOnHigher = llSource.ResetOnHigher
            Else
                'set long integer property value
                .Parent.Parent.ListLevels(.Index).ResetOnHigher = _
                    llSource.Parent.Parent.ListLevels(.Index).ResetOnHigher
            End If
        End If
        .StartAt = llSource.StartAt
        .Alignment = llSource.Alignment
        
        Set fontSource = llSource.Font
        With .Font
            .Bold = fontSource.Bold
            .Italic = fontSource.Italic
'           the following conditional is necessary to
'           prevent unintended toggle
            If .AllCaps <> fontSource.AllCaps Then _
                .AllCaps = fontSource.AllCaps
            .Underline = fontSource.Underline
            .ColorIndex = fontSource.ColorIndex
            
            lSetNumLevelProps llDest.Parent.Parent, llDest.Index, _
                xGetNumLevelProps(llSource.Parent.Parent, llSource.Index)

'            If llDest.NumberFormat = ChrW(&HB7) Then
'                .Name = "Symbol"
'            End If
            .Name = fontSource.Name
            .Size = fontSource.Size

'           these aren't offered in MacPac customization,
'           so can be skipped to increase speed; reset should
'           call, since user may have changed using Word
            If bCopyAllProps Then
                .Strikethrough = fontSource.Strikethrough
                .Subscript = fontSource.Subscript
                .Superscript = fontSource.Superscript
                .Shadow = fontSource.Shadow
                .Outline = fontSource.Outline
                .Emboss = fontSource.Emboss
                .Engrave = fontSource.Engrave
                .Hidden = fontSource.Hidden
                .DoubleStrikeThrough = fontSource.DoubleStrikeThrough
                .Animation = fontSource.Animation
            End If
        End With
    End With
    
End Function

Function bRelinkScheme(ByVal xScheme As String, _
                       ByVal iSchemeType As mpSchemeTypes, _
                       ByVal bUseWordHeadings As Boolean, _
                       Optional bPreserveStyles As Boolean = True, _
                       Optional bPreserveIndents As Boolean = True) As Boolean
'   reset level-style links in Personal sty
    Dim i As Integer
    Dim j As Integer
    Dim iLevels As Integer
    Dim ltP As Word.ListTemplate
    Dim oSource As Object
    Dim xStyleRoot As String
    
'   get source
    Set oSource = Source(iSchemeType)
    
'   tag paragraphs with current styles
    If (iSchemeType = mpSchemeType_Document) And _
            bPreserveStyles Then
        TagNumberedParas
    End If
    
'   get levels
    iLevels = iGetLevels(xScheme, iSchemeType)
    xStyleRoot = xGetStyleRoot(xScheme)
            
'   get listtemplate
    On Error GoTo ProcError_MissingLT
    Set ltP = oSource.ListTemplates(xGetFullLTName(xScheme))
    
'   relink
    On Error GoTo ProcError_MissingStyle
    If (iSchemeType = mpSchemeType_Document) And bPreserveIndents Then
        RelinkPreserveIndents ActiveDocument, ltP, xScheme, _
            1, iLevels, bUseWordHeadings
    Else
        For i = 9 To 1 Step -1
            If i > iLevels Then
'               level does not exist in scheme - unlink
                ltP.ListLevels(i).LinkedStyle = ""
            Else
'               link to appropriate style
                ltP.ListLevels(i).LinkedStyle = xGetStyleName(xScheme, i)
            End If
        Next i
    End If
    On Error GoTo 0
    
'   restyle numbered paras
    If (iSchemeType = mpSchemeType_Document) And _
            bPreserveStyles Then
        RestoreTaggedStyles
    End If
    
    bRelinkScheme = True
    
    Exit Function

ProcError_MissingStyle:
    If g_lUILanguage = wdFrenchCanadian Then
        xMsg = "Le style " & xStyleRoot & "_L" & i & _
               " n'existe pas dans ce document. Veuillez utiliser la bo�te de dialogue pour R�initialiser votre th�me."
    Else
        xMsg = "The style " & xStyleRoot & "_L" & i & _
               " does not exist in this document.  Please " & _
               "use the Schemes dialog to Reset your scheme."
    End If
    MsgBox xMsg, vbExclamation, g_xAppName
    Exit Function

ProcError_MissingLT:
    Exit Function
End Function

Function bRelinkHeadingStylesToMacPacStyles() As Boolean
'   reset level-style links in Personal sty
    Dim i As Integer
    Dim j As Integer
    Dim iLevels As Integer
    Dim ltP As ListTemplate
    Dim oSource As Object
    Dim xStyle As String
    Dim xLT As String
    Dim styMP As Word.Style
    Dim sFirst As Single
    Dim sLeft As Single
    
'   get listtemplate
    xLT = xGetFullLTName("HeadingStyles")
    Set ltP = Word.ActiveDocument.ListTemplates(xLT)
    
'   ensure existence of all MacPac styles
    For i = 1 To 9
        xStyle = "Heading_L" & i
        On Error Resume Next
        Set styMP = ActiveDocument.Styles(xStyle)
        On Error GoTo ProcError
        If styMP Is Nothing Then
            If g_bCreateUnlinkedStyles Then
                '9.9.4010
                mdlCC.AddUnlinkedParagraphStyle ActiveDocument, xStyle
            Else
                ActiveDocument.Styles.Add xStyle
            End If
        Else
            Set styMP = Nothing
        End If
    Next i
    
'   this is a workaround for a Word quirk that causes the
'   selected list level to be linked to "Heading_L9", after
'   replacing Heading 1-9 with Heading_L1-9
    i = Selection.Range.ListFormat.ListLevelNumber
    xStyle = "Heading_L" & i
    If Selection.Style = xStyle Then
        With ActiveDocument.Styles(xStyle).ParagraphFormat
            sFirst = .FirstLineIndent
            sLeft = .LeftIndent
        End With
        RelinkPreserveIndents ActiveDocument, ltP, "HeadingStyles", i, i, True
        With ActiveDocument.Styles(xStyle).ParagraphFormat
            .FirstLineIndent = sFirst
            .LeftIndent = sLeft
        End With
    End If
    
'   relink all levels
    RelinkPreserveIndents ActiveDocument, ltP, "HeadingStyles", 1, 9
    
    Exit Function

ProcError:
    RaiseError "numFunctions.bRelinkHeadingStylesToMacPacStyles"
    Exit Function
End Function

Function bRelinkSchemes(ByVal iSchemeType As mpSchemeTypes, _
                        Optional bPromptForReset As Boolean = False) _
                        As Boolean

'   reset level-style links;
'   returns TRUE if all MacPac schemes were successfully linked
    Dim i As Integer
    Dim iLevels As Integer
    Dim ltP As ListTemplate
    Dim oSource As Object
    Dim bIsHScheme As Boolean
    Dim bHSchemeIsUsed As Boolean
    Dim styScheme As Word.Style
    Dim styHeading As Word.Style
    Dim xStyle As String
    Dim xProblemScheme As String
    Dim oRecs As CSchemeRecords
    Dim xMsg As String
    Dim rngSelection As Word.Range
    Dim bDeleteFinalPara As Boolean
    Dim lVScrolled As Long
    Dim xScheme As String
    
    bRelinkSchemes = True
    
    Application.ScreenUpdating = False
    
'   if it becomes necessary to create Heading L1-9 based on
'   Heading 1-9, we don't want to do it with a numbered paragraph
'   selected; so create and select a normal para at end of doc
    If iSchemeType = mpSchemeType_Document Then
        lVScrolled = ActiveWindow.VerticalPercentScrolled
        Set rngSelection = Selection.Range.Duplicate
        With Selection
            .Collapse wdCollapseStart
            If .Range.ListFormat.ListType <> wdListNoNumbering Then
                .EndKey wdStory
                .InsertParagraphAfter
                .EndOf
                .Style = wdStyleNormal
                bDeleteFinalPara = True
            End If
        End With
    End If
    
'   tag paragraphs with current styles
    If iSchemeType = mpSchemeType_Document Then
        TagNumberedParas
    End If
    
'   get scheme records
    Set oRecs = New CSchemeRecords

'   get source
    Set oSource = Source(iSchemeType)
    
'   check if Headings 1-9 are linked to a named list template
    bHSchemeIsUsed = (xHeadingScheme() <> "")
    
    For Each ltP In oSource.ListTemplates
        If ltP.Name = "" Then GoTo labNextScheme
        If bIsMPListTemplate(ltP) Then
            xScheme = xGetLTRoot(ltP.Name)
            
            If iSchemeType = mpSchemeType_Document Then
'               determine if the scheme to be linked
'               is using Word Heading styles
                bIsHScheme = bIsHeadingScheme(xScheme)
            End If
    
'           get levels
            iLevels = iGetLevels(xScheme, iSchemeType)
            
'           for each possible level...
            For i = 1 To 9
                If iSchemeType <> mpSchemeType_Document Then
                    If i > iLevels Then
'                       level does not exist in scheme - unlink
                        ltP.ListLevels(i).LinkedStyle = ""
                    Else
'                       link to proprietary styles
                        xStyle = xGetStyleRoot(xScheme) & "_L" & i
                        ltP.ListLevels(i).LinkedStyle = xStyle
                    End If
                ElseIf i > iLevels Then
'                   unused levels of document schemes will be unlinked
'                   in new function below
                    Exit For
                ElseIf Not bIsHScheme Then
'                   ensure existence of proprietary style
                    xStyle = xGetStyleRoot(xScheme) & "_L" & i
                                            
                    On Error Resume Next
                    Set styScheme = Nothing
                    Set styScheme = ActiveDocument.Styles(xStyle)
                    On Error GoTo 0
                        
                    If styScheme Is Nothing Then
                        If xScheme = "HeadingStyles" Then
                            If bHSchemeIsUsed Then
'                               create Heading_Lx style
                                Set styHeading = ActiveDocument.Styles(xTranslateHeadingStyle(i))
                                If g_bCreateUnlinkedStyles Then
                                    '9.9.4010
                                    Set styScheme = mdlCC.AddUnlinkedParagraphStyle(ActiveDocument, _
                                        xStyle)
                                Else
                                    Set styScheme = ActiveDocument.Styles.Add(xStyle)
                                End If
                                With styScheme
                                    .BaseStyle = styHeading.BaseStyle
                                    .NextParagraphStyle = styHeading.NextParagraphStyle
                                    .Font = styHeading.Font
                                    .ParagraphFormat = styHeading.ParagraphFormat
                                    If Not g_bIsXP Then
'                                       this property was "unimplemented" in the June 2001
'                                       release of Word XP
                                        .Borders = styHeading.Borders
                                    End If
                                End With
                            Else
'                               link to Headings 1-9
                                bIsHScheme = True
                                Exit For
                            End If
                        Else
'                           prompt for reset
                            If bPromptForReset Then
                                xProblemScheme = oRecs.GetField(xScheme, _
                                                mpRecField_Alias, _
                                                mpSchemeType_Document, _
                                                ActiveDocument)
                                If g_lUILanguage = wdFrenchCanadian Then
                                    xMsg = xProblemScheme & " un ou plusieurs �l�ments sont manquants dans ce th�me. D�sirez-vous tenter de r�initialiser au format par d�faut ?"
                                Else
                                    xMsg = xProblemScheme & " scheme is missing " & _
                                        "one or more components.  Would you like to " & _
                                        "attempt to reset it to its default format?"
                                End If
                                lRet = MsgBox(xMsg, vbQuestion + vbYesNo, g_xAppName)
                                If lRet = vbYes Then
                                    bRet = bRepairScheme(xScheme)
                                    If Not bRet Then _
                                        bRelinkSchemes = False
                                Else
                                    bRelinkSchemes = False
                                End If
                            Else
                                bRelinkSchemes = False
                            End If
                            GoTo labNextScheme
                        End If
                    End If
                End If
labNextLevel:
            Next i
            
'           use new function for relinking document schemes
            If iSchemeType = mpSchemeType_Document Then
                RelinkPreserveIndents ActiveDocument, ltP, xScheme, 1, iLevels, bIsHScheme
            End If
        End If
labNextScheme:
    Next ltP
    
'   clean up
    If iSchemeType = mpSchemeType_Document Then
        RestoreTaggedStyles
        rngSelection.Select
        If bDeleteFinalPara Then
            '9.9.2004 - next line modified for mp10 GLOG item 3749 - numbering
            'was getting removed from the last paragraph of an mSEG
            ActiveDocument.Paragraphs.Last.Range.Previous(wdCharacter).Delete
        End If
        ActiveWindow.VerticalPercentScrolled = lVScrolled
    End If
    
    Application.ScreenUpdating = True
    
End Function

#If False Then
Public Function EditPaste() As Long
'relinks styles to appropriate list level
'in active document and sets active scheme
    Dim ltP As Word.ListTemplate
    Dim ltLast As Word.ListTemplate
    Dim xName As String
    Dim i As Integer
    Dim j As Integer
    Dim xStyleRoot As String
    
'   cycle through all list templates in doc
    For Each ltP In Word.ActiveDocument.ListTemplates
        xName = xGetLTRoot(ltP.Name)
        xStyleRoot = xGetStyleRoot(xName)
'       test if the list template has a name -
        If bIsMPListTemplate(ltP) Then
            j = j + 1
'           relink all styles to scheme
            For i = 1 To 9
                On Error GoTo NextLT
                ltP.ListLevels(i).LinkedStyle = _
                    xStyleRoot & "_L" & i
            Next i
NextLT:
            Set ltLast = ltP
        End If
    Next ltP
    
'   set active scheme - prompt if necessary
    If j = 1 Then
'       set as active scheme
'       set scheme of active doc - use key so that
'       type of scheme is carried in var
        bSetSelectedScheme ActiveDocument, 3 & ltLast.Name
    ElseIf j > 1 Then
'       prompt to set active scheme
    
    End If
    Exit Function
End Function
#End If

Function bStyleNameIsValid(ByVal xName As String, _
        Optional bTestUniqueness As Boolean = False, _
        Optional bAlert As Boolean = True) As Boolean
'returns TRUE if xName is a valid scheme name
'criteria: 1) no spaces
'               2) <10 chars
'               3) only alphanumeric characters
'the 8 char limit prevents users from naming schemes
'any of the 3 default schemes (eg LegalDefault).
    Dim xChar As String
    Dim iChar As Integer
    Dim iSchemes As Integer
    Dim i As Integer
    Dim bIsUnicode As Boolean
    
'   trim mpPrefix if necessary
    If Left(xName, 4) = mpPrefix Then
        xName = Mid(xName, Len(mpPrefix) + 1)
    End If
    
    bStyleNameIsValid = True
    
'   test for empty scheme names
    If xName = "" Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Veuillez inscrire un nom de style pour ce th�me."
        Else
            xMsg = "Please provide a style name for the scheme."
        End If
        If bAlert Then _
            MsgBox xMsg, vbExclamation, g_xAppName
        bStyleNameIsValid = False
        Exit Function
    End If
    
'   test for reserved scheme names
    If UCase(xName) = "GENERIC" Or UCase(xName) = "HEADING" Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Nom de style invalide.  '" & xName & "' est un nom de style r�serv�."
        Else
            xMsg = "Invalid style name.  '" & xName & "' is a " & _
                            "reserved style name."
        End If
        If bAlert Then _
            MsgBox xMsg, vbExclamation, g_xAppName
        bStyleNameIsValid = False
        Exit Function
    End If
    
'   test for spaces in name
    If InStr(xName, " ") Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Nom de style invalide. Espaces sont non permises dans le nom de style.."
        Else
            xMsg = "Invalid style name.  Spaces are not " & _
                            "allowed in style names."
        End If
        If bAlert Then _
            MsgBox xMsg, vbExclamation, g_xAppName
        bStyleNameIsValid = False
        Exit Function
    End If
    
'   test for name length
    If Len(xName) > 10 Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Nom de style invalide. le nom de style est limit� � 10 caract�res alphanum�riques."
        Else
            xMsg = "Invalid style name.  Style names are " & _
                    "limited to 10 alphanumeric characters."
        End If
        If bAlert Then _
            MsgBox xMsg, vbExclamation, g_xAppName
        bStyleNameIsValid = False
        Exit Function
    End If
    
'   test for alphanumeric characters
    For i = 1 To Len(xName)
        xChar = LCase(Mid(xName, i, 1))
        iChar = Asc(xChar)
        bIsUnicode = (AscW(xChar) <> Asc(xChar))
        
'       must be numeric, or lowercase alpha
        If Not (IsNumeric(xChar) Or _
                (iChar >= 97 And iChar <= 122) Or _
                bIsUnicode) Then
            If g_lUILanguage = wdFrenchCanadian Then
                If xChar = "'" Then
                    xMsg = "Nom de style invalide. le nom de style d'apostrophes."
                Else
                    xMsg = "Nom de style invalide. le nom de style ne peut pas contenir de caractere '" & xChar & "'."
                End If
            Else
                If xChar = "'" Then
                    xMsg = "Invalid style name.  Style names " & _
                            "cannot contain apostrophes."
                Else
                    xMsg = "Invalid style name.  Style names cannot " & _
                            "contain the '" & xChar & "' character."
                End If
            End If
            
            If bAlert Then _
                MsgBox xMsg, vbExclamation, g_xAppName
            bStyleNameIsValid = False
            Exit Function
        End If
    Next i
    
'   test for uniqueness of name
    
'   do private schemes first
    iSchemes = UBound(g_xPSchemes)
    For i = 0 To iSchemes
        If UCase(g_xPSchemes(i, 0)) = UCase(mpPrefix & xName) Then
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "Un th�me priv� utilise d�j� ce nom de style.  Veuillez entrer un nom unique."
            Else
                xMsg = "A private scheme that uses this style name already " & _
                    "exists.  " & vbCr & "Please enter a unique style name."
            End If
            If bAlert Then _
                MsgBox xMsg, vbExclamation, g_xAppName
            bStyleNameIsValid = False
            Exit Function
        End If
    Next i

'   do public schemes
    iSchemes = UBound(g_xFSchemes)
    For i = 0 To iSchemes
        If UCase(g_xFSchemes(i, 0)) = "ZZMP" & UCase(xName) Then
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "Un th�me public utilise d�j� ce nom de style.  Veuillez entrer un nom unique."
            Else
                xMsg = "A public scheme that uses this style name already " & _
                    "exists.  " & vbCr & "Please enter a unique style name."
            End If
            If bAlert Then _
                MsgBox xMsg, vbExclamation, g_xAppName
            bStyleNameIsValid = False
            Exit Function
        End If
    Next i
End Function

Function bSchemeNameIsValid(xName As String, _
        Optional bTestUniqueness As Boolean = False, _
        Optional bAlert As Boolean = True, _
        Optional iSchemeType As mpSchemeTypes = mpSchemeType_Private) As Boolean
'returns TRUE if xName is a valid scheme name
    Dim xChar As String
    Dim iChar As Integer
    Dim iSchemes As Integer
    Dim i As Integer
    
    bSchemeNameIsValid = True
    
'   test for empty scheme names
    If xName = "" Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Veuillez entrer un nom pour ce th�me."
        Else
            xMsg = "Please provide a name for the scheme."
        End If
        If bAlert Then _
            MsgBox xMsg, vbExclamation, g_xAppName
        bSchemeNameIsValid = False
        Exit Function
    End If
    
'   test for reserved scheme names
    If (UCase(xName) = "GENERIC") Or (UCase(xName) = "HEADING") Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Nom de th�me invalide.  " & _
                   "'" & xName & "' est un mot r�serv�."
        Else
            xMsg = "Invalid scheme name.  " & _
                   "'" & xName & "' is a reserved word."
        End If
        If bAlert Then _
            MsgBox xMsg, vbExclamation, g_xAppName
        bSchemeNameIsValid = False
        Exit Function
    End If
    
'   prevent characters not allowed in file names
    If InStr(xName, "\") Or _
            InStr(xName, "=") Or _
            InStr(xName, ":") Or _
            InStr(xName, "*") Or _
            InStr(xName, "?") Or _
            InStr(xName, Chr(34)) Or _
            InStr(xName, "<") Or _
            InStr(xName, ">") Or _
            InStr(xName, "|") Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Nom de th�me invalide. Les noms ne peuvent pas contenir les caract�res suivants: \ : * ? " & Chr(34) & " < > | ="
        Else
            xMsg = "Invalid scheme name.  " & _
                   "Names cannot contain the following " & _
                   " characters: \ : * ? " & Chr(34) & " < > | ="
        End If
        If bAlert Then _
            MsgBox xMsg, vbExclamation, g_xAppName
        bSchemeNameIsValid = False
        Exit Function
    End If
    
'   test for name length - length is limited because
'   storage space for scheme info contains max 31 chars -
'   removed limit in 9.9.3
'    If Len(xName) > 15 Then
'        xMsg = "Invalid scheme name.  Scheme " & _
'               "names are limited to 15 characters."
'        If bAlert Then _
'            MsgBox xMsg, vbExclamation, g_xAppName
'        bSchemeNameIsValid = False
'        Exit Function
'    End If
    
'   test for uniqueness of name
    If iSchemeType = mpSchemeType_Private Then
'       test all public schemes
        iSchemes = UBound(g_xFSchemes)
        For i = 0 To iSchemes
            If UCase(g_xFSchemes(i, 1)) = UCase(xName) Then
                If g_lUILanguage = wdFrenchCanadian Then
                    xMsg = "Un th�me public utilise d�j� ce nom de style.  Veuillez entrer un nom unique. "
                Else
                    xMsg = "A public scheme with this name already " & _
                        "exists.  " & vbCr & "Please enter a unique name."
                End If
                If bAlert Then _
                    MsgBox xMsg, vbExclamation, g_xAppName
                bSchemeNameIsValid = False
                Exit Function
            End If
        Next i
        
'       test all personal schemes
        iSchemes = UBound(g_xPSchemes)
        For i = 0 To iSchemes
            If UCase(g_xPSchemes(i, 1)) = UCase(xName) Then
                If g_lUILanguage = wdFrenchCanadian Then
                    xMsg = "Un th�me priv� utilise d�j� ce nom de style.  Veuillez entrer un nom unique."
                Else
                    xMsg = "A private scheme with this name already " & _
                        "exists.  " & vbCr & "Please enter a unique name."
                End If
                If bAlert Then _
                    MsgBox xMsg, vbExclamation, g_xAppName
                bSchemeNameIsValid = False
                Exit Function
            End If
        Next i
    Else
        iSchemes = UBound(g_xDSchemes)
        For i = 0 To iSchemes
            If UCase(g_xDSchemes(i, 1)) = UCase(xName) Then
                If g_lUILanguage = wdFrenchCanadian Then
                    xMsg = "Un th�me de document utilise d�j� ce nom.  Veuillez entrer un nom unique."
                Else
                    xMsg = "A document scheme with this name already " & _
                        "exists.  " & vbCr & "Please enter a unique name."
                End If
                If bAlert Then _
                    MsgBox xMsg, vbExclamation, g_xAppName
                bSchemeNameIsValid = False
                Exit Function
            End If
        Next i
    End If
End Function

Function bAppGetLists() As Boolean
    Dim xValue As String
    Dim oFSO As FileSystemObject
    Dim oFolder As Scripting.Folder
    Dim oFile As Scripting.File
    Dim iCount As Integer
    Dim i As Integer
    
'   get next paragraph styles
    ReDim g_xNextParaStyles(0)
    xValue = xGetAppIni("Numbering", "NextParaStyles")
    If xValue <> "" Then
        xValue = xTrimTrailingChrs(xValue, "|", True, True)
        bStringToArray xValue, g_xNextParaStyles(), "|"
    End If
    
'   fill underline formats
    ReDim g_xUnderlineFormats(2, 1)
    If g_lUILanguage = wdFrenchCanadian Then
        g_xUnderlineFormats(0, 0) = "(aucun)"
        g_xUnderlineFormats(1, 0) = "Simple"
        g_xUnderlineFormats(2, 0) = "Mots seulement"
    Else
        g_xUnderlineFormats(0, 0) = "(none)"
        g_xUnderlineFormats(1, 0) = "Single"
        g_xUnderlineFormats(2, 0) = "Words only"
    End If
    g_xUnderlineFormats(0, 1) = "0"
    g_xUnderlineFormats(1, 1) = "1"
    g_xUnderlineFormats(2, 1) = "2"
    
'   get permitted fonts
    ReDim g_xPermittedFonts(0)
    xValue = xGetAppIni("Numbering", "PermittedFonts")
    If xValue <> "" Then
        xValue = xTrimTrailingChrs(xValue, "|", True, True)
        bStringToArray xValue, g_xPermittedFonts(), "|"
    End If
    
'   get pleading scheme list
    ReDim g_xPleadingSchemes(0)
    xValue = xGetAppIni("Numbering", "AdjustSpacingInOnly")
    If xValue <> "" Then
        xValue = xTrimTrailingChrs(xValue, "|", True, True)
        bStringToArray xValue, g_xPleadingSchemes(), "|"
    End If
    
'   get supplemental number styles
    ReDim g_xSupplementalNumberStyles(0)
    xValue = xGetAppIni("Numbering", "SupplementalListNumberStyles")
    If xValue <> "" Then
        xValue = xTrimTrailingChrs(xValue, "|", True, True)
        bStringToArray xValue, g_xSupplementalNumberStyles(), "|"
    End If
    
'   get help files (9.9.4006)
    Set oFSO = New FileSystemObject
    If oFSO.FolderExists(g_xHelpPath) Then
        Set oFolder = oFSO.GetFolder(g_xHelpPath)
        iCount = oFolder.Files.Count
        ReDim g_xHelpFiles(iCount - 1)
        For Each oFile In oFolder.Files
            g_xHelpFiles(i) = Mid$(oFile.Name, 1, InStr(oFile.Name, ".") - 1)
            i = i + 1
        Next oFile
    End If
End Function

Function iGetSchemeOrigin(xScheme As String) As mpSchemeTypes
'origin of document scheme is stored in last column doc prop;
'first need to locate correct doc prop
        
    iGetSchemeOrigin = GetField(xScheme, _
                                mpRecField_Origin, , _
                                ActiveDocument)
                                
End Function

Function xMatchFirmSchemeCase(xScheme As String) As String
'forces new schemes using public scheme name to also use same case;
'this will prevent errors caused by case sensitivity
'of doc props and list templates
    Dim iSchemes As Integer
    Dim i As Integer
    
    iSchemes = UBound(g_xFSchemes)
    For i = 0 To iSchemes
        If UCase(g_xFSchemes(i, 0)) = UCase(xScheme) Then
            If g_xFSchemes(i, 0) <> xScheme Then
                xMatchFirmSchemeCase = g_xFSchemes(i, 0)
                Exit Function
            End If
        End If
    Next i
    
    xMatchFirmSchemeCase = xScheme
End Function

Public Sub RefreshSchemesList(tvwSchemes As MSComctlLib.TreeView, _
                              Optional ByVal xSelScheme As String, _
                              Optional ByVal iSelSchemeType As mpSchemeTypes = mpSchemeType_Private, _
                              Optional ByVal bShowPersonalSchemes As Boolean = True)
'reloads all nodes of tree with items in the
'three schemes arrays. will use passed args to
'select a scheme
    Dim iNumSchemes As Integer
    Dim i As Integer
    Dim nodP As ComctlLib.Node
    Dim xName As String
    Dim bDocExpanded As Boolean
    Dim bPersonalExpanded As Boolean
    Dim bFirmExpanded As Boolean
    Dim xDocNode As String
    Dim xPrivateNode As String
    Dim xPublicNode As String
    Dim xFavoritesNode As String
    
    On Error GoTo ProcError
    EchoOff
    
    xDocNode = mpSchemeType_Category & mpDocumentSchemes
    xPrivateNode = mpSchemeType_Category & mpPersonalSchemes
    xPublicNode = mpSchemeType_Category & mpPublicSchemes
    xFavoritesNode = mpSchemeType_Category & mpFavoriteSchemes
    
'   load schemes list
    With tvwSchemes
'       get expanded state of category nodes
        If .Nodes.Count Then
            bDocExpanded = .Nodes(xDocNode).Expanded
            If bShowPersonalSchemes Then
                bPersonalExpanded = .Nodes(xPrivateNode).Expanded
            End If
            If Not g_bIsAdmin Then
                bFirmExpanded = .Nodes(xPublicNode).Expanded
            End If
        End If
        
'       remove all nodes
        .Nodes.Clear
        
'       add parent nodes - personal/document schemes categories
        If g_lUILanguage = wdFrenchCanadian Then
            .Nodes.Add , , xDocNode, mpDocumentSchemesFrench  ', "FolderClosed", "FolderOpen"
        Else
            .Nodes.Add , , xDocNode, mpDocumentSchemes  ', "FolderClosed", "FolderOpen"
        End If
        
        If g_bIsAdmin Then
            If g_lUILanguage = wdFrenchCanadian Then
                .Nodes.Add , , xPrivateNode, mpAdminSchemesFrench
            Else
                .Nodes.Add , , xPrivateNode, mpAdminSchemes
            End If
        Else
            '10/1/12 - add Favorites node
            If g_lUILanguage = wdFrenchCanadian Then
                .Nodes.Add , , xPublicNode, mpPublicSchemesFrench ', "FolderClosed", "FolderOpen"
                If bShowPersonalSchemes Then _
                    .Nodes.Add , , xPrivateNode, mpPersonalSchemesFrench ', "FolderClosed", "FolderOpen"
                .Nodes.Add , , xFavoritesNode, mpFavoriteSchemesFrench
                .Nodes(xFavoritesNode).Sorted = True
            Else
                .Nodes.Add , , xPublicNode, mpPublicSchemes ', "FolderClosed", "FolderOpen"
                If bShowPersonalSchemes Then _
                    .Nodes.Add , , xPrivateNode, mpPersonalSchemes ', "FolderClosed", "FolderOpen"
                .Nodes.Add , , xFavoritesNode, mpFavoriteSchemes
                .Nodes(xFavoritesNode).Sorted = True
            End If
        End If
        
'       if there are doc schemes, add child nodes for each
        On Error Resume Next
        xName = g_xDSchemes(0, 0)
        On Error GoTo 0
        If Len(xName) Then
            For i = LBound(g_xDSchemes) To UBound(g_xDSchemes)
                If g_xDSchemes(i, 1) <> "" Then
                    .Nodes.Add .Nodes(xDocNode).Index, _
                                      tvwChild, _
                                      g_xDSchemes(i, 2) & g_xDSchemes(i, 0), _
                                      g_xDSchemes(i, 1)
                End If
            Next i
        End If
            
'       add nodes for public schemes
        On Error Resume Next
        xName = Empty
        xName = g_xFSchemes(0, 0)
        On Error GoTo 0
        If xName <> "" Then
            For i = LBound(g_xFSchemes) To UBound(g_xFSchemes)
                .Nodes.Add .Nodes(xPublicNode).Index, _
                                      tvwChild, _
                                      g_xFSchemes(i, 2) & g_xFSchemes(i, 0), _
                                      g_xFSchemes(i, 1)
            Next i
        End If
        
        If bShowPersonalSchemes Then
'           add nodes for private schemes
            On Error Resume Next
            xName = Empty
            xName = g_xPSchemes(0, 0)
            On Error GoTo 0
            If xName <> "" Then
                For i = LBound(g_xPSchemes) To UBound(g_xPSchemes)
                    If g_xPSchemes(i, 0) <> "" Then
                        .Nodes.Add .Nodes(xPrivateNode).Index, _
                                              tvwChild, _
                                              g_xPSchemes(i, 2) & g_xPSchemes(i, 0), _
                                              g_xPSchemes(i, 1)
                    End If
                Next i
            End If
        End If
        
'       add nodes for favorites schemes
        Dim xFavorite As String
        Dim vProps As Variant
        Dim oNode As MSComctlLib.Node
        i = 1
        xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
        While xFavorite <> ""
            vProps = Split(xFavorite, "|")
            If UBound(vProps) = 2 Then
                'make sure scheme still exists
                On Error Resume Next
                If vProps(2) = CStr(mpFavoritePrivate) Then
                    Set oNode = .Nodes(CStr(mpSchemeType_Private) & vProps(0))
                Else
                    Set oNode = .Nodes(CStr(mpSchemeType_Public) & vProps(0))
                End If
                On Error GoTo ProcError
                
                If Not oNode Is Nothing Then
                    'add
                    .Nodes.Add .Nodes(xFavoritesNode).Index, tvwChild, _
                        vProps(2) & vProps(0), vProps(1)
                    Set oNode = Nothing
                    i = i + 1
                Else
                    'remove from ini, don't increment
                    DeleteFavoriteScheme vProps(0)
                End If
            End If
            xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
        Wend
        
'       return nodes to start for expanded property
        .Nodes(xDocNode).Expanded = bDocExpanded
        If bShowPersonalSchemes Then
            .Nodes(xPrivateNode).Expanded = bPersonalExpanded
        End If
        If Not g_bIsAdmin Then
            .Nodes(xPublicNode).Expanded = bFirmExpanded
        End If
        
        On Error Resume Next
'       select specified scheme, if passed
        If Len(xSelScheme) Then
            .Nodes(iSelSchemeType & xSelScheme).Selected = True
        End If
        
        .SetFocus
        On Error GoTo 0
        EchoOn
    End With
    Exit Sub
    
ProcError:
    EchoOn
    If g_lUILanguage = wdFrenchCanadian Then
        MsgBox "L'erreur suivsnte s'est produite lors de l'actualisation de la liste des th�mes: " & _
            vbCr & Err.Number & "::" & Err.Description & _
            "::" & "frmSchemes.RefreshSchemesList", vbCritical, g_xAppName
    Else
        MsgBox "The following error occurred while refreshing the Schemes List: " & _
            vbCr & Err.Number & "::" & Err.Description & _
            "::" & "frmSchemes.RefreshSchemesList", vbCritical, g_xAppName
    End If
    Exit Sub
End Sub

Function lRenameScheme(ByVal xScheme As String, _
                       ByVal xNewAlias As String, _
                       ByVal xOldAlias As String) As Long
'renames the alias of the scheme with ID xKey to xAlias
    Dim xOldBit As String
    Dim xNewBit As String
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #Else
        On Error GoTo 0
    #End If
    
'   change name in Scheme Record
    SetField xScheme, _
             mpRecField_Alias, _
             xNewAlias, _
             mpSchemeType_Private
             
'   rename the bitmap
    xOldBit = xSubstitute(xOldAlias, "/", "=")
    xNewBit = xSubstitute(xNewAlias, "/", "=")
    Name g_xMPBPath & "\" & xOldBit & ".mpb" As _
         g_xMPBPath & "\" & xNewBit & ".mpb"
            
'   refresh schemes
    iGetSchemes g_xPSchemes(), mpSchemeType_Private
            
'   save numbers.sty
    If g_iWordVersion >= mpWordVersion_2010 Then
        'workaround for Word 2010 to avoid error 5986 -
        '"this command is not available in an unsaved document"
        g_oPNumSty.Saved = False 'GLOG 5123 (10/3/12)
        g_oPNumSty.OpenAsDocument.Save
        ActiveDocument.Close False
    Else
        g_oPNumSty.Save
    End If
    
    Exit Function
    
ProcError:
    lRenameScheme = Err.Number
    Exit Function
End Function

Public Function UpdateSchemeArray(iType As mpSchemeTypes) As Long
'refills the appropriate array with current
'schemes - based on scheme type iType
     Select Case iType
        Case mpSchemeType_Public
            lRet = iGetSchemes(g_xFSchemes(), _
                    mpSchemeType_Public)
        Case mpSchemeType_Document
            lRet = iGetSchemes(g_xDSchemes(), _
                    mpSchemeType_Document)
        Case Else
            lRet = iGetSchemes(g_xPSchemes(), _
                    mpSchemeType_Private)
    End Select
    UpdateSchemeArray = lRet
End Function

Function bPositionIsValid(xText As String, _
                          Optional sMin As Single = 0) As Boolean
'validates all indent/position fields measured in inches
    If g_lUILanguage = wdFrenchCanadian Then
        xMsg = "La valeur en pouces doit �tre comprise entre " & _
                Trim(Str(sMin)) & " et " & mpMaxPositionalValue & "."
    Else
        xMsg = "The value for inches must be a number between " & _
                Trim(Str(sMin)) & " and " & mpMaxPositionalValue & "."
    End If
    
    If Len(xText) = 0 Then
        MsgBox xMsg, vbExclamation, g_xAppName
        Exit Function
    ElseIf Not IsNumeric(xText) Then
        MsgBox xMsg, vbExclamation, g_xAppName
        Exit Function
    ElseIf Not ((xText >= sMin) And _
        (xText <= mpMaxPositionalValue)) Then
        MsgBox xMsg, vbExclamation, g_xAppName
        Exit Function
    End If
    xMsg = Empty
    bPositionIsValid = True
End Function

Function bPointsAreValid(xText As String) As Boolean
'validates all spacing fields measured in points
    If (Not IsNumeric(xText)) Or _
            (xText < 0) Or xText > 1584 Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "La valeur par points doit �tre un nombre entre 0 et 1584.", vbExclamation, g_xAppName
        Else
            MsgBox "The value for points must be a number " & _
                "between 0 and 1584.", vbExclamation, g_xAppName
        End If
        Exit Function
    End If
    
    bPointsAreValid = True

End Function

Public Function bIsLinked(ByVal xScheme As String, _
        Optional ByVal iSchemeType As mpSchemeTypes = mpSchemeType_Private) As Boolean
'returns TRUE if all levels of
'scheme are linked to styles
    Dim iLevels As Integer
    Dim i As Integer
    Dim oSource As Object
    Dim ltP As Word.ListTemplate
    Dim xLT As String
    Dim xStyle As String
    
    iLevels = iGetLevels(xScheme, iSchemeType)
    If iSchemeType = mpSchemeType_Document Then
        xLT = xGetFullLTName(xScheme)
    Else
        xLT = xScheme
    End If
    
    Set oSource = Source(iSchemeType)
    Set ltP = oSource.ListTemplates(xLT)
    
    For i = 1 To iLevels
'        If Len(ltP.ListLevels(i).LinkedStyle) = 0 Then
        xStyle = xGetStyleName(xScheme, i)
        If ltP.ListLevels(i).LinkedStyle <> xStyle Then
            bIsLinked = False
            Exit Function
        End If
    Next i
    bIsLinked = True
End Function

Public Function ConvertLineSpace(ByVal iLineSpaceRule As WdLineSpacing, _
                                 ByVal sLineSpace As Single, _
                                 Optional sBase As Single) As Integer
'returns an integer representing line spacing rule-
'returns -1 if couldn't convert value to a rule
'value accepted by MacPac
    If iLineSpaceRule = wdLineSpaceAtLeast Or _
            iLineSpaceRule = wdLineSpaceMultiple Then
        ConvertLineSpace = -1
    ElseIf iLineSpaceRule = wdLineSpaceExactly Then
        If sBase = 0 Then
            If sLineSpace = 24 Then
                ConvertLineSpace = wdLineSpaceDouble
            ElseIf sLineSpace = 18 Then
                ConvertLineSpace = wdLineSpace1pt5
            ElseIf sLineSpace = 12 Then
                ConvertLineSpace = wdLineSpaceSingle
            Else
                ConvertLineSpace = -1
            End If
        Else
            If sLineSpace / sBase = 2 Then
                ConvertLineSpace = wdLineSpaceDouble
            ElseIf sLineSpace / sBase = 1.5 Then
                ConvertLineSpace = wdLineSpace1pt5
            ElseIf sLineSpace / sBase = 1 Then
                ConvertLineSpace = wdLineSpaceSingle
            Else
                ConvertLineSpace = -1
            End If
        End If
    Else
        ConvertLineSpace = iLineSpaceRule
    End If
End Function

Public Sub AdjustAlignmentToNormal(ByVal xScheme As String)
'implements "Base on Normal style" alignment option
    Dim iNormal As WdParagraphAlignment
    Dim xStyle As String
    Dim xStyleRoot As String
    Dim styCont As Word.Style
    Dim xCont As String
    Dim i As Integer
    Dim iNumLevels As Integer
    Dim iAlignment As Integer
    
    EchoOff
    
    iNormal = ActiveDocument.Styles(wdStyleNormal).ParagraphFormat.Alignment
    xStyleRoot = xGetStyleRoot(xScheme)
    iNumLevels = iGetLevels(xScheme, mpSchemeType_Document)
    
    For i = 1 To iNumLevels
        xStyle = xGetStyleName(xScheme, i)
        xCont = xStyleRoot & " Cont" & " " & i
        iAlignment = xGetLevelProp(xScheme, i, _
            mpNumLevelProp_TrailUnderline, mpSchemeType_Document)
            
        With ActiveDocument.Styles(xStyle).ParagraphFormat
            If bBitwisePropIsTrue(iAlignment, _
                    mpTrailUnderlineField_AdjustToNormal) Then
                .Alignment = iNormal
            End If
        End With
        
        On Error Resume Next
        Set styCont = ActiveDocument.Styles(xCont)
        On Error GoTo 0
        
        If Not styCont Is Nothing Then
            With styCont.ParagraphFormat
                If bBitwisePropIsTrue(iAlignment, _
                        mpTrailUnderlineField_AdjustContToNormal) Then
                    .Alignment = iNormal
                End If
            End With
        End If
    Next i
    
    EchoOn
End Sub

Public Sub ApplyModifiedScheme(ByVal xScheme As String)
'removes existing line break shift returns, then edits
'trailing char 11 for all paras of specified scheme
    Dim paraP As Paragraph
    Dim iListLevel As Integer
    Dim bReFormatTCHeading As Boolean
    Dim i As Integer
    Dim iNumParas As Integer
    Dim rngP As Word.Range
    Dim xCurLT As String
    Dim xStyleRoot As String
    Dim rngScope As Word.Range
    Dim llCur As Word.ListLevel
    Dim fldP As Word.Field
    Dim bReformat As Boolean
    Dim styPara As Word.Style
    Dim rngNextPara As Word.Range
    Dim xNextPara As String
    Dim bShowHidden As Boolean
    Dim bFound As Boolean
    Dim rngStart As Word.Range
    Dim bTrackChanges As Boolean
    
#If compHandleErrors Then
    On Error GoTo ProcError
#End If

    'GLOG 5461 (9.9.5012) - disable track changes for the entirety
    'of this method - it was previously only done in one specific place
    bTrackChanges = ActiveDocument.TrackRevisions
    If bTrackChanges Then _
        ActiveDocument.TrackRevisions = False

'   scope is entire doc
    Set rngScope = Word.ActiveDocument.Content
    
'   get style root
    xStyleRoot = xGetStyleRoot(xScheme)
    
'   get number of numbers in rngScope
    iNumParas = rngScope.ListParagraphs.Count
    
'   hidden paragraphs need to be shown in order to
'   return ListFormat.ListLevelNumber
    With ActiveWindow.View
        bShowHidden = .ShowHiddenText
        .ShowHiddenText = True
    End With
    
'   change Native Word numbers
    For Each paraP In rngScope.ListParagraphs
        With paraP
            Set styPara = Nothing
'           if we're looking for only certain numbered paras,
'           get current para list template
            xCurLT = ""
            On Error Resume Next
            xCurLT = xGetLTRoot(.Range.ListFormat.ListTemplate.Name)
            On Error GoTo 0
            
'           do only if we're looking at all paras or if the
'           current para is numbered with the list
'           template we're targeting
            If xScheme = xCurLT Then
                Set rngP = .Range
                With rngP
'                   get list level
                    With .ListFormat
                        Set llCur = .ListTemplate _
                            .ListLevels(.ListLevelNumber)
                    End With
                    
'                   do only if applied style is the style
'                   specified by the list template - this
'                   condition accounts for list templates that
'                   are linked to eg Heading1 and Heading 9 only -
'                   MacPac should only consider Heading 1 as part
'                   of the scheme, but would consider Heading 9
'                   as part if not for this condition
                    If llCur.LinkedStyle = paraP.Style And Right(llCur.LinkedStyle, 1) <> "x" Then
                        Set styPara = paraP.Style
                        
'                       move start past page or section break (8/28/01)
                        While Left(.Text, 1) = Chr(12)
                            .MoveStart
                        Wend
                        
'                       remove user-added shift-returns;
'                       if track changes is on, deletion won't work and
'                       code will loop forever
'                        bTrackChanges = ActiveDocument.TrackRevisions
'                        ActiveDocument.TrackRevisions = False
                        While Left(.Text, 1) = Chr(11)
                            'account for content control start tag at start of
                            'paragraph to avoid an endless loop (11/3/11)
                            If .Characters(1) = "" Then _
                                .MoveStart
                            .Characters(1).Delete
                        Wend
'                        ActiveDocument.TrackRevisions = bTrackChanges
                        
'                       add appropriate trailing chr
                        rngEditTrailChr11 rngP, llCur

                        If bReformat Then
'                           get tc entry
                            For Each fldP In paraP.Range.Fields
                                If fldP.Code = " " & g_xTCPrefix & " " Then
'                                   reformat heading
                                    rngFormatTCHeading fldP.Code, _
                                                       llCur.Index, _
                                                       True
                                    Exit For
                                End If
                            Next
                        End If
                    
'**************************************************************
'11/7/01 - removed following block - we decided to
'stop restyling existing subsequent paragraphs
'**************************************************************
'                        If InStr(styPara.NextParagraphStyle, xStyleRoot & "_L") = 0 Then
'                            On Error GoTo lblNextParagraph
'                            Set rngNextPara = .Duplicate
'                            With rngNextPara
''                               change only proprietary next para styles
'                                xNextPara = .Next(wdParagraph).Style
'                                While InStr(xNextPara, " Cont ") Or _
'                                        InStr(xNextPara, "_Para") Or _
'                                        xNextPara = "Num Continue" Or _
'                                        xNextPara = "Plead Continue"
'                                    .Next(wdParagraph).Style = _
'                                        styPara.NextParagraphStyle
'                                    .Move wdParagraph
'                                    xNextPara = .Next(wdParagraph).Style
'                                Wend
'                            End With
'                        End If

                    End If
                End With
            End If
        End With
        
lblNextParagraph:
'       update status
        i = i + 1
        EchoOn
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = "Changements en cours: " & _
                                    Int(i / iNumParas * 100) & _
                                    "% Complet�"
        Else
            Application.StatusBar = "Applying changes to document: " & _
                                    Int(i / iNumParas * 100) & _
                                    "% Complete"
        End If
        EchoOff
    Next paraP
    
    'GLOG 5461 - restore track changes
    If bTrackChanges Then _
        ActiveDocument.TrackRevisions = True
        
'   restore view option
    ActiveWindow.View.ShowHiddenText = bShowHidden
    
    EchoOn
    Screen.MousePointer = vbDefault
    Application.StatusBar = ""
    mdlApplication.SendShiftKey
    Exit Sub
ProcError:
    'GLOG 5461 - restore track changes
    If bTrackChanges Then _
        ActiveDocument.TrackRevisions = True
        
    EchoOn
    Screen.MousePointer = vbDefault
    Application.StatusBar = ""
    mdlApplication.SendShiftKey
    RaiseError "ApplyModifiedScheme"
    Exit Sub
End Sub

Function bLevelIsLinked(ByVal xScheme As String, ByVal iLevel As Integer) As Boolean
    Dim xStyle As String
    Dim xLT As String
    
    On Error Resume Next
    xStyle = xGetStyleName(xScheme, iLevel)
    xStyle = StripStyleAlias(xStyle)
    xLT = xGetFullLTName(xScheme)
    bLevelIsLinked = (ActiveDocument.ListTemplates(xLT) _
        .ListLevels(iLevel).LinkedStyle = xStyle)
End Function

Sub SavePNumSty()
'   save p num sty if necessary
    With g_oPNumSty
        If Not .Saved Then
            If g_iWordVersion >= mpWordVersion_2010 Then
                'workaround for Word 2010 to avoid error 5986 -
                '"this command is not available in an unsaved document"
                .OpenAsDocument.Save
                ActiveDocument.Close False
            Else
                .Save
            End If
        End If
    End With
    
'   flag admin utility to run cleanup code
    g_bAdminIsDirty = True
End Sub

Function xGetLastWordEditVer(docP As Word.Document) As String
'returns the version of Word that the
'specified doc was last edited in where
'MacPac Numbering 9.0 was installed
    Dim xMSWordVer As String
    On Error Resume Next
    xMSWordVer = docP _
        .Variables(mpLastEditMSWordVer)
    On Error GoTo 0
    xGetLastWordEditVer = xMSWordVer
End Function

Function bSetLastWordEditVer(docP As Word.Document) As Boolean
'stores the current version of Word
'in a doc var in the doc
    Dim xVer As String
    Dim iVer As Integer
    xVer = Word.Application.Version
    iVer = Left(xVer, InStr(xVer, ".") - 1)
    docP.Variables(mpLastEditMSWordVer) = iVer
End Function

Sub iCopyPropFromActiveDoc(ByVal xScheme As String, _
                           ByVal iLevel As Integer, _
                           ByVal xDest As String)
    Dim oProp As Office.DocumentProperty
    Dim oDest As Object
    Dim xProp As String
    Dim xValue As String
    Dim xDestValue As String
    
    If xDest = g_xPNumSty Then
        Set oDest = g_oPNumSty
    Else
        Set oDest = Word.Application.Documents(xDest)
    End If
    
    xProp = xScheme & iLevel
    
'   get value from active doc doc prop
    On Error Resume Next
    xValue = Word.ActiveDocument _
        .CustomDocumentProperties(xProp)
    On Error GoTo 0
    
'   copy docProps if they exist
    If xValue <> Empty Then
        With oDest.CustomDocumentProperties
            On Error Resume Next
            xDestValue = .Item(xProp).Value
            On Error GoTo 0
            If xDestValue = Empty Then
'               prop doesn't exist - create
                .Add xProp, False, msoPropertyTypeString, xValue, False
            Else
'               change value of existing prop
                .Item(xProp).Value = xValue
            End If
        End With
    End If
End Sub
Sub ShowSchemesDialogs()
    Dim bShowDialog As Boolean
    Dim xAlias As String
    Static xScheme As String
    Static iSchemeType As mpSchemeTypes
    Dim lView As Long
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    Dim bShowAll As Boolean
    Dim bShowTabs As Boolean
    Dim bShowSpaces As Boolean
    Dim bShowParagraphs As Boolean
        
    On Error GoTo ProcError
        
    If Not g_dlgScheme Is Nothing Then _
        Set g_dlgScheme = Nothing
        
    bShowDialog = True
    g_bPreventDocChangeEvent = True
    
    'GLOG 5628 - preserve show formatting settings in Word 2013/2016
    If g_iWordVersion >= mpWordVersion_2013 Then
        With ActiveWindow.View
            bShowAll = .ShowAll
            bShowTabs = .ShowTabs
            bShowSpaces = .ShowSpaces
            bShowParagraphs = .ShowParagraphs
        End With
    End If
    
    'force accelerator cues in Word 2013
    If g_iWordVersion > mpWordVersion_2010 Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If
    
    While bShowDialog
        Select Case g_iSchemeEditMode
            Case mpModeSchemeMain
                If g_dlgScheme Is Nothing Then
                    If g_lUILanguage = wdFrenchCanadian Then
                        Set g_dlgScheme = New frmSchemesFrench
                    Else
                        Set g_dlgScheme = New frmSchemes
                    End If
                End If
                
                With g_dlgScheme
ShowForm:
                    .Show vbModal
                    
                    If .SavePrivateStyFile Then
                        'GLOG 5539 (9.9.6006)
                        .SavePrivateStyFile = False
                        SavePNumSty
                        GoTo ShowForm
                    Else
                        g_iSchemeEditMode = .m_iEditMode
                    End If
                End With
            Case mpModeSchemeNew
                With g_oCurScheme
                    xAlias = .Alias
                    xScheme = .Name
                    iSchemeType = .SchemeType
                End With
                
                If bNewScheme(xScheme, iSchemeType) Then
                    Unload g_dlgScheme
                    Set g_dlgScheme = Nothing
                    g_iSchemeEditMode = mpModeSchemeNewB
                    
                    'GLOG 5160 - option to bypass timer
                    If g_xTimerDelay <> "0" Then
                        frmTimer.Show
                    Else
                        ShowSchemesDialogs
                    End If
                    
                    'turn off accelerator cues if necessary
                    If bRestoreNoCues Then _
                        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
                    Exit Sub
                Else
                    g_iSchemeEditMode = mpModeSchemeMain
                End If
            Case mpModeSchemeNewB
                With g_oCurScheme
                    xAlias = .Alias
                    xScheme = .Name
                    iSchemeType = .SchemeType
                End With
                g_docPreview.ActiveWindow.Activate
                bNewSchemeB
                
                If g_lUILanguage = wdFrenchCanadian Then
                    Set g_dlgScheme = New frmSchemesFrench
                Else
                    Set g_dlgScheme = New frmSchemes
                End If
                
                Load g_dlgScheme
                RefreshSchemesList g_dlgScheme.tvwSchemes, _
                    xScheme, _
                    iSchemeType
                g_iSchemeEditMode = mpModeSchemeMain
            Case mpModeSchemeEdit
                With g_oCurScheme
                    xAlias = .Alias
                    xScheme = .Name
                    iSchemeType = .SchemeType
                End With
                
                If bSchemeExists(xScheme, xAlias, mpSchemeType_Document) Then
                    g_bCurSchemeIsHeading = bIsHeadingScheme(xScheme)
                Else
                    g_bCurSchemeIsHeading = False
                End If
                
                If bEditScheme(xScheme, iSchemeType) Then
                    Unload g_dlgScheme
                    Set g_dlgScheme = Nothing
                    g_iSchemeEditMode = mpModeSchemeEditB
                    
                    'GLOG 5160 - option to bypass timer
                    If g_xTimerDelay <> "0" Then
                        frmTimer.Show
                    Else
                        ShowSchemesDialogs
                    End If
                    
                    'turn off accelerator cues if necessary
                    If bRestoreNoCues Then _
                        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
                    Exit Sub
                Else
                    g_iSchemeEditMode = mpModeSchemeMain
                End If
            Case mpModeSchemeEditB
                g_docPreview.ActiveWindow.Activate
                bEditSchemeB g_lRulerDisplayDelay_Edit, False '9.9.6012
                                
                If g_lUILanguage = wdFrenchCanadian Then
                    Set g_dlgScheme = New frmSchemesFrench
                Else
                    Set g_dlgScheme = New frmSchemes
                End If
                
                Load g_dlgScheme
                RefreshSchemesList g_dlgScheme.tvwSchemes, _
                    xScheme, _
                    iSchemeType, _
                    g_bShowPersonalSchemes
                g_iSchemeEditMode = mpModeSchemeMain
            Case Else
                g_iSchemeEditMode = mpModeSchemeDone
                bShowDialog = False
        End Select
    Wend
    Unload g_dlgScheme
    Set g_dlgScheme = Nothing
    
'   schemes dialog was launched as admin
    If g_bIsAdmin Then
        If g_bAdminIsDirty Then
'           ensure that admin schemes are marked as public
            bMarkAdminSchemesAsPublic
        
'           relink schemes - twice is sometimes necessary
            Screen.MousePointer = vbHourglass
            If g_lUILanguage = wdFrenchCanadian Then
                Application.StatusBar = "R�tabli lien des th�mes. Veuillez patienter..."
            Else
                Application.StatusBar = "Resetting scheme links.  Please wait..."
            End If
            bRelinkSchemes mpSchemeType_Private
            bRelinkSchemes mpSchemeType_Private
            Application.StatusBar = ""
            Screen.MousePointer = vbDefault
            
'           save sty file
            If g_iWordVersion >= mpWordVersion_2010 Then
                'workaround for Word 2010 to avoid error 5986 -
                '"this command is not available in an unsaved document"
                g_oPNumSty.OpenAsDocument.Save
                ActiveDocument.Close False
            Else
                g_oPNumSty.Save
            End If
        End If
        
'       reinitialize for user mode
        WorkAsUser
    Else
        UnLoadStyFiles
    End If
    
'   unload mpTOC.sty - removed in 9.8.1007
'    If g_bAllowTOCLink Then
'        UnloadTOCSty
'    End If
    
'   reset style area width - will only stick if set in Normal view
    '9.9.6008 (GLOG 5581) - this is no longer true and switching the
    'view is causing a refresh issue in Word 2016
    With Word.ActiveWindow
        If g_iWordVersion < mpWordVersion_2013 Then
            lView = .View.Type
            .View.Type = wdNormalView
        Else
            'GLOG 5628 - restore show formatting settings - switching
            'the view was previously taking care of this
            .View.ShowAll = bShowAll
            .View.ShowTabs = bShowTabs
            .View.ShowSpaces = bShowSpaces
            .View.ShowParagraphs = bShowParagraphs
        End If
        .StyleAreaWidth = g_sStyleArea
        If g_iWordVersion < mpWordVersion_2013 Then
            .View.Type = lView
        End If
    End With
   
    Word.Application.Activate
    g_bPreventDocChangeEvent = False
    mdlApplication.SendShiftKey
            
    'restore mp10 event handling and request refresh if necessary
    If g_bIsMP10 Then
        Word.Application.Run "zzmpResumeXMLEventHandling"
        If g_bRefreshTaskPane Then
            g_bRefreshTaskPane = False
            Word.Application.Run "zzmpRefreshTaskPane"
        End If
    End If
    
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    
    Exit Sub
ProcError:
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    RaiseError Err.Source
    Exit Sub
End Sub

Public Function bListTemplateIsMissing(docDoc As Word.Document, _
                                        xScheme As String) As Boolean
'checks for "dead" MacPac numbering, where styles are in document
'but list template is missing - one scenario is a Word 97 document
'saved in an earlier version and now completing the round trip
    Dim styScheme As Word.Style
    
    On Error Resume Next
    
    With docDoc
        Set styScheme = .Styles(xGetStyleRoot(xScheme) & "_L1")
        If Not (styScheme Is Nothing) Then _
            bListTemplateIsMissing = Not bListTemplateExists(xScheme)
    End With
End Function

Public Function bRepairScheme(xScheme As String, _
                                Optional iOrigin As mpSchemeTypes = mpSchemeType_Private) _
                                As Boolean
'looks for scheme in private and/or public sty files and resets
'if possible
    Dim bOriginFound As Boolean
    Dim tmpSource As Word.Template
    
    Set tmpSource = Source(iOrigin)
    
'   check for scheme in origin mpNumbers.sty;
'   user may have deleted it or it may have
'   come from another user's mpNumbers.sty
    On Error Resume Next
    If iOrigin = mpSchemeType_Private Then
        bOriginFound = bIsMPListTemplate( _
                tmpSource.ListTemplates(xScheme))
    End If
    
'   if not found in personal or if firm is the origin,
'   check for scheme in firm
    If Not bOriginFound Then
        iOrigin = mpSchemeType_Public
        bOriginFound = bIsMPListTemplate(g_oFNumSty.ListTemplates(xScheme))
    End If
    
'   if still not found, alert user and exit
    If Not bOriginFound Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "Impossible de r�initialiser le th�me selectionn�e. Aucun th�me personnel ou public ne contient se nom.", _
                vbInformation, g_xAppName
        Else
            MsgBox "Could not reset selected scheme.  No " & _
                "personal or public scheme with this name exists.", _
                vbInformation, g_xAppName
        End If
        Exit Function
    End If
    
'   reload and relink scheme
    iLoadScheme ActiveDocument, xScheme, iOrigin
'    If bSchemeIsUnlinked(xScheme) Then
        bRelinkScheme xScheme, mpSchemeType_Document, False, False, False
'    End If
    BackupProps xScheme
    
    bRepairScheme = True
End Function

Public Function bMarkAdminSchemesAsPublic() As Boolean
    Dim ltScheme As Word.ListTemplate
    
    If Not g_bIsAdmin Then
        Exit Function
    ElseIf Dir(g_xPNumSty) = "" Then
        Exit Function
    End If
    
    With g_oPNumSty
        For Each ltScheme In .ListTemplates
            If bIsMPListTemplate(ltScheme) Then
                SetField ltScheme.Name, _
                            mpRecField_Origin, _
                            mpSchemeType_Public, _
                            mpSchemeType_Private
            End If
        Next ltScheme
'        .Save
    End With
    
    bMarkAdminSchemesAsPublic = True

End Function

Public Function bPrepareForSchemesDialog() As Boolean
    Dim iDocSchemes As Integer
    Dim i As Integer
    Dim xMsg As String
    Dim xScheme As String
    
'   test for user's desire to proceed
'   despite formerly active 8.0 scheme
'   with missing list template
    If Not bRepairActive80Scheme Then _
        Exit Function
    
'   get style area width before turning off for preview
    g_sStyleArea = Word.ActiveWindow.StyleAreaWidth
    
'***************************************************************
'NOTE - Before restoring remmed out portions of below code, decide:
'   1. whether to offer repair or just notify
'   2. what to do if user doesn't want to repair - unname lt?
'       delete scheme? prompt every time schemes dlg is relaunched?
'   3. what to do if can't repair - offer to load generic set of props?
'***************************************************************
'   remove duplicate doc schemes
    lRemoveDuplicateLTs
    
'   check for document schemes
    iDocSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)

    If iDocSchemes > 0 Then
'       relink document schemes
        xScheme = xActiveScheme(ActiveDocument)
        If xScheme <> "" Then
            If bSchemeIsUnlinked(xScheme) Then
                If g_bPromptBeforeAutoRelinking Then
                    If g_lUILanguage = wdFrenchCanadian Then
                        xMsg = "Ce document contient des styles de num�rotation qui sont dissoci�s.  Si un formatage direct s'applique � des titres num�rot�s, il est possible qu'il soit supprim� lorsque ces styles seront reli�s. D�sirez-vous continuer ?"
                    Else
                        xMsg = "There are unlinked Numbering schemes in this document.  " & _
                            "If there is direct formatting applied to numbered headings, " & _
                            "it�s possible this formatting could be removed when the " & _
                            "schemes are relinked.  Do you wish to continue?"
                    End If
                    lRet = MsgBox(xMsg, vbQuestion + vbYesNo, g_xAppName)
                    If lRet = vbNo Then _
                        Exit Function
                End If
                
                bRet = bRelinkSchemes(mpSchemeType_Document)
            End If
        End If
        
''       ensure that fonts are clean
'        For i = 0 To iDocSchemes - 1
'            Set ltScheme = ActiveDocument _
'                .ListTemplates(xGetFullLTName(g_xDSchemes(i, 0)))
'            ResetLLFontsToThemselves ltScheme
'        Next i
'
'       user may have used Word interface to modify
'       list level font, deleting scheme props
'        RestoreProps
    
''       ensure existence of scheme props
'        For i = 0 To iDocSchemes - 1
'            If g_xDSchemes(i, 1) = "" Then
'                xMsg = xGetStyleRoot(g_xDSchemes(i, 0)) & _
'                    " scheme is missing " & _
'                    "one or more components.  Would you like to " & _
'                    "attempt to reset it to its default format?"
'                lRet = MsgBox(xMsg, vbQuestion + vbYesNo, g_xAppName)
'                If lRet = vbYes Then
'                    bRet = bRepairScheme(g_xDSchemes(i, 0))
'                End If
'            End If
'        Next i
    End If
    
    bPrepareForSchemesDialog = True
End Function

Public Function ResetLLFontsToThemselves(ltScheme As Word.ListTemplate) As Long
'   gets rid of any residual font substitution
    Dim i As Integer
    Dim llFont As New Word.Font
    Dim iLevels As Integer
    Dim xTime As String
    
'    xTime = "Start = " & Now & vbCr
    On Error Resume Next
    iLevels = ltScheme.ListLevels.Count
    For i = 1 To iLevels
        With ltScheme.ListLevels(i).Font
'            Set llFont = .Duplicate
            llFont.Bold = .Bold
            llFont.Italic = .Italic
            llFont.AllCaps = .AllCaps
            llFont.Underline = .Underline
            llFont.ColorIndex = .ColorIndex
            llFont.Strikethrough = .Strikethrough
            llFont.Subscript = .Subscript
            llFont.Superscript = .Superscript
            llFont.Shadow = .Shadow
            llFont.Outline = .Outline
            llFont.Emboss = .Emboss
            llFont.Engrave = .Engrave
            llFont.Hidden = .Hidden
            llFont.DoubleStrikeThrough = .DoubleStrikeThrough
            llFont.Size = .Size
            llFont.Animation = .Animation
            llFont.Name = .Name

            .Reset
            
            .Bold = llFont.Bold
            .Italic = llFont.Italic
            .AllCaps = llFont.AllCaps
            .Underline = llFont.Underline
            .ColorIndex = llFont.ColorIndex
            .Strikethrough = llFont.Strikethrough
            .Subscript = llFont.Subscript
            .Superscript = llFont.Superscript
            .Shadow = llFont.Shadow
            .Outline = llFont.Outline
            .Emboss = llFont.Emboss
            .Engrave = llFont.Engrave
            .Hidden = llFont.Hidden
            .DoubleStrikeThrough = llFont.DoubleStrikeThrough
            .Size = llFont.Size
            .Animation = llFont.Animation
            .Name = llFont.Name
        End With
    Next i
'    xTime = xTime & "End = " & Now
'    MsgBox xTime
End Function

Public Function bParaHasSolidDirect(paraP As Word.Paragraph) As Boolean
'returns TRUE if a direct font format is applied to entire paragraph
    Dim styFont As Word.Font
    Dim paraFont As Word.Font
    
    Set paraFont = paraP.Range.Font
    Set styFont = ActiveDocument.Styles(paraP.Style).Font
    
    With paraFont
        If .Bold + .Italic + .Underline + _
                .AllCaps + .SmallCaps > wdUndefined - 5 Then
            Exit Function
        End If
        If (.Bold <> styFont.Bold) Or _
                (.Italic <> styFont.Italic) Or _
                (.AllCaps <> styFont.AllCaps) Or _
                (.SmallCaps <> styFont.AllCaps) Or _
                (.Underline <> styFont.Underline) Then
            bParaHasSolidDirect = True
        End If
    End With
End Function

Function bSchemeIsApplied(xScheme As String) As Boolean
    Dim iLevels As Integer
    Dim i As Integer
    Dim xStyle As String
    
    On Error GoTo ProcError
    
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    
    For i = 1 To iLevels
        xStyle = xGetStyleName(xScheme, i)
        With ActiveDocument.Content.Find
            .ClearFormatting
            .Format = True
            .Wrap = wdFindContinue
            .Style = xStyle
            .Execute
            If .Found Then
                bSchemeIsApplied = True
                Exit Function
            End If
        End With
    Next i
    
ProcError:
End Function

Public Function bLTExistsIgnoreCasing(xScheme As String) As Boolean
'this is a patch until we can completely review
'how we handle styles and/or list templates with
'identical names, differently cased; in the meantime,
'it seems too risky to change the tests for scheme
'matches in low-level functions
    Dim ltScheme As Word.ListTemplate
    
    For Each ltScheme In ActiveDocument.ListTemplates
        If ltScheme.Name <> "" Then
            If UCase(xGetLTRoot(ltScheme.Name)) = UCase(xScheme) Then
                bLTExistsIgnoreCasing = True
                Exit Function
            End If
        End If
    Next ltScheme
End Function

Public Function lSynchIndents(xScheme As String) As Long
    Dim ltP As Word.ListTemplate
    Dim i As Integer
    Dim iLevels As Integer
    Dim bIsHScheme As Boolean
    Dim xStyleRoot As String
    Dim styLevel As Word.Style
    Dim sPara As Single
    Dim xLT As String
    
    On Error GoTo ProcError
    
    xLT = xGetFullLTName(xScheme)
    Set ltP = ActiveDocument.ListTemplates(xLT)
    xStyleRoot = xGetStyleRoot(xScheme)
    bIsHScheme = bIsHeadingScheme(xScheme)
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    
    For i = 1 To iLevels
        On Error Resume Next
        If bIsHScheme Then
            Set styLevel = ActiveDocument _
                .Styles(xTranslateHeadingStyle(i))
        Else
            Set styLevel = ActiveDocument _
                .Styles(xStyleRoot & "_L" & i)
        End If
        On Error GoTo ProcError
        
        If Not styLevel Is Nothing Then
            With ltP.ListLevels(i)
'               left indent - text position
                sPara = styLevel.ParagraphFormat.LeftIndent
                If .TextPosition <> sPara Then _
                    .TextPosition = sPara
'               first line indent - number position
                With styLevel.ParagraphFormat
                    sPara = .LeftIndent + .FirstLineIndent
                End With
                If .NumberPosition <> sPara Then _
                    .NumberPosition = sPara
            End With
        End If
    Next i
    Exit Function
    
ProcError:
    RaiseError "mdlN90.lSynchIndents"
    Exit Function
End Function

Public Function lRemoveDuplicateLTs() As Long
'prevents code from attempting to load
'duplicate doc schemes on to the tree;
'most of this is also done in doc change conversion,
'but until that code is reworked to be
'independent of doc vars, double check here
    Dim ltP As Word.ListTemplate
    Dim xLT As String
    Dim xLTs(9) As String
    Dim iCount As Integer
    Dim i As Integer
    Dim iStartProps As Integer
    Dim iPos As Integer
    
    On Error GoTo ProcError
    
    For Each ltP In ActiveDocument.ListTemplates
        If ltP.Name = "" Then GoTo labNextScheme
        If bIsMPListTemplate(ltP) Then
            xLT = ltP.Name
            iStartProps = InStr(xLT, "||")
            If ltP.ListLevels.Count <> 9 Then
'               list template has less than nine levels
                ltP.Name = ""
            ElseIf xGetFullLTName(xLT) <> xLT Then
'               remove old 9.0 list template for which there
'               is already a piped version
                ltP.Name = ""
            ElseIf iStartProps = 0 Then
'               this is an old 9.0 list template - preserve,
'               but exit before remaining branches
                GoTo labNextScheme
            ElseIf (InStr(xLT, "|") <> 0) And _
                    (Right(xLT, 1) <> "|") Then
'               remove list templates with characters after last pipe
                ltP.Name = ""
            ElseIf Mid(xLT, iStartProps, 6) = "||mpNA" Then
'               remove 8.0 scheme converted to 9.0 despite missing
'               level one style - conversion code used to permit this
                ltP.Name = ""
            Else
'               used to check for literally identical names,
'               which, believe it or not, is possible;
'               now checking for identical scheme names, since
'               this is sufficient to create problems;
'               limit to 10 for speed reasons
                If iCount < 10 Then
                    For i = 0 To iCount - 1
                        iPos = InStr(xLTs(i), "||")
                        If (iStartProps > 0) And (iPos > 0) Then
                            If UCase(Left(xLT, iStartProps - 1)) = _
                                    UCase(Left(xLTs(i), iPos - 1)) Then
                                ltP.Name = ""
                                GoTo labNextScheme
                            End If
                        End If
                    Next i
                    xLTs(iCount) = xLT
                    iCount = iCount + 1
                End If
            End If
        End If
labNextScheme:
    Next ltP
    
    Exit Function
    
ProcError:
    RaiseError "mdlN90.lRemoveDuplicateLTs"
    Exit Function
End Function

Private Sub ResetListNumberFonts()
    Dim i As Integer
    Dim xLT As String
    
    On Error Resume Next
    
    Application.ScreenUpdating = False
    
'   ensure that preview is active;
'   this is inconsistent in Word 2000
    g_docPreview.ActiveWindow.Activate
    
'   the following should never occur if the preview is active,
'   but just in case, exit rather than crash
    If Not bListTemplateExists(g_oCurScheme.Name) Then _
        Exit Sub
    
    xLT = xGetFullLTName(g_oCurScheme.Name)
    With ActiveDocument.ListTemplates(xLT)
        For i = 1 To 9
            .ListLevels(i).Font.Size = g_sNumberFontSizes(i - 1)
        Next i
    End With
    
    Application.ScreenUpdating = True
    Application.ScreenRefresh
End Sub

Sub TagNumberedParas()
    Dim paraP As Word.Paragraph
    Dim rngLocation As Word.Range
    Dim i As Integer
    Dim iParas As Integer
    Dim bShowAll As Boolean
    Dim lShowTags As Long
    Dim lEnd As Long
    Dim iTags As Integer
    
'   ensure that hidden paragraph marks are showing
    With ActiveWindow.View
        bShowAll = .ShowAll
        .ShowAll = True
    End With
    
    'GLOG 3749 - show xml tags
    If g_bXMLSupport Then _
        lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, True)
    
    iParas = ActiveDocument.ListParagraphs.Count
    
    For Each paraP In ActiveDocument.Content.ListParagraphs
        Set rngLocation = paraP.Range
        With rngLocation
            .MoveEnd wdCharacter, -1
            
            If g_bXMLSupport Then
                'GLOG 3749 - adjust for XML tags
                lEnd = mdlWordXML.GetTagSafeParagraphEnd(rngLocation, iTags)
                '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                If (g_iWordVersion >= mpWordVersion_2007) And (iTags = 0) Then _
                    lEnd = mdlCC.GetCCSafeParagraphEnd(rngLocation)
                .SetRange .Start, lEnd
            End If
            
            If Not .Style Is Nothing Then 'GLOG 2847
                .InsertAfter "|" & .Style & mpTag
            End If
        End With
        
'       update status - % complete
        i = i + 1
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = _
                "R�tabli lien num�rotation hi�rarchis�e: " & _
                    Format(i / iParas, "0%")
        Else
            Application.StatusBar = _
                "Relinking outline numbering: " & _
                    Format(i / iParas, "0%")
        End If
    Next paraP
    
            
    'GLOG 3749 - rehide tags if necessary
    If g_bXMLSupport And lShowTags Then _
        mdlWordXML.SetXMLMarkupState ActiveDocument, False

'   restore view
    ActiveWindow.View.ShowAll = bShowAll
End Sub

Sub RestoreTaggedStyles()
    Dim paraP As Word.Paragraph
    Dim rngLocation As Word.Range
    Dim i As Integer
    Dim iParas As Integer
    Dim bShowAll As Boolean
    Dim lShowTags As Long
    Dim lEnd As Long
    Dim iTags As Integer
    
'   ensure that hidden paragraph marks are showing
    With ActiveWindow.View
        bShowAll = .ShowAll
        .ShowAll = True
    End With
        
    'GLOG 3749 - show xml tags
    If g_bXMLSupport Then _
        lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, True)
    
    iParas = ActiveDocument.Paragraphs.Count
    
    For Each paraP In ActiveDocument.Content.Paragraphs
        Set rngLocation = paraP.Range
        With rngLocation
            If InStr(.Text, mpTag) Then
                If g_bXMLSupport Then
                    'GLOG 3749 - adjust for XML tags
                    lEnd = mdlWordXML.GetTagSafeParagraphEnd(rngLocation, iTags) + 1
                    If (g_iWordVersion >= mpWordVersion_2007) And (iTags = 0) Then
                        '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                        lEnd = mdlCC.GetCCSafeParagraphEnd(rngLocation) + 1
                    End If
                    .SetRange lEnd, lEnd
                Else
                    .EndOf
                End If
                
                If (.End = ActiveDocument.Paragraphs _
                        .Last.Range.End - 1) And _
                        (.Paragraphs(1).Range <> vbCr) Then
                    .Move wdCharacter, -Len(mpTag)
                Else
                    .Move wdCharacter, -(Len(mpTag) + 1)
                End If
                .MoveStartUntil "|", wdBackward
                'GLOG 2909/5018 (10/31/11) - only apply style if not already applied,
                'so as not to lose paragraph-wide direct formatting unnecessarily
                If .Style <> .Text Then _
                    .Style = .Text
                .MoveStart wdCharacter, -1
                .MoveEnd wdCharacter, Len(mpTag)
                .Delete
            End If
        End With
        
'       update status - % complete
        i = i + 1
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = _
                "R�tabli lien num�rotation hi�rarchis�e: " & _
                    Format(i / iParas, "0%")
        Else
            Application.StatusBar = _
                "Relinking outline numbering: " & _
                    Format(i / iParas, "0%")
        End If
    Next paraP

    'GLOG 3749 - rehide tags if necessary
    If g_bXMLSupport And lShowTags Then _
        mdlWordXML.SetXMLMarkupState ActiveDocument, False

'   restore view
    ActiveWindow.View.ShowAll = bShowAll
End Sub

Public Function bSchemeIsUnlinked(xScheme As String) As Boolean
    Dim xLT As String
    Dim iLevels As Integer
    Dim i As Integer
    
'   get list template
    xLT = xGetFullLTName(xScheme)
    If xLT = xScheme Then
'       list template is missing
        Exit Function
    End If
    
'   get number of levels
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    
'   check whether each level is linked
    For i = 1 To iLevels
        If Not bLevelIsLinked(xScheme, i) Then
            bSchemeIsUnlinked = True
            Exit Function
        End If
    Next i
End Function

Public Function UseSchemeExternal(xScheme As String, _
                                  bReset As Boolean, _
                                  bAllowAutoInsertion As Boolean) As Long
    Dim bIsDocScheme As Boolean
    Dim bUseWordHeadings As Boolean
    Dim xType As String
    
'   get whether using heading styles
    On Error Resume Next
    bUseWordHeadings = xGetAppIni("Numbering", _
                                    "UseWordHeadingStyles")
    On Error GoTo ProcError
    
    If xScheme = "" Then
        'get default scheme
        If bDefaultSchemeExists Then
            xScheme = xGetUserIni("Numbering", "DefaultScheme")
            xType = xGetUserIni("Numbering", "DefaultSchemeType")
        Else
            xScheme = g_xFSchemes(0, 0)
            xType = mpSchemeType_Public
        End If
    Else
        'get specified scheme
        If Left(xScheme, 4) <> "zzmp" Then
            xScheme = "zzmp" & xScheme
        End If
    
        If IsMacPacScheme(xScheme, mpSchemeType_Public) Then
            xType = mpSchemeType_Public
        ElseIf IsMacPacScheme(xScheme, mpSchemeType_Private) Then
            xType = mpSchemeType_Private
        End If
    End If
    
    bIsDocScheme = IsMacPacScheme(xScheme, mpSchemeType_Document)
        
    If (Not bIsDocScheme) And (xType = "") Then
        UnLoadStyFiles
        Exit Function
    End If
        
    Application.ScreenUpdating = False
    
    If (xType <> "") And ((Not bIsDocScheme) Or bReset) Then
        'message user if doc isn't in Word 2010 compatibility mode and scheme requires it
        If g_iWordVersion >= mpWordVersion_2010 Then
            If mdlWord14.GetCompatibilityMode(ActiveDocument) < 14 Then
                If ContainsWord2010NumberStyle(xScheme, xType) Then
                    If g_lUILanguage = wdFrenchCanadian Then
                        MsgBox "Le th�me sp�cifi� ne peut �tre t�l�charg� car il contient un style de num�rotation qui est disponible uniquement en mode compatibilit� dans Word 2010.", vbInformation, g_xAppName
                    Else
                        MsgBox "The specified scheme cannot be loaded because it contains a " & _
                            "numbering style that is only available for documents that are " & _
                            "in Word 2010 compatibility mode.", vbInformation, g_xAppName
                    End If
                    UnLoadStyFiles
                    Exit Function
                End If
            End If
        End If
    
'       load scheme
        bRet = iLoadScheme(ActiveDocument, xScheme, xType)
        
'       relinking again is necessary, since copying styles
'       occasionally breaks links
'        If bSchemeIsUnlinked(xScheme) Then
            bRet = bRelinkScheme(xScheme, _
                mpSchemeType_Document, False, False, False)
'        End If

'       convert to Word Heading styles
        If bUseWordHeadings Then
            UseWordHeadingStyles xScheme
        End If

'       redo trailing characters
        If (bIsDocScheme And bReset) Or _
                bUseWordHeadings Then
'           ensure that left indent of numbered and para styles matches
            UpdateParaStyles xScheme

            ApplyModifiedScheme xScheme
        End If
        
'       backup scheme properties
        BackupProps xScheme
    End If
    
'   activate scheme
    bSetSelectedScheme ActiveDocument, "1" & xScheme
    
'   insert level 1 automatically
    If bAllowAutoInsertion And (Not bIsDocScheme) And _
            xGetUserIni("Numbering", "InsertLevel1Automatically") = "1" And _
            (Selection.Range.ListParagraphs.Count = 0) Then
        'insert level one if specified,
        'but only if selection is not already numbered
        bInsertNumberFromToolbar 1
    End If
    
'   unload sty files
    UnLoadStyFiles
    
'   return -1 for success
    UseSchemeExternal = -1
    
    Application.ScreenUpdating = True
    Exit Function
ProcError:
    UnLoadStyFiles
    Application.ScreenUpdating = True
    UseSchemeExternal = Err
End Function

Function SyncContStyleFonts(xScheme As String) As Long
    Dim i As Integer
    Dim styCont As Word.Style
    Dim styContNext As Word.Style
    Dim styLevel As Word.Style
    Dim xNextContFontName As String
    Dim sNextContFontSize As Single
    Dim xRoot As String
    Dim iLevels As Integer
    
    xRoot = xGetStyleRoot(xScheme)
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    
    For i = 1 To iLevels
        If g_xSyncContStyles(i - 1) = "1" Then
            On Error Resume Next
            Set styContNext = ActiveDocument.Styles(xRoot & " Cont " & i + 1)
            On Error GoTo 0
            If Not styContNext Is Nothing Then
                With styContNext.Font
                    xNextContFontName = .Name
                    sNextContFontSize = .Size
                End With
            End If
            
            On Error Resume Next
            Set styLevel = ActiveDocument.Styles(xGetStyleName(xScheme, i))
            Set styCont = ActiveDocument.Styles(xRoot & " Cont " & i)
            On Error GoTo 0
            If (Not styLevel Is Nothing) And _
                    (Not styCont Is Nothing) Then
                With styCont.Font
                    .Name = styLevel.Font.Name
                    .Size = styLevel.Font.Size
                End With
            End If
            
            If Not styContNext Is Nothing Then
                With styContNext.Font
                    .Name = xNextContFontName
                    .Size = sNextContFontSize
                End With
            End If
            
            Set styCont = Nothing
            Set styContNext = Nothing
            Set styLevel = Nothing
        End If
    Next i
End Function

Function CompareContStyleFonts(xScheme As String)
'   initializes array that stores whether to sync cont style font
    Dim i As Integer
    Dim styCont As Word.Style
    Dim styLevel As Word.Style
    Dim xRoot As String
    Dim iLevels As Integer
    
    ReDim g_xSyncContStyles(8)
    
    xRoot = xGetStyleRoot(xScheme)
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    
    For i = 1 To iLevels
        On Error Resume Next
        Set styLevel = ActiveDocument.Styles(xGetStyleName(xScheme, i))
        Set styCont = ActiveDocument.Styles(xRoot & " Cont " & i)
        On Error GoTo 0
        If (Not styLevel Is Nothing) And _
                (Not styCont Is Nothing) Then
            g_xSyncContStyles(i - 1) = _
                Abs((styCont.Font.Name = styLevel.Font.Name And _
                styCont.Font.Size = styLevel.Font.Size))
        End If
        Set styCont = Nothing
        Set styLevel = Nothing
    Next i
End Function

Function bEnsureSchemeStyles(xScheme As String) As Boolean
    Dim i As Integer
    Dim iLevels As Integer
    Dim xStyle As String
    Dim styLevel As Word.Style
    Dim xAlias As String
    Dim j As Integer
    
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    For i = 1 To iLevels
        xStyle = xGetStyleName(xScheme, i)
        On Error Resume Next
        Set styLevel = ActiveDocument.Styles(xStyle)
        On Error GoTo ProcError
        If styLevel Is Nothing Then
            xAlias = GetField(xScheme, mpRecField_Alias, mpSchemeType_Document)
            If i = 1 Then
                If g_lUILanguage = wdFrenchCanadian Then
                    MsgBox "Le th�me de document " & xAlias & " ne contient pas de styles de niveau un.  Veuillez r�initialiser ou supprimer ce th�me.", vbExclamation, g_xAppName
                Else
                    MsgBox "The " & xAlias & " document scheme is missing the style for " & _
                        "level one.  Please reset or delete this scheme.", vbExclamation, g_xAppName
                End If
                Exit Function
            Else
                If g_lUILanguage = wdFrenchCanadian Then
                    MsgBox "e th�me de document " & xAlias & " ne contient pas de styles pour un ou plusieurs niveaux. Le th�me sera red�fini pour refleter le nombre de niveaux disponibles.", _
                        vbExclamation, g_xAppName
                Else
                    MsgBox "The " & xAlias & " document scheme is missing the styles " & _
                        "for one or more of its levels.  The scheme will be " & _
                        "redefined to reflect the actual number of levels available.", _
                        vbExclamation, g_xAppName
                End If
                For j = i To iLevels
                    lSetLevelProps xScheme, _
                                   j, _
                                   mpSchemeType_Document, _
                                   PropertyNotAvailable
                Next j
                Exit For
            End If
        End If
        Set styLevel = Nothing
    Next i
    bEnsureSchemeStyles = True
    Exit Function
ProcError:
    RaiseError "bEnsureSchemeStyles"
    Exit Function
End Function

Sub RelinkPreserveIndents(oDoc As Word.Document, _
                          ltScheme As Word.ListTemplate, _
                          xScheme As String, _
                          iStart As Integer, _
                          iEnd As Integer, _
                          Optional bUseWordHeadings As Boolean = False, _
                          Optional bUnlinkOnly As Boolean = False)
'relinks while preserving indents that differ from list template number and text positions;
'when Word relinks, the list template decides conflicts
    Dim styStyle As Word.Style
    Dim xStyIndents(8, 2) As String
    Dim i As Integer
    Dim xRoot As String
    Dim xStyle As String
    Dim j As Integer
        
    On Error GoTo ProcError
    
    If Not bUseWordHeadings Then _
        xRoot = xGetStyleRoot(xScheme)
    
'   store indents for ALL styles - necessary due to their hierarchical nature
    For i = 1 To 9
        If bUseWordHeadings Then
            xStyle = xTranslateHeadingStyle(i)
        Else
            xStyle = xRoot & "_L" & i
        End If
        
        On Error Resume Next
        Set styStyle = oDoc.Styles(xStyle)
        On Error GoTo ProcError
        
        If Not styStyle Is Nothing Then
            xStyIndents(i - 1, 0) = xStyle
            With styStyle.ParagraphFormat
                xStyIndents(i - 1, 1) = .FirstLineIndent
                xStyIndents(i - 1, 2) = .LeftIndent
            End With
        End If
        
        Set styStyle = Nothing
    Next i
        
'   first unlink ALL levels - necessary when converting btwn
'   native Word and proprietary styles
    For i = 9 To 1 Step -1
        ltScheme.ListLevels(i).LinkedStyle = ""
    Next i
                
'   relink specified levels
    If Not bUnlinkOnly Then
        For j = 1 To 2
            For i = iEnd To iStart Step -1
                ltScheme.ListLevels(i).LinkedStyle = xStyIndents(i - 1, 0)
            Next i
        Next j
    End If
    
'   restore indents
    For i = 1 To 9
        xStyle = xStyIndents(i - 1, 0)
        If xStyle <> "" Then
            With oDoc.Styles(xStyle).ParagraphFormat
                .FirstLineIndent = xStyIndents(i - 1, 1)
                .LeftIndent = xStyIndents(i - 1, 2)
            End With
        End If
    Next i
    
    Exit Sub
ProcError:
    RaiseError "RelinkPreserveIndents"
    Exit Sub
End Sub

Function ActivateScheme(xScheme As String) As Long
    
    On Error GoTo ProcError
    
    If xScheme <> "" Then
        If Mid(xScheme, 2, 4) = "zzmp" Then
'           from variable - created for MacPac 2K reuse
            xScheme = Mid(xScheme, 2)
            If Not bListTemplateExists(xScheme) Then _
                Exit Function
        End If
            
        xScheme = xGetLTRoot(xScheme)
        
'       activate
        bSetSelectedScheme ActiveDocument, "1" & xScheme
    
'       return -1 for success
        ActivateScheme = -1
    End If
    
    Exit Function
ProcError:
    RaiseError "ActivateScheme"
    Exit Function
End Function

Function StoreIndents(xScheme As String, _
                      xStyIndents() As String) As Long
    Dim i As Integer
    Dim xRoot As String
    Dim xStyle As String
    Dim styStyle As Word.Style
    Dim bUseWordHeadings As Boolean
    
    On Error GoTo ProcError
    
    ReDim xStyIndents(8, 2)
    
    bUseWordHeadings = bIsHeadingScheme(xScheme)
    If Not bUseWordHeadings Then _
        xRoot = xGetStyleRoot(xScheme)
    
'   store indents for ALL styles - necessary due to their hierarchical nature
    For i = 1 To 9
        If bUseWordHeadings Then
            xStyle = xTranslateHeadingStyle(i)
        Else
            xStyle = xRoot & "_L" & i
        End If
        
        On Error Resume Next
        Set styStyle = ActiveDocument.Styles(xStyle)
        On Error GoTo ProcError
        
        If Not styStyle Is Nothing Then
            xStyIndents(i - 1, 0) = xStyle
            With styStyle.ParagraphFormat
                xStyIndents(i - 1, 1) = .FirstLineIndent
                xStyIndents(i - 1, 2) = .LeftIndent
            End With
        End If
        
        Set styStyle = Nothing
    Next i
    
    Exit Function
ProcError:
    RaiseError "mdlN90.StoreIndents"
    Exit Function
End Function

Function RestoreIndents(xStyIndents() As String) As Long
    Dim i As Integer
    Dim xStyle As String
    
    On Error GoTo ProcError
    
    For i = 1 To 9
        xStyle = xStyIndents(i - 1, 0)
        If xStyle <> "" Then
            With ActiveDocument.Styles(xStyle).ParagraphFormat
                .FirstLineIndent = xStyIndents(i - 1, 1)
                .LeftIndent = xStyIndents(i - 1, 2)
            End With
        End If
    Next i

    Exit Function
ProcError:
    RaiseError "mdlN90.RestoreIndents"
    Exit Function
End Function

Function NumberContSwitch(bUp As Boolean) As Long
    Dim xScheme As String
    Dim oLT As ListTemplate
    Dim xStyle As String
    Dim iPos As Integer
    Dim oPara As Word.Paragraph
    Dim iLevel As Integer
    Dim xRoot As String
    Dim oStyle As Word.Style
    Dim bFound As Boolean
    Dim rngPara As Word.Range
    Dim xChar As String
    Dim rngSelection As Word.Range
    Dim bShowAll As Boolean
    Dim rngExpand As Word.Range
    Dim iLevels As Integer '9.9.6004
    Dim i As Integer '9.9.6004
    Dim xCont As String '9.9.6004
    
    On Error GoTo ProcError
    
'   turn on show all - this will prevent error when preceding paragraph is hidden
    With ActiveWindow.View
        bShowAll = .ShowAll
        .ShowAll = True
    End With
    
    Set rngSelection = Selection.Range
    
'   expand to include partner style separator paragraph
    If g_iWordVersion > mpWordVersion_XP Then
        Set rngExpand = rngSelection.Duplicate
        rngExpand.Expand wdParagraph
        
        'if the previous paragraph is style separator, add it to range
        If rngExpand.Start > 0 Then
            If mdlWordXML.IsStyleSeparator(rngExpand _
                    .Previous(wdParagraph).Paragraphs.First) Then
                rngExpand.MoveStart wdParagraph, -1
            End If
        End If
        
        'if the last paragraph is style separator, include the next paragraph
        If mdlWordXML.IsStyleSeparator(rngExpand.Paragraphs.Last) Then
            rngExpand.MoveEnd wdParagraph
        End If
        
        If rngExpand.Paragraphs.Count > rngSelection.Paragraphs.Count Then _
            rngSelection.SetRange rngExpand.Start, rngExpand.End
    End If

    For Each oPara In rngSelection.Paragraphs
        Set rngPara = oPara.Range
        If bUp Then
'           promote - check for Cont style
'           GLOG 2847 - trap for unstyled paragraphs
            On Error Resume Next
            xStyle = oPara.Style
            On Error GoTo ProcError
            
            iPos = InStr(xStyle, " Cont ")
            If iPos = 0 Then GoTo labNextPara
            
'           get level
            iLevel = Val(Right(xStyle, 1))
            If iLevel = 0 Then GoTo labNextPara
            
'           get scheme
            xRoot = Left(xStyle, iPos - 1)
            If xRoot = "Heading" Then
                xScheme = "HeadingStyles"
            Else
                xScheme = "zzmp" & xRoot
            End If
            xStyle = xGetStyleName(xScheme, iLevel, xRoot)
            
'           check for existence of numbered style
            On Error Resume Next
            Set oStyle = ActiveDocument.Styles(xStyle)
            On Error GoTo ProcError
            
            If oStyle Is Nothing Then GoTo labNextPara
            
'           insert numbered para
            rngInsertNumber rngPara, iLevel, xScheme, False
            bFound = True
        Else
'           demote - check for list template
            On Error Resume Next
            Set oLT = rngPara.Characters(1).ListFormat.ListTemplate
            On Error GoTo ProcError
            
            If Not oLT Is Nothing Then
'               ensure that outline numbered
                If oLT.ListLevels.Count <> 9 Then GoTo labNextPara
                
'               ensure that it's a MacPac scheme
                xScheme = xGetLTRoot(oLT.Name)
                If xScheme <> "" Then
                    If bIsMPListTemplate(oLT) Then
'                       get level
                        iLevel = rngPara.Characters(1).ListFormat.ListLevelNumber
                        xRoot = xGetStyleRoot(xScheme)
                        xStyle = xRoot & " Cont " & iLevel
                        
'                       ensure existence of cont style
                        On Error Resume Next
                        Set oStyle = ActiveDocument.Styles(xStyle)
                        On Error GoTo ProcError
                        
                        If oStyle Is Nothing Then
                            If g_iLoadContStyles <> mpLoadIndividuallyOnDemand Then '9.9.6004
                                iLevels = iGetLevels(xScheme, mpSchemeType_Document)
                                For i = 1 To iLevels
                                    xCont = xRoot & " Cont " & i
                                    On Error Resume Next
                                    Set oStyle = ActiveDocument.Styles(xCont)
                                    On Error GoTo ProcError
                                    If oStyle Is Nothing Then
                                        CreateContStyles xScheme, i, , True
                                    End If
                                    Set oStyle = Nothing
                                Next i
                            Else
                                CreateContStyles xScheme, iLevel, , True
                            End If
                            Set oStyle = ActiveDocument.Styles(xStyle)
                        End If
                        
'                       delete trailing characters
                        With rngPara
                            xChar = Left(.Text, 1)
                            If xChar = Chr(11) Or xChar = Chr(32) Then
                                .StartOf
                                .MoveEndWhile xChar, 2
                                .Delete
                            End If
                        End With
                        
                        'remove style separator if necessary
                        If g_iWordVersion > mpWordVersion_XP Then
                            If mdlWordXML.IsStyleSeparator(oPara) Then
                                'remove style separator
                                rngPara.Paragraphs(1).Range.Characters.Last.Delete
                            End If
                        End If
                        
'                       restyle para
                        oPara.Style = xStyle
                        
                        bFound = True
                    End If
                End If
            End If
        End If
labNextPara:
        Set oStyle = Nothing
        Set oLT = Nothing
    Next oPara
    
'    If Not bFound Then
'        If bUp Then
'        Else
'        End If
'    End If

'   restore user's setting
    ActiveWindow.View.ShowAll = bShowAll
    
    Exit Function
ProcError:
    RaiseError "mdlN90.NumberContSwitch"
    Exit Function
End Function

Function HideParagraph()
    Dim rngSelection As Word.Range
    Dim rngLocation As Word.Range
    Dim bShowHidden As Boolean
    Dim bShowAll As Boolean
    Dim bParaInserted As Boolean
    Dim oPara As Word.Paragraph
    
    On Error GoTo ProcError
    
    With ActiveWindow.View
'        bShowHidden = .ShowHiddenText
'        bShowAll = .ShowAll
'        .ShowHiddenText = True

'       ensure that hidden text is showing
        If Not .ShowHiddenText Then _
            .ShowAll = True
    End With
    
    Set rngSelection = Selection.Range
    Set rngLocation = rngSelection.Duplicate
    rngLocation.Select
    
    With Selection
        .Expand wdParagraph
        
'        If (.End = ActiveDocument.Paragraphs.Last.Range.End) And _
'                (.Paragraphs.Last.Range.Text = vbCr) Then
        If .End = ActiveDocument.Paragraphs.Last.Range.End Then
'           native Hidden command won't work on last paragraph in doc
            .InsertParagraphAfter
            .MoveEnd wdCharacter, -1
            bParaInserted = True
        End If
        
'       hide block
        Application.Run "Hidden"
                
'       add/remove red for hidden
        If .Font.Hidden Then
            .Font.ColorIndex = wdRed
        Else
            For Each oPara In .Paragraphs
                .Font.ColorIndex = ActiveDocument.Styles(oPara.Style).Font.ColorIndex
            Next oPara
        End If
    
'note: removed following conditional because we're now turning on show all
'and leaving it turned on
'        If .Font.Hidden And (Not bShowHidden) And (Not bShowAll) Then
''           put cursor immediately after hidden block
'            .Collapse wdCollapseEnd
'        Else
'           delete inserted paragraph
            If bParaInserted Then _
                .Next(wdParagraph).Delete
                
'           restore original selection
            rngSelection.Select
'        End If
    End With
        
'    ActiveWindow.View.ShowHiddenText = bShowHidden
    
    Exit Function
ProcError:
    RaiseError "mdlN90.HideParagraph"
    Exit Function
End Function

Public Sub ConvertToInexactSpacing(xScheme As String, sBase As Single)
'since dynamic line spacing adjustment can only be one-way, from inexact
'in sty file to exact in document, we need to force inexact spacing in
'new schemes that are based on a doc scheme in an exactly spaced doc
    Dim i As Integer
    Dim j As Integer
    Dim xStyle As String
    Dim xRoot As String
    Dim oStyle As Word.Style
    Dim sCompare As Single
    
    On Error GoTo ProcError
    
    xRoot = xGetStyleRoot(xScheme)
    
    For i = 9 To 1 Step -1
        For j = 1 To 2
            Set oStyle = Nothing
            
            If j = 1 Then
'               do numbered style
                xStyle = xRoot & "_L" & i
            Else
'               do cont style
                xStyle = xRoot & " Cont " & i
            End If
            
            On Error Resume Next
            Set oStyle = ActiveDocument.Styles(xStyle)
            On Error GoTo ProcError
            
            If Not oStyle Is Nothing Then
                With oStyle.ParagraphFormat
                    If .LineSpacingRule = wdLineSpaceExactly Then
'                       get number of lines for each normal line
                        sCompare = .LineSpacing / sBase
                        If sCompare / 0.5 = Int(sCompare / 0.5) Then
'                           do only if ratio is a multiple of 0.5
                            .LineSpacingRule = wdLineSpaceMultiple
                            .LineSpacing = LinesToPoints(sCompare)
                        End If
                    End If
                End With
            End If
        Next j
    Next i

    Exit Sub
ProcError:
    RaiseError "mdlN90.ConvertToInexactSpacing"
    Exit Sub
End Sub

Sub LinkToNewListTemplate(xScheme As String)
'intended to fix NewScheme bug in Word XP
'whereby prototype list template changes along
'with new list template when edited immediately,
'i.e. using same buffer doc in which renaming was done;
'for now, call only from bNewScheme
    Dim oLTNew As Word.ListTemplate
    Dim oLTOld As Word.ListTemplate
    Dim i As Integer
    
    With ActiveDocument.ListTemplates
        Set oLTOld = .Item(xScheme)
        Set oLTNew = .Add(True)
    End With
    
    For i = 1 To 9
        iCopyListLevel "", oLTOld.ListLevels(i), oLTNew.ListLevels(i), True
    Next i
    
    oLTOld.Name = ""
    oLTNew.Name = xScheme
    
    bRelinkScheme xScheme, mpSchemeType_Document, False, False
End Sub

Sub EditScheme(ByVal xScheme As String)
    Dim lView As Long
    Dim xRoot As String
    Dim iSchemes As Integer
    Dim oForm As VB.Form
    Dim bToggleRibbon As Boolean
    Dim bShowAll As Boolean
    Dim bShowTabs As Boolean
    Dim bShowSpaces As Boolean
    Dim bShowParagraphs As Boolean
    Dim oReg As CRegistry
    Dim xRegFolder As String
    Dim vValue As Variant
    Dim bResetQAT As Boolean
    
    g_bPreventDocChangeEvent = True

    'GLOG 5628 - preserve show formatting settings in Word 2013/2016
    If g_iWordVersion >= mpWordVersion_2013 Then
        With ActiveWindow.View
            bShowAll = .ShowAll
            bShowTabs = .ShowTabs
            bShowSpaces = .ShowSpaces
            bShowParagraphs = .ShowParagraphs
        End With
    End If
    
    'get style area width before turning off for preview
    g_sStyleArea = Word.ActiveWindow.StyleAreaWidth
    
    '10/4/12 - user is no longer required to select scheme
    If xScheme = "" Then
        iSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
        If iSchemes = 0 Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Aucun th�me de num�rotation MacPac dans ce document.", vbInformation, g_xAppName
            Else
                MsgBox "There are no MacPac numbering schemes " & _
                    "in this document.", vbInformation, g_xAppName
            End If
        ElseIf iSchemes = 1 Then
            xScheme = g_xDSchemes(0, 0)
        Else
            Set oForm = New frmSchemeSelector
            oForm.Show vbModal
            If Not oForm.Cancelled Then
                xScheme = g_xDSchemes(oForm.lstSchemes.ListIndex, 0)
            End If
            Set oForm = Nothing
        End If
    End If
    
    If xScheme <> "" Then
        'GLOG 5643 - collapse the ribbon and move the QAT to
        'avoid freezing Word if necessary
        If g_bCollapseRibbon Then
            Set oReg = New CRegistry
            If InStr(Application.Version, "16.") <> 0 Then
                xRegFolder = "16.0"
            ElseIf g_iWordVersion = mpWordVersion_2013 Then
                xRegFolder = "15.0"
            ElseIf g_iWordVersion = mpWordVersion_2010 Then
                xRegFolder = "14.0"
            Else
                xRegFolder = "12.0"
            End If
            vValue = String(255, Chr(32))
            lRet = oReg.GetValue(HKEY_CURRENT_USER, "Software\Microsoft\Office\" & xRegFolder & _
                "\Common\Toolbars\Word", "QuickAccessToolbarStyle", vValue)
            m_xQATStyle = RTrim(vValue)
            bResetQAT = ((m_xQATStyle <> "") And (m_xQATStyle <> "4"))
            If bResetQAT Then
                oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\" & xRegFolder & _
                    "\Common\Toolbars\Word", "QuickAccessToolbarStyle", REG_DWORD, "4"
            End If
        End If
    
        'remmed 9/27/16 in light of GLOG 5643
'        'GLOG 5224 - on some Windows 8 machines, focus was reverting to the
'        'originating ribbon, causing the Edit Scheme dialog to disappear and
'        'Word to become frozen
'        bToggleRibbon = ((g_lWinMajorVersion = 6) And (g_lWinMinorVersion = 2) And _
'            (g_iWordVersion > mpWordVersion_2003))
'        If bToggleRibbon Then _
'            mdlCC.ToggleRibbon
        
        'set current scheme
        xRoot = xGetLTRoot(xScheme)
        Set g_oCurScheme = GetRecord(xRoot, mpSchemeType_Document)
        g_bCurSchemeIsHeading = bIsHeadingScheme(xRoot)
        
        'relink if necessary
        If bSchemeIsUnlinked(xRoot) Then _
            bRelinkScheme xRoot, mpSchemeType_Document, g_bCurSchemeIsHeading
        
        'GLOG 5351 - avoid error when styles have been manually deleted
        If Not bEnsureSchemeStyles(xRoot) Then
            Exit Sub
        End If
        
        'edit scheme
        If bEditScheme(xRoot, mpSchemeType_Document) Then
            g_docPreview.ActiveWindow.Activate
            bEditSchemeB g_lRulerDisplayDelay_EditDirect, bResetQAT '9.9.6012, 9.9.6014
        ElseIf bResetQAT Then 'GLOG 5643
            oReg.SetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\" & xRegFolder & _
                "\Common\Toolbars\Word", "QuickAccessToolbarStyle", REG_DWORD, m_xQATStyle
        End If
    
        'reset style area width - will only stick if set in Normal view
        '9.9.6008 (GLOG 5581) - this is no longer true and switching the
        'view is causing a refresh issue in Word 2016
        With Word.ActiveWindow
            If g_iWordVersion < mpWordVersion_2013 Then
                lView = .View.Type
                .View.Type = wdNormalView
            Else
                'GLOG 5628 - restore show formatting settings - switching
                'the view was previously taking care of this
                .View.ShowAll = bShowAll
                .View.ShowTabs = bShowTabs
                .View.ShowSpaces = bShowSpaces
                .View.ShowParagraphs = bShowParagraphs
            End If
            .StyleAreaWidth = g_sStyleArea
            If g_iWordVersion < mpWordVersion_2013 Then
                .View.Type = lView
            End If
        End With
        
        'GLOG 5224
        If bToggleRibbon Then _
            mdlCC.ToggleRibbon
    End If
   
    Word.Application.Activate
    g_bPreventDocChangeEvent = False
    mdlApplication.SendShiftKey
End Sub

Sub NewScheme(ByVal xScheme As String)
    Dim lView As Long
    Dim xRoot As String
    Dim iType As mpSchemeTypes
    Dim bShowAll As Boolean
    Dim bShowTabs As Boolean
    Dim bShowSpaces As Boolean
    Dim bShowParagraphs As Boolean
    
    'this method will ultimately leave user in Schemes dialog -
    'some of the prerequisites for ensuring its proper display are
    'also necessary for the New and Edit dialogs
    bRet = bPrepareForSchemesDialog()
    If Not bRet Then _
        Exit Sub
    
    g_bPreventDocChangeEvent = True
    
    'GLOG 5628 - preserve show formatting settings in Word 2013/2016
    If g_iWordVersion >= mpWordVersion_2013 Then
        With ActiveWindow.View
            bShowAll = .ShowAll
            bShowTabs = .ShowTabs
            bShowSpaces = .ShowSpaces
            bShowParagraphs = .ShowParagraphs
        End With
    End If
    
    '10/4/12 - user is no longer required to select scheme
    If xScheme = "" Then
        If iGetSchemes(g_xDSchemes(), mpSchemeType_Document) > 0 Then
            xScheme = xActiveScheme(ActiveDocument)
            If xScheme = "" Then _
                xScheme = g_xDSchemes(0, 0)
            iType = mpSchemeType_Document
        Else
            'use default scheme
            xScheme = xGetUserIni("Numbering", "DefaultScheme")
            If xScheme <> "" Then
                iType = xGetUserIni("Numbering", "DefaultSchemeType")
            Else
                xScheme = g_xFSchemes(0, 0)
                iType = mpSchemeType_Public
            End If
        End If
    Else
        iType = mpSchemeType_Document
    End If

    'set current scheme
    xRoot = xGetLTRoot(xScheme)
    Set g_oCurScheme = GetRecord(xRoot, iType)
    If iType = mpSchemeType_Document Then
        g_bCurSchemeIsHeading = bIsHeadingScheme(xRoot)
    
        'relink if necessary
        If bSchemeIsUnlinked(xRoot) Then _
            bRelinkScheme xRoot, mpSchemeType_Document, g_bCurSchemeIsHeading
    End If
    
    'display new scheme dialog
    If bNewScheme(xRoot, iType) Then
        'create/edit new scheme
        g_iSchemeEditMode = mpModeSchemeNewB
        ShowSchemesDialogs
    Else
    '   reset style area width - will only stick if set in Normal view
        '9.9.6008 (GLOG 5581) - this is no longer true and switching the
        'view is causing a refresh issue in Word 2016
        With Word.ActiveWindow
            If g_iWordVersion < mpWordVersion_2013 Then
                lView = .View.Type
                .View.Type = wdNormalView
            Else
                'GLOG 5628 - restore show formatting settings - switching
                'the view was previously taking care of this
                .View.ShowAll = bShowAll
                .View.ShowTabs = bShowTabs
                .View.ShowSpaces = bShowSpaces
                .View.ShowParagraphs = bShowParagraphs
            End If
            .StyleAreaWidth = g_sStyleArea
            If g_iWordVersion < mpWordVersion_2013 Then
                .View.Type = lView
            End If
        End With
       
        Word.Application.Activate
        g_bPreventDocChangeEvent = False
        mdlApplication.SendShiftKey
    End If
End Sub

Sub RelinkDocumentSchemes()
    Dim iDocSchemes As Integer
    
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "R�tabli lien th�mes de Document.  Veuillez patienter..."
    Else
        Application.StatusBar = "Relinking Document Schemes.  Please wait..."
    End If
    
    'relink schemes
    bRet = bRelinkSchemes(mpSchemeType_Document, True)
    iRenameLTsFromVars

    'display message
    iDocSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
    If g_lUILanguage = wdFrenchCanadian Then
        If iDocSchemes > 0 Then
            xMsg = "Les liens des th�mes dans ce document ont �t� r�tablis."
        Else
            xMsg = "Aucun th�me dans le document en cours."
        End If
    Else
        If iDocSchemes > 0 Then
            xMsg = "The schemes in the current " & _
               "document have been relinked."
        Else
            xMsg = "There are no schemes in the current document."
        End If
    End If
    MsgBox xMsg, vbInformation, g_xAppName

    Application.StatusBar = ""
End Sub

Sub DisplaySchemeActivationMenu(oActionControl As CommandBarControl)
    Dim lTop As Long
    Dim lHeight As Long
    Dim lLeft As Long
    Dim oMenu As CommandBar
    Dim oControl As CommandBarControl
    Dim oToolbar As CommandBar
    Dim iSchemes As Integer
    Dim i As Integer
    Dim xDefaultScheme As String
    Dim bDefaultIsLoaded As Boolean
    Dim iIndex As Integer
    Dim bSchemeIsSelected As Boolean
    Dim vData As Variant
    Dim lFormat As Long
    Dim oPicture As StdPicture
    
    Set oToolbar = CommandBars("MacPac Numbering")
    Set oMenu = CommandBars("MacPac Scheme Activation")
    
    'get default scheme
    If bDefaultSchemeExists Then
        xDefaultScheme = xGetUserIni("Numbering", "DefaultScheme")
    Else
        xDefaultScheme = g_xFSchemes(0, 0)
    End If
    
    'check whether cursor is in a MacPac scheme
    bSchemeIsSelected = (xGetScheme <> "")
    
    'get menu position before changing context,
    'since it doesn't seem to return accurate values afterward
    With oActionControl
        lTop = .Top
        lHeight = .Height
        lLeft = .Left
    End With
    
    With oMenu
        'clear existing items
        For Each oControl In .Controls
            oControl.Delete
        Next oControl
        
        'get document schemes
        iSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
        
        'store clipboard contents before copy/paste below
        If iSchemes > 0 Then _
            GetClipboard vData, oPicture, lFormat
    
        'add menu item for each scheme
        For i = 1 To iSchemes
            iIndex = i - 1
            Set oControl = .Controls.Add
            With oControl
                .Caption = g_xDSchemes(iIndex, 1)
                .Tag = g_xDSchemes(iIndex, 0)
                .OnAction = "zzmpActivateSchemeFromMenu"
                                
                'add icon to active scheme
                If g_xDSchemes(iIndex, 0) = xActiveScheme(ActiveDocument) Then
                    If (g_lActiveSchemeIcon = 0) Or _
                            (g_iWordVersion < mpWordVersion_2003) Then
                        'get icon from toolbar
                        oActionControl.CopyFace
                        .PasteFace
                    Else
                        'use id specified in ini - we added this option in 9.9.1010 to
                        'work around an error that a client was seeing on terminal server -
                        'use mpnWordXML to prevent compile error in Word 2000
                        mdlWordXML.SetControlFaceID oControl, g_lActiveSchemeIcon
                    End If
                End If
            End With
            
            'determine whether default scheme is already in doc
            If g_xDSchemes(iIndex, 0) = xDefaultScheme Then _
                bDefaultIsLoaded = True
        Next i
        
        'restore clipboard contents if necessary
        If iSchemes > 0 Then _
            RestoreClipboard vData, oPicture, lFormat
        
        'add menu item to select scheme at cursor
        If bSchemeIsSelected Then
            Set oControl = .Controls.Add
            With oControl
                If g_lUILanguage = wdFrenchCanadian Then
                    .Caption = "Activer th�me au curseur (Alt+Maj+A)"
                Else
                    .Caption = "Activate Scheme at Cursor (Alt+Shift+A)"
                End If
                .OnAction = "zzmpActivateScheme"
                .BeginGroup = (iSchemes > 0)
            End With
        End If
        
        'add menu item to load default scheme
        If Not bDefaultIsLoaded Then
            Set oControl = .Controls.Add
            With oControl
                If g_lUILanguage = wdFrenchCanadian Then
                    .Caption = "Charger th�me par d�faut"
                Else
                    .Caption = "Load Default Scheme"
                End If
                .OnAction = "zzmpLoadDefaultScheme"
                .BeginGroup = ((iSchemes > 0) And (Not bSchemeIsSelected))
            End With
        End If
        
        'display menu
        .ShowPopup lLeft, lTop + lHeight
    End With
    
    'prevent save prompt
    MarkStartupSaved
    
    Exit Sub
ProcError:
    RaiseError "mdlN90.DisplaySchemeActivationMenu"
    Exit Sub
End Sub

Sub ActivateSchemeFromRibbon(ByVal iIndex As Integer)
    Dim iSchemes As Integer
    
    On Error GoTo ProcError
    
    'get document schemes
    iSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
    
    If iIndex > iSchemes Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "Impossible d'activer le th�me selectionn�.", vbCritical, g_xAppName
        Else
            MsgBox "Could not activate the selected scheme.", vbCritical, g_xAppName
        End If
        Exit Sub
    End If
    
    'activate by index
    ActivateScheme g_xDSchemes(iIndex, 0)
    
    Exit Sub
ProcError:
    RaiseError "mdlN90.ActivateSchemeFromRibbon"
    Exit Sub
End Sub

Function GetSchemeActivationXML() As String
    Dim xXML As String
    Dim iSchemes As Integer
    Dim i As Integer
    Dim xDefaultScheme As String
    Dim bDefaultIsLoaded As Boolean
    Dim iIndex As Integer
    Dim bSchemeIsSelected As Boolean
    Dim xType As String
    Dim xDisplayName As String
    Dim xFavoritesXML As String
    
    On Error GoTo ProcError
    
    'get default scheme
    If bDefaultSchemeExists Then
        xDefaultScheme = xGetUserIni("Numbering", "DefaultScheme")
    Else
        xDefaultScheme = g_xFSchemes(0, 0)
    End If
    
    'get document schemes
    iSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
    
    'limit to 5
    iSchemes = mpMin(CDbl(iSchemes), 5)
    
    'check whether cursor is in a MacPac scheme
    bSchemeIsSelected = (xGetScheme <> "")
    
    'build XML string
    xXML = "<menu xmlns=""http://schemas.microsoft.com/office/2006/01/customui"">" & vbCrLf
    
    'add menu item for each scheme
    For i = 1 To iSchemes
        iIndex = i - 1
        
        'GLOG 4977 (2/1/12) - replace ampersand
        xDisplayName = Replace(g_xDSchemes(iIndex, 1), "&", "&amp;&amp;")
        
        'add menu item
        xXML = xXML & "<button id=" & """" & "mnuActivate" & i & """" & " label=" & _
            """" & xDisplayName & """" & " onAction=" & """" & "zzmpCallback" & _
            """" & " tag=" & """" & "zzmpActivateSchemeFromRibbon" & i & """"
            
        'add icon if this is the active scheme
        If g_xDSchemes(iIndex, 0) = xActiveScheme(ActiveDocument) Then
            xXML = xXML & " image=" & """" & "Num1" & """"
            If g_lUILanguage = wdFrenchCanadian Then
                xXML = xXML & " screentip=" & """" & xDisplayName & _
                    " est le th�me actif" & """"
            Else
                xXML = xXML & " screentip=" & """" & xDisplayName & _
                    " is the active scheme" & """"
            End If
        Else
            If g_lUILanguage = wdFrenchCanadian Then
                xXML = xXML & " screentip=" & """" & "Faire de " & xDisplayName & _
                    " le th�me actif" & """"
            Else
                xXML = xXML & " screentip=" & """" & "Make the " & xDisplayName & _
                    " scheme active" & """"
            End If
        End If
            
        xXML = xXML & "/>" & vbCrLf
        
        'determine whether default scheme is already in doc
        If g_xDSchemes(iIndex, 0) = xDefaultScheme Then _
            bDefaultIsLoaded = True
    Next i

    'add separator if appropriate
'    If (iSchemes > 0) And ((Not bDefaultIsLoaded) Or bSchemeIsSelected) Then
        xXML = xXML & "<menuSeparator id=" & """" & "mnuActivateSep" & _
            """" & "/>" & vbCrLf
'    End If
    
    'add menu item to activate scheme at cursor
    If bSchemeIsSelected Then
        If g_lUILanguage = wdFrenchCanadian Then
            xXML = xXML & "<button id=" & """" & "mnuActivateSelectedScheme" & """" & _
                " label=" & """" & "Activer th�me au curseur (Alt+Maj+A)" & """" & _
                " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
                "zzmpActivateScheme" & """" & " screentip=" & """" & _
                "D�finir le th�me s�lectionn� comme actif" & """" & "/>" & vbCrLf
        Else
            xXML = xXML & "<button id=" & """" & "mnuActivateSelectedScheme" & """" & _
                " label=" & """" & "&amp;Activate Scheme at Cursor (Alt+Shift+A)" & """" & _
                " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
                "zzmpActivateScheme" & """" & " screentip=" & """" & _
                "Make the selected scheme active" & """" & "/>" & vbCrLf
        End If
    End If
    
    'add menu item to load default scheme
    If Not bDefaultIsLoaded Then
        'add menu item
        If g_lUILanguage = wdFrenchCanadian Then
            xXML = xXML & "<button id=" & """" & "mnuActivateLoadDefault" & """" & _
                " label=" & """" & "Charger th�me par d�faut" & """" & " onAction=" & _
                """" & "zzmpCallback" & """" & " tag=" & """" & "zzmpLoadDefaultScheme" & _
                """" & "/>" & vbCrLf
        Else
            xXML = xXML & "<button id=" & """" & "mnuActivateLoadDefault" & """" & _
                " label=" & """" & "Load &amp;Default Scheme" & """" & " onAction=" & _
                """" & "zzmpCallback" & """" & " tag=" & """" & "zzmpLoadDefaultScheme" & _
                """" & "/>" & vbCrLf
        End If
    End If
    
    '9.9.6001 - add favorite schemes menu
    xFavoritesXML = GetFavoriteSchemesXML()
    If g_lUILanguage = wdFrenchCanadian Then
        xXML = xXML & "<menu id=" & """" & "mnuActivateLoadFavorites" & """" & _
            " label=" & """" & "Utilisez le th�me &amp;favori" & """"
    Else
        xXML = xXML & "<menu id=" & """" & "mnuActivateLoadFavorites" & """" & _
            " label=" & """" & "Use &amp;Favorite Scheme" & """"
    End If
    If xFavoritesXML <> "" Then
        xXML = xXML & ">" & vbCrLf & xFavoritesXML
    ElseIf g_lUILanguage = wdFrenchCanadian Then
        xXML = xXML & " screentip=" & """" & "Aucun th�mes favori n'a �t� fix�.  Vous pouvez sauvegarder des favoris dans la fen�tre des Th�mes." & """" & ">" & vbCrLf & _
            "<button id=" & """" & "mnuActivateNoFavorites" & """" & "/>" & vbCrLf
    Else
        xXML = xXML & " screentip=" & """" & "No favorites have been set.  " & _
            "You can add favorites in the Schemes dialog." & """" & ">" & vbCrLf & _
            "<button id=" & """" & "mnuActivateNoFavorites" & """" & "/>" & vbCrLf
    End If
    xXML = xXML & "</menu>"
                
    'add the root closing tag
    xXML = xXML & "</menu>"
    
    'prevent save prompt
    MarkStartupSaved
    
    GetSchemeActivationXML = xXML
    
    Exit Function
ProcError:
    RaiseError "mdlN90.GetSchemeActivationXML"
    Exit Function
End Function

Function GetContStyleMenuXML() As String
    Dim xScheme As String
    Dim xRoot As String
    Dim i As Integer
    Dim oStyle As Word.Style
    Dim xXML As String
    Dim iLevels As Integer

    On Error GoTo ProcError

    xScheme = xActiveScheme(ActiveDocument)

    If xScheme = "" Then _
        Exit Function

    xRoot = xGetStyleRoot(xScheme)

    'build XML string
    xXML = "<menu xmlns=""http://schemas.microsoft.com/office/2006/01/customui"">" & vbCrLf

    '9.9.6001 - display all levels unconditionally
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    For i = 1 To iLevels
'        Set oStyle = Nothing
'        On Error Resume Next
'        Set oStyle = ActiveDocument.Styles(xRoot & " Cont " & i)
'        On Error GoTo ProcError
'        If Not oStyle Is Nothing Then
            xXML = xXML & "<button id=" & """" & "mnuContStyle" & CStr(i) & """" & _
                " label=" & """" & xRoot & " Cont &amp;" & CStr(i) & """" & _
                " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
                "zzmpApplyContStyle" & CStr(i) & """" & "/>" & vbCrLf
'        End If
    Next i

    'add the root closing tag
    xXML = xXML & "</" & "menu" & ">"
    
    'prevent save prompt
    MarkStartupSaved
    
    GetContStyleMenuXML = xXML
    
    Exit Function
ProcError:
    RaiseError "mdlN90.GetContStyleMenuXML"
    Exit Function
End Function

Private Function xGetScheme() As String
'returns MacPac numbering scheme name of
'first paragraph in selection
    Dim xScheme As String
    Dim ltScheme As ListTemplate
    Dim xStyle As String
    Dim iPos As Integer
    
    On Error GoTo ProcError
    
'   get scheme of 1st selected paragraph
'   GLOG 2847 - trap for unstyled paragraphs
    On Error Resume Next
    xStyle = Selection.Paragraphs(1).Style
    On Error GoTo ProcError
    
    iPos = InStr(xStyle, " Cont ")
    If iPos <> 0 Then
'       cursor is in a cont style
        xScheme = Left(xStyle, iPos - 1)
        If xScheme = "Heading" Then
            xScheme = "HeadingStyles"
        Else
            xScheme = "zzmp" & xScheme
        End If
    Else
'       attempt to get list template
        With Selection.Paragraphs(1).Range.Characters(1).ListFormat
            On Error Resume Next
            Set ltScheme = .ListTemplate
            On Error GoTo 0
'           if outline numbered, get list template name
            If Not ltScheme Is Nothing Then
                If ltScheme.ListLevels.Count = 9 Then
                    xScheme = ltScheme.Name
                End If
            End If
        End With
    End If

'   check for existence of MacPac scheme
    If IsMacPacScheme(xScheme, , ActiveDocument) Then
        xGetScheme = xScheme
    End If
    
    Exit Function
ProcError:
    RaiseError "mdlN90.xGetScheme"
    Exit Function
End Function

Public Function bDefaultSchemeExists() As Boolean
'validates user's default scheme setting
    Dim xDefaultScheme As String
    Dim xType As String
    
    On Error GoTo ProcError
    
    xDefaultScheme = xGetUserIni("Numbering", "DefaultScheme")
    If xDefaultScheme = "" Then Exit Function
    
    xType = xGetUserIni("Numbering", "DefaultSchemeType")
    If (xType <> "2") And (xType <> "3") Then Exit Function
    
    bDefaultSchemeExists = IsMacPacScheme(xDefaultScheme, CInt(xType))
    
    Exit Function
ProcError:
    RaiseError "mdlN90.bDefaultSchemeExists"
    Exit Function
End Function

Private Sub GetClipboard(ByRef vData As Variant, _
                         ByRef oPicture As StdPicture, _
                         ByRef lFormat As Long)
    On Error Resume Next
    
    If Clipboard.GetFormat(vbCFRTF) Then
      vData = Clipboard.GetText(vbCFRTF)
      lFormat = vbCFRTF
    ElseIf Clipboard.GetFormat(vbCFText) Then
      vData = Clipboard.GetText(vbCFText)
      lFormat = vbCFText
    ElseIf Clipboard.GetFormat(vbCFBitmap) Then
      Set oPicture = Clipboard.GetData(vbCFBitmap)
      lFormat = vbCFBitmap
    ElseIf Clipboard.GetFormat(vbCFDIB) Then
      Set oPicture = Clipboard.GetData(vbCFDIB)
      lFormat = vbCFDIB
    ElseIf Clipboard.GetFormat(vbCFEMetafile) Then
      Set oPicture = Clipboard.GetData(vbCFEMetafile)
      lFormat = vbCFEMetafile
    ElseIf Clipboard.GetFormat(vbCFFiles) Then
      vData = Clipboard.GetData(vbCFFiles)
      lFormat = vbCFFiles
    ElseIf Clipboard.GetFormat(vbCFLink) Then
      vData = Clipboard.GetData(vbCFLink)
      lFormat = vbCFLink
    ElseIf Clipboard.GetFormat(vbCFMetafile) Then
      Set oPicture = Clipboard.GetData(vbCFMetafile)
      lFormat = vbCFMetafile
    ElseIf Clipboard.GetFormat(vbCFPalette) Then
      Set oPicture = Clipboard.GetData(vbCFPalette)
      lFormat = vbCFPalette
    Else
      vData = Null
      lFormat = 0
    End If
    
    Err.Clear
End Sub
 
Private Sub RestoreClipboard(ByVal vData As Variant, _
                             ByVal oPicture As StdPicture, _
                             ByVal lFormat As Long)
    On Error Resume Next
      
    Clipboard.Clear
    If (lFormat) Then
      Select Case lFormat
          Case vbCFText
            Clipboard.SetText vData, vbCFText
          Case vbCFRTF
            Clipboard.SetText vData, vbCFRTF
          Case vbCFBitmap
            Clipboard.SetData oPicture, vbCFBitmap
          Case vbCFDIB
            Clipboard.SetData oPicture, vbCFDIB
          Case vbCFEMetafile
            Clipboard.SetData oPicture, vbCFEMetafile
          Case vbCFFiles
            Clipboard.SetData vData, vbCFFiles
          Case vbCFLink
            Clipboard.SetData vData, vbCFLink
          Case vbCFMetafile
            Clipboard.SetData oPicture, vbCFMetafile
          Case vbCFPalette
            Clipboard.SetData oPicture, vbCFPalette
          Case Else
      End Select
    End If
    
    Err.Clear
End Sub

Function xIntToListNumStyle(i As Integer, _
                            iNumberStyle As Integer) As String
    Dim xNumber As String
    Dim j As Integer
    Dim k As Integer

    Select Case iNumberStyle
        Case wdListNumberStyleNone
            xNumber = ""
            
        Case wdListNumberStyleBullet
            xNumber = ChrW(&HB7)
            
        Case wdListNumberStyleUppercaseRoman, _
                wdListNumberStyleLowercaseRoman
            k = i
            If i > 1000 Then
                i = 1
            ElseIf i = 1000 Then
                xNumber = "m"
                k = 0
            End If

            If k > 899 Then
                xNumber = "cm"
                k = k - 900
            End If

            If k > 499 Then
                xNumber = "d"
                k = k - 500
            End If

            If k > 399 Then
                xNumber = "cd"
                k = k - 400
            End If

            j = Int(k / 100)
            xNumber = xNumber & String(j, "c")
            k = k - (j * 100)

            If k > 89 Then
                xNumber = xNumber & "xc"
                k = k - 90
            End If

            If k > 49 Then
                xNumber = xNumber & "l"
                k = k - 50
            End If

            If k > 39 Then
                xNumber = xNumber & "xl"
                k = k - 40
            End If

            xNumber = xNumber & String(Int(k / 10), "x")

            j = k Mod 10
            Select Case j
                Case 1
                    xNumber = xNumber & "i"
                Case 2
                    xNumber = xNumber & "ii"
                Case 3
                    xNumber = xNumber & "iii"
                Case 4
                    xNumber = xNumber & "iv"
                Case 5
                    xNumber = xNumber & "v"
                Case 6
                    xNumber = xNumber & "vi"
                Case 7
                    xNumber = xNumber & "vii"
                Case 8
                    xNumber = xNumber & "viii"
                Case 9
                    xNumber = xNumber & "ix"
            End Select

            If iNumberStyle = wdListNumberStyleUppercaseRoman Then _
                xNumber = UCase(xNumber)
                
        Case wdListNumberStyleUppercaseLetter
            j = i Mod 26
            If j = 0 Then
                xNumber = String(i / 26, "Z")
            Else
                xNumber = String((Int(i / 26) + 1), Chr(j + 64))
            End If
            
        Case wdListNumberStyleLowercaseLetter
            j = i Mod 26
            If j = 0 Then
                xNumber = String(i / 26, "z")
            Else
                xNumber = String((Int(i / 26) + 1), Chr(j + 96))
            End If
            
        Case wdListNumberStyleArabicLZ, wdListNumberStyleLegalLZ
            xNumber = "0" & i
            
        Case wdListNumberStyleOrdinal
            k = i Mod 100
            If g_lUILanguage = wdFrenchCanadian Then
                Select Case k
                    Case 1
                        xNumber = i & "er"
                    Case Else
                        xNumber = i & "e"
                End Select
            Else
                Select Case k
                    Case 11
                        xNumber = i & "th"
                    Case 12
                        xNumber = i & "th"
                    Case 13
                        xNumber = i & "th"
                    Case Else
                        j = i Mod 10
                        Select Case j
                            Case 1
                                xNumber = i & "st"
                            Case 2
                                xNumber = i & "nd"
                            Case 3
                                xNumber = i & "rd"
                            Case Else
                                xNumber = i & "th"
                        End Select
                End Select
            End If
            
         Case wdListNumberStyleOrdinalText
            If g_lUILanguage = wdFrenchCanadian Then
                If i > 1000 Then
                    i = 1
                    k = 1
                ElseIf i = 1000 Then
                    xNumber = "un milli�me"
                    k = 0
                Else
                    k = i
                End If
    
                If k > 99 Then
                    j = Int(k / 100)
                    Select Case j
                        Case 1
                            xNumber = "cent"
                        Case 2
                            xNumber = "deux cent"
                        Case 3
                            xNumber = "trois cent"
                        Case 4
                            xNumber = "quatre cent"
                        Case 5
                            xNumber = "cinq cent"
                        Case 6
                            xNumber = "six cent"
                        Case 7
                            xNumber = "sept cent"
                        Case 8
                            xNumber = "huit cent"
                        Case 9
                            xNumber = "neuf cent"
                    End Select
                    If k Mod 100 = 0 Then
                        xNumber = xNumber & "i�me"
                    Else
                        xNumber = xNumber & Space(1)
                    End If
                    k = k - (j * 100)
                    If k = 1 Then
                        xNumber = xNumber & "et uni�me"
                        k = 0
                    End If
                End If
    
                Select Case k
                    Case 1
                        xNumber = "premier"
                    Case 2
                        xNumber = xNumber & "deuxi�me"
                    Case 3
                        xNumber = xNumber & "troisi�me"
                    Case 4
                        xNumber = xNumber & "quatri�me"
                    Case 5
                        xNumber = xNumber & "cinqui�me"
                    Case 6
                        xNumber = xNumber & "sixi�me"
                    Case 7
                        xNumber = xNumber & "septi�me"
                    Case 8
                        xNumber = xNumber & "huiti�me"
                    Case 9
                        xNumber = xNumber & "neuvi�me"
                    Case 10
                        xNumber = xNumber & "dixi�me"
                    Case 11
                        xNumber = xNumber & "onzi�me"
                    Case 12
                        xNumber = xNumber & "douzi�me"
                    Case 13
                        xNumber = xNumber & "treizi�me"
                    Case 14
                        xNumber = xNumber & "quatorzi�me"
                    Case 15
                        xNumber = xNumber & "quinzi�me"
                    Case 16
                        xNumber = xNumber & "seizi�me"
                    Case 17
                        xNumber = xNumber & "dix-septi�me"
                    Case 18
                        xNumber = xNumber & "dix-huiti�me"
                    Case 19
                        xNumber = xNumber & "dix-neuvi�me"
                    Case 20
                        xNumber = xNumber & "vingti�me"
                    Case 21
                        xNumber = xNumber & "vingt et uni�me"
                    Case 22
                        xNumber = xNumber & "vingt-deuxi�me"
                    Case 23
                        xNumber = xNumber & "vingt-troisi�me"
                    Case 24
                        xNumber = xNumber & "vingt-quatri�me"
                    Case 25
                        xNumber = xNumber & "vingt-cinqui�me"
                    Case 26
                        xNumber = xNumber & "vingt-sixi�me"
                    Case 27
                        xNumber = xNumber & "vingt-septi�me"
                    Case 28
                        xNumber = xNumber & "ving-huiti�me"
                    Case 29
                        xNumber = xNumber & "ving-neuvi�me"
                    Case 30
                        xNumber = xNumber & "trenti�me"
                    Case 31
                        xNumber = xNumber & "trente et uni�me"
                    Case 32
                        xNumber = xNumber & "trente-deuxi�me"
                    Case 33
                        xNumber = xNumber & "trente-troisi�me"
                    Case 34
                        xNumber = xNumber & "trente-quatri�me"
                    Case 35
                        xNumber = xNumber & "trent-cinqui�me"
                    Case 36
                        xNumber = xNumber & "trent-sixi�me"
                    Case 37
                        xNumber = xNumber & "trent-septi�me"
                    Case 38
                        xNumber = xNumber & "trent-huiti�me"
                    Case 39
                        xNumber = xNumber & "trent-neuvi�me"
                    Case 40
                        xNumber = xNumber & "quaranti�me"
                    Case 41
                        xNumber = xNumber & "quarante et uni�me"
                    Case 42
                        xNumber = xNumber & "quarante-deuxi�me"
                    Case 43
                        xNumber = xNumber & "quarante-troisi�me"
                    Case 44
                        xNumber = xNumber & "quarante-quatri�me"
                    Case 45
                        xNumber = xNumber & "quarante-cinqui�me"
                    Case 46
                        xNumber = xNumber & "quarante-sixi�me"
                    Case 47
                        xNumber = xNumber & "quarante-septi�me"
                    Case 48
                        xNumber = xNumber & "quarante-huiti�me"
                    Case 49
                        xNumber = xNumber & "quarante-neuvi�me"
                    Case 50
                        xNumber = xNumber & "cinquanti�me"
                    Case 51
                        xNumber = xNumber & "cinquante et uni�me"
                    Case 52
                        xNumber = xNumber & "cinquante-deuxi�me"
                    Case 53
                        xNumber = xNumber & "cinquante-troisi�me"
                    Case 54
                        xNumber = xNumber & "cinquante-quatri�me"
                    Case 55
                        xNumber = xNumber & "cinquante-cinqui�me"
                    Case 56
                        xNumber = xNumber & "cinquante-sixi�me"
                    Case 57
                        xNumber = xNumber & "cinquante-septi�me"
                    Case 58
                        xNumber = xNumber & "cinquante-huiti�me"
                    Case 59
                        xNumber = xNumber & "cinquante-neuvi�me"
                    Case 60
                        xNumber = xNumber & "soixanti�me"
                    Case 61
                        xNumber = xNumber & "soixante et uni�me"
                    Case 62
                        xNumber = xNumber & "soixante-deuxi�me"
                    Case 63
                        xNumber = xNumber & "soixante-troisi�me"
                    Case 64
                        xNumber = xNumber & "soixante-quatri�me"
                    Case 65
                        xNumber = xNumber & "soixante-cinqui�me"
                    Case 66
                        xNumber = xNumber & "soixante-sixi�me"
                    Case 67
                        xNumber = xNumber & "soixante-septi�me"
                    Case 68
                        xNumber = xNumber & "soixante-huiti�me"
                    Case 69
                        xNumber = xNumber & "soixante-neuvi�me"
                    Case 70
                        xNumber = xNumber & "soixante-dixi�me"
                    Case 71
                        xNumber = xNumber & "soixante et onzi�me"
                    Case 72
                        xNumber = xNumber & "soixante-douzi�me"
                    Case 73
                        xNumber = xNumber & "soixante-treizi�me"
                    Case 74
                        xNumber = xNumber & "soixante-quatorzi�me"
                    Case 75
                        xNumber = xNumber & "soixante-quinzi�me"
                    Case 76
                        xNumber = xNumber & "soixante-seizi�me"
                    Case 77
                        xNumber = xNumber & "soixante-dix-septi�me"
                    Case 78
                        xNumber = xNumber & "soixante-dix-huiti�me"
                    Case 79
                        xNumber = xNumber & "soixante-dix-neuvi�me"
                    Case 80
                        xNumber = xNumber & "quatre-vingti�me"
                    Case 81
                        xNumber = xNumber & "quatre-vingt et uni�me"
                    Case 82
                        xNumber = xNumber & "quatre-vingt-deuxi�me"
                    Case 83
                        xNumber = xNumber & "quatre-vingt-troisi�me"
                    Case 84
                        xNumber = xNumber & "quatre-vingt-quatri�me "
                    Case 85
                        xNumber = xNumber & "quatre-vingt-cinqui�me"
                    Case 86
                        xNumber = xNumber & "quatre-vingt-sixi�me"
                    Case 87
                        xNumber = xNumber & "quatre-vingt-septi�me"
                    Case 88
                        xNumber = xNumber & "quatre-vingt-huiti�me"
                    Case 89
                        xNumber = xNumber & "quatre-vingt-neuvi�me"
                    Case 90
                        xNumber = xNumber & "quatre-vingt-dixi�me"
                    Case 91
                        xNumber = xNumber & "quatre-vingt-onzi�me"
                    Case 92
                        xNumber = xNumber & "quatre-vingt-douzi�me"
                    Case 93
                        xNumber = xNumber & "quatre-vingt-treizi�me"
                    Case 94
                        xNumber = xNumber & "quatre-vingt-quatorzi�me"
                    Case 95
                        xNumber = xNumber & "quatre-vingt-quinzi�me"
                    Case 96
                        xNumber = xNumber & "quatre-vingt-seizi�me"
                    Case 97
                        xNumber = xNumber & "quatre-vingt-dix-septi�me"
                    Case 98
                        xNumber = xNumber & "quatre-vingt-dix-huiti�me"
                    Case 99
                        xNumber = xNumber & "quatre-vingt-dix-neuvi�me"
                End Select
            Else
                If i > 1000 Then
                    i = 1
                    k = 1
                ElseIf i = 1000 Then
                    xNumber = "one thousandth"
                    k = 0
                Else
                    k = i
                End If
                
                If k > 99 Then
                    j = Int(k / 100)
                    Select Case j
                        Case 1
                            xNumber = "one hundred"
                        Case 2
                            xNumber = "two hundred"
                        Case 3
                            xNumber = "three hundred"
                        Case 4
                            xNumber = "four hundred"
                        Case 5
                            xNumber = "five hundred"
                        Case 6
                            xNumber = "six hundred"
                        Case 7
                            xNumber = "seven hundred"
                        Case 8
                            xNumber = "eight hundred"
                        Case 9
                            xNumber = "nine hundred"
                    End Select
                    If k Mod 100 = 0 Then
                        xNumber = xNumber & "th"
                    Else
                        xNumber = xNumber & Space(1)
                    End If
                    k = k - (j * 100)
                End If
                
                If k > 19 Then
                    j = Int(k / 10)
                    Select Case j
                        Case 2
                            xNumber = xNumber & "twen"
                        Case 3
                            xNumber = xNumber & "thir"
                        Case 4
                            xNumber = xNumber & "for"
                        Case 5
                            xNumber = xNumber & "fif"
                        Case 6
                            xNumber = xNumber & "six"
                        Case 7
                            xNumber = xNumber & "seven"
                        Case 8
                            xNumber = xNumber & "eigh"
                        Case 9
                            xNumber = xNumber & "nine"
                    End Select
                    If k Mod 10 = 0 Then
                        xNumber = xNumber & "tieth"
                    Else
                        xNumber = xNumber & "ty-"
                    End If
                    k = k - (j * 10)
                End If
                
                Select Case k
                    Case 1
                        xNumber = xNumber & "first"
                    Case 2
                        xNumber = xNumber & "second"
                    Case 3
                        xNumber = xNumber & "third"
                    Case 4
                        xNumber = xNumber & "fourth"
                    Case 5
                        xNumber = xNumber & "fifth"
                    Case 6
                        xNumber = xNumber & "sixth"
                    Case 7
                        xNumber = xNumber & "seventh"
                    Case 8
                        xNumber = xNumber & "eighth"
                    Case 9
                        xNumber = xNumber & "ninth"
                    Case 10
                        xNumber = xNumber & "tenth"
                    Case 11
                        xNumber = xNumber & "eleventh"
                    Case 12
                        xNumber = xNumber & "twelfth"
                    Case 13
                        xNumber = xNumber & "thirteenth"
                    Case 14
                        xNumber = xNumber & "fourteenth"
                    Case 15
                        xNumber = xNumber & "fifteenth"
                    Case 16
                        xNumber = xNumber & "sixteenth"
                    Case 17
                        xNumber = xNumber & "seventeenth"
                    Case 18
                        xNumber = xNumber & "eighteenth"
                    Case 19
                        xNumber = xNumber & "nineteenth"
                End Select
            End If
            
            xNumber = UCase(Left(xNumber, 1)) & _
                Right(xNumber, Len(xNumber) - 1)
                
        Case wdListNumberStyleCardinalText
            If g_lUILanguage = wdFrenchCanadian Then
                If i > 1000 Then
                    i = 1
                    k = 1
                ElseIf i = 1000 Then
                    xNumber = "mille"
                    k = 0
                Else
                    k = i
                End If
    
                If k > 99 Then
                    j = Int(k / 100)
                    Select Case j
                        Case 1
                            xNumber = "cent"
                        Case 2
                            xNumber = "deux cent"
                        Case 3
                            xNumber = "trois cent"
                        Case 4
                            xNumber = "quatre cent"
                        Case 5
                            xNumber = "cinq cent"
                        Case 6
                            xNumber = "six cent"
                        Case 7
                            xNumber = "sept cent"
                        Case 8
                            xNumber = "huit cent"
                        Case 9
                            xNumber = "neuf cent"
                    End Select
                    If k Mod 100 = 0 And j <> 1 Then
                        xNumber = xNumber & "s"
                    Else
                        xNumber = xNumber & Space(1)
                    End If
                    k = k - (j * 100)
                End If
    
                Select Case k
                    Case 1
                        xNumber = "un"
                    Case 2
                        xNumber = xNumber & "deux"
                    Case 3
                        xNumber = xNumber & "trois"
                    Case 4
                        xNumber = xNumber & "quatre"
                    Case 5
                        xNumber = xNumber & "cinq"
                    Case 6
                        xNumber = xNumber & "six"
                    Case 7
                        xNumber = xNumber & "sept"
                    Case 8
                        xNumber = xNumber & "huit"
                    Case 9
                        xNumber = xNumber & "neuf"
                    Case 10
                        xNumber = xNumber & "dix"
                    Case 11
                        xNumber = xNumber & "onze"
                    Case 12
                        xNumber = xNumber & "douze"
                    Case 13
                        xNumber = xNumber & "treize"
                    Case 14
                        xNumber = xNumber & "quatorze"
                    Case 15
                        xNumber = xNumber & "quinze"
                    Case 16
                        xNumber = xNumber & "seize"
                    Case 17
                        xNumber = xNumber & "dix-sept"
                    Case 18
                        xNumber = xNumber & "dix-huit"
                    Case 19
                        xNumber = xNumber & "dix-neuf"
                    Case 20
                        xNumber = xNumber & "vingt"
                    Case 21
                        xNumber = xNumber & "vingt et un"
                    Case 22
                        xNumber = xNumber & "vingt-deux"
                    Case 23
                        xNumber = xNumber & "vingt-trois"
                    Case 24
                        xNumber = xNumber & "vingt-quatre"
                    Case 25
                        xNumber = xNumber & "vingt-cinq"
                    Case 26
                        xNumber = xNumber & "vingt-six"
                    Case 27
                        xNumber = xNumber & "vingt-sept"
                    Case 28
                        xNumber = xNumber & "ving-huit"
                    Case 29
                        xNumber = xNumber & "ving-neuf"
                    Case 30
                        xNumber = xNumber & "trente"
                    Case 31
                        xNumber = xNumber & "trente et un"
                    Case 32
                        xNumber = xNumber & "trente-deux"
                    Case 33
                        xNumber = xNumber & "trente-trois"
                    Case 34
                        xNumber = xNumber & "trente-quatre"
                    Case 35
                        xNumber = xNumber & "trent-cinq"
                    Case 36
                        xNumber = xNumber & "trent-six"
                    Case 37
                        xNumber = xNumber & "trent-sept"
                    Case 38
                        xNumber = xNumber & "trent-huit"
                    Case 39
                        xNumber = xNumber & "trent-neuf"
                    Case 40
                        xNumber = xNumber & "quarante"
                    Case 41
                        xNumber = xNumber & "quarante et un"
                    Case 42
                        xNumber = xNumber & "quarante-deux"
                    Case 43
                        xNumber = xNumber & "quarante-trois"
                    Case 44
                        xNumber = xNumber & "quarante-quatre"
                    Case 45
                        xNumber = xNumber & "quarante-cinq"
                    Case 46
                        xNumber = xNumber & "quarante-six"
                    Case 47
                        xNumber = xNumber & "quarante-sept"
                    Case 48
                        xNumber = xNumber & "quarante-huit"
                    Case 49
                        xNumber = xNumber & "quarante-neuf"
                    Case 50
                        xNumber = xNumber & "cinquante"
                    Case 51
                        xNumber = xNumber & "cinquante et un"
                    Case 52
                        xNumber = xNumber & "cinquante-deux"
                    Case 53
                        xNumber = xNumber & "cinquante-trois"
                    Case 54
                        xNumber = xNumber & "cinquante-quatre"
                    Case 55
                        xNumber = xNumber & "cinquante-cinq"
                    Case 56
                        xNumber = xNumber & "cinquante-six"
                    Case 57
                        xNumber = xNumber & "cinquante-sept"
                    Case 58
                        xNumber = xNumber & "cinquante-huit"
                    Case 59
                        xNumber = xNumber & "cinquante-neuf"
                    Case 60
                        xNumber = xNumber & "soixante"
                    Case 61
                        xNumber = xNumber & "soixante et un"
                    Case 62
                        xNumber = xNumber & "soixante-deux"
                    Case 63
                        xNumber = xNumber & "soixante-trois"
                    Case 64
                        xNumber = xNumber & "soixante-quatre"
                    Case 65
                        xNumber = xNumber & "soixante-cinq"
                    Case 66
                        xNumber = xNumber & "soixante-six"
                    Case 67
                        xNumber = xNumber & "soixante-sept"
                    Case 68
                        xNumber = xNumber & "soixante-huit"
                    Case 69
                        xNumber = xNumber & "soixante-neuf"
                    Case 70
                        xNumber = xNumber & "soixante-dix"
                    Case 71
                        xNumber = xNumber & "soixante et onze"
                    Case 72
                        xNumber = xNumber & "soixante-douze"
                    Case 73
                        xNumber = xNumber & "soixante-treize"
                    Case 74
                        xNumber = xNumber & "soixante-quatorze"
                    Case 75
                        xNumber = xNumber & "soixante-quinze"
                    Case 76
                        xNumber = xNumber & "soixante-seize"
                    Case 77
                        xNumber = xNumber & "soixante-dix-sept"
                    Case 78
                        xNumber = xNumber & "soixante-dix-huit"
                    Case 79
                        xNumber = xNumber & "soixante-dix-neuf"
                    Case 80
                        xNumber = xNumber & "quatre-vingts"
                    Case 81
                        xNumber = xNumber & "quatre-vingt-un"
                    Case 82
                        xNumber = xNumber & "quatre-vingt-deux"
                    Case 83
                        xNumber = xNumber & "quatre-vingt-trois"
                    Case 84
                        xNumber = xNumber & "quatre-vingt-quatre"
                    Case 85
                        xNumber = xNumber & "quatre-vingt-cinq"
                    Case 86
                        xNumber = xNumber & "quatre-vingt-six"
                    Case 87
                        xNumber = xNumber & "quatre-vingt-sept"
                    Case 88
                        xNumber = xNumber & "quatre-vingt-huit"
                    Case 89
                        xNumber = xNumber & "quatre-vingt-neuf"
                    Case 90
                        xNumber = xNumber & "quatre-vingt-dix"
                    Case 91
                        xNumber = xNumber & "quatre-vingt-onze"
                    Case 92
                        xNumber = xNumber & "quatre-vingt-douze"
                    Case 93
                        xNumber = xNumber & "quatre-vingt-treize"
                    Case 94
                        xNumber = xNumber & "quatre-vingt-quatorze"
                    Case 95
                        xNumber = xNumber & "quatre-vingt-quinze"
                    Case 96
                        xNumber = xNumber & "quatre-vingt-seize"
                    Case 97
                        xNumber = xNumber & "quatre-vingt-dix-sept"
                    Case 98
                        xNumber = xNumber & "quatre-vingt-dix-huit"
                    Case 99
                        xNumber = xNumber & "quatre-vingt-dix-neuf"
                End Select
            Else
                If i > 1000 Then
                    i = 1
                    k = 1
                ElseIf i = 1000 Then
                    xNumber = "one thousand"
                    k = 0
                Else
                    k = i
                End If
                
                If k > 99 Then
                    j = Int(k / 100)
                    Select Case j
                        Case 1
                            xNumber = "one hundred"
                        Case 2
                            xNumber = "two hundred"
                        Case 3
                            xNumber = "three hundred"
                        Case 4
                            xNumber = "four hundred"
                        Case 5
                            xNumber = "five hundred"
                        Case 6
                            xNumber = "six hundred"
                        Case 7
                            xNumber = "seven hundred"
                        Case 8
                            xNumber = "eight hundred"
                        Case 9
                            xNumber = "nine hundred"
                    End Select
                    If k Mod 100 > 0 Then _
                        xNumber = xNumber & Space(1)
                    k = k - (j * 100)
                End If
                
                If k > 19 Then
                    j = Int(k / 10)
                    Select Case j
                        Case 2
                            xNumber = xNumber & "twenty"
                        Case 3
                            xNumber = xNumber & "thirty"
                        Case 4
                            xNumber = xNumber & "forty"
                        Case 5
                            xNumber = xNumber & "fifty"
                        Case 6
                            xNumber = xNumber & "sixty"
                        Case 7
                            xNumber = xNumber & "seventy"
                        Case 8
                            xNumber = xNumber & "eighty"
                        Case 9
                            xNumber = xNumber & "ninety"
                    End Select
                    If k Mod 10 > 0 Then _
                        xNumber = xNumber & "-"
                    k = k - (j * 10)
                End If
                
                Select Case k
                    Case 1
                        xNumber = xNumber & "one"
                    Case 2
                        xNumber = xNumber & "two"
                    Case 3
                        xNumber = xNumber & "three"
                    Case 4
                        xNumber = xNumber & "four"
                    Case 5
                        xNumber = xNumber & "five"
                    Case 6
                        xNumber = xNumber & "six"
                    Case 7
                        xNumber = xNumber & "seven"
                    Case 8
                        xNumber = xNumber & "eight"
                    Case 9
                        xNumber = xNumber & "nine"
                    Case 10
                        xNumber = xNumber & "ten"
                    Case 11
                        xNumber = xNumber & "eleven"
                    Case 12
                        xNumber = xNumber & "twelve"
                    Case 13
                        xNumber = xNumber & "thirteen"
                    Case 14
                        xNumber = xNumber & "fourteen"
                    Case 15
                        xNumber = xNumber & "fifteen"
                    Case 16
                        xNumber = xNumber & "sixteen"
                    Case 17
                        xNumber = xNumber & "seventeen"
                    Case 18
                        xNumber = xNumber & "eighteen"
                    Case 19
                        xNumber = xNumber & "nineteen"
                End Select
            End If
            
            xNumber = UCase(Left(xNumber, 1)) & _
                Right(xNumber, Len(xNumber) - 1)
                
        'new fixed digit numbering styles
        Case 62
            xNumber = "00" & i
        Case 63
            xNumber = "000" & i
        Case 64
            xNumber = "0000" & i
        
        Case Else
            xNumber = Str(i)
            
    End Select

    xIntToListNumStyle = LTrim(xNumber)
End Function

Public Function bIsValidFontSize(ByVal xText As String) As Boolean
    Dim bIsInvalid As Boolean
    Dim xDecimal As String
    Dim i As Integer
    Dim xChar As String

    If Not IsNumeric(xText) Then
        bIsInvalid = True
    ElseIf (CSng(xText) < 1) Or (CSng(xText) > 1638) Then
        bIsInvalid = True
    Else
        xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
        For i = 1 To Len(xText)
            xChar = Mid$(xText, i, 1)
            If (Not IsNumeric(xChar)) And (xChar <> xDecimal) Then
                bIsInvalid = True
                Exit For
            End If
        Next i
        
        If Not bIsInvalid Then
            bIsInvalid = (InStr(xText, xDecimal) > 0) And _
                    (Right$(xText, 2) <> xDecimal & "5")
        End If
    End If
    
    If bIsInvalid Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Ce n'est pas une taille de police valide."
        Else
            xMsg = "This is not a valid font size."
        End If
        MsgBox xMsg, vbExclamation, g_xAppName
    End If
    
    bIsValidFontSize = Not bIsInvalid
End Function

Public Function ContainsWord2010NumberStyle(ByVal xScheme As String, _
                                            ByVal iSchemeType As mpSchemeTypes, _
                                            Optional ByVal oDoc As Word.Document) As Boolean
'returns TRUE if specified scheme contains a list number style that's only available
'in documents with Word 2010 compatibility mode
    Dim oSource As Object
    Dim ltP As Word.ListTemplate
    Dim i As Integer
    Dim iNumberStyle As Integer
    
    On Error GoTo ProcError

'   get source
    If iSchemeType <> mpSchemeType_Document Then
        Set oSource = Source(iSchemeType)
        Set ltP = oSource.ListTemplates(xScheme)
    Else
        If oDoc Is Nothing Then _
            Set oDoc = ActiveDocument
        For Each ltP In oDoc.ListTemplates
            On Error Resume Next
            If Left(ltP.Name, Len(xScheme) + 1) = xScheme & "|" Then
                Exit For
            End If
        Next ltP
    End If
    
    'cycle through list levels
    For i = 1 To 9
        iNumberStyle = ltP.ListLevels(i).NumberStyle
        If (iNumberStyle > 59) And (iNumberStyle < 69) Then
            ContainsWord2010NumberStyle = True
            Exit Function
        End If
    Next i
    
    Exit Function
ProcError:
    RaiseError "mdlN90.ContainsWord2010NumberStyle"
    Exit Function
End Function

Public Sub DeleteFavoriteScheme(ByVal xScheme As String)
    Dim i As Integer
    Dim bFound As Boolean
    Dim xFavorite As String
    Dim vProps As Variant
    
    On Error GoTo ProcError
    
    'find ini key
    i = 1
    xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
    While (xFavorite <> "") And Not bFound
        vProps = Split(xFavorite, "|")
        bFound = (vProps(0) = xScheme)
        If Not bFound Then
            i = i + 1
            xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
        End If
    Wend
    
    If bFound Then
        'delete key
        WritePrivateProfileString "Numbering", "FavoriteScheme" & CStr(i), _
            vbNullString, g_xUserIni
        
        'rename subsequent keys
        i = i + 1
        xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
        While xFavorite <> ""
            WritePrivateProfileString "Numbering", "FavoriteScheme" & CStr(i - 1), _
                xFavorite, g_xUserIni
            WritePrivateProfileString "Numbering", "FavoriteScheme" & CStr(i), _
                vbNullString, g_xUserIni
            i = i + 1
            xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
        Wend
    End If
    
    Exit Sub
ProcError:
    RaiseError "mdlN90.DeleteFavoriteScheme"
    Exit Sub
End Sub

Public Sub UpdateParaStyles(ByVal xScheme As String)
'updates para styles left indent to match numbered styles left indent and
'next paragraph style - left indent won't happen automatically because numbered
'styles get left indent from list template
    Dim i As Integer
    Dim oStyle As Word.Style
    Dim iLevels As Integer
    Dim xStyleRoot As String
    Dim xStyle As String
    
    On Error GoTo ProcError
    
    xStyleRoot = xGetStyleRoot(xScheme)
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    For i = 1 To iLevels
        On Error Resume Next
        Set oStyle = ActiveDocument.Styles(xStyleRoot & " Para " & i)
        On Error GoTo ProcError
        If Not oStyle Is Nothing Then
            xStyle = xGetStyleName(xScheme, i, xStyleRoot)
            With ActiveDocument.Styles(xStyle)
                oStyle.ParagraphFormat.LeftIndent = .ParagraphFormat.LeftIndent
                oStyle.NextParagraphStyle = .NextParagraphStyle
            End With
            Set oStyle = Nothing
        End If
    Next i
    
    Exit Sub
ProcError:
    RaiseError "mdlN90.UpdateParaStyles"
    Exit Sub
End Sub

Sub ApplyContStyle(ByVal iLevel As Integer)
    Dim xScheme As String
    Dim oLT As ListTemplate
    Dim xStyle As String
    Dim iPos As Integer
    Dim oPara As Word.Paragraph
    Dim xRoot As String
    Dim oStyle As Word.Style
    Dim bFound As Boolean
    Dim rngPara As Word.Range
    Dim xChar As String
    Dim rngSelection As Word.Range
    Dim bShowAll As Boolean
    Dim iLevels As Integer
    Dim i As Integer
    Dim xCont As String

    On Error GoTo ProcError

'   turn on show all - this will prevent error when preceding paragraph is hidden
    With ActiveWindow.View
        bShowAll = .ShowAll
        .ShowAll = True
    End With

    'get active scheme
    xScheme = xActiveScheme(ActiveDocument)
    If xScheme = "" Then _
        Exit Sub
    xRoot = xGetStyleRoot(xScheme)
    xStyle = xRoot & " Cont " & iLevel
    
    'ensure existence of cont style
    On Error Resume Next
    Set oStyle = ActiveDocument.Styles(xStyle)
    On Error GoTo ProcError
    If oStyle Is Nothing Then
        If g_iLoadContStyles <> mpLoadIndividuallyOnDemand Then '9.9.6004
            iLevels = iGetLevels(xScheme, mpSchemeType_Document)
            For i = 1 To iLevels
                xCont = xRoot & " Cont " & i
                On Error Resume Next
                Set oStyle = ActiveDocument.Styles(xCont)
                On Error GoTo ProcError
                If oStyle Is Nothing Then
                    CreateContStyles xScheme, i, , True
                End If
                Set oStyle = Nothing
            Next i
        Else
            CreateContStyles xScheme, iLevel, , True
        End If
    End If
    
    Set rngSelection = Selection.Range

    For Each oPara In rngSelection.Paragraphs
        Set rngPara = oPara.Range

        'if mp list template, delete trailing characters
        On Error Resume Next
        Set oLT = rngPara.Characters(1).ListFormat.ListTemplate
        On Error GoTo ProcError
        If Not oLT Is Nothing Then
            xScheme = xGetLTRoot(oLT.Name)
            If xScheme <> "" Then
                If bIsMPListTemplate(oLT) Then
                    With rngPara
                        xChar = Left(.Text, 1)
                        If xChar = Chr(11) Or xChar = Chr(32) Then
                            .StartOf
                            .MoveEndWhile xChar, 2
                            .Delete
                        End If
                    End With
                End If
            End If
        End If
            
        'apply style
        oPara.Style = xStyle
    Next oPara

'   restore user's setting
    ActiveWindow.View.ShowAll = bShowAll

    Exit Sub
ProcError:
    RaiseError "mdlN90.ApplyContStyle"
    Exit Sub
End Sub

Private Function GetQATFile() As Scripting.File
    Dim oFSO As FileSystemObject
    Dim oReg As CRegistry
    Dim vValue As Variant
    Dim xValueName As String

    On Error GoTo ProcError
   
    Set oReg = New CRegistry
    Set oFSO = New FileSystemObject
            
    vValue = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_CURRENT_USER, "Software\Microsoft\Office\14.0\Common\Toolbars", _
        "CustomUIRoaming", vValue)
    If RTrim(vValue) = "1" Then
        xValueName = "AppData"
    Else
        xValueName = "Local AppData"
    End If
    lRet = oReg.GetValue(HKEY_CURRENT_USER, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", _
        xValueName, vValue)
    Set GetQATFile = oFSO.GetFile(RTrim(vValue) & "\Microsoft\OFFICE\Word.officeUI")
    Exit Function
ProcError:
    Exit Function
End Function

Private Function bCopyContStylesFromSource(ByVal xScheme As String, _
        ByVal iLevel As Integer, ByRef iOrigin As mpSchemeTypes) As Boolean
    Dim bOriginFound As Boolean
    Dim tmpSource As Word.Template
    Dim xStyleRoot As String
    Dim i As Integer
    Dim iStart As Integer
    Dim iEnd As Integer
    Dim xStyle As String
    Dim xStyleSourceFile As String
    Dim xStyleDestFile As String
    
    On Error GoTo ProcError
    
    iOrigin = iGetSchemeOrigin(xScheme)
    Set tmpSource = Source(iOrigin)
    
'   check for scheme in origin mpNumbers.sty;
'   user may have deleted it or it may have
'   come from another user's mpNumbers.sty
    On Error Resume Next
    If (iOrigin = mpSchemeType_Private) And _
            (g_xPSchemes(0, 0) <> "") Then
        bOriginFound = bIsMPListTemplate( _
                tmpSource.ListTemplates(xScheme))
    End If
    
'   if not found in personal or if firm is the origin,
'   check for scheme in firm
    If Not bOriginFound Then
        iOrigin = mpSchemeType_Public
        If g_xFSchemes(0, 0) <> "" Then
            bOriginFound = bIsMPListTemplate(g_oFNumSty.ListTemplates(xScheme))
        End If
    End If
    
    If bOriginFound Then
        If iOrigin = mpSchemeType_Private Then
            xStyleSourceFile = g_xPNumSty
        Else
            xStyleSourceFile = g_xFNumSty
        End If
        xStyleDestFile = WordBasic.FileName$()

        xStyleRoot = xGetStyleRoot(xScheme)
        If iLevel = 0 Then
            iStart = 1
            iEnd = iGetLevels(xScheme, mpSchemeType_Document)
        Else
            iStart = iLevel
            iEnd = iLevel
        End If
        
        On Error Resume Next
        For i = iStart To iEnd
            xStyle = xStyleRoot & " Cont " & i
            Application.OrganizerCopy xStyleSourceFile, _
                                      xStyleDestFile, _
                                      xStyle, _
                                      wdOrganizerObjectStyles
            If Err > 0 Then _
                Exit Function
        Next i
        On Error GoTo ProcError
        
        bCopyContStylesFromSource = True
    End If
        
    Exit Function
ProcError:
    RaiseError "mdlN90.bCopyContStylesFromSource"
    Exit Function
End Function

Private Sub StoreContStyles(ByVal xScheme As String)
    Dim oStyle As Word.Style
    Dim i As Integer
    Dim xStyleRoot As String
    
    On Error GoTo ProcError
    
    xStyleRoot = xGetStyleRoot(xScheme)
    
    For i = 1 To 9
        On Error Resume Next
        Set oStyle = ActiveDocument.Styles(xStyleRoot & " Cont " & i)
        On Error GoTo ProcError
        m_bContStyles(i - 1) = (Not oStyle Is Nothing)
        Set oStyle = Nothing
    Next i
        
    Exit Sub
ProcError:
    RaiseError "mdlN90.StoreContStyles"
    Exit Sub
End Sub

Private Sub RestoreContStyles(ByVal xScheme As String)
    Dim oStyle As Word.Style
    Dim xStyleRoot As String
    Dim i As Integer
    
    On Error GoTo ProcError
    
    '9.9.6004 - if cont styles are not set to LoadIndividuallyOnDemand
    'and any one style exists, leave them all in - this is simpler
    'than trying to track which of them got added in the course
    'of edit scheme
    If g_iLoadContStyles <> mpLoadIndividuallyOnDemand Then
        For i = 1 To 9
            If m_bContStyles(i - 1) Then _
                Exit Sub
        Next i
    End If
    
    xStyleRoot = xGetStyleRoot(xScheme)
    
    For i = 1 To 9
        On Error Resume Next
        Set oStyle = ActiveDocument.Styles(xStyleRoot & " Cont " & i)
        On Error GoTo ProcError
        If Not oStyle Is Nothing Then
            If Not m_bContStyles(i - 1) Then
                oStyle.Delete
            End If
            Set oStyle = Nothing
        End If
    Next i
        
    Exit Sub
ProcError:
    RaiseError "mdlN90.RestoreContStyles"
    Exit Sub
End Sub

Function GetFavoriteSchemesXML() As String
    Dim xXML As String
    Dim iSchemes As Integer
    Dim i As Integer
    Dim iType As mpSchemeTypes
    Dim xDisplayName As String
    Dim xFavorite As String
    Dim vProps As Variant
    Dim xFavorites As String
    Dim vFavorites As Variant
    
    On Error GoTo ProcError
    
    i = 1
    xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
    If xFavorite = "" Then Exit Function
    While xFavorite <> ""
        vProps = Split(xFavorite, "|")
        If UBound(vProps) = 2 Then
            'make sure scheme still exists
            If vProps(2) = CStr(mpFavoritePrivate) Then
                iType = mpSchemeType_Private
            Else
                iType = mpSchemeType_Public
            End If
            If bSchemeExists(vProps(0), vProps(1), iType) Then
                xFavorites = xFavorites & xFavorite & "�"
            End If
        End If
        i = i + 1
        xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
    Wend
        
    'limit to 10
    iSchemes = lCountChrs(xFavorites, "�")
    iSchemes = mpMin(CDbl(iSchemes), 10)
    xFavorites = Left$(xFavorites, Len(xFavorites) - 1)
    vFavorites = Split(xFavorites, "�")
       
    'populate array for use by OnAction macros
    ReDim g_xFavoriteSchemes(iSchemes - 1, 2)
    For i = 0 To iSchemes - 1
        vProps = Split(vFavorites(i), "|")
        g_xFavoriteSchemes(i, 0) = vProps(0)
        g_xFavoriteSchemes(i, 1) = vProps(1)
        g_xFavoriteSchemes(i, 2) = vProps(2)
    Next i
    WordBasic.SortArray g_xFavoriteSchemes(), 0, 0, iSchemes - 1, 0, 1
        
    'add menu item for each scheme
    For i = 0 To iSchemes - 1
        'GLOG 4977 (2/1/12) - replace ampersand
        xDisplayName = Replace(g_xFavoriteSchemes(i, 1), "&", "&amp;&amp;")
        
        'add menu item
        If g_lUILanguage = wdFrenchCanadian Then
            xXML = xXML & "<button id=" & """" & "mnuFavorites" & i & """" & " label=" & _
                """" & xDisplayName & """" & " onAction=" & """" & "zzmpCallback" & _
                """" & " tag=" & """" & "zzmpUseFavoriteSchemeFromRibbon" & CStr(i + 1) & """" & _
                " screentip=" & """" & "Utilisez le th�me " & xDisplayName & """" & _
                "/>" & vbCrLf
        Else
            xXML = xXML & "<button id=" & """" & "mnuFavorites" & i & """" & " label=" & _
                """" & xDisplayName & """" & " onAction=" & """" & "zzmpCallback" & _
                """" & " tag=" & """" & "zzmpUseFavoriteSchemeFromRibbon" & CStr(i + 1) & """" & _
                " screentip=" & """" & "Use the " & xDisplayName & " scheme" & """" & _
                "/>" & vbCrLf
        End If
    Next i
    
    GetFavoriteSchemesXML = xXML
    
    Exit Function
ProcError:
    RaiseError "mdlN90.GetFavoriteSchemesXML"
    Exit Function
End Function

Private Sub SetContStyleProperties(ByVal xScheme As String, ByVal iLevel As Integer, _
        ByVal iOrigin As mpSchemeTypes)
    Dim i As Integer
    Dim iLineSpacing As WdLineSpacing
    Dim xCont As String
    Dim xStyleRoot As String
    Dim styCont As Word.Style
    Dim styNormal As Word.Style
    Dim pfNormal As Word.ParagraphFormat
    Dim bIsMPPleadingStyle As Boolean
    Dim lLanguage As Long
    Dim sNormalSpacing As Single
    Dim styNext As Word.Style
    Dim lNextAlignment As Long
    Dim sNextSpacing As Single
    Dim lNextRule As Long
    Dim sNextBefore As Single
    Dim sNextAfter As Single
    Dim styContNext As Word.Style
    Dim lContNextAlignment As Long
    Dim sContNextSpacing As Single
    Dim lContNextRule As Long
    Dim sContNextBefore As Single
    Dim sContNextAfter As Single
    Dim sLines As Single
    Dim bIsExact As Boolean
    Dim iTrailUnderline As Integer
    Dim iAlignment As Integer
    Dim bBaseOnNormalFont As Boolean
    Dim bAdjustSpacing As Boolean
    Dim oScheme As CNumScheme
    
    On Error GoTo ProcError
    
    Set oScheme = GetRecord(xScheme, iOrigin)
    bBaseOnNormalFont = oScheme.DynamicFonts
    bAdjustSpacing = oScheme.DynamicSpacing
    
    Set styNormal = ActiveDocument.Styles(wdStyleNormal)
    Set pfNormal = styNormal.ParagraphFormat
    xStyleRoot = xGetStyleRoot(xScheme)
    xCont = xStyleRoot & " Cont" & " " & iLevel
    lLanguage = styNormal.LanguageID
    
'   in generic pleading schemes, we're adjusting space after
'   to space before when line spacing is exact, i.e. pleading paper
    bIsMPPleadingStyle = g_bAlwaysAdjustSpacing
    If Not bIsMPPleadingStyle Then
        If g_xPleadingSchemes(0) <> "" Then
            For i = 0 To UBound(g_xPleadingSchemes)
                If UCase(g_xPleadingSchemes(i)) = UCase(xStyleRoot) Then
                    bIsMPPleadingStyle = True
                    Exit For
                End If
            Next i
        End If
    End If
        
    With ActiveDocument.Styles
        Set styCont = .Item(xCont)
        On Error Resume Next
        Set styContNext = .Item(xStyleRoot & " Cont " & (iLevel + 1)) 'GLOG 5494
        On Error GoTo ProcError
    End With
        
'   adjust language and font
    If bBaseOnNormalFont Then
        With styCont.Font
            .Name = styNormal.Font.Name
            .Size = styNormal.Font.Size
        End With
    End If
    styCont.LanguageID = lLanguage
    
'   do para formats
'   store subsequent level attributes
    If Not (styContNext Is Nothing) Then
        With styContNext.ParagraphFormat
            lContNextAlignment = .Alignment
            lContNextRule = .LineSpacingRule
            sContNextSpacing = .LineSpacing
            sContNextBefore = .SpaceBefore
            sContNextAfter = .SpaceAfter
        End With
    End If
        
'   right alignment vs. base on normal is now stored in
'   TrailUnderline level prop
    iTrailUnderline = xGetLevelProp(xScheme, iLevel, _
        mpNumLevelProp_TrailUnderline, mpSchemeType_Document) 'GLOG 5494
    If bBitwisePropIsTrue(iTrailUnderline, _
            mpTrailUnderlineField_AdjustContToNormal) Then
        styCont.ParagraphFormat.Alignment = pfNormal.Alignment
    End If
                
    If bAdjustSpacing Then
'       set line spacing based on line spacing
'       rule in Normal style of active doc
        If pfNormal.LineSpacingRule = wdLineSpaceExactly Then
            sNormalSpacing = pfNormal.LineSpacing
            With styCont.ParagraphFormat
'               get current line spacing
                iLineSpacing = .LineSpacingRule
                bIsExact = ((iLineSpacing = wdLineSpaceAtLeast) Or _
                    (iLineSpacing = wdLineSpaceExactly))
                    
'               convert to exact spacing
                If Not bIsExact Then
                    sLines = .LineSpacing / 12
                    .LineSpacingRule = wdLineSpaceExactly
                    .LineSpacing = sNormalSpacing * sLines
                End If
        
'               adjust before/after in MP pleading style
                If iLineSpacing = wdLineSpaceSingle Then
                    If bIsMPPleadingStyle And _
                            (.SpaceAfter = 12) And _
                            (.SpaceBefore = 0) Then
                        .SpaceAfter = 0
                        .SpaceBefore = 12
                    End If
                End If
            End With
        ElseIf pfNormal.LineSpacingRule = wdLineSpaceMultiple Then
            'GLOG5683 - added branch
            sNormalSpacing = pfNormal.LineSpacing
            
            With styCont.ParagraphFormat
    '           get current line spacing
                iLineSpacing = .LineSpacingRule
    
    '           convert to multiple spacing
                If (iLineSpacing = wdLineSpaceSingle) Or _
                        (iLineSpacing = wdLineSpace1pt5) Or _
                        (iLineSpacing = wdLineSpaceDouble) Then
                    sLines = .LineSpacing / 12
                    .LineSpacingRule = wdLineSpaceMultiple
                    .LineSpacing = sNormalSpacing * sLines
                End If
            End With
        ElseIf pfNormal.LineSpacingRule = _
                wdLineSpaceSingle Then
'           single/double - adjust only "pleading" schemes
            If bIsMPPleadingStyle Then
                With styCont.ParagraphFormat
                    If (.LineSpacingRule = wdLineSpaceSingle) And _
                            (.SpaceAfter = 0) And _
                            (.SpaceBefore = 12) Then
                        .SpaceAfter = 12
                        .SpaceBefore = 0
                    End If
                End With
            End If 'pleading style
        End If 'line spacing rule
    End If 'adjust
                
'   restore subsequent level attributes
    If Not (styContNext Is Nothing) Then
        With styContNext.ParagraphFormat
            .Alignment = lContNextAlignment
            .LineSpacingRule = lContNextRule
            .LineSpacing = sContNextSpacing
            .SpaceBefore = sContNextBefore
            .SpaceAfter = sContNextAfter
        End With
    End If
        
    Exit Sub
ProcError:
    RaiseError "mdlN90.SetContStyleProperties"
    Exit Sub
End Sub

Sub SetPreviewZoom(ByVal bZoomIn As Boolean, Optional ByVal lLoopLength As Long = 100)
'added in 9.9.6006 (GLOG 5549)
'added lLoopLength parameter in 9.9.6012
    Dim Preview As MPN90.CPreview
    On Error GoTo ProcError
    
    EchoOff
    
    With Word.Application.ActiveWindow
        .DisplayRulers = False
        Set Preview = New MPN90.CPreview
        If bZoomIn Then
            Preview.ZoomIn
        Else
            Preview.ZoomOut
        End If
        Set Preview = Nothing
        .DisplayRulers = True
    End With
    
    If g_iWordVersion = mpWordVersion_2013 Then
        Dim l As Long
        For l = 1 To lLoopLength
            DoEvents
        Next l
    End If
    
    EchoOn
    
    Exit Sub
ProcError:
    RaiseError "mdlN90.SetPreviewZoom"
    Exit Sub
End Sub
