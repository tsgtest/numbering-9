Attribute VB_Name = "mpPleadingPaper"
Option Explicit

Function bInsert(secScope As Word.Section, _
                 xType As String, _
                 fpsSection As PageSetup, _
                 xFont As String, _
                 sFontSize As Single, _
                 lOffice As Long, _
                 xFooterBP As String, _
                 Optional xDocTitle As String, _
                 Optional vBodyTextAlignment As Variant, _
                 Optional vBodyTextFirstIndent As Variant, _
                 Optional bFirstPagePageNum As Boolean = False, _
                 Optional bOtherPagesPageNum As Boolean = False, _
                 Optional bIncludeFirmName As Boolean = True, _
                 Optional xPageNumFormat As String = "", _
                 Optional xPageNumIntroChr As String = "-", _
                 Optional xPageNumTrailingChr As String = "-", _
                 Optional vFirmNameLandscape As Variant) As Long
    
    Dim sTopMargin As Single
    Dim sBotMargin As Single
    Dim iFirmName As Integer
    Dim rngHeader As Word.Range
    Dim rngFooter As Word.Range
    Dim rngMain As Word.Range
    Dim rngFirmName As Word.Range
    Dim xPaper As String
    Dim iType As Integer
    Dim hdrDefined As HeaderFooter
    Dim ftrDefined As HeaderFooter
    Dim rngPageNum As Word.Range
    Dim fldPageNum As Word.Field
    Dim sWidth As Single
    
'   get office detail
    If xFirmName = "" Then _
        xFirmName = xAppGetFirmINIValue("Firm", "Name")
        
'    Set rOffice = rGetOfficeDetail(lOffice)
    
'   suppress top line spacing
    ActiveDocument.Compatibility(wdSuppressTopSpacing) = True

'   set page setup
    Select Case UCase(xType)
        Case "28-LINE"
            sTopMargin = Pleading28TopMargin
            sBotMargin = Pleading28BotMargin
            xPaper = "zzmpPleadingLineNo"
            iType = mpPleadingPaper28Line
            
        Case "26-LINE"
            sTopMargin = Pleading26TopMargin
            sBotMargin = Pleading26BotMargin
            xPaper = "zzmpPleadingLineNo26"
            iType = mpPleadingPaper26Line
            
        Case Else
            sTopMargin = TopMarginDefault
            sBotMargin = BottomMarginDefault
            xPaper = "zzmpNothing"
            iType = mpPleadingPaperNone
    End Select

    With secScope.PageSetup
        .TopMargin = InchesToPoints(sTopMargin)
        .BottomMargin = InchesToPoints(sBotMargin)
        .LeftMargin = fpsSection.LeftMargin
        .RightMargin = fpsSection.RightMargin
        .HeaderDistance = InchesToPoints(PleadingHeaderDistance)
        .FooterDistance = InchesToPoints(PleadingFooterDistance)
    End With

'---use special boilerplate if firm name landscaped -
'   if arg is not supplied, get from ini
    If IsMissing(vFirmNameLandscape) Then
        If UCase(xAppGetFirmINIValue(TemplatePleading, _
                "LandscapedFirmName")) = "YES" Then
            vFirmNameLandscape = True
        Else
            vFirmNameLandscape = False
        End If
    End If
            
    If vFirmNameLandscape And bIncludeFirmName Then _
        xPaper = xPaper & "FN"
        
'---set document variable (for possible later use)
    If Not bIncludeFirmName Then
        iFirmName = mpPleadingFirmNameNone
    Else
        If Not vFirmNameLandscape Then
            iFirmName = mpPleadingFirmNameInFooter
        Else
            iFirmName = mpPleadingFirmNameLandscaped
        End If
    End If

    bRet = bSetPleadingPaperType(ActiveDocument, _
                                iType, _
                                iFirmName)

'   Work Headers-
'   width of 2nd column in pleading paper
'   table must be changed to match margins
    With secScope.PageSetup
        sWidth = .PageWidth - (.LeftMargin + .RightMargin) + 12
    End With

    For Each hdrDefined In secScope.Headers
        hdrDefined.LinkToPrevious = False

        If hdrDefined.Exists Then
            Set rngHeader = hdrDefined.Range
            
'---        insert boilerplate
            With rngHeader
                .InsertFile xBP, xPaper
                .WholeStory
                If iType <> mpPleadingPaperNone Then
                    .Tables(1).Columns.Last.Width = sWidth
                Else
                    bRet = rngAdjustHeaderTabs(rngHeader)
                End If
            End With

    
            If vFirmNameLandscape Then
'---            add the office name -- landscaped
'                rngInsertAt xLabel:="(Office)", _
'                            xText:=rOffice!fldOfficeName, _
'                            vSearch:=rngHeader
    
'---            Reset the range
                rngHeader.WholeStory
                
'               set font, font size
                With rngHeader.Tables(1).Cell(1, 2).Range.Font
                    .Name = xFont
                    .Size = sFontSize
                End With
                With rngHeader.Tables(1).Cell(1, 3).Range.Font
                    .Name = xFont
                    .Size = sFontSize
                End With
                
'               add firm name
                Set rngFirmName = rngInsertAt(xLabel:="(Firm Name)", _
                                            xText:=xFirmName, _
                                            vSearch:=rngHeader)
            Else
                With rngHeader.Font
                    .Name = xFont
                    .Size = sFontSize
                End With
            End If  '---landscaped firm name''
            
        End If
    Next hdrDefined


'---Work footers
    For Each ftrDefined In secScope.Footers
        ftrDefined.LinkToPrevious = False
        If ftrDefined.Exists Then
            Set rngFooter = ftrDefined.Range
            rngFooter.InsertFile xBP, xFooterBP
    
'            rngInsertAt xLabel:="(Office)", _
'                        xText:=rOffice!fldOfficeName, _
'                        vSearch:=rngFooter
                        
'---        Reset the footer range
            rngFooter.WholeStory
            Set rngFirmName = rngInsertAt(xLabel:="(Firm Name)", _
                                          xText:=xFirmName, _
                                          vSearch:=rngFooter)
    
'---        Delete firm name contents from footer table if specified
            If (Not bIncludeFirmName) Or (vFirmNameLandscape) Then
                rngFirmName.MoveEnd wdParagraph, 1
                rngFirmName.Delete
            End If
    
'           insert doc title
            rngFooter.WholeStory
            Set rngFirmName = rngInsertAt(xLabel:="(Doc Title)", _
                                          xText:=xDocTitle, _
                                          vSearch:=rngFooter)
'           set font, font size
            If Not (rngFirmName Is Nothing) Then
                With rngFirmName.Font
                    .Name = xFont
                    .Size = sFontSize
                End With
            End If
        End If
    Next ftrDefined
    
'********************************************************
'   ***do items that may be different for each footer***
'********************************************************

'---add page no field code if specified
    Set rngPageNum = rngInsertAt(xLabel:="-(Page)-", _
                                xText:="", _
                                vSearch:=secScope _
                                .Footers(wdHeaderFooterFirstPage).Range)
    
    If bFirstPagePageNum Then
        With rngPageNum
            .InsertAfter xPageNumTrailingChr
            .StartOf
            Set fldPageNum = .Fields.Add(rngPageNum, _
                                         wdFieldPage, _
                                         xPageNumFormat)
            Set rngPageNum = rngGetField(fldPageNum.Code)
            .InsertBefore xPageNumIntroChr
            
'           set font, font size
            With .Cells(1).Range
                .Font.Name = xFont
                .Font.Size = sFontSize
            End With
        End With
    End If
    
'   do primary footer page numbers
    Set rngPageNum = rngInsertAt(xLabel:="-(Page)-", _
                                xText:="", _
                                vSearch:=secScope _
                                .Footers(wdHeaderFooterPrimary).Range)
                                    
'   set font, font size
    With rngPageNum.Font
        .Name = xFont
        .Size = sFontSize
    End With
            
    If bOtherPagesPageNum Then
        With rngPageNum
            .InsertAfter xPageNumTrailingChr
            .StartOf
            Set fldPageNum = .Fields.Add(rngPageNum, _
                                         wdFieldPage, _
                                         xPageNumFormat)
            Set rngPageNum = rngGetField(fldPageNum.Code)
            .InsertBefore xPageNumIntroChr
        End With
    End If
bInsert = Err
Err = 0
End Function

Function bDetail(iPleadingPaperType As Integer, _
                 Optional xBPRange As String, _
                 Optional sTopMargin As Single, _
                 Optional sBottomMargin As Single, _
                 Optional sLeftMargin As Single, _
                 Optional sRightMargin As Single, _
                 Optional sHeaderDistance As Single, _
                 Optional sFooterDistance As Single, _
                 Optional bIsTOCTOASection As Boolean = False) As Boolean
                         
'fills args with appropriate values
'for type iPleadingPaperType -
'**************************************************
'   THIS FUNCTION SHOULD REALLY REFERENCE THE
'   PUBLIC DB TO GET THESE VALUES - IN THE FUTURE
'   SOMEONE SHOULD CODE THIS AS SUCH
'**************************************************
    Select Case iPleadingPaperType
        Case mpPleadingPaperNone
            If bIsTOCTOASection Then
                xBPRange = "zzmpTOCHeader_Primary"
                sTopMargin = mpTopMarginTOC
                sBottomMargin = mpBottomMarginTOC
                sLeftMargin = mpLeftMarginTOC
                sRightMargin = mpRightMarginTOC
                sHeaderDistance = mpHeaderDistTOC
                sFooterDistance = mpFooterDistTOC
            Else
                xBPRange = "zzmpNothing"
                sTopMargin = TopMarginDefault
                sBottomMargin = BottomMarginDefault
                sLeftMargin = LeftMarginDefault
                sRightMargin = RightMarginDefault
                sHeaderDistance = HeaderDistDefault
                sFooterDistance = FooterDistDefault
            End If
        
        Case mpPleadingPaper28Line
            xBPRange = "zzmpPleadingLineNo_TOHeader"
            sTopMargin = mpPleadingTOCTopMargin
            sBottomMargin = 0.5
            sLeftMargin = Pleading28LeftMargin
            sRightMargin = Pleading28RightMargin
            sHeaderDistance = PleadingHeaderDistance
            sFooterDistance = PleadingFooterDistance
            
        Case mpPleadingPaper28LineFNLand
            xBPRange = "zzmpPleadingLineNo_TOHeaderFN"
            sTopMargin = mpPleadingTOCTopMargin
            sBottomMargin = 0.5
            sLeftMargin = Pleading28LeftMargin
            sRightMargin = Pleading28RightMargin
            sHeaderDistance = PleadingHeaderDistance
            sFooterDistance = PleadingFooterDistance
        
        Case mpPleadingPaper28LineFNPort
            xBPRange = "zzmpPleadingLineNo_TOHeader"
            sTopMargin = mpPleadingTOCTopMargin
            sBottomMargin = 0.5
            sLeftMargin = Pleading28LeftMargin
            sRightMargin = Pleading28RightMargin
            sHeaderDistance = PleadingHeaderDistance
            sFooterDistance = PleadingFooterDistance
        
        Case mpPleadingPaper26Line
            xBPRange = "zzmpPleadingLineNo26_TOHeader"
            sTopMargin = mpPleading26TOCTopMargin
            sBottomMargin = Pleading26BotMargin
            sLeftMargin = Pleading26LeftMargin
            sRightMargin = Pleading26RightMargin
            sHeaderDistance = PleadingHeaderDistance
            sFooterDistance = PleadingFooterDistance
        
        Case mpPleadingPaper26LineFNLand
            xBPRange = "zzmpPleadingLineNo26_TOHeaderFN"
            sTopMargin = mpPleading26TOCTopMargin
            sBottomMargin = Pleading26BotMargin
            sLeftMargin = Pleading26LeftMargin
            sRightMargin = Pleading26RightMargin
            sHeaderDistance = PleadingHeaderDistance
            sFooterDistance = PleadingFooterDistance
        
        Case mpPleadingPaper26LineFNPort
            xBPRange = "zzmpPleadingLineNo26_TOHeader"
            sTopMargin = mpPleading26TOCTopMargin
            sBottomMargin = Pleading26BotMargin
            sLeftMargin = Pleading26LeftMargin
            sRightMargin = Pleading26RightMargin
            sHeaderDistance = PleadingHeaderDistance
            sFooterDistance = PleadingFooterDistance
    End Select
    

End Function


Public Function iPleadingPaperType(rngHeader As Word.Range) As Integer
    With rngHeader.Find
        .Text = "26" & Chr(11) & "27" & Chr(11) & "28"
        .Execute
        If .Found Then
            iPleadingPaperType = 28
        Else
            .Text = "24" & Chr(11) & "25" & Chr(11) & "26"
            .Execute
            If .Found Then
                iPleadingPaperType = 26
            Else
                iPleadingPaperType = 0
            End If
        End If
    End With
End Function
Public Function rngSetFirmNameTarget(rngTarget As Word.Range, _
                            secSection As Word.Section) As Word.Range
    
    Dim iNumHeaderCols As Integer
    Dim iNumFooterCols As Integer
    
    Dim iFirmNameType
    
'---determine if landscaped header col exists
    iNumHeaderCols = iFooterType(secSection _
                        .Headers(wdHeaderFooterPrimary))
                            
    If iNumHeaderCols <> 3 Then    'firm name in footer
'---        check the footer
        iNumFooterCols = iFooterType(secSection _
                        .Footers(wdHeaderFooterPrimary))
                
        If iNumFooterCols = 0 Then     'no table, no firm name
            Set rngSetFirmNameTarget = Nothing
        Else
            Set rngSetFirmNameTarget = rngTarget.Tables(1) _
                                .Cell(Row:=1, Column:=1).Range
        End If
    
    ElseIf iNumHeaderCols = 3 Then  'firm name in header
        Set rngSetFirmNameTarget = rngTarget.Tables(1) _
                                    .Cell(1, 1).Range
    End If
            
End Function
Public Function iGetPleadingPaperType(docDocument As Word.Document) As Integer
    Dim xPPaper As String
    Dim xPrefix As String
    Dim xSuffix As String
    Dim iLines As Integer
    Dim rngHeader As Word.Range
    Dim rngFooter As Word.Range
    Dim iNumHeaderCols As Integer
    Dim iNumFooterCols As Integer
    Dim rngFNTarget As Word.Range
    Dim secScope As Word.Section
    Dim iTestLen As Integer
    Dim xTest As String
    Dim dVar As Variable
    Dim bDVExists As Boolean
    
'---First check for PP doc var
    For Each dVar In docDocument.Variables
        If dVar.Name = "zzmpFixed_PleadingPaperType" Then
            bDVExists = True
            Exit For
        End If
    Next
    
    If bDVExists Then
        xPPaper = docDocument.Variables("zzmpFixed_PleadingPaperType")
    Else
'---See if there's pleading paper at all
        Set secScope = docDocument.Sections.First
        Set rngHeader = docDocument.Sections.First.Headers _
                                (wdHeaderFooterPrimary).Range
        Set rngFooter = docDocument.Sections.First.Footers _
                                (wdHeaderFooterPrimary).Range
        
        iLines = iPleadingPaperType(rngHeader)
    
        If iLines = 0 Then      'no pleading paper
            iGetPleadingPaperType = mpPleadingPaperNone
            Exit Function
        Else                    'determine paper type
            Select Case iLines
                Case 26
                    xPrefix = "PP26"
                
                Case 28
                    xPrefix = "PP28"
                
                Case Else
            End Select
        '---now check for a) three cols in header
            iNumHeaderCols = iFooterType(secScope _
                            .Headers(wdHeaderFooterPrimary))
            
            If iNumHeaderCols = 3 Then  'check for firm name text in first col
                
                Set rngFNTarget = rngSetFirmNameTarget(rngHeader, _
                                                    secScope)
            '---find out if there's any text
                iTestLen = Len(rngFNTarget.Text)
                If iTestLen < 5 Then
                    xSuffix = "None"
                Else
                    xSuffix = "FNLandscaped"
                End If
            
            Else   'check for footer table
             
                iNumFooterCols = iFooterType(secScope _
                                .Footers(wdHeaderFooterPrimary))
                If iNumFooterCols <> 4 Then  'it's not a pleading footer table
                    xSuffix = "None"
                Else
                    Set rngFNTarget = rngSetFirmNameTarget(rngFooter, _
                                                        secScope)
                '---find out if there's any text
                    iTestLen = Len(rngFNTarget.Text)
                    If iTestLen < 5 Then
                        xSuffix = "None"
                    Else
                        xSuffix = "FNPortrait"
                    End If
            
                End If
            
            End If
        
        End If
    
    xPPaper = xPrefix & xSuffix
    
    End If
                                        
    '---now assign constant determined by PPtype string
    
    Select Case xPPaper
        Case "PP26None"
            iGetPleadingPaperType = mpPleadingPaper26Line
        Case "PP26FNPortrait"
            iGetPleadingPaperType = mpPleadingPaper26LineFNPort
        Case "PP26FNLandscaped"
            iGetPleadingPaperType = mpPleadingPaper26LineFNLand
        Case "PP28None"
            iGetPleadingPaperType = mpPleadingPaper28Line
        Case "PP28FNPortrait"
            iGetPleadingPaperType = mpPleadingPaper28LineFNPort
        Case "PP28FNLandscaped"
            iGetPleadingPaperType = mpPleadingPaper28LineFNLand
                                        
        Case Else
    End Select
                                        
End Function
Function bSetPleadingPaperType(docExisting As Word.Document, _
                                iType As Integer, _
                                Optional iFirmName As Integer = _
                                0) As Boolean
    
    Dim xPrefix As String
    Dim xSuffix As String
    
    Select Case iType
        Case mpPleadingPaper26Line
            xPrefix = "PP26"
        Case mpPleadingPaper28Line
            xPrefix = "PP28"
        Case Else
            xPrefix = "PP"
    End Select
    
    Select Case iFirmName
        Case mpPleadingFirmNameNone
            xSuffix = "None"
        Case mpPleadingFirmNameInFooter
            xSuffix = "FNPortrait"
        Case Else
            xSuffix = "FNLandscaped"
    End Select

'---Generate Document Var
    On Error Resume Next
    
    docExisting.Variables.Add Name:="zzmpFixed_PleadingPaperType", _
                              Value:=xPrefix & xSuffix
    docExisting.Variables("zzmpFixed_PleadingPaperType") = _
            xPrefix & xSuffix

End Function

Function bIsStateCourt() As Boolean
    Dim xCourtVar As String
    On Error Resume Next
    xCourtVar = UCase(ActiveDocument.Variables("lstCourts"))
    Select Case xCourtVar
        Case "SUPERIOR", "MUNICIPAL", "OTHER", ""
            bIsStateCourt = True
        Case Else
            bIsStateCourt = False
    End Select
    
End Function





