VERSION 5.00
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmEditTOC 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Edit Table of Contents"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5355
   Icon            =   "frmEditTOC.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   5355
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraMain 
      Height          =   3864
      Left            =   36
      TabIndex        =   1
      Top             =   468
      Width           =   5265
      Begin VB.CommandButton btnOK 
         Caption         =   "O&K"
         Default         =   -1  'True
         Height          =   393
         Left            =   2880
         TabIndex        =   19
         Top             =   3336
         Width           =   1100
      End
      Begin VB.CommandButton btnCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         CausesValidation=   0   'False
         Height          =   393
         Left            =   4020
         TabIndex        =   18
         Top             =   3336
         Width           =   1100
      End
      Begin mpControls3.SpinTextInternational spnBefore 
         Height          =   315
         Left            =   3045
         TabIndex        =   14
         Top             =   2375
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   556
         Appearance      =   1
         IncrementValue  =   6
         MaxValue        =   1584
         AppendSymbol    =   -1  'True
      End
      Begin VB.CheckBox chkCenterLevel1 
         Caption         =   "&Center Level 1"
         Height          =   240
         Left            =   2745
         TabIndex        =   12
         Top             =   1800
         Width           =   1680
      End
      Begin VB.CheckBox chkDefaultScheme 
         Caption         =   "Set as Default &TOC Scheme"
         Height          =   240
         Left            =   165
         TabIndex        =   17
         Top             =   3465
         Width           =   2385
      End
      Begin mpControls3.SpinTextInternational spnBetween 
         Height          =   315
         Left            =   3045
         TabIndex        =   16
         Top             =   2750
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   556
         Appearance      =   1
         IncrementValue  =   6
         MaxValue        =   1584
         AppendSymbol    =   -1  'True
      End
      Begin TrueDBList60.TDBCombo cmbAllCaps 
         Height          =   564
         Left            =   75
         OleObjectBlob   =   "frmEditTOC.frx":058A
         TabIndex        =   7
         Top             =   1135
         Width           =   2385
      End
      Begin TrueDBList60.TDBCombo cmbBold 
         Height          =   564
         Left            =   75
         OleObjectBlob   =   "frmEditTOC.frx":24B0
         TabIndex        =   11
         Top             =   1820
         Width           =   2385
      End
      Begin TrueDBList60.TDBCombo cmbPageNums 
         Height          =   564
         Left            =   2748
         OleObjectBlob   =   "frmEditTOC.frx":43D3
         TabIndex        =   5
         Top             =   480
         Width           =   2388
      End
      Begin TrueDBList60.TDBCombo cmbDotLeaders 
         Height          =   564
         Left            =   2745
         OleObjectBlob   =   "frmEditTOC.frx":62FA
         TabIndex        =   9
         Top             =   1135
         Width           =   2385
      End
      Begin TrueDBList60.TDBCombo cmbLineSpacing 
         Height          =   564
         Left            =   72
         OleObjectBlob   =   "frmEditTOC.frx":8223
         TabIndex        =   3
         Top             =   480
         Width           =   2385
      End
      Begin VB.Label lblBold 
         Caption         =   "B&old:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1610
         Width           =   1215
      End
      Begin VB.Label lblAllCaps 
         Caption         =   "&All Caps:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   925
         Width           =   1215
      End
      Begin VB.Label lblPageNums 
         Caption         =   "Include Page &Numbers:"
         Height          =   270
         Left            =   2760
         TabIndex        =   4
         Top             =   240
         Width           =   1875
      End
      Begin VB.Label lblDotLeaders 
         Caption         =   "Use &Dot Leaders:"
         Height          =   285
         Left            =   2760
         TabIndex        =   8
         Top             =   925
         Width           =   1740
      End
      Begin VB.Label lblSpacing 
         Caption         =   "&Line Spacing:"
         Height          =   285
         Left            =   105
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label lblBetween 
         Caption         =   "Space &Between Entries of Same Level:"
         Height          =   280
         Left            =   180
         TabIndex        =   15
         Top             =   2795
         Width           =   2865
      End
      Begin VB.Label lblBefore 
         Caption         =   "&Space Before New Level:"
         Height          =   280
         Left            =   180
         TabIndex        =   13
         Top             =   2435
         Width           =   2115
      End
   End
   Begin VB.Label lblMessage 
      Alignment       =   2  'Center
      Caption         =   $"frmEditTOC.frx":A14D
      Height          =   525
      Left            =   90
      TabIndex        =   0
      Top             =   120
      Width           =   5250
   End
End
Attribute VB_Name = "frmEditTOC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_bEditCustomScheme As Boolean
Private m_xarLineSpacing As xArray
Private m_xarAllCaps As xArray
Private m_xarBold As xArray
Private m_xarDotLeaders As xArray
Private m_xarPageNums As xArray

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let EditCustomScheme(bNew As Boolean)
    m_bEditCustomScheme = bNew
'   GLOG : 5445 : ceh
'   moved to Form_Load so controls get properly initialized
'   SetupDlg bNew
End Property

Property Get EditCustomScheme() As Boolean
    EditCustomScheme = m_bEditCustomScheme
End Property

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
End Sub

Private Sub cmbBefore_Click()
''if custom, load ini value;
''otherwise, disable and clear controls
'    Dim bEnabled As Boolean
'    Dim xBefore As String
'
'    bEnabled = (cmbBefore = "Custom")
'
'    Me.txtBefore.Enabled = bEnabled
'    Me.lblPointsBefore.Enabled = bEnabled
'
'    If bEnabled Then
'        xBefore = xnulltostring(xAppGetUserINIValue("TOC", _
'            "PointsBefore"))
'        If xBefore = "" Then _
'            xBefore = "0"
'    End If
'    Me.txtBefore = xBefore
End Sub

Private Sub cmbBetween_Click()
''if custom, load ini value;
''otherwise, disable and clear controls
'    Dim bEnabled As Boolean
'    Dim xBetween As String
'
'    bEnabled = (cmbBetween = "Custom")
'
'    Me.txtBetween.Enabled = bEnabled
'    Me.lblPointsBetween.Enabled = bEnabled
'
'    If bEnabled Then
'        xBetween = xnulltostring(xAppGetUserINIValue("TOC", _
'            "PointsBetween"))
'        If xBetween = "" Then _
'            xBetween = "0"
'    End If
'    Me.txtBetween = xBetween
End Sub

Private Sub cmbCaps_Click()
''if custom, load ini value;
''otherwise, disable and clear controls
'    Dim bEnabled As Boolean
'    Dim xDo As String
'
'    bEnabled = (cmbCaps = "Custom")
'
'    Me.chkCapLevelOne.Enabled = bEnabled
'    Me.chkCapSubsequent.Enabled = bEnabled
'
'    If bEnabled Then
'        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
'            "CapLevelOne"))
'        Me.chkCapLevelOne = Val(xDo)
'        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
'            "CapSubsequent"))
'        Me.chkCapSubsequent = Val(xDo)
'    Else
'        Me.chkCapLevelOne = 0
'        Me.chkCapSubsequent = 0
'    End If
End Sub

Private Sub btnOK_Click()
    m_bCancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub cmbAllCaps_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAllCaps, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditTOC.cmbAllCaps_Mismatch"
    Exit Sub
End Sub

Private Sub cmbBold_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbBold, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditTOC.cmbBold_Mismatch"
    Exit Sub
End Sub

Private Sub cmbDotLeaders_Click()
''if custom, load ini value;
''otherwise, disable and clear controls
'    Dim bEnabled As Boolean
'    Dim xDo As String
'
'    bEnabled = (cmbDotLeaders = "Custom")
'
'    Me.chkDotLevelOne.Enabled = bEnabled
'    Me.chkDotSubsequent.Enabled = bEnabled
'
'    If bEnabled Then
'        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
'            "DotLevelOne"))
'        Me.chkDotLevelOne = Val(xDo)
'        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
'            "DotSubsequent"))
'        Me.chkDotSubsequent = Val(xDo)
'    Else
'        Me.chkDotLevelOne = 0
'        Me.chkDotSubsequent = 0
'    End If
End Sub


Private Sub cmbDotLeaders_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDotLeaders, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditTOC.cmbDotLeaders_Mismatch"
    Exit Sub
End Sub

Private Sub cmbLineSpacing_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLineSpacing, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditTOC.cmbLineSpacing_Mismatch"
    Exit Sub
End Sub

Private Sub cmbPageNums_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbPageNums, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditTOC.cmbPageNums_Mismatch"
    Exit Sub
End Sub

Private Sub Form_Activate()
    Dim xValue As String
    
    If EditCustomScheme Then
    '   initialize values
    '   line spacing
        xValue = xAppGetUserINIValue("TOC", "LineSpacing")
        If xValue = "" Or xValue = "-1" Then xValue = "0"
        
        Me.cmbLineSpacing.SelectedItem = m_xarLineSpacing(CLng(xValue), 1)
    '   Me.cmbLineSpacing.Text = m_xarLineSpacing(CLng(xValue), 0)
    
    '   all caps
        xValue = xAppGetUserINIValue("TOC", "AllCaps")
        If xValue = "" Or xValue = "-1" Then xValue = "0"
    '   we removed an item from this list
        If xValue > m_xarAllCaps.Count(1) - 1 Then _
            xValue = xValue - 1
            
        Me.cmbAllCaps.SelectedItem = m_xarAllCaps(CLng(xValue), 1)
    '   Me.cmbAllCaps.Text = m_xarAllCaps(CLng(xValue), 0)
    
    '   bold
        xValue = xAppGetUserINIValue("TOC", "Bold")
        If xValue = "" Or xValue = "-1" Then xValue = "2"
        
        Me.cmbBold.SelectedItem = m_xarBold(CLng(xValue), 1)
        Me.cmbBold.Text = m_xarBold(CLng(xValue), 0)
    
    '   dot leaders
        xValue = xAppGetUserINIValue("TOC", "DotLeaders")
        If xValue = "" Or xValue = "-1" Then xValue = "0"
            
        Me.cmbDotLeaders.SelectedItem = m_xarDotLeaders(CLng(xValue), 1)
    '    Me.cmbDotLeaders.Text = m_xarDotLeaders(CLng(xValue), 0)
            
    '   page numbers
        xValue = xAppGetUserINIValue("TOC", "PageNumbers")
        If xValue = "" Or xValue = "-1" Then xValue = "0"
        
        Me.cmbPageNums.SelectedItem = m_xarPageNums(CLng(xValue), 1)
    '    Me.cmbPageNums.Text = m_xarPageNums(CLng(xValue), 0)
            
    '   points before
        xValue = xAppGetUserINIValue("TOC", "PointsBefore")
        If xValue = "" Then
            xValue = "0"
        Else
            xValue = xLocalizeNumericString(xValue)
        End If
        Me.spnBefore.Value = CDbl(xValue)
            
    '   points between
        xValue = xAppGetUserINIValue("TOC", "PointsBetween")
        If xValue = "" Then
            xValue = "0"
        Else
            xValue = xLocalizeNumericString(xValue)
        End If
        Me.spnBetween.Value = CDbl(xValue)
            
    '   center level 1
        xValue = xAppGetUserINIValue("TOC", "CenterLevel1")
        If xValue = "" Then xValue = "0"
        Me.chkCenterLevel1 = xValue
            
    '   default to custom
        xValue = xAppGetUserINIValue("TOC", "DefaultToCustom")
        If xValue = "" Then xValue = "0"
        Me.chkDefaultScheme = xValue
    End If
    
End Sub

Private Sub Form_Load()
    Dim ctlControl As Control
    
    m_bCancelled = True 'GLOG 5696

'   GLOG : 5445 : ceh
'   moved from EditCustomScheme property let
    SetupDlg EditCustomScheme
'   hide border - left on in design to
'   avoid confusion
    Me.fraMain.BorderStyle = 0
End Sub

Private Sub SetupDlg(ByVal bForCustomTOCScheme As Boolean)
    Dim xValue As String
    Dim ctlP As Control
    
    If bForCustomTOCScheme Then
        Me.Caption = " Edit Custom TOC Scheme"
        Me.btnOK.Caption = "Sa&ve"
'        On Error Resume Next
'cehtdb
        For Each ctlP In Me.Controls
            If TypeOf ctlP Is TDBCombo Then
                If GetTDBListCount(ctlP) = 5 Then
                    RemoveItemFromTDBCombo ctlP, 4
                End If
            End If
        Next ctlP

        Me.fraMain.Top = 150
        
        'GLOG : 5591 : ceh
        'fix dialog height for Word version post 2010
        'GLOG 5707 (dm) - fixed type conversion issue
        If (InStr(Word.Application.Version, "15.") <> 0) Or _
                (InStr(Application.Version, "16.") <> 0) Then
            Me.Height = 4575
        Else
            Me.Height = 4500
        End If

'       load line spacing list
        Set m_xarLineSpacing = New xArray
        
        m_xarLineSpacing.ReDim 0, 2, 0, 1
        m_xarLineSpacing(0, 0) = "Single"
        m_xarLineSpacing(0, 1) = 0
        m_xarLineSpacing(1, 0) = "1.5 Lines"
        m_xarLineSpacing(1, 1) = 1
        m_xarLineSpacing(2, 0) = "Double"
        m_xarLineSpacing(2, 1) = 2
        
        Me.cmbLineSpacing.Array = m_xarLineSpacing
        ResizeTDBCombo Me.cmbLineSpacing, 3
        
'       load all caps list
        Set m_xarAllCaps = New xArray
        
        m_xarAllCaps.ReDim 0, 2, 0, 1
        m_xarAllCaps(0, 0) = "All Levels"
        m_xarAllCaps(0, 1) = 0
        m_xarAllCaps(1, 0) = "Level 1"
        m_xarAllCaps(1, 1) = 1
        m_xarAllCaps(2, 0) = "Do Not Cap Any Level"
        m_xarAllCaps(2, 1) = 2
        
        Me.cmbAllCaps.Array = m_xarAllCaps
        ResizeTDBCombo Me.cmbAllCaps, 3
        
'       load bold list
        Set m_xarBold = New xArray
        
        m_xarBold.ReDim 0, 2, 0, 1
        m_xarBold(0, 0) = "All Levels"
        m_xarBold(0, 1) = 0
        m_xarBold(1, 0) = "Level 1"
        m_xarBold(1, 1) = 1
        m_xarBold(2, 0) = "Do Not Bold Any Level"
        m_xarBold(2, 1) = 2
        
        Me.cmbBold.Array = m_xarBold
        ResizeTDBCombo Me.cmbBold, 3

'       load DotLeaders list
        Set m_xarDotLeaders = New xArray
        
        m_xarDotLeaders.ReDim 0, 3, 0, 1
        m_xarDotLeaders(0, 0) = "All Levels"
        m_xarDotLeaders(0, 1) = 0
        m_xarDotLeaders(1, 0) = "Level 1"
        m_xarDotLeaders(1, 1) = 1
        m_xarDotLeaders(2, 0) = "Levels 2-9"
        m_xarDotLeaders(2, 1) = 2
        m_xarDotLeaders(3, 0) = "No Dot Leaders"
        m_xarDotLeaders(3, 1) = 3
        
        Me.cmbDotLeaders.Array = m_xarDotLeaders
        ResizeTDBCombo Me.cmbDotLeaders, 4
                
'       load PageNumbers list
        Set m_xarPageNums = New xArray
        
        m_xarPageNums.ReDim 0, 3, 0, 1
        m_xarPageNums(0, 0) = "All Levels"
        m_xarPageNums(0, 1) = 0
        m_xarPageNums(1, 0) = "Level 1"
        m_xarPageNums(1, 1) = 1
        m_xarPageNums(2, 0) = "Levels 2-9"
        m_xarPageNums(2, 1) = 2
        m_xarPageNums(3, 0) = "No Page Numbers"
        m_xarPageNums(3, 1) = 3
        
        Me.cmbPageNums.Array = m_xarPageNums
        ResizeTDBCombo Me.cmbPageNums, 4
    Else
        Me.Caption = "Edit TOC"
        Me.btnOK.Caption = "O&K"
'        On Error Resume Next
'cehtdb
        For Each ctlP In Me.Controls
            If TypeOf ctlP Is TDBCombo Then
                If GetTDBListCount(ctlP) = 4 Then
                    AddItemToTDBCombo ctlP, "Use Current Settings"
                End If
            End If
        Next ctlP
'        Me.fraMain.Top = 165
        Me.btnOK.Top = Me.btnOK.Top - 100
        Me.btnCancel.Top = Me.btnCancel.Top - 100
        Me.Height = 4650
    End If
    Me.lblMessage.Visible = Not bForCustomTOCScheme
    Me.chkDefaultScheme.Visible = bForCustomTOCScheme
End Sub

Private Sub spnBefore_Validate(Cancel As Boolean)
    If Not spnBefore.IsValid Then _
        Cancel = True
End Sub

Private Sub spnBetween_Validate(Cancel As Boolean)
    If Not spnBetween.IsValid Then _
        Cancel = True
End Sub
