VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNumbers"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function bGetIntegerSequence(xArray() As String, _
                             iLBound As Integer, _
                             iUBound As Integer, _
                             Optional bIncludeBlankLine = False) As Boolean
    Dim i As Integer
    
    If bIncludeBlankLine Then
        ReDim xArray(0 To (iUBound - iLBound + 1))
        xArray(0) = String(4, "_")
        For i = iLBound To iUBound
            xArray(i - iLBound + 1) = i
        Next i
    Else
        ReDim xArray(0 To (iUBound - iLBound))
        For i = iLBound To iUBound
            xArray(i - iLBound) = i
        Next i
    End If
    
End Function
Public Function bGetOrdinalSequence(xArray() As String, _
                             iLBound As Integer, _
                             iUBound As Integer, _
                             Optional bIncludeBlankLine = False) As Boolean
    Dim i As Integer
    
    If bIncludeBlankLine Then
        ReDim xArray(0 To (iUBound - iLBound + 1))
        xArray(0) = String(4, "_")
        For i = iLBound To iUBound
            xArray(i - iLBound + 1) = xGetOrdinal(i)
        Next i
    Else
        ReDim xArray(0 To (iUBound - iLBound))
        For i = iLBound To iUBound
            xArray(i - iLBound) = xGetOrdinal(i)
        Next i
    End If
    
End Function

Public Function mpMax(i As Double, j As Double) As Double
    If i > j Then
        mpMax = i
    Else
        mpMax = j
    End If
End Function

Public Function mpMin(i As Double, j As Double) As Double
    If i > j Then
        mpMin = j
    Else
        mpMin = i
    End If
End Function



Public Function xGetOrdinal(i As Integer) As String
    If i >= 4 And i <= 20 Then
        xGetOrdinal = i & "th"
    ElseIf Right(i, 1) = 1 Then
        xGetOrdinal = i & "st"
    ElseIf Right(i, 1) = 2 Then
        xGetOrdinal = i & "nd"
    ElseIf Right(i, 1) = 3 Then
        xGetOrdinal = i & "rd"
    Else
        xGetOrdinal = i & "th"
    End If
End Function

Public Function bDecimalSeparatorIsComma() As Boolean
'returns TRUE if stystem is set to use comma as
'decimal separator
    Dim xDecimal As String
    xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
    bDecimalSeparatorIsComma = (xDecimal = ",")
End Function



