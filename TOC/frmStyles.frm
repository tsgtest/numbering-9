VERSION 5.00
Object = "{00028CAD-0000-0000-0000-000000000046}#5.0#0"; "TDBL5.OCX"
Begin VB.Form frmStyles 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "TOC Entries"
   ClientHeight    =   2880
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4155
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2880
   ScaleWidth      =   4155
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   420
      Left            =   2970
      TabIndex        =   1
      Top             =   2340
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   420
      Left            =   2955
      TabIndex        =   0
      Top             =   1815
      Width           =   1100
   End
   Begin TrueDBList50.TDBList lstStyles 
      Height          =   2415
      Left            =   75
      OleObjectBlob   =   "frmStyles.frx":0000
      TabIndex        =   2
      Top             =   345
      Width           =   2610
   End
   Begin VB.Label Label1 
      Caption         =   "Create TOC Entries From:"
      Height          =   270
      Left            =   90
      TabIndex        =   3
      Top             =   120
      Width           =   2100
   End
End
Attribute VB_Name = "frmStyles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Cancelled As Boolean

Private Sub btnCancel_Click()
    Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnOK_Click()
    Cancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Load()
    Cancelled = True
    xarGetSchemes
    Me.lstStyles.Array = xarStyles
    Me.lstStyles.Rebind
End Sub

