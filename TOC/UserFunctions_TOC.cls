VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UserFunctions_TOC"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Function zzmpConvertHiddenParas() As Long
    If g_lUILanguage = wdFrenchCanadian Then
        MsgBox "Cette macro n'est pas disponible.", _
            vbInformation, "Numérotation MacPac"
    Else
        MsgBox "This macro is still under construction.", _
            vbInformation, "MacPac Numbering"
    End If
'    If bReadyToGo(True, True, True) Then
'        zzmpConvertHiddenParas = mpTOC.ConvertHiddenParas()
'    End If
End Function

Function zzmpFillTCCodes() As Long
    If bReadyToGo(True, True, True) Then
        zzmpFillTCCodes = mpTOC.FillTCCodes()
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpFillTCCodesFormatted() As Long
    If bReadyToGo(True, True, True) Then
        zzmpFillTCCodesFormatted = mpTOC.FillTCCodes(True)
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpRestoreTCCodes() As Long
    If bReadyToGo(True, True, True) Then
        zzmpRestoreTCCodes = mpTOC.RestoreMPTCCodes()
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpModifyTOC()
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
        
    If bReadyToGo(True, True, True) Then
        On Error GoTo ProcError
        
        'force accelerator cues in Word 2013
        If (InStr(Word.Application.Version, "15.") <> 0) Or _
                (InStr(Application.Version, "16.") <> 0) Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If
    
        Dim oTOC As MPTOC90.CTOCProps
        Set oTOC = New MPTOC90.CTOCProps
        oTOC.Modify
        Set oTOC = Nothing
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    End If
    
    Exit Function
ProcError:
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    RaiseError Err.Source
    Exit Function
End Function

Function zzmpEditCustomTOCScheme()
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
        
    If bReadyToGo(True, True, True) Then
        On Error GoTo ProcError
        
        'force accelerator cues in Word 2013
        If (InStr(Word.Application.Version, "15.") <> 0) Or _
                (InStr(Application.Version, "16.") <> 0) Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If
    
        mpTOC.EditCustomTOCScheme
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    End If
    
    Exit Function
ProcError:
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    RaiseError Err.Source
    Exit Function
End Function

Function zzmpChangeTOCTabs() As Long
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
        
    If bReadyToGo(True, True, True) Then
        On Error GoTo ProcError
        
        'force accelerator cues in Word 2013
        If (InStr(Word.Application.Version, "15.") <> 0) Or _
                (InStr(Application.Version, "16.") <> 0) Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If
    
        zzmpChangeTOCTabs = mpTOC.bUserChangeTOCTabs()
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    End If
    
    Exit Function
ProcError:
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    RaiseError Err.Source
    Exit Function
End Function
Function zzmpMarkHeading() As Long
    If bReadyToGo(True, True, True) Then
        Application.ScreenUpdating = False
        If g_bMarkWithStyleSeparators Then
            zzmpMarkHeading = mpTOC.InsertStyleSeparator(False)
        Else
            zzmpMarkHeading = mpTOC.bMarkForTOC(False)
        End If
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpMarkAndFormatHeading() As Long
    If bReadyToGo(True, True, True) Then
        Application.ScreenUpdating = False
        If g_bMarkWithStyleSeparators Then
            zzmpMarkAndFormatHeading = mpTOC.InsertStyleSeparator(True)
        Else
            zzmpMarkAndFormatHeading = mpTOC.bMarkForTOC(True)
        End If
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpTableOfContentsInsert() As Long
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
        
    If bReadyToGo(True, True, True) Then
        On Error GoTo ProcError
        
        'force accelerator cues in Word 2013
        If (InStr(Word.Application.Version, "15.") <> 0) Or _
                (InStr(Application.Version, "16.") <> 0) Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If
    
'       disable mp10 event handling
        If g_bIsMP10 Then _
            Word.Application.Run "zzmpSuppressXMLEventHandling"
            
'       get doc/application environment
        envMpDoc = envGetDocEnvironment()
        envMpApp = envGetAppEnvironment()
    
        '9.9.5001 - new field-only dialog
        If g_iTOCDialogStyle = mpTOCDialogStyle_Field Then
            lRet = bInsertTOCAsField()
        Else
            lRet = bInsertTOC
        End If
    
'       reset doc/application environment
'       envMpApp/envMpDoc below are global vars
        bSetDocEnvironment envMpDoc
        bSetAppEnvironment envMpApp, Not g_bPreserveUndoListTOC
    
        If lRet = 0 Then _
            Application.StatusBar = MsgFinished
        zzmpTableOfContentsInsert = lRet
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoListTOC Then _
            ActiveDocument.UndoClear
    
'       restore mp10 event handling
        If g_bIsMP10 Then _
            Word.Application.Run "zzmpResumeXMLEventHandling"
    
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    End If
    
    Exit Function
ProcError:
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    RaiseError Err.Source
    Exit Function
End Function

Function zzmpUnmarkForTOC() As Long
    Dim bTCCodes As Boolean
    Dim bStyleSeps As Boolean
    
    If bReadyToGo(True, True, True) Then
        Application.ScreenUpdating = False
        bStyleSeps = bRemoveStyleSeparators(Selection.Range)
        bTCCodes = bMarkForTOCRemove(Selection.Range)
        If Not bTCCodes And Not bStyleSeps Then
            If g_lUILanguage = wdFrenchCanadian Then
                If g_bMarkWithStyleSeparators Then
                    MsgBox "Il n'y a pas de séparateurs de style dans la sélection.", _
                        vbExclamation, "Numérotation MacPac"
                Else
                    MsgBox "Il n'y a pas d'entrées TM dans la sélection.", _
                        vbExclamation, "Numérotation MacPac"
                End If
            Else
                If g_bMarkWithStyleSeparators Then
                    MsgBox "There are no style separators in the current selection.", _
                        vbExclamation, AppName
                Else
                    MsgBox "There are no TC entries in the current selection.", _
                        vbExclamation, AppName
                End If
            End If
        End If
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpMarkAll() As Long
    Dim bCancel As Boolean
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
            
    If bReadyToGo(True, True, True) Then
        On Error GoTo ProcError
        
        'force accelerator cues in Word 2013
        If (InStr(Word.Application.Version, "15.") <> 0) Or _
                (InStr(Application.Version, "16.") <> 0) Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If
    
'       get doc/application environment
        envMpDoc = envGetDocEnvironment()
        envMpApp = envGetAppEnvironment()
        
        bCancel = mpTOC.bMarkAll

'       reset doc/application environment
'       envMpApp/envMpDoc below are global vars
        bSetAppEnvironment envMpApp
        If Not bCancel Then _
            bSetDocEnvironment envMpDoc
        
        zzmpMarkAll = CLng(bCancel)
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    End If
    
    Exit Function
ProcError:
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    RaiseError Err.Source
    Exit Function
End Function

Function zzmpIncrTOCTabs() As Long
    If bReadyToGo(True, True, True) Then
        zzmpIncrTOCTabs = IncrementTOCTabs()
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
        WordBasic.WaitCursor False
    End If
End Function

Function zzmpDecrTOCTabs() As Long
    If bReadyToGo(True, True, True) Then
        zzmpDecrTOCTabs = IncrementTOCTabs(True)
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
        WordBasic.WaitCursor False
    End If
End Function

Function zzmpDeleteMPTOC() As Long
    If bReadyToGo(True, True, True) Then
        zzmpDeleteMPTOC = mpTOC.bDeleteMPTOC
        If (g_iWordVersion = mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function EnsureLatestNumbering() As Long
    On Error Resume Next
    If Not bIsConverted() Then
        Application.Run "zzmpDoConversions"
    End If
End Function

Private Function bReadyToGo(ByVal bOnlyIfActiveDocument As Boolean, _
                            ByVal bOnlyIfDocUnprotected As Boolean, _
                            ByVal bDoConversions As Boolean) As Boolean
'returns TRUE iff Word environment
'is ready to run TOC functions
    Dim oAddIn As Word.AddIn

    '9.9.3 - attempt to get ui language immediately -
    'this will allow us to show pre-initialization messages in the correct language
    If (g_lUILanguage = 0) And ((InStr(Application.Version, "12.") <> 0) Or _
            (InStr(Application.Version, "14.") <> 0) Or _
            (InStr(Application.Version, "15.") <> 0) Or _
            (InStr(Application.Version, "16.") <> 0)) Then _
        g_lUILanguage = mpBase.GetLanguageFromStartupTemplate

    If bOnlyIfActiveDocument Then
        If Documents.Count = 0 Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Veuillez ouvrir un document avant d'utiliser cette fonction.", _
                    vbInformation, "Numérotation MacPac"
            Else
                MsgBox "Please open a document before running this function.", _
                    vbInformation, "MacPac Numbering"
            End If
            Exit Function
        End If
    End If
            
    If bOnlyIfDocUnprotected Then
'       false if doc is protected
        If mpBase.bDocProtected(ActiveDocument, True) Then _
            Exit Function
    End If
        
'   reinitialize if necessary
    If xBP = "" Then
        bAppInitialize
    End If
    
'   6/30/09 - check for doc var corruption caused by KB969604
    mpBase.DeleteCorruptedVariables
    
'   run conversions
    If bDoConversions Then
        EnsureLatestNumbering
    End If

    bReadyToGo = True
End Function

Function zzmpGoToMPTOC() As Long
    If bReadyToGo(True, True, False) Then
        zzmpGoToMPTOC = mpTOC.bGoToMPTOC
    End If
End Function

Function zzmpSwitchMarkingMode()
    Dim xMode As String
    If bReadyToGo(True, True, False) Then
        g_bMarkWithStyleSeparators = Not g_bMarkWithStyleSeparators
        bAppSetUserINIValue "TOC", "MarkWithStyleSeparators", _
            CStr(g_bMarkWithStyleSeparators)
        If g_lUILanguage = wdFrenchCanadian Then
            If g_bMarkWithStyleSeparators Then
                xMode = "séparateurs de style"
            Else
                xMode = "codes TM"
            End If
            MsgBox "Les titres seront marqués avec des " & xMode & ".", _
                vbInformation, "Numérotation MacPac"
        Else
            If g_bMarkWithStyleSeparators Then
                xMode = "style separators"
            Else
                xMode = "TC codes"
            End If
            MsgBox "Headings will now be marked with " & xMode & ".", _
                vbInformation, "MacPac Numbering"
        End If
    End If
End Function

Function zzmpTCCodesToStyleSeparators()
    If bReadyToGo(True, True, True) Then
        mpTOC.ConvertToStyleSeparators
    End If
End Function

Function zzmpStyleSeparatorsToTCCodes()
    If bReadyToGo(True, True, True) Then
        mpTOC.ConvertToTCCodes
    End If
End Function

Function GetMarkMenuXML() As String
    If bReadyToGo(False, False, False) Then
        GetMarkMenuXML = mpApplication.GetMarkMenuXML
    End If
End Function

Function zzmpSwitchTOCMode()
    Dim xMode As String
    If bReadyToGo(True, True, False) Then
        If g_lUILanguage = wdFrenchCanadian Then
            If g_iTOCDialogStyle = mpTOCDialogStyle_Field Then
                g_iTOCDialogStyle = mpTOCDialogStyle_Text
                xMode = "texte"
            Else
                g_iTOCDialogStyle = mpTOCDialogStyle_Field
                xMode = "codes de champ"
            End If
            bAppSetUserINIValue "TOC", "InsertAsFieldDefault", _
                CStr(g_iTOCDialogStyle = mpTOCDialogStyle_Field)
            MsgBox "Les TMs seront dorénavant insérées comme " & xMode & ".", _
                vbInformation, "Numérotation MacPac"
        Else
            If g_iTOCDialogStyle = mpTOCDialogStyle_Field Then
                g_iTOCDialogStyle = mpTOCDialogStyle_Text
                xMode = "text"
            Else
                g_iTOCDialogStyle = mpTOCDialogStyle_Field
                xMode = "field codes"
            End If
            bAppSetUserINIValue "TOC", "InsertAsFieldDefault", _
                CStr(g_iTOCDialogStyle = mpTOCDialogStyle_Field)
            MsgBox "TOCs will now be inserted as " & xMode & ".", _
                vbInformation, "MacPac Numbering"
        End If
    End If
End Function

Function GetTOCMenuXML() As String
    If bReadyToGo(False, False, False) Then
        GetTOCMenuXML = mpApplication.GetTOCMenuXML
    End If
End Function

Function AllowTOCAsField() As Boolean
    If bReadyToGo(False, False, False) Then
        AllowTOCAsField = g_bAllowTOCAsField
    End If
End Function

Function InsertTOCsAsField() As Boolean
    If bReadyToGo(False, False, False) Then
        InsertTOCsAsField = (g_iTOCDialogStyle = mpTOCDialogStyle_Field)
    End If
End Function

Function zzmpReinitialize() As Long
    'added in 9.9.6006 for GLOG 3940
    zzmpReinitialize = CLng(bAppInitialize())
End Function
