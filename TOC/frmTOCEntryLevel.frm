VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmTOCEntryLevel 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Mark Heading"
   ClientHeight    =   1110
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3570
   Icon            =   "frmTOCEntryLevel.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1110
   ScaleWidth      =   3570
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2490
      TabIndex        =   3
      Top             =   600
      Width           =   960
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2490
      TabIndex        =   2
      Top             =   165
      Width           =   960
   End
   Begin TrueDBList60.TDBCombo cbxTOCLevels 
      Height          =   564
      Left            =   1485
      OleObjectBlob   =   "frmTOCEntryLevel.frx":058A
      TabIndex        =   1
      Top             =   330
      Width           =   585
   End
   Begin VB.Label Label1 
      Caption         =   "Mark As &Level:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   240
      TabIndex        =   0
      Top             =   390
      Width           =   1260
   End
End
Attribute VB_Name = "frmTOCEntryLevel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Cancelled As Boolean

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnOK_Click()
    Me.Cancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub cbxTOCLevels_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxTOCLevels, Reposition
    Exit Sub
ProcError:
    RaiseError "frmChangeTOCTabs.cbxTOCLevels_Mismatch"
    Exit Sub
End Sub

Private Sub Form_Activate()
    'initialize to first in list
    Me.cbxTOCLevels.SelectedItem = 0
End Sub

Private Sub Form_Load()
    Dim xarTOCLevels As xArray
    Dim i As Integer
    
    Me.Cancelled = True
    
'   fill Tab Levels array
    Set xarTOCLevels = New xArray
    xarTOCLevels.ReDim 0, 8, 0, 1
    For i = 0 To 8
        xarTOCLevels(i, 0) = i + 1
        xarTOCLevels(i, 1) = i + 1
    Next i
    
    Me.cbxTOCLevels.Array = xarTOCLevels
    ResizeTDBCombo Me.cbxTOCLevels, 9

End Sub



