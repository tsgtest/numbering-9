VERSION 5.00
Begin VB.Form frmAboutNewFrench 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4965
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   8130
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Gill Sans MT"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4965
   ScaleWidth      =   8130
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox pctClose 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   330
      Left            =   7790
      Picture         =   "frmAboutNewFrench.frx":0000
      ScaleHeight     =   330
      ScaleWidth      =   240
      TabIndex        =   9
      Top             =   75
      Width           =   235
   End
   Begin VB.PictureBox pctTSGTagline 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   175
      Picture         =   "frmAboutNewFrench.frx":03CC
      ScaleHeight     =   255
      ScaleWidth      =   4380
      TabIndex        =   3
      Top             =   4400
      Width           =   4380
   End
   Begin VB.PictureBox pctTSGLogo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3925
      Picture         =   "frmAboutNewFrench.frx":0D9A
      ScaleHeight     =   420
      ScaleWidth      =   570
      TabIndex        =   2
      Top             =   3805
      Width           =   570
   End
   Begin VB.PictureBox pctTSGInc 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   130
      Picture         =   "frmAboutNewFrench.frx":1279
      ScaleHeight     =   420
      ScaleWidth      =   3795
      TabIndex        =   1
      Top             =   4000
      Width           =   3795
   End
   Begin VB.PictureBox pctMPLogo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3390
      Left            =   210
      Picture         =   "frmAboutNewFrench.frx":1E32
      ScaleHeight     =   3390
      ScaleMode       =   0  'User
      ScaleWidth      =   3525
      TabIndex        =   0
      Top             =   285
      Width           =   3525
   End
   Begin VB.Label lblAppName2 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "MacPac"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   36
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   950
      Left            =   4165
      TabIndex        =   10
      Top             =   1170
      Width           =   3495
   End
   Begin VB.Label lblCompatibility2 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "d'Office 2016, 2013, 365 ProPlus et 2010"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4165
      TabIndex        =   8
      Top             =   3265
      Width           =   3495
   End
   Begin VB.Label lblCompatibility1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Compatible avec les versions 32 bit"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4165
      TabIndex        =   7
      Top             =   3015
      Width           =   3495
   End
   Begin VB.Label lblCopyright 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "©1990 - 2017"
      Height          =   295
      Left            =   4165
      TabIndex        =   6
      Top             =   2675
      Width           =   3495
   End
   Begin VB.Label lblVersion 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Version automatic"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   4165
      TabIndex        =   5
      Top             =   2165
      Width           =   3495
   End
   Begin VB.Label lblAppName 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Numérotation"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   36
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   850
      Left            =   3865
      TabIndex        =   4
      Top             =   405
      Width           =   4095
   End
End
Attribute VB_Name = "frmAboutNewFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
End Sub

Private Sub pctClose_Click()
    Unload Me
End Sub
