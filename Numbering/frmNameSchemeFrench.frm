VERSION 5.00
Begin VB.Form frmNameSchemeFrench 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   2025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5430
   Icon            =   "frmNameSchemeFrench.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2025
   ScaleWidth      =   5430
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   4200
      TabIndex        =   6
      Top             =   1455
      Width           =   1100
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   4200
      TabIndex        =   5
      Top             =   1005
      Width           =   1100
   End
   Begin VB.TextBox txtAlias 
      Height          =   315
      Left            =   105
      MaxLength       =   15
      TabIndex        =   2
      Top             =   870
      Width           =   3825
   End
   Begin VB.TextBox txtName 
      Height          =   315
      Left            =   105
      MaxLength       =   10
      TabIndex        =   4
      Top             =   1485
      Width           =   3825
   End
   Begin VB.Label lblMessage 
      Caption         =   "###"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   105
      TabIndex        =   0
      Top             =   120
      Width           =   5265
   End
   Begin VB.Label lblAlias 
      Caption         =   "Nom du &th�me:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   135
      TabIndex        =   1
      Top             =   645
      Width           =   1185
   End
   Begin VB.Label lblName 
      Caption         =   "&Nom du style:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   135
      TabIndex        =   3
      Top             =   1275
      Width           =   1365
   End
End
Attribute VB_Name = "frmNameSchemeFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bCancelled As Boolean
Private m_bAliasEdited As Boolean
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Private Sub cmdCancel_Click()
    Me.Hide
    Me.Refresh
    m_bCancelled = True
End Sub
Private Sub cmdOK_Click()
'validate info
    m_bCancelled = False
    If Len(Me.txtName) = 0 Then
        xMsg = "Le nom du th�me est une information obligatoire."
        MsgBox xMsg, vbExclamation, g_xAppName
        Me.txtAlias.SetFocus
    ElseIf Len(Me.txtAlias) = 0 Then
        xMsg = "L'affichage de nom du th�me est une information obligatoire."
        MsgBox xMsg, vbExclamation, g_xAppName
        Me.txtAlias.SetFocus
    ElseIf Not bStyleNameIsValid(mpPrefix & Me.txtName, True) Then
        With Me.txtName
            .SelStart = 0
            .SelLength = Len(.Text)
            .SetFocus
        End With
    ElseIf Not bSchemeNameIsValid(Me.txtAlias, True) Then
        With Me.txtAlias
            .SelStart = 0
            .SelLength = Len(.Text)
            .SetFocus
        End With
    Else
        Me.Hide
        DoEvents
    End If
End Sub

Private Sub Form_Activate()
    Me.txtAlias.SetFocus
End Sub

Private Sub txtAlias_Change()
    Me.cmdOK.Enabled = (Len(Me.txtAlias) > 0) And _
                       (Len(Me.txtName) > 0)
'   set flag based on whether the 2 names are different-
'   this will be used to prevent changes in scheme
'   name from getting mirrored in alias - once set
'   no mirroring can occur
    m_bAliasEdited = m_bAliasEdited Or (Me.txtAlias <> Me.txtName)
End Sub
Private Sub txtAlias_GotFocus()
    bEnsureSelectedContent Me.txtAlias, True
End Sub
Private Sub txtName_Change()
    If Not m_bAliasEdited Then _
        Me.txtAlias = Me.txtName
    Me.cmdOK.Enabled = (Len(Me.txtAlias) > 0) And (Len(Me.txtName) > 0)
End Sub
Private Sub txtName_GotFocus()
    bEnsureSelectedContent Me.txtName, True
End Sub
Private Sub txtName_LostFocus()
    If (txtName <> "") And (txtAlias = "") Then _
        txtAlias = txtName
End Sub

