VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "Comct232.ocx"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmEditSchemeFrench 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edit Numbering Scheme"
   ClientHeight    =   6060
   ClientLeft      =   2490
   ClientTop       =   2400
   ClientWidth     =   6720
   Icon            =   "frmEditSchemeFrench.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   6720
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame pnlContGeneral 
      Caption         =   "G�n�ral"
      Height          =   1250
      Left            =   120
      TabIndex        =   116
      Top             =   450
      Visible         =   0   'False
      Width           =   6440
      Begin TrueDBList60.TDBCombo cmbContFontName 
         Height          =   564
         Left            =   1300
         OleObjectBlob   =   "frmEditSchemeFrench.frx":058A
         TabIndex        =   70
         Top             =   300
         Width           =   2460
      End
      Begin TrueDBList60.TDBCombo cmbContFontSize 
         Height          =   585
         Left            =   5205
         OleObjectBlob   =   "frmEditSchemeFrench.frx":24B5
         TabIndex        =   72
         Top             =   300
         Width           =   1000
      End
      Begin TrueDBList60.TDBCombo cmbAlignment 
         Height          =   564
         Left            =   1300
         OleObjectBlob   =   "frmEditSchemeFrench.frx":43E0
         TabIndex        =   75
         Top             =   750
         Width           =   2460
      End
      Begin VB.Label lblContFontName 
         Alignment       =   1  'Right Justify
         Caption         =   "No&m de police:"
         Height          =   210
         Left            =   100
         TabIndex        =   69
         Top             =   360
         Width           =   1100
      End
      Begin VB.Label lblContFontSize 
         Alignment       =   1  'Right Justify
         Caption         =   "&Taille de police:"
         Height          =   210
         Left            =   3950
         TabIndex        =   71
         Top             =   360
         Width           =   1200
      End
      Begin VB.Label lblAlignment 
         Alignment       =   1  'Right Justify
         Caption         =   "Ali&gnement:"
         Height          =   270
         Left            =   100
         TabIndex        =   73
         Top             =   810
         Width           =   1100
      End
   End
   Begin VB.Frame pnlContIndentation 
      Caption         =   "Retrait"
      Height          =   1250
      Left            =   120
      TabIndex        =   115
      Top             =   1760
      Visible         =   0   'False
      Width           =   6440
      Begin mpControls3.SpinTextInternational spnContLeftIndent 
         Height          =   330
         Left            =   765
         TabIndex        =   76
         Top             =   300
         Width           =   1300
         _ExtentX        =   2302
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   0.1
         MinValue        =   -576
         MaxValue        =   576
         AppendSymbol    =   -1  'True
         DisplayUnit     =   0
      End
      Begin mpControls3.SpinTextInternational spnContRightIndent 
         Height          =   330
         Left            =   765
         TabIndex        =   78
         Top             =   750
         Width           =   1300
         _ExtentX        =   2302
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   0.1
         MinValue        =   -576
         MaxValue        =   576
         AppendSymbol    =   -1  'True
         DisplayUnit     =   0
      End
      Begin mpControls3.SpinTextInternational spnBy 
         Height          =   330
         Left            =   3560
         TabIndex        =   82
         Top             =   750
         Width           =   1300
         _ExtentX        =   2302
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   0.1
         MaxValue        =   576
         AppendSymbol    =   -1  'True
         DisplayUnit     =   0
      End
      Begin TrueDBList60.TDBCombo cmbSpecial 
         Height          =   564
         Left            =   3560
         OleObjectBlob   =   "frmEditSchemeFrench.frx":6308
         TabIndex        =   80
         Top             =   300
         Width           =   1300
      End
      Begin VB.Label lblContLeftIndent 
         Alignment       =   1  'Right Justify
         Caption         =   "G&auche:"
         Height          =   315
         Left            =   80
         TabIndex        =   74
         Top             =   360
         Width           =   600
      End
      Begin VB.Label lblContRightIndent 
         Alignment       =   1  'Right Justify
         Caption         =   "D&roite:"
         Height          =   330
         Left            =   80
         TabIndex        =   77
         Top             =   810
         Width           =   600
      End
      Begin VB.Label lblSpecial 
         Alignment       =   1  'Right Justify
         Caption         =   "Sp&�cial:"
         Height          =   210
         Left            =   2830
         TabIndex        =   79
         Top             =   360
         Width           =   600
      End
      Begin VB.Label lblBy 
         Alignment       =   1  'Right Justify
         Caption         =   "&Par:"
         Height          =   210
         Left            =   2830
         TabIndex        =   81
         Top             =   810
         Width           =   600
      End
   End
   Begin VB.Frame pnlContSpacing 
      Caption         =   "Espacement"
      Height          =   1250
      Left            =   120
      TabIndex        =   114
      Top             =   3070
      Visible         =   0   'False
      Width           =   6440
      Begin mpControls3.SpinTextInternational spnContSpaceBefore 
         Height          =   330
         Left            =   715
         TabIndex        =   84
         Top             =   300
         Width           =   1300
         _ExtentX        =   2302
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   6
         MaxValue        =   1584
         AppendSymbol    =   -1  'True
      End
      Begin mpControls3.SpinTextInternational spnContSpaceAfter 
         Height          =   330
         Left            =   715
         TabIndex        =   86
         Top             =   750
         Width           =   1300
         _ExtentX        =   2302
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   6
         MaxValue        =   1584
         AppendSymbol    =   -1  'True
      End
      Begin mpControls3.SpinTextInternational spnContAt 
         Height          =   330
         Left            =   3560
         TabIndex        =   90
         Top             =   750
         Width           =   1300
         _ExtentX        =   2302
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   6
         MaxValue        =   1584
         AppendSymbol    =   -1  'True
      End
      Begin TrueDBList60.TDBCombo cmbContLineSpacing 
         Height          =   564
         Left            =   3560
         OleObjectBlob   =   "frmEditSchemeFrench.frx":822E
         TabIndex        =   88
         Top             =   300
         Width           =   1300
      End
      Begin VB.Label lblContAt 
         Alignment       =   1  'Right Justify
         Caption         =   "&De:"
         Height          =   240
         Left            =   2430
         TabIndex        =   89
         Top             =   810
         Width           =   1000
      End
      Begin VB.Label lblContSpaceAfter 
         Alignment       =   1  'Right Justify
         Caption         =   "A&pr�s:"
         Height          =   240
         Left            =   80
         TabIndex        =   85
         Top             =   810
         Width           =   600
      End
      Begin VB.Label lblContSpaceBefore 
         Alignment       =   1  'Right Justify
         Caption         =   "&Avant:"
         Height          =   240
         Left            =   80
         TabIndex        =   83
         Top             =   360
         Width           =   600
      End
      Begin VB.Label lblContLineSpacing 
         Alignment       =   1  'Right Justify
         Caption         =   "&Interligne:"
         Height          =   270
         Left            =   2430
         TabIndex        =   87
         Top             =   360
         Width           =   1000
      End
   End
   Begin VB.Frame pnlContPagination 
      Caption         =   "Pagination"
      Height          =   1045
      Left            =   120
      TabIndex        =   113
      Top             =   4380
      Visible         =   0   'False
      Width           =   6440
      Begin VB.CheckBox chkContWidowOrphan 
         Caption         =   "�viter &veuves et orphelines"
         Height          =   255
         Left            =   100
         TabIndex        =   91
         Top             =   300
         Width           =   2240
      End
      Begin VB.CheckBox chkContKeepWithNext 
         Caption         =   "Paragraphes so&lidaires"
         Height          =   255
         Left            =   3000
         TabIndex        =   93
         Top             =   300
         Width           =   1950
      End
      Begin VB.CheckBox chkContKeepLinesTogether 
         Caption         =   "Lignes &solidaires"
         Height          =   255
         Left            =   100
         TabIndex        =   92
         Top             =   650
         Width           =   1950
      End
   End
   Begin VB.Frame pnlPosition 
      Caption         =   "Format"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2835
      Left            =   120
      TabIndex        =   111
      Top             =   450
      Visible         =   0   'False
      Width           =   6440
      Begin VB.ComboBox cmbNumberAlign 
         Height          =   315
         ItemData        =   "frmEditSchemeFrench.frx":A15C
         Left            =   2520
         List            =   "frmEditSchemeFrench.frx":A169
         Style           =   2  'Dropdown List
         TabIndex        =   112
         Top             =   3090
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdSynchronize 
         Caption         =   "&Synchroniser"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4465
         Picture         =   "frmEditSchemeFrench.frx":A182
         TabIndex        =   55
         Top             =   2300
         Width           =   1135
      End
      Begin mpControls3.SpinTextInternational spnNumberPosition 
         Height          =   330
         Left            =   180
         TabIndex        =   35
         Top             =   450
         Width           =   1950
         _ExtentX        =   3440
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   0.1
         MinValue        =   -576
         MaxValue        =   576
         AppendSymbol    =   -1  'True
         DisplayUnit     =   0
      End
      Begin mpControls3.SpinTextInternational spnTextPosition 
         Height          =   330
         Left            =   180
         TabIndex        =   37
         Top             =   1080
         Width           =   1950
         _ExtentX        =   3440
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   0.1
         MinValue        =   -576
         MaxValue        =   576
         AppendSymbol    =   -1  'True
         DisplayUnit     =   0
      End
      Begin mpControls3.SpinTextInternational spnSpaceBefore 
         Height          =   330
         Left            =   2550
         TabIndex        =   51
         Top             =   1710
         Width           =   1630
         _ExtentX        =   2778
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   6
         MaxValue        =   1584
         AppendSymbol    =   -1  'True
      End
      Begin mpControls3.SpinTextInternational spnSpaceAfter 
         Height          =   330
         Left            =   4405
         TabIndex        =   53
         Top             =   1710
         Width           =   1630
         _ExtentX        =   2778
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   6
         MaxValue        =   1584
         AppendSymbol    =   -1  'True
      End
      Begin mpControls3.SpinTextInternational spnAt 
         Height          =   330
         Left            =   5135
         TabIndex        =   49
         Top             =   1080
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   6
         MaxValue        =   1584
         AppendSymbol    =   -1  'True
      End
      Begin mpControls3.SpinTextInternational spnRightIndent 
         Height          =   330
         Left            =   180
         TabIndex        =   39
         Top             =   1710
         Width           =   1950
         _ExtentX        =   3440
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   0.1
         MinValue        =   -576
         MaxValue        =   576
         AppendSymbol    =   -1  'True
         DisplayUnit     =   0
      End
      Begin TrueDBList60.TDBCombo cmbFontName 
         Height          =   564
         Left            =   2550
         OleObjectBlob   =   "frmEditSchemeFrench.frx":A2E8
         TabIndex        =   43
         Top             =   450
         Width           =   2465
      End
      Begin TrueDBList60.TDBCombo cmbFontSize 
         Height          =   564
         Left            =   5135
         OleObjectBlob   =   "frmEditSchemeFrench.frx":C20F
         TabIndex        =   45
         Top             =   450
         Width           =   900
      End
      Begin TrueDBList60.TDBCombo cmbLineSpacing 
         Height          =   564
         Left            =   2550
         OleObjectBlob   =   "frmEditSchemeFrench.frx":E136
         TabIndex        =   47
         Top             =   1080
         Width           =   2465
      End
      Begin TrueDBList60.TDBCombo cmbTextAlign 
         Height          =   564
         Left            =   180
         OleObjectBlob   =   "frmEditSchemeFrench.frx":10060
         TabIndex        =   41
         Top             =   2340
         Width           =   1950
      End
      Begin VB.Label lblNumberPosition 
         Caption         =   "&Retrait du num�ro:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   195
         TabIndex        =   34
         Top             =   240
         Width           =   1400
      End
      Begin VB.Label lblTextPosition 
         Caption         =   "Retrait de &texte:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   195
         TabIndex        =   36
         Top             =   870
         Width           =   1400
      End
      Begin VB.Label lblTextAlign 
         Caption         =   "Ali&gnement:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   195
         TabIndex        =   40
         Top             =   2130
         Width           =   840
      End
      Begin VB.Label lblLineSpacing 
         Caption         =   "&Interligne:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2565
         TabIndex        =   46
         Top             =   870
         Width           =   1395
      End
      Begin VB.Label lblSpaceAfter 
         Caption         =   "Espace a&pr�s:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4540
         TabIndex        =   52
         Top             =   1500
         Width           =   1100
      End
      Begin VB.Label lblSpaceBefore 
         Caption         =   "Espace &avant:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2565
         TabIndex        =   50
         Top             =   1500
         Width           =   1200
      End
      Begin VB.Label lblAt 
         Caption         =   "D&e:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5150
         TabIndex        =   48
         Top             =   870
         Width           =   500
      End
      Begin VB.Label lblFontName 
         Caption         =   "No&m de police:"
         Height          =   240
         Left            =   2565
         TabIndex        =   42
         Top             =   240
         Width           =   1395
      End
      Begin VB.Label lblFontSize 
         Caption         =   "Tai&lle:"
         Height          =   240
         Left            =   5150
         TabIndex        =   44
         Top             =   240
         Width           =   585
      End
      Begin VB.Label lblRightIndent 
         Caption         =   "Retrait d&roit:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   195
         TabIndex        =   38
         Top             =   1500
         Width           =   1290
      End
      Begin VB.Label lblSynchronize 
         Caption         =   "Appliquer Format pour continuer le style:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   2565
         TabIndex        =   54
         Top             =   2250
         Width           =   2000
      End
   End
   Begin VB.Frame pnlFont 
      Caption         =   "Titre"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1065
      Left            =   120
      TabIndex        =   110
      Top             =   4360
      Visible         =   0   'False
      Width           =   6440
      Begin VB.CheckBox chkTCSmallCaps 
         Caption         =   "P&etites majuscules"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   195
         TabIndex        =   65
         Top             =   680
         Width           =   1665
      End
      Begin VB.CheckBox chkTCUnderline 
         Caption         =   "&Soulign�"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1900
         TabIndex        =   64
         Top             =   315
         Width           =   1000
      End
      Begin VB.CheckBox chkTCBold 
         Caption         =   "&Gras"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   195
         TabIndex        =   62
         Top             =   315
         Width           =   675
      End
      Begin VB.OptionButton optStyleFormats 
         Caption         =   "Sa&uvegarder style (Titre seul)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3700
         TabIndex        =   68
         Top             =   630
         Width           =   2675
      End
      Begin VB.OptionButton optDirectFormats 
         Caption         =   "&Format direct (activer dans Titre)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3700
         TabIndex        =   67
         Top             =   315
         Width           =   2675
      End
      Begin VB.CheckBox chkTCCaps 
         Caption         =   "Toutes &majuscules"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1900
         TabIndex        =   66
         Top             =   680
         Width           =   1665
      End
      Begin VB.CheckBox chkTCItalic 
         Caption         =   "&Italique"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   990
         TabIndex        =   63
         Top             =   315
         Width           =   810
      End
   End
   Begin VB.Frame pnlOther 
      Caption         =   "Autre"
      Height          =   1065
      Left            =   120
      TabIndex        =   109
      Top             =   3300
      Visible         =   0   'False
      Width           =   6440
      Begin VB.CheckBox chkKeepWithNext 
         Caption         =   "Paragraphes so&lidaires"
         Height          =   240
         Left            =   4250
         TabIndex        =   60
         Top             =   275
         Width           =   1900
      End
      Begin VB.CheckBox chkKeepLinesTogether 
         Caption         =   "Lignes &solidaires"
         Height          =   240
         Left            =   2375
         TabIndex        =   59
         Top             =   625
         Width           =   1780
      End
      Begin VB.CheckBox chkWidowOrphan 
         Caption         =   "&Veuves et orphelines"
         Height          =   240
         Left            =   2375
         TabIndex        =   58
         Top             =   275
         Width           =   1800
      End
      Begin VB.CheckBox chkPageBreak 
         Caption         =   "&Saut de page avant"
         Height          =   240
         Left            =   4250
         TabIndex        =   61
         Top             =   625
         Width           =   1900
      End
      Begin TrueDBList60.TDBCombo cmbNextParagraph 
         Height          =   564
         Left            =   180
         OleObjectBlob   =   "frmEditSchemeFrench.frx":11F88
         TabIndex        =   57
         Top             =   485
         Width           =   1950
      End
      Begin VB.Label lblNextParagraph 
         Caption         =   "Style du paragrap&he suivant:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   195
         TabIndex        =   56
         Top             =   275
         Width           =   1680
      End
   End
   Begin VB.Frame pnlNumber 
      Height          =   3390
      Left            =   120
      TabIndex        =   107
      Top             =   450
      Width           =   6440
      Begin VB.CheckBox chkRightAlign 
         Caption         =   "Num�ro &align� � droite"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   380
         Left            =   3260
         TabIndex        =   22
         Top             =   2500
         Width           =   1900
      End
      Begin VB.TextBox txtStartAt 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   2330
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   1145
         Width           =   770
      End
      Begin VB.CommandButton btnAddPrevLevel 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3260
         Picture         =   "frmEditSchemeFrench.frx":13EB4
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   465
         Width           =   495
      End
      Begin VB.CheckBox chkReset 
         Caption         =   "Recommencer la num�rotation apr�s le &premier niveau"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   380
         Left            =   165
         TabIndex        =   20
         Top             =   2500
         Width           =   3000
      End
      Begin VB.CheckBox chkLegalStyle 
         Caption         =   "Style Num�rotation &juridique"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   165
         TabIndex        =   21
         Top             =   2950
         Width           =   3000
      End
      Begin VB.CheckBox chkNonbreakingSpaces 
         Caption         =   "Uti&liser espaces ins�cables"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3260
         TabIndex        =   23
         Top             =   2950
         Width           =   2500
      End
      Begin RichTextLib.RichTextBox rtfNumberFormat 
         Height          =   330
         Left            =   150
         TabIndex        =   10
         Top             =   450
         Width           =   2950
         _ExtentX        =   5212
         _ExtentY        =   582
         _Version        =   393217
         MultiLine       =   0   'False
         TextRTF         =   $"frmEditSchemeFrench.frx":13FB6
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin ComCtl2.UpDown udStartAt 
         Height          =   330
         Left            =   3100
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   1145
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         Value           =   1
         OrigLeft        =   1260
         OrigTop         =   1110
         OrigRight       =   1500
         OrigBottom      =   1470
         Max             =   1000
         Min             =   1
         Enabled         =   -1  'True
      End
      Begin mpControls3.SpinTextInternational spnTabPosition 
         Height          =   330
         Left            =   2320
         TabIndex        =   19
         Top             =   1825
         Width           =   1000
         _ExtentX        =   1773
         _ExtentY        =   582
         Appearance      =   1
         IncrementValue  =   0.1
         MaxValue        =   576
         AppendSymbol    =   -1  'True
         DisplayUnit     =   0
      End
      Begin TrueDBList60.TDBList lstPrevLevels 
         Height          =   1830
         Left            =   3930
         OleObjectBlob   =   "frmEditSchemeFrench.frx":14038
         TabIndex        =   117
         ToolTipText     =   "Double-click or drag to add the selected previous level to the current level"
         Top             =   430
         Width           =   1680
      End
      Begin TrueDBList60.TDBCombo cmbTrailingChar 
         Height          =   564
         Left            =   150
         OleObjectBlob   =   "frmEditSchemeFrench.frx":161B1
         TabIndex        =   17
         Top             =   1825
         Width           =   2005
      End
      Begin TrueDBList60.TDBCombo cmbNumberStyle 
         Height          =   564
         Left            =   150
         OleObjectBlob   =   "frmEditSchemeFrench.frx":180DC
         TabIndex        =   12
         Top             =   1145
         Width           =   2005
      End
      Begin VB.Label lblNumberFormat 
         Caption         =   "Format de nu&m�ro:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   180
         TabIndex        =   9
         Top             =   255
         Width           =   1575
      End
      Begin VB.Label lblNumberStyle 
         Caption         =   "St&yle de num�ro:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   180
         TabIndex        =   11
         Top             =   935
         Width           =   1320
      End
      Begin VB.Label lblPreviousLevels 
         Appearance      =   0  'Flat
         Caption         =   "Ni&veaux pr�c�dents:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   3930
         TabIndex        =   108
         Top             =   240
         Width           =   1940
      End
      Begin VB.Label lblStartAt 
         Alignment       =   1  'Right Justify
         Caption         =   "&Commencer � :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2330
         TabIndex        =   13
         Top             =   935
         Width           =   1100
      End
      Begin VB.Label lblTrailingChar 
         Caption         =   "Faire s&uivre le num�ro de :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   180
         TabIndex        =   16
         Top             =   1615
         Width           =   1950
      End
      Begin VB.Label lblTabPosition 
         Alignment       =   2  'Center
         Caption         =   "Lar&geur:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2230
         TabIndex        =   18
         Top             =   1615
         Width           =   1000
      End
   End
   Begin VB.Frame pnlNumberFont 
      Caption         =   "Police"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1600
      Left            =   120
      TabIndex        =   105
      Top             =   3900
      Width           =   6440
      Begin VB.CheckBox chkNumberCaps 
         Caption         =   "Toutes &majuscules"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1830
         TabIndex        =   27
         Top             =   475
         Width           =   1700
      End
      Begin VB.CheckBox chkNumberItalic 
         Caption         =   "&Italique"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   937
         TabIndex        =   26
         Top             =   475
         Width           =   870
      End
      Begin VB.CheckBox chkNumberUnderline 
         Caption         =   "&Underline"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1395
         TabIndex        =   106
         Top             =   2200
         Visible         =   0   'False
         Width           =   1020
      End
      Begin VB.CheckBox chkNumberBold 
         Caption         =   "&Gras"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   165
         TabIndex        =   25
         Top             =   475
         Width           =   750
      End
      Begin TrueDBList60.TDBCombo cmbUnderlineStyle 
         Height          =   564
         Left            =   150
         OleObjectBlob   =   "frmEditSchemeFrench.frx":1A006
         TabIndex        =   29
         Top             =   1100
         Width           =   2460
      End
      Begin TrueDBList60.TDBCombo cmbNumFontName 
         Height          =   564
         Left            =   3760
         OleObjectBlob   =   "frmEditSchemeFrench.frx":1BF33
         TabIndex        =   31
         Top             =   450
         Width           =   2460
      End
      Begin TrueDBList60.TDBCombo cmbNumFontSize 
         Height          =   564
         Left            =   3760
         OleObjectBlob   =   "frmEditSchemeFrench.frx":1DE5D
         TabIndex        =   33
         Top             =   1100
         Width           =   2460
      End
      Begin VB.Label Label2 
         Caption         =   "&Soulign�:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   195
         TabIndex        =   28
         Top             =   890
         Width           =   795
      End
      Begin VB.Label lblNumFontSize 
         Caption         =   "&Taille:"
         Height          =   210
         Left            =   3775
         TabIndex        =   32
         Top             =   890
         Width           =   750
      End
      Begin VB.Label lblNumFontName 
         Caption         =   "N&om:"
         Height          =   210
         Left            =   3775
         TabIndex        =   30
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.CheckBox chkTab 
      Caption         =   "Style C&ont"
      Height          =   350
      Index           =   2
      Left            =   2290
      Style           =   1  'Graphical
      TabIndex        =   104
      Top             =   60
      Width           =   1035
   End
   Begin VB.CheckBox chkTab 
      Caption         =   "&Paragraphe"
      Height          =   350
      Index           =   1
      Left            =   1205
      Style           =   1  'Graphical
      TabIndex        =   103
      Top             =   60
      Width           =   1035
   End
   Begin VB.CheckBox chkTab 
      Caption         =   "&Num�ro"
      Enabled         =   0   'False
      Height          =   350
      Index           =   0
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   102
      Top             =   60
      Value           =   1  'Checked
      Width           =   1035
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "&Actualiser"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1105
      TabIndex        =   95
      Top             =   5620
      Width           =   1000
   End
   Begin VB.CheckBox chkZoom 
      Caption         =   "&Zoom"
      Height          =   360
      Left            =   105
      Style           =   1  'Graphical
      TabIndex        =   94
      Top             =   5620
      Value           =   2  'Grayed
      Width           =   1000
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&9"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   8
      Left            =   6065
      Style           =   1  'Graphical
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   60
      Width           =   330
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&8"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   7
      Left            =   5735
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   60
      Width           =   330
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&7"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   6
      Left            =   5405
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   60
      Width           =   330
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&6"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   5
      Left            =   5075
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   60
      Width           =   330
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   4
      Left            =   4745
      Style           =   1  'Graphical
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   60
      Width           =   330
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   3
      Left            =   4415
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   60
      Width           =   330
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   2
      Left            =   4085
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   60
      Width           =   330
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   1
      Left            =   3755
      Style           =   1  'Graphical
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   60
      Width           =   330
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&1"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   0
      Left            =   3425
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   60
      Value           =   1  'Checked
      Width           =   330
   End
   Begin VB.ComboBox cmbLevel 
      Height          =   315
      ItemData        =   "frmEditSchemeFrench.frx":1FD87
      Left            =   1050
      List            =   "frmEditSchemeFrench.frx":1FD89
      Style           =   2  'Dropdown List
      TabIndex        =   100
      TabStop         =   0   'False
      Top             =   6500
      Visible         =   0   'False
      Width           =   750
   End
   Begin VB.CommandButton cmdDeleteLevel 
      Caption         =   "Effacer niveau"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3340
      TabIndex        =   97
      Top             =   5620
      Width           =   1235
   End
   Begin VB.CommandButton cmdAddLevel 
      Caption         =   "Ajouter niveau"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2105
      TabIndex        =   96
      Top             =   5620
      Width           =   1235
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "O&K"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4575
      TabIndex        =   98
      Top             =   5620
      Width           =   1000
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5575
      TabIndex        =   99
      Top             =   5620
      Width           =   1000
   End
   Begin VB.Label Label1 
      Caption         =   "&Level:"
      Enabled         =   0   'False
      Height          =   315
      Left            =   300
      TabIndex        =   101
      Top             =   6500
      Visible         =   0   'False
      Width           =   585
   End
End
Attribute VB_Name = "frmEditSchemeFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bForceSelContent As Boolean
Private m_xCode As String
Private xUserAll As String
Private bRestore As Boolean
Private m_bFormInit As Boolean
Private m_bAllowRefresh As Boolean
Private m_bClicked As Boolean
Private m_iPrevTRISTATE As OLE_TRISTATE
Private m_bStopChangeEvent As Boolean
Private m_bNoSelChangeEvent As Boolean
Private m_bIsPrevLevels As Boolean
Private m_iSelStart As Integer
Private m_bLevelChanged As Boolean
Private m_bLevelDirty As Boolean
Private m_bTCDirty As Boolean
Private m_bTOCDirty As Boolean
Private m_bPropDirty As Boolean
Private m_bParaDirty As Boolean
Private m_bNextParaDirty As Boolean
Private m_iLastSelLevel As Integer
Private m_xarPrevLevels As xArray
Private m_oPreview As CPreview
Private m_bSelChangeRunning As Boolean
Private m_bInsertingPrevLevel As Boolean
Private m_bAlertDisplayed As Boolean
Private m_bSwitchingTabs As Boolean
Private m_xPrevCtl As String
Private m_bRawNumberFormat As Boolean
Private m_iFixedDigitNumberingOffset As Integer
Private m_xBulletFont As String
Private m_xBulletCharNum As String
Private m_xarNumFontName As xArray
Private m_xarLineSpacing As xArray
Private m_xarNextPara As xArray
Private m_xarNumStyle As xArray
Private m_lRulerDisplayDelay As Long '9.9.6012

'these are exposed as properties
Private m_iInitLevels As Integer
Private m_iCurLevels As Integer
Private m_bCancelled As Boolean
Private m_xarIsDirty As xArray
Private m_docStart As Word.Document
Private m_docPreview As Word.Document
Private m_bCreateBitmap As Boolean
Private m_bZoomClicked As Boolean 'GLOG 5549

Public Property Let StartDoc(docP As Word.Document)
    Set m_docStart = docP
End Property

Public Property Get StartDoc() As Word.Document
    Set StartDoc = m_docStart
End Property

Public Property Let PreviewDoc(docP As Word.Document)
    Set m_docPreview = docP
End Property

Public Property Get PreviewDoc() As Word.Document
    Set PreviewDoc = m_docPreview
End Property

Public Property Let RulerDisplayDelay(iLength As Long)
    m_lRulerDisplayDelay = iLength
End Property


Private Sub btnAddPrevLevel_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    InsertPrevLevel
End Sub
Private Sub chkKeepLinesTogether_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub
Private Sub chkKeepLinesTogether_LostFocus()
    m_xPrevCtl = "chkKeepLinesTogether"
End Sub
Private Sub chkKeepWithNext_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub
Private Sub chkKeepWithNext_LostFocus()
    m_xPrevCtl = "chkKeepWithNext"
End Sub

Private Sub chkTab_Click(Index As Integer)
    Dim i As Integer
    Dim xCaption As String

    On Error GoTo ProcError
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    
    If Me.chkTab.Item(Index).Value = vbChecked Then
        'uncheck all buttons other than the one clicked
        For i = 0 To 2
            With Me.chkTab.Item(i)
                If i <> Index Then
                    .Value = vbUnchecked
                    .Enabled = True
                Else
                    .Enabled = False
                End If
            End With
        Next
        
        'display appropriate frames
        Me.pnlNumber.Visible = (Index = 0)
        Me.pnlNumberFont.Visible = (Index = 0)
        Me.pnlFont.Visible = (Index = 1)
        Me.pnlOther.Visible = (Index = 1)
        Me.pnlPosition.Visible = (Index = 1)
        Me.pnlContGeneral.Visible = (Index = 2)
        Me.pnlContIndentation.Visible = (Index = 2)
        Me.pnlContPagination.Visible = (Index = 2)
        Me.pnlContSpacing.Visible = (Index = 2)
        
        'set caption and select appropriate control
        On Error Resume Next
        Select Case Index
            Case 0
                xCaption = "Num�rotation"
                Me.rtfNumberFormat.SetFocus
                If Err.Number Then
                    Me.cmbNumberStyle.SetFocus
                End If
            Case 1
                xCaption = "Paragraphe"
                If g_bNoSchemeIndents Then
                    Me.spnSpaceBefore.SetFocus
                Else
                    Me.spnNumberPosition.SetFocus
                End If
            Case 2
                xCaption = "Style Cont (continu)"
                Me.cmbContFontName.SetFocus
        End Select
        On Error GoTo ProcError
        Me.Caption = " Modifier th�me " & g_oCurScheme.Alias & _
            " - " & xCaption
    End If

    Exit Sub
ProcError:
    EchoOn
    If Err.Number <> 5 Then _
        RaiseError "frmEditSchemeFrench.chkTab_Click"
    Exit Sub
End Sub

Private Sub chkWidowOrphan_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub
Private Sub chkWidowOrphan_LostFocus()
    m_xPrevCtl = "chkWidowOrphan"
End Sub
Private Sub chkPageBreak_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub
Private Sub chkPageBreak_LostFocus()
    m_xPrevCtl = "chkPageBreak"
End Sub
Private Sub chkLegalStyle_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
    FormatLegalNumber
End Sub

Private Sub chkLegalStyle_LostFocus()
    m_xPrevCtl = "chkLegalStyle"
End Sub

Private Sub chkLevel_Click(Index As Integer)
    Dim i As Integer

    On Error GoTo ProcError
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    
    If Me.chkLevel.Item(Index).Value = vbChecked Then
'       uncheck all buttons other than the one clicked
        For i = 0 To m_iCurLevels - 1
            With Me.chkLevel.Item(i)
                If i <> Index Then
                    .Value = vbUnchecked
                    .Enabled = True
                Else
                    .Enabled = False
                End If
            End With
        Next
        
'       set cmblevel to the clicked level -
'       cmblevel exists because these chkboxes
'       were added after the app was almost
'       completely built.
        Me.cmbLevel.ListIndex = Index
    
'       reselect previous control
        With Me.Controls(m_xPrevCtl)
            On Error Resume Next
            If m_xPrevCtl = "optDirectFormats" Then
                If Not Me.optDirectFormats Then
                    Me.optStyleFormats.SetFocus
                End If
            ElseIf m_xPrevCtl = "optStyleFormats" Then
                If Not Me.optStyleFormats Then
                    Me.optDirectFormats.SetFocus
                End If
            Else
                .SetFocus
            End If
            If .Name <> "rtfNumberFormat" Then
                .SelLength = Len(.Text)
            End If
        End With
    End If
    Exit Sub
ProcError:
    EchoOn
    If Err.Number = 5 Then
'       the control can't receive focus -
'       select a different control
        On Error Resume Next
        If Me.chkTab(0).Value = vbChecked Then
            Me.rtfNumberFormat.SetFocus
            If Err.Number Then
                Me.cmbNumberStyle.SetFocus
            End If
        ElseIf Me.chkTab(1).Value = vbChecked Then
            If g_bNoSchemeIndents Then
                Me.spnSpaceBefore.SetFocus
            Else
                Me.spnNumberPosition.SetFocus
            End If
        Else
            Me.cmbContFontName.SetFocus
        End If
    Else
        RaiseError "frmEditSchemeFrench.chkLevel_Click"
    End If
    Exit Sub
End Sub

Private Sub chkNonbreakingSpaces_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkNonbreakingSpaces_LostFocus()
    m_xPrevCtl = "chkNonbreakingSpaces"
End Sub

Private Sub chkNumberBold_Click()
'    SetTriStateChkbox Me.chkNumberBold
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkNumberBold_LostFocus()
    m_xPrevCtl = "chkNumberBold"
End Sub

Private Sub chkNumberBold_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_iPrevTRISTATE = Me.chkNumberBold
End Sub

Private Sub chkNumberCaps_Click()
'    SetTriStateChkbox Me.chkNumberCaps
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkNumberCaps_LostFocus()
    m_xPrevCtl = "chkNumberCaps"
End Sub

Private Sub chkNumberCaps_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_iPrevTRISTATE = Me.chkNumberCaps
End Sub

Private Sub chkNumberItalic_Click()
'    SetTriStateChkbox Me.chkNumberItalic
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkNumberItalic_LostFocus()
    m_xPrevCtl = "chkNumberItalic"
End Sub

Private Sub chkNumberItalic_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_iPrevTRISTATE = Me.chkNumberItalic
End Sub

Private Sub chkNumberUnderline_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    SetTriStateChkbox Me.chkNumberUnderline
    SetTrailUnderlineState
End Sub

Private Sub chkNumberUnderline_LostFocus()
    m_xPrevCtl = "chkNumberUnderline"
    RefreshLevel
End Sub

Private Sub chkNumberUnderline_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_iPrevTRISTATE = Me.chkNumberUnderline
End Sub

Private Sub chkReset_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkReset_LostFocus()
    m_xPrevCtl = "chkReset"
End Sub

Private Sub chkRightAlign_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkRightAlign_LostFocus()
    m_xPrevCtl = "chkRightAlign"
End Sub

Private Sub chkTCBold_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bTCDirty = True
    If Me.optStyleFormats Then _
        m_bParaDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkTCBold_LostFocus()
    m_xPrevCtl = "chkTCBold"
End Sub

Private Sub chkTCCaps_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bTCDirty = True
    If Me.optStyleFormats Then _
        m_bParaDirty = True
    If chkTCCaps = 1 Then _
        chkTCSmallCaps = 0
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkTCCaps_LostFocus()
    m_xPrevCtl = "chkTCCaps"
End Sub

Private Sub chkTCItalic_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bTCDirty = True
    If Me.optStyleFormats Then _
        m_bParaDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkTCItalic_LostFocus()
    m_xPrevCtl = "chkTCItalic"
End Sub

Private Sub chkTCSmallCaps_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bTCDirty = True
    If Me.optStyleFormats Then _
        m_bParaDirty = True
    If chkTCSmallCaps = 1 Then _
        chkTCCaps = 0
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkTCSmallCaps_LostFocus()
    m_xPrevCtl = "chkTCSmallCaps"
End Sub

Private Sub chkTCUnderline_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bTCDirty = True
    If Me.optStyleFormats Then _
        m_bParaDirty = True
    If Not m_bStopChangeEvent Then _
        RefreshLevel
End Sub

Private Sub chkTCUnderline_LostFocus()
    m_xPrevCtl = "chkTCUnderline"
End Sub

Private Sub chkTrailingUnderline_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bPropDirty = True
    m_bLevelDirty = True
    
End Sub

Private Sub chkTrailingUnderline_LostFocus()
    m_xPrevCtl = "chkTrailingUnderline"
    RefreshLevel
End Sub

Private Sub chkZoom_Click()
    Dim Preview As MPN90.CPreview
    Dim bZoomIn As Boolean
    
    If Not m_bFormInit Then
        If ActiveDocument <> g_docPreview Then _
            g_docPreview.Activate
            
        '9.9.6006 (GLOG 5549)
        If g_iWordVersion = mpWordVersion_2013 Then
            'workaround for ruler not reappearing in Word 2013
            'with modal dialog visible
            m_bZoomClicked = True
            Me.Hide
        Else
            'moved code into shared method outside of form
            Me.MousePointer = vbHourglass
            bZoomIn = (Me.chkZoom.Value = vbChecked)
            SetPreviewZoom bZoomIn
            Me.MousePointer = vbDefault
        End If
    End If
End Sub

Private Sub cmbAlignment_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAlignment, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbAlignment_Mismatch"
    Exit Sub

End Sub

Private Sub cmbContFontName_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbContFontName, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbContFontName_Mismatch"
    Exit Sub
End Sub

Private Sub cmbContFontSize_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    'GLOG 5262 (9.9.5003) - I was previously setting the wrong flags here
    m_bNextParaDirty = True
End Sub

Private Sub cmbContFontSize_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbContFontSize, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbContFontSize_Mismatch"
    Exit Sub
End Sub

Private Sub cmbContFontSize_Validate(Cancel As Boolean)
    If cmbContFontSize.Text <> "-Non d�fini-" Then
        Cancel = Not bIsValidFontSize(cmbContFontSize.Text)
    End If
End Sub

Private Sub cmbContLineSpacing_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbContLineSpacing, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbContLineSpacing_Mismatch"
    Exit Sub
End Sub

'Private Sub cmbFontName_Click()
Private Sub cmbFontName_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bTCDirty = True
    m_bParaDirty = True
End Sub

Private Sub cmbFontName_LostFocus()
    m_xPrevCtl = "cmbFontName"
    RefreshLevel
End Sub

Private Sub cmbFontName_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbFontName, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbFontName_Mismatch"
    Exit Sub
End Sub

Private Sub cmbFontSize_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bTCDirty = True
    m_bParaDirty = True
End Sub

'Private Sub cmbFontSize_Click()
Private Sub cmbFontSize_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bTCDirty = True
    m_bParaDirty = True
End Sub

Private Sub cmbFontSize_LostFocus()
    m_xPrevCtl = "cmbFontSize"
    
    'GLOG 5031 (2/1/12) - don't refresh if cancelling
    If Not Me.ActiveControl Is Me.cmdCancel Then
        RefreshLevel
    End If
End Sub

'Private Sub cmbContFontName_Click()
Private Sub cmbContFontName_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub

Private Sub cmbContFontName_LostFocus()
    m_xPrevCtl = "cmbContFontName"
End Sub

'Private Sub cmbContFontSize_Click()
Private Sub cmbContFontSize_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub

Private Sub cmbContFontSize_LostFocus()
    m_xPrevCtl = "cmbContFontSize"
End Sub

'Private Sub cmbAlignment_Click()
Private Sub cmbAlignment_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub

Private Sub cmbAlignment_LostFocus()
    m_xPrevCtl = "cmbAlignment"
End Sub

Private Sub cmbFontSize_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbFontSize, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbFontSize_Mismatch"
    Exit Sub

End Sub

Private Sub cmbFontSize_Validate(Cancel As Boolean)
    If cmbFontSize.Text <> "-Non d�fini-" Then
        Cancel = Not bIsValidFontSize(cmbFontSize.Text)
    End If
End Sub

Private Sub cmbLineSpacing_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLineSpacing, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbLineSpacing_Mismatch"
    Exit Sub
End Sub

Private Sub cmbNextParagraph_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNextParagraph, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbNextParagraph_Mismatch"
    Exit Sub
End Sub

'Private Sub cmbNumFontName_Click()
Private Sub cmbNumFontName_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
End Sub

Private Sub cmbNumFontName_LostFocus()
    m_xPrevCtl = "cmbNumFontName"
    RefreshLevel
End Sub

Private Sub cmbNumFontName_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNumFontName, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbNumFontName_Mismatch"
    Exit Sub
End Sub

Private Sub cmbNumFontSize_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
End Sub

'Private Sub cmbNumFontSize_Click()
Private Sub cmbNumFontSize_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
End Sub

Private Sub cmbNumFontSize_LostFocus()
    m_xPrevCtl = "cmbNumFontSize"
    
    'GLOG 5031 (2/1/12) - don't refresh if cancelling
    If Not Me.ActiveControl Is Me.cmdCancel Then
        RefreshLevel
    End If
End Sub

Private Sub cmbLevel_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    SwitchLevels
'    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
End Sub

Private Sub cmbLineSpacing_ItemChange()
    Dim iSpacing As WdLineSpacing
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True

    iSpacing = Me.cmbLineSpacing.Bookmark
    Me.lblAt.Enabled = (iSpacing > 2)
    With Me.spnAt
        .Enabled = (iSpacing > 2)
        .AppendSymbol = (iSpacing <> wdLineSpaceMultiple)
        If iSpacing = wdLineSpaceMultiple Then
            .MinValue = 0.5
            .IncrementValue = 0.5
        Else
            .MinValue = 1
            .IncrementValue = 1
        End If
        If Not m_bLevelChanged Then
            Select Case iSpacing
                Case wdLineSpaceSingle, wdLineSpaceAtLeast, wdLineSpaceExactly
                    .Value = 12
                Case wdLineSpace1pt5
                    .Value = 18
                Case wdLineSpaceDouble
                    .Value = 24
                Case wdLineSpaceMultiple
                    .Value = 3
            End Select
            If iSpacing < 3 Then
                .DisplayText = ""
            Else
                .Refresh
            End If
        End If
    End With
End Sub

Private Sub cmbLineSpacing_LostFocus()
    m_xPrevCtl = "cmbLineSpacing"
    RefreshLevel
End Sub

'Private Sub cmbNextParagraph_Click()
Private Sub cmbNextParagraph_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
    m_bNextParaDirty = True
End Sub

Private Sub cmbNextParagraph_LostFocus()
    m_xPrevCtl = "cmbNextParagraph"
End Sub

Private Sub cmbNumberAlign_LostFocus()
    m_xPrevCtl = "cmbNumberAlign"
End Sub

Private Sub cmbNumberStyle_ItemChange()
    Dim iNumberStyle As Integer
    Dim iLevel As Integer
    Dim iStartAt As Integer
    Dim iPos As Integer
    Dim iOldNumLength As Integer
    Dim xNewNumber As String
    Dim bEnabled As Boolean
    Dim xLT As String
    Dim bReformatForMax As Boolean
    Dim lMax As Long

    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    iNumberStyle = cmbNumberStyle.SelectedItem
    If iNumberStyle > 9 Then _
        iNumberStyle = iNumberStyle + m_iFixedDigitNumberingOffset
        
    If iNumberStyle = mpListNumberStyleNewBullet Then
        Dim oDlg As Word.Dialog
        Set oDlg = Word.Dialogs(wdDialogInsertSymbol)
        If oDlg.Display() = -1 Then
            If oDlg.CharNum Then
                m_xBulletFont = oDlg.Font
                m_xBulletCharNum = ChrW(oDlg.CharNum)
            End If
        End If
        iNumberStyle = mpListNumberStyleBullet
        Me.cmbNumberStyle.SelectedItem = Me.cmbNumberStyle.SelectedItem - 1
        Me.cmbNumberStyle.SetFocus
    End If
        
    'if this is an unsupported style, we'll leave the level tokens unevaluated
    m_bRawNumberFormat = (iNumberStyle > 13)
    
    iLevel = Me.cmbLevel
    xLT = xGetFullLTName(g_oCurScheme.Name)
        
'   set max StartAt value - some styles have limits
    Select Case Me.cmbNumberStyle.Text
        Case "Zodiac 1"
            lMax = 10
        Case "Zodiac 2"
            lMax = 12
        Case "Zodiac 3"
            lMax = 60
        Case Else
            lMax = 1000
    End Select
    Me.udStartAt.Max = lMax
    
'   fill StartAt
    If m_bLevelChanged Then
        iStartAt = ActiveDocument.ListTemplates(xLT) _
            .ListLevels(iLevel).StartAt
        If iStartAt > 1000 Then
'           this won't by itself trigger save or
'           refresh preview, but it's the easiest way
'           to prevent errors and unintended consequences
            ActiveDocument.ListTemplates(xLT) _
                .ListLevels(iLevel).StartAt = 1000
            iStartAt = 1000
            bReformatForMax = True
        End If
    Else
        iStartAt = 1
    End If
    
    If iStartAt <> 0 Then
'       we don't allow up/down to go to 0, but Word does;
'       0 setting is preserved in bCreatelevel
        udStartAt.Value = iStartAt
    End If
    
    If m_bRawNumberFormat Then
        'style is unsupported - display arabic
        Me.txtStartAt.Text = xIntToListNumStyle(iStartAt, _
            wdListNumberStyleArabic) & " "
    Else
        Select Case iNumberStyle
            Case mpListNumberStyleNone, mpListNumberStyleBullet
                'no start at value
                Me.txtStartAt.Text = ""
            Case mpListNumberStyleOrdinal, mpListNumberStyleOrdinalText, _
                    mpListNumberStyleCardinalText
                'display arabic
                Me.txtStartAt.Text = xIntToListNumStyle(iStartAt, _
                    wdListNumberStyleArabic) & " "
            Case Else
                'display actual character
                Me.txtStartAt.Text = xIntToListNumStyle(iStartAt, _
                    iMapNumberStyle_MPToWord(iNumberStyle)) & Space(1)
        End Select
    End If
    
    bEnabled = (iNumberStyle <> mpListNumberStyleNone) And _
               (iNumberStyle <> mpListNumberStyleBullet)
    Me.udStartAt.Enabled = bEnabled
    Me.txtStartAt.Enabled = bEnabled
    Me.lblStartAt.Enabled = bEnabled
    
'   enable/disable legal style
    If iNumberStyle = mpListNumberStyleArabic Or _
            iNumberStyle = mpListNumberStyleArabicLZ Then
        Me.chkLegalStyle.Enabled = True
    Else
        With Me.chkLegalStyle
            .Enabled = False
            If .Value = vbChecked Then
                .Value = vbUnchecked
            End If
        End With
    End If
    
'   enable/disable restart numbering
    With Me.chkReset
        If iLevel = 1 Or _
                iNumberStyle = mpListNumberStyleNone Or _
                iNumberStyle = mpListNumberStyleBullet Then
            m_bStopChangeEvent = True
            .Value = 0
            .Enabled = False
        Else
            .Enabled = True
        End If
        If Not m_bLevelChanged Then _
            .Value = Abs(.Enabled)
    End With
    
'   enable/disable previous levels
    Me.lstPrevLevels.Enabled = Not mpListNumberStyleBullet
    
'   if no number, default to no trailing character
'    If (iNumberStyle = mpListNumberStyleNone) And _
'            (Me.rtfNumberFormat.Text = "") And _
'            (Not m_bLevelChanged) Then
'       Me.cmbTrailingChar.ListIndex = mpTrailingChar_None
'    End If
    
'   update number format
    If (Not m_bLevelChanged) Or bReformatForMax Then
        m_bStopChangeEvent = True
        If iNumberStyle = mpListNumberStyleBullet Then
            With Me.rtfNumberFormat
                .Enabled = False
                .Text = ChrW(&HB7)
                .Font.Name = "Symbol"
                m_xCode = ChrW(&HB7)
            End With
            
'           update Extras tab
            Me.cmbNumFontName.BoundText = m_xBulletFont
            
        Else        'anything other than bullet
            If m_bRawNumberFormat Then
                xNewNumber = "%" & iLevel
            Else
                xNewNumber = xIntToListNumStyle(iStartAt, _
                    iMapNumberStyle_MPToWord(iNumberStyle))
            End If
            iPos = InStr(m_xCode, LTrim(Str(iLevel)))
            If iPos = 0 Then    'no number
                iPos = 1
            End If
            iOldNumLength = lCountChrs(m_xCode, _
                LTrim(Str(iLevel)))
            
'           insert and format new number
            With Me.rtfNumberFormat
'               if style was previously bullet
'               clear and enable text box
                If .Enabled = False Or .Font.Name = "Symbol" Then
                    .Enabled = True
                    .Font.Name = "Arial"
                    .Text = ""
                    m_xCode = ""
                    .SelColor = vbBlue

'                   update Extras tab
                    If Me.cmbNumFontName.Text = "Symbol" Or _
                            Me.cmbNumFontName.Text = "Wingdings" Then
                        Me.cmbNumFontName.BoundText = Me.cmbFontName.Text
                    End If
                End If
                .SelStart = iPos - 1
                .SelLength = iOldNumLength
                .SelText = xNewNumber
                If iOldNumLength = 0 Then
                    .SelStart = iPos - 1
                    .SelLength = Len(xNewNumber)
                    .SelColor = vbBlue
                    .SelStart = 0
                End If
                xUserAll = .TextRTF
'                .SelColor = vbBlue
            End With
            
            If m_xCode = "" Then
'               start new code
                m_xCode = String(Len(xNewNumber), LTrim(Str(iLevel)))
            Else
'               edit existing code
                m_xCode = Left(m_xCode, iPos - 1) & _
                    String(Len(xNewNumber), LTrim(Str(iLevel))) & _
                    Right(m_xCode, Len(m_xCode) - iPos - iOldNumLength + 1)
            End If
        End If
        m_bStopChangeEvent = False
    End If
    m_bAllowRefresh = True
End Sub

Private Sub cmbNumberStyle_LostFocus()
    m_xPrevCtl = "cmbNumberStyle"
    RefreshLevel
End Sub

Private Sub cmbNumFontSize_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNumFontSize, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbNumFontSize_Mismatch"
    Exit Sub

End Sub

Private Sub cmbNumFontSize_Validate(Cancel As Boolean)
    If cmbNumFontSize.Text <> "-Non d�fini-" Then
        Cancel = Not bIsValidFontSize(cmbNumFontSize.Text)
    End If
End Sub

Private Sub cmbSpecial_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbSpecial, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbSpecial_Mismatch"
    Exit Sub
End Sub

Private Sub cmbTextAlign_ItemChange()
    Dim bEnabled As Boolean
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
    m_bPropDirty = True
    
'   enable/disable indent controls
    bEnabled = (Not (Me.cmbTextAlign.Text = "Centr�")) And Me.cmbTrailingChar.Bookmark = mpTrailingChar_Tab
    
    Me.lblTabPosition.Enabled = bEnabled
    Me.spnTabPosition.Enabled = bEnabled
    If Not bEnabled Then _
        Me.chkRightAlign = 0
        
    If (m_bStopChangeEvent = False) And _
            (Me.cmbTextAlign.Text = "Centr�") Then
        Me.spnTextPosition.Value = Me.spnRightIndent.Value
        Me.spnNumberPosition.Value = Me.spnRightIndent.Value
    End If
End Sub

Private Sub cmbTextAlign_LostFocus()
    m_xPrevCtl = "cmbTextAlign"
    RefreshLevel
End Sub

'Private Sub cmbTrailingChar_Click()
Private Sub cmbTrailingChar_ItemChange()
    Dim bEnabled As Boolean
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    m_bPropDirty = True
    
    With Me.cmbTrailingChar
        bEnabled = (.Bookmark = mpTrailingChar_Tab)
        Me.spnTabPosition.Enabled = bEnabled
        Me.lblTabPosition.Enabled = bEnabled
    
'        bEnabled = _
'            (.ListIndex <> mpTrailingChar_ShiftReturn) And _
'            (.ListIndex <> mpTrailingChar_DoubleShiftReturn) And _
'            (.ListIndex <> mpTrailingChar_None) And _
'            Me.chkNumberUnderline = vbChecked
'
'        Me.chkTrailingUnderline.Enabled = bEnabled
'        If Not Me.chkTrailingUnderline.Enabled Then
'            Me.chkTrailingUnderline = vbUnchecked
'        End If
    End With
End Sub

Private Sub cmbTrailingChar_LostFocus()
'   This will err when tring to add space to NumberFormat property;
'   11/8/01 - two spaces moved outside number, so this is no longer problematic
'    If (Me.cmbNumberStyle = "Bullet") And _
'            (Me.cmbTrailingChar.ListIndex = _
'            mpTrailingChar_DoubleSpace) Then
'        MsgBox "Two spaces are not available as the trailing characters " & _
'            "for bullets.  Please select one of the other options.", _
'            vbInformation, g_xAppName
'        With Me.cmbTrailingChar
'            .ListIndex = mpTrailingChar_Tab
'            .SetFocus
'        End With
'        Exit Sub
'    End If
    
    m_xPrevCtl = "cmbTrailingChar"
    RefreshLevel
End Sub

Private Sub cmbTrailingChar_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbTrailingChar, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbTrailingChar_Mismatch"
    Exit Sub
End Sub

'Private Sub cmbUnderlineStyle_Click()
Private Sub cmbUnderlineStyle_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bPropDirty = True
    m_bLevelDirty = True
End Sub

Private Sub cmbUnderlineStyle_LostFocus()
    m_xPrevCtl = "cmbUnderlineStyle"
    RefreshLevel
End Sub

Private Sub cmbUnderlineStyle_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbUnderlineStyle, Reposition
    Exit Sub
ProcError:
    RaiseError "frmEditScheme.cmbUnderlineStyle_Mismatch"
    Exit Sub
End Sub

Private Sub cmdAddLevel_Click()
    Dim xNumberFormat As String
    Dim xContStyle As String
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If

'   can't add tenth level
    If m_iCurLevels = 9 Then
        MsgBox "Le th�me actuel contient d�j� neuf niveaux.", _
            vbInformation, g_xAppName
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    EchoOff
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate

'   update current level
    If bLevelIsDirty() Then
'       implement changes
        bRet = bCreateLevel(g_oCurScheme.Name, Me.cmbLevel)
        
'       cont style
        If m_bNextParaDirty Then
            bRet = bCreateContLevel(g_oCurScheme.Name, Me.cmbLevel)
        End If
        
'       update dirty flag array
        bRet = bSetDirt(Me.cmbLevel, _
                        m_bLevelDirty, _
                        m_bTCDirty, _
                        m_bTOCDirty, _
                        m_bPropDirty, _
                        m_bParaDirty, _
                        m_bNextParaDirty)
                        
'       reset dirty flags
        ClearSchemeDirtFlags
    End If
    
'   add new level
    m_iCurLevels = m_iCurLevels + 1
    lRet = lAddLevel(g_oCurScheme.Name, m_iCurLevels)

'   update dirty flag array
    bRet = bSetDirt(m_iCurLevels, _
            True, True, True, True, True, True)

'   update level combobox
    Me.cmbLevel.AddItem Str(m_iCurLevels)
    
'   update next paragraph dropdown
    FillNextParaList m_iCurLevels
'    xContStyle = xGetStyleRoot(g_oCurScheme.Name) & _
'        " Cont " & m_iCurLevels
'    Me.cmbNextParagraph.AddItem xContStyle
    
'   recreate preview
    m_oPreview.RefreshPreviewLevel g_oCurScheme.Name, m_iCurLevels
        
'   enable/disable add/delete level buttons
    Me.cmdAddLevel.Enabled = Not Me.chkLevel(8).Visible
    Me.cmdDeleteLevel.Enabled = True
    
'   move buttons
    RedrawLevelButtons
        
'   cleanup
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Screen.MousePointer = vbDefault
    Me.chkLevel.Item(m_iCurLevels - 1).Value = vbChecked
    
'   send focus back to form - bug in Word 2000
    '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
    'screen capture and design mode execution
    If g_iWordVersion < mpWordVersion_2013 Then _
        mdlApplication.SendShiftKey
    
    Exit Sub
    
ProcError:
'   cleanup
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Screen.MousePointer = vbDefault
    RaiseError "frmEditSchemeFrench.cmdAddLevel_Click"
    Exit Sub
End Sub

Private Sub cmdCancel_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bCancelled = True
    Me.Hide
'    Application.ScreenUpdating = False
'    ResetListNumberFonts
'    Application.ScreenUpdating = True
'    Application.ScreenRefresh
    Word.Application.Activate
    '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
    'screen capture and design mode execution
    If g_iWordVersion < mpWordVersion_2013 Then _
        mdlApplication.SendShiftKey
End Sub

Private Sub cmdDeleteLevel_Click()
    Dim iLevel As Integer
    Dim i As Integer
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   disallow deletion of only level
    If m_iCurLevels = 1 Then
        MsgBox "Le th�me actuel contient un seul niveau.", _
            vbInformation, g_xAppName
        Exit Sub
    End If
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate

'   get level
    iLevel = Me.cmbLevel
    
'   prompt for confirmation
    lRet = MsgBox("Supprimer niveau " & iLevel & "?", _
        vbYesNo + vbQuestion, g_xAppName)
        
    If lRet = vbYes Then
        Screen.MousePointer = vbHourglass
        EchoOff
        bSetUserIni "Numbering", "Zoom", chkZoom.Value
'       delete level
        lRet = lDeleteLevel(g_oCurScheme.Name, m_iCurLevels, iLevel)
                
'       update dirty flag array
        If iLevel < m_iCurLevels Then
            For i = iLevel To m_iCurLevels - 1
                bRet = bSetDirt(i, True, True, _
                    True, True, True, True)
            Next i
        End If
        
'       mark deleted level as dirty - this will force
'       the "deleted" listlevel to be saved.
        bRet = bSetDirt(m_iCurLevels, True, 0, 0, True, 0, 0)
        
'       remove level from list
        m_iCurLevels = m_iCurLevels - 1
        Me.cmbLevel.RemoveItem m_iCurLevels

'       uncheck last level
        Me.chkLevel.Item(m_iCurLevels).Value = vbUnchecked
        
'       recreate preview
        ActiveDocument.Content.Delete
        m_oPreview.ShowPreview g_oCurScheme.Name, mpSchemeType_Document
        
'       remove next para style
        FillNextParaList m_iCurLevels
'        Me.cmbNextParagraph.RemoveItem Me.cmbNextParagraph.ListCount - 1
        
'       select appropriate level chkbox
        If iLevel > m_iCurLevels Then
'           select last level
            Me.chkLevel.Item(m_iCurLevels - 1).Value = vbChecked
        Else
            m_iLastSelLevel = m_iLastSelLevel + 1
'           refresh dlg for selected level
            SwitchLevels
        End If
        
        m_oPreview.HighlightLevel CInt(Me.cmbLevel)
        
'       enable/disable add/delete level buttons
        Me.cmdDeleteLevel.Enabled = Me.chkLevel(1).Visible
        Me.cmdAddLevel.Enabled = True
        
'       redraw buttons
        RedrawLevelButtons
        
        EchoOn
        Application.ScreenUpdating = True
        Application.ScreenRefresh
        Screen.MousePointer = vbDefault
        
'       send focus back to form - bug in Word 2000
        '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
        'screen capture and design mode execution
        If g_iWordVersion < mpWordVersion_2013 Then _
            mdlApplication.SendShiftKey
    End If
    Exit Sub
    
ProcError:
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    
'   send focus back to form - bug in Word 2000
    '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
    'screen capture and design mode execution
    If g_iWordVersion < mpWordVersion_2013 Then _
        mdlApplication.SendShiftKey

    Screen.MousePointer = vbDefault
    RaiseError "frmEditSchemeFrench.cmdDeleteLevel_Click"
    Exit Sub
End Sub

Private Sub cmdSynchronize_Click()
    Dim xRoot As String
    Dim xCont As String
    Dim xStyle As String
    Dim iLevel As Integer
        
'   ensure that list level tab position reflects
'   actual indents - user may have edited style
'   indents in Word
    m_bLevelDirty = True
    RefreshLevel
    
'   update style
    CreateContStyles g_oCurScheme.Name, Me.cmbLevel, True
    
'   update dialog
    m_bLevelChanged = True
    lLoadContLevel
    m_bLevelChanged = False

'   update dirty flags
    xarDirty(Me.cmbLevel, mpNextParaIsDirty) = "True"
    m_bNextParaDirty = False
    
'   notify user
    iLevel = Val(Me.cmbLevel)
    xStyle = xGetStyleName(g_oCurScheme.Name, iLevel)
    xRoot = xGetStyleRoot(g_oCurScheme.Name)
    xCont = xRoot & " Cont " & iLevel
    Application.StatusBar = xCont & " style a �t� synchronis� avec style " & _
        xStyle & "."
End Sub

Private Sub cmdSynchronize_LostFocus()
    m_xPrevCtl = "cmdSynchronize"
End Sub

Private Sub cmdSave_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    RefreshLevel
    m_bCancelled = False
    Me.Hide
'    Application.ScreenUpdating = False
'    ResetListNumberFonts
'    Application.ScreenUpdating = True
'    Application.ScreenRefresh
    Word.Application.Activate
    '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
    'screen capture and design mode execution
    If g_iWordVersion < mpWordVersion_2013 Then _
        mdlApplication.SendShiftKey
End Sub

Private Sub cmdUpdate_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    
    With Me.Controls(m_xPrevCtl)
'        don't need this since LostFocus event will do it
'        RefreshLevel
        On Error Resume Next
        If m_xPrevCtl = "optDirectFormats" Then
            If Not Me.optDirectFormats Then
                Me.optStyleFormats.SetFocus
            End If
        ElseIf m_xPrevCtl = "optStyleFormats" Then
            If Not Me.optStyleFormats Then
                Me.optDirectFormats.SetFocus
            End If
        Else
            .SetFocus
        End If
        If .Name <> "rtfNumberFormat" Then
            .SelLength = Len(.Text)
        End If
    End With
End Sub


Private Sub Form_Activate()
    Dim iZoom As Integer
    
    DoEvents
    
'   this will trigger loading of all properties
    Me.cmbLevel.ListIndex = 0

'   initialize m_xarIsDirty Array
    m_xarIsDirty.ReDim 1, 9, 1, 6

'   if specified, hide scheme edit in Word 97 and fill gap
    If g_bNoSchemeIndents Then
        Me.spnNumberPosition.Visible = False
        Me.spnTextPosition.Visible = False
        Me.lblNumberPosition.Visible = False
        Me.lblTextPosition.Visible = False
        Me.lblSpaceBefore.Top = Me.lblNumberPosition.Top
        Me.lblSpaceBefore.Left = Me.lblNumberPosition.Left
        Me.lblSpaceBefore.TabIndex = Me.lblNumberPosition.TabIndex
        Me.spnSpaceBefore.Top = Me.spnNumberPosition.Top
        Me.spnSpaceBefore.Left = Me.spnNumberPosition.Left
        Me.spnSpaceBefore.Width = Me.spnNumberPosition.Width
        Me.spnSpaceBefore.TabIndex = Me.spnNumberPosition.TabIndex
        Me.lblSpaceAfter.Top = Me.lblTextPosition.Top
        Me.lblSpaceAfter.Left = Me.lblTextPosition.Left
        Me.lblSpaceAfter.TabIndex = Me.lblTextPosition.TabIndex
        Me.spnSpaceAfter.Top = Me.spnTextPosition.Top
        Me.spnSpaceAfter.Left = Me.spnTextPosition.Left
        Me.spnSpaceAfter.Width = Me.spnTextPosition.Width
        Me.spnSpaceAfter.TabIndex = Me.spnTextPosition.TabIndex
    End If

'   update flags
    m_bFormInit = False
    m_bLevelDirty = False
    m_bTCDirty = False
    m_bTOCDirty = False
    m_bPropDirty = False
    m_bParaDirty = False
    m_bNextParaDirty = False
    g_bSchemeIsDirty = False

'   initialize focus marker
    m_xPrevCtl = "rtfNumberFormat"

    Screen.MousePointer = vbDefault

'   enable/disable add/delete level buttons
    Me.cmdDeleteLevel.Enabled = Me.chkLevel(1).Visible
    Me.cmdAddLevel.Enabled = Not Me.chkLevel(8).Visible
    
    RedrawLevelButtons
'    EchoOn

    Application.ScreenUpdating = True
    Application.ScreenRefresh
    
    '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
    'screen capture and design mode execution
    If g_iWordVersion < mpWordVersion_2013 Then _
        mdlApplication.SendShiftKey
    Exit Sub
ProcError:
    If Not g_oStatus Is Nothing Then
        g_oStatus.Hide
        Set g_oStatus = Nothing
    End If

    RaiseError "frmEditSchemeFrench.Form_Activate"
    Exit Sub
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF5
            RefreshLevel
    End Select
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim j As Integer
    Dim xStyleRoot As String
    Dim sDlgTop As Single
    Dim sDlgLeft As Single
    Dim iZoom As Integer
    Dim bCreateBitmap As Boolean
    Dim xLT As String
    Dim aFont As Variant
    Dim iNumFonts As Integer
    Dim xFonts() As String
    Dim bRestrictFonts As Boolean
    Dim lUnit As Long
    Dim sIncrement As Single
    Dim xarUnderlineStyles As xArray
    Dim xarFontName As xArray
    Dim xarSize As xArray
    Dim xarLineSpacing As xArray
    Dim xarSpecial As xArray
    Dim xarTrailingChars As xArray
    Dim xarAlignment As xArray
    Dim iNumStyleCount As Integer
    Dim xNumStyle As String
    Dim bActiveDocIs2010Compatible As Boolean 'GLOG 5622

    ReDim g_sNumberFontSizes(8)

    m_bFormInit = True
    m_bCancelled = True
    m_bAllowRefresh = True
    m_iSelStart = -1
    
'   set top/left to ini values
    'remmed out 1/31/14 (see note below)
'    On Error Resume Next
'    sDlgTop = xGetUserIni("Numbering", "DlgTop")
'    sDlgLeft = xGetUserIni("Numbering", "DlgLeft")
'    On Error GoTo ProcError
    
    'GLOG 5266 (9.9.5004) - this is problematic for users moving between different
    'environments/monitors - Word locks up if positions are off the screen - trap errors
    '1/31/14 (9.9.5005) - removed this code altogether, as there were settings that
    'positioned the dialog without the screen without causing an error - dialog is now
    'configured for center owner
'    If sDlgTop Then
'        On Error Resume Next
'        Me.Top = sDlgTop
'        If Err > 0 Then _
'            sDlgTop = 0
'        On Error GoTo ProcError
'    End If
'
'    If sDlgTop = 0 Then
''       vertically center top
'        Me.Top = Screen.Height / 2 - Me.Height / 2
'    End If
'
'    If sDlgLeft Then
'        On Error Resume Next
'        Me.Left = sDlgLeft
'        If Err > 0 Then _
'            sDlgLeft = 0
'        On Error GoTo ProcError
'    End If
'
'    If sDlgLeft = 0 Then
''       horizontally center width
'        Me.Left = Screen.Width / 2 - Me.Width / 2
'    End If
    
    Set m_oPreview = New CPreview
    Set m_xarIsDirty = New xArray
    Set m_xarPrevLevels = New xArray
    Set xarFontName = New xArray
    Set m_xarNumFontName = New xArray
    Set xarSize = New xArray
    Set xarLineSpacing = New xArray
    
    If Not g_oStatus Is Nothing Then
        g_oStatus.Show , "Cr�ation de l'aper�u.  Veuillez patienter�"
    End If
    
    Screen.MousePointer = vbHourglass
    
    'initialize bullet variables to default
    m_xBulletFont = "Symbol"
    m_xBulletCharNum = ChrW(&HB7)
    
'   get number of levels
    m_iInitLevels = iGetLevels(g_oCurScheme.Name, _
                               mpSchemeType_Document)
    m_iCurLevels = m_iInitLevels
    
'   disable chkboxes for non-existent levels
    For i = m_iCurLevels To 8
        With Me.chkLevel.Item(i)
            .Visible = False
            .Caption = ""
        End With
    Next i
    
'   set caption
    Me.Caption = " Modifier th�me " & g_oCurScheme.Alias & " - Num�rotation"
    
'   fill Special array
    Set xarSpecial = New xArray
    xarSpecial.ReDim 0, 2, 0, 1
    xarSpecial(0, 0) = "(aucun)"
    xarSpecial(0, 1) = "(aucun)"
    xarSpecial(1, 0) = "1�re ligne"
    xarSpecial(1, 1) = "1�re ligne"
    xarSpecial(2, 0) = "Retrait"
    xarSpecial(2, 1) = "Retrait"
    
    Me.cmbSpecial.Array = xarSpecial
    ResizeTDBCombo Me.cmbSpecial, 3
   
'   fill alignment array
    Set xarAlignment = New xArray
    xarAlignment.ReDim 0, 4, 0, 1
    xarAlignment(0, 0) = "Gauche"
    xarAlignment(0, 1) = "0"
    xarAlignment(1, 0) = "Centr�"
    xarAlignment(1, 1) = "1"
    xarAlignment(2, 0) = "Droit"
    xarAlignment(2, 1) = "2"
    xarAlignment(3, 0) = "Justifi�"
    xarAlignment(3, 1) = "3"
    xarAlignment(4, 0) = "Ajuster au mod�le Normal"
    xarAlignment(4, 1) = "4"
    
    Me.cmbAlignment.Array = xarAlignment
    ResizeTDBCombo Me.cmbAlignment, 5
    Me.cmbTextAlign.Array = xarAlignment
    ResizeTDBCombo Me.cmbTextAlign, 5
    
'   fill line spacing array
    Set m_xarLineSpacing = New xArray
    m_xarLineSpacing.ReDim 0, 5, 0, 1
    m_xarLineSpacing(0, 0) = "Simple"
    m_xarLineSpacing(0, 1) = "Simple"
    m_xarLineSpacing(1, 0) = "1,5 ligne"
    m_xarLineSpacing(1, 1) = "1,5 ligne"
    m_xarLineSpacing(2, 0) = "Double"
    m_xarLineSpacing(2, 1) = "Double"
    m_xarLineSpacing(3, 0) = "Au moins"
    m_xarLineSpacing(3, 1) = "Au moins"
    m_xarLineSpacing(4, 0) = "Exactement"
    m_xarLineSpacing(4, 1) = "Exactement"
    m_xarLineSpacing(5, 0) = "Multiple"
    m_xarLineSpacing(5, 1) = "Multiple"
    
    Me.cmbLineSpacing.Array = m_xarLineSpacing
    ResizeTDBCombo Me.cmbLineSpacing, 6
    Me.cmbContLineSpacing.Array = m_xarLineSpacing
    ResizeTDBCombo Me.cmbContLineSpacing, 6

'   fill trailing chars array
    Set xarTrailingChars = New xArray
    xarTrailingChars.ReDim 0, 5, 0, 1
    xarTrailingChars(0, 0) = "(Rien)"
    xarTrailingChars(0, 1) = "0"
    xarTrailingChars(1, 0) = "Tabulation"
    xarTrailingChars(1, 1) = "1"
    xarTrailingChars(2, 0) = "Espace"
    xarTrailingChars(2, 1) = "2"
    xarTrailingChars(3, 0) = "Deux espaces"
    xarTrailingChars(3, 1) = "3"
    xarTrailingChars(4, 0) = "Saut de ligne"
    xarTrailingChars(4, 1) = "4"
    xarTrailingChars(5, 0) = "Deux sauts de ligne"
    xarTrailingChars(5, 1) = "5"

'   fill next paragraph list
    FillNextParaList m_iInitLevels
    
'   underline dropdown
    With Me.cmbUnderlineStyle
        .ClearFields
        
        Set xarUnderlineStyles = New xArray
        xarUnderlineStyles.ReDim 0, UBound(g_xUnderlineFormats), 0, 1
        For i = 0 To UBound(g_xUnderlineFormats)
            xarUnderlineStyles(i, 0) = g_xUnderlineFormats(i, 0)
            xarUnderlineStyles(i, 1) = CLng(g_xUnderlineFormats(i, 1))
        Next i
        .Array = xarUnderlineStyles
        ResizeTDBCombo Me.cmbUnderlineStyle, 4
        
'        For i = 0 To UBound(g_xUnderlineFormats)
'            .AddItem g_xUnderlineFormats(i, 0)
'            .ItemData(.NewIndex) = CLng(g_xUnderlineFormats(i, 1))
'        Next i
'        .Refresh
    End With
    
'   fill trailing char dropdown
    Me.cmbTrailingChar.Array = xarTrailingChars
    ResizeTDBCombo Me.cmbTrailingChar, 6
        
'   store list template font sizes - these will have to be forced
'   for preview
    xLT = xGetFullLTName(g_oCurScheme.Name)
    With ActiveDocument.ListTemplates(xLT)
        For i = 1 To m_iCurLevels
            g_sNumberFontSizes(i - 1) = .ListLevels(i).Font.Size
        Next i
        For i = m_iCurLevels + 1 To 9
            g_sNumberFontSizes(i - 1) = wdUndefined
        Next i
    End With
    
'   get fonts
    On Error Resume Next
    bRestrictFonts = xGetAppIni("Numbering", "RestrictFontList")
    On Error GoTo ProcError
    
    If bRestrictFonts Then
'       load list from ini
        If g_xPermittedFonts(0) <> "" Then
            
            'fill cmbNumFontName
            m_xarNumFontName.ReDim 0, UBound(g_xPermittedFonts) + 2, 0, 1
            xarFontName.ReDim 0, UBound(g_xPermittedFonts), 0, 1
            
            m_xarNumFontName(0, 0) = "-Non d�fini-"
            m_xarNumFontName(0, 1) = "-Non d�fini-"
            
            For i = LBound(g_xPermittedFonts) To UBound(g_xPermittedFonts)
                m_xarNumFontName(i + 1, 0) = g_xPermittedFonts(i)
                m_xarNumFontName(i + 1, 1) = g_xPermittedFonts(i)
                xarFontName(i, 0) = g_xPermittedFonts(i)
                xarFontName(i, 1) = g_xPermittedFonts(i)
            Next i
            
            m_xarNumFontName(i + 1, 0) = "Symbol"
            m_xarNumFontName(i + 1, 1) = "Symbol"
            
            Me.cmbNumFontName.Array = m_xarNumFontName
            ResizeTDBCombo Me.cmbNumFontName, i
            Me.cmbFontName.Array = xarFontName
            ResizeTDBCombo Me.cmbFontName, i
            Me.cmbContFontName.Array = xarFontName
            ResizeTDBCombo Me.cmbContFontName, i
            
        End If
    Else
        'reset counter
        i = 0
        
'       load system fonts
        'GLOG 4839 - omit vertical fonts (names start with '@')
        For Each aFont In Word.FontNames
            If Left$(aFont, 1) <> "@" Then
                iNumFonts = iNumFonts + 1
            End If
        Next aFont
        ReDim xFonts(iNumFonts - 1)
    
'       stick in variant array
        For Each aFont In Word.FontNames
            If Left$(aFont, 1) <> "@" Then
                xFonts(i) = aFont
                i = i + 1
            End If
        Next aFont
    
'       sort array
        WordBasic.SortArray xFonts
    
'       add array items to lists
        m_xarNumFontName.ReDim 0, UBound(xFonts) + 1, 0, 1
        m_xarNumFontName(0, 0) = "-Non d�fini-"
        m_xarNumFontName(0, 1) = "-Non d�fini-"
        
        xarFontName.ReDim 0, UBound(xFonts), 0, 1
        
        For i = 0 To iNumFonts - 1
            'GLOG 4839 - omit vertical fonts (names start with '@')
            If Left$(xFonts(i), 1) <> "@" Then
                m_xarNumFontName(i + 1, 0) = xFonts(i)
                m_xarNumFontName(i + 1, 1) = xFonts(i)
                xarFontName(i, 0) = xFonts(i)
                xarFontName(i, 1) = xFonts(i)
            End If
        Next i
        Me.cmbNumFontName.Array = m_xarNumFontName
        ResizeTDBCombo Me.cmbNumFontName, 22
        Me.cmbFontName.Array = xarFontName
        ResizeTDBCombo Me.cmbFontName, 22
        Me.cmbContFontName.Array = xarFontName
        ResizeTDBCombo Me.cmbContFontName, 22

    End If
    
'   get font sizes
    xarSize.ReDim 0, 20, 0, 1
    xarSize(0, 0) = "-Non d�fini-"
    xarSize(0, 1) = "-Non d�fini-"
    
    For i = 7 To 26
        xarSize(i - 6, 0) = CStr(i)
        xarSize(i - 6, 1) = CStr(i)
    Next i
    
    Me.cmbNumFontSize.Array = xarSize
    ResizeTDBCombo Me.cmbNumFontSize, 20
    Me.cmbContFontSize.Array = xarSize
    ResizeTDBCombo Me.cmbContFontSize, 20
    Me.cmbFontSize.Array = xarSize
    ResizeTDBCombo Me.cmbFontSize, 20
    
'   set spinners to display Word unit of measurement
    lUnit = Word.Options.MeasurementUnit
    sIncrement = GetStandardIncrement(lUnit)
    With Me.spnNumberPosition
        .DisplayUnit = lUnit
        .IncrementValue = sIncrement
    End With
    With Me.spnRightIndent
        .DisplayUnit = lUnit
        .IncrementValue = sIncrement
    End With
    With Me.spnTabPosition
        .DisplayUnit = lUnit
        .IncrementValue = sIncrement
    End With
    With Me.spnTextPosition
        .DisplayUnit = lUnit
        .IncrementValue = sIncrement
    End With
    With Me.spnContLeftIndent
        .DisplayUnit = lUnit
        .IncrementValue = sIncrement
    End With
    With Me.spnContRightIndent
        .DisplayUnit = lUnit
        .IncrementValue = sIncrement
    End With
    With Me.spnBy
        .DisplayUnit = lUnit
        .IncrementValue = sIncrement
    End With
    
'   set base count
    iNumStyleCount = 12

    'new fixed-digit numbering formats are only available in documents with
    'Word 2010 compatibility mode
    If g_iWordVersion >= mpWordVersion_2010 Then
        Dim bAllow As Boolean
        If g_oCurScheme.SchemeType = mpSchemeType_Document Then
            'if editing a document scheme, only display if source doc also allows
            bAllow = (mdlWord14.GetCompatibilityMode(Documents(g_xCurDest)) >= 14)
        Else
            bAllow = True
        End If
        'GLOG 5622 - put in variable here, since can't call in Word 2007
        bActiveDocIs2010Compatible = (mdlWord14.GetCompatibilityMode(ActiveDocument) >= 14)
        If bAllow And bActiveDocIs2010Compatible Then
'           reset count
            iNumStyleCount = 15
        Else
            m_iFixedDigitNumberingOffset = 3
        End If
    Else
        m_iFixedDigitNumberingOffset = 3
    End If
    
    'account for supplemental styles
    If g_xSupplementalNumberStyles(0) <> "" Then
        For i = 0 To UBound(g_xSupplementalNumberStyles)
'           increment count
            iNumStyleCount = iNumStyleCount + 1
        Next i
    End If
    
    Set m_xarNumStyle = New xArray
    m_xarNumStyle.ReDim 0, iNumStyleCount - 1, 0, 1

    m_xarNumStyle(0, 0) = "(pas de num�ro)"
    m_xarNumStyle(0, 1) = "(pas de num�ro)"
    m_xarNumStyle(1, 0) = "1, 2, 3, ..."
    m_xarNumStyle(1, 1) = "1, 2, 3, ..."
    m_xarNumStyle(2, 0) = "I, II, III, ..."
    m_xarNumStyle(2, 1) = "I, II, III, ..."
    m_xarNumStyle(3, 0) = "i, ii, iii, ..."
    m_xarNumStyle(3, 1) = "i, ii, iii, ..."
    m_xarNumStyle(4, 0) = "A, B, C, ..."
    m_xarNumStyle(4, 1) = "A, B, C, ..."
    m_xarNumStyle(5, 0) = "a, b, c, ..."
    m_xarNumStyle(5, 1) = "a, b, c, ..."
    m_xarNumStyle(6, 0) = "1er, 2e, ..."
    m_xarNumStyle(6, 1) = "1er, 2e, ..."
    m_xarNumStyle(7, 0) = "Un, Deux, ..."
    m_xarNumStyle(7, 1) = "Un, Deux, ..."
    m_xarNumStyle(8, 0) = "Premier, Deuxi�me, ..."
    m_xarNumStyle(8, 1) = "Premier, Deuxi�me, ..."
    m_xarNumStyle(9, 0) = "01, 02, ..."
    m_xarNumStyle(9, 1) = "01, 02, ..."
    'GLOG 5618 - modified conditional - was crashing in Word 2007
    'GLOG 5622 - modified again - was problematic on New Scheme
    If bAllow And bActiveDocIs2010Compatible Then
        m_xarNumStyle(10, 0) = "001, 002, ..."
        m_xarNumStyle(10, 1) = "001, 002, ..."
        m_xarNumStyle(11, 0) = "0001, 0002, ..."
        m_xarNumStyle(11, 1) = "0001, 0002, ..."
        m_xarNumStyle(12, 0) = "00001, 00002, ..."
        m_xarNumStyle(12, 1) = "00001, 00002, ..."
        m_xarNumStyle(13, 0) = "Puce"
        m_xarNumStyle(13, 1) = "Puce"
        m_xarNumStyle(14, 0) = "Puce nouveau..."
        m_xarNumStyle(14, 1) = "Puce nouveau..."
        i = 15
    Else
        m_xarNumStyle(10, 0) = "Puce"
        m_xarNumStyle(10, 1) = "Puce"
        m_xarNumStyle(11, 0) = "Puce nouveau..."
        m_xarNumStyle(11, 1) = "Puce nouveau..."
        i = 12
    End If
    
    'add supplemental styles to Number Style list
    If g_xSupplementalNumberStyles(0) <> "" Then
        For j = 0 To UBound(g_xSupplementalNumberStyles)
            xNumStyle = GetNumStyleDisplayName(CInt(g_xSupplementalNumberStyles(j)))
            m_xarNumStyle(i + j, 0) = xNumStyle
            m_xarNumStyle(i + j, 1) = xNumStyle
        Next j
    End If

    Me.cmbNumberStyle.Array = m_xarNumStyle
    ResizeTDBCombo Me.cmbNumberStyle, iNumStyleCount

'   fill level list
    m_iLastSelLevel = 0
    With Me.cmbLevel
        For i = 1 To m_iInitLevels
            .AddItem Str(i)
        Next i
'       moved to Form Activate event
''       this will trigger loading of all properties
'        .ListIndex = 0
    End With
'9.9.6009 - moved to Form Activate event
'
''   initialize m_xarIsDirty Array
'    m_xarIsDirty.ReDim 1, 9, 1, 6
'
''   if specified, hide scheme edit in Word 97 and fill gap
'    If g_bNoSchemeIndents Then
'        Me.spnNumberPosition.Visible = False
'        Me.spnTextPosition.Visible = False
'        Me.lblNumberPosition.Visible = False
'        Me.lblTextPosition.Visible = False
'        Me.lblSpaceBefore.Top = Me.lblNumberPosition.Top
'        Me.lblSpaceBefore.Left = Me.lblNumberPosition.Left
'        Me.lblSpaceBefore.TabIndex = Me.lblNumberPosition.TabIndex
'        Me.spnSpaceBefore.Top = Me.spnNumberPosition.Top
'        Me.spnSpaceBefore.Left = Me.spnNumberPosition.Left
'        Me.spnSpaceBefore.Width = Me.spnNumberPosition.Width
'        Me.spnSpaceBefore.TabIndex = Me.spnNumberPosition.TabIndex
'        Me.lblSpaceAfter.Top = Me.lblTextPosition.Top
'        Me.lblSpaceAfter.Left = Me.lblTextPosition.Left
'        Me.lblSpaceAfter.TabIndex = Me.lblTextPosition.TabIndex
'        Me.spnSpaceAfter.Top = Me.spnTextPosition.Top
'        Me.spnSpaceAfter.Left = Me.spnTextPosition.Left
'        Me.spnSpaceAfter.Width = Me.spnTextPosition.Width
'        Me.spnSpaceAfter.TabIndex = Me.spnTextPosition.TabIndex
'    End If
'

'9.9.6010 - restored to Load to get ruler to display in Word 2013
    CreatePreview

    If Not (g_oStatus Is Nothing) Then
        g_oStatus.Hide
        Set g_oStatus = Nothing
    End If

'   get current zoom status
    On Error Resume Next
    iZoom = xGetUserIni("Numbering", "Zoom")
    Me.chkZoom = iZoom
    On Error GoTo 0

    '9.9.6006 (8/28/15) - moved the zoom code from Activate to
    'avoid having to hide and reshow the dialog to get the ruler
    'to display in Word 2013
    '9.9.6010 - extended to earlier Word versions
'    If g_iWordVersion >= mpWordVersion_2013 Then
        SetPreviewZoom (Me.chkZoom.Value = vbChecked), m_lRulerDisplayDelay
'    End If
    
'
''   update flags
'    m_bFormInit = False
'    m_bLevelDirty = False
'    m_bTCDirty = False
'    m_bTOCDirty = False
'    m_bPropDirty = False
'    m_bParaDirty = False
'    m_bNextParaDirty = False
'    g_bSchemeIsDirty = False
'
''   initialize focus marker
'    m_xPrevCtl = "rtfNumberFormat"
'
'    Screen.MousePointer = vbDefault
    Exit Sub
ProcError:
    If Not g_oStatus Is Nothing Then
        g_oStatus.Hide
        Set g_oStatus = Nothing
    End If

    RaiseError "frmEditSchemeFrench.Form_Load"
    Exit Sub
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Set m_oPreview = Nothing
    Set m_xarPrevLevels = Nothing
    Set m_xarIsDirty = Nothing
    
'   save dlg position
    bSetUserIni "Numbering", "DlgTop", Me.Top
    bSetUserIni "Numbering", "DlgLeft", Me.Left

'   delete the refresh buffer file -
'   the file will only exist if the
'   number of refreshes that have
'   occurred exceeds the threshold
    On Error Resume Next
    Kill g_xUserPath & "\Refresh.mpf"
    On Error GoTo 0
    Set frmEditSchemeFrench = Nothing
End Sub

Private Sub lblStartAt_Click()
    m_xPrevCtl = "lblStartAt"
End Sub

Private Sub lstPrevLevels_LostFocus()
    m_xPrevCtl = "lstPrevLevels"
End Sub

Private Sub optDirectFormats_LostFocus()
    m_xPrevCtl = "optDirectFormats"
    RefreshLevel
End Sub

Private Sub optStyleFormats_LostFocus()
    m_xPrevCtl = "optStyleFormats"
    RefreshLevel
End Sub

Private Sub rtfNumberFormat_KeyPress(KeyAscii As Integer)
'added in 9.9.4014
    Dim xChar As String
    Dim iSelLength As Integer
    Dim xFormat As String
    Dim iLen As Integer
    
    xChar = LCase(Chr(KeyAscii))
    If KeyAscii = 8 Then
'       backspace key pressed
        Exit Sub
    End If
    
    iSelLength = Me.rtfNumberFormat.SelLength
    xFormat = xGetNumFormat(m_xCode, _
        Me.rtfNumberFormat.Text, Me.chkNonbreakingSpaces)
    iLen = Len(xFormat) - lCountChrs(xFormat, "%")
    
    If (iLen = 31) And (iSelLength = 0) Then
        xMsg = "Vous avez atteint le nombre maximum de caract�res que Word permet pour un format de num�rotation."
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    End If
End Sub

Private Sub spnAt_Change()
    If m_bStopChangeEvent Then
        Exit Sub
    End If
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
End Sub

Private Sub spnAt_LostFocus()
    m_xPrevCtl = "spnAt"
    RefreshLevel
End Sub

Private Sub spnAt_Validate(Cancel As Boolean)
    If Not Me.spnAt.IsValid Then _
        Cancel = True
End Sub

Private Sub spnBy_Validate(Cancel As Boolean)
    If Not Me.spnBy.IsValid Then _
        Cancel = True
End Sub

Private Sub spnContAt_Validate(Cancel As Boolean)
    If Not Me.spnContAt.IsValid Then _
        Cancel = True
End Sub

Private Sub spnContSpaceBefore_Validate(Cancel As Boolean)
    If Not Me.spnContSpaceBefore.IsValid Then _
        Cancel = True
End Sub

Private Sub spnContSpaceAfter_Validate(Cancel As Boolean)
    If Not Me.spnContSpaceAfter.IsValid Then _
        Cancel = True
End Sub

Private Sub spnNumberPosition_LostFocus()
    m_xPrevCtl = "spnNumberPosition"
    RefreshLevel
End Sub

Private Sub spnTabPosition_LostFocus()
    m_xPrevCtl = "spnTabPosition"
    RefreshLevel
End Sub

Private Sub spnTextPosition_LostFocus()
    m_xPrevCtl = "spnTextPosition"
    RefreshLevel
End Sub

Private Sub spnRightIndent_LostFocus()
    m_xPrevCtl = "spnRightIndent"
    RefreshLevel
End Sub

Private Sub spnSpaceBefore_Validate(Cancel As Boolean)
    If Not Me.spnSpaceBefore.IsValid Then _
        Cancel = True
End Sub

Private Sub spnSpaceAfter_Validate(Cancel As Boolean)
    If Not Me.spnSpaceAfter.IsValid Then _
        Cancel = True
End Sub

Private Sub spnNumberPosition_Validate(Cancel As Boolean)
    If Not Me.spnNumberPosition.IsValid Then _
        Cancel = True
End Sub

Private Sub spnTabPosition_Validate(Cancel As Boolean)
    If Not Me.spnTabPosition.IsValid Then _
        Cancel = True
End Sub

Private Sub spnTextPosition_Validate(Cancel As Boolean)
    If Not Me.spnTextPosition.IsValid Then _
        Cancel = True
End Sub

Private Sub spnRightIndent_Validate(Cancel As Boolean)
    If Not Me.spnRightIndent.IsValid Then _
        Cancel = True
End Sub

Private Sub txtStartAt_GotFocus()
    bEnsureSelectedContent Me.txtStartAt, , _
                           m_bForceSelContent
End Sub

Private Sub txtStartAt_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iOffset As Integer
    Dim bIsNavigationKey As Boolean
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    
    If (KeyCode = vbKeyUp) Or (KeyCode = vbKeyDown) Then
        ChangeUpDown Me.udStartAt, KeyCode = vbKeyUp
        ChangeStartAt
    Else
        bIsNavigationKey = (KeyCode = vbKeyLeft) Or _
                           (KeyCode = vbKeyRight) Or _
                           (KeyCode = vbKeyHome) Or _
                           (KeyCode = vbKeyEnd) Or _
                           (KeyCode = vbKeyInsert) Or _
                           (KeyCode = vbKeyUp) Or _
                           (KeyCode = vbKeyDown) Or _
                           (KeyCode = vbKeyPageUp) Or _
                           (KeyCode = vbKeyPageDown) Or _
                           (KeyCode = vbKeyControl) Or _
                           (KeyCode = vbKeyMenu) Or _
                           (KeyCode = vbKeyShift)
                           
        If Not bIsNavigationKey Then
            'GLOG 5284 (9.9.5004) - modified keys in message below
            xMsg = "Veuillez utiliser la fl�che vers la droite pour changer la position 'd�buter �'.  " & vbCr & _
                   "Vous pouvez �galement utiliser les fl�ches Haut/Bas. " & vbCr & vbCr & _
                   "Appuyez CTRL+Haut/Bas pour changer la valeur par 10.  " & vbCr & _
                   "Appuyez CTRL+ALT+MAJ+Haut/Bas pour changer la valeur par 100."
            MsgBox xMsg, vbInformation, g_xAppName
        End If
    End If
End Sub

Private Sub txtStartAt_LostFocus()
    m_xPrevCtl = "txtStartAt"
    If Not Screen.ActiveControl Is Me.udStartAt Then
        RefreshLevel
    End If
End Sub

Private Sub lstPrevLevels_FetchCellStyle(ByVal Condition As Integer, ByVal Split As Integer, Bookmark As Variant, ByVal Col As Integer, ByVal CellStyle As TrueDBList60.StyleDisp)
    If Me.lstPrevLevels.Array.Value(Bookmark, 1) = ChrW(&HB7) Then
        CellStyle.Font.Name = "Symbol"
    End If
End Sub

Private Sub lstPrevLevels_KeyDown(KeyCode As Integer, Shift As Integer)
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    If KeyCode = 13 Then
        InsertPrevLevel
    End If
End Sub

Private Sub lstPrevLevels_MouseDown(Button As Integer, _
                Shift As Integer, X As Single, Y As Single)
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    If Not (Me.ActiveControl Is Me.lstPrevLevels) Then
        Me.lstPrevLevels.SetFocus
    End If
End Sub

Private Sub optDirectFormats_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bPropDirty = True
    m_bTCDirty = True
    m_bLevelDirty = True
    m_bParaDirty = True
End Sub

Private Sub optStyleFormats_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bPropDirty = True
    m_bTCDirty = True
    m_bLevelDirty = True
    m_bParaDirty = True
End Sub

Private Sub rtfNumberFormat_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iSelStart As Integer
    Dim iSelLen As Integer
    Dim xSelCode As String
    Dim iLevel As Integer
    Dim bNumIsInSel As Boolean
    Dim bCharIsNum As Boolean
    Dim xPreSel As String
    Dim xPostSel As String
    Dim iKey As Integer
    Dim bIsAllowed As Boolean
    Dim bIsNavigationKey As Boolean
    Dim xNew As String
    Dim xChar As String
    Dim iPrevLevel As Integer
    Dim i As Integer

'   get selection detail
    With Me.rtfNumberFormat
        iSelStart = .SelStart
        iSelLen = .SelLength
        xSelCode = Mid(m_xCode, iSelStart + 1, iSelLen)
    End With
    
    If Shift = vbCtrlMask And (KeyCode = 86 Or KeyCode = 88) And _
            iSelLen Then
        'user is pasting over or cutting a selection -
        'delete the corresponding chars in m_xCode
        xPreSel = Left(m_xCode, iSelStart)
        xPostSel = Mid(m_xCode, iSelStart + iSelLen + 1)
        m_xCode = xPreSel & xPostSel
    End If
    
'   exit if ctl or alt is down
    If Shift = vbCtrlMask Or Shift = vbAltMask Then
        Exit Sub
    End If

    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate

'   define valid characters -
'   shift = 16
'   GLOG 3505 (9.9.5001) - allowed smart quotes and apostrophes (145-148)
    iKey = UCase(KeyCode)
    bIsAllowed = (iKey >= 32 And iKey <= 96) Or _
                 (iKey >= 123 And iKey <= 126) Or _
                 (iKey = vbKeyDelete) Or _
                 (iKey = vbKeyBack) Or (iKey = 190) Or _
                 (iKey >= 145 And iKey <= 148)

    bIsNavigationKey = (iKey = vbKeyLeft) Or _
                       (iKey = vbKeyRight) Or _
                       (iKey = vbKeyHome) Or _
                       (iKey = vbKeyEnd) Or _
                       (iKey = vbKeyInsert) Or _
                       (iKey = vbKeyUp) Or _
                       (iKey = vbKeyDown) Or _
                       (iKey = vbKeyPageUp) Or _
                       (iKey = vbKeyPageDown)
                       
'   allow non-input keys to go through unmodified
    If Not bIsAllowed Then
        Exit Sub
    ElseIf bIsNavigationKey Then
        Exit Sub
    End If

    iLevel = Me.cmbLevel

    With Me.rtfNumberFormat
'       different actions for selection/non-selection
        If iSelLen Then
'           there is a selection-
'           delete the corresponding chars
'           in m_xCode and insert new character
            xPreSel = Left(m_xCode, iSelStart)
            xPostSel = Mid(m_xCode, iSelStart + iSelLen + 1)
            m_xCode = xPreSel & xPostSel
        Else
'           there is no selection - only
'           delete and backspace need to be addressed
            If KeyCode = vbKeyDelete Then
                If iSelStart > Len(.Text) - 1 Then
                    Exit Sub
                End If

'               get char to delete
                xChar = Mid(m_xCode, iSelStart + 1, 1)

'               test if char is a previous level
                If IsNumeric(xChar) Then
                    iPrevLevel = xChar
                    i = 0
'                   modify selection to select all chars in number
                    While xChar = CStr(iPrevLevel)
                        i = i + 1
                        xChar = Mid(m_xCode, iSelStart + 1 + i, 1)
                    Wend
                    .SelLength = i

                    xPostSel = Mid(m_xCode, iSelStart + .SelLength + 1)
                Else
                    xPostSel = Mid(m_xCode, iSelStart + 2)
                End If

'               delete the corresponding chars in m_xCode
                xPreSel = Left(m_xCode, iSelStart)
                m_xCode = xPreSel & xPostSel
            ElseIf KeyCode = vbKeyBack Then
                If iSelStart = 0 Then
                    Exit Sub
                End If

'               get char to delete
                xChar = Mid(m_xCode, iSelStart, 1)

'               test if char is a previous level
                If IsNumeric(xChar) Then
                    iPrevLevel = xChar
                    i = 0
'                   modify selection to select all chars in number
                    While (xChar = CStr(iPrevLevel))
                        i = i + 1
                        On Error Resume Next
                        xChar = ""
                        xChar = Mid(m_xCode, iSelStart - i, 1)
                        On Error GoTo 0
                    Wend
                    iSelStart = .SelStart - i
                    .SelStart = iSelStart
                    .SelLength = i
                    If iSelStart Then
                        xPreSel = Left(m_xCode, iSelStart)
                    End If
                    xPostSel = Mid(m_xCode, iSelStart + .SelLength + 1)
                Else
                    xPreSel = Left(m_xCode, iSelStart - 1)
                    xPostSel = Mid(m_xCode, iSelStart + 1)
                End If

'               delete the corresponding chars in m_xCode
                m_xCode = xPreSel & xPostSel
            End If
'           prevent rtNumberFormat_SelChange event from running
'           before m_xCode is updated by rtfNumberFormat_Change
            m_bNoSelChangeEvent = True
        End If
    End With
End Sub

#If False Then
Private Sub rtfNumberFormat_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iSelStart As Integer
    Dim iSelLen As Integer
    Dim xSelCode As String
    Dim iLevel As Integer
    Dim bNumIsInSel As Boolean
    Dim bCharIsNum As Boolean
    Dim xPreSel As String
    Dim xPostSel As String
    Dim iKey As Integer
    Dim bIsAllowed As Boolean
    Dim bIsNavigationKey As Boolean
    Dim xNew As String
    Dim xChar As String
    Dim iPrevLevel As Integer
    Dim i As Integer

'   exit if ctl or alt is down
    If Shift = vbCtrlMask Or Shift = vbAltMask Then
        Exit Sub
    End If

    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate

'   define valid characters -
'   shift = 16
    iKey = UCase(KeyCode)
    bIsAllowed = (iKey >= 32 And iKey <= 96) Or _
                 (iKey >= 123 And iKey <= 126) Or _
                 (iKey = vbKeyDelete) Or _
                 (iKey = vbKeyBack) Or (iKey = 190)

    bIsNavigationKey = (iKey = vbKeyLeft) Or _
                       (iKey = vbKeyRight) Or _
                       (iKey = vbKeyHome) Or _
                       (iKey = vbKeyEnd) Or _
                       (iKey = vbKeyInsert) Or _
                       (iKey = vbKeyUp) Or _
                       (iKey = vbKeyDown) Or _
                       (iKey = vbKeyPageUp) Or _
                       (iKey = vbKeyPageDown)

'   allow non-input keys to go through unmodified
    If Not bIsAllowed Then
        Exit Sub
    ElseIf bIsNavigationKey Then
        Exit Sub
    End If

    iLevel = Me.cmbLevel


    With Me.rtfNumberFormat
'       get selection detail
        iSelStart = .SelStart
        iSelLen = .SelLength
        xSelCode = Mid(m_xCode, iSelStart + 1, iSelLen)

'       different actions for selection/non-selection
        If iSelLen Then
'           there is a selection - test for existence of
'           current level number in selection
            bNumIsInSel = (InStr(xSelCode, iLevel) > 0)
            If bNumIsInSel Then
'               prevent any key from going through and alert user
                KeyCode = 0
                xMsg = "Ne peut supprimer le num�ro du niveau actuel. Veuillez modifier votre s�lection pour exclure ce num�ro de niveau " & iLevel & "."
                MsgBox xMsg, vbExclamation, g_xAppName
            Else
'               delete the corresponding chars
'               in m_xCode and insert new character
                xPreSel = Left(m_xCode, iSelStart)
                xPostSel = Mid(m_xCode, iSelStart + iSelLen + 1)
                m_xCode = xPreSel & xPostSel
            End If
        Else
'           there is no selection - only
'           delete and backspace need to be addressed
            If KeyCode = vbKeyDelete Then
                If iSelStart > Len(.Text) - 1 Then
                    Exit Sub
                End If

'               get char to delete
                xChar = Mid(m_xCode, iSelStart + 1, 1)

'               test if the char to be deleted is current level number
                bCharIsNum = (xChar = CStr(iLevel))

                If bCharIsNum Then
'                   prevent key from going through and alert user
                    KeyCode = 0
                    xMsg = "Ne peut supprimer le num�ro du niveau actuel."
                    MsgBox xMsg, vbExclamation, g_xAppName
                Else
'                   test if char is a previous level
                    If IsNumeric(xChar) Then
                        iPrevLevel = xChar
                        i = 0
'                       modify selection to select all chars in number
                        While xChar = CStr(iPrevLevel)
                            i = i + 1
                            xChar = Mid(m_xCode, iSelStart + 1 + i, 1)
                        Wend
                        .SelLength = i

                        xPostSel = Mid(m_xCode, iSelStart + .SelLength + 1)
                    Else
                        xPostSel = Mid(m_xCode, iSelStart + 2)
                    End If

'                   delete the corresponding chars in m_xCode
                    xPreSel = Left(m_xCode, iSelStart)
                    m_xCode = xPreSel & xPostSel
                End If

            ElseIf KeyCode = vbKeyBack Then
                If iSelStart = 0 Then
                    Exit Sub
                End If

'               get char to delete
                xChar = Mid(m_xCode, iSelStart, 1)

'               test if the char to be deleted is current level number
                bCharIsNum = (xChar = CStr(iLevel))

                If bCharIsNum Then
'                   prevent key from going through and alert user
                    KeyCode = 0
                    xMsg = "Ne peut supprimer le num�ro du niveau actuel."
                    MsgBox xMsg, vbExclamation, g_xAppName
                Else
'                   test if char is a previous level
                    If IsNumeric(xChar) Then
                        iPrevLevel = xChar
                        i = 0
'                       modify selection to select all chars in number
                        While (xChar = CStr(iPrevLevel))
                            i = i + 1
                            On Error Resume Next
                            xChar = ""
                            xChar = Mid(m_xCode, iSelStart - i, 1)
                            On Error GoTo 0
                        Wend
                        iSelStart = .SelStart - i
                        .SelStart = iSelStart
                        .SelLength = i
                        If iSelStart Then
                            xPreSel = Left(m_xCode, iSelStart)
                        End If
                        xPostSel = Mid(m_xCode, iSelStart + .SelLength + 1)
                    Else
                        xPreSel = Left(m_xCode, iSelStart - 1)
                        xPostSel = Mid(m_xCode, iSelStart + 1)
                    End If

'                   delete the corresponding chars in m_xCode
                    m_xCode = xPreSel & xPostSel
                End If
            End If
'           prevent rtNumberFormat_SelChange event from running
'           before m_xCode is updated by rtfNumberFormat_Change
            m_bNoSelChangeEvent = True
        End If
    End With
End Sub
#End If

Private Sub rtfNumberFormat_Change()
'deal with number format backend string
    Dim iStart As Integer
    Dim xCodeSelection As String
    Dim iLevel As Integer
    Dim iAdded As Integer
    Dim xAdded As String
    Dim i As Integer
    Dim iPrevLevel As Integer
    Dim xLevelCode As String
    Dim xPreCode As String
    Dim xPostCode As String
    Dim iColor As Integer
    Dim bRawNumberFormat As Boolean
    Dim xLT As String

    If m_bFormInit Or m_bStopChangeEvent _
        Then Exit Sub
        
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
        
    m_bLevelDirty = True
    iLevel = Me.cmbLevel
    
    With Me.rtfNumberFormat
        If InStr(m_xCode, iLevel) = 0 Then
'           current level is not in number -
'           set number style to "no number"
            Me.cmbNumberStyle.SelectedItem = 0
        End If
        
'       get num of chars added
        DoEvents
        iAdded = Len(.Text) - Len(m_xCode)

'       get start pos from current selection
'       pos minus the number of chars entered
        iStart = mpMax(.SelStart - iAdded, 0)

'       if chars were added...
        If iAdded > 0 Then
            If m_bIsPrevLevels Then
'               the added chars were previous levels
                With Me.lstPrevLevels.SelBookmarks
'                   cycle through all added levels
                    For i = 0 To .Count - 1
                        iPrevLevel = .Item(i) + 1
'                       converts selected level number to
'                       appropriate code - each char in the
'                       number is represented by the number's
'                       level - eg the level 2 number 'One'
'                       is represented as '222'
                        xLT = xGetFullLTName(g_oCurScheme.Name)
                        bRawNumberFormat = (iMapNumberStyle_WordToMP(ActiveDocument _
                            .ListTemplates(xLT).ListLevels(iPrevLevel).NumberStyle) > 16)
                        If bRawNumberFormat Then
                            'always two characters long - % + level
                            xLevelCode = String(2, CStr(iPrevLevel))
                        Else
                            xLevelCode = String(Len(xGetListNumber( _
                                g_oCurScheme.Name, iPrevLevel)), CStr(iPrevLevel))
                        End If
                        
'                       build code string
                        xAdded = xAdded & xLevelCode
                    Next
                End With
                iColor = vbRed
            Else
'               the added chars were straight text -
'               a straight text char is represented by '$'
                xAdded = String(iAdded, mpTextCode)
                iColor = vbBlack
            End If

'           modify number code representation as appropriate
            If iStart Then
                xPreCode = Left(m_xCode, iStart)
                xPostCode = Mid(m_xCode, iStart + 1)
                m_xCode = xPreCode & xAdded & xPostCode
            Else
                m_xCode = xAdded & m_xCode
            End If

            m_bStopChangeEvent = True

'           ensure that the added text displays the
'           correct color for the type of text that
'           was added - prev levels are red,
'           straight text is black
            .SelStart = iStart
            .SelLength = iAdded
            .SelColor = iColor


'           set selection point to the pos
'           after the inserted chars
            m_bNoSelChangeEvent = True
            .SelStart = .SelStart + iAdded
            m_bNoSelChangeEvent = True
            .SelLength = 0
            On Error Resume Next
            .SetFocus
            On Error GoTo 0
            m_bStopChangeEvent = False
            m_bNoSelChangeEvent = False
        End If
    End With
End Sub

Private Sub rtfNumberFormat_LostFocus()
    m_xPrevCtl = "rtfNumberFormat"
    RefreshLevel
End Sub

Private Sub rtfNumberFormat_SelChange()
    If (m_bFormInit Or m_bStopChangeEvent Or m_bNoSelChangeEvent) Then
        m_bNoSelChangeEvent = False
        Exit Sub
    End If
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate

'   m_bSelChangeRunning prevents the SelChange
'   event from firing a second time
    If Not m_bSelChangeRunning Then
        m_bSelChangeRunning = True
        bModifySelection
        m_bSelChangeRunning = False
    End If
    
'   set tooltip text for current selection
    SetNumFormatTooltip
End Sub

Private Function SetNumFormatTooltip()
'   set tooltip text to descriptor of selection
    Dim xText As String
    Dim xCode As String
    Dim xChr1 As String
    Dim iLevel As Integer
    
    With Me.rtfNumberFormat
        Select Case .SelLength
            Case -1
                xText = ""
            Case 0, 1
'               get code behind selection
                xCode = Mid(m_xCode, .SelStart + 1, 1)
                Select Case xCode
                    Case mpTextCode
                        xText = "Texte"
                    Case Else
                        If Len(xCode) Then
                            xText = "Niveau " & xCode
                            iLevel = CInt(xCode)
                        End If
                End Select
            Case Else
'               get code behind selection
                xCode = Mid(m_xCode, .SelStart + 1, .SelLength)

'               get code of first chr in selection
                xChr1 = Mid(m_xCode, .SelStart + 1, 1)
                
                
'               check if code string is all same chr
                If Len(xChr1) = 0 Or (xChr1 = "$") Then
                    xText = ""
                ElseIf String(.SelLength, xChr1) = xCode Then
                    xText = "Niveau " & xChr1
                    iLevel = CInt(xChr1)
                Else
                    xText = ""
                End If
        End Select
        
'       set tooltip
        .ToolTipText = xText
        
''       select row in previous levels
''       if selection is prev level
'        If iLevel > 0 Then
'            If iLevel < CInt(Me.cmbLevel) Then
''               it's a prev level - clear
''               existing prev level selections
'                ClearPrevLevelSelections
''               select prev level
'                With Me.lstPrevLevels
'                    .Row = iLevel - 1
'                    .SelBookmarks.Add iLevel - 1
'                End With
'            End If
'        End If
    End With
End Function

Private Function bModifySelection(Optional bGoForward As Boolean = False, _
                                  Optional bGoBack As Boolean = False) As Boolean
'modifies current selection to include the selection
'of entire numbers - prevents numbers from being
'split apart
    Dim xLeftChar As String
    Dim xRightChar As String
    Dim xNextChar As String
    Dim xPrevChar As String
    Dim bIsSameLevel As Boolean
    Dim iStart As Integer
    Dim iEnd As Integer
    Dim bInNumber As Boolean
    
    With Me.rtfNumberFormat
        If .SelStart = 0 Then
            Exit Function
        End If
        
'       test for selection
        If Len(.SelText) = 0 Then
            xLeftChar = Mid(m_xCode, .SelStart, 1)
            xRightChar = Mid(m_xCode, .SelStart + 1, 1)
            bInNumber = IsNumeric(xLeftChar) And _
                        IsNumeric(xRightChar) And _
                        (xLeftChar = xRightChar)
            If bInNumber Then
                iStart = .SelStart
                iEnd = .SelStart
            Else
                Exit Function
            End If
        Else
'           get start and end points
            iStart = .SelStart
            iEnd = .SelStart + .SelLength
        End If
        
'       check left border of selection for a
'       prev level or current level number
        xLeftChar = Mid(m_xCode, iStart + 1, 1)
            
'       check right border of selection for a
'       prev level or current level number
        xRightChar = Mid(m_xCode, iEnd, 1)
            
'       if left side is a level, extend
'       the selection to the left
'       for as far as that number goes
        If IsNumeric(xLeftChar) And iStart Then
            xPrevChar = Mid(m_xCode, iStart, 1)
            
            While (xPrevChar = xLeftChar) And iStart
                iStart = iStart - 1
                On Error Resume Next
                xPrevChar = Mid(m_xCode, iStart, 1)
                On Error GoTo 0
            Wend
        End If
        
'       if right side is a level, extend
'       the selection to the right
'       for as far as that number goes
        If IsNumeric(xRightChar) And iEnd <> (.SelStart + .SelLength + 1) Then
            xNextChar = Mid(m_xCode, iEnd + 1, 1)
            
            While (xNextChar = xRightChar) And _
                    iEnd <> Len(.Text)
                iEnd = iEnd + 1
                xNextChar = Mid(m_xCode, iEnd + 1, 1)
            Wend
        End If
        
'       make new selection
        .SelStart = iStart
        .SelLength = iEnd - .SelStart
    End With
    m_bSelChangeRunning = True
End Function

Function lLoadLevelProperties() As Long
    Dim xStyle As String
    Dim xTCStyle As String
    Dim iLevel As Integer
    Dim iNumStyle As Integer
    Dim xNumFmt As String
    Dim xNumFmtTemp As String
    Dim xNumber As String
    Dim iLoc As Integer
    Dim iSelLevel As Integer
    Dim iSelStart As Integer
    Dim xNextPara As String
    Dim bEnabled As Boolean
    Dim bLegal As Boolean
    Dim fntTCHeading As Word.Font
    Dim xLT As String
    Dim iLineSpaceRule As Integer
    Dim pfNormal As Word.ParagraphFormat
    Dim sTabPosition As Single
    Dim sMod As Single
    Dim sDef As Single
    Dim iAlignment As Integer
    Dim iHeadingFormat As Integer
    Dim xRoot As String
    Dim bRawNumberFormat As Boolean
    Dim oLT As Word.ListTemplate
    
    On Error GoTo ProcError
    
    m_bLevelChanged = True
    m_bAllowRefresh = False
    
    iLevel = Me.cmbLevel
    xStyle = xGetStyleName(g_oCurScheme.Name, iLevel)
    xLT = xGetFullLTName(g_oCurScheme.Name)
    Set oLT = ActiveDocument.ListTemplates(xLT)
    xRoot = xGetStyleRoot(g_oCurScheme.Name)
    
    With oLT.ListLevels(iLevel)
        'fill number format rtf text box
        m_xCode = ""
        xNumFmt = Trim(.NumberFormat)
        'determine whether we can display the actual characters of
        'the dynamic portion of the number - if the style is not supported,
        'we'll leave the level tokens unevaluated
        iNumStyle = iMapNumberStyle_WordToMP(.NumberStyle)
        m_bRawNumberFormat = (iNumStyle > 16)
        If .NumberStyle = wdListNumberStyleBullet Then
            '9.9.6001 - support all bullets
            m_xBulletFont = .Font.Name
            m_xBulletCharNum = .NumberFormat
            
            With Me.rtfNumberFormat
                .Enabled = False
                .Font.Name = "Symbol"
                .Text = ChrW(&HB7)
                .SelStart = 0
                .SelLength = 1
                .SelColor = vbBlack
                .SelLength = 0
                m_xCode = ChrW(&HB7)
            End With
        Else    'anything other than bullet
            '9.9.6001 - restore bullet defaults
            m_xBulletFont = "Symbol"
            m_xBulletCharNum = ChrW(&HB7)
            
            With Me.rtfNumberFormat
                .Enabled = True
                .Font.Name = "Arial"
                .Text = xNumFmt
                .SelStart = 0
                .SelLength = Len(.Text)
                .SelColor = vbBlack
                iLoc = InStr(.Text, "%")
                While iLoc
                    .SelStart = iLoc - 1
                    iSelStart = .SelStart
                    .SelLength = 2
                    iSelLevel = Mid(.Text, iLoc + 1, 1)
                    If iSelLevel = iLevel Then
                        bRawNumberFormat = m_bRawNumberFormat
                    Else
                        bRawNumberFormat = (iMapNumberStyle_WordToMP(oLT _
                            .ListLevels(iSelLevel).NumberStyle) > 16)
                    End If
                    If bRawNumberFormat Then
                        xNumber = "%" & iSelLevel
                    Else
                        xNumber = LTrim(xGetListNumber(g_oCurScheme.Name, _
                            iSelLevel))
                    End If
                    .SelText = xNumber
                    .SelStart = iSelStart
                    .SelLength = Len(xNumber)
                    If iSelLevel = iLevel Then
                        .SelColor = vbBlue
                    Else
                        .SelColor = vbRed
                    End If
                    m_xCode = m_xCode & _
                        String(iLoc - 1 - Len(m_xCode), mpTextCode) & _
                        String(Len(xNumber), LTrim(Str(iSelLevel)))
                    iLoc = InStr(iLoc + 1, .Text, "%")
                Wend
                m_xCode = m_xCode & _
                    String(Len(.Text) - Len(m_xCode), mpTextCode)
                xUserAll = .TextRTF
                .SelStart = 0
            End With
        End If
        
'       get font properties
        With .Font
            On Error Resume Next
            Me.chkNumberBold = ConvToTripleState(.Bold)
            Me.chkNumberCaps = ConvToTripleState(.AllCaps)
            Me.chkNumberItalic = ConvToTripleState(.Italic)
            
            Me.cmbUnderlineStyle.BoundText = .Underline
            If Me.cmbUnderlineStyle.BoundText = "" Then
                Me.cmbUnderlineStyle.SelectedItem = 0
            End If
            
            If .Name = "" Then
                Me.cmbNumFontName.SelectedItem = 0
            Else
                Me.cmbNumFontName.BoundText = .Name
                If Me.cmbNumFontName.BoundText = "" Then
                    Me.cmbNumFontName.SelectedItem = 0
                End If
            End If
            If g_sNumberFontSizes(iLevel - 1) = wdUndefined Then
                Me.cmbNumFontSize.SelectedItem = 0
            Else
                Me.cmbNumFontSize.BoundText = _
                    Trim(Str(g_sNumberFontSizes(iLevel - 1)))
            End If
            On Error GoTo ProcError
        End With
        
'       get number style
        Select Case iNumStyle
            Case mpListNumberStyleArabic, mpListNumberStyleArabicLZ
                Me.chkLegalStyle = vbUnchecked
            Case mpListNumberStyleLegal
                iNumStyle = mpListNumberStyleArabic
                Me.chkLegalStyle = vbChecked
                FormatLegalNumber
            Case mpListNumberStyleLegalLZ
                iNumStyle = mpListNumberStyleArabicLZ
                Me.chkLegalStyle = vbChecked
                FormatLegalNumber
            Case Else
                Me.chkLegalStyle = vbUnchecked
                If Me.chkLegalStyle.Enabled Then _
                    FormatLegalNumber
        End Select
        
'        Dim iListIndex As Integer
'        On Error Resume Next
'        iListIndex = Me.cmbNumberStyle.SelectedItem
'        On Error GoTo ProcError
'        If iListIndex > 9 Then _
'            iListIndex = iListIndex + m_iFixedDigitNumberingOffset
'        If iListIndex <> iNumStyle Then
            If m_bRawNumberFormat Then
                'the two built-in legal styles are not on the list
                Me.cmbNumberStyle.SelectedItem = iNumStyle - 2 - m_iFixedDigitNumberingOffset
            ElseIf iNumStyle > 9 Then
                Me.cmbNumberStyle.SelectedItem = iNumStyle - m_iFixedDigitNumberingOffset
            Else
                Me.cmbNumberStyle.SelectedItem = iNumStyle
            End If
'        Else
''           this will ensure dependent items, e.g. start at
'            cmbNumberStyle_Click
'        End If
                
'       get other number properties
        '9.9.6014 - ResetOnHigher property is no longer a boolean, so this was
        'causing an error for coders without the Word 8 obj if value was greater than 2 -
        'it's easy enough to fix this
        Me.chkReset = IIf(Abs(CInt(.ResetOnHigher)) = 0, 0, 1) 'Abs(CInt(.ResetOnHigher))
        Me.chkRightAlign = Abs(.Alignment = wdListLevelAlignRight)
        Me.chkNonbreakingSpaces = Abs(CBool(InStr(.NumberFormat, ChrW(&HA0))))
        
'        Me.txtNumberPosition = Format(.NumberPosition / 72, "#,##0.00")
'        If .TabPosition = 9999999 Then
'            Me.txtTabPosition = "0.00"
'        Else
'            Me.txtTabPosition = _
'                Format((.TabPosition - .NumberPosition) / 72, "#,##0.00")
'        End If
'        Me.txtTextPosition = Format(.TextPosition / 72, "#,##0.00")
        sTabPosition = .TabPosition
    End With
    
'   get trailing character from level properties
    Me.cmbTrailingChar.BoundText = CLng(xGetLevelProp(g_oCurScheme.Name, _
                                    iLevel, _
                                    mpNumLevelProp_TrailChr, _
                                    mpSchemeType_Document))
    
    bEnabled = (Me.cmbTrailingChar.Text = "Tabulation")
    Me.spnTabPosition.Enabled = bEnabled
    Me.lblTabPosition.Enabled = bEnabled

'   get tc format from level properties-
'   format info is held as bits
'   in an integer - get integer
    iHeadingFormat = xGetLevelProp(g_oCurScheme.Name, _
                              iLevel, _
                              mpNumLevelProp_HeadingFormat, _
                              mpSchemeType_Document)
                              
   ' test bits for format value
    Me.chkTCBold = iHasTCFormat(iHeadingFormat, _
                       mpTCFormatField_Bold)
    Me.chkTCItalic = iHasTCFormat(iHeadingFormat, _
                         mpTCFormatField_Italic)
    Me.chkTCCaps = iHasTCFormat(iHeadingFormat, _
                       mpTCFormatField_AllCaps)
    Me.chkTCUnderline = iHasTCFormat(iHeadingFormat, _
                            mpTCFormatField_Underline)
    Me.chkTCSmallCaps = iHasTCFormat(iHeadingFormat, _
                            mpTCFormatField_SmallCaps)
                            
'   get type
    If (iHeadingFormat And mpTCFormatField_Type) Then
        Me.optStyleFormats = True
    Else
        Me.optDirectFormats = True
    End If
        
'   get whether to honor right aligned styles
    iAlignment = xGetLevelProp(g_oCurScheme.Name, _
                               iLevel, _
                               mpNumLevelProp_TrailUnderline, _
                               mpSchemeType_Document)
                               
    With ActiveDocument.Styles(xStyle)
'       get paragraph format
        With .ParagraphFormat
'           alignment
            If ((.Alignment = wdAlignParagraphRight) And _
                    (iAlignment < 2)) Or bBitwisePropIsTrue(iAlignment, _
                    mpTrailUnderlineField_AdjustToNormal) Then
'               base on normal style; after editing in 9.8,
'               iAlignment will always be greater than 2
                Me.cmbTextAlign.BoundText = mpBaseAlignmentOnNormal
            Else
                Me.cmbTextAlign.BoundText = .Alignment
            End If
            
'           line spacing
            Me.cmbLineSpacing.BoundText = m_xarLineSpacing(.LineSpacingRule, 0)
'            Me.cmbLineSpacing.Text = m_xarLineSpacing(.LineSpacingRule, 0)
            If .LineSpacingRule = wdLineSpaceMultiple Then
                Me.spnAt.Value = .LineSpacing / 12
            Else
                Me.spnAt.Value = .LineSpacing
            End If
            
            If m_bFormInit Then
'               doevents is only safe here (and only necessary) on form load;
'               after that, it will wreak havoc if Alt+1, Alt+2, etc. is used
'               to switch levels
                DoEvents
            End If
            
            If .LineSpacingRule < 3 Then
                Me.spnAt.DisplayText = ""
            Else
                Me.spnAt.Refresh
            End If

'           space after is really space between
            Me.spnSpaceAfter.Value = .SpaceAfter
            Me.spnSpaceBefore.Value = .SpaceBefore
            Me.chkKeepLinesTogether = Abs(.KeepTogether)
            Me.chkKeepWithNext = Abs(.KeepWithNext)
            Me.chkWidowOrphan = Abs(.WidowControl)
            Me.chkPageBreak = Abs(.PageBreakBefore)
            Me.spnRightIndent.Value = .RightIndent
            
'           style indents now take priority over LT positions
            Me.spnTextPosition.Value = .LeftIndent
            Me.spnNumberPosition.Value = (.LeftIndent + .FirstLineIndent)
            If sTabPosition = 9999999 Then
                Me.spnTabPosition.Value = 0
            ElseIf sTabPosition > (.LeftIndent + .FirstLineIndent) Then
'               this is the normal situation
                Me.spnTabPosition.Value = (sTabPosition - .LeftIndent - _
                    .FirstLineIndent)
            Else
'               if tab is to left of number, Word will use default tab stop
                sDef = ActiveDocument.DefaultTabStop
                sMod = Abs((.LeftIndent + .FirstLineIndent + sDef) Mod sDef)
                If sMod = 0 Then
                    Me.spnTabPosition.Value = sDef
                Else
                    Me.spnTabPosition.Value = sMod
                End If
            End If
        End With

'       get next paragraph style
        If bIsHeadingStyle(.NextParagraphStyle) Then
            xNextPara = xRoot & "_L" & Right(.NextParagraphStyle, 1)
        Else
            xNextPara = .NextParagraphStyle
        End If
'       GLOG : 5445 : ceh
'       On Error GoTo AddStyleToNextParaList
        Me.cmbNextParagraph.BoundText = xNextPara
        If Me.cmbNextParagraph.BoundText = "" Then
            AddItemToTDBCombo Me.cmbNextParagraph, xNextPara
        End If
'       On Error GoTo ProcError
        
'       get font
        On Error Resume Next
        With .Font
            Me.cmbFontName.BoundText = .Name
            If Me.cmbFontName.BoundText = "" Then
                Me.cmbFontName.BoundText = "Times New Roman"
            End If
            Me.cmbFontSize.BoundText = Trim(Str(.Size))
        End With
        On Error GoTo ProcError
    End With
    
'   enable/disable trailing underline
'    SetTrailUnderlineState

'   cont style
    lLoadContLevel

'   enable/disable restart checkbox
    Me.chkReset.Enabled = (iLevel <> 1)
    
    'GLOG 5664 - moved DoEvents above flag resets because it can trigger control
    'event handlers that might run unnecessary code - this was specifically
    'a problem with the Bylaws scheme after implementing the TrueDB combos
    DoEvents
    
    ClearSchemeDirtFlags
    m_bLevelChanged = False
    m_bStopChangeEvent = False
    m_bAllowRefresh = True
    
    Exit Function
    
AddStyleToNextParaList:
'    GLOG : 5445 : ceh
'    Me.cmbNextParagraph.AddItem xNextPara
'    Resume
    
ProcError:
    If Err.Number = mpError_InvalidSchemePropValue Then
        If Not m_bAlertDisplayed Then
            xMsg = "Ce th�me a �t� �dit� avec Microsoft Word. Vous pouvez continuer � �diter ce th�me avec MacPac, mais notez que certains champs peuvent �tre vides.  Les champs vides n'affecteront pas les �ditions pr�c�dents � ce style.  En plus, l'interligne dans l'aper�u peuvent ne pas s'afficher."
            MsgBox xMsg, vbExclamation, g_xAppName
            m_bAlertDisplayed = True
        End If
        Resume Next
    Else
        RaiseError "frmEditSchemeFrench.lLoadLevelProperties"
        Exit Function
    End If
End Function

Private Function lLoadContLevel() As Long
    Dim iLevel As Integer
    Dim xRoot As String
    Dim xCont As String
    Dim iAlignment As Integer
    Dim styCont As Word.Style
    
    On Error GoTo ProcError
    
    iLevel = Me.cmbLevel
    xRoot = xGetStyleRoot(g_oCurScheme.Name)
    xCont = xRoot & " Cont " & iLevel
    
    iAlignment = xGetLevelProp(g_oCurScheme.Name, _
                               iLevel, _
                               mpNumLevelProp_TrailUnderline, _
                               mpSchemeType_Document)
                               
    On Error Resume Next
    Set styCont = ActiveDocument.Styles(xCont)
    On Error GoTo ProcError
    If styCont Is Nothing Then
        '9.9.6001 - copy cont style from source
        CreateContStyles g_oCurScheme.Name, iLevel, , _
            (g_oCurScheme.SchemeType = mpSchemeType_Document)
        Set styCont = ActiveDocument.Styles(xCont)
    End If
    
    With styCont
'       get font
        On Error Resume Next
        With .Font
            Me.cmbContFontName.BoundText = .Name
            If Me.cmbContFontName.BoundText = "" Then
                Me.cmbContFontName.BoundText = "Times New Roman"
            End If
            Me.cmbContFontSize.BoundText = Trim(Str(.Size))
        End With
        On Error GoTo ProcError
        
'       paragraph format
        With .ParagraphFormat
'           alignment
            If bBitwisePropIsTrue(iAlignment, _
                    mpTrailUnderlineField_AdjustContToNormal) Then
'               base on normal style
                Me.cmbAlignment.BoundText = mpBaseAlignmentOnNormal
            Else
                Me.cmbAlignment.BoundText = .Alignment
            End If
        
'           indents
            If .FirstLineIndent > 0 Then
                Me.cmbSpecial.BoundText = "1�re ligne"
                Me.spnContLeftIndent.Value = .LeftIndent
            ElseIf .FirstLineIndent < 0 Then
                Me.cmbSpecial.BoundText = "Retrait"
                Me.spnContLeftIndent.Value = .LeftIndent + .FirstLineIndent
            Else
                Me.cmbSpecial.BoundText = "(aucun)"
                Me.spnContLeftIndent.Value = .LeftIndent
            End If
            Me.spnBy.Value = Abs(.FirstLineIndent)
            Me.spnContRightIndent.Value = .RightIndent
        
'           line spacing
            Me.cmbContLineSpacing.BoundText = m_xarLineSpacing(.LineSpacingRule, 0)
            If .LineSpacingRule = wdLineSpaceMultiple Then
                Me.spnContAt.Value = .LineSpacing / 12
            Else
                Me.spnContAt.Value = .LineSpacing
            End If
            
            If m_bFormInit Then
'               doevents is only safe here (and only necessary) on form load;
'               after that, it will wreak havoc if Alt+1, Alt+2, etc. is used
'               to switch levels
                DoEvents
            End If

            If .LineSpacingRule < 3 Then
                Me.spnContAt.DisplayText = ""
            Else
                Me.spnContAt.Refresh
            End If
                        
            Me.spnContSpaceAfter.Value = .SpaceAfter
            Me.spnContSpaceBefore.Value = .SpaceBefore

'           other
            Me.chkContWidowOrphan = Abs(.WidowControl)
            Me.chkContKeepWithNext = Abs(.KeepWithNext)
            Me.chkContKeepLinesTogether = Abs(.KeepTogether)
        End With
    End With
    
    Exit Function
ProcError:
    RaiseError "frmEditSchemeFrench.lLoadContLevel"
    Exit Function
End Function

Private Sub udStartAt_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    ChangeStartAt
End Sub

Private Sub ChangeStartAt()
    Dim iNumberStyle As Integer
    Dim iLevel As Integer
    Dim iStartAt As Integer
    Dim iPos As Integer
    Dim iOldNumLength As Integer
    Dim xNewNumber As String
    Dim xStartAt As String
    Dim iListIndex As Integer
    
    m_bLevelDirty = True
    
'   convert appropriate format
    If Not m_bRawNumberFormat Then
        iListIndex = Me.cmbNumberStyle.SelectedItem
        If iListIndex > 9 Then _
            iListIndex = iListIndex + m_iFixedDigitNumberingOffset
        iNumberStyle = iMapNumberStyle_MPToWord(iListIndex)
    End If
    iStartAt = mpMax(mpMin(Me.udStartAt.Value, 1000), 1)

'   update start at textbox
    If m_bRawNumberFormat Or (iNumberStyle = wdListNumberStyleOrdinal) Or _
            (iNumberStyle = wdListNumberStyleOrdinalText) Or _
            (iNumberStyle = wdListNumberStyleCardinalText) Then
        Me.txtStartAt.Text = LTrim(Str(iStartAt)) & Space(1)
    Else
        xStartAt = xIntToListNumStyle(iStartAt, iNumberStyle)
        Me.txtStartAt.Text = xStartAt & Space(1)
    End If

'   update number format
    If Not m_bLevelChanged Then
        m_bStopChangeEvent = True
        
        If iNumberStyle = wdListNumberStyleBullet Then
            With Me.rtfNumberFormat
                .Text = ChrW(&HB7)
                .Font.Name = "Symbol"
            End With
            m_xCode = ChrW(&HB7)
        Else
            iLevel = Me.cmbLevel
            If m_bRawNumberFormat Then
                xNewNumber = "%" & iLevel
            Else
                xNewNumber = xIntToListNumStyle(iStartAt, _
                    iNumberStyle)
            End If
            If m_xCode <> ChrW(&HB7) Then
                Dim bCurLevelExists As Boolean
                Dim iPosCurLevel As Integer
    '           get position of current level number
                iPosCurLevel = InStr(m_xCode, LTrim(iLevel))
                
                If iPosCurLevel Then
    '               there is a current level # - store pos
                    iPos = iPosCurLevel
                Else
    '               there is no current level # -
    '               get end position of code
                    iPos = Len(m_xCode) + 1
                End If
            Else
                iPos = 1
            End If
            
            iOldNumLength = lCountChrs(m_xCode, LTrim(iLevel))
            
    '       update code behind rtf number
            m_xCode = Left(m_xCode, iPos - 1) & _
                      String(Len(xNewNumber), LTrim(iLevel)) & _
                      Right(m_xCode, Len(m_xCode) - iPos - iOldNumLength + 1)
            
    '       insert text in number format box
            With Me.rtfNumberFormat
                .SelStart = mpMax(iPos - 1, 0)
                .SelLength = iOldNumLength
                If iOldNumLength = 0 Then
                    .SelColor = vbBlue
                End If
                .SelText = xNewNumber
                xUserAll = .TextRTF
            End With
        End If
        
        m_bStopChangeEvent = False
    End If
End Sub

Private Sub ClearPrevLevelSelectionsButLast()
    Dim i As Integer
    
'   remove listing selections
    With Me.lstPrevLevels.SelBookmarks
        .Add Me.lstPrevLevels.Bookmark
        For i = .Count - 2 To 0 Step -1
            .Remove i
        Next
        DoEvents
    End With
End Sub

Private Sub ClearPrevLevelSelections()
    Dim i As Integer
    
'   remove listing selections
    With Me.lstPrevLevels.SelBookmarks
        .Add Me.lstPrevLevels.Bookmark
        For i = .Count - 1 To 0 Step -1
            .Remove i
        Next
        DoEvents
    End With
End Sub

Private Sub lstPrevLevels_DblClick()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    InsertPrevLevel
End Sub
'
'Private Sub lstPrevLevels_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'    Dim dtoDataObj As MSForms.DataObject
'    Dim xPrev As String
'    Dim i As Integer
'    Dim iPrevLevel As Integer
'    Dim xLT As String
'
''   this will be true when inserting a
''   previous level - you don't want
''   to run anything during that time
'    If m_bInsertingPrevLevel Then
'        Exit Sub
'    End If
'
'    If ActiveDocument <> g_docPreview Then _
'        g_docPreview.Activate
'
'    xLT = xGetFullLTName(g_oCurScheme.Name)
'
''   do only if user is initiating a drag
'    If Button = vbLeftButton Then
'        With Me.lstPrevLevels
''           make appropriate selections
'            SelPrevLevels Y
'
''           start drag
'            .Drag vbBeginDrag
'
'            For i = 0 To .SelBookmarks.Count - 1
'                If .SelBookmarks(i) > -1 Then
'                    iPrevLevel = .SelBookmarks(i) + 1
''                   alert and exit if user is attempting
''                   to insert a previous level twice.
'                    If InStr(m_xCode, iPrevLevel) Then
'                        .Drag vbEndDrag
'                        Exit Sub
'                    End If
'
'                    If Me.chkLegalStyle Then
'                        xPrev = xPrev & _
'                            ActiveDocument.ListTemplates(xLT) _
'                            .ListLevels(iPrevLevel).StartAt
'                    Else
'                        xPrev = xPrev & xGetListNumber(g_oCurScheme.Name, _
'                                            iPrevLevel)
'
'    '                   disallow bullet
'                        If Right(xPrev, 1) = ChrW(&HB7) Then _
'                            xPrev = Left(xPrev, Len(xPrev) - 1)
'                    End If
'                End If
'            Next i
'
'            If xPrev <> "" Then
'                m_bIsPrevLevels = True
'                Set dtoDataObj = New MSForms.DataObject
'                dtoDataObj.SetText xPrev
'                dtoDataObj.StartDrag
'            Else
'                .Drag vbEndDrag
'            End If
'        End With
'        m_bIsPrevLevels = False
'    End If
'
'End Sub

Private Sub lstPrevLevels_RowChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    If Not (IsPressed(KEY_CTL) Or IsPressed(KEY_SHIFT)) Then
        ClearPrevLevelSelectionsButLast
    End If
End Sub

Private Sub spnNumberPosition_Change()
    If m_bStopChangeEvent Then
        Exit Sub
    End If
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    m_bParaDirty = True
End Sub

Private Sub spnSpaceAfter_Change()
    If m_bStopChangeEvent Then
        Exit Sub
    End If
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
End Sub

Private Sub spnSpaceAfter_LostFocus()
    m_xPrevCtl = "spnSpaceAfter"
    RefreshLevel
End Sub

Private Sub spnSpaceBefore_Change()
    If m_bStopChangeEvent Then
        Exit Sub
    End If
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
End Sub

Private Sub spnSpaceBefore_LostFocus()
    m_xPrevCtl = "spnSpaceBefore"
    RefreshLevel
End Sub

Private Sub txtStartAt_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
End Sub

Private Sub spnTabPosition_Change()
    If m_bStopChangeEvent Then
        Exit Sub
    End If
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    m_bParaDirty = True
End Sub

Private Sub spnTextPosition_Change()
    If m_bStopChangeEvent Then
        Exit Sub
    End If
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bLevelDirty = True
    m_bParaDirty = True
End Sub

Property Get InitialLevels() As Integer
    InitialLevels = m_iInitLevels
End Property

Property Get CurrentLevels() As Integer
    CurrentLevels = m_iCurLevels
End Property

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Get ZoomClicked() As Boolean
    ZoomClicked = m_bZoomClicked
End Property

Property Let ZoomClicked(ByVal bValue As Boolean)
    m_bZoomClicked = bValue
End Property

Property Get xarDirty() As xArray
    Set xarDirty = m_xarIsDirty
End Property

Function bSetDirt(iLevel As Integer, _
                               bSaveLevel As Boolean, _
                               bSaveTC As Boolean, _
                               bSaveTOC As Boolean, _
                               bSaveProp As Boolean, _
                               bSavePara As Boolean, _
                               bSaveNextPara As Boolean) As Boolean
'appropriate element is switched on once any change
'to that level component has been implemented in buffer;
'elements will never be set to "False"
    
    If bSaveLevel Then _
        xarDirty(iLevel, mpLevelIsDirty) = "True"
    If bSaveTC Then _
        xarDirty(iLevel, mpTCIsDirty) = "True"
    If bSaveTOC Then _
        xarDirty(iLevel, mpTOCIsDirty) = "True"
    If bSaveProp Then _
        xarDirty(iLevel, mpDocPropIsDirty) = "True"
    If bSavePara Then _
        xarDirty(iLevel, mpParaIsDirty) = "True"
    If bSaveNextPara Then _
        xarDirty(iLevel, mpNextParaIsDirty) = "True"
        
End Function

Private Sub FillPrevLevelsList(xScheme As String)
'fill previous level list

    Dim iPrevLevel As Integer
    Dim i As Integer
    Dim xNum As String
    
    iPrevLevel = Val(Me.cmbLevel.Text) - 1
    
    If iPrevLevel Then
        m_xarPrevLevels.ReDim 0, iPrevLevel - 1, 0, 1
        For i = 0 To iPrevLevel - 1
            xNum = xGetListNumber(xScheme, i + 1)
            m_xarPrevLevels(i, 0) = " Niveau " & i + 1
            m_xarPrevLevels(i, 1) = xNum
        Next i
    Else
        m_xarPrevLevels.ReDim 0, -1, 0, 1
    End If
    
    With Me.lstPrevLevels
        Set .Array = m_xarPrevLevels
        EchoOff
        .Rebind
        If m_xarPrevLevels.Count(1) Then
            .Row = 0
            .SelBookmarks.Add 0
        End If
        EchoOn
    End With
    
End Sub

Sub RedrawLevelButtons()
    Const mpRight As Integer = 6595
    Const mpBtnWidth As Integer = 330
    
    Dim i As Integer
    
    With Me.chkLevel
'       make visible all buttons up to last level
        For i = 0 To (m_iCurLevels - 1)
            .Item(i).Left = mpRight - _
                (m_iCurLevels - i) * mpBtnWidth
            .Item(i).Caption = "&" & i + 1
            .Item(i).Visible = True
        Next i
        
'       ensure that levels past last
'       level in scheme are invisible
        For i = m_iCurLevels To 8
            .Item(i).Visible = False
        Next
    End With
End Sub

Sub SelPrevLevels(iYRow As Single)
    Dim i As Integer
    Dim iRow As Integer
    Dim bSelected As Boolean
        Dim iLastSel As Integer
    
'   get row that is currently being selected
    iRow = Me.lstPrevLevels.RowContaining(iYRow)
            
    With Me.lstPrevLevels.SelBookmarks
'       see if this row is in selection
        For i = 0 To .Count - 1
            If .Item(i) = iRow Then
                bSelected = True
                Exit For
            End If
        Next i
        
'       if not already in selection...
        If Not bSelected Then
'           clear all selections unless ctl
'           or shift has been pressed
            If Not (IsPressed(KEY_CTL) Or _
                IsPressed(KEY_SHIFT)) Then
'               clear all selections
                While .Count
                    .Remove 0
                Wend
            ElseIf IsPressed(KEY_SHIFT) Then
                While .Count > 1
                    .Remove 0
                Wend
            End If

'           if shift was pressed, add all rows
'           between last selection and row clicked on
            If IsPressed(KEY_SHIFT) Then
'               get last selection
                iLastSel = .Item(.Count - 1)
                
                If iRow > iLastSel Then
'                   select downward
                    For i = .Item(.Count - 1) To iRow
                        .Add i
                    Next i
                Else
'                   select upward
                    For i = iRow To .Item(.Count - 1)
                        .Add iRow
                    Next i
                End If
            Else
'               add to selection
                .Add iRow
            End If
            
'           set new current row
            If iRow > -1 Then
                Me.lstPrevLevels.Row = iRow
            End If
'*****
'the branch below does not work because the
'row will be selected on mouse up anyway
'*****
'        Else
''           the row clicked on is
''           already in selection - remove
''           if ctl has been clicked
'            If IsPressed(KEY_CTL) Then
'                .Remove iRow
'            End If
        End If
    End With
End Sub

Private Sub udStartAt_LostFocus()
    m_xPrevCtl = "txtStartAt"
    If Not Screen.ActiveControl Is Me.txtStartAt Then
        RefreshLevel
    End If
End Sub

Private Function bLevelIsDirty() As Boolean
'returns TRUE if one of the module level
'flags representing level dirt has been set true
    bLevelIsDirty = ((m_bLevelDirty + _
                      m_bTCDirty + _
                      m_bParaDirty + _
                      m_bPropDirty + _
                      m_bNextParaDirty + _
                      m_bTOCDirty) < 0)
End Function

Private Sub RefreshLevel()
    Dim i As Integer
    Dim iPrevLevel As Integer
    Dim xNum As String
    Dim xNumFormat As String
    Dim bIsDirty As Boolean
    
    On Error GoTo ProcError
    
    m_bStopChangeEvent = True
    
'   create level based on user choices
    bIsDirty = bLevelIsDirty()
                 
    If (Not m_bFormInit) And m_bAllowRefresh And bIsDirty Then
        EchoOff
            
        Word.ActiveDocument.UndoClear
        
'       implement changes
        bRet = bCreateLevel(g_oCurScheme.Name, m_iLastSelLevel)
                            
'       cont style
        If m_bNextParaDirty Then
            bRet = bCreateContLevel(g_oCurScheme.Name, m_iLastSelLevel)
        End If
        
'       update dirty flag array
        bRet = bSetDirt(m_iLastSelLevel, _
                        m_bLevelDirty, _
                        m_bTCDirty, _
                        m_bTOCDirty, _
                        m_bPropDirty, _
                        m_bParaDirty, _
                        m_bNextParaDirty)
                                     
        On Error Resume Next
        m_oPreview.RefreshPreviewLevel g_oCurScheme.Name, _
                                       m_iLastSelLevel
        EchoOn
    End If

'   reset flags
    m_bStopChangeEvent = False
    ClearSchemeDirtFlags
    DoEvents
    '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
    'screen capture and design mode execution
    If g_iWordVersion < mpWordVersion_2013 Then _
        mdlApplication.SendShiftKey
    Exit Sub
ProcError:
    RaiseError "frmEditSchemeFrench.RefreshLevel"
    Exit Sub
End Sub

Private Sub SwitchLevels()
    Dim i As Integer
    Dim iPrevLevel As Integer
    Dim xNum As String
    Dim xNumFormat As String
    Dim bIsDirty As Boolean
    Dim bEnabled As Boolean
    
    On Error GoTo ProcError
    
    m_bStopChangeEvent = True
    m_bAllowRefresh = False
    
    EchoOff
    Application.ScreenUpdating = False
    
'   create level based on user choices
    bIsDirty = bLevelIsDirty()
    
    If (Not m_bFormInit) And bIsDirty Then
'       implement changes
        bRet = bCreateLevel(g_oCurScheme.Name, m_iLastSelLevel)
        
'       cont style
        If m_bNextParaDirty Then
            bRet = bCreateContLevel(g_oCurScheme.Name, m_iLastSelLevel)
        End If
        
'       update dirty flag array
        bRet = bSetDirt(m_iLastSelLevel, _
                        m_bLevelDirty, _
                        m_bTCDirty, _
                        m_bTOCDirty, _
                        m_bPropDirty, _
                        m_bParaDirty, _
                        m_bNextParaDirty)
    End If
    
    On Error Resume Next
    
    If Not m_bFormInit Then
        If bIsDirty Then _
            m_oPreview.RefreshPreviewLevel g_oCurScheme.Name, m_iLastSelLevel
        m_oPreview.HighlightLevel Me.cmbLevel
    End If
    
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    
'   load properties of level
    lRet = lLoadLevelProperties
        
'   fill previous level list
    FillPrevLevelsList g_oCurScheme.Name
    
'   set text in 'Heading' frame (removed in 9.5.1)
'    Me.optStyleFormats.Caption = "Save In " & _
'        xGetStyleRoot(g_oCurScheme.Name) & "_L" & _
'            Me.cmbLevel.ListIndex + 1 & " St&yle"
            
    bEnabled = (Me.cmbNumberStyle <> "Puce") And (Me.cmbLevel <> 1)

'    enable/disable prev levels - disabled on level 1 and bullet
    Me.lstPrevLevels.Enabled = bEnabled
    Me.lblPreviousLevels.Enabled = bEnabled
    Me.btnAddPrevLevel.Enabled = bEnabled
    
'   reset flags
    ClearSchemeDirtFlags
    
    m_bStopChangeEvent = False
    m_bLevelChanged = False
    m_bAllowRefresh = True
    
'   set as last selected level
    m_iLastSelLevel = cmbLevel
    DoEvents
    Exit Sub
ProcError:
    EchoOn
    RaiseError "frmEditSchemeFrench.SwitchLevels"
    Exit Sub
End Sub

Sub ClearSchemeDirtFlags()
    m_bLevelDirty = False
    m_bTCDirty = False
    m_bTOCDirty = False
    m_bPropDirty = False
    m_bParaDirty = False
    m_bNextParaDirty = False
End Sub
Sub InsertPrevLevel()
'insert selected previous levels directly
'before current level number
    Dim i As Integer
    Dim iLevel As Integer
    Dim iPos As Integer
    Dim xPrePos As String
    Dim xPostPos As String
    Dim xPrev As String
    Dim xNumFormat As String
    Dim iPrevLevel As Integer
    Dim xNum As String
    Dim bRawNumberFormat As Boolean
    Dim xLT As String
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    m_bInsertingPrevLevel = True
    
    iLevel = Me.cmbLevel
    
'   build string of numbers
    With Me.lstPrevLevels.SelBookmarks
        For i = (.Count - 1) To 0 Step -1
            iPrevLevel = .Item(i) + 1
'           alert and exit if user is
'           attempting to add level a
'           second time
            If InStr(m_xCode, iPrevLevel) Then
                xMsg = "Niveau " & iLevel & " contient d�j� le num�ro de niveau " & iPrevLevel & "."
                MsgBox xMsg, vbExclamation, g_xAppName
                m_bInsertingPrevLevel = False
                Exit Sub
            End If

            'determine whether we can display the actual characters of
            'the dynamic portion of the number - if the style is not supported,
            'we'll leave the level tokens unevaluated
            xLT = xGetFullLTName(g_oCurScheme.Name)
            bRawNumberFormat = (iMapNumberStyle_WordToMP(ActiveDocument _
                .ListTemplates(xLT).ListLevels(iPrevLevel).NumberStyle) > 16)
            If bRawNumberFormat Then
                xNum = "%" & iPrevLevel
            Else
                xNum = xGetListNumber(g_oCurScheme.Name, iPrevLevel, _
                    Me.chkLegalStyle = 1)
            End If

'           disallow bullet
            If xNum <> ChrW(&HB7) Then
                xPrev = xPrev & xNum
            End If
        Next i
    End With
    
'   do only if there are selected previous levels
    If Len(xPrev) Then
'       get position of current level number
        iPos = InStr(m_xCode, iLevel)
        
        With Me.rtfNumberFormat
'           prevent sel change from running
'           when we make our selection
            m_bNoSelChangeEvent = True
'           if current level number exists...
            If iPos Then
'               insert previous levels directly
'               before current level number
                .SelStart = iPos - 1
            Else
'               insert previous levels at end of text
                .SelStart = Len(.Text)
            End If
            m_bIsPrevLevels = True
            m_iSelStart = .SelStart
'           again, prevent change code from running
            m_bNoSelChangeEvent = True
            .SelLength = 0
            .SelColor = vbRed
            .SelText = xPrev
            m_bIsPrevLevels = False
        End With
    End If
    RefreshLevel
    m_bInsertingPrevLevel = False
    Exit Sub
ProcError:
    m_bInsertingPrevLevel = False
    RaiseError "frmEditSchme.InsertPrevLevel"
    Exit Sub
End Sub

Sub SetTrailUnderlineState()
'   uncheck and disable trailing underline
'   if number is not underlined - enable
'   trailing underline if number underline
'   is enabled
    If Me.chkNumberUnderline = vbUnchecked Then
'        Me.chkTrailingUnderline = vbUnchecked
        Me.cmbUnderlineStyle.Enabled = False
    ElseIf Me.chkNumberUnderline = vbChecked Then
        Me.cmbUnderlineStyle.Enabled = True
    Else
        Me.cmbUnderlineStyle.Enabled = False
    End If
    
'   enable trail underline if num underline
'   is checked and trailing chr is not shift-return(s)
    
'    Me.chkTrailingUnderline.Enabled = _
'        (Me.chkNumberUnderline = vbChecked) And _
'        (Me.cmbTrailingChar.ListIndex <> mpTrailingChar_ShiftReturn) And _
'        (Me.cmbTrailingChar.ListIndex <> mpTrailingChar_DoubleShiftReturn) And _
'        (Me.cmbTrailingChar.ListIndex <> mpTrailingChar_None)
End Sub

Sub SetTriStateChkbox(chkP As VB.CheckBox)
    If Not m_bClicked Then
        m_bLevelDirty = True
    End If
End Sub

Public Sub CreatePreview()
    EchoOff
'   create preview in doc
    With g_oCurScheme
        m_oPreview.ShowPreview .Name, .SchemeType
        m_oPreview.HighlightLevel 1 '9.9.6010
        
'       create initial bitmap for new scheme
        If .SchemeType = mpSchemeType_Private Then
            If Dir(g_xMPBPath & "\" & g_oCurScheme.Alias & ".mpb") = "" Then _
                m_oPreview.CreateBitmap .Name, .Alias
        End If
    End With
    EchoOn
End Sub
Sub FormatLegalNumber()
    Exit Sub
'  STILL UNDER DEVELOPMENT

'insert selected previous levels directly
'before current level number
    Dim i As Integer
    Dim iLevel As Integer
    Dim iPos As Integer
    Dim xNum As String
    Dim iStartSel As Integer
    Dim iOldNumLength As Integer
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    m_bInsertingPrevLevel = True
    
    
    iLevel = Me.cmbLevel
    iStartSel = Me.rtfNumberFormat.SelStart
    
    For i = 1 To iLevel - 1
        iPos = InStr(m_xCode, LTrim(Str(i)))
        
        If iPos > 0 Then
            iOldNumLength = lCountChrs(m_xCode, _
                LTrim(Str(i)))
            
            xNum = xGetListNumber(g_oCurScheme.Name, i, Me.chkLegalStyle = 1)
            iStartSel = iStartSel + (Len(xNum) - iOldNumLength)
            
'           insert and format new number
            With Me.rtfNumberFormat
                .SelStart = iPos - 1
                .SelLength = iOldNumLength
                .SelText = xNum
                xUserAll = .TextRTF
'                .SelColor = vbBlue
            End With
            m_xCode = Left(m_xCode, iPos - 1) & _
                String(Len(xNum), LTrim(Str(i))) & _
                Right(m_xCode, Len(m_xCode) - iPos - iOldNumLength + 1)
        End If
    Next i
    Me.rtfNumberFormat.SelStart = iStartSel
    
    RefreshLevel
    m_bInsertingPrevLevel = False
    Exit Sub
ProcError:
    m_bInsertingPrevLevel = False
    RaiseError "frmEditSchme.InsertPrevLevel"
    Exit Sub
End Sub

Private Sub spnRightIndent_Change()
    If m_bStopChangeEvent Then
        Exit Sub
    End If
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bParaDirty = True
End Sub

Private Sub FillNextParaList(iLevels As Integer)
    Dim xStyleRoot As String
    Dim i As Integer
    Dim iCount As Integer
    Dim xList As String
    Dim xNextPara() As String
    
    On Error GoTo ProcError
    
    Set m_xarNextPara = New xArray
    
'   these are from doc prop in firm's mpNumbers.sty
    If g_xNextParaStyles(0) <> "" Then
        For i = 0 To UBound(g_xNextParaStyles)
            xList = xList & xTranslateBuiltInStyle(g_xNextParaStyles(i))
            If i < UBound(g_xNextParaStyles) Then
                xList = xList & ","
            End If
        Next i
    End If

'   add num continue style if this
'   is an upgrade to v8.0
    If g_iUpgradeFrom = 80 Then
        xList = xList & "," & "Num Continue"
    End If

    xStyleRoot = xGetStyleRoot(g_oCurScheme.Name)

'   add numbering styles
    For i = 1 To iLevels
        xList = xList & "," & xStyleRoot & "_L" & i
    Next i
    
'   these are offered for every scheme
    For i = 1 To iLevels
        xList = xList & "," & xStyleRoot & " Cont " & i
    Next i

'   get array from string
    mpBase.xStringToArray xList, xNextPara()
    
'   dimension xArray
    m_xarNextPara.ReDim 0, UBound(xNextPara()), 0, 1
    
'   get xArray
    For i = 0 To UBound(xNextPara())
        m_xarNextPara(i, 0) = xNextPara(i, 0)
        m_xarNextPara(i, 1) = xNextPara(i, 0)
    Next i
    
'   fill next paragraph list
    With Me.cmbNextParagraph
        .Array = m_xarNextPara
        ResizeTDBCombo Me.cmbNextParagraph, UBound(xNextPara())
    End With
    
    Exit Sub
ProcError:
    RaiseError "frmEditSchmeFrench.FillNextParaList"
    Exit Sub
End Sub

Private Sub spnContLeftIndent_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub

Private Sub spnContLeftIndent_LostFocus()
    m_xPrevCtl = "spnContLeftIndent"
End Sub

Private Sub spnContLeftIndent_Validate(Cancel As Boolean)
    If Not Me.spnContLeftIndent.IsValid Then _
        Cancel = True
End Sub

Private Sub spnContRightIndent_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub

Private Sub spnContRightIndent_LostFocus()
    m_xPrevCtl = "spnContRightIndent"
End Sub

Private Sub spnContRightIndent_Validate(Cancel As Boolean)
    If Not Me.spnContRightIndent.IsValid Then _
        Cancel = True
End Sub

Private Sub cmbSpecial_ItemChange()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
    
    If Not m_bLevelChanged Then
        m_bLevelChanged = True
        If cmbSpecial.Text = "(aucun)" Then
            Me.spnBy.Value = 0
        ElseIf Me.spnBy.Value = 0 Then
            Me.spnBy.Value = 36
        End If
        m_bLevelChanged = False
    End If
End Sub

Private Sub cmbSpecial_LostFocus()
    m_xPrevCtl = "cmbSpecial"
End Sub

Private Sub spnBy_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
    
    If Not m_bLevelChanged Then
        m_bLevelChanged = True
        If Me.cmbSpecial.Text = "(aucun)" And _
                Me.spnBy.DisplayText <> "" Then
            Me.cmbSpecial.BoundText = "1�re ligne"
        End If
        m_bLevelChanged = False
    End If
End Sub

Private Sub spnBy_SpinUp()
    m_bLevelChanged = True
    If Me.cmbSpecial.Text = "(aucun)" Then
        Me.cmbSpecial.BoundText = "1�re ligne"
    End If
    m_bLevelChanged = False
End Sub

Private Sub spnBy_LostFocus()
    m_xPrevCtl = "spnBy"
End Sub

Private Sub spnContSpaceBefore_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub

Private Sub spnContSpaceBefore_LostFocus()
    m_xPrevCtl = "spnContSpaceBefore"
End Sub

Private Sub spnContSpaceAfter_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub

Private Sub spnContSpaceAfter_LostFocus()
    m_xPrevCtl = "spnContSpaceAfter"
End Sub

Private Sub cmbContLineSpacing_ItemChange()
    Dim iSpacing As WdLineSpacing
    
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True

    iSpacing = Me.cmbContLineSpacing.Bookmark
    Me.lblContAt.Enabled = (iSpacing > 2)
    With Me.spnContAt
        .Enabled = (iSpacing > 2)
        .AppendSymbol = (iSpacing <> wdLineSpaceMultiple)
        If iSpacing = wdLineSpaceMultiple Then
            .MinValue = 0.5
            .IncrementValue = 0.5
        Else
            .MinValue = 1
            .IncrementValue = 1
        End If
        If Not m_bLevelChanged Then
            Select Case iSpacing
                Case wdLineSpaceSingle, wdLineSpaceAtLeast, wdLineSpaceExactly
                    .Value = 12
                Case wdLineSpace1pt5
                    .Value = 18
                Case wdLineSpaceDouble
                    .Value = 24
                Case wdLineSpaceMultiple
                    .Value = 3
            End Select
            If iSpacing < 3 Then
                .DisplayText = ""
            Else
                .Refresh
            End If
        End If
    End With
End Sub

Private Sub cmbContLineSpacing_LostFocus()
    m_xPrevCtl = "cmbContLineSpacing"
End Sub

Private Sub chkContKeepLinesTogether_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub
Private Sub chkContKeepLinesTogether_LostFocus()
    m_xPrevCtl = "chkContKeepLinesTogether"
End Sub
Private Sub chkContKeepWithNext_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub
Private Sub chkContKeepWithNext_LostFocus()
    m_xPrevCtl = "chkContKeepWithNext"
End Sub
Private Sub chkContWidowOrphan_Click()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub
Private Sub chkContWidowOrphan_LostFocus()
    m_xPrevCtl = "chkContWidowOrphan"
End Sub

Private Sub spnContAt_Change()
    If ActiveDocument <> g_docPreview Then _
        g_docPreview.Activate
    m_bNextParaDirty = True
End Sub

Private Sub spnContAt_LostFocus()
    m_xPrevCtl = "spnContAt"
End Sub

Private Function iMapNumberStyle_MPToWord(iConst As Integer) As Integer
    Dim i As Integer
    
    Select Case iConst
        Case mpListNumberStyleNone
            i = wdListNumberStyleNone
        Case mpListNumberStyleArabic
            i = wdListNumberStyleArabic
        Case mpListNumberStyleUppercaseRoman
            i = wdListNumberStyleUppercaseRoman
        Case mpListNumberStyleLowercaseRoman
            i = wdListNumberStyleLowercaseRoman
        Case mpListNumberStyleUppercaseLetter
            i = wdListNumberStyleUppercaseLetter
        Case mpListNumberStyleLowercaseLetter
            i = wdListNumberStyleLowercaseLetter
        Case mpListNumberStyleOrdinal
            i = wdListNumberStyleOrdinal
        Case mpListNumberStyleCardinalText
            i = wdListNumberStyleCardinalText
        Case mpListNumberStyleOrdinalText
            i = wdListNumberStyleOrdinalText
        Case mpListNumberStyleArabicLZ
            i = wdListNumberStyleArabicLZ
        Case mpListNumberStyleArabicLZ2
            i = 62
        Case mpListNumberStyleArabicLZ3
            i = 63
        Case mpListNumberStyleArabicLZ4
            i = 64
        Case mpListNumberStyleBullet
            i = wdListNumberStyleBullet
        Case Else
            'supplemental style - get index from array
            i = CInt(g_xSupplementalNumberStyles(iConst - 17))
    End Select
    
    iMapNumberStyle_MPToWord = i
End Function

Private Function iMapNumberStyle_WordToMP(iConst As WdListNumberStyle) As Integer
    Dim i As Integer
    Dim iCount As Integer
    Dim j As Integer
    
    Select Case iConst
        Case wdListNumberStyleNone
            i = mpListNumberStyleNone
        Case wdListNumberStyleArabic
            i = mpListNumberStyleArabic
        Case wdListNumberStyleUppercaseRoman
            i = mpListNumberStyleUppercaseRoman
        Case wdListNumberStyleLowercaseRoman
            i = mpListNumberStyleLowercaseRoman
        Case wdListNumberStyleUppercaseLetter
            i = mpListNumberStyleUppercaseLetter
        Case wdListNumberStyleLowercaseLetter
            i = mpListNumberStyleLowercaseLetter
        Case wdListNumberStyleOrdinal
            i = mpListNumberStyleOrdinal
        Case wdListNumberStyleCardinalText
            i = mpListNumberStyleCardinalText
        Case wdListNumberStyleOrdinalText
            i = mpListNumberStyleOrdinalText
        Case wdListNumberStyleArabicLZ
            i = mpListNumberStyleArabicLZ
        Case 62
            i = mpListNumberStyleArabicLZ2
        Case 63
            i = mpListNumberStyleArabicLZ3
        Case 64
            i = mpListNumberStyleArabicLZ4
        Case wdListNumberStyleBullet
            i = mpListNumberStyleBullet
        Case wdListNumberStyleLegal
            i = mpListNumberStyleLegal
        Case wdListNumberStyleLegalLZ
            i = mpListNumberStyleLegalLZ
        Case Else
            'supplemental style - get index from array
            iCount = UBound(g_xSupplementalNumberStyles)
            For j = 0 To iCount
                If g_xSupplementalNumberStyles(j) = CStr(iConst) Then
                    i = j + 17
                    Exit For
                End If
            Next j
            
            'if not in array, add now
            If i = 0 Then
                If g_xSupplementalNumberStyles(0) <> "" Then _
                    iCount = iCount + 1
                ReDim Preserve g_xSupplementalNumberStyles(iCount)
                g_xSupplementalNumberStyles(iCount) = CStr(iConst)
 'GLOG : 5445 : ceh - needs to be tested
'                Me.cmbNumberStyle.AddItem GetNumStyleDisplayName(iConst)
                AddItemToTDBCombo Me.cmbNumberStyle, GetNumStyleDisplayName(iConst)
                i = iCount + 17
            End If
    End Select
    
    iMapNumberStyle_WordToMP = i
End Function

Private Function GetNumStyleDisplayName(iStyle As WdListNumberStyle) As String
    Dim xDisplayName As String
    
    'we need to use integers because the supplemental styles
    'were not in the Word 8 library
    Select Case iStyle
        Case 20
            xDisplayName = "Aiueo"
        Case 12
            xDisplayName = "Aiueo Half Width"
        Case WdListNumberStyle.wdListNumberStyleArabic
            xDisplayName = "1, 2, 3, ..."
        Case 46
            xDisplayName = "Arabic 1"
        Case 48
            xDisplayName = "Arabic 2"
        Case 14
            xDisplayName = "Arabic Full Width"
        Case WdListNumberStyle.wdListNumberStyleArabicLZ
            xDisplayName = "01, 02, ..."
        Case 62
            xDisplayName = "001, 002, ..."
        Case 63
            xDisplayName = "0001, 0002, ..."
        Case 64
            xDisplayName = "00001, 00002, ..."
        Case WdListNumberStyle.wdListNumberStyleBullet
            xDisplayName = "Puce"
        Case WdListNumberStyle.wdListNumberStyleCardinalText
            xDisplayName = "Un, Deux, ..."
            xDisplayName = ""
        Case 25
            xDisplayName = "Chosung"
        Case 24
            xDisplayName = "Ganada"
        Case 26
            xDisplayName = "GB Numeric 1"
        Case 27
            xDisplayName = "GB Numeric 2"
        Case 28
            xDisplayName = "GB Numeric 3"
        Case 29
            xDisplayName = "GB Numeric 4"
        Case 43
            xDisplayName = "Hangul"
        Case 44
            xDisplayName = "Hanja"
        Case 41
            xDisplayName = "Hanja Read"
        Case 42
            xDisplayName = "Hanja Read Digit"
        Case 45
            xDisplayName = "Hebrew 1"
        Case 47
            xDisplayName = "Hebrew 2"
        Case 51
            xDisplayName = "Hindi Arabic"
        Case 52
            xDisplayName = "Hindi Cardinal Text"
        Case 49
            xDisplayName = "Hindi Letter 1"
        Case 50
            xDisplayName = "Hindi Letter 2"
        Case 21
            xDisplayName = "Iroha"
        Case 13
            xDisplayName = "Iroha Half Width"
        Case 10
            xDisplayName = "Kanji"
        Case 11
            xDisplayName = "Kanji Digit"
        Case 16
            xDisplayName = "KanjiTraditional"
        Case 17
            xDisplayName = "KanjiTraditional 2"
        Case WdListNumberStyle.wdListNumberStyleLegal
            xDisplayName = "Legal"
        Case WdListNumberStyle.wdListNumberStyleLegalLZ
            xDisplayName = "Legal LZ"
        Case WdListNumberStyle.wdListNumberStyleLowercaseLetter
            xDisplayName = "a, b, c, ..."
        Case WdListNumberStyle.wdListNumberStyleLowercaseRoman
            xDisplayName = "i, ii, iii, ..."
        Case 58
            xDisplayName = "Lowercase Russian"
        Case WdListNumberStyle.wdListNumberStyleNone
            xDisplayName = "(pas de num�ro)"
        Case 18
            xDisplayName = "Number In Circle"
        Case WdListNumberStyle.wdListNumberStyleOrdinal
            xDisplayName = "1er, 2e, ..."
        Case WdListNumberStyle.wdListNumberStyleOrdinalText
            xDisplayName = "Premier, Deuxi�me, ..."
        Case 249
            xDisplayName = "Picture Bullet"
        Case 37
            xDisplayName = "Simplified Chinese 1"
        Case 38
            xDisplayName = "Simplified Chinese 2"
        Case 39
            xDisplayName = "Simplified Chinese 3"
        Case 40
            xDisplayName = "Simplified Chinese 4"
        Case 54
            xDisplayName = "Thai Arabic"
        Case 55
            xDisplayName = "Thai Cardinal Text"
        Case 53
            xDisplayName = "Thai Letter"
        Case 33
            xDisplayName = "Traditional Chinese 1"
        Case 34
            xDisplayName = "Traditional Chinese 2"
        Case 35
            xDisplayName = "Traditional Chinese 3"
        Case 36
            xDisplayName = "Traditional Chinese 4"
        Case WdListNumberStyle.wdListNumberStyleUppercaseLetter
            xDisplayName = "A, B, C, ..."
        Case WdListNumberStyle.wdListNumberStyleUppercaseRoman
            xDisplayName = "I, II, III, ..."
        Case 59
            xDisplayName = "Uppercase Russian"
        Case 56
            xDisplayName = "Vietnamese Cardinal Text"
        Case 30
            xDisplayName = "Zodiac 1"
        Case 31
            xDisplayName = "Zodiac 2"
        Case 32
            xDisplayName = "Zodiac 3"
    End Select
    
    GetNumStyleDisplayName = xDisplayName
End Function

Private Function bCreateLevel(xScheme As String, iLevel As Integer)
    Dim xNumberFormat As String
    Dim iNumberStyle As Integer
    Dim xStyle As String
    Dim xNextLevel As String
    Dim xHeadingSourceStyle As String
    Dim lNextColor As Long
    Dim lNextBold As Long
    Dim lNextAllCaps As Long
    Dim lNextUnderline As Long
    Dim lNextItalic As Long
    Dim lNextSmallCaps As Long
    Dim bIsBullet As Boolean
    Dim iHeadingFormat As Integer
    Dim iNextAlign As Integer
    Dim iNextLineSpace As Single
    Dim iNextLineSpaceRule As Integer
    Dim iNextSpaceBefore As Integer
    Dim iNextSpaceAfter As Integer
    Dim iNextKeepTogether As Integer
    Dim iNextKeepWithNext As Integer
    Dim iNextWidowControl As Integer
    Dim iNextPageBreak As Integer
    Dim iLevels As Integer
    Dim iLineSpaceRule As Integer
    Dim styContNew As Word.Style
    Dim xLT As String
    Dim xNextFontName As String
    Dim sNextFontSize As Single
    Dim sNextRightIndent As Single
    Dim xNextContFontName As String
    Dim sNextContFontSize As Single
    Dim xCont As String
    Dim xNextLevelCont As String
    Dim styContNext As Word.Style
    Dim xStyRoot As String
    Dim pfNormal As Word.ParagraphFormat
    Dim sNextFirstIndent As Single
    Dim sNextLeftIndent As Single
    Dim iContlevel As Integer
    Dim iAlignment As Integer
    Dim iTrailUnderline As Integer
    Dim iStyleIndex As Integer
    Dim oLL As Word.ListLevel
    
    On Error GoTo ProcError
    
    xStyle = xGetStyleName(xScheme, iLevel)
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)
    xLT = xGetFullLTName(xScheme)
    xStyRoot = xGetStyleRoot(xScheme)
    xCont = xStyRoot & " Cont " & iLevel
    
    With ActiveDocument
'       list template properties
        If m_bLevelDirty Then
'           modify list level props according to Edit dlg props
            Set oLL = .ListTemplates(xLT).ListLevels(iLevel)
            With oLL
'               get number style
                iStyleIndex = Me.cmbNumberStyle.SelectedItem
                If iStyleIndex > 9 Then _
                    iStyleIndex = iStyleIndex + m_iFixedDigitNumberingOffset
                If iStyleIndex > 13 Then
                    'supplemental style - add 2 because legal
                    'styles aren't on the list
                    iStyleIndex = iStyleIndex + 2
                End If
               iNumberStyle = iMapNumberStyle_MPToWord(iStyleIndex)
                    
'               modify num style as appropriate if legal style is checked
                If Me.chkLegalStyle Then
                    If iNumberStyle = wdListNumberStyleArabic Then
                        iNumberStyle = wdListNumberStyleLegal
                    Else
                        iNumberStyle = wdListNumberStyleLegalLZ
                    End If
                End If
                
'               reformat rtf value
                xNumberFormat = xGetNumFormat(m_xCode, _
                    Me.rtfNumberFormat.Text, Me.chkNonbreakingSpaces)

'               clear out existing format/style to avoid
'               incompatibilities - ie code errs when number format
'               has a number placeholder and an attempt is made to
'               set the style to bullets, etc.
'               12/21/00 - don't overwrite preexisting bullet
'                If (iNumberStyle <> wdListNumberStyleBullet) Or _
'                        (.NumberStyle <> wdListNumberStyleBullet) Then
                    .NumberFormat = ""
                    .NumberStyle = wdListNumberStyleArabic
                    .NumberStyle = iNumberStyle
                    If .NumberStyle <> wdListNumberStyleBullet Then
                        .NumberFormat = xNumberFormat
                    Else
                        .NumberFormat = m_xBulletCharNum
                    End If
'                End If
                
'               set start at
                If Me.txtStartAt.Text = "0 " Then
'                   we don't allow user to set this to 0, but it
'                   may have been inherited from Word
                    .StartAt = 0
                ElseIf Me.txtStartAt.Text <> "" Then
                    .StartAt = Me.udStartAt.Value
                Else
                    .StartAt = 1
                End If
                
                'set reset on higher
                If iLevel > 1 Then
                    If (Me.chkReset = 0) Or (g_iWordVersion = mpWordVersion_97) Then
                        'this was a boolean property in Word 97, so our checkbox is
                        'sufficient; otherwise, it's sufficient only to turn off
                        .ResetOnHigher = Me.chkReset
                    ElseIf .ResetOnHigher = False Then
                        'if checked in another Word version, only set to True if value
                        'was previously False; otherwise, preserve existing setting
                        .ResetOnHigher = True
                    End If
                End If
                
'               turn off error checking here to prevent messages
'               when data is invalid - this is handled by the
'               interface, and brings up unnecessary msgs because
'               of the relationship between validation and preview refresh
                On Error Resume Next
                .NumberPosition = Me.spnNumberPosition.Value
                .TextPosition = Me.spnTextPosition.Value
                .TabPosition = Me.spnNumberPosition.Value + _
                    Me.spnTabPosition.Value
                On Error GoTo 0
                
                If Me.chkRightAlign Then
                    .Alignment = wdListLevelAlignRight
                Else
                    .Alignment = wdListLevelAlignLeft
                End If
                
'               trailing characters are part of
'               number unless they're shift-returns
                Select Case Me.cmbTrailingChar.Bookmark
                    Case mpTrailingChar_Tab
                        .TrailingCharacter = wdTrailingTab
                    Case mpTrailingChar_Space
                        .TrailingCharacter = wdTrailingSpace
'                    Case mpTrailingChar_DoubleSpace
'                        .NumberFormat = .NumberFormat & Space(1)
'                        .TrailingCharacter = wdTrailingSpace
                    Case Else
                        .TrailingCharacter = wdTrailingNone
                End Select
            
                bIsBullet = (xNumberFormat = ChrW(&HB7))
                
                With .Font
'                    If bIsBullet Then
'                        .Name = "Symbol"
'                    ElseIf .Name = "Symbol" Then
'                        .Name = ActiveDocument.Styles(wdStyleNormal).Font.Name
'                    End If
                    
'                   get bold, italic, underline from Edit Scheme dlg
                    .AllCaps = ConvFromTripleState(Me.chkNumberCaps)
'                   correct Word 97 cardinal/ordinal text AllCaps
'                   issue (e.g. "ARTICLE One")
                    If (InStr(Word.Application.Version, "8.") <> 0) And _
                            (Me.optStyleFormats = True) And _
                            (Me.chkNumberCaps = 1) And _
                            (Me.chkTCCaps = 1) Then
                        .Reset
                    End If
                    .Bold = ConvFromTripleState(Me.chkNumberBold)
                    .Italic = ConvFromTripleState(Me.chkNumberItalic)

'                   different types of underline
'                   for different scenarios
                    If Me.cmbUnderlineStyle.SelectedItem >= 0 Then
                        .Underline = Me.cmbUnderlineStyle.SelectedItem
                    Else
                        .Underline = wdUnderlineNone
                    End If
                    
                    If g_bApplyHeadingColor Then
                        If Me.optDirectFormats Then
                            .ColorIndex = wdAuto
                        Else
                            .ColorIndex = iHeadingColor()
                        End If
                    End If
                    
                    If Me.cmbNumFontName.Text <> "-Non d�fini-" Then _
                        .Name = Me.cmbNumFontName.Text
                    If Me.cmbNumFontSize.Text <> "-Non d�fini-" Then
'                       font size is stored in array because it needs
'                       to be forced for preview in buffer doc
                        g_sNumberFontSizes(iLevel - 1) = _
                            CSng(Me.cmbNumFontSize.Text)
                    End If
                End With
            End With
        End If
        
'       style paragraph format
        If m_bParaDirty Then
'           save alignment and line spacing for next
'           para - then return these values after
'           setting values for cur para - this is
'           made necessary because the styles are
'           built hierarchically, but the UI treats
'           them as if they are not
            If iLevel < iLevels Then
                xNextLevel = xGetStyleName(xScheme, iLevel + 1)
                With ActiveDocument.Styles(xNextLevel).ParagraphFormat
                    iNextAlign = .Alignment
                    iNextLineSpaceRule = .LineSpacingRule
                    iNextLineSpace = .LineSpacing
                    iNextSpaceBefore = .SpaceBefore
                    iNextSpaceAfter = .SpaceAfter
                    iNextKeepTogether = .KeepTogether
                    iNextKeepWithNext = .KeepWithNext
                    iNextWidowControl = .WidowControl
                    iNextPageBreak = .PageBreakBefore
                    sNextRightIndent = .RightIndent
                    sNextLeftIndent = .LeftIndent
                    sNextFirstIndent = .FirstLineIndent
                End With
            End If
            
            With .Styles(xStyle).ParagraphFormat
'               alignment
                If Me.cmbTextAlign.Bookmark = mpBaseAlignmentOnNormal Then
                    .Alignment = wdAlignParagraphLeft
                Else
                    .Alignment = Me.cmbTextAlign.Bookmark
                End If
                
'               change line spacing only if list index has a value and
'               it doesn't match current line spacing
                If Not IsNull(Me.cmbLineSpacing.Bookmark) Then
                    iLineSpaceRule = Me.cmbLineSpacing.Bookmark
                    .LineSpacingRule = iLineSpaceRule
                    If iLineSpaceRule = wdLineSpaceMultiple Then
                        .LineSpacing = LinesToPoints(Me.spnAt.Value)
                    Else
                        .LineSpacing = Me.spnAt.Value
                    End If
'                    Set pfNormal = ActiveDocument.Styles(wdStyleNormal) _
'                        .ParagraphFormat
'                    If (pfNormal.LineSpacingRule = wdLineSpaceExactly) And _
'                            (.LineSpacingRule = wdLineSpaceExactly) Then
''                       exact line spacing is now set relative to Normal
'                        iLineSpaceRule = ConvertLineSpace(.LineSpacingRule, _
'                            .LineSpacing, pfNormal.LineSpacing)
'                    Else
'                        iLineSpaceRule = ConvertLineSpace(.LineSpacingRule, _
'                            .LineSpacing)
'                    End If
'
'                    If Me.cmbLineSpacing.ListIndex <> iLineSpaceRule Then
'                        .LineSpacingRule = Me.cmbLineSpacing.ListIndex
'                    End If
                End If
                .SpaceBefore = Me.spnSpaceBefore.Value
                .SpaceAfter = Me.spnSpaceAfter.Value
                .KeepTogether = -1 * Me.chkKeepLinesTogether
                .KeepWithNext = -1 * Me.chkKeepWithNext
                .WidowControl = -1 * Me.chkWidowOrphan
                .PageBreakBefore = -1 * Me.chkPageBreak
                .RightIndent = Me.spnRightIndent.Value
                
'                style indents now takes priority over LT positions
                .LeftIndent = Me.spnTextPosition.Value
                .FirstLineIndent = Me.spnNumberPosition.Value - .LeftIndent
            End With
        
            If iLevel < iLevels Then
'               return next level to original
'               alignment/spacing
                With ActiveDocument.Styles(xNextLevel).ParagraphFormat
                    .Alignment = iNextAlign
                    .LineSpacingRule = iNextLineSpaceRule
                    .LineSpacing = iNextLineSpace
                    .SpaceAfter = iNextSpaceAfter
                    .SpaceBefore = iNextSpaceBefore
                    .KeepTogether = -1 * Abs(iNextKeepTogether)
                    .KeepWithNext = -1 * Abs(iNextKeepWithNext)
                    .WidowControl = -1 * Abs(iNextWidowControl)
                    .PageBreakBefore = -1 * Abs(iNextPageBreak)
                    .RightIndent = sNextRightIndent
                    .LeftIndent = sNextLeftIndent
                    .FirstLineIndent = sNextFirstIndent
                End With
            End If
                    
'           next paragraph style
            Err.Clear
            On Error Resume Next
            Set styContNew = Nothing
            Set styContNew = .Styles(Me.cmbNextParagraph.Text)
            On Error GoTo 0
            If styContNew Is Nothing Then
                iContlevel = Val(Right(Me.cmbNextParagraph.Text, 1))
                If (InStr(Me.cmbNextParagraph.Text, " Cont ") <> 0) And _
                        (iContlevel >= 1) And (iContlevel <= 9) Then
'                   MacPac proprietary cont style
                    CreateContStyles xScheme, iContlevel
                    Set styContNew = .Styles(Me.cmbNextParagraph.Text)
                Else
                    If g_bCreateUnlinkedStyles Then
                        '9.9.4010
                        Set styContNew = mdlCC.AddUnlinkedParagraphStyle(ActiveDocument, _
                            Me.cmbNextParagraph.Text)
                    Else
                        Set styContNew = .Styles.Add(Me.cmbNextParagraph.Text, _
                            wdStyleTypeParagraph)
                    End If
                    styContNew.Font = ActiveDocument.Styles(wdStyleNormal).Font
                End If
            End If
            If bIsHeadingStyle(xStyle) And _
                    InStr(styContNew.NameLocal, xStyRoot & "_L") Then
'               convert to heading style
                .Styles(xStyle).NextParagraphStyle = _
                    xTranslateHeadingStyle(Right(styContNew.NameLocal, 1))
            Else
                .Styles(xStyle).NextParagraphStyle = styContNew
            End If
            If g_iUpgradeFrom < 90 And styContNew = "Num Continue" Then
                styContNew.BaseStyle = wdStyleBodyText
                styContNew.ParagraphFormat = ActiveDocument.Styles(wdStyleBodyText).ParagraphFormat
            End If
        End If
        
'       heading format/style font format
        If m_bTCDirty Then
'           next level style is based on this level
'           so capture font attributes before changing
            If iLevel < iLevels Then
                xNextLevel = xGetStyleName(xScheme, iLevel + 1)
                With ActiveDocument.Styles(xNextLevel).Font
                    lNextColor = .ColorIndex
                    lNextBold = .Bold
                    lNextAllCaps = .AllCaps
                    lNextItalic = .Italic
                    lNextUnderline = .Underline
                    lNextSmallCaps = .SmallCaps
                    xNextFontName = .Name
                    sNextFontSize = .Size
                End With

'               "cont" style
'                If Me.chkNextParaFont Then
'                    xNextLevelCont = xStyRoot & " Cont " & (iLevel + 1)
'                    On Error Resume Next
'                    Set styContNext = ActiveDocument.Styles(xNextLevelCont)
'                    On Error GoTo 0
'                    If Not styContNext Is Nothing Then
'                        xNextContFontName = styContNext.Font.Name
'                        sNextContFontSize = styContNext.Font.Size
'                    End If
'                End If
            End If
                    
'           set bit for heading type
            If Me.optStyleFormats Then
                iHeadingFormat = iHeadingFormat Or _
                                 mpTCFormatField_Type
            End If
        
'           set bit for heading bold
            If Me.chkTCBold Then
                iHeadingFormat = iHeadingFormat Or _
                                 mpTCFormatField_Bold
            End If
        
'           set bit for heading all caps
            If Me.chkTCCaps Then
                iHeadingFormat = iHeadingFormat Or _
                                 mpTCFormatField_AllCaps
            End If
        
'           set bit for heading italic
            If Me.chkTCItalic Then
                iHeadingFormat = iHeadingFormat Or _
                                 mpTCFormatField_Italic
            End If
        
'           set bit for heading small caps
            If Me.chkTCSmallCaps Then
                iHeadingFormat = iHeadingFormat Or _
                                 mpTCFormatField_SmallCaps
            End If
        
'           set bit for heading small caps
            If Me.chkTCUnderline Then
                iHeadingFormat = iHeadingFormat Or _
                                 mpTCFormatField_Underline
            End If
        
'           save heading format
            lSetLevelProp xScheme, _
                          iLevel, _
                          mpNumLevelProp_HeadingFormat, _
                          CStr(iHeadingFormat), _
                          mpSchemeType_Document
                          
            With .Styles(xStyle).Font
                If Me.optDirectFormats Then
                    .Bold = False
                    .Italic = False
                    .AllCaps = False
                    .SmallCaps = False
                    .Underline = False
                    If g_bApplyHeadingColor Then _
                        .ColorIndex = wdAuto
                Else
'                   font formats are style-based,
'                   so modify style
                    .Bold = Me.chkTCBold
                    .Italic = Me.chkTCItalic
                    .AllCaps = Me.chkTCCaps
                    .SmallCaps = Me.chkTCSmallCaps
                    .Underline = Me.chkTCUnderline
                    If g_bApplyHeadingColor Then _
                        .ColorIndex = iHeadingColor()
                End If
                .Name = Me.cmbFontName.Text
                If Me.cmbFontSize.Text <> "-Non d�fini-" Then
                    .Size = Me.cmbFontSize.Text
                End If
                
'               "cont" style
'                If Me.chkNextParaFont Then
'                    Set styContNew = Nothing
'                    On Error Resume Next
'                    Set styContNew = ActiveDocument.Styles(xCont)
'                    On Error GoTo 0
'                    If Not styContNew Is Nothing Then
'                        styContNew.Font.Name = Me.cmbFontName.Text
'                        If Me.cmbFontSize.Text <> "-Non d�fini-" Then
'                            styContNew.Font.Size = Me.cmbFontSize.Text
'                        End If
'                    End If
'                End If
            End With

'           restore next level font attributes
            If iLevel < iLevels Then
                With ActiveDocument.Styles(xNextLevel).Font
                    .ColorIndex = lNextColor
                    .Bold = lNextBold
                    .AllCaps = lNextAllCaps
                    .Italic = lNextItalic
                    .Underline = lNextUnderline
                    .SmallCaps = lNextSmallCaps
                    .Name = xNextFontName
                    .Size = sNextFontSize
                End With

'               "cont" style
'                If Me.chkNextParaFont Then
'                    If Not styContNext Is Nothing Then
'                        styContNext.Font.Name = xNextFontName
'                        styContNext.Font.Size = sNextFontSize
'                    End If
'                End If
            End If
                    
        End If
        
'       store more level properties
        If m_bPropDirty Then
'           do trailing char
            lSetLevelProp xScheme, _
                          iLevel, _
                          mpNumLevelProp_TrailChr, _
                          Me.cmbTrailingChar.Bookmark, _
                          mpSchemeType_Document
            
'           do alignment
            iTrailUnderline = xGetLevelProp(xScheme, _
                                       iLevel, _
                                       mpNumLevelProp_TrailUnderline, _
                                       mpSchemeType_Document)
            If bBitwisePropIsTrue(iTrailUnderline, _
                    mpTrailUnderlineField_Underline) Then
                iAlignment = iAlignment Or mpTrailUnderlineField_Underline
            End If
            If bBitwisePropIsTrue(iTrailUnderline, _
                    mpTrailUnderlineField_AdjustContToNormal) Then
                iAlignment = iAlignment Or mpTrailUnderlineField_AdjustContToNormal
            End If
            If Me.cmbTextAlign.Bookmark = mpBaseAlignmentOnNormal Then
                iAlignment = iAlignment Or mpTrailUnderlineField_AdjustToNormal
            Else
                iAlignment = iAlignment Or mpTrailUnderlineField_HonorAlignment
            End If
            lSetLevelProp xScheme, _
                          iLevel, _
                          mpNumLevelProp_TrailUnderline, _
                          CStr(iAlignment), _
                          mpSchemeType_Document
        End If
        
    End With    'active document
    
    g_bSchemeIsDirty = True
    
    Exit Function
ProcError:
    RaiseError "frmEditSchemeFrench.bCreateLevel"
    Exit Function
End Function

Private Function bCreateContLevel(xScheme As String, iLevel As Integer) As Boolean
    Dim xCont As String
    Dim xNext As String
    Dim styCont As Word.Style
    Dim styNext As Word.Style
    Dim iAlign As WdParagraphAlignment
    Dim sFirstLineIndent As Single
    Dim sLeftIndent As Single
    Dim sRightIndent As Single
    Dim sLineSpace As Single
    Dim iLineSpaceRule As WdLineSpacing
    Dim iLineSpaceRuleNext As WdLineSpacing
    Dim sSpaceAfter As Single
    Dim sSpaceBefore As Single
    Dim bWidowCtl As Boolean
    Dim bKeepTogether As Boolean
    Dim bKeepWithNext As Boolean
    Dim bRestoreNext As Boolean
    Dim xFont As String
    Dim sSize As Single
    Dim xRoot As String
    Dim iProp As Integer
    
    On Error GoTo ProcError
    
    xRoot = xGetStyleRoot(xScheme)
    xCont = xRoot & " Cont " & iLevel
    
    If iLevel < 9 Then
        xNext = xRoot & " Cont " & iLevel + 1
        On Error Resume Next
        Set styNext = ActiveDocument.Styles(xNext)
        On Error GoTo ProcError
        If Not styNext Is Nothing Then
            bRestoreNext = True
            With styNext
                With .ParagraphFormat
                    iAlign = .Alignment
                    sFirstLineIndent = .FirstLineIndent
                    sLeftIndent = .LeftIndent
                    sRightIndent = .RightIndent
                    iLineSpaceRuleNext = .LineSpacingRule
                    sLineSpace = .LineSpacing
                    sSpaceAfter = .SpaceAfter
                    sSpaceBefore = .SpaceBefore
                    bWidowCtl = .WidowControl
                    bKeepTogether = .KeepTogether
                    bKeepWithNext = .KeepWithNext
                End With
                With .Font
                    xFont = .Name
                    sSize = .Size
                End With
            End With
        End If
    End If

'   modify level prop
    iProp = xGetLevelProp(xScheme, iLevel, _
        mpNumLevelProp_TrailUnderline, mpSchemeType_Document)
    If Me.cmbAlignment.SelectedItem = mpBaseAlignmentOnNormal Then
        iProp = iProp Or mpTrailUnderlineField_AdjustContToNormal
    Else
        iProp = iProp And (Not mpTrailUnderlineField_AdjustContToNormal)
    End If
    lSetLevelProp xScheme, iLevel, mpNumLevelProp_TrailUnderline, _
        CStr(iProp), mpSchemeType_Document
                  
'   edit style
    Set styCont = ActiveDocument.Styles(xCont)
    With styCont
'       paragraph
        With .ParagraphFormat
            If Me.cmbAlignment.SelectedItem = mpBaseAlignmentOnNormal Then
                .Alignment = wdAlignParagraphLeft
            Else
                .Alignment = Me.cmbAlignment.SelectedItem
            End If
            
            If Me.cmbSpecial.Text = "Retrait" Then
                .FirstLineIndent = -(Me.spnBy.Value)
                .LeftIndent = Me.spnContLeftIndent.Value - .FirstLineIndent
            Else
                .FirstLineIndent = Me.spnBy.Value
                .LeftIndent = Me.spnContLeftIndent.Value
            End If
            .RightIndent = Me.spnContRightIndent.Value
            iLineSpaceRule = Me.cmbContLineSpacing.Bookmark
            .LineSpacingRule = iLineSpaceRule
            If iLineSpaceRule = wdLineSpaceMultiple Then
                .LineSpacing = LinesToPoints(Me.spnContAt.Value)
            Else
                .LineSpacing = Me.spnContAt.Value
            End If
            .SpaceAfter = Me.spnContSpaceAfter.Value
            .SpaceBefore = Me.spnContSpaceBefore.Value
            .WidowControl = CBool(Me.chkContWidowOrphan)
            .KeepWithNext = CBool(Me.chkContKeepWithNext)
            .KeepTogether = CBool(Me.chkContKeepLinesTogether)
        End With
        
'       font
        With .Font
            .Name = Me.cmbContFontName.Text
            If Me.cmbContFontSize.Text <> "-Non d�fini-" Then
                .Size = Me.cmbContFontSize.Text
            End If
        End With
    End With
    
'   restore next cont style
    If bRestoreNext Then
        With styNext
            With .ParagraphFormat
                .Alignment = iAlign
                .FirstLineIndent = sFirstLineIndent
                .LeftIndent = sLeftIndent
                .RightIndent = sRightIndent
                .LineSpacingRule = iLineSpaceRuleNext
                .LineSpacing = sLineSpace
                .SpaceAfter = sSpaceAfter
                .SpaceBefore = sSpaceBefore
                .WidowControl = bWidowCtl
                .KeepWithNext = bKeepWithNext
                .KeepTogether = bKeepTogether
            End With
            With .Font
                .Name = xFont
                .Size = sSize
            End With
        End With
    End If
    
    Exit Function
ProcError:
    RaiseError "frmEditSchemeFrench.bCreateContLevel"
    Exit Function
End Function

