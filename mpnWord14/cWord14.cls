VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cWord14"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function GetCompatibilityMode(ByVal oDocument As Word.Document) As Integer
    On Error GoTo ProcError
    GetCompatibilityMode = oDocument.CompatibilityMode
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpnWord14.cWord14.GetCompatibilityMode", Err.Description
    Exit Function
End Function

Public Sub Convert(ByVal oDoc As Word.Document)
    On Error GoTo ProcError
    oDoc.Convert
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpnWord14.cWord14.Convert", Err.Description
    Exit Sub
End Sub



