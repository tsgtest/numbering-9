VERSION 5.00
Begin VB.Form frmTOCScheme 
   Caption         =   "Set Default TOC Scheme"
   ClientHeight    =   2985
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5520
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   5520
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstTOCScheme 
      Height          =   2400
      Left            =   120
      TabIndex        =   4
      Top             =   390
      Width           =   2145
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   3165
      TabIndex        =   1
      Top             =   2430
      Width           =   1100
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   4305
      TabIndex        =   0
      Top             =   2430
      Width           =   1100
   End
   Begin VB.Label lblMsg 
      Caption         =   "###"
      Height          =   1560
      Left            =   2550
      TabIndex        =   3
      Top             =   405
      Width           =   2835
   End
   Begin VB.Label lblTOCScheme 
      Caption         =   "&TOC Scheme:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   135
      TabIndex        =   2
      ToolTipText     =   "Apply the selected Table of Contents format to documents that use this numbering scheme:"
      Top             =   165
      Width           =   1230
   End
End
Attribute VB_Name = "frmTOCScheme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancelled As Boolean

Private Sub cmdCancel_Click()
    Me.Hide
    DoEvents
    Cancelled = True
End Sub

Private Sub cmdOK_Click()
    Me.Hide
    DoEvents
    Cancelled = False
End Sub

Private Sub Form_Activate()
    With Me.lstTOCScheme
        If .ListIndex > -1 Then
            .SetFocus
        End If
    End With
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Cancelled = True
    For i = LBound(g_xTOCSchemes) To UBound(g_xTOCSchemes)
        Me.lstTOCScheme.AddItem g_xTOCSchemes(i, 0)
    Next i
End Sub
