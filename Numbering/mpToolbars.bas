Attribute VB_Name = "mdlToolbars"
Option Explicit

Function bSetNumberingTbrEnable(bEnable As Boolean, _
                                Optional xScheme As String) As Boolean
    Dim ctlP As CommandBarControl
    
    For Each ctlP In CommandBars(mpNumberingToolbar).Controls
        ctlP.Enabled = bEnable
    Next
    
'   do menus too.
    On Error Resume Next
    For Each ctlP In CommandBars("Menu Bar") _
            .Controls("Numbering").Controls
        ctlP.Enabled = bEnable
    Next
    On Error GoTo 0
    
'   set numbering insertion buttons
    bSetNumberingBtnsEnable xScheme
    
'   mark startup templates as saved
    MarkStartupSaved
End Function

Public Function bSetNumberingBtnsEnable(xScheme As String) As Boolean
'enables/disables appropriate
'"Insert Number" toolbar btns

    Dim xStyle As String
    Dim i As Integer
    Dim styNumbering As Word.Style
    Dim xStyleRoot As String
    Dim iLevels As Integer
    
    On Error Resume Next
    
'   do only if doc change event is enabled
    If Not g_bDocumentChange Then _
        Exit Function
    
    xStyleRoot = xGetStyleRoot(xScheme)
        
    With Word.Application
        If xScheme = Empty Then
'           disable all insertion btns
            For i = 1 To 9
                .CommandBars(mpNumberingToolbar) _
                    .Controls(1 + i).Enabled = False
                 CommandBars("Menu Bar").Controls("Numbering") _
                    .Controls("Insert Level") _
                    .Controls(i).Enabled = False
           Next i
        Else
'           enable all appropriate insertion buttons
            iLevels = iGetLevels(xScheme, mpSchemeType_Document)
            For i = 1 To 9
                With .CommandBars(mpNumberingToolbar).Controls(1 + i)
                    If i > iLevels Then
                        .Enabled = False
                        CommandBars("Menu Bar").Controls("Numbering") _
                            .Controls("Insert Level") _
                            .Controls(i).Enabled = False
                    Else
                        .Enabled = True
                        CommandBars("Menu Bar").Controls("Numbering") _
                            .Controls("Insert Level") _
                            .Controls(i).Enabled = True
                    End If
                End With
            Next i
        End If
        
'       mark startup templates as saved
        MarkStartupSaved
    End With
End Function



