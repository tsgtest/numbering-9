VERSION 5.00
Begin VB.Form frmLongName 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Scheme Long Name"
   ClientHeight    =   1530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3525
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1530
   ScaleWidth      =   3525
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   1020
      TabIndex        =   3
      Top             =   1020
      Width           =   1100
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   2205
      TabIndex        =   2
      Top             =   1020
      Width           =   1100
   End
   Begin VB.TextBox txtAlias 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   165
      MaxLength       =   15
      TabIndex        =   0
      Top             =   450
      Width           =   3135
   End
   Begin VB.Label lblAlias 
      Caption         =   "Long &Name (up to 15 characters):"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   180
      TabIndex        =   1
      Top             =   225
      Width           =   2760
   End
End
Attribute VB_Name = "frmLongName"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Cancelled As Boolean

Private Sub cmdCancel_Click()
    Cancelled = True
    Me.Hide
End Sub

Private Sub cmdOK_Click()
    Cancelled = False
    Me.Hide
End Sub

Private Sub Form_Load()
    Cancelled = True
End Sub
