VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CMacros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CMacros Collection Class
'   created 4/20/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   CMacros Collection object.
'**********************************************************
    Enum xarCols
        xarCols_Caption = 0
        xarCols_Command = 1
        xarCols_Description = 2
        xarCols_Category = 3
    End Enum

    Dim m_xarMacros As xArray
'**********************************************************
'PROPERTIES
'**********************************************************

'**********************************************************
'METHODS
'**********************************************************
Private m_xarMacros As xArray
'**********************************************************

Public Sub Add(ltgP As mpCI.Listing)
    m_xarMacros.Insert 1, m_xarMacros.UpperBound(1) + 1
    With m_xarMacros
        .Value(.UpperBound(1), xarCols_DisplayName) = ltgP.Name
        .Value(.UpperBound(1), xarCols_ID) = ltgP.Id
        .Value(.UpperBound(1), xarCols_Parent) = ltgP.Parent
    End With
    RaiseEvent OnAdd(ltgP)
End Sub

Public Sub Delete(Index As Long)
    m_xarMacros.Delete 1, Index
    RaiseEvent OnDelete
End Sub

Public Sub DeleteAll()
    m_xarMacros.Clear
End Sub

Public Function Count() As Long
    On Error Resume Next
    Count = m_xarMacros.Count(1)
End Function

Public Function Item(Index As Long) As mpCI.Listing
    Dim ltgP As mpCI.Listing
    Set ltgP = New mpCI.Listing
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    Index = ciMax(CDbl(Index), 0)
    With m_xarMacros
        ltgP.Name = .Value(Index, xarCols_DisplayName)
        ltgP.Id = .Value(Index, xarCols_ID)
        On Error Resume Next
'       only ODBC backend will have this
        ltgP.Source = .Value(Index, xarCols_Source)
        ltgP.Parent = .Value(Index, xarCols_Parent)
    End With
    
    Set Item = ltgP
    Exit Function
ProcError:
    Application.Error.Raise ciError_ListingItemMethodFailed, ""
End Function

'**********************************************************
'Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_xarMacros = New xArray
End Sub

Private Sub Class_Terminate()
    Set m_xarMacros = Nothing
End Sub
