VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmInsertTOCOld 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Insert Table of Contents"
   ClientHeight    =   5280
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   4680
   Icon            =   "frmTOCOld.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5280
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   4600
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   75
      Width           =   4580
      _ExtentX        =   8065
      _ExtentY        =   8128
      _Version        =   393216
      Style           =   1
      TabHeight       =   609
      TabCaption(0)   =   "&Main"
      TabPicture(0)   =   "frmTOCOld.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraContent"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Options"
      TabPicture(1)   =   "frmTOCOld.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTOCScheme"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "fraSchemes"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "fraTOCBody"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "&Advanced"
      TabPicture(2)   =   "frmTOCOld.frx":05C2
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "chkHyperlinks"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "btnEditHeaderFooter"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "cmdEdit"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "fraStyleList"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).ControlCount=   4
      Begin VB.CheckBox chkHyperlinks 
         Caption         =   "&Insert TOC entries as hyperlinks"
         Height          =   270
         Left            =   -74800
         TabIndex        =   32
         Top             =   3185
         Width           =   4000
      End
      Begin VB.CommandButton btnEditHeaderFooter 
         Caption         =   "&Header/Footer Format..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -74800
         TabIndex        =   33
         Top             =   3650
         Width           =   1875
      End
      Begin VB.CommandButton cmdEdit 
         Caption         =   "&Edit Custom TOC Scheme..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -72780
         TabIndex        =   34
         Top             =   3650
         Width           =   2160
      End
      Begin VB.Frame fraStyleList 
         Caption         =   "Include the Following Available Styles"
         Height          =   2576
         Left            =   -74800
         TabIndex        =   30
         Top             =   490
         Width           =   4195
         Begin TrueDBList60.TDBList lstDesignate 
            Height          =   2145
            Left            =   150
            OleObjectBlob   =   "frmTOCOld.frx":05DE
            TabIndex        =   31
            Top             =   285
            Width           =   3855
         End
      End
      Begin VB.Frame fraTOCScheme 
         Caption         =   "Format"
         Height          =   990
         Left            =   -74895
         TabIndex        =   17
         Top             =   440
         Width           =   4385
         Begin VB.ComboBox cbxTOCScheme 
            Height          =   315
            ItemData        =   "frmTOCOld.frx":2CAE
            Left            =   2145
            List            =   "frmTOCOld.frx":2CB0
            Style           =   2  'Dropdown List
            TabIndex        =   19
            Top             =   225
            Width           =   1800
         End
         Begin VB.OptionButton optTOCStyles 
            Caption         =   "Update TOC &Styles:"
            Height          =   300
            Index           =   0
            Left            =   195
            TabIndex        =   18
            Top             =   240
            Value           =   -1  'True
            Width           =   1750
         End
         Begin VB.OptionButton optTOCStyles 
            Caption         =   "Keep &Current TOC Styles"
            Height          =   300
            Index           =   1
            Left            =   195
            TabIndex        =   20
            Top             =   615
            Width           =   2200
         End
      End
      Begin VB.Frame fraSchemes 
         Caption         =   "Include"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1630
         Left            =   -74895
         TabIndex        =   26
         Top             =   2870
         Width           =   4385
         Begin VB.OptionButton optInclude 
            Caption         =   "&Designate Styles Individually (Advanced tab)"
            Height          =   195
            Index           =   1
            Left            =   195
            TabIndex        =   29
            Top             =   1315
            Width           =   3500
         End
         Begin VB.OptionButton optInclude 
            Caption         =   "&Entries From The Following Schemes"
            Height          =   195
            Index           =   0
            Left            =   195
            TabIndex        =   27
            Top             =   285
            Value           =   -1  'True
            Width           =   3500
         End
         Begin TrueDBList60.TDBList lstStyles 
            Height          =   675
            Left            =   450
            OleObjectBlob   =   "frmTOCOld.frx":2CB2
            TabIndex        =   28
            Top             =   550
            Width           =   3550
         End
      End
      Begin VB.Frame fraTOCBody 
         Caption         =   "Other"
         Height          =   1325
         Left            =   -74895
         TabIndex        =   21
         Top             =   1490
         Width           =   4385
         Begin VB.CheckBox chkTwoColumn 
            Caption         =   "&Use 2-Column Format"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   195
            TabIndex        =   22
            Top             =   225
            Width           =   3500
         End
         Begin VB.ComboBox cbxScheduleStyles 
            Height          =   315
            ItemData        =   "frmTOCOld.frx":537F
            Left            =   2145
            List            =   "frmTOCOld.frx":5381
            Style           =   2  'Dropdown List
            TabIndex        =   25
            Top             =   880
            Width           =   1800
         End
         Begin VB.CheckBox chkApplyTOC9 
            Caption         =   "Apply TOC &9 Style to:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   195
            TabIndex        =   24
            Top             =   900
            Width           =   1900
         End
         Begin VB.CheckBox chkApplyTOCScheme 
            Caption         =   "Apply TOC &Scheme:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   195
            TabIndex        =   37
            Top             =   3285
            Value           =   1  'Checked
            Visible         =   0   'False
            Width           =   1830
         End
         Begin VB.CheckBox chkApplyManualFormatsToTOC 
            Caption         =   "Appl&y Direct Heading Formats to TOC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   195
            TabIndex        =   23
            Top             =   560
            Width           =   3500
         End
      End
      Begin VB.Frame fraContent 
         Caption         =   "Content"
         Height          =   4060
         Left            =   105
         TabIndex        =   1
         Top             =   440
         Width           =   4385
         Begin VB.CommandButton btnSet 
            Caption         =   "Set"
            Height          =   330
            Left            =   3500
            TabIndex        =   15
            Top             =   2700
            Width           =   570
         End
         Begin VB.ComboBox cbxMinLevel 
            Height          =   315
            ItemData        =   "frmTOCOld.frx":5383
            Left            =   1320
            List            =   "frmTOCOld.frx":53A2
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   370
            Width           =   570
         End
         Begin VB.ComboBox cbxMaxLevel 
            Height          =   315
            ItemData        =   "frmTOCOld.frx":53C1
            Left            =   2660
            List            =   "frmTOCOld.frx":53E0
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   370
            Width           =   570
         End
         Begin VB.ComboBox cbxInsertAt 
            Height          =   315
            ItemData        =   "frmTOCOld.frx":53FF
            Left            =   730
            List            =   "frmTOCOld.frx":5401
            Style           =   2  'Dropdown List
            TabIndex        =   14
            Top             =   2720
            Width           =   2480
         End
         Begin VB.OptionButton optCreateFrom 
            Caption         =   "First &Sentence (period and 2 spaces)"
            Height          =   225
            Index           =   0
            Left            =   1405
            TabIndex        =   9
            Top             =   1295
            Value           =   -1  'True
            Width           =   2900
         End
         Begin VB.OptionButton optCreateFrom 
            Caption         =   "First S&entence (period and 1 space)"
            Height          =   225
            Index           =   1
            Left            =   1405
            TabIndex        =   10
            Top             =   1580
            Width           =   2900
         End
         Begin VB.CheckBox chkStyles 
            Caption         =   "St&yles:"
            Height          =   285
            Left            =   1170
            TabIndex        =   8
            Top             =   980
            Value           =   1  'Checked
            Width           =   825
         End
         Begin VB.CheckBox chkTCEntries 
            Caption         =   "&TC Entries"
            Height          =   270
            Left            =   1170
            TabIndex        =   12
            Top             =   2165
            Width           =   2175
         End
         Begin VB.OptionButton optCreateFrom 
            Caption         =   "Whole &Paragraph"
            Height          =   225
            Index           =   2
            Left            =   1405
            TabIndex        =   11
            Top             =   1865
            Width           =   2900
         End
         Begin VB.CheckBox chkInsertAsField 
            Caption         =   "Insert TOC as &Field"
            Height          =   270
            Left            =   200
            TabIndex        =   16
            Top             =   3220
            Width           =   2175
         End
         Begin VB.CommandButton btnSetLevels 
            Caption         =   "Set"
            Height          =   330
            Left            =   3500
            TabIndex        =   6
            Top             =   370
            Width           =   570
         End
         Begin VB.Label lblThrough 
            Caption         =   "T&hrough"
            Height          =   225
            Left            =   1945
            TabIndex        =   4
            Top             =   430
            Width           =   660
         End
         Begin VB.Label lblIncludeLevels 
            Caption         =   "Inc&lude Levels"
            Height          =   225
            Left            =   200
            TabIndex        =   2
            Top             =   430
            Width           =   1125
         End
         Begin VB.Label lblInsertAt 
            Caption         =   "&Insert:"
            Height          =   225
            Left            =   200
            TabIndex        =   13
            Top             =   2750
            Width           =   500
         End
         Begin VB.Label Label2 
            Caption         =   "Create From:"
            Height          =   300
            Left            =   200
            TabIndex        =   7
            Top             =   1010
            Width           =   1185
         End
      End
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3425
      TabIndex        =   36
      Top             =   4795
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   2255
      TabIndex        =   35
      Top             =   4795
      Width           =   1100
   End
End
Attribute VB_Name = "frmInsertTOCOld"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancelled As Boolean
Private m_bStepIndent As Boolean
Private xarStyles As xArray
Private xarDesignate As xArray
Private m_bTOCBold As Boolean
Private m_bTOCCap As Boolean
Private m_bTOCUnderline As Boolean
Private m_bPageBold As Boolean
Private m_bPageCap As Boolean
Private m_bPageUnderline As Boolean
Private m_iNumberStyle As Integer
Private m_iNumberPunctuation As Integer
Private m_xSchemes() As String
Private m_bRefreshStyleList As Boolean
Private m_bFormInit As Boolean
Private m_bNoJumpToAdvanced As Boolean

Public Property Get TOCScheme() As String
    If Me.cbxTOCScheme.ListIndex = 0 Then
        TOCScheme = "Custom"
    Else
        TOCScheme = m_xSchemes(Me.cbxTOCScheme.ListIndex - 1, 1)
    End If
End Property

Public Property Get BoldHeaderTOC() As Boolean
    BoldHeaderTOC = m_bTOCBold
End Property

Public Property Let BoldHeaderTOC(bNew As Boolean)
    m_bTOCBold = bNew
End Property

Public Property Get CapHeaderTOC() As Boolean
    CapHeaderTOC = m_bTOCCap
End Property

Public Property Let CapHeaderTOC(bNew As Boolean)
    m_bTOCCap = bNew
End Property

Public Property Get UnderlineHeaderTOC() As Boolean
    UnderlineHeaderTOC = m_bTOCUnderline
End Property

Public Property Let UnderlineHeaderTOC(bNew As Boolean)
    m_bTOCUnderline = bNew
End Property

Public Property Get BoldHeaderPage() As Boolean
    BoldHeaderPage = m_bPageBold
End Property

Public Property Let BoldHeaderPage(bNew As Boolean)
    m_bPageBold = bNew
End Property

Public Property Get CapHeaderPage() As Boolean
    CapHeaderPage = m_bPageCap
End Property

Public Property Let CapHeaderPage(bNew As Boolean)
    m_bPageCap = bNew
End Property

Public Property Get UnderlineHeaderPage() As Boolean
    UnderlineHeaderPage = m_bPageUnderline
End Property

Public Property Let UnderlineHeaderPage(bNew As Boolean)
    m_bPageUnderline = bNew
End Property

Public Property Get NumberStyle() As Integer
    NumberStyle = m_iNumberStyle
End Property

Public Property Let NumberStyle(iNew As Integer)
    m_iNumberStyle = iNew
End Property

Public Property Get NumberPunctuation() As Integer
    NumberPunctuation = m_iNumberPunctuation
End Property

Public Property Let NumberPunctuation(iNew As Integer)
    m_iNumberPunctuation = iNew
End Property

Public Property Let Exclusions(ByVal xNew As String)
'checks off the selected list
    Dim i As Integer

'   cycle through xarray
    With xarStyles
        For i = 0 To .Count(1) - 1
'           if scheme is in exclusion string, uncheck
            If InStr(xNew, "," & .Value(i, 2) & ",") Then
'               if so, add to comma delimited string
                .Value(i, 0) = 0
            End If
        Next i
    End With

End Property

Public Property Get Exclusions() As String
'returns a string of schemes that have been unchecked
    Dim i As Integer
    Dim xTemp As String

'   cycle through xarray
    With xarStyles
        For i = 0 To .Count(1) - 1
'           get if scheme should be excluded
            If (.Value(i, 0) = 0) Then
'               if so, add to comma delimited string
                xTemp = xTemp & .Value(i, 2) & ","
            End If
        Next i
    End With

    If Len(xTemp) Then
'       if list is not empty, add a preceding ','
        xTemp = "," & xTemp
    End If

    Exclusions = xTemp
End Property

Public Property Get Inclusions() As String
'returns the list of schemes to
'use to create toc entries - if
'user has specified that only marked
'text should be used, return an empty
'inclusions list
    Dim i As Integer
    Dim xTemp As String

'   cycle through xarray
    With xarStyles
        For i = 0 To .Count(1) - 1
'           get if scheme should be included
            If .Value(i, 0) = 1 Then
'               if so, add to comma delimited string
                xTemp = xTemp & .Value(i, 2) & ","
            End If
        Next i
    End With

    If Len(xTemp) Then
'       if list is not empty, add a preceding ','
        xTemp = "," & xTemp
    End If

    Inclusions = xTemp
End Property

Private Sub btnEditHeaderFooter_Click()
    With frmHeaderFooter
        .chkPageBold = Abs(Me.BoldHeaderPage)
        .chkTOCBold = Abs(Me.BoldHeaderTOC)
        .chkPageCaps = Abs(Me.CapHeaderPage)
        .chkTOCCaps = Abs(Me.CapHeaderTOC)
        .chkPageUnderline = Abs(Me.UnderlineHeaderPage)
        .chkTOCUnderline = Abs(Me.UnderlineHeaderTOC)
        .cmbPunctuation.SelectedItem = Abs(Me.NumberPunctuation)
        If .cmbPunctuation.BoundText = "" Then
            .cmbPunctuation.SelectedItem = 0
        End If
        .cmbNumberStyle.SelectedItem = Me.NumberStyle
        If .cmbNumberStyle.BoundText = "" Then
            .cmbNumberStyle.SelectedItem = 0
        End If
        
        .Show vbModal
        
        If Not .Cancelled Then
            Me.BoldHeaderPage = .chkPageBold
            Me.BoldHeaderTOC = .chkTOCBold
            Me.CapHeaderPage = .chkPageCaps
            Me.CapHeaderTOC = .chkTOCCaps
            Me.UnderlineHeaderPage = .chkPageUnderline
            Me.UnderlineHeaderTOC = .chkTOCUnderline
            Me.NumberPunctuation = .cmbPunctuation.SelectedItem
            Me.NumberStyle = .cmbNumberStyle.SelectedItem
            
'           reset current boilerplate
            If .cbxType.BoundText <> "" Then
                bAppSetUserINIValue "TOC", "CurrentBoilerplate", _
                    g_xBoilerplates(.cbxType.SelectedItem, 1)
            End If
        End If
    End With
    Unload frmHeaderFooter
End Sub

Private Sub cbxInsertAt_Click()
    If Not m_bFormInit Then
        Me.btnSet.Enabled = _
            (Me.cbxInsertAt.Text <> mpTOCLocationAboveTOA)
    End If
End Sub

Private Sub cbxMaxLevel_Click()
    If Not m_bFormInit Then _
        m_bRefreshStyleList = True
End Sub

Private Sub cbxMinLevel_Click()
    If Not m_bFormInit Then _
        m_bRefreshStyleList = True
End Sub

Private Sub cbxMinLevel_LostFocus()
    If Me.cbxMinLevel > Me.cbxMaxLevel Then
        xMsg = "Lowest level must be less than or " & _
               "equal to highest level."
        MsgBox xMsg, vbExclamation, AppName
        Me.cbxMinLevel = 1
        Me.cbxMinLevel.SetFocus
    ElseIf m_bRefreshStyleList Then
        xarGetStyles
    End If
End Sub

Private Sub cbxMaxLevel_LostFocus()
    If Me.cbxMinLevel > Me.cbxMaxLevel Then
        xMsg = "Highest level must be greater than " & _
               "or equal to the lowest level."
        MsgBox xMsg, vbExclamation, AppName
        Me.cbxMaxLevel = 9
        Me.cbxMaxLevel.SetFocus
    ElseIf m_bRefreshStyleList Then
        xarGetStyles
    End If
End Sub

Private Sub cbxScheduleStyles_Click()
    If Not m_bFormInit Then _
        m_bRefreshStyleList = True
End Sub

Private Sub cbxScheduleStyles_LostFocus()
    If m_bRefreshStyleList Then
        If Me.optInclude(0) Then
            xarGetStyles
        ElseIf Me.chkApplyTOC9 = 1 Then
            SelectTOC9Style Me.cbxScheduleStyles
        End If
    End If
End Sub

Private Sub cbxTOCScheme_Click()
    Me.cmdEdit.Enabled = Me.cbxTOCScheme.Text = "Custom"
End Sub

Private Sub chkApplyTOC9_Click()
    If Not m_bFormInit Then
        If Me.optInclude(0) Then
'           refresh list
            xarGetStyles

'           force designation option
            If Me.chkApplyTOC9 = 1 Then
                m_bNoJumpToAdvanced = True
                Me.optInclude(1) = True
                m_bNoJumpToAdvanced = False
            End If
        Else
'           designate TOC9 style for inclusion
            SelectTOC9Style Me.cbxScheduleStyles
        End If
    End If
End Sub

Private Sub chkInsertAsField_Click()
    Dim xValue As String

    'set defaults
    If Me.chkInsertAsField = 1 Then
        'whole paragraph is only available option
        Me.optCreateFrom(2) = True
    Else
        xValue = xAppGetUserINIValue("TOC", "CreateFrom")
        If xValue = "3" Then
            'period and 1 space
            Me.optCreateFrom(1) = True
        ElseIf xValue = "1" Then
            'whole paragraph
            Me.optCreateFrom(2) = True
        Else
            'period and 2 spaces
            Me.optCreateFrom(0) = True
        End If
    End If

    If InStr(Word.Application.Version, "8.") = 0 Then _
        Me.chkHyperlinks = 1
    Me.chkHyperlinks = Abs((Me.chkInsertAsField = 1) And _
        (InStr(Word.Application.Version, "8.") = 0))

    If Me.chkInsertAsField = 1 Then
        If g_bForceTCEntries Then
            Me.chkStyles = 0
            Me.chkTCEntries = 1
        End If
        If g_bForceLevels Then
            Me.cbxMinLevel = 1
            Me.cbxMaxLevel = 3
        End If
    End If

    'enable/disable
    If Me.chkStyles = 1 Then
        Me.optCreateFrom(0).Enabled = Not CBool(Me.chkInsertAsField.Value)
        Me.optCreateFrom(1).Enabled = Not CBool(Me.chkInsertAsField.Value)
    End If
    If InStr(Word.Application.Version, "8.") = 0 Then _
        Me.chkHyperlinks.Enabled = CBool(Me.chkInsertAsField.Value)
    If g_xScheduleStyles(0) <> "" Then
        Me.chkApplyTOC9.Enabled = Not CBool(Me.chkInsertAsField.Value)
        Me.cbxScheduleStyles.Enabled = Not CBool(Me.chkInsertAsField.Value)
    End If
End Sub

Private Sub chkStyles_Click()
    Me.optCreateFrom(0).Enabled = CBool(Me.chkStyles) And Not CBool(Me.chkInsertAsField)
    Me.optCreateFrom(1).Enabled = CBool(Me.chkStyles) And Not CBool(Me.chkInsertAsField)
    Me.optCreateFrom(2).Enabled = CBool(Me.chkStyles)
End Sub

Private Sub cmdEdit_Click()
    Dim bDefaultCustom
    bRet = mpTOC.EditCustomTOCScheme()
    If bRet Then
'       user saved the edits to custom toc scheme

'       set current toc scheme to
'       custom if specified by user
        bDefaultCustom = xAppGetUserINIValue( _
                            "TOC", _
                            "DefaultToCustom")
        If bDefaultCustom Then
            Me.cbxTOCScheme.ListIndex = 0
        End If
    End If
End Sub

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnOK_Click()
'   validate
    If bIsValid() Then
        Me.Cancelled = False
        Me.Hide
        DoEvents
        SaveCurInput
    End If
End Sub

Private Sub btnSet_Click()
    With Me.cbxInsertAt
        bAppSetUserINIValue "TOC", "DefaultLocation", .ListIndex
        Application.StatusBar = "Default TOC location set to " & .Text
    End With
End Sub

Private Sub btnSetLevels_Click()
    Dim xMin As String
    Dim xMax As String

    xMin = Trim(Me.cbxMinLevel)
    xMax = Trim(Me.cbxMaxLevel)
    bAppSetUserINIValue "TOC", "LevelStart", xMin
    bAppSetUserINIValue "TOC", "LevelEnd", xMax
    Application.StatusBar = "Default levels set to " & xMin & _
        " through " & xMax
End Sub

Private Sub lstStyles_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim lRow As Long

    If KeyCode = vbKeySpace Then
'       toggle chkbox in Schemes list
        lRow = Me.lstStyles.Row
        If xarStyles.Value(lRow, 0) = 0 Then
            xarStyles.Value(lRow, 0) = 1
        Else
            xarStyles.Value(lRow, 0) = 0
        End If
        Me.lstStyles.Refresh
        m_bRefreshStyleList = True
    End If
End Sub

Private Sub lstStyles_LostFocus()
    If m_bRefreshStyleList Then
        xarGetStyles
    End If
End Sub

Private Sub optInclude_Click(Index As Integer)
    Me.lstStyles.Enabled = (Index = 0)
    Me.cbxMaxLevel.Enabled = (Index = 0)
    Me.cbxMinLevel.Enabled = (Index = 0)
    Me.lblIncludeLevels.Enabled = (Index = 0)
    Me.lblThrough.Enabled = (Index = 0)

    If Not m_bFormInit Then
        If Index = 0 Then
'           uncheck Apply TOC 9 - this will also reset defaults
            Me.chkApplyTOC9 = 0

''           reset defaults
'            xarGetStyles
        Else
''           take user to Advanced tab
'            If Not m_bNoJumpToAdvanced Then
'                Me.vsIndexTab1.CurrTab = 2
'                Me.lstDesignate.SetFocus
'            End If
        End If
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim iNumSchemes As Integer
    Dim xStyleSchemes() As String
    Dim iCurDefScheme As Integer
    Dim xHeadingStyles As String
    Dim xItemLocations() As String
    Dim iNumStyleSchemes As Integer
    Dim xUserDef As String
    Dim iBoilerplate As Integer
    Dim iTOCMarker As mpTOCExistsMarker
    Dim xUserMin As String
    Dim xUserMax As String
    Dim xValue As String

    On Error GoTo Initialize_Error
    m_bFormInit = True
    Me.Cancelled = True

    iTOCMarker = iTOCExists(ActiveDocument)
    If iTOCMarker <> mpTOCExistsMarker_None Then
        With Me.cbxInsertAt
            .AddItem mpTOCLocationAtCurrent
            .ListIndex = 0
            .Enabled = False
        End With
        Me.lblInsertAt.Enabled = False
        Me.btnSet.Enabled = False
        Me.btnEditHeaderFooter.Enabled = False

        If iTOCMarker <> mpTOCExistsMarker_Field Then
'           there's an existing MacPac TOC;
'           default to keep current TOC styles
            Me.optTOCStyles(1) = True
        End If
    Else
        xUserDef = xAppGetUserINIValue("TOC", "DefaultLocation")
        With Me.cbxInsertAt
            For i = 0 To UBound(g_xTOCLocations)
                .AddItem g_xTOCLocations(i)
            Next i

            If bTOAExists(ActiveDocument) Then
'               add and default to "above TOA"
                .AddItem mpTOCLocationAboveTOA
                .ListIndex = .ListCount - 1
                Me.btnSet.Enabled = False
            Else
                If (IsNull(xUserDef)) Or _
                        (Val(xUserDef) > .ListCount - 1) Then
                    .ListIndex = 0
                Else
                    .ListIndex = Val(xUserDef)
                End If
            End If
        End With

'       sync current boilerplate with default - this should
'       ultimately be a property of the form
        On Error Resume Next
        iBoilerplate = xAppGetUserINIValue("TOC", _
            "DefaultBoilerplateIndex")
        If g_xBoilerplates(iBoilerplate, 1) <> "" Then
            bAppSetUserINIValue "TOC", "CurrentBoilerplate", _
                g_xBoilerplates(iBoilerplate, 1)
        End If
        On Error GoTo Initialize_Error
    End If

    iNumSchemes = iGetTOCSchemes(m_xSchemes())

'   load TOC schemes
    With Me.cbxTOCScheme
        .AddItem "Custom"
        For i = 0 To UBound(m_xSchemes)
            .AddItem m_xSchemes(i, 0)
        Next i

        On Error Resume Next
        If xAppGetUserINIValue("TOC", "DefaultToCustom") = "1" Then
            .Text = "Custom"
        Else
            iCurDefScheme = iGetCurDefTOCScheme()
'            .ListIndex = iCurDefScheme
            .Text = GetTOCField(iCurDefScheme, _
                                mpTOCRecField_Name, _
                                mpSchemeType_TOC)
            If .ListIndex = -1 Then _
                .ListIndex = 1
        End If
        On Error GoTo Initialize_Error
    End With

'   load schedule styles
    If g_xScheduleStyles(0) = "" Then
        Me.chkApplyTOC9.Visible = False
        Me.cbxScheduleStyles.Visible = False
        Me.fraTOCBody.Height = 1000
        Me.fraSchemes.Top = 2185
    Else
        With Me.cbxScheduleStyles
            For i = 0 To UBound(g_xScheduleStyles)
                .AddItem g_xScheduleStyles(i)
            Next i
            .ListIndex = 0
        End With

        'set default checkbox value
        If g_bApplyTOC9StyleDefault Then
            Me.chkApplyTOC9 = 1
            Me.optInclude(1) = True
        End If
    End If

    With Me
'       levels - first try user ini, then firm ini
        xUserMin = xAppGetUserINIValue("TOC", "LevelStart")
        If xUserMin <> "" Then
            On Error Resume Next
            .cbxMinLevel = xUserMin
            On Error GoTo Initialize_Error
            If .cbxMinLevel.ListIndex = -1 Then _
                .cbxMinLevel = g_iDefaultLevelStart
        Else
            .cbxMinLevel = g_iDefaultLevelStart
        End If
        xUserMax = xAppGetUserINIValue("TOC", "LevelEnd")
        If xUserMax <> "" Then
            On Error Resume Next
            .cbxMaxLevel = xUserMax
            On Error GoTo Initialize_Error
            If .cbxMaxLevel.ListIndex = -1 Then _
                .cbxMaxLevel = g_iDefaultLevelEnd
        Else
            .cbxMaxLevel = g_iDefaultLevelEnd
        End If

        .chkInsertAsField = Abs(g_bInsertTOCAsField)

'       load schemes in use
        xarGetSchemes

'       "create from" option is sticky
        xValue = xAppGetUserINIValue("TOC", "CreateFrom")
        If xValue = "3" Then
            'period and 1 space
            .optCreateFrom(1) = True
        ElseIf xValue = "1" Then
            'whole paragraph
            .optCreateFrom(2) = True
        End If

'       get defaults for Header/Footer
        On Error Resume Next
        .BoldHeaderTOC = xAppGetUserINIValue("TOC", "HeaderTOCBold")
        .CapHeaderTOC = xAppGetUserINIValue("TOC", "HeaderTOCCaps")
        .UnderlineHeaderTOC = xAppGetUserINIValue("TOC", "HeaderTOCUnderline")
        .BoldHeaderPage = xAppGetUserINIValue("TOC", "HeaderPageBold")
        .CapHeaderPage = xAppGetUserINIValue("TOC", "HeaderPageCaps")
        .UnderlineHeaderPage = xAppGetUserINIValue("TOC", "HeaderPageUnderline")
        .NumberStyle = xAppGetUserINIValue("TOC", "PageNoStyle")
        .NumberPunctuation = xAppGetUserINIValue("TOC", "PageNoPunctuation")

        .btnEditHeaderFooter.Visible = g_bAllowHeaderFooterEdit
    End With

    GetPrevInput

'   load available styles
    xarGetStyles
    On Error Resume Next
    Me.StyleExclusions = ActiveDocument.Variables("StyleExclusions")
    Me.StyleInclusions = ActiveDocument.Variables("StyleInclusions")
    On Error GoTo Initialize_Error

'   hide TOC field checkbox if specified
    If Not g_bAllowTOCAsField Then
        Me.chkInsertAsField.Visible = False
        Me.chkInsertAsField.Value = 0
    End If

'   hyperlink switch is not available in Word 97
    If (Not g_bAllowTOCAsField) Or _
            (InStr(Word.Application.Version, "8.") <> 0) Then
        Me.chkHyperlinks.Visible = False
        Me.chkHyperlinks.Value = 0
'       fill space on advanced tab
        Me.fraStyleList.Height = 3000
        Me.lstDesignate.Height = 2544
    ElseIf Me.chkInsertAsField.Value = 0 Then
        'GLOG 4714 (9/30/11)
        Me.chkHyperlinks.Enabled = False
    End If

    m_bFormInit = False
    Exit Sub

Initialize_Error:
    Select Case Err
        Case Else
            MsgBox Error(Err.Number), vbExclamation, AppName
    End Select
    Exit Sub
End Sub

Property Let bStepIndent(bStepIndent As Boolean)
    m_bStepIndent = bStepIndent
End Property

Property Get bStepIndent() As Boolean
    bStepIndent = m_bStepIndent
End Property


Private Function xarGetSchemes()
    Dim i As Integer
    Dim iNumSchemes As Integer
    Dim xDocSchemes() As String
    Dim bHSchemeExists As Boolean
    Dim bHStylesUsed As Boolean
    Dim iSchedule As Integer
    Dim bFirstRun As Boolean

'   position dropdown will be disabled on subsequent runs
    bFirstRun = Me.cbxInsertAt.Enabled

'   get numbering schemes in active doc
    iNumSchemes = iGetSchemes(xDocSchemes(), _
                              mpSchemeType_Document)

'   check if any schedule styles are listed in ini
'    If g_xScheduleStyles(0) <> "" Then
'        iSchedule = 1
'    End If

'   get if there is a heading scheme in the doc
    bHSchemeExists = (Len(xHeadingScheme()) > 0)

    Set xarStyles = New xArray
    With xarStyles
        .ReDim 0, iNumSchemes + iSchedule, 0, 2

'       add schedule option only if at least one style is listed in ini
        If iSchedule = 1 Then
            .Value(iNumSchemes + 1, 2) = "Schedule"
            .Value(iNumSchemes + 1, 1) = "Schedule/Exhibit Styles"
            .Value(iNumSchemes + 1, 0) = Abs(g_bIncludeSchedule Or Not bFirstRun)
        End If

''       add other styles option
'        .Value(iNumSchemes + 1, 2) = "Other"
'        .Value(iNumSchemes + 1, 1) = "Other Outline Level Styles"
'        .Value(iNumSchemes + 1, 0) = Abs(Not bFirstRun)

'       get all numbering schemes in document
        For i = 1 To iNumSchemes
            .Value(i, 2) = xDocSchemes(i - 1, 0)
            .Value(i, 1) = xDocSchemes(i - 1, 1)
            If .Value(i, 1) = "" Then
'               if scheme props are missing for some reason,
'               display style root rather than an empty string
                .Value(i, 1) = xGetStyleRoot(.Value(i, 2))
            End If
            If bIsHeadingScheme(.Value(i, 2)) Then
                .Value(i, 1) = "Word Heading Styles (" & _
                    .Value(i, 1) & ")"
            End If
            .Value(i, 0) = 1
        Next i

        If bHSchemeExists Then
'           there is a MacPac scheme
'           that uses heading styles...
'           delete row reserved for
'           Word Heading Styles
            .Delete 1, 0
        Else
'           if there is no MacPac scheme
'           that uses heading styles...
'           add heading styles
            .Value(0, 2) = "Heading"
            .Value(0, 1) = "Word Heading Styles"
'           only default on if no schemes
            .Value(0, 0) = Abs(iNumSchemes = 0 Or Not bFirstRun)
        End If
    End With

'   load list
    Me.lstStyles.Array = xarStyles
    Me.lstStyles.Rebind

End Function

Private Function xarGetStyles()
    Dim styP As Word.Style
    Dim i As Integer
    Dim iCount As Integer
    Dim iLevel As Integer
    Dim iPos As Integer
    Dim xScheme As String
    Dim bOn As Boolean
    Dim bIncludeHScheme As Boolean
    Dim xHScheme As String
    Dim xInclusions As String
    Dim xBase As String
    Dim xarTemp As xArray
    Dim bIsReUse As Boolean
    Dim xTest As String

    On Error GoTo ProcError

    Set xarTemp = New xArray
    xarTemp.ReDim 0, 1000, 0, 2

'   test to see whether previous exclusion list is stored
    On Error Resume Next
    xTest = ActiveDocument.Variables("StyleExclusions")
    bIsReUse = (Err = 0)
    On Error GoTo ProcError

'   get inclusions from Options tab
    xInclusions = UCase(Me.Inclusions)

'   get scheme using Word Heading styles and whether to include
    xHScheme = UCase(xHeadingScheme())
    If xHScheme <> "" Then
        bIncludeHScheme = InStr(xInclusions, "," & xHScheme & ",")
    End If

    For Each styP In Word.ActiveDocument.Styles
        bOn = False
        With styP
            If .Type = wdStyleTypeParagraph Then
'               get outline level;
'               retrieving outline level will make style "in use",
'               so only do this if style is already in use; use
'               different method to check Word Heading styles,
'               which should be listed in any case
                iLevel = iGetWordHeadingLevel(.NameLocal)
                If iLevel = 0 Then
                    If .InUse Then
                        iLevel = .ParagraphFormat.OutlineLevel
                    Else
                        iLevel = wdOutlineLevelBodyText
                    End If
                End If

'               strip alias
                xBase = StripStyleAlias(.NameLocal)

                If iLevel <> wdOutlineLevelBodyText Then
'                   it's an outline level style
                    iPos = InStr(UCase(xBase), "_L")
                    If iPos Then
'                       it's a MacPac numbering style
                        xScheme = "ZZMP" & UCase(Left(xBase, iPos - 1))
'                       if scheme uses Word Heading styles, do not list
'                       proprietary style
                        If xScheme = xHScheme Then GoTo labNextSty
                    End If

'                   add style to list
                    xarTemp.Value(iCount, 2) = xBase
                    xarTemp.Value(iCount, 1) = .NameLocal

'                   default style to ON if TOC 9 style or if level is in designated range
'                   and scheme is on inclusion list
                    If (xBase = Me.cbxScheduleStyles) And _
                            (Me.chkApplyTOC9 = 1) Then
'                       TOC9 Style
                        bOn = True
                    ElseIf (iLevel >= Val(Me.cbxMinLevel)) And _
                            (iLevel <= Val(Me.cbxMaxLevel)) Then
                        If bIsHeadingStyle(.NameLocal) Then
'                           Word Heading style
                            bOn = bIncludeHScheme Or InStr(xInclusions, ",HEADING,")
                        ElseIf iPos Then
'                           MacPac numbering style
                            bOn = InStr(xInclusions, "," & xScheme & ",")
'                        ElseIf bIsScheduleStyle(xBase) Then
''                           Schedule/Exhibit style
'                            bOn = InStr(xInclusions, ",SCHEDULE,")
                        End If
                    End If
                    xarTemp.Value(iCount, 0) = Abs(bOn)

'                   increment count
                    iCount = iCount + 1
                ElseIf bIsScheduleStyle(xBase) Then
'                   schedule/exhibit styles may not be defined as outline level
                    xarTemp.Value(iCount, 2) = xBase
                    xarTemp.Value(iCount, 1) = .NameLocal

''                   default style to ON if level is in designated range
''                   and scheme is on inclusion list
'                    If (.NameLocal = Me.cbxScheduleStyles) And _
'                            (Me.chkApplyTOC9 = 1) Then
''                       designated for inclusion as TOC 9 - always in range
'                        iLevel = Val(Me.cbxMinLevel)
'                    Else
''                       treat others as level one
'                        iLevel = 1
'                    End If
'
'                    If (iLevel >= Val(Me.cbxMinLevel)) And _
'                            (iLevel <= Val(Me.cbxMaxLevel)) Then
'                        xarTemp.Value(iCount, 0) = Abs(InStr(xInclusions, _
'                            ",SCHEDULE,") <> 0)
'                    End If

'                   default TOC9 style ON, others OFF
                    xarTemp.Value(iCount, 0) = _
                        Abs((xBase = Me.cbxScheduleStyles) And (Me.chkApplyTOC9 = 1))

'                   increment count
                    iCount = iCount + 1
                End If
            End If
        End With
labNextSty:
    Next styP

'   dim array to actual size
    Set xarDesignate = New xArray
    xarDesignate.ReDim 0, iCount - 1, 0, 2

    For i = 0 To iCount - 1
        xarDesignate.Value(i, 0) = xarTemp.Value(i, 0)
        xarDesignate.Value(i, 1) = xarTemp.Value(i, 1)
        xarDesignate.Value(i, 2) = xarTemp.Value(i, 2)
    Next i

'   load list
    Me.lstDesignate.Array = xarDesignate
    Me.lstDesignate.Rebind

    m_bRefreshStyleList = False
    Exit Function
ProcError:
    RaiseError "frmInsertTOCOld.xarGetStyles"
    Exit Function
End Function

Private Sub lstStyles_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lRow As Long
    Dim lBkmk As Long

    lRow = Me.lstStyles.RowContaining(Y)
    lBkmk = Me.lstStyles.RowBookmark(lRow)

    If xarStyles.Value(lBkmk, 0) = 0 Then
        xarStyles.Value(lBkmk, 0) = 1
    Else
        xarStyles.Value(lBkmk, 0) = 0
    End If
    Me.lstStyles.Refresh
    m_bRefreshStyleList = True
End Sub

Private Function SaveCurInput()
    With ActiveDocument.Variables
        .Item("cbxMinLevel") = Me.cbxMinLevel
        .Item("cbxMaxLevel") = Me.cbxMaxLevel

        If Me.optCreateFrom(0) Then
            .Item("optCreateFrom") = 0
        ElseIf Me.optCreateFrom(1) Then
'           period & 1 space is new option
            .Item("optCreateFrom") = 3
        ElseIf Me.optCreateFrom(2) Then
            .Item("optCreateFrom") = 1
        Else
            .Item("optCreateFrom") = 2
        End If

'       create from is sticky
        If Me.chkInsertAsField = 0 Then
            bAppSetUserINIValue "TOC", "CreateFrom", _
                .Item("optCreateFrom")
        End If

        If Me.optInclude(0) Then
            .Item("optInclude") = 0
        Else
            .Item("optInclude") = 1
        End If

        .Item("cbxTOCScheme") = Me.cbxTOCScheme.ListIndex
        .Item("chkApplyManualFormatsToTOC") = Me.chkApplyManualFormatsToTOC
        .Item("chkStyles") = Me.chkStyles
        .Item("chkTCEntries") = Me.chkTCEntries
        .Item("Exclusions") = Me.Exclusions
        .Item("StyleExclusions") = Me.StyleExclusions
        .Item("StyleInclusions") = Me.StyleInclusions
        .Item("cbxScheduleStyles") = Me.cbxScheduleStyles.ListIndex
        .Item("chkApplyTOC9") = Me.chkApplyTOC9
        .Item("chkInsertAsField") = Me.chkInsertAsField
        .Item("chkHyperlinks") = Me.chkHyperlinks
        .Item("chkTwoColumn") = Me.chkTwoColumn
    End With
End Function

Private Function GetPrevInput()
    Dim xValue As String

    With ActiveDocument.Variables
        On Error Resume Next
        Me.chkInsertAsField = Abs(g_bAllowTOCAsField And CBool(.Item("chkInsertAsField")))
        Me.cbxMinLevel = .Item("cbxMinLevel")
        Me.cbxMaxLevel = .Item("cbxMaxLevel")

        xValue = .Item("optCreateFrom")
        If xValue <> "" Then
            If xValue = "0" Then
                Me.optCreateFrom(0) = True
            ElseIf xValue = "3" Then
'               period & 1 space is new option
                Me.optCreateFrom(1) = True
            ElseIf xValue = "1" Then
                Me.optCreateFrom(2) = True
            End If
        End If

        xValue = ""
        xValue = .Item("optInclude")
        If xValue = "0" Then
            Me.optInclude(0) = True
        ElseIf xValue = "1" Then
            Me.optInclude(1) = True
        End If

        Me.cbxTOCScheme.ListIndex = .Item("cbxTOCScheme")
        Me.chkApplyManualFormatsToTOC = .Item("chkApplyManualFormatsToTOC")
        Me.chkStyles = .Item("chkStyles")
        Me.chkTCEntries = .Item("chkTCEntries")
        Me.chkTwoColumn = .Item("chkTwoColumn")
        Me.Exclusions = .Item("Exclusions")
        Me.cbxScheduleStyles.ListIndex = .Item("cbxScheduleStyles")
        Me.chkApplyTOC9 = .Item("chkApplyTOC9")
        Me.chkHyperlinks = .Item("chkHyperlinks")
    End With
End Function

Function bIsValid() As Boolean
'returns TRUE if user choices in
'dlg can produce a toc with entries
    Dim xMsg As String

    If Me.chkStyles + Me.chkTCEntries = 0 Then
'       at least one must be selected
        xMsg = "No Table Of Contents can be generated with " & _
               "these choices.  " & vbCr & _
               "Please specify to create the TOC " & _
               "from styles, tc entries, or both."
        MsgBox xMsg, vbExclamation, AppName
        On Error Resume Next
        SSTab1.Tab = 0
        Me.chkStyles.SetFocus
        Exit Function
    ElseIf Me.chkStyles And Me.optInclude(0) And Me.Inclusions = "" Then
'       user specified 'styles', but no styles have been included
        xMsg = "No styles were chosen to be included in the TOC.  " & _
               vbCr & "Please select styles by choosing at least one " & _
               "scheme listed in the 'Options' tab."
        MsgBox xMsg, vbExclamation, AppName
        On Error Resume Next
        SSTab1.Tab = 1
        Me.lstStyles.SetFocus
        Exit Function
    ElseIf Me.chkStyles And Me.optInclude(1) And Me.StyleInclusions = "" Then
'       user specified 'styles', but no styles have been included
        xMsg = "No styles were chosen to be included in the TOC.  " & _
               vbCr & "Please select at least one style " & _
               "from the list on the 'Advanced' tab."
        MsgBox xMsg, vbExclamation, AppName
        On Error Resume Next
        SSTab1.Tab = 2
        Me.lstDesignate.SetFocus
        Exit Function
    End If
    bIsValid = True
End Function

Private Sub lstDesignate_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lRow As Long
    Dim lBkmk As Long

    lRow = Me.lstDesignate.RowContaining(Y)
    lBkmk = Me.lstDesignate.RowBookmark(lRow)

    If xarDesignate.Value(lBkmk, 0) = 0 Then
        xarDesignate.Value(lBkmk, 0) = 1
    Else
        xarDesignate.Value(lBkmk, 0) = 0
    End If
    Me.lstDesignate.Refresh
    Me.optInclude(1) = True
End Sub

Private Sub lstDesignate_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim lRow As Long

    If KeyCode = vbKeySpace Then
'       toggle chkbox in Schemes list
        lRow = Me.lstDesignate.Row
        If xarDesignate.Value(lRow, 0) = 0 Then
            xarDesignate.Value(lRow, 0) = 1
        Else
            xarDesignate.Value(lRow, 0) = 0
        End If
        Me.lstDesignate.Refresh
        Me.optInclude(1) = True
    End If
End Sub

Public Property Let StyleExclusions(ByVal xNew As String)
'checks off the selected list
    Dim i As Integer

'   cycle through xarray
    With xarDesignate
        For i = 0 To .Count(1) - 1
'           if scheme is in exclusion string, uncheck
            If .Value(i, 2) <> Empty Then
                If InStr(xNew, "," & .Value(i, 2) & ",") Then
                    .Value(i, 0) = 0
                End If
            Else
                Exit For
            End If
        Next i
    End With

End Property

Public Property Get StyleExclusions() As String
'returns a string of styles that have been unchecked
    Dim i As Integer
    Dim xTemp As String

'   cycle through xarray
    With xarDesignate
        For i = 0 To .Count(1) - 1
'           get if scheme should be excluded
            If .Value(i, 2) <> Empty Then
                If (.Value(i, 0) = 0) Then
'                   if so, add to comma delimited string
                    xTemp = xTemp & .Value(i, 2) & ","
                End If
            Else
                Exit For
            End If
        Next i
    End With

    If Len(xTemp) Then
'       if list is not empty, add a preceding ','
        xTemp = "," & xTemp
    End If

    StyleExclusions = xTemp
End Property

Public Property Get StyleInclusions() As String
'returns the list of styles to
'use to create toc entries
    Dim i As Integer
    Dim xTemp As String

'   cycle through xarray
    With xarDesignate
        For i = 0 To .Count(1) - 1
'           get if scheme should be included
            If .Value(i, 2) <> Empty Then
                If .Value(i, 0) = 1 Then
'                   if so, add to comma delimited string
                    xTemp = xTemp & .Value(i, 2) & ","
                End If
            Else
                Exit For
            End If
        Next i
    End With

    If Len(xTemp) Then
'       if list is not empty, add a preceding ','
        xTemp = "," & xTemp
    End If

    StyleInclusions = xTemp
End Property

Public Property Let StyleInclusions(ByVal xNew As String)
'checks on the selected list
    Dim i As Integer

'   cycle through xarray
    With xarDesignate
        For i = 0 To .Count(1) - 1
'           if scheme is in inclusion string, check
            If .Value(i, 2) <> Empty Then
                If InStr(xNew, "," & .Value(i, 2) & ",") Then
                    .Value(i, 0) = 1
                End If
            Else
                Exit For
            End If
        Next i
    End With

End Property

Private Sub SelectTOC9Style(xTOC9Style As String)
'ensures that TOC 9 style is selected
    Dim i As Integer

    With xarDesignate
        For i = 0 To .Count(1) - 1
            If .Value(i, 2) = xTOC9Style Then
                .Value(i, 0) = 1
            End If
        Next i
    End With
    Me.lstDesignate.Rebind
    m_bRefreshStyleList = False
End Sub

Private Sub optTOCStyles_Click(Index As Integer)
    Me.cbxTOCScheme.Enabled = (Index = 0)
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab = 0 Then
        If Me.cbxMinLevel.Enabled Then
            Me.cbxMinLevel.SetFocus
        Else
            Me.chkStyles.SetFocus
        End If
    ElseIf SSTab1.Tab = 1 Then
        If Me.optTOCStyles(0) Then
            Me.optTOCStyles(0).SetFocus
        Else
            Me.optTOCStyles(1).SetFocus
        End If
    Else
        Me.lstDesignate.SetFocus
    End If
    
End Sub
