VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CBitmap"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' 32-bit WinAPI Capture Routines
'
' This module contains several routines for capturing windows into a
' picture.  All the routines work on  32 bit Windows
' platforms.
' The routines also have palette support.
'
' CreateBitmapPicture - Creates a picture object from a bitmap and
' palette.
' CaptureWindow - Captures any window given a window handle.
' CaptureActiveWindow - Captures the active window on the desktop.
' CaptureForm - Captures the entire form.
' CaptureClient - Captures the client area of a form.
' CaptureScreen - Captures the entire screen.
' PrintPictureToFitPage - prints any picture as big as possible on
' the page.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      
Option Explicit
Option Base 0

Private Type RECT
      Left As Long
      Top As Long
      Right As Long
      Bottom As Long
End Type

Private Declare Function DSCreateDIBSection Lib "dibsectn.dll" ( _
  ByVal dwX As Long, _
  ByVal dwY As Long, _
  ByVal wBits As Integer) _
As Long

Private Declare Function DSLoadDIBSectionFromBMPFile Lib "dibsectn.dll" ( _
  ByVal szFileName As String, _
  phBitmap As Long, _
  phPalette As Long) _
As Boolean

Private Declare Function DSDrawDIBSectionOnDC Lib "dibsectn.dll" ( _
  ByVal hDC As Long, _
  ByVal hBitmap As Long, _
  pRect As RECT) _
As Boolean

Private Declare Function DSStoreDIBSectionInBMPFile Lib "dibsectn.dll" ( _
  ByVal szFileName As String, _
  ByVal hBitmap As Long) _
As Boolean

Private Declare Function DSCreatePaletteForDIBSection Lib "dibsectn.dll" ( _
  ByVal hBitmap As Long) _
As Long

Private Declare Function DSGetBITMAPINFOForDIBSection Lib "dibsectn.dll" ( _
  ByVal hBitmap As Long) _
As Long

Private Declare Function DSColorTableSize Lib "dibsectn.dll" ( _
  ByVal pbmi As Long) _
As Long

Private Declare Function DSTotalBytesSize Lib "dibsectn.dll" ( _
  ByVal pbmi As Long) _
As Long

Private Declare Function DSImageBitsSize Lib "dibsectn.dll" ( _
  ByVal pbmi As Long) _
As Long

Private Declare Function DSGetPointerToDIBSectionImageBits Lib "dibsectn.dll" ( _
  ByVal hBitmap As Long) _
As Long

Private Declare Function DSCreateSpectrumPalette Lib "dibsectn.dll" () _
As Long

Private Const RASTERCAPS As Long = 38
Private Const RC_PALETTE As Long = &H100
Private Const SIZEPALETTE As Long = 104

Private Declare Function CreateCompatibleDC Lib "gdi32" ( _
  ByVal hDC As Long) As Long
Private Declare Function CreateCompatibleBitmap Lib "gdi32" ( _
  ByVal hDC As Long, ByVal nWidth As Long, _
  ByVal nHeight As Long) As Long
Private Declare Function GetDeviceCaps Lib "gdi32" ( _
  ByVal hDC As Long, ByVal iCapabilitiy As Long) As Long
Private Declare Function SelectObject Lib "gdi32" ( _
  ByVal hDC As Long, ByVal hObject As Long) As Long
Private Declare Function BitBlt Lib "gdi32" ( _
  ByVal hDCDest As Long, ByVal xDest As Long, _
  ByVal YDest As Long, ByVal nWidth As Long, _
  ByVal nHeight As Long, ByVal hDCSrc As Long, _
  ByVal XSrc As Long, ByVal YSrc As Long, ByVal dwRop As Long) _
  As Long
Private Declare Function DeleteDC Lib "gdi32" ( _
  ByVal hDC As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" ( _
  ByVal hObject As Long) As Long
Private Declare Function GetWindowDC Lib "user32" ( _
  ByVal hwnd As Long) As Long
Private Declare Function GetDC Lib "user32" ( _
  ByVal hwnd As Long) As Long
Private Declare Function ReleaseDC Lib "user32" ( _
  ByVal hwnd As Long, ByVal hDC As Long) As Long
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' CaptureWindow
'    - Captures any portion of a window.
'
' hWndSrc
'    - Handle to the window to be captured.
'
' Client
'    - If True CaptureWindow captures from the client area of the
'      window.
'    - If False CaptureWindow captures from the entire window.
'
' LeftSrc, TopSrc, WidthSrc, HeightSrc
'    - Specify the portion of the window to capture.
'    - Dimensions need to be specified in pixels.
'
' Returns
'    - Returns a Picture object containing a 24 bit bitmap of the specified
'      portion of the window that was captured.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Function CaptureWindow(ByVal hWndSrc As Long, _
                              ByVal LeftSrc As Long, _
                              ByVal TopSrc As Long, _
                              ByVal WidthSrc As Long, _
                              ByVal HeightSrc As Long, _
                              ByVal xFile As String) As Picture

    Dim hDCMemory As Long
    Dim hBmp As Long
    Dim hBmpPrev As Long
    Dim r As Long
    Dim hDCSrc As Long

'   Get device context for client area.
    hDCSrc = GetDC(hWndSrc)
    
'   create a memory device context for the copy process
    hDCMemory = CreateCompatibleDC(hDCSrc)

'   create a bitmap and place it in the memory DC
    hBmp = CreateCompatibleBitmap(hDCSrc, WidthSrc, HeightSrc)

'   create 16 color DIB - 4 bits/pixel
    hBmp = DSCreateDIBSection(WidthSrc, HeightSrc, 4)

    hBmpPrev = SelectObject(hDCMemory, hBmp)

'   copy the image into the memory DC
    r = BitBlt(hDCMemory, 0, 0, WidthSrc, HeightSrc, hDCSrc, _
               LeftSrc, TopSrc, vbSrcCopy)

'   remove the new copy of the  on-screen image
    hBmp = SelectObject(hDCMemory, hBmpPrev)

'   create 16 color (4 bit) bmp file
    r = DSStoreDIBSectionInBMPFile(xFile, hBmp)

'   release resources back to the system
    r = DeleteDC(hDCMemory)
    r = ReleaseDC(hWndSrc, hDCSrc)
    r = DeleteObject(hBmp)
End Function

