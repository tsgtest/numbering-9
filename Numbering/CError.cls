VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Enum mpnErrors
    mpError_NoTOCStyFile = vbError + 512 + 1
    mpError_InvalidSchemePropValue = vbError + 512 + 2
    mpError_CouldNotUpgradeStyFile = vbError + 512 + 3
    mpError_NoSharedSchemeDeletePermission = vbError + 512 + 4
    mpError_NoSharedSchemeCategories = vbError + 512 + 5
    mpError_CouldNotOpenFAQ = vbError + 512 + 6
End Enum

Sub Raise(CurError As ErrObject)
    Dim xDesc As String
    Dim iSeverity As Integer
    Dim xError As String
    Dim lError As Long
    Dim xErrSource As String
    Dim xTitle As String
    
    With CurError
        xError = .Description
        lError = .Number
        xErrSource = .Source
    End With
    
    On Error GoTo ProcError
    EchoOn
    Word.Application.ScreenUpdating = True
    
    Screen.MousePointer = vbDefault
    If g_lUILanguage = wdFrenchCanadian Then
        xTitle = "Num�rotation MacPac"
        Select Case lError
            Case mpError_CouldNotOpenFAQ
                xDesc = "Ne peut ouvrir FAQ.doc. Le fichier est peut �tre corrompu ou n'est pas en format MS Word."
                iSeverity = vbExclamation
            Case mpError_NoSharedSchemeCategories
                xDesc = "Aucune cat�gorie de th�me partag� n'a �t� trouv�.  Votre administrateur doit ajouter au moins une cat�gorie � vos th�mes partag�s."
                iSeverity = vbCritical
            Case mpError_NoTOCStyFile
                xDesc = "mpTOC.sty est manquant du r�pertoire de groupe de travail.  Veuillez contacter votre administrateur."
                iSeverity = vbCritical
            Case mpError_CouldNotUpgradeStyFile
                xDesc = "Ne peut mettre � jour Th�mes de Num�rotation publics/priv�s. Th�mes utilis�s dans Office 2000.  Veuillez contacter votre administrateur."
                iSeverity = vbCritical
            Case mpError_NoSharedSchemeDeletePermission
                xDesc = "Vous n'avez pas les droits pour supprimer ce th�me. Seulement la personne qui a affich� ce th�me et votre administrateur peuvent supprimer ce th�me."
                iSeverity = vbExclamation
            Case Else
                xDesc = "Une erreur inattendue s'est produite dans la proc�dure " & _
                    xErrSource & "." & vbCr & "Erreur #" & lError & ": " & _
                    xError
                iSeverity = vbCritical
        End Select
    Else
        xTitle = App.Title
        Select Case lError
            Case mpError_CouldNotOpenFAQ
                xDesc = "Could not open FAQ.doc.  The file may " & _
                        "be corrupt or not in MS Word format."
                iSeverity = vbExclamation
            Case mpError_NoSharedSchemeCategories
                xDesc = "No shared scheme categories were found.  " & _
                        "Please have your administrator " & _
                        "add at least one category to your " & _
                        "set of shared schemes."
                iSeverity = vbCritical
            Case mpError_NoTOCStyFile
                xDesc = "mpTOC.sty is missing from your Workgroup directory.  " & _
                    "Please contact your administrator."
                iSeverity = vbCritical
            Case mpError_CouldNotUpgradeStyFile
                xDesc = "Could not upgrade your public/private Numbering " & _
                               "Schemes for use with office 2000.  " & _
                               "Please contact your administrator."
                iSeverity = vbCritical
            Case mpError_NoSharedSchemeDeletePermission
                xDesc = "You do not have permission to delete this scheme.  " & vbCr & _
                    "Only the person who posted the scheme and your " & _
                    "administrator can delete this scheme."
                iSeverity = vbExclamation
            Case Else
                xDesc = "An unexpected error occurred in procedure " & _
                    xErrSource & "." & vbCr & "Error #" & lError & ": " & _
                    xError
                iSeverity = vbCritical
        End Select
    End If
    MsgBox xDesc, iSeverity, xTitle
    Exit Sub
    
ProcError:
    If g_lUILanguage = wdFrenchCanadian Then
        xMsg = "Une erreur inattendue s'est produite dans ""CError.RaiseError"". " & _
               vbCr & Err.Number & ":" & Err.Description & "."
    Else
        xMsg = "An unexpected error occurred in ""CError.RaiseError"". " & _
               vbCr & Err.Number & ":" & Err.Description & "."
    End If
    MsgBox xMsg, vbCritical, g_xAppName
    Exit Sub
End Sub
