Attribute VB_Name = "mpDocument"
Option Explicit

Function secCreateSection(rngLocation As Word.Range, _
                          Optional bLinkHeadersFooters As Boolean = False, _
                          Optional bAllowTrailer As Boolean = True, _
                          Optional bRestartPageNumbers = True, _
                          Optional bRetagMP10HeadersFooters As Boolean = False) As Word.Section
'create section at start of range rngLocation
'bRetagMP10HeadersFooters parameter added in 9.9.5008
    Dim secNew As Word.Section
    Dim iNewSec As Integer
    Dim rngNextChr As Word.Range
    Dim rngPrevChr As Word.Range
    Dim ftrExisting As HeaderFooter
    Dim iPageNum1 As Integer
    Dim iPageNum2 As Integer
    Dim iSecNum1 As Integer
    Dim iSecNum2 As Integer
    Dim iPageStart As Integer
    Dim secNext As Word.Section
    Dim bParaAdded As Boolean
    
        With rngLocation
'           create different section breaks based on location
            If .Start = ActiveDocument.Content.Start Then
                iNewSec = .Sections.First.Index ' .Information(wdActiveEndSectionNumber)
'               get current starting page number of section one
                With ActiveDocument.Sections(1) _
                        .Footers(wdHeaderFooterPrimary).PageNumbers
                    If .RestartNumberingAtSection = True Then
                        iPageStart = .StartingNumber
                    Else
                        iPageStart = 1
                    End If
                End With
                
                'if there's a table at start of doc, add a paragraph before to prevent
                'issue with xml tags
                If .Tables.Count > 0 Then
                    rngAddParaBeforeTable .Tables(1)
                    bParaAdded = True
                End If
                    
                .InsertBreak wdSectionBreakNextPage
                
                'delete added para
                If bParaAdded Then _
                    ActiveDocument.Sections(2).Range.Characters.First.Delete
                
                ActiveDocument.Sections(1).Range.Style = wdStyleNormal
                bRet = bUnlinkHeadersFooters(ActiveDocument.Sections(2))
                
                'GLOG 5264 - restore mp10 bookmarks
                If g_bIsMP10 Then _
                    RestoreMP10BookmarksAfterUnlinking ActiveDocument.Sections(2), False
                
'               restart page numbering in what is now section two
                With ActiveDocument.Sections(2) _
                        .Footers(wdHeaderFooterPrimary).PageNumbers
                    .RestartNumberingAtSection = bRestartPageNumbers 'GLOG 5609
                    .StartingNumber = iPageStart
                End With
            ElseIf .Start = ActiveDocument.Content.End - 1 Then
                iNewSec = .Sections.First.Index + 1 ' .Information(wdActiveEndSectionNumber) + 1
                .InsertBreak wdSectionBreakNextPage
                .Previous(wdParagraph).Style = wdStyleNormal
            Else
                Set rngPrevChr = ActiveDocument.Range(.End - 1, .End)
                Set rngNextChr = ActiveDocument.Range(.End, .End + 1)
                
                iNewSec = .Sections.First.Index ' .Information(wdActiveEndSectionNumber)

'               delete succeeding manual page breaks
                'GLOG 5082 - moved this code up to before insertion of section break
                If bIsPageBreak(rngNextChr) Then
                    If rngNextChr.Paragraphs(1).Range.Text = Chr(12) & vbCr Then
                        'GLOG 5082 - delete the entire paragraph if all it contains
                        'is the page break
                        rngNextChr.Paragraphs(1).Range.Delete
                    Else
                        rngNextChr.Delete
                    End If
                End If

'               delete preceding manual page breaks
                If bIsPageBreak(rngPrevChr) Then
                    rngPrevChr.Delete
                End If
                
                If rngPrevChr <> Chr(12) Then
                    iNewSec = iNewSec + 1
                    .InsertBreak wdSectionBreakNextPage
                    '---ensure normal format
                    .MoveEnd wdCharacter, -1
                    .Style = wdStyleNormal
                    .MoveEnd wdCharacter, 1
                End If
                
                Set rngNextChr = ActiveDocument.Range(.End, .End + 1)
                
''               delete succeeding manual page breaks
'                If bIsPageBreak(rngNextChr) Then
'                    rngNextChr.Delete
'                    Set rngNextChr = ActiveDocument.Range(.End, .End + 1)
'                End If
                
                If rngNextChr <> Chr(12) Then
                    .InsertBreak wdSectionBreakNextPage
                End If
                                
'               ensure that heads/foots are unlinked
'               in subsequent section
                Set secNext = ActiveDocument.Sections(iNewSec + 1)
                bUnlinkHeadersFooters secNext
                    
                'GLOG 5264 - restore mp10 bookmarks
                'remmed in 9.9.5007 - we should be calling this only
                'when inserting the TOC at start of document
                '9.9.5008 - we do need to do this when inserting at
                'insertion point, in case we're splitting a segment
                If g_bIsMP10 And bRetagMP10HeadersFooters Then _
                    RestoreMP10BookmarksAfterUnlinking secNext, True
                
'               restart page numbering in subsequent section
                If bRestartPageNumbers Then 'GLOG 5609
                    For Each ftrExisting In secNext.Footers
                        With ftrExisting.PageNumbers
                            .RestartNumberingAtSection = True
                            .StartingNumber = 1
                        End With
                    Next ftrExisting
                End If
            End If
            Set secNew = ActiveDocument.Sections(iNewSec)
        End With
        
        If Not bLinkHeadersFooters Then _
            bRet = bUnlinkHeadersFooters(secNew)
        
        bClearContent bAllSections:=False, _
                      secCurSection:=secNew, _
                      bHeaders:=True, _
                      bFooters:=True
                      
'       set restart of page numbers
        For Each ftrExisting In secNew.Footers
            ftrExisting.PageNumbers _
                .RestartNumberingAtSection = bRestartPageNumbers
        Next ftrExisting
        
'        If Not bAllowTrailer Then _
'            mpTrailer.bMarkForNoTrailer secNew
            
        Set secCreateSection = secNew
        
End Function

Function iInsertAtMultiple(ByVal xLabel As String, _
                     Optional ByVal xText As String, _
                     Optional ByVal xBPRange As String, _
                     Optional iLineLength As Integer = 0, _
                     Optional bDeletePara As Boolean = False, _
                     Optional ByVal vSearch As Variant, _
                     Optional xDefaultText As String = "") As Integer
                     
'   runs rngInsertAt for each
'   occurrence of xLabel
    Dim rngFound As Word.Range
    Dim i As Integer

'   dummy range to get started in loop
    Set rngFound = ActiveDocument.Content
    
    While Not (rngFound Is Nothing)
        Set rngFound = rngInsertAt(xLabel, _
                                   xText, _
                                   xBPRange, _
                                   iLineLength, _
                                   bDeletePara, _
                                   vSearch, _
                                   xDefaultText)
        i = i + 1
    Wend
    
'   return number of finds
    iInsertAtMultiple = i - 1
End Function

Public Function bIsPleadingDocument(docDoc As Word.Document) As Boolean
    With docDoc
        If InStr(UCase(.AttachedTemplate), "PLEAD") Or _
                InStr(UCase(.AttachedTemplate), TemplatePOS) Or _
                InStr(UCase(.AttachedTemplate), "VER") Then
            bIsPleadingDocument = True
        End If
    End With
End Function


Function bClearContent(Optional bAllSections As Boolean = True, _
                        Optional secCurSection As Word.Section, _
                        Optional bMainStory As Boolean = False, _
                        Optional bHeaders As Boolean = False, _
                        Optional bFooters As Boolean = False) _
                        As Boolean

'clears contents of specified stories
'of specified sections

'/////////MODIFIED COMPLETELY
    Dim hdrHeader As HeaderFooter
    Dim ftrFooter As HeaderFooter
    Dim secSection As Word.Section
    Dim secSections As Word.Sections
    Dim fldLoc As Word.Field
    Dim bClearShapes As Boolean
    
    If bAllSections Then
        'delete shapes first
        DeleteHeaderFooterShapes , bHeaders, bFooters
        
        For Each secSection In ActiveDocument.Sections
            With secSection
                If bMainStory Then
                    With .Range
                        .Delete
                        For Each fldLoc In .Fields
                            fldLoc.Delete
                        Next fldLoc
                    End With
                End If
                        
                If bHeaders Then
                    For Each hdrHeader In .Headers
                        With hdrHeader.Range
                            .Delete
                             For Each fldLoc In .Fields
                                fldLoc.Delete
                             Next fldLoc
                        End With
                    Next hdrHeader
                End If
        
                If bFooters Then
                    For Each ftrFooter In .Footers
                        With ftrFooter.Range
                            .Delete
                             For Each fldLoc In .Fields
                                fldLoc.Delete
                             Next fldLoc
                        End With
                    Next ftrFooter
                End If
            End With
        Next secSection
    Else
        With secCurSection
            If bMainStory Then
                With .Range
                    .Delete
                    For Each fldLoc In .Fields
                        fldLoc.Delete
                    Next fldLoc
                End With
            End If
                
            'delete shapes first
            '2/19/13 - deleting shapes here is causing Word to crash in documents
            'with 2013 compatibility mode and doesn't appear to be necessary
            '(at least for pleading paper)
            'GLOG 5413/5414 - always skip this code in Word 2013 - it was also
            'crashing in mp9 pleadings in earlier Word compatibility modes
            If (InStr(Word.Application.Version, "15.") <> 0) Or _
                    (InStr(Word.Application.Version, "16.") <> 0) Then
'                bClearShapes = (mdlWord14.GetCompatibilityMode(ActiveDocument) < 15)
                bClearShapes = False
            Else
                bClearShapes = True
            End If
            If bClearShapes Then _
                DeleteHeaderFooterShapes .Index, bHeaders, bFooters
        
            If bHeaders Then
                For Each hdrHeader In .Headers
                    With hdrHeader.Range
                        .Delete
                         For Each fldLoc In .Fields
                            fldLoc.Delete
                         Next fldLoc
                         
                         'GLOG 5083 - table may not get deleted automatically when
                         'there's no trailing paragraph due to corruption
                         If .Tables.Count Then
                            .Tables(1).Delete
                            .StartOf
                            .InsertParagraphAfter
                         End If
                    End With
                Next hdrHeader
            End If
    
            If bFooters Then
                For Each ftrFooter In .Footers
                    With ftrFooter.Range
                        .Delete
                         For Each fldLoc In .Fields
                            fldLoc.Delete
                         Next fldLoc
                         
                         'GLOG 5083 - table may not get deleted automatically when
                         'there's no trailing paragraph due to corruption
                         If .Tables.Count Then
                            .Tables(1).Delete
                            .StartOf
                            .InsertParagraphAfter
                         End If
                    End With
                Next ftrFooter
            End If
        End With
    End If
End Function

Function bDeleteAllBookmarks(Optional bMacPacOnly _
                    As Boolean = False) As Boolean
                    
'-7/29 this is a slightly different version, which
'-leaves zzmpFixed - prefixed bookmarks alone, as w/ docvars

'   deletes all bookmarks or all
'   MacPac (zzmp) bookmarks

'---safety if boilerplate doc
    
    If InStr(UCase(ActiveDocument.Name), ".ATE") Then
        Exit Function
    End If

    Dim bmkBookmark As Bookmark
    
    If bMacPacOnly Then
        For Each bmkBookmark In ActiveDocument.Bookmarks
            If Left(bmkBookmark.Name, 4) = "zzmp" Then _
                If InStr(bmkBookmark.Name, "zzmpFixed") = 0 Then _
                    bmkBookmark.Delete
        Next bmkBookmark
    Else
        For Each bmkBookmark In ActiveDocument.Bookmarks
            bmkBookmark.Delete
        Next bmkBookmark
    End If
End Function

Function envGetDocEnvironment() As mpDocEnvironment
'fills type mpDocEnvironment with
'current doc environment settings -
'envMpDoc below is a global var

'   some properties are not available
'   in preview mode - this will cause
'   error - hence turn off trapping
    On Error Resume Next
    
    envMpDoc.iProtectionType = ActiveDocument.ProtectionType
    envMpDoc.bTrackChanges = ActiveDocument.TrackRevisions
    
'   get selection parameters
    With Selection
        envMpDoc.iSelectionStory = .StoryType
        envMpDoc.lSelectionStartPos = .Start
        envMpDoc.lSelectionEndPos = .End
    End With
        
    With ActiveDocument.ActiveWindow.ActivePane
        envMpDoc.sVScroll = .VerticalPercentScrolled
        envMpDoc.sHScroll = .HorizontalPercentScrolled
        With .View
            envMpDoc.bShowAll = .ShowAll
            envMpDoc.bFieldCodes = .ShowFieldCodes
            envMpDoc.bBookmarks = .ShowBookmarks
            envMpDoc.bTabs = .ShowTabs
            envMpDoc.bSpaces = .ShowSpaces
            envMpDoc.bParagraphs = .ShowParagraphs
            envMpDoc.bHyphens = .ShowHyphens
            envMpDoc.bHiddenText = .ShowHiddenText
            envMpDoc.bTextBoundaries = .ShowTextBoundaries
            envMpDoc.iView = .Type
        End With
    
    End With
    
    envGetDocEnvironment = envMpDoc
    
End Function

Function rngInsertAt(ByVal xLabel As String, _
                     Optional ByVal xText As String, _
                     Optional ByVal xBPRange As String, _
                     Optional iLineLength As Integer = 0, _
                     Optional bDeletePara As Boolean = False, _
                     Optional ByVal vSearch As Variant, _
                     Optional xDefaultText As String = "") As Word.Range

'replaces first instance of label xLabel
'with xBPRange entry or xText -
'if xText and xBPRange is empty, replaces with line
'of iLineLength characters.  Deletes entire para
'if specified and xText is empty and no line length = 0.
'Returns range of inserted text. Use return value to format.

    Dim rngFind As Word.Range

    If IsMissing(vSearch) Then
        Set rngFind = ActiveDocument.Content
    Else
        Set rngFind = vSearch
    End If
    
    With rngFind.Find
        .Text = xLabel
        .Execute
        If .Found Then
            If xBPRange = "" Then
'               use default text, if no xText
                If xText = "" Then _
                    xText = xDefaultText
                    
'               insert text or underline at
'               label if still no text
                If xText = "" Then
                    xText = String(iLineLength, "_")
                    rngFind.Text = xText
                    
'                   delete para if specified
'                   and no line specified
                    If iLineLength = 0 And bDeletePara Then _
                        rngFind.Paragraphs(1).Range.Text = ""
                        
                Else
                    rngFind.Text = xText
                End If
            Else
'               insert boilerplate entry at label
                rngFind.InsertFile xBP, xBPRange, , , False
            End If
            
            Set rngInsertAt = rngFind
       Else
            Set rngInsertAt = Nothing
        End If
    End With
End Function
Function bSetDocEnvironment(envCur As mpDocEnvironment, _
                            Optional bScroll As Boolean = False, _
                            Optional bSelect As Boolean = False) As Boolean
'   sets the doc/window related vars
'   based on type mpDocEnvironment var (global)

    Dim rngStory As Word.Range
    
'   some properties are not available
'   in preview mode - this will cause
'   error - hence turn off trapping
    On Error Resume Next
    With ActiveWindow.ActivePane
        With .View
            .ShowAll = envCur.bShowAll
            .ShowFieldCodes = envCur.bFieldCodes
            .ShowBookmarks = envCur.bBookmarks
            .ShowTabs = envCur.bTabs
            .ShowSpaces = envCur.bSpaces
            .ShowParagraphs = envCur.bParagraphs
            .ShowHyphens = envCur.bHyphens
            .ShowHiddenText = envCur.bHiddenText
            .ShowTextBoundaries = envCur.bTextBoundaries
            .Type = envCur.iView
        End With
        
        If bSelect Then
            With envCur
                Set rngStory = ActiveDocument.StoryRanges(.iSelectionStory)
                rngStory.SetRange .lSelectionStartPos, .lSelectionEndPos
                rngStory.Select
'               this needs to happen again here
'               because selecting the headers/footers
'               automatically puts you in normal view.
                ActiveWindow.View.Type = .iView
            End With
        End If
        
        If bScroll Then
            .VerticalPercentScrolled = envCur.sVScroll
            .HorizontalPercentScrolled = envCur.sHScroll
        End If
        
    End With
    
    ActiveDocument.TrackRevisions = envCur.bTrackChanges
    
    bSetDocEnvironment = True

End Function
Function bUnlinkHeadersFooters(xSection As Word.Section) As Boolean
            
    On Error Resume Next
    With xSection
        With .Headers(wdHeaderFooterPrimary)
            .LinkToPrevious = False
        End With
        With .Headers(wdHeaderFooterFirstPage)
            .LinkToPrevious = False
        End With
        With .Footers(wdHeaderFooterPrimary)
            .LinkToPrevious = False
        End With
        With .Footers(wdHeaderFooterFirstPage)
            .LinkToPrevious = False
        End With
    End With

End Function

Function bQueryHFLink(xHeader As Boolean, _
                    secSection As Word.Section, _
                    vHF As Variant) As Boolean
    
    Select Case xHeader
        Case True
            With secSection.Headers(vHF)
                bQueryHFLink = .LinkToPrevious
            End With
        Case Else
            With secSection.Footers(vHF)
                bQueryHFLink = .LinkToPrevious
            End With
    End Select
    

End Function
Function bHFIsLinked(hfExisting As HeaderFooter) As Boolean
    bHFIsLinked = hfExisting.LinkToPrevious
End Function


Function bUnlinkAllHeaderFooters(secSection As Word.Section) As Boolean

    On Error Resume Next
    With secSection
        .Headers(wdHeaderFooterFirstPage).LinkToPrevious = False
        .Headers(wdHeaderFooterPrimary).LinkToPrevious = False

        .Footers(wdHeaderFooterFirstPage).LinkToPrevious = False
        .Footers(wdHeaderFooterPrimary).LinkToPrevious = False
   End With

End Function

Function rngAdjustHeaderTabs(rngHeader As Word.Range) As Word.Range
'resets default header tabs
'call after implementing user's margins
    Dim sRight As Single
    
    With rngHeader
        With .Sections(1).PageSetup
            sRight = .PageWidth - .LeftMargin - _
                .RightMargin - .Gutter
        End With
        
        With .ParagraphFormat.TabStops
            .ClearAll
            .Add sRight, wdAlignTabRight
            .Add sRight / 2, wdAlignTabCenter
        End With
    End With
    
    Set rngAdjustHeaderTabs = rngHeader
    
End Function

Function bAdjustCenteredFooter(rngFooter As Range) As Boolean
'in a 3-column table, splits the available space
'equally between columns 1 and 3
    Dim sWidth As Single
    
    With rngFooter
        If .Tables.Count <> 1 Then Exit Function
        
        With .Sections(1).PageSetup
            sWidth = .PageWidth - (.LeftMargin + .RightMargin + .Gutter) - 3
        End With
        
        With .Tables(1)
            If (.Columns.Count <> 3) Or _
                    (.Rows(1).Alignment <> wdAlignRowCenter) Then
                Exit Function
            End If
            
            sWidth = sWidth - .Columns(2).Width
            .Columns(1).Width = sWidth / 2
            .Columns(3).Width = sWidth / 2
        End With
    End With
End Function

Private Sub DeleteHeaderFooterShapes(Optional ByVal lSection As Long = 0, _
                                     Optional ByVal bDoHeaders As Boolean = True, _
                                     Optional ByVal bDoFooters As Boolean = True)
    Dim i As Integer
    Dim oShapes As Word.Shapes
    Dim bDo As Boolean
    Dim iStoryType As WdStoryType
    
    'there's a single shapes collection for all headers and footers
    Set oShapes = ActiveDocument.Sections(1).Headers(wdHeaderFooterPrimary).Shapes
    
    For i = oShapes.Count To 1 Step -1
        'validate section
        bDo = ((lSection = 0) Or (oShapes(i).Anchor.Sections.First.Index = lSection))
        
        'validate story type
        If bDo Then
            iStoryType = oShapes(i).Anchor.StoryType
            Select Case iStoryType
                Case wdEvenPagesHeaderStory, wdFirstPageHeaderStory, wdPrimaryHeaderStory
                    bDo = bDoHeaders
                Case wdEvenPagesFooterStory, wdFirstPageFooterStory, wdPrimaryFooterStory
                    bDo = bDoFooters
                Case Else
                    bDo = False
            End Select
        End If
        
        'delete if appropriate
        If bDo Then
            oShapes(i).Delete
        End If
    Next i
End Sub

Public Function rngAddParaBeforeTable(tblFirst As Word.Table) As Word.Range
'creates a paragraph immediately
'before table tblFirst - use with
'tables that are at bof.
    Dim rngStart As Word.Range
    
    With tblFirst
        .Rows.Add .Rows(1)
        
        Set rngStart = .Range
        rngStart.StartOf
        'Make sure any automatic XML Tags are cleared
        .Rows(1).Range.Delete
        .Rows(1).ConvertToText vbTab
    End With
    rngStart.MoveEndUntil vbCr
    If rngStart.Text <> "" Then     '#3648
        rngStart.Delete
    End If
    Set rngAddParaBeforeTable = rngStart
End Function

Private Sub RestoreMP10BookmarksAfterUnlinking(ByVal oSection As Word.Section, _
                                               ByVal bRetagSource As Boolean)
'GLOG 5264 - after unlinking, bookmarks will be in the preceding section -
'this method can be used to restore them to oSection after inserting a new section before
'bRetagSource parameter added in 9.9.5008
    Dim oHeadFoot As Word.HeaderFooter
    Dim oRange As Word.Range
    Dim oSourceSection As Word.Section
    Dim oBmk As Word.Bookmark
    Dim oBmkRange As Word.Range
    Dim oShape As Word.Shape
    Dim oTextRange As Word.Range
    Dim i As Integer
    Dim oBmkTextRange As Word.Range
    
    On Error GoTo ProcError
    
    If oSection.Index = 1 Then _
        Exit Sub
            
    Set oSourceSection = ActiveDocument.Sections(oSection.Index - 1)
    
    'headers
    For Each oHeadFoot In oSourceSection.Headers
        Set oRange = oHeadFoot.Range
        TransferSegmentBookmarks oRange, oSection.Headers(oHeadFoot.Index).Range, _
            bRetagSource
        
        'shapes
        For i = 1 To oRange.ShapeRange.Count
            Set oShape = oRange.ShapeRange(i)
            If oShape.Type = msoTextBox Then
                Set oTextRange = oShape.TextFrame.TextRange
                'GLOG 5301 (dm) - target shape may not exist in oSection
                Set oBmkTextRange = Nothing
                On Error Resume Next
                Set oBmkTextRange = oSection.Headers(oHeadFoot.Index) _
                    .Range.ShapeRange(i).TextFrame.TextRange
                On Error GoTo ProcError
                If Not oBmkTextRange Is Nothing Then
                    TransferSegmentBookmarks oTextRange, oBmkTextRange, _
                        bRetagSource
                End If
            End If
        Next i
    Next oHeadFoot
    
    'footers
    For Each oHeadFoot In oSourceSection.Footers
        Set oRange = oHeadFoot.Range
        TransferSegmentBookmarks oRange, oSection.Footers(oHeadFoot.Index).Range, _
            bRetagSource
        
        'shapes
        For i = 1 To oRange.ShapeRange.Count
            Set oShape = oRange.ShapeRange(i)
            If oShape.Type = msoTextBox Then
                Set oTextRange = oShape.TextFrame.TextRange
                'GLOG 5301 (dm) - target shape may not exist in oSection
                Set oBmkTextRange = Nothing
                On Error Resume Next
                Set oBmkTextRange = oSection.Footers(oHeadFoot.Index) _
                    .Range.ShapeRange(i).TextFrame.TextRange
                On Error GoTo ProcError
                If Not oBmkTextRange Is Nothing Then
                    TransferSegmentBookmarks oTextRange, oBmkTextRange, _
                        bRetagSource
                End If
            End If
        Next i
    Next oHeadFoot
    
    Exit Sub
ProcError:
    RaiseError "MPTOC90.mpDocument.RestoreMP10BookmarksAfterUnlinking"
    Exit Sub
End Sub

Private Sub RetagmSEG(ByVal oRange As Word.Range, ByVal xOldTag As String)
'added in 9.9.5008 (GLOG 5264)
    Dim oDoc As Word.Document
    Dim xOldID As String
    Dim xNewID As String
    Dim xValue As String
    Dim iIndex As Integer
    Dim xVar As String
    Dim bShowHidden As Boolean
    Dim xNewTag As String

    On Error GoTo ProcError
    
    Set oDoc = ActiveDocument
    
    'retag
    xOldID = Mid$(xOldTag, 4, 8)
    xNewID = GenerateDocVarID(oDoc)
    xNewTag = Left$(xOldTag, 3) & xNewID & Mid$(xOldTag, 12)
    
    'add new doc var with same value as original one
    xValue = oDoc.Variables("mpo" & xOldID).Value
    oDoc.Variables.Add "mpo" & xNewID, xValue
    
    'add new mpd doc vars as necessary
    If xOldTag Like "mps*" Then
        xValue = "x"
        While xValue <> ""
            xValue = ""
            iIndex = iIndex + 1
            xVar = "mpd" & xOldID & String(2 - Len(CStr(iIndex)), "0") & CStr(iIndex)
            On Error Resume Next
            xValue = oDoc.Variables(xVar).Value
            On Error GoTo ProcError
            If xValue <> "" Then
                oDoc.Variables.Add "mpd" & xNewID & _
                    String(2 - Len(CStr(iIndex)), "0") & CStr(iIndex), xValue
            End If
        Wend
    End If
    
    'replace bookmark
    With oDoc.Bookmarks
        bShowHidden = .ShowHidden
        .ShowHidden = True
        .Add "_" & xNewTag, oRange
        .ShowHidden = bShowHidden
    End With
    
    Exit Sub
ProcError:
    RaiseError "MPTOC90.mpDocument.RetagmSEG"
    Exit Sub
End Sub

Private Function GenerateDocVarID(Optional ByVal oDoc As Word.Document) As String
'added in 9.9.5008 (GLOG 5264)
'generates the 8-digit id used to link a content control
'with the doc vars that hold its attributes
    Dim xDocVarID As String
    Dim xValue As String
    
    On Error GoTo ProcError
    
    If oDoc Is Nothing Then _
        Set oDoc = ActiveDocument
        
    xValue = "x"
    While xValue <> ""
        Randomize
        xDocVarID = CStr(CLng(100000000 * Rnd))
        xDocVarID = String(8 - Len(xDocVarID), "0") & xDocVarID
    
        'test for conflict
        xValue = ""
        On Error Resume Next
        xValue = oDoc.Variables("mpo" & xDocVarID).Value
        On Error GoTo ProcError
    Wend
    
    GenerateDocVarID = xDocVarID
    
    Exit Function
ProcError:
    RaiseError "MPTOC90.mpDocument.GenerateDocVarID"
    Exit Function
End Function

Private Sub TransferSegmentBookmarks(ByVal oSourceRange As Word.Range, _
    ByVal oTargetRange As Word.Range, ByVal bRetagSource As Boolean)
'added in 9.9.5008 (GLOG 5264)
    Dim xBmks As String
    Dim vBmks As Variant
    Dim i As Integer
    Dim oBmk As Word.Bookmark
    Dim oBmkRange As Word.Range
    
    On Error GoTo ProcError
    
    'get segment bookmarks in source - get in advance because
    'collection will change in process
    For Each oBmk In oSourceRange.Bookmarks
        If oBmk.Name Like "_mps*" Then _
            xBmks = xBmks & oBmk.Name & "|"
    Next oBmk
    
    If xBmks <> "" Then
        vBmks = Split(xBmks, "|")
        For i = 0 To UBound(vBmks) - 1
            'move bookmark to target range
            Set oBmk = oSourceRange.Bookmarks(vBmks(i))
            Set oBmkRange = oBmk.Range
            With oTargetRange
                .SetRange oBmk.Start, oBmk.End
                .Bookmarks.Add vBmks(i)
            End With
            
            'retag original bookmark range
            If bRetagSource Then _
                RetagmSEG oBmkRange, Mid$(vBmks(i), 2)
        Next i
    End If
    
    Exit Sub
ProcError:
    RaiseError "MPTOC90.mpDocument.TransferSegmentBookmarks"
End Sub


