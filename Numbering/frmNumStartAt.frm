VERSION 5.00
Begin VB.Form frmNumStartAt 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Restart Numbering At"
   ClientHeight    =   1170
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3000
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1170
   ScaleWidth      =   3000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtRestartAt 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   375
      TabIndex        =   1
      Text            =   "1"
      Top             =   604
      Width           =   795
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1755
      TabIndex        =   3
      Top             =   657
      Width           =   1125
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1755
      TabIndex        =   2
      Top             =   200
      Width           =   1125
   End
   Begin VB.Label lblRestartAt 
      Caption         =   $"frmNumStartAt.frx":0000
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   375
      TabIndex        =   0
      Top             =   155
      Width           =   1290
   End
End
Attribute VB_Name = "frmNumStartAt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False






































Option Explicit
Private vbControls() As String
Public m_bFinished As Boolean
Private Sub txtRestartAt_GotFocus()
    bEnsureSelectedContent Me.txtRestartAt
End Sub

Private Sub btnCancel_Click()
    Unload Me
End Sub


Private Sub btnOK_Click()
    Dim iStartAt As Integer
    Dim vRestartAt As Variant
    
    vRestartAt = UCase(Me.txtRestartAt)
    
'   allow alphanumeric restart values only
    If IsNumeric(vRestartAt) Then
        iStartAt = vRestartAt
    ElseIf Asc(vRestartAt) >= 65 And _
            Asc(vRestartAt) <= 90 Then
        iStartAt = Asc(vRestartAt) - 64
    Else
        xMsg = "Not a valid restart value."
        MsgBox xMsg, vbExclamation, AppName
        Me.txtRestartAt.SetFocus
        Exit Sub
    End If
    
    Me.Hide
'    iChangeStartAt iStartAt
'    Unload Me
End Sub



Private Sub Form_Click()
    Me.txtRestartAt.SetFocus
End Sub



