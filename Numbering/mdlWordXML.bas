Attribute VB_Name = "mdlWordXML"
Option Explicit

Public Function SetXMLMarkupState(ByVal oDocument As Word.Document, _
                                  ByVal bShowXML As Boolean) As Long
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    SetXMLMarkupState = oWordXML.SetXMLMarkupState(oDocument, bShowXML)
End Function

Public Function GetTagSafeParagraphStart(ByVal oInsertionRange As Word.Range, _
                                         Optional ByRef iParagraphLevelTagsAtStart As Integer) As Long
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    
    If g_iWordVersion = mpWordVersion_2013 Then 'GLOG 5552
        'custom xml not supported in Word 2013
        GetTagSafeParagraphStart = oInsertionRange.Paragraphs(1).Range.Start
    Else
        GetTagSafeParagraphStart = oWordXML.GetTagSafeParagraphStart(oInsertionRange, _
            iParagraphLevelTagsAtStart)
    End If
End Function

Public Function GetTagSafeParagraphEnd(ByVal oInsertionRange As Word.Range, _
                                       Optional ByRef iParagraphLevelTagsAtEnd As Integer) As Long
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    
    If g_iWordVersion = mpWordVersion_2013 Then 'GLOG 5552
        'custom xml not supported in Word 2013
        GetTagSafeParagraphEnd = oInsertionRange.Paragraphs(1).Range.End - 1
    Else
        GetTagSafeParagraphEnd = oWordXML.GetTagSafeParagraphEnd(oInsertionRange, _
            iParagraphLevelTagsAtEnd)
    End If
End Function

Public Function CursorIsAtStartOfBlockTag() As Boolean
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    
    If g_iWordVersion <> mpWordVersion_2013 Then 'GLOG 5552
        'custom xml not supported in Word 2013
        CursorIsAtStartOfBlockTag = oWordXML.CursorIsAtStartOfBlockTag()
    End If
End Function

Public Sub SetControlFaceID(ByVal oControl As CommandBarControl, ByVal lFaceID As Long)
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    oWordXML.SetControlFaceID oControl, lFaceID
End Sub

Public Function IsStyleSeparator(ByVal oPara As Word.Paragraph) As Boolean
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    IsStyleSeparator = oWordXML.IsStyleSeparator(oPara)
End Function


