Attribute VB_Name = "mpApplication"
Option Explicit
Option Base 1

Public Const KEY_SHIFT = &H10
Public Const KEY_CTL = &H11
Public Const KEY_MENU = &H12
Public Const KEY_F1 = &H70
Public Const KEY_F5 = &H74
Public Const KEY_a = 65
Public Const KEY_DEL = 46
Public Const KEY_ENTER = 13
Public Const KEY_TAB = 9

'TOC / TC Entry
Public Const mpSchemeTypeTCCodeBased As Integer = 1
Public Const mpSchemeTypeStyleBased As Integer = 2
Public Const mpTCPrefix  As String = " TC "

Public Const mpSkipTOCMarking As Integer = -2
Public Const mpRemoveTOCMarking As Integer = -1

Public Const mpTopMarginTOC As Single = 1
Public Const mpBottomMarginTOC As Single = 1
Public Const mpLeftMarginTOC As Single = 1
Public Const mpRightMarginTOC As Single = 1
Public Const mpHeaderDistTOC As Single = 0.75
Public Const mpFooterDistTOC As Single = 0.5

Public Const mpPleadingTOCTopMargin As String = "-1.58"
Public Const mpPleading26TOCTopMargin As String = "-2.02"

Public Const mpMarkedDel As String = "zzmpDel"
Public Const mpTCMarked As String = "|MPTC|"
Public Const mpEndHeading As String = "|MPEH|"
Public Const mpStartPara As String = "|MPSP|"
Public Const mpEndPara As String = "|MPEP|"
Public Const mpStartLevel As String = "|MPSL|"
Public Const mpEndLevel As String = "|MPEL|"
Public Const mpSchemeNameStart As String = "|MPSN|"
Public Const mpSchemeNameEnd As String = "|MPEN|"
Public Const mpPageBreakMarker As String = "|MPPB|"

Public Const mpTCCode_Dyn As String = "\y"
Public Const mpTCCode_PDyn As String = "\u"

Public Const mpSentencePeriodOne As String = ". "
Public Const mpSentencePeriodTwo As String = ".  "
Public Const mpSentenceQuoteOne As String = "."" "
Public Const mpSentenceQuoteTwo As String = ".""  "

Public Const mpTOCLocationBOF = "At Start of Document"
Public Const mpTOCLocationEOF = "At End of Document"
Public Const mpTOCLocationAtInsertion = "At Insertion Point"
Public Const mpTOCLocationAboveTOA = "Above Table of Authorities"
Public Const mpTOCLocationAtCurrent = "At Current TOC Location"

Public Const mpTOCLocationBOFFrench = "Au d�but du document"
Public Const mpTOCLocationEOFFrench = "� la fin du document"
Public Const mpTOCLocationAtInsertionFrench = "Au point de l'insertion"
Public Const mpTOCLocationAboveTOAFrench = "Au-dessus de la Table des r�f�rences"
Public Const mpTOCLocationAtCurrentFrench = "Dans la TM en cours"

Public Enum mpTOCPagePunctuation
    mpTOCPagePunctuationNone = 0
    mpTOCPagePunctuationHyphens = 1
    mpTOCPagePunctuationHyphensWithSpaces = 2
End Enum

Public Enum mpTOCEntryTypes
    mpTOCEntryType_TCEntries = 1
    mpTOCEntryType_Sentence = 2
    mpTOCEntryType_Para = 4
    mpTOCEntryType_PeriodSpace = 6
End Enum

'Word versions
Public Enum mpWordVersions
    mpWordVersion_97 = 8
    mpWordVersion_2000 = 9
    mpWordVersion_XP = 10
    mpWordVersion_2003 = 11
    mpWordVersion_2007 = 12
End Enum

'API declarations for LockWindowUpdate() and FindWindow()
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
Private Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long
Private Declare Function GetTickCount Lib "kernel32" () As Long

Public Declare Function GetSystemDefaultLCID Lib "kernel32" () As Long

Public g_xTOCSchemes() As String

'9.9.5011
Public Declare Function GetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
    ByVal lAction As Long, ByVal lParam As Long, ByRef lpvParam As Long, ByVal lWinIni As Long) As Long
Public Declare Function SetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
    ByVal lAction As Long, ByVal lParam As Long, ByVal lpvParam As Long, ByVal lWinIni As Long) As Long
Public Const SPI_GETKEYBOARDCUES = 4106
Public Const SPI_SETKEYBOARDCUES = 4107

Sub EchoOff()
    Call LockWindowUpdate(FindWindow("OpusApp", 0&))
End Sub

Sub EchoOn()
    Call LockWindowUpdate(0&)
End Sub


Function envGetAppEnvironment() As mpAppEnvironment
'fills type mpAppEnvironment with
'application environment settings -
'envMpApp below is a global var
    On Error Resume Next
    With Application
        envMpApp.lBrowserTarget = .Browser.Target
        .Assistant.Animation = msoAnimationWritingNotingSomething
    End With
        
    envGetAppEnvironment = envMpApp
    
End Function

Function bSetAppEnvironment(envCurrent As mpAppEnvironment, _
                            Optional bClearUndo As Boolean = False) As Boolean
'sets application environment settings
'using values in type mpAppEnvironment

'   added to ensure that Edit Find is always cleared
    bEditFindReset
    
    With Application
        On Error Resume Next
        .Browser.Target = envCurrent.lBrowserTarget
        
        If bClearUndo Then
'           clear "undo" list
            ActiveDocument.UndoClear
        End If
    
'       assistant animation sometimes
'       sticks, so reset
        With .Assistant
            .Animation = msoAnimationIdle
        End With
        On Error GoTo 0
    End With
    
    
    bSetAppEnvironment = True
End Function

Function xAppGetFirmINIValue(xSection As String, _
                             xKey As String) As String
    If xFirmIni = Empty Then
        bAppInitialize
    End If
    
    xAppGetFirmINIValue = _
        System.PrivateProfileString(xFirmIni, xSection, xKey)
                             
End Function

Function bAppGetFileLocations() As Boolean
'   fills global vars with appropriate paths

    xFirmIni = GetAppPath & "\mpN90.INI"
    xUserIni = xUserPath & "\NumTOC.INI"
    xTOCSTY = GetAppPath & "\mpTOC.sty"
    
    If Dir(GetAppPath & "\mpnBoilerpl.ate") <> "" Then
        xBP = GetAppPath & "\mpnBoilerpl.ate"
    Else
        xBP = GetAppPath & "\bp.doc"
    End If
    
End Function

Function bAppInitialize() As Boolean
    Dim bRet As Boolean
    Dim oAddIn As Word.AddIn
    Dim bTest As Boolean
    Dim xValue As String
    
    On Error GoTo ProcError
    
'   get version
    With Word.Application
        If InStr(.Version, "8.") <> 0 Then
            g_iWordVersion = mpWordVersion_97
        ElseIf InStr(.Version, "9.") <> 0 Then
            g_iWordVersion = mpWordVersion_2000
        ElseIf InStr(.Version, "10.") <> 0 Then
            g_iWordVersion = mpWordVersion_XP
        ElseIf InStr(.Version, "11.") <> 0 Then
            g_iWordVersion = mpWordVersion_2003
        Else
            g_iWordVersion = mpWordVersion_2007
        End If
        
        g_bIsXP = (g_iWordVersion > mpWordVersion_2000)
    End With
    
'   determine whether mp10 is loaded
    'GLOG 5551 (9.9.6006) - also check whether COM addin is loaded
    If g_iWordVersion > mpWordVersion_2003 Then
        For Each oAddIn In Word.Application.AddIns
            If UCase$(oAddIn.Name) = "MP10.DOTM" Then
                g_bIsMP10 = mpBase.COMAddInIsLoaded("MacPac102007")
                Exit For
            ElseIf UCase$(oAddIn.Name) = "FORTE.DOTM" Then
                g_bIsMP10 = mpBase.COMAddInIsLoaded("ForteAddIn")
                Exit For
            End If
        Next oAddIn
    End If
    
'   determine whether xml is supported
    If g_iWordVersion > mpWordVersion_XP Then
        On Error Resume Next
        bTest = mdlWordXML.CursorIsAtStartOfBlockTag()
        g_bXMLSupport = (Err <> 6140)
        On Error GoTo ProcError
    End If
    
'   get paths
    bRet = bAppGetPaths()
    
'   get files
    bRet = bAppGetFileLocations()
        
    'get ui language if we don't already have it
    If g_lUILanguage = 0 Then
        xValue = xAppGetFirmINIValue("General", "UILanguage")
        If xValue <> "" Then
            g_lUILanguage = CLng(xValue)
            If g_lUILanguage = 0 Then _
                g_lUILanguage = wdEnglishUS
        End If
    End If
    
    'localize former constants
    If g_lUILanguage = wdFrenchCanadian Then
        g_xTOCStatusMsg = "Veuillez patienter pendant la cr�ation de Table des mati�res..."
    Else
        g_xTOCStatusMsg = "Please wait while the Table of Contents is generating..."
    End If
    
'   get available locations
    bRet = bAppGetTOCLocations()
    
'   get available boilerplates
    bRet = bAppGetArrayFromIni(g_xBoilerplates(), _
        "BoilerplateAlias", "BoilerplateName")
        
'   get available sentence definitions
    bRet = bAppGetArrayFromIni(g_xSentenceDefs(), _
        "SentenceDefDisplay", "SentenceDefText")
    
'   get other options
    bRet = bAppGetOptions()
    
'   load sentence definition "constants"
    g_xSmartOne = "." & ChrW(&H201D) & Chr(32)
    g_xSmartTwo = "." & ChrW(&H201D) & Chr(32) & Chr(32)
    
    'get bullet character
    If GetSystemDefaultLCID = 1041 Then
        'when locale is Japanese, the standard unicode bullet is automatically converted
        'to a Japanese character (&H30FB) that our dialogs can't display -
        'the ascii character works fine
        g_xBullet = Chr(183)
    Else
        g_xBullet = ChrW(&HB7)
    End If
    
    Exit Function
    
ProcError:
    RaiseError "MPTOC90.mpApplication.bAppInitialize"
End Function

Function bAppGetPaths()
'   loads start, work, user and litigation paths
'   into respective global vars

    With Application.Options
'       global in mpVariables
        xStartPath = .DefaultFilePath(wdStartupPath)
        xWorkgroupPath = .DefaultFilePath(wdWorkgroupTemplatesPath)
        
        xUserPath = Word.System.PrivateProfileString(GetAppPath & "\mpN90.ini", _
            "General", "UserDir")
        If (InStr(UCase(xUserPath), "<USERNAME>") > 0) Or _
                (InStr(UCase(xUserPath), "<USER>") > 0) Then
'           use API to get Windows user name
            xUserPath = GetUserVarPath(xUserPath)
        Else
'           use environmental variable
            xUserPath = GetEnvironVarPath(xUserPath)
        End If
        If Dir(xUserPath & "\mpNumbers.sty") = "" Then
'           use Word's default path
            xUserPath = .DefaultFilePath(wdUserTemplatesPath)
        ElseIf Right(xUserPath, 1) = "\" Then
            xUserPath = Left(xUserPath, Len(xUserPath) - 1)
        End If
        
'       litigation path could be different than workgroup path
        xLitigationPath = xWorkgroupPath
    End With
    
End Function

Function bAppPathReInitialize() As Boolean
    If xWorkgroupPath = "" Then
        bRet = bAppGetPaths()
        bRet = bAppGetFileLocations()
    End If
End Function

Function bQuit()
    Application.Quit
End Function

Function bAppSetUserINIValue(xSection As String, _
                             xKey As String, _
                             xValue As String) As Boolean
    System.PrivateProfileString(xUserIni, _
        xSection, xKey) = xValue
End Function

Function xAppGetUserINIValue(xSection As String, _
                             xKey As String) As String
    xAppGetUserINIValue = System.PrivateProfileString(xUserIni, _
        xSection, xKey)
                             
End Function


Public Function RaiseError(xSource As String)
    Dim oError As CError
    Set oError = New CError
    Err.Source = xSource
    oError.Raise Err
    Set oError = Nothing
End Function

Function bAppGetTOCLocations() As Boolean
    Dim xValue As String
    Dim xLocations As String
    
    On Error Resume Next
    ReDim g_xTOCLocations(0 To 0)
        
    xValue = xAppGetFirmINIValue("TOC", "StartOfDocument")
    If UCase(xValue) <> "FALSE" Then
        If g_lUILanguage = wdFrenchCanadian Then
            xLocations = mpTOCLocationBOFFrench & "|"
        Else
            xLocations = mpTOCLocationBOF & "|"
        End If
    End If
    
    xValue = xAppGetFirmINIValue("TOC", "EndOfDocument")
    If UCase(xValue) <> "FALSE" Then
        If g_lUILanguage = wdFrenchCanadian Then
            xLocations = xLocations & mpTOCLocationEOFFrench & "|"
        Else
            xLocations = xLocations & mpTOCLocationEOF & "|"
        End If
    End If
        
    xValue = xAppGetFirmINIValue("TOC", "InsertionPoint")
    If UCase(xValue) <> "FALSE" Then
        If g_lUILanguage = wdFrenchCanadian Then
            xLocations = xLocations & mpTOCLocationAtInsertionFrench & "|"
        Else
            xLocations = xLocations & mpTOCLocationAtInsertion & "|"
        End If
    End If
        
    If Len(xLocations) Then _
        xLocations = Left(xLocations, Len(xLocations) - 1)
        
    bStringToArray xLocations, g_xTOCLocations(), "|"
End Function

Function bAppGetOptions() As Boolean
    Dim xValue As String
    Dim xPreserveLineBreaks As String
    Dim xMP90Dir As String
    
    On Error Resume Next
    g_bApplyHeadingColor = xAppGetFirmINIValue("Numbering", "ApplyColorToHeadings")
    
    xValue = xAppGetFirmINIValue("TOC", "AllowHeaderFooterEdit")
    g_bAllowHeaderFooterEdit = (UCase(xValue) <> "FALSE")
    
    If Not g_bAllowHeaderFooterEdit Then
        g_xPageNoIntroChar = xAppGetFirmINIValue("TOC", _
            "PageNoIntroCharacters")
        g_xPageNoTrailingChar = xAppGetFirmINIValue("TOC", _
            "PageNoTrailingCharacters")
        g_iPageNoStyle = Val(xAppGetFirmINIValue("TOC", _
            "PageNoStyle"))
    End If
    
    g_xTCPrefix = xAppGetFirmINIValue("TOC", "TCPrefix")
    If g_xTCPrefix = "" Then g_xTCPrefix = "TC"
    
'   get common abbreviations to skip as sentence indicators
    bGetCommonAbbrevs g_xAbbreviations()
    
'   default for include schedules/exhibits as level nine
    xValue = xAppGetFirmINIValue("TOC", "IncludeScheduleExhibitStyles")
    g_bIncludeSchedule = (UCase(xValue) = "TRUE")
    
'   get schedule styles
    ReDim g_xScheduleStyles(0 To 0)
    xValue = xAppGetFirmINIValue("TOC", "ScheduleExhibitStyles")
    If xValue <> "" Then
        xValue = xTrimTrailingChrs(xValue, "|", True, True)
        bStringToArray xValue, g_xScheduleStyles(), "|"
    End If
    
'   default for TOC 9 checkbox
    g_bApplyTOC9StyleDefault = xAppGetFirmINIValue("TOC", "ApplyTOC9StyleDefault")
    
'   TOC as field
    g_bAllowTOCAsField = xAppGetFirmINIValue("TOC", "AllowTOCAsField")
    xValue = xAppGetFirmINIValue("TOC", "DefaultToTCEntriesForField")
    g_bForceTCEntries = (UCase(xValue) <> "FALSE")
    xValue = xAppGetFirmINIValue("TOC", "SetDefaultLevelsForField")
    g_bForceLevels = (UCase(xValue) <> "FALSE")
    xPreserveLineBreaks = xAppGetFirmINIValue("TOC", "PreserveLineBreaksInField")
    g_vPreserveLineBreaks = Split(xPreserveLineBreaks, "|")
    
'   default levels
    g_iDefaultLevelStart = xAppGetFirmINIValue("TOC", "DefaultLevelStart")
    If g_iDefaultLevelStart = 0 Then _
        g_iDefaultLevelStart = 1
    g_iDefaultLevelEnd = xAppGetFirmINIValue("TOC", "DefaultLevelEnd")
    If g_iDefaultLevelEnd = 0 Then _
        g_iDefaultLevelEnd = 3
        
    '9.9.2010 - Organizer save prompt
    If InStr(Application.Version, "12.") <> 0 Then
        g_bOrganizerSavePrompt = xAppGetFirmINIValue("General", "Word2007SP2OrganizerSavePrompt")
        If Not g_bOrganizerSavePrompt Then
            'check MacPac.ini for key
            xMP90Dir = mpBase.GetMacPacAppPath
            If xMP90Dir <> "" Then
                xValue = System.PrivateProfileString(xMP90Dir & "MacPac.ini", _
                    "General", "Word2007SP2OrganizerSavePrompt")
                If xValue <> "" Then _
                    g_bOrganizerSavePrompt = CBool(xValue)
            End If
        End If
    End If
    
    '9.9.4007 - added new ini key for organizer save prompt that's
    'not specific to any Word version
    If Not g_bOrganizerSavePrompt Then _
        g_bOrganizerSavePrompt = xAppGetFirmINIValue("General", "OrganizerSavePrompt")
    
    '9.9.4004 - option to not clear undo list
    xValue = xAppGetFirmINIValue("General", "PreserveUndoList")
    g_bPreserveUndoList = (UCase$(xValue) = "TRUE")
    xValue = xAppGetFirmINIValue("TOC", "PreserveUndoListDuringTOCInsertion")
    g_bPreserveUndoListTOC = (UCase$(xValue) = "TRUE")
    
    'GLOG 5113 (9/24/12) - option to continue to use outline level switch
    'when 18 or more styles
    '10/29/12 (dm) - I was wrong about this native limit having been eliminated -
    'the workaround still needs to be used in all circumstances
    g_bUseTOCStyleLimitWorkaround = True
'    xValue = xAppGetFirmINIValue("TOC", "UseTOCStyleLimitWorkaround")
'    g_bUseTOCStyleLimitWorkaround = (UCase$(xValue) = "TRUE")
    
    '9.9.5001
    If g_iWordVersion > mpWordVersion_2003 Then
        'TOC dialog style - check for user setting first
        If g_bAllowTOCAsField Then
            xValue = xAppGetUserINIValue("TOC", "InsertAsFieldDefault")
            If xValue = "" Then _
                xValue = xAppGetFirmINIValue("TOC", "InsertAsFieldDefault")
            If UCase$(xValue) = "TRUE" Then _
                g_iTOCDialogStyle = mpTOCDialogStyle_Field
        End If
        
        'option to mark with style separators - check for user setting first
        xValue = xAppGetUserINIValue("TOC", "MarkWithStyleSeparators")
        If xValue = "" Then _
            xValue = xAppGetFirmINIValue("TOC", "MarkWithStyleSeparators")
        g_bMarkWithStyleSeparators = (UCase$(xValue) = "TRUE")
    Else
        'earlier versions still support only one dialog with optional checkbox for field
        g_iTOCDialogStyle = mpTOCDialogStyle_Legacy
        If g_bAllowTOCAsField Then _
            g_bInsertTOCAsField = xAppGetFirmINIValue("TOC", "InsertAsFieldDefault")
    End If
    
    'option to apply new rules for when and by how much to automatically adjust
    'the TOC indents to alleviate tightness between numbers and headings
    xValue = xAppGetFirmINIValue("TOC", "UseNewTabAdjustmentRules")
    g_bUseNewTabAdjustmentRules = (UCase$(xValue) = "TRUE")
    
    '9.9.5002 - Include default for TOC dialog
    xValue = xAppGetFirmINIValue("TOC", "IncludeInTOCDefault")
    g_iIncludeInTOCDefault = CInt(xValue)
    
    '9.9.5005
    xValue = xAppGetFirmINIValue("TOC", "VariablesInBoilerplate")
    g_bVariablesInBoilerplate = (UCase$(xValue) <> "FALSE")
    xValue = xAppGetFirmINIValue("TOC", "DisplayBenchmarks")
    g_bDisplayBenchmarks = (UCase$(xValue) = "TRUE")
End Function

Function bAppGetArrayFromIni(xArray() As String, _
                             xDisplay As String, _
                             xValue As String) As Boolean
    Dim xRet As String
    Dim iBPCount As Integer
    Dim i As Integer
    
    On Error Resume Next
        
    xRet = "x"
    While xRet <> ""
        iBPCount = iBPCount + 1
        xRet = xAppGetFirmINIValue("TOC", _
            xValue & iBPCount)
    Wend
    iBPCount = iBPCount - 1
    
    If iBPCount > 1 Then
        ReDim xArray(0 To iBPCount - 1, 0 To 1)
        For i = 0 To iBPCount - 1
            xArray(i, 0) = xAppGetFirmINIValue("TOC", _
                xDisplay & (i + 1))
            xArray(i, 1) = xAppGetFirmINIValue("TOC", _
                xValue & (i + 1))
        Next i
    Else
        ReDim xArray(0 To 0, 0 To 1)
    End If
End Function

Function LoadTOCSty() As Boolean
    Dim xTOCSTY As String
    Dim addTOC As Word.AddIn
    Dim i As Integer

'   ensure that mpTOC.sty is loaded
    xTOCSTY = GetAppPath & "\mpTOC.sty"
    
    On Error Resume Next
    Err = 0
    Set addTOC = Word.AddIns(xTOCSTY)
    On Error GoTo ProcError
    
    If Err = 5941 Then
'---    remove any wrong project versions of mpTOC.sty &
'---    add correct version
        For i = Word.AddIns.Count To 1 Step -1
            If InStr(UCase(Word.AddIns.Item(i).Name), "mpTOC.STY") Then
                Word.AddIns.Item(i).Delete
            End If
        Next i
        Err = 0
        Word.AddIns.Add xTOCSTY, True
        Set addTOC = Word.AddIns(xTOCSTY)
    End If
    
    If addTOC Is Nothing Then _
        Set addTOC = Word.AddIns.Add(xTOCSTY)
        
    If Not addTOC.Installed Then _
        Word.AddIns(xTOCSTY).Installed = True
    
    Exit Function
    
ProcError:
    RaiseError "MPTOC90.mpApplication.LoadTOCSty"
End Function

Function UnloadTOCSty() As Boolean
    Dim xTOCSTY As String
    
    On Error Resume Next
    
    xTOCSTY = GetAppPath & "\mpTOC.sty"
    Word.AddIns(xTOCSTY).Installed = False
End Function

'********************************
'Benchmark functions - use these
'instead of Now - they're much
'more precise

Function CurrentTick() As Long
    CurrentTick = GetTickCount()
End Function

Function ElapsedTime(lStartTick As Long) As Single
'returns the time elapsed from lStartTick-
'precision in milliseconds
    ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
End Function
'********************************

Function GetMarkMenuXML() As String
    Dim xXML As String
    
    On Error GoTo ProcError
    
    'build XML string
    xXML = "<menu xmlns=""http://schemas.microsoft.com/office/2006/01/customui"">" & vbLf

    'Mark and Format is now a menu item
    If g_lUILanguage = wdFrenchCanadian Then
        xXML = xXML & "<button id=" & """" & "mnuNumSub3_13" & """" & " label=" & """" & _
            "&amp;Marquer et formater titre (Alt+Maj+N, K)" & """" & " onAction=" & """" & "zzmpCallback" & _
            """" & " tag=" & """" & "zzmpMarkandFormatHeading" & """" & " screentip=" & """" & _
            "Marquer et formater titre (Alt+Maj+N, K)" & """" & "/><menuSeparator id=" & """" & _
            "numSep7" & """" & "/>" & vbLf
            
        xXML = xXML & "<button id=" & """" & _
            "mnuNumSub3_11" & """" & " label=" & """" & "Convertir codes &amp;TM en s�parateurs de style" & _
            """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
            "zzmpTCCodesToStyleSeparators" & """" & " screentip=" & """" & _
            "Convertir codes TM en s�parateurs de style" & """" & "/>" & vbLf & "<button id=" & """" & _
            "mnuNumSub3_12" & """" & " label=" & """" & "Convertir &amp;s�parateurs de style en codes TM" & _
            """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
            "zzmpStyleSeparatorsToTCCodes" & """" & " screentip=" & """" & _
            "Convertir s�parateurs de style en codes TM" & """" & "/>" & vbLf & _
            "<menuSeparator id=" & """" & "numSep6" & """" & "/>" & vbLf
            
        xXML = xXML & "<button id=" & """" & "mnuNumSub3_6" & """" & " label=" & """" & _
            "C&amp;onvertir codes TM MacPac en codes TM Word" & """" & " onAction=" & """" & "zzmpCallback" & _
            """" & " tag=" & """" & "zzmpFillTCCodes" & """" & " screentip=" & """" & _
            "Convertir codes TM MacPac en codes TM Word" & """" & "/>" & vbLf & "<button id=" & """" & _
            "mnuNumSub3_7" & """" & " label=" & """" & "Con&amp;vertir codes TM MacPac en codes TM Word (Format�s)" & _
            """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
            "zzmpFillTCCodesFormatted" & """" & " screentip=" & """" & _
            "Convertir codes TM MacPac en codes TM Word (format�s)" & """" & "/>" & _
            vbLf & "<button id=" & """" & "mnuNumSub3_8" & _
            """" & " label=" & """" & "&amp;R�tablir codes TM Word en codes TM MacPac" & """" & _
            " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
            "zzmpRestoreTCCodes" & """" & " screentip=" & """" & _
            "R�tablir codes TM Word en codes TM MacPac" & """" & "/><menuSeparator id=" & """" & "numSep5" & _
            """" & "/>" & vbLf
            
        xXML = xXML & "<menu id=" & """" & "mpnSwitchMarkingModeMenu" & """" & " label=" & _
            """" & "C&amp;hanger le mode de marquage TM" & """" & ">" & vbLf
        xXML = xXML & "<button id=" & _
            """" & "mpnUseStyleSepsToMark" & """" & " label=" & """" & _
            "Utiliser &amp;s�parateurs de style pour marquer entr�es TM" & """" & _
            " tag=" & """" & "zzmpSwitchMarkingMode" & """"
        If g_bMarkWithStyleSeparators Then
            xXML = xXML & " imageMso=" & """" & "TagMarkComplete" & """"
            xXML = xXML & " enabled=" & """" & "false" & """"
'            xXML = xXML & " screentip=" & """" & "" & """"
        Else
            xXML = xXML & " onAction=" & """" & "zzmpCallback" & """"
            xXML = xXML & " enabled=" & """" & "true" & """"
            xXML = xXML & " screentip=" & """" & "Utiliser s�parateurs de style pour marquer entr�es TM" & """"
        End If
        xXML = xXML & "/>" & vbLf
        xXML = xXML & "<button id=" & _
            """" & "mpnUseTCCodesToMark" & """" & " label=" & """" & _
            "Utiliser codes &amp;TM pour marquer entr�es TM" & """" & _
            " tag=" & """" & "zzmpSwitchMarkingMode" & """"
        If Not g_bMarkWithStyleSeparators Then
            xXML = xXML & " imageMso=" & """" & "TagMarkComplete" & """"
            xXML = xXML & " enabled=" & """" & "false" & """"
'            xXML = xXML & " screentip=" & """" & "" & """"
        Else
            xXML = xXML & " onAction=" & """" & "zzmpCallback" & """"
            xXML = xXML & " enabled=" & """" & "true" & """"
            xXML = xXML & " screentip=" & """" & "Utiliser codes TM pour marquer entr�es TM" & """"
        End If
    Else
        xXML = xXML & "<button id=" & """" & "mnuNumSub3_13" & """" & " label=" & """" & _
            "&amp;Mark and Format Heading (Alt+Shift+N,K)" & """" & " onAction=" & """" & "zzmpCallback" & _
            """" & " tag=" & """" & "zzmpMarkandFormatHeading" & """" & " screentip=" & """" & _
            "Mark and format heading (Alt+Shift+N,K)" & """" & "/><menuSeparator id=" & """" & _
            "numSep7" & """" & "/>" & vbLf
            
        xXML = xXML & "<button id=" & """" & _
            "mnuNumSub3_11" & """" & " label=" & """" & "Convert &amp;TC Codes to Style Separators" & _
            """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
            "zzmpTCCodesToStyleSeparators" & """" & " screentip=" & """" & _
            "Convert TC codes to style separators" & """" & "/>" & vbLf & "<button id=" & """" & _
            "mnuNumSub3_12" & """" & " label=" & """" & "Convert &amp;Style Separators to TC Codes" & _
            """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
            "zzmpStyleSeparatorsToTCCodes" & """" & " screentip=" & """" & _
            "Convert style separators to TC codes" & """" & "/>" & vbLf & _
            "<menuSeparator id=" & """" & "numSep6" & """" & "/>" & vbLf
            
        xXML = xXML & "<button id=" & """" & "mnuNumSub3_6" & """" & " label=" & """" & _
            "C&amp;onvert MacPac TC Codes to Word TC Codes" & """" & " onAction=" & """" & "zzmpCallback" & _
            """" & " tag=" & """" & "zzmpFillTCCodes" & """" & " screentip=" & """" & _
            "Convert MacPac TC codes to Word TC codes" & """" & "/>" & vbLf & "<button id=" & """" & _
            "mnuNumSub3_7" & """" & " label=" & """" & "Con&amp;vert MacPac TC Codes to Word TC Codes (Formatted)" & _
            """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
            "zzmpFillTCCodesFormatted" & """" & " screentip=" & """" & _
            "Convert MacPac TC codes to Word TC codes (formatted)" & """" & "/>" & _
            vbLf & "<button id=" & """" & "mnuNumSub3_8" & _
            """" & " label=" & """" & "&amp;Restore MacPac TC Codes" & """" & _
            " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
            "zzmpRestoreTCCodes" & """" & " screentip=" & """" & _
            "Restore Word TC codes to MacPac TC codes" & """" & "/><menuSeparator id=" & """" & "numSep5" & _
            """" & "/>" & vbLf
            
        xXML = xXML & "<menu id=" & """" & "mpnSwitchMarkingModeMenu" & """" & " label=" & _
            """" & "S&amp;witch TOC Marking Mode" & """" & ">" & vbLf
        xXML = xXML & "<button id=" & _
            """" & "mpnUseStyleSepsToMark" & """" & " label=" & """" & _
            "Use &amp;Style Separators to Mark" & """" & _
            " tag=" & """" & "zzmpSwitchMarkingMode" & """"
        If g_bMarkWithStyleSeparators Then
            xXML = xXML & " imageMso=" & """" & "TagMarkComplete" & """"
            xXML = xXML & " enabled=" & """" & "false" & """"
            xXML = xXML & " screentip=" & """" & "Style separators are currently used to mark TOC entries" & """"
        Else
            xXML = xXML & " onAction=" & """" & "zzmpCallback" & """"
            xXML = xXML & " enabled=" & """" & "true" & """"
            xXML = xXML & " screentip=" & """" & "Use style separators to mark TOC entries" & """"
        End If
        xXML = xXML & "/>" & vbLf
        xXML = xXML & "<button id=" & _
            """" & "mpnUseTCCodesToMark" & """" & " label=" & """" & _
            "Use &amp;TC Codes to Mark" & """" & _
            " tag=" & """" & "zzmpSwitchMarkingMode" & """"
        If Not g_bMarkWithStyleSeparators Then
            xXML = xXML & " imageMso=" & """" & "TagMarkComplete" & """"
            xXML = xXML & " enabled=" & """" & "false" & """"
            xXML = xXML & " screentip=" & """" & "TC codes are currently used to mark TOC entries" & """"
        Else
            xXML = xXML & " onAction=" & """" & "zzmpCallback" & """"
            xXML = xXML & " enabled=" & """" & "true" & """"
            xXML = xXML & " screentip=" & """" & "Use TC codes to mark TOC entries" & """"
        End If
    End If
    
    xXML = xXML & "/>" & vbLf & "</menu>" & vbLf
        
    'add the root closing tag
    xXML = xXML & "</menu>"
    
    'prevent save prompt
    
    GetMarkMenuXML = xXML
    
    Exit Function
ProcError:
    RaiseError "MPTOC90.mpApplication.GetMarkMenuXML"
End Function

Function GetTOCMenuXML() As String
    Dim xXML As String
    
    On Error GoTo ProcError
    
    'build XML string
    xXML = "<menu xmlns=""http://schemas.microsoft.com/office/2006/01/customui"">" & vbLf

    xXML = xXML & "<button id=" & """" & "mnuNumSub3_1" & """" & " label=" & """" & _
        "&amp;Go to MacPac TOC" & """" & " onAction=" & """" & "zzmpCallback" & _
        """" & " tag=" & """" & "zzmpGoToMPTOC" & """" & " screentip=" & """" & _
        "Go to the MacPac TOC" & """" & "/>" & vbLf
        
    xXML = xXML & "<button id=" & """" & _
        "mnuNumSub3_2" & """" & " label=" & """" & "Edit &amp;Custom TOC Scheme" & _
        """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
        "EditCustomScheme" & """" & " screentip=" & """" & _
        "Edit the Custom TOC scheme" & """" & "/>" & vbLf & "<menuSeparator id=" & _
        """" & "numSep1" & """" & "/>" & vbLf & "<button id=" & """" & _
        "mnuNumSub3_3" & """" & " label=" & """" & "&amp;Increase TOC Tab (Ctrl+Alt+Shift+&gt;)" & _
        """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
        "zzmpIncrementTOCTabs" & """" & " screentip=" & """" & _
        "Increase the tab spacing for the selected TOC level (Ctrl+Alt+Shift+&gt;)" & """" & "/>" & vbLf
        
    xXML = xXML & "<button id=" & """" & "mnuNumSub3_4" & """" & " label=" & """" & _
        "&amp;Decrease TOC Tab (Ctrl+Alt+Shift+&lt;)" & """" & " onAction=" & """" & "zzmpCallback" & _
        """" & " tag=" & """" & "zzmpDecrementTOCTabs" & """" & " screentip=" & """" & _
        "Decrease the tab spacing for the selected TOC level (Ctrl+Alt+Shift+&lt;)" & """" & "/>" & vbLf & "<button id=" & """" & _
        "mnuNumSub3_5" & """" & " label=" & """" & "C&amp;hange TOC Tabs" & _
        """" & " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
        "zzmpChangeTOCTabs" & """" & " screentip=" & """" & _
        "Change the tab spacing for specified TOC levels" & """" & "/>" & _
        "<menuSeparator id=" & """" & "numSep3" & """" & "/>" & vbLf & _
        "<button id=" & """" & "mnuNumSub3_9" & _
        """" & " label=" & """" & "De&amp;lete MacPac TOC" & """" & _
        " onAction=" & """" & "zzmpCallback" & """" & " tag=" & """" & _
        "zzmpDeleteMPTOC" & """" & " screentip=" & """" & _
        "Delete the MacPac TOC" & """" & "/>" & vbLf
        
    If g_bAllowTOCAsField Then
        xXML = xXML & "<menu id=" & """" & "mpnSwitchTOCModeMenu" & """" & " label=" & _
            """" & "S&amp;witch TOC Insertion Mode" & """" & ">" & vbLf
        xXML = xXML & "<button id=" & _
            """" & "mpnInsertAsField" & """" & " label=" & """" & _
            "Insert TOC's as &amp;Field Codes" & """" & _
            " tag=" & """" & "zzmpSwitchTOCMode" & """"
        If g_iTOCDialogStyle = mpTOCDialogStyle_Field Then
            xXML = xXML & " imageMso=" & """" & "TagMarkComplete" & """"
            xXML = xXML & " enabled=" & """" & "false" & """"
            xXML = xXML & " screentip=" & """" & "TOC's are currently inserted as field codes" & """"
        Else
            xXML = xXML & " onAction=" & """" & "zzmpCallback" & """"
            xXML = xXML & " enabled=" & """" & "true" & """"
            xXML = xXML & " screentip=" & """" & "Insert TOC's as field codes" & """"
        End If
        xXML = xXML & "/>" & vbLf
        xXML = xXML & "<button id=" & _
            """" & "mpnInsertAsText" & """" & " label=" & """" & _
            "Insert TOC's as Te&amp;xt" & """" & _
            " tag=" & """" & "zzmpSwitchTOCMode" & """"
        If g_iTOCDialogStyle = mpTOCDialogStyle_Text Then
            xXML = xXML & " imageMso=" & """" & "TagMarkComplete" & """"
            xXML = xXML & " enabled=" & """" & "false" & """"
            xXML = xXML & " screentip=" & """" & "TOC's are currently inserted as text" & """"
        Else
            xXML = xXML & " onAction=" & """" & "zzmpCallback" & """"
            xXML = xXML & " enabled=" & """" & "true" & """"
            xXML = xXML & " screentip=" & """" & "Insert TOC's as text" & """"
        End If
        xXML = xXML & "/>" & vbLf & "</menu>" & vbLf
    End If
        
    'add the root closing tag
    xXML = xXML & "</menu>"
    
    'prevent save prompt
    
    GetTOCMenuXML = xXML
    
    Exit Function
ProcError:
    RaiseError "MPTOC90.mpApplication.GetTOCMenuXML"
End Function


