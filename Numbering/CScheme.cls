VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CScheme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_xName As String
Private m_xStyleName As String
Private m_iType As mpSchemeTypes

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let StyleName(xNew As String)
    m_xStyleName = xNew
End Property

Public Property Get StyleName() As String
    StyleName = m_xStyleName
End Property

Public Property Let iType(xNew As mpSchemeTypes)
    m_iType = xNew
End Property

Public Property Get iType() As mpSchemeTypes
    iType = m_iType
End Property


