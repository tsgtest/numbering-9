VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmSchemeProperties 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   3765
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6465
   Icon            =   "frmSchemeProperties.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3765
   ScaleWidth      =   6465
   ShowInTaskbar   =   0   'False
   Begin TrueDBList60.TDBCombo cmbTOCScheme 
      Height          =   315
      Left            =   120
      OleObjectBlob   =   "frmSchemeProperties.frx":058A
      TabIndex        =   5
      ToolTipText     =   "This is the default TOC format for a document that uses this new scheme."
      Top             =   1755
      Width           =   2955
   End
   Begin VB.TextBox txtDescription 
      Height          =   1755
      Left            =   3360
      MaxLength       =   120
      MultiLine       =   -1  'True
      TabIndex        =   7
      Top             =   360
      Width           =   2955
   End
   Begin VB.CheckBox chkAdjustSpacing 
      Caption         =   "&Change single/double line spacing to match that of Normal style for either exact or multiple spacing"
      Height          =   400
      Left            =   180
      TabIndex        =   9
      ToolTipText     =   "Change font name and font size of all levels to the Normal style font."
      Top             =   2615
      Value           =   1  'Checked
      Width           =   3800
   End
   Begin VB.CheckBox chkBaseOnNormal 
      Caption         =   "&Use Normal style font"
      Height          =   300
      Left            =   180
      TabIndex        =   8
      ToolTipText     =   "Change font name and font size of all levels to the Normal style font."
      Top             =   2280
      Value           =   1  'Checked
      Width           =   2115
   End
   Begin VB.TextBox txtSchemeName 
      Height          =   315
      Left            =   120
      TabIndex        =   1
      ToolTipText     =   "This is the name of the scheme as displayed in all MacPac Numbering dialogs."
      Top             =   390
      Width           =   2955
   End
   Begin VB.TextBox txtStyleName 
      Enabled         =   0   'False
      Height          =   315
      Left            =   120
      TabIndex        =   3
      ToolTipText     =   "This is the name that will be used by all styles in the scheme (e.g. 'Article' in 'Article_L1-9')."
      Top             =   1065
      Width           =   2955
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   3975
      TabIndex        =   10
      Top             =   3225
      Width           =   1130
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   5160
      TabIndex        =   11
      Top             =   3225
      Width           =   1130
   End
   Begin VB.Label lblDescription 
      Caption         =   "&Description (up to 120 characters):"
      Height          =   210
      Left            =   3390
      TabIndex        =   6
      Top             =   150
      Width           =   2535
   End
   Begin VB.Label lblTOCScheme 
      Caption         =   "Default &TOC Scheme:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   150
      TabIndex        =   4
      ToolTipText     =   "This is the default TOC format for a document that uses this new scheme."
      Top             =   1545
      Width           =   1695
   End
   Begin VB.Label lblSchemeName 
      Caption         =   "&Scheme Name:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   150
      TabIndex        =   0
      ToolTipText     =   "This is the name of the scheme as displayed in all MacPac Numbering dialogs."
      Top             =   180
      Width           =   1500
   End
   Begin VB.Label lblStyleName 
      Caption         =   "Style Name:"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   150
      TabIndex        =   2
      ToolTipText     =   "This is the name that will be used by all styles in the scheme (e.g. 'Article' in 'Article_L1-9')."
      Top             =   855
      Width           =   1500
   End
   Begin VB.Label lblMsg 
      Caption         =   "###"
      Height          =   435
      Left            =   150
      TabIndex        =   12
      Top             =   3180
      Width           =   3690
   End
End
Attribute VB_Name = "frmSchemeProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancelled As Boolean
Private m_oScheme As CNumScheme
Private m_bReadOnly As Boolean '10/2/12

Public Property Let Scheme(oNew As CNumScheme)
    Set m_oScheme = oNew
End Property

Public Property Get Scheme() As CNumScheme
    Set Scheme = m_oScheme
End Property

Public Property Let ReadOnly(bReadOnly As Boolean)
    m_bReadOnly = bReadOnly
End Property

Public Property Get ReadOnly() As Boolean
    ReadOnly = m_bReadOnly
End Property

Private Sub cmbTOCScheme_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbTOCScheme, Reposition
    Exit Sub
ProcError:
    RaiseError "frmSchemeProperties.cmbTOCScheme_Mismatch"
    Exit Sub
End Sub

Private Sub cmdCancel_Click()
    Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub cmdOK_Click()
'   validate alias
    If Me.txtSchemeName <> Me.Scheme.Alias Then
        If Not bSchemeNameIsValid(Me.txtSchemeName) Then
            Exit Sub
        End If
    End If
    Cancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Activate()
    Dim xTOCField As String
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #Else
        On Error GoTo 0
    #End If

    With Me.Scheme
        On Error Resume Next
        Me.Top = frmSchemes.Top + 743
        Me.Left = frmSchemes.Left + 135
        On Error GoTo 0
        
'       fill controls on form with scheme properties
        Me.txtSchemeName = .Alias
        If (.SchemeType = mpSchemeType_Document) And _
                bIsHeadingScheme(.Name) Then
            Me.txtStyleName = "Heading"
        Else
            Me.txtStyleName = xGetStyleRoot(.Name)
        End If
        If g_bAllowTOCLink Then
            On Error Resume Next
            xTOCField = GetTOCField(.TOCScheme, _
                                    mpTOCRecField_Name, _
                                    mpSchemeType_TOC)
                                    
            MatchCompleteInXArrayList xTOCField, Me.cmbTOCScheme
            
            On Error GoTo 0
'            Me.cmbTOCScheme.ListIndex = .TOCScheme - 1
        End If
        Me.chkBaseOnNormal = .DynamicFonts
        Me.chkAdjustSpacing = .DynamicSpacing
        Me.txtDescription = .Description
        
        If (.SchemeType = mpSchemeType_Private) And Not Me.ReadOnly Then
'            Me.lblMsg.Left = Me.cmdOK.Left
            Me.lblMsg = .Alias & " is a private Scheme."
        Else
'           disable controls if scheme type is not private -
'           dynamic fonts control enabling is set in form load
            Me.lblSchemeName.Enabled = False
            If g_bAllowTOCLink Then
                Me.lblTOCScheme.Enabled = False
                Me.cmbTOCScheme.Enabled = False
            End If
            Me.txtSchemeName.Enabled = False
            Me.chkBaseOnNormal.Enabled = False
            Me.chkAdjustSpacing.Enabled = False
            Me.lblDescription.Enabled = False
            Me.txtDescription.Enabled = False
            Me.cmdOK.Visible = False
            Me.cmdCancel.Caption = "&Close"
            
'           remove hotkeys
            Me.lblSchemeName = xSubstitute(Me.lblSchemeName, "&", "")
            If g_bAllowTOCLink Then _
                Me.lblTOCScheme = xSubstitute(Me.lblTOCScheme, "&", "")
            
'           set message
            If .SchemeType = mpSchemeType_Document Then
                Me.lblMsg = .Alias & " is a document scheme." & _
                          vbCr & "Document scheme properties cannot be edited."
            ElseIf .SchemeType = mpSchemeType_Public Then
                Me.lblMsg = .Alias & " is a public scheme." & _
                          vbCr & "Public scheme properties cannot be edited."
            Else
'                Me.lblMsg.Alignment = vbCenter
                Me.lblMsg = .Alias & " is a Private Scheme."
            End If
        End If
        
'       set form caption
        Me.Caption = " Properties For " & .Alias & " Scheme"
    End With

    Exit Sub
ProcError:
    RaiseError "frmSchemeProperties.Form_Activate"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim oScheme As CNumScheme
    Dim bEnabled As Boolean
    Dim xOrigAlias As String
    Dim bTOCInstalled As Boolean
    Dim xarTOCSchemes As xArray
        
    #If compHandleErrors Then
        On Error GoTo ProcError
    #Else
        On Error GoTo 0
    #End If
    
    Cancelled = True
    
    Me.cmbTOCScheme.Visible = g_bAllowTOCLink
    Me.lblTOCScheme.Visible = g_bAllowTOCLink
    
    If g_bAllowTOCLink Then
'       first ensure TOC link - may not be there if
'       Word was started with a protected document
        lInitializeTOCLink
                        
        Set xarTOCSchemes = New xArray
        xarTOCSchemes.ReDim 0, UBound(g_xTOCSchemes), 0, 1
        For i = LBound(g_xTOCSchemes) To UBound(g_xTOCSchemes)
            xarTOCSchemes(i, 0) = g_xTOCSchemes(i, 0)
            xarTOCSchemes(i, 1) = g_xTOCSchemes(i, 1)
        Next i
        Me.cmbTOCScheme.Array = xarTOCSchemes
        ResizeTDBCombo Me.cmbTOCScheme, 4
    End If
    
    Exit Sub
ProcError:
    RaiseError "frmSchemeProperties.Form_Load"
    Exit Sub
End Sub
