Attribute VB_Name = "mdlCC"
Option Explicit

Public Function BeforeBlockLevelCC() As Boolean
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    BeforeBlockLevelCC = oCC.BeforeBlockLevelCC
End Function

Public Function AfterBlockLevelCC() As Boolean
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    AfterBlockLevelCC = oCC.AfterBlockLevelCC
End Function

Public Function GetCCSafeParagraphStart(ByVal oInsertionRange As Word.Range) As Long
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    GetCCSafeParagraphStart = oCC.GetCCSafeParagraphStart(oInsertionRange)
End Function

Public Function GetCCSafeParagraphEnd(ByVal oInsertionRange As Word.Range) As Long
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    GetCCSafeParagraphEnd = oCC.GetCCSafeParagraphEnd(oInsertionRange)
End Function

Public Function AddUnlinkedParagraphStyle(ByVal oDoc As Word.Document, _
                                          ByVal xName As String) As Word.Style
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    Set AddUnlinkedParagraphStyle = oCC.AddUnlinkedParagraphStyle(oDoc, xName)
End Function

Public Sub ToggleRibbon()
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    oCC.ToggleRibbon
End Sub

Public Function AddTempParaToContentControl(ByVal oRange As Word.Range) As Boolean
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    AddTempParaToContentControl = oCC.AddTempParaToContentControl(oRange)
End Function
