Attribute VB_Name = "mpDocVars"
Option Explicit

Function bDocVarsManage() As Long
'   displays Manage DocVars form
    Dim frmDocVars As New frmDocVars
    frmDocVars.Show vbModal
    bDocVarsManage = Err
End Function



Function sGetSDPText(rngRange As Word.Range, _
                Optional xStyleName As String = "Delivery Phrase") As String
                
    Dim stySDP As Style
    
'   check for existence of xStyleName
    On Error Resume Next
    Set stySDP = ActiveDocument.Styles(xStyleName)
    
'   if style xStyleName does not exist,
'   check for style SDP
    If stySDP Is Nothing Then _
        Set stySDP = ActiveDocument.Styles("SDP")
        
    If stySDP Is Nothing Then _
        Exit Function
        
    xStyleName = stySDP.NameLocal
    
    On Error GoTo 0
    
    With rngRange.Find
        .ClearFormatting
        .Style = xStyleName
        .Text = ""
        .Execute
        If .Found Then
            sGetSDPText = rngRange.Text
            .ClearFormatting
            Exit Function
        End If
    End With

End Function


Public Function DeleteAllDocVars(Optional bDeleteAll = False)
    Dim oDocVar As Variable
    
    For Each oDocVar In ActiveDocument.Variables
        If bDeleteAll = True Then
            oDocVar.Delete
        Else
            If Left(oDocVar.Name, 9) <> "zzmpFixed" Then
                oDocVar.Delete
            End If
        End If
    Next oDocVar
End Function
Function sGetSDP(rngRange As Word.Range, _
                vDocVar As Variant, _
                Optional xStyleName As String = "Delivery Phrase") As String
                
    Dim stySDP As Style
    
'   check for existence of xStyleName
    On Error Resume Next
    Set stySDP = ActiveDocument.Styles(xStyleName)
    
'   if style xStyleName does not exist,
'   check for style SDP
    If stySDP Is Nothing Then _
        Set stySDP = ActiveDocument.Styles("SDP")
        
    If stySDP Is Nothing Then _
        Exit Function
        
    xStyleName = stySDP.NameLocal
    
    On Error GoTo 0
    
    With rngRange.Find
        .ClearFormatting
        .Style = xStyleName
        .Text = ""
        .Execute
        If .Found Then
            sGetSDP = xSubstitute(rngRange.Text, Chr(11), vbCrLf)
            .ClearFormatting
            Exit Function
        End If
    End With

End Function
Function bDeleteDocVar(xDocVar As String) As Boolean
    Dim oDocVar As Variable
    For Each oDocVar In ActiveDocument.Variables
        If oDocVar.Name = xDocVar Then
            oDocVar.Delete
            Exit Function
        End If
    Next oDocVar
    
End Function

'Public Sub SetDocVars(ctlControls As Controls, _
'                      Optional bMarkAsRestarted As Boolean = True)
'    Dim ctlControl As Control
'    Dim xName As String
'    Dim vValue As Variant
'    Dim varReUse As Variable
'
'    For Each ctlControl In ctlControls
'        If ControlHasValue(ctlControl) Then
'                On Error Resume Next
'                xName = ctlControl.Name
'                vValue = ctlControl
'
''               account for null values for
''               chkboxes and toggles
'                If InStr(xName, "chk") Or InStr(xName, "tgl") Then
'                    If Not vValue Then
'                        vValue = False
'                    Else
'                        vValue = True
'                    End If
'                End If
'
''               if doc var exists, set value,
''               else add doc var
'                With ActiveDocument
'                    On Error Resume Next
'                    Set varReUse = Nothing
'                    Set varReUse = .Variables(xName)
'                    On Error GoTo 0
'
'                    If varReUse Is Nothing Then
'                        .Variables.Add xName, xNullToString(vValue)
'                    Else
'                        varReUse.Value = xNullToString(vValue)
'                    End If
'                End With
'        End If
'    Next ctlControl
'
''   mark as restarted
'    If bMarkAsRestarted Then _
'        ActiveDocument.Variables("Restarted") = True
'End Sub
'
'Public Sub GetDocVars(ctlControls As Controls)
''assigns stored doc variable values to
''controls of same name - forces author list
''controls to evaluate first to avoid change
''events of those controls from affecting
''the values of other controls
'
'    Dim ctlControl As Control
'    Dim vCurValue As Variant
'    Dim xPriorityControls(1 To 6) As String
'    Dim bIsPriorityControl As Boolean
'    Dim i As Integer
'    Dim xName As String
'
''   enumerate trouble controls - this is the only part,
''   if any, that should require modification
'    xPriorityControls(1) = "cmbAuthorLists"
'    xPriorityControls(2) = "lstAttyList"
'    xPriorityControls(3) = "cmbAuthor"
'    xPriorityControls(4) = "cmbPrefLists"
'    xPriorityControls(5) = "cmbSetAuthorPref"
'    xPriorityControls(6) = "cmbOptOffices"  'needed for Verification
'
''   run through priority controls, setting their values-
''   remember, change events will occur, but they should
''   be irrelevant after non-priority controls are set below
'    For i = 1 To UBound(xPriorityControls)
'        On Error Resume Next
'        Set ctlControl = ctlControls(xPriorityControls(i))
'        vCurValue = ActiveDocument.Variables(ctlControl.Name)
'        If Not IsEmpty(vCurValue) Then
'            SkipListUpdate = True
'            ctlControl = vCurValue
'        End If
'    Next i
'
'    On Error GoTo 0
'
''   run through non-priority controls,
''   setting their values
'    For Each ctlControl In ctlControls
'        If ControlHasValue(ctlControl) Then
'            xName = ctlControl.Name
'
'            bIsPriorityControl = _
'                (xName = xPriorityControls(1)) Or _
'                (xName = xPriorityControls(2)) Or _
'                (xName = xPriorityControls(3)) Or _
'                (xName = xPriorityControls(4)) Or _
'                (xName = xPriorityControls(5)) Or _
'                (xName = xPriorityControls(6))
'
'            If Not bIsPriorityControl Then
'                On Error Resume Next
'                vCurValue = ActiveDocument.Variables(ctlControl.Name)
'                If Not IsEmpty(vCurValue) Then
'                    SkipListUpdate = True
'                    ctlControl = vCurValue
'                End If
'            End If
'        End If
'    Next ctlControl
'End Sub



Public Sub SetArrayDocVars(PackedArray)
    On Error GoTo AddVar
    ActiveDocument.Variables("packedarray") = PackedArray
    Exit Sub
AddVar:
    ActiveDocument.Variables.Add "packedarray", PackedArray
    Resume
End Sub
Public Sub GetArrayDocVars(xDocVarName As String, _
                           PackedArray)
   On Error Resume Next
    PackedArray = ActiveDocument _
        .Variables(xDocVarName)
End Sub
Function DocVarsCollect(ctlControls As Controls)
'--- utility used to generate a hard copylist of controls in the project
    Dim ctlControl As Control
    
    For Each ctlControl In ctlControls
         ActiveDocument.Range(Start:=0, End:=0).InsertAfter ctlControl.Name + vbCr
    Next ctlControl

End Function

Public Sub SetDocVars(frmP As Form, _
                      Optional bMarkAsRestarted As Boolean = True)
    
    Dim ctlControl As VB.Control
    Dim xName As String
    Dim vValue As Variant
    Dim varReuse As Variable
    
    For Each ctlControl In frmP.Controls
        If ControlHasValue(ctlControl) Then
                On Error Resume Next
'---        assign true false values for chkboxes, tglbuttons, Optionbuttons
                xName = ctlControl.Name
                vValue = ctlControl
                If InStr(xName, "chk") Or InStr(xName, "tgl") Or InStr(xName, "opt") Then
                    If vValue = False Then
                        vValue = "False"
                    Else
                        vValue = "True"
                    End If
                End If
                
'               if doc var exists, set value, else add doc var
                With ActiveDocument
                    On Error Resume Next
                    Set varReuse = Nothing
                    Set varReuse = .Variables(xName)
                    On Error GoTo 0

                    If varReuse Is Nothing Then
                        .Variables.Add xName, xNullToString(vValue)
                    Else
                        varReuse.Value = xNullToString(vValue)
                    End If
                End With
        End If
    Next ctlControl
    
'   mark as restarted
    If bMarkAsRestarted Then _
        ActiveDocument.Variables("Restarted") = True
End Sub

Public Sub GetDocVars(frmP As Form)
'assigns stored doc variable values to
'controls of same name - forces author list
'controls to evaluate first to avoid change
'events of those controls from affecting
'the values of other controls

    Dim ctlControl As Control
    Dim vCurValue As Variant
    Dim xPriorityCtls(1 To 9) As String
    Dim bIsPriorityControl As Boolean
    Dim i As Integer
    Dim xName As String
    Dim SkipListUpdate As Boolean
    
'   enumerate trouble controls - this is the only part,
'   if any, that should require modification
    xPriorityCtls(1) = "cmbAuthorLists"
    xPriorityCtls(2) = "lstAttyList"
    xPriorityCtls(3) = "cmbAuthor"
    xPriorityCtls(4) = "cmbPrefLists"
    xPriorityCtls(5) = "cmbSetAuthorPref"
    xPriorityCtls(6) = "cmbOptOffices"  'needed for Verification
    xPriorityCtls(7) = "cmbRecipient"
    xPriorityCtls(8) = "cmbRecipientLists"
    xPriorityCtls(9) = "cmbState"  'needed for Certificate of Service
    
'   run through priority controls, setting their values-
'   remember, change events will occur, but they should
'   be irrelevant after non-priority controls are set below
    For i = 1 To UBound(xPriorityCtls)
        On Error Resume Next
        Set ctlControl = frmP.Controls(xPriorityCtls(i))
        vCurValue = ActiveDocument.Variables(ctlControl.Name)
        If Not IsEmpty(vCurValue) Then
            SkipListUpdate = True
            ctlControl = vCurValue
        End If
    Next i
    
    On Error GoTo 0
    
'   run through non-priority controls,
'   setting their values
    For Each ctlControl In frmP.Controls
        If ControlHasValue(ctlControl) Then
            xName = ctlControl.Name
            
            bIsPriorityControl = _
                (xName = xPriorityCtls(1)) Or _
                (xName = xPriorityCtls(2)) Or _
                (xName = xPriorityCtls(3)) Or _
                (xName = xPriorityCtls(4)) Or _
                (xName = xPriorityCtls(5)) Or _
                (xName = xPriorityCtls(6)) Or _
                (xName = xPriorityCtls(7)) Or _
                (xName = xPriorityCtls(8)) Or _
                (xName = xPriorityCtls(9))
                
            If Not bIsPriorityControl Then
                On Error Resume Next
                    vCurValue = ActiveDocument.Variables(ctlControl.Name)
                If Not IsEmpty(vCurValue) Then
                    SkipListUpdate = True
                    If InStr(xName, "chk") Then
                        ctlControl = Abs(CBool(vCurValue))
                    Else
                        ctlControl = vCurValue
                    End If
                End If
            End If
        End If
    Next ctlControl
End Sub
    

