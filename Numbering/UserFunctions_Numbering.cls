VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UserFunctions_Numbering"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Type DocumentSchemesInfo
    ActiveSchemeName As String
    ActiveSchemeDisplayName As String
    ActiveSchemeLevels As Integer
    Count As Integer
    DefaultIsLoaded As Boolean
End Type

Function zzmpShowModify() As Long

End Function
Function zzmpEditPaste() As Long
'    If bReadyToGo() Then
'        zzmpEditPaste = EditPaste()
'    End If
End Function

Function zzmpDocChange() As Long
    If bReadyToGo(False, False, False, False, False) Then
        zzmpDocChange = DocumentChange()
    End If
End Function

Function zzmpConvertNumDoc() As Long
    If bReadyToGo(False, False, True, True, False) Then
        zzmpConvertNumDoc = ConvMPN80To90()
    End If
End Function

Function IsMacPacScheme(ByVal xScheme As String, _
                        Optional iSchemeType As mpSchemeTypes = mpSchemeType_Private, _
                        Optional oSource As Object) As Boolean
'returns true if xScheme is the name of a MacPac
'numbering scheme in oSource
    If bReadyToGo(True, False, False, True, True) Then
        IsMacPacScheme = mpBase.IsMacPacScheme(xScheme, _
                                               iSchemeType, _
                                               oSource)
    End If
End Function

Function InitializeNumbering() As Long
    InitializeNumbering = lInitializeNumbering()
End Function

Function zzmpContinueFromPrevious() As Long
    If bReadyToGo(True, False, True, False, True) Then
        Word.Application.ScreenUpdating = False
        iChangeStartAt mpContinueFromPrevious
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpDemoteLevel() As Long
    If bReadyToGo(True, False, True, False, True) Then
        Application.ScreenUpdating = False
        iChangeLevel 1
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpPromoteLevel() As Long
    If bReadyToGo(True, False, True, False, True) Then
        Application.ScreenUpdating = False
        iChangeLevel -1
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpRemoveNumbers() As Long
    If bReadyToGo(True, False, True, False, True) Then
        iRemove Selection.Range
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpRestartAt() As Long
    If bReadyToGo(True, False, True, False, True) Then
        Application.ScreenUpdating = False
        iChangeStartAt 0
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpSetScheme() As Long
    If bReadyToGo(True, False, True, True, True) Then
'       disable mp10 event handling
        If g_bIsMP10 Then _
            Word.Application.Run "zzmpSuppressXMLEventHandling"
            
        bRet = bPrepareForSchemesDialog()
        If Not bRet Then _
            Exit Function
        g_iSchemeEditMode = mpModeSchemeMain
        ShowSchemesDialogs
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpLevel1Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel1Insert = bInsertNumberFromToolbar(1)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpLevel2Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel2Insert = bInsertNumberFromToolbar(2)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpLevel3Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel3Insert = bInsertNumberFromToolbar(3)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpLevel4Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel4Insert = bInsertNumberFromToolbar(4)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpLevel5Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel5Insert = bInsertNumberFromToolbar(5)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpLevel6Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel6Insert = bInsertNumberFromToolbar(6)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpLevel7Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel7Insert = bInsertNumberFromToolbar(7)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpLevel8Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel8Insert = bInsertNumberFromToolbar(8)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpLevel9Insert() As Long
    Dim bState As Boolean
    
    If bReadyToGo(True, True, True, False, True) Then
        Application.ScreenUpdating = False
        
'       if NUM LOCK is ON, turn OFF
        bState = bGetNumLockState()
        If bState = True Then _
            lSetNumLockState True, False
        
        zzmpLevel9Insert = bInsertNumberFromToolbar(9)
        
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear

'       if NUM LOCK was ON, turn back ON
        If bState = True Then _
            lSetNumLockState False, True
    End If
End Function

Function zzmpUnderLineHeadingToggle() As Long
    If bReadyToGo(True, False, True, False, True) Then
'       disable mp10 event handling
        If g_bIsMP10 Then _
            Word.Application.Run "zzmpSuppressXMLEventHandling"
            
        bUnderLineHeadingToggle
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
            
'       restore mp10 event handling
        If g_bIsMP10 Then _
            Word.Application.Run "zzmpResumeXMLEventHandling"
    End If
End Function

Function zzmpSetFormatHeadingsType()
    If bReadyToGo(False, False, True, False, True) Then
        bToggleFormatHeadings
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpFixCorruption()
    With Application
        .ScreenUpdating = False
        If g_lUILanguage = wdFrenchCanadian Then
            .StatusBar = "R�tabli styles aux niveaux de liste.  Veuillez patienter..."
        Else
            .StatusBar = "Relinking styles to list levels.  Please wait..."
        End If
    
        lRet = mdlN80.iRelinkAllListTemplateStyles
        
        .ScreenUpdating = True
        If g_lUILanguage = wdFrenchCanadian Then
            .StatusBar = "Termin�.  Nombre de th�mes v�rifi�: " & lRet
        Else
            .StatusBar = "Finished.  Number of schemes checked: " & lRet
        End If
        mdlApplication.SendShiftKey
    End With
End Function

Private Function bReadyToGo(ByVal bOnlyIfActiveDocument As Boolean, _
                            ByVal bOnlyIfActiveSchemeExists As Boolean, _
                            ByVal bOnlyIfDocUnprotected As Boolean, _
                            ByVal bLoadStyFiles As Boolean, _
                            ByVal bDoConversions As Boolean) As Boolean
'returns TRUE iff Word environment
'is ready to fun Numbering functions
    Dim xValue As String
    Dim bDelete As Boolean
    Dim oAddIn As Word.AddIn

    '9.9.3 - attempt to get startup template name and ui language immediately -
    'this will allow us to show pre-initialization messages in the correct language
    If (g_lUILanguage = 0) And ((InStr(Application.Version, "12.") <> 0) Or _
            (InStr(Application.Version, "14.") <> 0) Or _
            (InStr(Application.Version, "15.") <> 0)) Or _
            (InStr(Application.Version, "16.") <> 0) Then
        g_lUILanguage = mpBase.GetLanguageFromStartupTemplate
        If g_lUILanguage = wdEnglishUS Then
            g_xMPNDot = "MacPac Numbering.dotm"
        ElseIf g_lUILanguage = wdFrenchCanadian Then
            g_xMPNDot = "MacPac Numbering French.dotm"
        End If
    End If
    
    '9.9.5003
    If g_xAppName = "" Then
        If g_lUILanguage = wdFrenchCanadian Then
            g_xAppName = "Num�rotation MacPac"
        Else
            g_xAppName = AppName
        End If
    End If
    
    If bOnlyIfActiveDocument Then
''       false if schemes btn is disabled
'        If Not CommandBars(mpNumberingToolbar) _
'            .Controls(mpCtlSchemes).Enabled Then _
'                Exit Function
        If Documents.Count = 0 Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Vous devez ouvrir un document avant d'utiliser cette fonction.", _
                    vbInformation, g_xAppName
            Else
                MsgBox "Please open a document before running this function.", _
                    vbInformation, g_xAppName
            End If
            Exit Function
        End If
    End If
            
    If bOnlyIfDocUnprotected Then
'       false if doc is protected
        If mpBase.bDocProtected(ActiveDocument, True) Then _
            Exit Function
    End If
        
'   reinitialize if necessary, if not necessary,
    If g_xPNumSty = "" Then
        If Not bAppInitialize() Then
'           false if init failed
            Exit Function
        End If
    End If
    
'   ensure that all files are loaded
    If bLoadStyFiles Then
        LoadStyFiles
'        If Not bLoadFiles(g_bAllowTOCLink) Then
''           false if load failed
'            Exit Function
'        End If
    End If
    
'   6/30/09 - check for doc var corruption caused by KB969604
    mpBase.DeleteCorruptedVariables
    
'   run conversions
    If bDoConversions Then
        RenameActiveLTFromVar
        xActivateFirstScheme
        If Not bIsConverted() Then
            DoConversions
        End If
    End If

'   ensure active scheme
    If bOnlyIfActiveSchemeExists Then
''       false if insert level 1 is disabled
'        If Not CommandBars(mpNumberingToolbar) _
'            .Controls(mpCtlLevel1).Enabled Then _
'                Exit Function
        If xActiveScheme(ActiveDocument) = "" Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Aucun th�me actif dans ce document.  Cliquez sur le bouton Th�mes pour s�lectionner un th�me.", vbInformation, g_xAppName
            Else
                MsgBox "There is no active scheme in this document.  " & _
                    "Click the Schemes button to select a " & _
                    "scheme to use.", vbInformation, g_xAppName
            End If
            Exit Function
        End If
    End If
            
    bReadyToGo = True
End Function

Function zzmpUnregisterVersionSpecificDLLs() As Long
    On Error Resume Next
        
    If mpDeveloperEnvironment Then _
        Exit Function
        
    Shell "regsvr32 /u /s " & """" & _
        App.Path & "\mpWD80.dll" & """"
    Shell "regsvr32 /u /s " & """" & _
        App.Path & "\mpWD90.dll" & """"
    Shell "regsvr32 /s " & """" & _
        App.Path & "\mpWDXX.dll" & """"
        
'    lRet = SetVersionFiles()
End Function

Function zzmpWorkAsAdmin() As Long
    If bReadyToGo(True, False, True, True, True) Then
'       warn about editing in Word 2000/XP
        If g_bPost97AdminWarning And (InStr(Application.Version, "8.") = 0) Then
            If g_lUILanguage = wdFrenchCanadian Then
                lRet = MsgBox("Si vous avez des utilisateurs MacPac Numbering qui travaillent dans Word 97, nous vous recommandons d'untiliser cette fonctionnalit� seulement dans Word 97." & _
                    vbCr & vbCr & "D�sirez vous continuer ?", _
                    vbYesNo + vbQuestion + vbDefaultButton2, g_xAppName)
            Else
                lRet = MsgBox("If you have any MacPac Numbering users working in Word 97, " & _
                    "we strongly recommend that you use this utility in Word 97 only." & _
                    vbCr & vbCr & "Do you wish to continue anyway?", _
                    vbYesNo + vbQuestion + vbDefaultButton2, g_xAppName)
            End If
            If lRet = vbNo Then
                UnLoadStyFiles
                Exit Function
            End If
        End If
        
        bRet = bPrepareForSchemesDialog()
        If Not bRet Then _
            Exit Function
        WorkAsAdmin
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpActivateScheme(ByVal docP As Word.Document, _
                            ByVal xKey As String) As Long
    If bReadyToGo(True, False, True, False, True) Then
        zzmpActivateScheme = ActivateScheme(xKey)
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function Path() As String
    Path = App.Path
End Function

Function zzmpDoConversions()
'runs conversion code if necessary
    bReadyToGo True, False, True, False, True
    If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
        ActiveDocument.UndoClear
End Function

Function zzmpUseScheme(ByVal xScheme As String, _
                       ByVal bReset As Boolean) As Long
    If bReadyToGo(True, False, True, True, True) Then
        zzmpUseScheme = UseSchemeExternal(xScheme, bReset, False)
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpPromoteToNumberedStyle() As Long
    If bReadyToGo(True, False, True, False, True) Then
        Application.ScreenUpdating = False
        zzmpPromoteToNumberedStyle = NumberContSwitch(True)
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpDemoteToContStyle() As Long
    If bReadyToGo(True, False, True, False, True) Then
        Application.ScreenUpdating = False
        zzmpDemoteToContStyle = NumberContSwitch(False)
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpHideParagraph() As Long
    If bReadyToGo(True, False, True, False, True) Then
        Application.ScreenUpdating = False
        zzmpHideParagraph = HideParagraph
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpConvertToMacPac() As Long
    If bReadyToGo(True, False, True, False, True) Then
        Application.ScreenUpdating = False
        zzmpConvertToMacPac = ConvertToMacPac
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpSyncContStyles(xScheme As String) As Long
    If bReadyToGo(True, False, True, False, True) Then
        CreateContStyles xScheme
        '9.9.5001
        UpdateParaStyles xScheme
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpConvertStyles(xScheme As String, _
                           Optional bShowMessage As Boolean = True) As Long
    Dim xRoot As String
    Dim iSchemes As Integer
    Dim oForm As VB.Form
    
    If bReadyToGo(True, False, True, False, True) Then
        '10/4/12 - user is no longer required to select scheme
        If xScheme = "" Then
            iSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
            If iSchemes = 0 Then
                If g_lUILanguage = wdFrenchCanadian Then
                    MsgBox "Aucun th�me de num�rotation MacPac dans ce document.", vbInformation, g_xAppName
                Else
                    MsgBox "There are no MacPac numbering schemes " & _
                        "in this document.", vbInformation, g_xAppName
                End If
            ElseIf iSchemes = 1 Then
                xScheme = g_xDSchemes(0, 0)
            Else
                Set oForm = New frmSchemeSelector
                oForm.Show vbModal
                If Not oForm.Cancelled Then
                    xScheme = g_xDSchemes(oForm.lstSchemes.ListIndex, 0)
                End If
                Set oForm = Nothing
            End If
        End If
        
        If xScheme <> "" Then
            Application.ScreenUpdating = False
            xRoot = xGetLTRoot(xScheme)
            If bIsHeadingScheme(xRoot) Then
                bConvertToMacPacStyles xRoot
            Else
                bConvertToHeadingStyles xRoot
            End If
            If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
                ActiveDocument.UndoClear
        End If
    End If
End Function

Function zzmpShowHelpAbout() As Long
    '9.9.3 - attempt to get startup template name and ui language immediately -
    'this will allow us to show pre-initialization messages in the correct language
    If (g_lUILanguage = 0) And ((InStr(Application.Version, "12.") <> 0) Or _
            (InStr(Application.Version, "14.") <> 0) Or _
            (InStr(Application.Version, "15.") <> 0)) Or _
            (InStr(Application.Version, "16.") <> 0) Then
        g_lUILanguage = mpBase.GetLanguageFromStartupTemplate
        If g_lUILanguage = wdEnglishUS Then
            g_xMPNDot = "MacPac Numbering.dotm"
        ElseIf g_lUILanguage = wdFrenchCanadian Then
            g_xMPNDot = "MacPac Numbering French.dotm"
        End If
    End If
    
    '9.9.5003
    If g_xAppName = "" Then
        If g_lUILanguage = wdFrenchCanadian Then
            g_xAppName = "Num�rotation MacPac"
        Else
            g_xAppName = AppName
        End If
    End If
    
    If g_lUILanguage = wdFrenchCanadian Then
        frmAboutNewFrench.Show vbModal
    Else
        frmAboutNew.Show vbModal
    End If
End Function

Function zzmpShowHelpContents() As Long
    On Error GoTo ProcError
    HtmlHelp 0, App.Path & "\Fullhelp.chm", HH_DISPLAY_TOC, 0
    Exit Function
ProcError:
    RaiseError mpError_CouldNotOpenFAQ
    Exit Function
End Function

Function zzmpShowHelpFAQ() As Long
    On Error GoTo ProcError
    HtmlHelp 0, App.Path & "\FAQ.chm", HH_DISPLAY_TOC, 0
    Exit Function
ProcError:
    RaiseError mpError_CouldNotOpenFAQ
    Exit Function
End Function

Function zzmpShowHelpQuickSteps() As Long
    On Error GoTo ProcError
    HtmlHelp 0, App.Path & "\QuickSteps.chm", HH_DISPLAY_TOC, 0
    Exit Function
ProcError:
    RaiseError mpError_CouldNotOpenFAQ
    Exit Function
End Function

Function zzmpEditScheme(ByVal xScheme As String) As Long
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
        
    If bReadyToGo(True, False, True, True, True) Then
        On Error GoTo ProcError
        
        'force accelerator cues in Word 2013
        If g_iWordVersion > mpWordVersion_2010 Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If
    
        If g_bAllowSchemeEdit Then
            EditScheme xScheme
            If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
                ActiveDocument.UndoClear
        Else
            'GLOG 4723
            If g_lUILanguage = wdFrenchCanadian Then
                 MsgBox "Cette fonctionnalit� n'est pas disponible. Veuillez contacter votre administrateur.", _
                    vbInformation, g_xAppName
            Else
                MsgBox "This feature is not available.  Please contact your system administrator.", _
                    vbInformation, g_xAppName
            End If
        End If
    
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    End If
    
    Exit Function
ProcError:
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    RaiseError Err.Source
    Exit Function
End Function

Function zzmpNewScheme(ByVal xScheme As String) As Long
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
        
    If bReadyToGo(True, False, True, True, True) Then
        On Error GoTo ProcError
        
        'force accelerator cues in Word 2013
        If g_iWordVersion > mpWordVersion_2010 Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If
    
        If g_bAllowSchemeNew Then
            NewScheme xScheme
            If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
                ActiveDocument.UndoClear
        Else
            'GLOG 4723
            If g_lUILanguage = wdFrenchCanadian Then
                 MsgBox "Cette fonctionnalit� n'est pas disponible. Veuillez contacter votre administrateur.", _
                    vbInformation, g_xAppName
            Else
                MsgBox "This feature is not available.  Please contact your system administrator.", _
                    vbInformation, g_xAppName
            End If
        End If
    
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    End If
    
    Exit Function
ProcError:
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    RaiseError Err.Source
    Exit Function
End Function

Function zzmpRelinkDocumentSchemes() As Long
    If bReadyToGo(True, False, True, True, True) Then
        RelinkDocumentSchemes
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpLoadDefaultScheme() As Long
    If bReadyToGo(True, False, True, True, True) Then
        'prompt to save document if necessary
        If g_bOrganizerSavePrompt And (WordBasic.FileNameFromWindow$() = "") Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox mpOrganizerSavePromptFrench, vbInformation, g_xAppName
            Else
                MsgBox mpOrganizerSavePrompt, vbInformation, g_xAppName
            End If
            Exit Function
        End If
    
        zzmpLoadDefaultScheme = UseSchemeExternal("", True, True)
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpDisplaySchemeActivationMenu(oActionControl As CommandBarControl) As Long
    If bReadyToGo(True, False, True, True, True) Then
        DisplaySchemeActivationMenu oActionControl
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpActivateSchemeFromRibbon(ByVal iIndex As Integer) As Long
    If bReadyToGo(True, False, True, False, True) Then
        ActivateSchemeFromRibbon iIndex
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function GetSchemeActivationXML() As String
    If bReadyToGo(True, False, True, False, True) Then
        GetSchemeActivationXML = mdlN90.GetSchemeActivationXML
    End If
End Function

Function GetContStyleMenuXML() As String
    If bReadyToGo(True, False, True, False, True) Then
        GetContStyleMenuXML = mdlN90.GetContStyleMenuXML
    End If
End Function

Function zzmpApplyContStyle(ByVal iLevel As Integer) As Long
    If bReadyToGo(True, True, True, True, True) Then
        ApplyContStyle iLevel
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpSetSchemeWithoutInsertion() As Long
    Dim xValue As String
            
    'turn off "Insert Level 1 Automatically"
    xValue = xGetUserIni("Numbering", "InsertLevel1Automatically")
    If xValue = "1" Then _
        bSetUserIni "Numbering", "InsertLevel1Automatically", "0"
        
    'display dialog
    zzmpSetScheme
    
    'restore original setting
    If xValue = "1" Then _
        bSetUserIni "Numbering", "InsertLevel1Automatically", "1"
End Function

Function zzmpSelectScheme() As Long
    'turn on selection-only mode
    g_bSchemeSelectionOnly = True
    
    'display dialog with automatic insertion disabled
    zzmpSetSchemeWithoutInsertion
    
    'turn off selection-only mode
    g_bSchemeSelectionOnly = False
End Function

Sub ShowHelp(xFileName As String)
    LaunchDocumentByExtension xFileName
End Sub

Function zzmpGetDocumentSchemesInfo() As DocumentSchemesInfo
    Dim oSchemes As DocumentSchemesInfo
    Dim oRecord As CNumScheme
    Dim xScheme As String
    Dim iCount As Integer
    Dim i As Integer
    Dim xDefaultScheme As String
    
    On Error GoTo ProcError
    
    If Documents.Count > 0 Then
        'GLOG 5247 - changed all parameters to False to prevent autorelinking
        'from occurring as soon as you click the Numbering tab on the ribbon
        If bReadyToGo(False, False, False, False, False) Then
            xScheme = xActiveScheme(ActiveDocument)
            If xScheme <> "" Then
                On Error Resume Next
                Set oRecord = GetRecord(xScheme, mpSchemeType_Document)
                If Err = 0 Then
                    On Error GoTo ProcError
                    oSchemes.ActiveSchemeName = xScheme
                    oSchemes.ActiveSchemeDisplayName = oRecord.Alias
                    oSchemes.ActiveSchemeLevels = iGetLevels(xScheme, mpSchemeType_Document)
                    iCount = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
                    oSchemes.Count = iCount
                    
                    'determine whether default scheme is loaded
                    If bDefaultSchemeExists Then
                        xDefaultScheme = xGetUserIni("Numbering", "DefaultScheme")
                    Else
                        xDefaultScheme = g_xFSchemes(0, 0)
                    End If
                    For i = 0 To iCount - 1
                        If g_xDSchemes(i, 0) = xDefaultScheme Then
                            oSchemes.DefaultIsLoaded = True
                            Exit For
                        End If
                    Next i
                End If
            End If
        End If
    End If
    
    zzmpGetDocumentSchemesInfo = oSchemes
    Exit Function
ProcError:
    'GLOG 5476 - suppress error if the only document closes during execution
    If Err.Number <> 4248 Then
        RaiseError "MPN90.UserFunctions_Numbering.zzmpGetDocumentSchemesInfo"
    End If
    Exit Function
End Function

Function zzmpUseFavoriteSchemeFromRibbon(ByVal iIndex As Integer) As Long
    If bReadyToGo(True, False, True, True, True) Then
        'prompt to save document if necessary
        If g_bOrganizerSavePrompt And (WordBasic.FileNameFromWindow$() = "") Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox mpOrganizerSavePromptFrench, vbInformation, g_xAppName
            Else
                MsgBox mpOrganizerSavePrompt, vbInformation, g_xAppName
            End If
            Exit Function
        End If
    
        zzmpUseFavoriteSchemeFromRibbon = UseSchemeExternal( _
            g_xFavoriteSchemes(iIndex, 0), True, True)
        If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
            ActiveDocument.UndoClear
    End If
End Function

Function zzmpReinitialize() As Long
    'added in 9.9.6001 for GLOG 3940
    zzmpReinitialize = CLng(bAppInitialize())
End Function

Function zzmpDeleteScheme(ByVal xScheme As String, _
    ByVal bSuppressMessages As Boolean) As Long
'added in 9.9.6013
    Dim xAlias As String
    Dim xMsg As String
    If bReadyToGo(True, False, True, True, True) Then
        On Error Resume Next
        xAlias = GetField(xScheme, mpRecField_Alias, mpSchemeType_Document)
        On Error GoTo 0
        If xAlias <> "" Then
            bDeleteScheme xScheme, xAlias, mpSchemeType_Document
            If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
                ActiveDocument.UndoClear
        ElseIf Not bSuppressMessages Then
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "Le th�me " & Chr(34) & xScheme & Chr(34) & _
                    " n'existe pas dans ce document."
            Else
                xMsg = "The scheme " & Chr(34) & xScheme & Chr(34) & _
                    " does not exist in this document."
            End If
            MsgBox xMsg, vbExclamation, g_xAppName
        End If
    End If
End Function

Function zzmpChangeToDocumentScheme(ByVal xScheme As String, _
    ByVal bSuppressMessages As Boolean) As Long
'added in 9.9.6013
    Dim xAlias As String
    Dim xMsg As String
    If bReadyToGo(True, False, True, True, True) Then
        'native Heading scheme may not yet be a MacPac scheme
        If xScheme = "HeadingStyles" Then
            ConvertWordHeadingStyles
        End If
            
        On Error Resume Next
        xAlias = GetField(xScheme, mpRecField_Alias, mpSchemeType_Document)
        On Error GoTo 0
        
        If xAlias <> "" Then
            iChangeScheme xScheme, mpSchemeType_Document, "", _
                False, , False, , bIsHeadingScheme(xScheme)
                
            If (g_iWordVersion >= mpWordVersion_2007) And Not g_bPreserveUndoList Then _
                ActiveDocument.UndoClear
        ElseIf Not bSuppressMessages Then
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "Le th�me " & Chr(34) & xScheme & Chr(34) & _
                    " n'existe pas dans ce document."
            Else
                xMsg = "The scheme " & Chr(34) & xScheme & Chr(34) & _
                    " does not exist in this document."
            End If
            MsgBox xMsg, vbExclamation, g_xAppName
        End If
    End If
End Function


