VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmHeaderFooterFrench 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " TM En-t�te et pied de page"
   ClientHeight    =   5415
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4755
   Icon            =   "frmHeaderFooterFrench.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5415
   ScaleWidth      =   4755
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame pnlLanguage 
      Caption         =   "Langue"
      Height          =   695
      Left            =   105
      TabIndex        =   22
      Top             =   4000
      Width           =   4545
      Begin VB.CommandButton cmdSetType 
         Caption         =   "D�finir"
         Height          =   330
         Left            =   3825
         TabIndex        =   19
         Top             =   240
         Width           =   600
      End
      Begin TrueDBList60.TDBCombo cbxType 
         Height          =   585
         Left            =   840
         OleObjectBlob   =   "frmHeaderFooterFrench.frx":058A
         TabIndex        =   18
         Top             =   240
         Width           =   2865
      End
      Begin VB.Label lblType 
         Caption         =   "T&ype:"
         Height          =   225
         Left            =   150
         TabIndex        =   17
         Top             =   300
         Width           =   500
      End
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   393
      Left            =   2070
      TabIndex        =   20
      Top             =   4875
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      Height          =   393
      Left            =   3240
      TabIndex        =   21
      Top             =   4875
      Width           =   1100
   End
   Begin VB.Frame pnlPage 
      Caption         =   "Pied de page - Pagination"
      Height          =   1540
      Left            =   105
      TabIndex        =   10
      Top             =   2370
      Width           =   4545
      Begin VB.CheckBox chkContinue 
         Caption         =   "Utilisez la num�rotation de &page en continu"
         Height          =   225
         Left            =   165
         TabIndex        =   15
         Top             =   1100
         Width           =   3500
      End
      Begin VB.CommandButton cmdSetPage 
         Caption         =   "D�finir"
         Height          =   330
         Left            =   3825
         TabIndex        =   16
         Top             =   1040
         Width           =   600
      End
      Begin TrueDBList60.TDBCombo cmbNumberStyle 
         Height          =   585
         Left            =   120
         OleObjectBlob   =   "frmHeaderFooterFrench.frx":24AD
         TabIndex        =   12
         Top             =   555
         Width           =   1695
      End
      Begin TrueDBList60.TDBCombo cmbPunctuation 
         Height          =   585
         Left            =   1965
         OleObjectBlob   =   "frmHeaderFooterFrench.frx":43D7
         TabIndex        =   14
         Top             =   555
         Width           =   1755
      End
      Begin VB.Label lblPunctuation 
         Caption         =   "Ponc&tuation:"
         Height          =   225
         Left            =   1990
         TabIndex        =   13
         Top             =   315
         Width           =   1500
      End
      Begin VB.Label lblNumberStyle 
         Caption         =   "Style de &num�ro:"
         Height          =   225
         Left            =   150
         TabIndex        =   11
         Top             =   315
         Width           =   1440
      End
   End
   Begin VB.Frame pnlHeader 
      Caption         =   "En-t�te"
      Height          =   2085
      Left            =   105
      TabIndex        =   0
      Top             =   135
      Width           =   4545
      Begin VB.CheckBox chkTOCUnderline 
         Caption         =   "&Soulign�"
         Height          =   225
         Left            =   165
         TabIndex        =   3
         Top             =   1155
         Width           =   1050
      End
      Begin VB.CheckBox chkTOCCaps 
         Caption         =   "Toutes &Majuscules"
         Height          =   225
         Left            =   165
         TabIndex        =   4
         Top             =   1560
         Width           =   1700
      End
      Begin VB.CheckBox chkTOCBold 
         Caption         =   "&Gras"
         Height          =   225
         Left            =   165
         TabIndex        =   2
         Top             =   750
         Width           =   1000
      End
      Begin VB.CommandButton cmdSetHeader 
         Caption         =   "D�finir"
         Height          =   330
         Left            =   3825
         TabIndex        =   9
         Top             =   1500
         Width           =   600
      End
      Begin VB.CheckBox chkPageBold 
         Caption         =   "&Gras"
         Height          =   225
         Left            =   2470
         TabIndex        =   6
         Top             =   750
         Width           =   915
      End
      Begin VB.CheckBox chkPageCaps 
         Caption         =   "&Majuscules"
         Height          =   225
         Left            =   2470
         TabIndex        =   8
         Top             =   1560
         Width           =   1100
      End
      Begin VB.CheckBox chkPageUnderline 
         Caption         =   "&Soulign�"
         Height          =   225
         Left            =   2470
         TabIndex        =   7
         Top             =   1155
         Width           =   1050
      End
      Begin VB.Label lblTableOfContents 
         Caption         =   "Table de mati�res"
         Height          =   225
         Left            =   150
         TabIndex        =   1
         Top             =   390
         Width           =   2055
      End
      Begin VB.Label lblPage 
         Caption         =   "Page"
         Height          =   225
         Left            =   2485
         TabIndex        =   5
         Top             =   390
         Width           =   495
      End
   End
End
Attribute VB_Name = "frmHeaderFooterFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Cancelled As Boolean
Private m_iNumberStyleDefault As Integer
Private m_iNumberPunctuationDefault As Integer

Private Sub btnCancel_Click()
    Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnOK_Click()
    Cancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub cbxType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxType, Reposition
    Exit Sub
ProcError:
    RaiseError "frmHeaderFooter.cbxType"
    Exit Sub
End Sub

Private Sub chkTOCBold_Click()
    Me.lblTableOfContents.FontBold = Me.chkTOCBold
End Sub

Private Sub chkTOCCaps_Click()
    If Me.chkTOCCaps = 1 Then
        Me.lblTableOfContents = "TABLE DES MATI�RES"
    Else
        Me.lblTableOfContents = "Table des mati�res"
    End If
End Sub

Private Sub chkTOCUnderline_Click()
    Me.lblTableOfContents.FontUnderline = Me.chkTOCUnderline
End Sub

Private Sub chkPageBold_Click()
    Me.lblPage.FontBold = Me.chkPageBold
End Sub

Private Sub chkPageCaps_Click()
    If Me.chkPageCaps = 1 Then
        Me.lblPage = "PAGE"
    Else
        Me.lblPage = "Page"
    End If
End Sub

Private Sub chkPageUnderline_Click()
    Me.lblPage.FontUnderline = Me.chkPageUnderline
End Sub

Private Sub cmbNumberStyle_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNumberStyle, Reposition
    Exit Sub
ProcError:
    RaiseError "frmHeaderFooter.cmbNumberStyle_Mismatch"
    Exit Sub
End Sub

Private Sub cmbPunctuation_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbPunctuation, Reposition
    Exit Sub
ProcError:
    RaiseError "frmHeaderFooter.cmbPunctuation_Mismatch"
    Exit Sub
End Sub

Private Sub cmdSetHeader_Click()
    bAppSetUserINIValue "TOC", "HeaderTOCBold", Me.chkTOCBold
    bAppSetUserINIValue "TOC", "HeaderTOCCaps", Me.chkTOCCaps
    bAppSetUserINIValue "TOC", "HeaderTOCUnderline", Me.chkTOCUnderline
    bAppSetUserINIValue "TOC", "HeaderPageBold", Me.chkPageBold
    bAppSetUserINIValue "TOC", "HeaderPageCaps", Me.chkPageCaps
    bAppSetUserINIValue "TOC", "HeaderPageUnderline", Me.chkPageUnderline
    Application.StatusBar = "En-t�te pr�d�fini par d�faut"
End Sub

Private Sub cmdSetPage_Click()
    bAppSetUserINIValue "TOC", "PageNoStyle", Me.cmbNumberStyle.Bookmark
    bAppSetUserINIValue "TOC", "PageNoPunctuation", Me.cmbPunctuation.Bookmark
    bAppSetUserINIValue "TOC", "ContinuePageNoFromPrevious", Me.chkContinue
    Application.StatusBar = "Pagination pr�d�finie par d�faut"
End Sub

Private Sub cmdSetType_Click()
    bAppSetUserINIValue "TOC", "DefaultBoilerplateIndex", Me.cbxType.Bookmark
    Application.StatusBar = "Type Langue pr�d�finie par d�faut"
End Sub

Private Sub Form_Activate()
    Me.cmbPunctuation.SelectedItem = Abs(Me.NumberPunctuationDefault)
    If Me.cmbPunctuation.BoundText = "" Then
        Me.cmbPunctuation.SelectedItem = 0
    End If
    Me.cmbNumberStyle.SelectedItem = Me.NumberStyleDefault
    If Me.cmbNumberStyle.BoundText = "" Then
        Me.cmbNumberStyle.SelectedItem = 0
    End If
    
    If g_xBoilerplates(0, 0) <> "" Then
        On Error Resume Next
        Me.cbxType.SelectedItem = CLng(xAppGetUserINIValue("TOC", _
                                        "DefaultBoilerplateIndex"))
        If Me.cbxType.BoundText = "" Then _
            Me.cbxType.SelectedItem = 0
    End If

End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim xarNumberStyle As xArray
    Dim xarPunctuation As xArray
    Dim xarType As xArray

    Cancelled = True
    
'   load number style
    Set xarNumberStyle = New xArray
    
    xarNumberStyle.ReDim 0, 4, 0, 1
    xarNumberStyle(0, 0) = "Arabique(1, 2, 3)"
    xarNumberStyle(0, 1) = "0"
    xarNumberStyle(1, 0) = "Romain (I, II, III)"
    xarNumberStyle(1, 1) = "1"
    xarNumberStyle(2, 0) = "Romain (i, ii, iii)"
    xarNumberStyle(2, 1) = "2"
    xarNumberStyle(3, 0) = "Lettre (A, B, C)"
    xarNumberStyle(3, 1) = "3"
    xarNumberStyle(4, 0) = "Lettre (a, b, c)"
    xarNumberStyle(4, 1) = "4"
    
    Me.cmbNumberStyle.Array = xarNumberStyle
    ResizeTDBCombo Me.cmbNumberStyle, 5

'   load punctuation
    Set xarPunctuation = New xArray
    
    xarPunctuation.ReDim 0, 2, 0, 1
    xarPunctuation(0, 0) = "(None)"
    xarPunctuation(0, 1) = "0"
    xarPunctuation(1, 0) = "Traits d'union (-#-)"
    xarPunctuation(1, 1) = "1"
    xarPunctuation(2, 0) = "Traits d'union (- # -)"
    xarPunctuation(2, 1) = "2"
    
    Me.cmbPunctuation.Array = xarPunctuation
    ResizeTDBCombo Me.cmbPunctuation, 3

    If g_xBoilerplates(0, 0) <> "" Then
        Set xarType = New xArray
        
        xarType.ReDim 0, UBound(g_xBoilerplates), 0, 1
        
        For i = 0 To UBound(g_xBoilerplates)
            xarType(i, 0) = g_xBoilerplates(i, 0)
            xarType(i, 1) = g_xBoilerplates(i, 1)
        Next i
            
        Me.cbxType.Array = xarType
        ResizeTDBCombo Me.cbxType, UBound(g_xBoilerplates) + 1
    Else
        Me.pnlLanguage.Visible = False
        Me.lblType.Visible = False
        Me.cbxType.Visible = False
        Me.cmdSetType.Visible = False
        Me.Height = Me.Height - 800
        Me.btnCancel.Top = Me.btnCancel.Top - 800
        Me.btnOK.Top = Me.btnOK.Top - 800
    End If
End Sub

Public Property Get NumberStyleDefault() As Integer
    NumberStyleDefault = m_iNumberStyleDefault
End Property

Public Property Let NumberStyleDefault(iNew As Integer)
    m_iNumberStyleDefault = iNew
End Property

Public Property Get NumberPunctuationDefault() As Integer
    NumberPunctuationDefault = m_iNumberPunctuationDefault
End Property

Public Property Let NumberPunctuationDefault(iNew As Integer)
    m_iNumberPunctuationDefault = iNew
End Property

