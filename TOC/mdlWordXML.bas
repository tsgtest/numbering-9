Attribute VB_Name = "mdlWordXML"
Option Explicit

Public Function SetXMLMarkupState(ByVal oDocument As Word.Document, _
                                  ByVal bShowXML As Boolean) As Long
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    SetXMLMarkupState = oWordXML.SetXMLMarkupState(oDocument, bShowXML)
End Function

Public Function GetTagSafeParagraphStart(ByVal oInsertionRange As Word.Range, _
                                         Optional ByRef iParagraphLevelTagsAtStart As Integer) As Long
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    
    If (InStr(Word.Application.Version, "15.") <> 0) Or _
            (InStr(Word.Application.Version, "16.") <> 0) Then
        'custom xml not supported in Word 2013
        GetTagSafeParagraphStart = oInsertionRange.Paragraphs(1).Range.Start
    Else
        GetTagSafeParagraphStart = oWordXML.GetTagSafeParagraphStart(oInsertionRange, _
            iParagraphLevelTagsAtStart)
    End If
End Function

Public Function GetTagSafeParagraphEnd(ByVal oInsertionRange As Word.Range, _
                                       Optional ByRef iParagraphLevelTagsAtEnd As Integer) As Long
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    
    If (InStr(Word.Application.Version, "15.") <> 0) Or _
            (InStr(Word.Application.Version, "16.") <> 0) Then
        'custom xml not supported in Word 2013
        GetTagSafeParagraphEnd = oInsertionRange.Paragraphs(1).Range.End - 1
    Else
        GetTagSafeParagraphEnd = oWordXML.GetTagSafeParagraphEnd(oInsertionRange, _
            iParagraphLevelTagsAtEnd)
    End If
End Function

Public Function CursorIsAtStartOfBlockTag() As Boolean
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    
    If (InStr(Word.Application.Version, "15.") = 0) And _
            (InStr(Word.Application.Version, "16.") = 0) Then
        'custom xml not supported in Word 2013
        CursorIsAtStartOfBlockTag = oWordXML.CursorIsAtStartOfBlockTag()
    End If
End Function

Public Function CountTagsEndingInRange(ByVal oRange As Word.Range) As Integer
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    
    If (InStr(Word.Application.Version, "15.") = 0) And _
            (InStr(Word.Application.Version, "16.") = 0) Then
        'custom xml not supported in Word 2013
        CountTagsEndingInRange = oWordXML.CountTagsEndingInRange(oRange)
    End If
End Function

Public Sub InsertStyleSeparator()
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    oWordXML.InsertStyleSeparator
End Sub

Public Function IsStyleSeparator(ByVal oPara As Word.Paragraph) As Boolean
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    IsStyleSeparator = oWordXML.IsStyleSeparator(oPara)
End Function

Public Function GetPositionAfterInlineTag(ByVal oRange As Word.Range) As Long
    Dim oWordXML As cWordXML
    Set oWordXML = New cWordXML
    
    If (InStr(Word.Application.Version, "15.") = 0) And _
            (InStr(Word.Application.Version, "16.") = 0) Then
        'custom xml not supported in Word 2013
        GetPositionAfterInlineTag = oWordXML.GetPositionAfterInlineTag(oRange)
    End If
End Function


