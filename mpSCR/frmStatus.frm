VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "Comctl32.ocx"
Begin VB.Form frmStatus 
   BorderStyle     =   0  'None
   Caption         =   "###"
   ClientHeight    =   2130
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5160
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2130
   ScaleWidth      =   5160
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.ProgressBar pbStatus 
      Height          =   255
      Left            =   285
      TabIndex        =   0
      Top             =   1350
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   450
      _Version        =   327682
      Appearance      =   0
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   276
      Left            =   84
      TabIndex        =   2
      Top             =   84
      Width           =   5430
   End
   Begin VB.Label Label2 
      BackColor       =   &H80000003&
      Height          =   372
      Left            =   15
      TabIndex        =   3
      Top             =   -45
      Width           =   5430
   End
   Begin VB.Image imgP 
      Height          =   480
      Left            =   270
      Picture         =   "frmStatus.frx":0000
      Top             =   555
      Width           =   480
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "###"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   930
      TabIndex        =   1
      Top             =   690
      Width           =   4080
   End
End
Attribute VB_Name = "frmStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Call SetWindowPos(Me.hwnd, HWND_TOPMOST, 0, 0, 0, 0, _
                               SWP_NOSIZE Or SWP_NOMOVE)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmStatus = Nothing
End Sub
