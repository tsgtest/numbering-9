VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "Comct232.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmLineSpacingFrench 
   Caption         =   "###"
   ClientHeight    =   2580
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4485
   Icon            =   "frmLineSpacingFrench.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2580
   ScaleWidth      =   4485
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtAt 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2915
      TabIndex        =   3
      Top             =   420
      Width           =   520
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      Height          =   385
      Left            =   2312
      TabIndex        =   15
      Top             =   2030
      Width           =   1100
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   385
      Left            =   1072
      TabIndex        =   14
      Top             =   2030
      Width           =   1100
   End
   Begin VB.TextBox txtSpaceAfter 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2145
      TabIndex        =   11
      Top             =   1240
      Width           =   1290
   End
   Begin VB.TextBox txtSpaceBefore 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   170
      TabIndex        =   7
      Top             =   1240
      Width           =   1290
   End
   Begin ComCtl2.UpDown udSpaceAfter 
      Height          =   330
      Left            =   3436
      TabIndex        =   12
      TabStop         =   0   'False
      Tag             =   "6"
      Top             =   1240
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   327681
      BuddyControl    =   "txtSpaceAfter"
      BuddyDispid     =   196612
      OrigLeft        =   3105
      OrigTop         =   1190
      OrigRight       =   3345
      OrigBottom      =   1520
      Increment       =   6
      Max             =   1584
      Enabled         =   -1  'True
   End
   Begin ComCtl2.UpDown udSpaceBefore 
      Height          =   330
      Left            =   1461
      TabIndex        =   9
      TabStop         =   0   'False
      Tag             =   "6"
      Top             =   1240
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   327681
      BuddyControl    =   "txtSpaceBefore"
      BuddyDispid     =   196613
      OrigLeft        =   1255
      OrigTop         =   1190
      OrigRight       =   1495
      OrigBottom      =   1520
      Increment       =   6
      Max             =   1584
      Enabled         =   -1  'True
   End
   Begin ComCtl2.UpDown udAt 
      Height          =   330
      Left            =   3436
      TabIndex        =   4
      TabStop         =   0   'False
      Tag             =   "1"
      Top             =   420
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   327681
      Value           =   1
      BuddyControl    =   "txtAt"
      BuddyDispid     =   196609
      OrigLeft        =   1255
      OrigTop         =   1190
      OrigRight       =   1495
      OrigBottom      =   1520
      Max             =   1584
      Min             =   1
      Enabled         =   0   'False
   End
   Begin TrueDBList60.TDBCombo cmbLineSpacing 
      Height          =   585
      Left            =   170
      OleObjectBlob   =   "frmLineSpacingFrench.frx":058A
      TabIndex        =   1
      Top             =   420
      Width           =   2640
   End
   Begin VB.Label lblAtUnits 
      Caption         =   "pts"
      Height          =   300
      Left            =   3740
      TabIndex        =   5
      Top             =   480
      Width           =   250
   End
   Begin VB.Label lblAt 
      Caption         =   "D&e:"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2930
      TabIndex        =   2
      Top             =   210
      Width           =   1200
   End
   Begin VB.Label lblSpaceAfter 
      Caption         =   "Espace &apr�s:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2160
      TabIndex        =   10
      Top             =   1030
      Width           =   1500
   End
   Begin VB.Label lblSpaceAfterUnits 
      Caption         =   "pts"
      Height          =   300
      Left            =   3740
      TabIndex        =   13
      Top             =   1300
      Width           =   250
   End
   Begin VB.Label lblSpaceBeforeUnits 
      Caption         =   "pts"
      Height          =   300
      Left            =   1765
      TabIndex        =   8
      Top             =   1300
      Width           =   250
   End
   Begin VB.Label lblSpaceBefore 
      Caption         =   "Espac&e avant:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   185
      TabIndex        =   6
      Top             =   1030
      Width           =   1200
   End
   Begin VB.Label lblLineSpacing 
      Caption         =   "&Interligne:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   185
      TabIndex        =   0
      Top             =   210
      Width           =   1395
   End
End
Attribute VB_Name = "frmLineSpacingFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub cmbLineSpacing_ItemChange()
    Select Case Me.cmbLineSpacing.Text
        Case "Au moins", "Exactement"
'           at least, exactly
            Me.lblAt.Enabled = True
            Me.txtAt.Enabled = True
            Me.udAt.Enabled = True
            Me.lblAtUnits.Visible = True
            Me.txtAt = "12"
            Me.udAt.Value = 12
        Case "Multiple"
'           multiple
            Me.lblAt.Enabled = True
            Me.txtAt.Enabled = True
            Me.udAt.Enabled = True
            Me.lblAtUnits.Visible = False
            Me.txtAt = "3"
            Me.udAt.Value = 3
        Case Else
            Me.lblAt.Enabled = False
            Me.txtAt.Enabled = False
            Me.udAt.Enabled = False
            Me.lblAtUnits.Visible = False
            Me.txtAt = ""
    End Select
End Sub

Private Sub cmdCancel_Click()
    m_bCancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub cmdOK_Click()
    m_bCancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Activate()
    Me.cmbLineSpacing.SelectedItem = 0
    Me.txtSpaceAfter = "-Utiliser courant-"
    Me.txtSpaceBefore = "-Utiliser courant-"
End Sub

Private Sub Form_Load()
    Dim xarLineSpacing As xArray
    
    Set xarLineSpacing = New xArray
    xarLineSpacing.ReDim 0, 6, 0, 1
    xarLineSpacing(0, 0) = "-Utiliser courant-"
    xarLineSpacing(0, 1) = "-Utiliser courant-"
    xarLineSpacing(1, 0) = "Simple"
    xarLineSpacing(1, 1) = "Simple"
    xarLineSpacing(2, 0) = "1,5 Ligne"
    xarLineSpacing(2, 1) = "1,5 Ligne"
    xarLineSpacing(3, 0) = "Double"
    xarLineSpacing(3, 1) = "Double"
    xarLineSpacing(4, 0) = "Au moins"
    xarLineSpacing(4, 1) = "Au moins"
    xarLineSpacing(5, 0) = "Exactement"
    xarLineSpacing(5, 1) = "Exactement"
    xarLineSpacing(6, 0) = "Multiple"
    xarLineSpacing(6, 1) = "Multiple"
    Me.cmbLineSpacing.Array = xarLineSpacing
    ResizeTDBCombo Me.cmbLineSpacing, 7
    
    'GLOG 5689
    m_bCancelled = True
End Sub

Private Sub txtAt_LostFocus()
    If Me.txtAt = "" Then
        Me.txtAt = Me.udAt.Value
    Else
        bValidateSpaceInput Me.txtAt
    End If
End Sub

Private Sub txtSpaceAfter_Change()
    With Me.txtSpaceAfter
        If Len(.Text) = 0 Then
            .Text = "-Utiliser courant-"
            .SelStart = 0
            .SelLength = 18
        End If
    End With
End Sub

Private Sub txtSpaceAfter_GotFocus()
    bEnsureSelectedContent Me.txtSpaceAfter
End Sub

Private Sub txtSpaceAfter_LostFocus()
    If Me.txtSpaceAfter <> "-Utiliser courant-" Then
        bValidateSpaceInput Me.txtSpaceAfter
    End If
End Sub

Private Sub txtSpaceBefore_Change()
    With Me.txtSpaceBefore
        If Len(.Text) = 0 Then
            .Text = "-Utiliser courant-"
            .SelStart = 0
            .SelLength = 18
        End If
    End With
End Sub

Private Sub txtSpaceBefore_GotFocus()
    bEnsureSelectedContent Me.txtSpaceBefore
End Sub

Private Sub txtSpaceBefore_LostFocus()
    If Me.txtSpaceBefore <> "-Utiliser courant-" Then
        bValidateSpaceInput Me.txtSpaceBefore
    End If
End Sub

Private Sub txtSpaceAfter_KeyDown(KeyCode As Integer, Shift As Integer)
    If Me.txtSpaceAfter = "-Utiliser courant-" Then _
        Me.txtSpaceAfter = ""
    If (KeyCode = vbKeyUp) Then
        ChangeUpDown Me.udSpaceAfter, _
            True, 6, 6, 6
        udSpaceAfter_UpClick
    ElseIf (KeyCode = vbKeyDown) Then
        ChangeUpDown Me.udSpaceAfter, _
            False, 6, 6, 6
        udSpaceAfter_DownClick
    End If
End Sub

Private Sub txtSpaceAfter_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtSpaceAfter, False)
End Sub

Private Sub txtSpaceBefore_KeyDown(KeyCode As Integer, Shift As Integer)
    If Me.txtSpaceBefore = "-Utiliser courant-" Then _
        Me.txtSpaceBefore = ""
    If (KeyCode = vbKeyUp) Then
        ChangeUpDown Me.udSpaceBefore, _
            True, 6, 6, 6
        udSpaceBefore_UpClick
    ElseIf (KeyCode = vbKeyDown) Then
        ChangeUpDown Me.udSpaceBefore, _
            False, 6, 6, 6
        udSpaceBefore_DownClick
    End If
End Sub

Private Sub txtSpaceBefore_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtSpaceBefore, False)
End Sub

Private Sub udSpaceAfter_DownClick()
    If Me.txtSpaceAfter = "-Utiliser courant-" Then _
        Me.txtSpaceAfter = "0"
    DecrementBuddyCtl Me.udSpaceAfter
End Sub

Private Sub udSpaceAfter_UpClick()
    If Me.txtSpaceAfter = "-Utiliser courant-" Then _
        Me.txtSpaceAfter = "0"
    IncrementBuddyCtl Me.udSpaceAfter
End Sub

Private Sub udSpaceBefore_DownClick()
    If Me.txtSpaceBefore = "-Utiliser courant-" Then _
        Me.txtSpaceBefore = "0"
    DecrementBuddyCtl Me.udSpaceBefore
End Sub

Private Sub udSpaceBefore_UpClick()
    If Me.txtSpaceBefore = "-Utiliser courant-" Then _
        Me.txtSpaceBefore = "0"
    IncrementBuddyCtl Me.udSpaceBefore
End Sub

Private Sub txtAt_GotFocus()
    bEnsureSelectedContent Me.txtAt
End Sub

Private Sub txtAt_KeyDown(KeyCode As Integer, Shift As Integer)
    If (KeyCode = vbKeyUp) Then
        ChangeUpDown Me.udAt, _
            True, 1, 1, 1
        udAt_UpClick
    ElseIf (KeyCode = vbKeyDown) Then
        ChangeUpDown Me.udAt, _
            False, 1, 1, 1
        udAt_DownClick
    End If
End Sub

Private Sub txtAt_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtAt, False)
End Sub

Private Sub udAt_DownClick()
    DecrementBuddyCtl Me.udAt
End Sub

Private Sub udAt_UpClick()
    IncrementBuddyCtl Me.udAt
End Sub

Private Sub cmbLineSpacing_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLineSpacing, Reposition
    Exit Sub
ProcError:
    RaiseError "frmLineSpacing.cmbLineSpacing_Mismatch"
    Exit Sub
End Sub

