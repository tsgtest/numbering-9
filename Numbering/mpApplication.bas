Attribute VB_Name = "mdlApplication"
Option Explicit
Option Base 1

Public Const KEY_SHIFT = &H10
Public Const KEY_CTL = &H11
Public Const KEY_MENU = &H12
Public Const KEY_F1 = &H70
Public Const KEY_F5 = &H74
Public Const KEY_a = 65
Public Const KEY_DEL = 46
Public Const KEY_ENTER = 13
Public Const KEY_TAB = 9
Public Const mpDemoRegKey = _
    "HKEY_LOCAL_MACHINE\SOFTWARE\Legal MacPac\" & _
    "MacPac Numbering 2000 Demo Version\9.2.0"


'API declarations for LockWindowUpdate() and FindWindow()
Public Declare Function GetKeyState Lib "user32" ( _
    ByVal nVirtKey As Long) As Integer
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" ( _
    ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
Private Declare Function LockWindowUpdate Lib "user32" ( _
    ByVal hwndLock As Long) As Long
Declare Function SetActiveWindow Lib "user32" (ByVal hwnd As Long) As Long
    
Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" ( _
    ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Private Declare Function GetTickCount Lib "kernel32" () As Long
Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
    ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, _
    ByVal lpFileName As String) As Long
    
'HTML help
Public Declare Function HtmlHelp Lib "hhctrl.ocx" Alias "HtmlHelpA" _
               (ByVal hwndCaller As Long, ByVal pszFile As String, _
                ByVal uCommand As Long, ByVal dwData As Long) As Long
Public Const HH_DISPLAY_TOPIC = &H0         ' select last opened tab, [display a specified topic]
Public Const HH_DISPLAY_TOC = &H1           ' select contents tab, [display a specified topic]
Public Const HH_DISPLAY_INDEX = &H2         ' select index tab and searches for a keyword
Public Const HH_DISPLAY_SEARCH = &H3        ' select search tab and perform a search

Public g_xTOCSchemes() As String

Public Declare Function ShellExecute Lib "shell32.dll" _
  Alias "ShellExecuteA" (ByVal hwnd As Long, _
  ByVal lpOperation As String, ByVal lpFile As String, _
  ByVal lpParameters As String, ByVal lpDirectory As String, _
  ByVal nShowCmd As Long) As Long

Private Const SW_SHOWNORMAL = 1

Private Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128   '  Maintenance string for PSS usage
End Type

Private Declare Function GetVersionEx Lib "kernel32" _
    Alias "GetVersionExA" _
    (lpVersionInformation As OSVERSIONINFO) As Long

'9.9.5011
Public Declare Function GetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
    ByVal lAction As Long, ByVal lParam As Long, ByRef lpvParam As Long, ByVal lWinIni As Long) As Long
Public Declare Function SetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
    ByVal lAction As Long, ByVal lParam As Long, ByVal lpvParam As Long, ByVal lWinIni As Long) As Long
Public Const SPI_GETKEYBOARDCUES = 4106
Public Const SPI_SETKEYBOARDCUES = 4107

Public Function PublicSchemesDir() As String
'returns the path to the public schemes dir
'as specified in the user ini
    Dim xDir As String
    
    xDir = xGetAppIni("Numbering", "SharedDirectory")
    If xDir <> "" Then
        xDir = GetEnvironVarPath(xDir)
        If Right(xDir, 1) <> "\" Then _
            xDir = xDir & "\"
    End If
    PublicSchemesDir = xDir
End Function

Public Function bSetMenuCtlState(bEnable As Boolean, _
                                  Optional xScheme As String) As Boolean
'enable/disable all menu items based on bEnable-
'enable/disable Mark and Format item based on
'whether there is a macpac scheme in the doc
    Dim ctlMenu As CommandBarControl
    Dim tmpActive As Template
    
    On Error Resume Next
    
    With Word.Application.CommandBars( _
            "Menu Bar").Controls("Numbering")
        For Each ctlMenu In .Controls
            ctlMenu.Enabled = bEnable
        Next ctlMenu
    End With

    Set tmpActive = ActiveDocument.AttachedTemplate
    If tmpActive <> NormalTemplate Then _
        tmpActive.Saved = True
    
    MarkStartupSaved
End Function

Sub EchoOff()
    Call LockWindowUpdate(FindWindow("OpusApp", 0&))
End Sub

Sub EchoOn()
    Call LockWindowUpdate(0&)
End Sub


Function envGetAppEnvironment() As mpAppEnvironment
'fills type mpAppEnvironment with
'application environment settings -
'g_envMpApp below is a global var
    On Error Resume Next
    With Application
        g_envMpApp.lBrowserTarget = .Browser.Target
        .Assistant.Animation = msoAnimationWritingNotingSomething
    End With
        
    envGetAppEnvironment = g_envMpApp
    
End Function

Function bSetAppEnvironment(envCurrent As mpAppEnvironment, _
                            Optional bClearUndo As Boolean = False) As Boolean
'sets application environment settings
'using values in type mpAppEnvironment

'   added to ensure that Edit Find is always cleared
    bEditFindReset
    
    With Application
        On Error Resume Next
        .Browser.Target = envCurrent.lBrowserTarget
        
        If bClearUndo Then
'           clear "undo" list
            ActiveDocument.UndoClear
        End If
    
'       assistant animation sometimes
'       sticks, so reset
        With .Assistant
            .Animation = msoAnimationIdle
        End With
        On Error GoTo 0
    End With
    
    
    bSetAppEnvironment = True
End Function

Function bSetAppIni(xSection As String, _
                             xKey As String, _
                             xValue As String) As Boolean
    System.PrivateProfileString(g_xFirmIni, xSection, xKey) = xValue
End Function

Function xGetAppIni(xSection As String, xKey As String) As String
    If g_xFirmIni = Empty Then
        bAppInitialize
    End If
    
    xGetAppIni = _
        System.PrivateProfileString(g_xFirmIni, xSection, xKey)
                             
End Function

Function GetAppFiles() As Boolean
'   fill global vars with appropriate paths
    Dim xAppPath As String
    
    xAppPath = mpBase.GetAppPath
    
    If Dir(xAppPath & "\mpnBoilerpl.ate") <> "" Then
        g_xBP = xAppPath & "\mpnBoilerpl.ate"
    Else
        g_xBP = xAppPath & "\bp.doc"
    End If
    
    g_xFirmIni = xAppPath & "\mpN90.INI"
    g_xUserIni = g_xUserPath & "\NumTOC.INI"
    g_xFNum80Sty = xAppPath & "\mpNumbers80.sty"
    
    If g_bIsAdmin Then
        g_xFNumSty = xAppPath & "\mpNumbers.sty.clean"
        g_xPNumSty = g_xAdminSchemesPath & "\mpNumbers.sty"
    Else
        g_xFNumSty = xAppPath & "\mpNumbers.sty"
        g_xPNumSty = g_xUserPath & "\mpNumbers.sty"
    End If
End Function

Function bAppInitialize() As Boolean
'initializes MPN90
    Dim bRet As Boolean
    Dim oStatus As mpSCR.CStatus
    Dim xErrSource As String
    Dim bDo As Boolean
    Dim oAT As Word.AutoTextEntry
    Dim oAddIn As Word.AddIn
    Dim xValue As String
    Dim oVersion As OSVERSIONINFO
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    xErrSource = "mpApplication.bAppInitialize"
    
    'GLOG 5085 - the following line isn't necessary and sometimes errs
'    Set g_oWord = GetObject(, "Word.Application")
    
''   (removed in 9.8.2002) ensure correct WD functions for Word version
'    SetVersionFiles
    
    'GLOG 5224 - get Windows Version
    oVersion.dwOSVersionInfoSize = Len(oVersion)
    GetVersionEx oVersion
    g_lWinMajorVersion = oVersion.dwMajorVersion
    g_lWinMinorVersion = oVersion.dwMinorVersion

'   get paths
    bRet = GetAppPaths()
    
'   get files
    bRet = GetAppFiles()
    
    'get ui language if we don't already have it
    If g_lUILanguage = 0 Then
        xValue = xGetAppIni("General", "UILanguage")
        If xValue <> "" Then
            g_lUILanguage = CLng(xValue)
            If g_lUILanguage = 0 Then _
                g_lUILanguage = wdEnglishUS
        End If
    End If

'   get global template names - moved after previous two functions in 9.9.2008
'   because we now need to locate ini first
    GetGlobalTemplateNames
    
'   get startup path - this was moved out of GetAppPaths in 9.9.2008
    g_xStartPath = Application.Options.DefaultFilePath(wdStartupPath)
    
'   GLOG 5421 - the following call to Dir() was intermittently
'   erring when g_xStartPath was empty
    Dim bStartupTemplateFound As Boolean
    If g_xStartPath <> "" Then
        bStartupTemplateFound = (Dir(g_xStartPath & "\" & g_xMPNDot) <> "")
    End If
    If Not bStartupTemplateFound Then
'       startup templates may be in Word's default startup directory
        On Error Resume Next
        Set oAddIn = Application.AddIns(g_xMPNDot)
        On Error GoTo ProcError
        If oAddIn Is Nothing Then
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "Le mod�le global pr�cis� dans mpn90.ini (" & Chr(34) & _
                    g_xMPNDot & Chr(34) & ") n'est pas charg�.  Veuillez contacter votre administrateur."
            Else
                xMsg = "The global template specified in mpn90.ini (" & Chr(34) & _
                    g_xMPNDot & Chr(34) & ") is not loaded.  Please contact your " & _
                    "system administrator."
            End If
            MsgBox xMsg, vbExclamation, g_xAppName
            Exit Function
        End If
        g_xStartPath = Application.AddIns(g_xMPNDot).Path
    End If

'   demo is limited to 30 days
'    #If bDemo Then
'        If bDemoIsExpired Then
        If Not MacPacIsLegal Then
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "Votre evaluation de MacPac Numbering 2000 est expir�e.  Veuillez contacter votre adminstrateur."
            Else
                xMsg = "Your evaluation copy of MacPac Numbering 2000 has expired." & _
                       "  Please contact your system administrator."
            End If
            MsgBox xMsg, vbExclamation, g_xAppName
            On Error Resume Next
            MarkStartupSaved
            Word.AddIns(g_xMPNConvertDot).Installed = False
            Word.AddIns(g_xMPNDot).Installed = False
            Word.AddIns(g_xMPNAdminDot).Installed = False
            
'           cleanup after ourselves
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "D�sirez-vous enlever tous le fichiers de num�rotation MacPac de votre r�pertoire mod�les utilisateur ?"
            Else
                xMsg = "Would you like us to remove MacPac Numbering files from your User templates directory?"
            End If
            lRet = MsgBox(xMsg, vbQuestion + vbYesNo, g_xAppName)
            If lRet = vbYes Then
                DeleteMPFiles
            End If
            Exit Function
        End If
'        End If
'    #End If
    
    bRet = bLoadFiles(g_bAllowTOCLink)
    If Not bRet Then
        Exit Function
    End If
    
    '9.9.3006 - hold onto mpNumbers.sty to avoid having to cycle
    'through Templates collection to find it in a network location in Word 2007
    Set g_oPNumSty = GetTemplate(g_xPNumSty)
    Set g_oFNumSty = GetTemplate(g_xFNumSty)

'   transfer level props to correct
'   storage if necessary - ie if none
'   are currently there
'    If Word.Templates(g_xFNumSty).CustomDocumentProperties.Count = 1 Then
'        TransferPropsToDocProps Word.Templates(g_xFNumSty)
'    End If
    
'   initialize numbering
    lRet = lInitializeNumbering

'   get lists
    bRet = bAppGetLists()
    
'   get schemes
    lRet = iGetSchemes(g_xPSchemes(), mpSchemeType_Private)
    lRet = iGetSchemes(g_xFSchemes(), mpSchemeType_Public)
    
    If Not g_bIsAdmin Then
'       reset compatibility options in private sty file if necessary (9.8.1001)
        On Error Resume Next
        bDo = CBool(xGetAppIni("Numbering", "EnsurePrivateStyCompatibility"))
        On Error GoTo ProcError
        If bDo Then
            '9.9.6015 (GLOG 5166) - convert private sty file if necessary -
            'EnsureCompatibilityOptions() is now obsolete
'            EnsureCompatibilityOptions g_oPNumSty
            ConvertPrivateStyFileIfNecessary
        End If
        
'       delete autotext entries in private sty file (9.8.1007)
        With g_oPNumSty
            For Each oAT In .AutoTextEntries
                 oAT.Delete
            Next oAT
        End With
    End If
    
'   create global variable for session
    CreateSessionVariable
    
'   remove sty files as add-ins
    UnLoadStyFiles
    
'   delete 00mpnStart.dot
    RemoveStartDot
    
    bAppInitialize = True
    Exit Function
    
ProcError:
    Application.ScreenUpdating = True
    EchoOn
    RaiseError xErrSource
End Function

Function GetAppPaths()
'   loads start, work, user and litigation paths
'   into respective global vars
    Dim xAppPath As String
    Dim lPos As Long
    
    xAppPath = mpBase.GetAppPath
    g_xWorkgroupPath = Application.Options.DefaultFilePath(wdWorkgroupTemplatesPath)
        
'   get admin schemes directory
    On Error Resume Next
    g_xAdminSchemesPath = Word.System.PrivateProfileString(xAppPath & _
        "\mpN90.ini", "Numbering", "AdminDirectory")
    If g_xAdminSchemesPath = "" Then
        g_xAdminSchemesPath = xAppPath & "\Admin Schemes"
    ElseIf (InStr(UCase(g_xAdminSchemesPath), "<USERNAME>") > 0) Or _
            (InStr(UCase(g_xAdminSchemesPath), "<USER>") > 0) Then
'       use API to get Windows user name
        g_xAdminSchemesPath = GetUserVarPath(g_xAdminSchemesPath)
    Else
'       use environmental variable
        g_xAdminSchemesPath = GetEnvironVarPath(g_xAdminSchemesPath)
    End If
    If Right(g_xAdminSchemesPath, 1) = "\" Then
        g_xAdminSchemesPath = Left(g_xAdminSchemesPath, _
            Len(g_xAdminSchemesPath) - 1)
    End If
    On Error GoTo 0
    
'   get personal directory
    g_xUserPath = GetUserDir(xAppPath)
    
    If g_bIsAdmin Then
        g_xMPBPath = g_xAdminSchemesPath
    Else
        g_xMPBPath = g_xUserPath
    End If
    
    'get pdf help file path (9.9.4006)
    lPos = InStrRev(App.Path, "\")
    g_xHelpPath = Left$(App.Path, lPos) & "Tools\User Documentation\pdf"
End Function

Function bQuit()
    Word.Application.Quit
End Function

Function bSetUserIni(xSection As String, _
                             xKey As String, _
                             xValue As String) As Boolean
    System.PrivateProfileString(g_xUserIni, _
        xSection, xKey) = xValue
End Function

Function xGetUserIni(xSection As String, _
                             xKey As String) As String
    xGetUserIni = System.PrivateProfileString(g_xUserIni, _
        xSection, xKey)
End Function


Public Function RaiseError(xSource As String)
    Dim oError As CError
    Set oError = New CError
    Err.Source = xSource
    EchoOn
    Screen.MousePointer = vbDefault
    Application.StatusBar = ""
'    mdlApplication.SendShiftKey 'remmed 9.9.6014
    oError.Raise Err
    Set oError = Nothing
End Function

Public Sub GetAppOptions()
    Dim xKey As String
    Dim bDisallow As Boolean
    Dim xDMS As String
    Dim xValue As String
    
'   get options
    On Error GoTo ProcError
    xKey = "AllowSchemeSharing"
    g_bAllowSchemeSharing = xGetAppIni( _
                "Numbering", "AllowSchemeSharing")
                
    xKey = "AllowSchemeEdit"
    g_bAllowSchemeEdit = xGetAppIni( _
                "Numbering", "AllowSchemeEdit")
                    
    xKey = "AllowSchemeNew"
    g_bAllowSchemeNew = xGetAppIni( _
                "Numbering", "AllowSchemeNew")
                
    xKey = "AllowLinkToTOCScheme"
    g_bAllowTOCLink = CBool(xGetAppIni("Numbering", _
        "AllowLinkToTOCScheme")) And g_bAllowSchemeNew
        
    g_bShowPersonalSchemes = (g_bAllowSchemeSharing Or _
                              g_bAllowSchemeNew)
    
    On Error Resume Next
    
    bDisallow = xGetAppIni("Numbering", "DisableSchemeIndentsInWord97")
    If bDisallow Then
        g_bNoSchemeIndents = (InStr(Application.Version, "8.") <> 0)
    End If
        
    g_iUpgradeFrom = xGetAppIni("Numbering", "UpgradeFrom")
    g_bResetFonts = xGetAppIni("Numbering", "ResetFontsOnFileOpen")
    g_bApplyHeadingColor = xGetAppIni("Numbering", "ApplyColorToHeadings")
    g_bDocumentChange = xGetAppIni("Numbering", "DocumentChangeEvent")
    g_bAlwaysAdjustSpacing = xGetAppIni("Numbering", "AdjustSpacingInAllSchemes")
    g_bAcceptAllFlags = xGetAppIni("Numbering", "AcceptAll9.7Flags")
    g_bCodeForMSBug = xGetAppIni("Numbering", "CodeForMicrosoftIndentBug")
    g_bPost97AdminWarning = xGetAppIni("Numbering", "Post97AdminWarning")
    g_bPreserveRestarts = xGetAppIni("Numbering", "PreserveRestartsOnChangeTo")
    g_bGenerateBufferFromCopy = xGetAppIni("Numbering", "GenerateBufferFromCopy")
    g_lActiveSchemeIcon = xGetAppIni("Numbering", "ActiveSchemeIcon")
    g_xBufferTemplate = xGetAppIni("Numbering", "BufferTemplate")
    
'   set to no upgrade if key is not present
    If g_iUpgradeFrom = 0 Then
        g_iUpgradeFrom = 90
    End If
    
'   get style to apply when removing a number
    g_xRemoveNumberStyle = xGetAppIni( _
        "Numbering", "RemoveNumberStyle")
    If Len(g_xRemoveNumberStyle) = 0 Then
        g_xRemoveNumberStyle = "Normal"
    End If
    
'   get language-specific list num text
    g_xListNum = xGetAppIni("Numbering", "ListNumText")
    If g_xListNum = "" Then g_xListNum = "LISTNUM"
    
'   get DMS - first look in MacPac.ini
    If g_xMP90Dir <> "" Then
        'look in 9.7.1 location
        xDMS = System.PrivateProfileString(g_xMP90Dir & _
            "MacPac.ini", "DMS", "DMS")
        If xDMS = "" Then
            'look in pre-9.7.1 location
            xDMS = System.PrivateProfileString(g_xMP90Dir & _
                "MacPac.ini", "General", "DMS")
        End If
        g_iDMS = CInt(xDMS)
    Else
        g_iDMS = xGetAppIni("General", "DMS")
    End If
    
    '9.9.2010 - Organizer save prompt
    If InStr(Application.Version, "12.") <> 0 Then
        g_bOrganizerSavePrompt = xGetAppIni("General", "Word2007SP2OrganizerSavePrompt")
        If (g_xMP90Dir <> "") And (g_bOrganizerSavePrompt = False) Then
            xValue = System.PrivateProfileString(g_xMP90Dir & "MacPac.ini", _
                "General", "Word2007SP2OrganizerSavePrompt")
            If xValue <> "" Then _
                g_bOrganizerSavePrompt = CBool(xValue)
        End If
    End If
    
    '9.9.4007 - added new ini key for organizer save prompt that's
    'not specific to any Word version
    If Not g_bOrganizerSavePrompt Then _
        g_bOrganizerSavePrompt = xGetAppIni("General", "OrganizerSavePrompt")
    
    '9.9.4004 - option to not clear undo list
    xValue = xGetAppIni("General", "PreserveUndoList")
    g_bPreserveUndoList = (UCase$(xValue) = "TRUE")
    
    '9.9.4010 (GLOG 4917) - option to use unlinked paragraph styles
    If g_iWordVersion >= mpWordVersion_2007 Then
        xValue = ""
        xValue = xGetAppIni("Numbering", "CreateUnlinkedStyles")
        If xValue <> "" Then _
            g_bCreateUnlinkedStyles = (UCase$(xValue) = "TRUE")
    End If
    
    '9.9.4014 (GLOG 5025) - option to prompt before automatically relinking
    xValue = ""
    xValue = xGetAppIni("Numbering", "PromptBeforeAutoRelinking")
    If xValue <> "" Then _
        g_bPromptBeforeAutoRelinking = (UCase$(xValue) = "TRUE")
    
    '9.9.4018 (GLOG 5160) - option to bypass timer by setting existing key to 0
    g_xTimerDelay = xGetAppIni("Timer", "Delay")
    
    '9.9.5012 (GLOG 5461) - option to warn if track changes is on
    xValue = xGetAppIni("Numbering", "TrackChangesWarningThreshold")
    If IsNumeric(xValue) Then
        g_lTrackChangesWarningThreshold = CLng(xValue)
    Else
        g_lTrackChangesWarningThreshold = 1000
    End If
    
    '9.9.6004 (GLOG 5525) - option to load cont styles with scheme or on demand
    xValue = xGetAppIni("Numbering", "LoadContinuationStyles")
    If (xValue = "1") Or (xValue = "2") Then
        g_iLoadContStyles = CInt(xValue)
    Else
        g_iLoadContStyles = mpLoadWithScheme
    End If
    
    '9.9.6012 (GLOG 5625) - different loop lengths to get ruler to display
    'in edit scheme preview under different circumstances
    'edit from New Scheme dialog
    xValue = ""
    xValue = xGetAppIni("Numbering", "RulerDisplayDelay_New")
    If xValue = "" Then
        g_lRulerDisplayDelay_New = 125
    Else
        g_lRulerDisplayDelay_New = CLng(xValue)
    End If
    
    'edit from Schemes dialog
    xValue = ""
    xValue = xGetAppIni("Numbering", "RulerDisplayDelay_Edit")
    If xValue = "" Then
        g_lRulerDisplayDelay_Edit = 4
    Else
        g_lRulerDisplayDelay_Edit = CLng(xValue)
    End If
    
    'edit from ribbon
    xValue = ""
    xValue = xGetAppIni("Numbering", "RulerDisplayDelay_EditDirect")
    If xValue = "" Then
        g_lRulerDisplayDelay_EditDirect = 3
    Else
        g_lRulerDisplayDelay_EditDirect = CLng(xValue)
    End If
    
    'GLOG 5624 (9.9.6012)
    xValue = ""
    xValue = xGetAppIni("Numbering", "DefaultZoomPercentage")
    If xValue = "" Then
        g_lDefaultZoomPercentage = 100
    Else
        g_lDefaultZoomPercentage = CLng(xValue)
    End If
    
    'GLOG 5643 (9.9.6014)
    xValue = ""
    xValue = xGetAppIni("Numbering", "CollapseRibbonOnDirectEdit")
    g_bCollapseRibbon = (UCase$(xValue) <> "FALSE")
    
    Err.Clear
    Exit Sub
    
ProcError:
    If g_lUILanguage = wdFrenchCanadian Then
        xMsg = "Impossible de r�cup�rer les valeurs de demarrage.  " & _
               "La cl� '" & xKey & "' key est manquante. Veuillez contacter votre administrateur."
    Else
        xMsg = "Could not retrieve required initialization values.  " & _
               "The '" & xKey & "' key is missing.  Please contact " & _
               "your administrator."
    End If
    MsgBox xMsg, vbCritical, g_xAppName
    Exit Sub
End Sub

Public Function DocumentChange() As Long
'handles all necessary actions that should
'occur on a 'change' of doc (as defined by Word)
    Dim bDirty As Boolean
    Dim xScheme As String
    Dim cbNum As CommandBar
    Dim xTimeMsg As String
        
'    xTimeMsg = "Doc change started at " & Now & vbCr
    
    If g_bPreventDocChangeEvent Then
        Exit Function
    End If
    
'   reset global variable
    g_bLeaveDocDirty = False

'   test added for compatibility with Dragon NaturallySpeaking
'   which alters Word environment behind the scenes - in general,
'   if numbering toolbar isn't around, there's no reason to run doc change
    On Error Resume Next
    Set cbNum = CommandBars(mpNumberingToolbar)
    On Error GoTo 0
    If cbNum Is Nothing Then
        Exit Function
    End If
    
    Word.Application.ScreenUpdating = False
    If Word.Windows.Count Then
'       get current dirt status
        bDirty = Word.ActiveDocument.Saved
        
        On Error Resume Next
'       check protected status
        If Not bDocProtected(ActiveDocument, False) Then
'           perform any necessary conversions
            DoConversions
        Else
            Exit Function
        End If
        
'       get MacPac Numbering 9.0 scheme
        xScheme = Word.ActiveDocument _
            .Variables(mpActiveSchemeDocVar)
    End If
    
'   this routine may be called from outside of
'   numbering, e.g. MP2K - only manipulate
'   toolbar if doc change event is enabled
    If g_bDocumentChange Then
'       enable/disable numbering menu
        With Word.Application.CommandBars("Menu Bar")
            If Word.Application.Windows.Count Then
                DoEvents
                If ActiveWindow.View <> wdPrintPreview Then
                    bSetMenuCtlState True, xScheme
                End If
            Else
                bSetMenuCtlState False
            End If
        End With

'       enable/disable numbering toolbar controls
        With cbNum
            If Word.Application.Windows.Count = 0 Then
                bSetNumberingTbrEnable False
            Else
                .Enabled = True
                If Len(xScheme) Then
'                   trim origin prefix from scheme id
                    xScheme = Mid(xScheme, 2)
                End If
                
                On Error GoTo ProcError
                bSetNumberingTbrEnable True, xScheme
            End If
        End With
    End If

'   return to original dirt status
    MarkStartupSaved
    If Word.Documents.Count And _
            (Not g_bLeaveDocDirty) Then
        Word.ActiveDocument.Saved = bDirty
    End If
    
    Word.Application.ScreenUpdating = True
'    If Word.Documents.Count Then
'        xTimeMsg = xTimeMsg & "Doc change ended at " & Now & vbCr
'        Selection.InsertAfter xTimeMsg
'    End If
    Exit Function
    
ProcError:
    g_bPreventDocChangeEvent = False
    Select Case Err
        Case Else
            DocumentChange = Err.Number
            RaiseError "mpApplication.DocumentChange"
    End Select
    Exit Function
End Function

Function MarkStartupSaved()
'force startup templates to be 'saved'
    On Error Resume Next
    With Word.Application
        GetTemplate(g_xStartPath & "\" & g_xMPNDot).Saved = True
'       error trap isn't enough for Word 10;
'       if files aren't there, trying to set .Saved will
'       disable mouse pointer for rest of session
        If Dir(g_xStartPath & "\" & g_xMPNConvertDot) <> "" Then _
            GetTemplate(g_xStartPath & "\" & g_xMPNConvertDot).Saved = True
        If Dir(g_xStartPath & "\" & g_xMPNAdminDot) <> "" Then _
            GetTemplate(g_xStartPath & "\" & g_xMPNAdminDot).Saved = True
    End With
    Err.Clear
End Function

Public Sub AlertBadFileVer(ByVal xFile As String)
    If g_lUILanguage = wdFrenchCanadian Then
        If Dir(xFile) = "" Then
            xMsg = "Le fichier '" & xFile & "' n'est pas install� sur votre ordinateur. Veuillez contacter votre administrateur pour obtenir la version appropri�e."
        Else
            xMsg = "La version de ce fichier '" & xFile & "' install�e sur votre ordinateur ne peut pas utiliser Microsoft Word version " & _
                    Word.Application.Version & "." & vbCr & vbCr & _
                    "Veuillez contacter votre administrateur pour obtenir la version appropri�e."
        End If
    Else
        If Dir(xFile) = "" Then
            xMsg = "The file '" & xFile & "' is not installed on " & _
                   "this machine." & vbCr & "Please contact your administrator " & _
                   "to obtain the appropriate version of this file."
        Else
            xMsg = "The version of the file '" & xFile & _
                   "' that is " & vbCr & "currently installed on " & _
                   "this machine can not run in Microsoft Word version " & _
                    Word.Application.Version & "." & vbCr & vbCr & _
                    "Please contact your administrator to obtain " & _
                    "the appropriate version of this file."
        End If
    End If
    MsgBox xMsg, vbCritical, g_xAppName
End Sub

Public Sub SetVersionFiles()
    Dim xCurWordVer As String
    Dim xmpWDXX As String
    Dim xFinalWD As String
    
    On Error Resume Next
    
'   ensure that correct version of mpWDXX.dll exists -
'   delete existing Word Version-Specific dll-
    xFinalWD = App.Path & "\mpWDXX.dll"
    If Dir(xFinalWD) <> "" Then
        Kill xFinalWD
'       exit sub if permission denied -
'       file is in use - this typically happens
'       when running the client application
'       at design time - vb will start mpWDXX.dll
        If Err = 70 Then
            Exit Sub
        End If
    End If
    
'   get Word version
    xCurWordVer = Word.Application.Version

'   get correct version of mpWDXX.dll
'   based on current version of Word
    If InStr(xCurWordVer, "8.") Then
        xmpWDXX = "\mpWD80.dll"
    Else
        xmpWDXX = "\mpWD90.dll"
    End If

'   copy correct version
    FileCopy App.Path & xmpWDXX, _
             App.Path & "\mpWDXX.dll"
End Sub
Function bDemoIsExpired() As Boolean
    Dim ldiff As Long
    Dim xDate As String
    On Error Resume Next
    xDate = System.PrivateProfileString("", mpDemoRegKey, "DemoInstall")
    If xDate = "" Then System.PrivateProfileString("", mpDemoRegKey, "DemoInstall") = Now
    ldiff = DateDiff("d", Now, xDate)
    If Abs(ldiff) > 30 Then bDemoIsExpired = True
End Function

Function WorkAsAdmin() As Long
    Dim xFile As String
    
    On Error GoTo WorkAsAdmin_Error
    
'   set admin flags
    g_bIsAdmin = True
    g_bAdminIsDirty = False
    lSetAdminFlag True
    
'   remove user limitations
    g_bAllowSchemeEdit = True
    g_bAllowSchemeNew = True
    g_bAllowTOCLink = True
    g_bShowPersonalSchemes = True
    
'   point to admin sty files
    GetAppPaths
    GetAppFiles
    
'   load clean sty as public
    Word.AddIns.Add g_xFNumSty
    lRet = iGetSchemes(g_xFSchemes(), mpSchemeType_Public)

'   if admin directory exists, load schemes as private
    If Dir(g_xPNumSty) = "" Then
        lRet = iGetSchemes(g_xPSchemes(), mpSchemeType_Public)
    Else
        Word.AddIns.Add g_xPNumSty
        lRet = iGetSchemes(g_xPSchemes(), mpSchemeType_Private)
    End If
    
    '9.9.3006 - hold onto mpNumbers.sty to avoid having to cycle
    'through Templates collection to find it in a network location in Word 2007
    Set g_oPNumSty = GetTemplate(g_xPNumSty)
    Set g_oFNumSty = GetTemplate(g_xFNumSty)

'   show dialogs
    g_iSchemeEditMode = mpModeSchemeMain
    ShowSchemesDialogs
    
    Exit Function
    
WorkAsAdmin_Error:
    WorkAsAdmin = Err.Number
    RaiseError "mdlApplication.WorkAsAdmin"
End Function

Function WorkAsUser() As Long
    If Not g_bIsAdmin Then _
        Exit Function
        
    g_bIsAdmin = False
    lSetAdminFlag False
    
    With Word.AddIns
        If Dir(g_xPNumSty) <> "" Then _
            .Item(g_xPNumSty).Delete
        .Item(g_xFNumSty).Delete
    End With
    
    bAppInitialize
End Function

Function LoadPublicSchemesAsAdmin()
    Dim xFile As String
    Dim xAppPath As String
    
    On Error Resume Next
    
    xAppPath = mpBase.GetAppPath
    
'   create admin schemes directory if necessary
    If Dir(g_xAdminSchemesPath) = "" Then
        MkDir g_xAdminSchemesPath
        If Err = 76 Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Le r�pertoire Admin n'a pas �t� cr��, l'emplacement precise dans mpn90.ini, " & Chr(34) & g_xAdminSchemesPath & _
                    Chr(34) & ", est invalide.  Veuillez fermer la bo�te de dialogue, modifier l'emplacement et essayez de nouveau.", vbCritical, g_xAppName
            Else
                MsgBox "The Admin Directory could not be created because the " & _
                    "path specified in mpn90.ini, " & Chr(34) & g_xAdminSchemesPath & _
                    Chr(34) & ", is invalid.  Please close this dialog box, " & _
                    "edit the path, and try again.", vbCritical, g_xAppName
            End If
            Exit Function
        End If
    End If

'   remove as add-in before trying to delete
    Word.AddIns(g_xPNumSty).Installed = False
    
'   clear admin schemes directory
    Kill g_xAdminSchemesPath & "\*.*"
    
    On Error GoTo LoadPublicSchemesAsAdmin_Error
    
'   copy public bitmaps
    xFile = Dir(xAppPath & "\*.mpb")
    While xFile <> ""
        FileCopy xAppPath & "\" & xFile, _
            g_xAdminSchemesPath & "\" & xFile
        xFile = Dir()
    Wend
    
'   copy public mpNumbers.sty
    Word.AddIns(xAppPath & "\mpNumbers.sty").Installed = False
    FileCopy xAppPath & "\mpNumbers.sty", g_xPNumSty
    
'   load admin schemes
    On Error Resume Next
    Word.AddIns.Add g_xPNumSty
    On Error GoTo LoadPublicSchemesAsAdmin_Error
    
    Word.AddIns(g_xPNumSty).Installed = True
    Set g_oPNumSty = GetTemplate(g_xPNumSty)
    lRet = iGetSchemes(g_xPSchemes(), mpSchemeType_Private)
    
    Exit Function

LoadPublicSchemesAsAdmin_Error:
    LoadPublicSchemesAsAdmin = Err.Number
    RaiseError "mdlApplication.LoadPublicSchemesAsAdmin"
End Function

Function DeleteMPFiles() As Long
    Dim xFile As String
    Dim xNumbersSty As String
    
    On Error Resume Next
    Word.AddIns(g_xPNumSty).Installed = False
    
    On Error GoTo DeleteMPFiles_Error

'   delete personal files
    If Dir(g_xPNumSty) <> "" Then _
        Kill g_xPNumSty
    If Dir(g_xUserPath & "\NumTOC.ini") <> "" Then _
        Kill g_xUserPath & "\NumTOC.ini"
    xFile = Dir(g_xUserPath & "\*.mpb")
    While xFile <> ""
        Kill g_xUserPath & "\" & xFile
        xFile = Dir()
    Wend
    
    If g_lUILanguage = wdFrenchCanadian Then
        xMsg = "Les fichiers MacPac ont �t� supprim�s. Pour supprimer enti�rement MacPac Num�rotation 2000:" & String(2, 13) & _
            "1. Enlevez " & g_xMPNDot & ", " & g_xMPNAdminDot & ", et " & _
            g_xMPNConvertDot & " du r�pertoire de D�marrage Word." & _
            String(2, 13) & "2. Allez � " & Chr(34) & "Ajout/Suppression de programmes" & Chr(34) & " dans le Panneau de configuration Windows et s�lectionnez MacPac dans la liste des programmes."
    Else
        xMsg = "The specified MacPac files have been deleted.  To remove " & _
            "the remainder of MacPac Numbering 2000:" & String(2, 13) & _
            "1. Remove " & g_xMPNDot & ", " & g_xMPNAdminDot & ", and " & _
            g_xMPNConvertDot & " from your Word Startup directory." & _
            String(2, 13) & "2. Go to " & Chr(34) & "Add/Remove Programs" & Chr(34) & _
            " on the Windows Control Panel, and select " & Chr(34) & _
            "MacPac Numbering" & Chr(34) & " from the list of programs."
    End If
    MsgBox xMsg, vbInformation, g_xAppName
    
    Exit Function

DeleteMPFiles_Error:
    If g_lUILanguage = wdFrenchCanadian Then
        xMsg = "Impossible de supprimer les fichiers pr�cis�s. Pour supprimer MacPac Num�rotation 2000:" & String(2, 13) & _
            "1. Supprimez mpNumbers.sty, NumTOC.ini et tous fichiers avec l'extension .mpb du r�pertoire Word mod�les utilisateur." & String(2, 13) & _
            "2. Supprimez " & g_xMPNDot & ", " & g_xMPNAdminDot & ", et " & _
            g_xMPNConvertDot & " du r�pertoire de D�marrage Word." & _
            String(2, 13) & "3. Allez � " & Chr(34) & "Ajout/Suppression de programmes" & Chr(34) & " dans le Panneau de configuration Windows et s�lectionnez MacPac dans la liste des programmes."
    Else
        xMsg = "Unable to delete specified files.  To remove " & _
            "MacPac Numbering 2000:" & String(2, 13) & _
            "1. Delete mpNumbers.sty, NumTOC.ini, and all files with an .mpb extension, " & _
            "from your Word User templates directory." & String(2, 13) & _
            "2. Remove " & g_xMPNDot & ", " & g_xMPNAdminDot & ", and " & _
            g_xMPNConvertDot & " from your Word Startup directory." & _
            String(2, 13) & "3. Go to " & Chr(34) & "Add/Remove Programs" & Chr(34) & _
            " on the Windows Control Panel, and select " & Chr(34) & _
            "MacPac Numbering" & Chr(34) & " from the list of programs."
    End If
    MsgBox xMsg, vbInformation, g_xAppName
    Exit Function
    
End Function

Sub RemoveBufferFromList()
    On Error GoTo ProcError
    With Word.Application.RecentFiles
        If .Count Then
            With .Item(1)
                If (InStr(UCase(.Name), "BUFFER.MPF")) + _
                        (InStr(UCase(.Name), "REFRESH.MPF")) > 0 Then
                    .Delete
                End If
            End With
        End If
    End With
ProcError:
End Sub

Public Function MacPacIsLegal() As Boolean
    Dim d As Date
    Dim xCode As String
    Dim bIsValid As Boolean
    Dim oReg As CRegistry
    Dim oForm As VB.Form
    Dim iPos As Integer
    Dim xMPPath As String
    
    On Error GoTo ProcError
    
'   get MacPac app directory if it exists
    g_xMP90Dir = mpBase.GetMacPacAppPath
        
    If g_xMP90Dir = "" Then
'       registry may be unavailable, e.g. Citrix;
'       try to locate MacPac.ini by relative location to Numbering
        iPos = InStr(UCase(App.Path), "\NUMBERING\APP")
        If iPos Then
            xMPPath = Left(App.Path, iPos) & "MacPac90\App\"
            If Dir(xMPPath & "MacPac.ini") <> "" Then
                g_xMP90Dir = xMPPath
            End If
        End If
    End If

'   first look for license in mpn90.ini
    xCode = xGetAppIni("General", "Signature")
    
'   if no license in mpn90.ini, look in MacPac.ini
    If xCode = "" Then
        If g_xMP90Dir <> "" Then
            xCode = System.PrivateProfileString(g_xMP90Dir & _
                "MacPac.ini", "General", "Signature")
            If xCode <> "" Then
'               write license to mpn90.ini
                bSetAppIni "General", "Signature", xCode
            End If
        End If
    End If
                  
    If xCode = "" Then
        bIsValid = False
    Else
'       unencrypt date
        d = UnEncryptDate(xCode)
        bIsValid = (Now < d)
    End If
    
'   load license key prompt
    If Not bIsValid Then
        If g_lUILanguage = wdFrenchCanadian Then
            Set oForm = New frmLicenseNumFrench
        Else
            Set oForm = New frmLicenseNum
        End If
    End If

    While Not bIsValid
        With oForm
'           show prompt
            .Show vbModal

            If .Cancelled Then
                Exit Function
            Else
'               unencrypt date
                d = UnEncryptDate(.Key)

'               test for validity
                bIsValid = (Now < UnEncryptDate(.Key))

'               write to ini if valid
                If bIsValid Then
                    bSetAppIni "General", "Signature", .Key
                    If g_xMP90Dir <> "" Then
'                       prevent second prompt when MacPac initializes
                        System.PrivateProfileString(g_xMP90Dir & _
                            "MacPac.ini", "General", "Signature") = .Key
                    End If
                Else
'                   set message for next time around
                    .Message = "Invalid entry.  Please enter a valid license number:"
                End If
            End If
        End With
    Wend

'   unload form if necessary
    If Not oForm Is Nothing Then
        Unload oForm
    End If
    
    MacPacIsLegal = bIsValid
    Exit Function
ProcError:
    RaiseError "MPN90.mdlApplication.MacPacIsLegal"
End Function

Public Function UnEncryptDate(ByVal xCode As String) As Date
'returns the date encrypted in xcode
    Dim bRnd As Byte
    Dim xCodeMod As String
    Dim i As Integer
    Dim xDate As String
    Dim X As String
    
    On Error GoTo ProcError
    
'   remove garbage 1st digit
    xCode = Mid(xCode, 2)

'   store and remove random factor
    bRnd = Left(xCode, 1)
    xCode = Mid(xCode, 2)
    
'   collect chars at even indexes
    For i = 2 To Len(xCode) Step 2
        xCodeMod = xCodeMod & Mid(xCode, i, 1)
    Next
    
    While Len(xCodeMod)
        X = Left(xCodeMod, 1)
        If Not IsNumeric(X) Then
            X = Asc(X) - 100
        End If
        
        xDate = xDate & X
        
'       trim left char
        xCodeMod = Mid(xCodeMod, 2)
    Wend
    
'DanCore 9.2.0 - UnEncryptDateFix - remember to add fix to PostInstall utility
'   convert to date
    UnEncryptDate = DateSerial(Mid(xDate, 3, 4), Mid(xDate, 7, 2), Mid(xDate, 1, 2))
    Exit Function
    
ProcError:
    Exit Function
End Function

Function UnloadTOCSty() As Boolean
    Dim xTOCSTY As String
    
    On Error Resume Next
    
    xTOCSTY = mpBase.GetAppPath & "\mpTOC.sty"
    Word.AddIns(xTOCSTY).Installed = False
End Function

Public Function RemoveStartDot()
    Dim aiAddIn As Word.AddIn
    
    On Error Resume Next
    
    For Each aiAddIn In Word.AddIns
        With aiAddIn
            If ((UCase(.Name) = "00MPNSTART.DOT") Or _
                    (UCase(.Name) = "00MPNSTART.DOTM")) And _
                    (.Installed = True) Then
                .Delete
                Kill .Path & "\" & .Name
                Exit Function
            End If
        End With
    Next aiAddIn
End Function

Public Sub ConvertPrivateStyFileIfNecessary()
'added in 9.9.6015 (GLOG 5166)
    Dim iVersion As Integer
    Dim iNewVersion As Integer
    Dim oDoc As Word.Document
    Dim oDoc1 As Word.Document
    
    On Error GoTo ProcError
    
    If g_iWordVersion < mpWordVersion_2010 Then _
        Exit Sub
        
    'check doc prop for current compatibility - this wasn't
    'maintained in the past, but at worst will indicate older
    'version, forcing us to open the template unnecessarily -
    'no need to convert 2010 and later because there have been
    'no subsequent compatibility changes relevant to Numbering
    On Error Resume Next
    iVersion = g_oPNumSty.CustomDocumentProperties("WordVersion")
    On Error GoTo ProcError
    If iVersion >= mpWordVersion_2010 Then _
        Exit Sub
    
    'opening template as doc will close untouched document 1
    For Each oDoc In Application.Documents
        If oDoc.Name = "Document1" Then
            If oDoc.Saved = True Then
                oDoc.Saved = False
                Set oDoc1 = oDoc
                Exit For
            End If
        End If
    Next oDoc
    
    EchoOff
    Application.ScreenUpdating = False
    
    'convert if necessary
    Set oDoc = g_oPNumSty.OpenAsDocument
    If mdlWord14.GetCompatibilityMode(oDoc) < 14 Then
        mdlWord14.Convert oDoc
    End If
    
    With oDoc
        'add/update doc prop
        iNewVersion = mdlWord14.GetCompatibilityMode(oDoc)
        If iVersion = 0 Then
            .CustomDocumentProperties.Add "WordVersion", False, _
                msoPropertyTypeNumber, iNewVersion
        Else
            .CustomDocumentProperties("WordVersion").Value = iNewVersion
        End If
        
        'save template
        .Saved = False
        .SaveAs g_oPNumSty.FullName
        .Close False
    End With
    
    'retore document 1 if necessary
    If Not oDoc1 Is Nothing Then
        oDoc1.Saved = True
    End If
    
    'remove mpNumbers.sty from recent files list
    With Word.Application.RecentFiles
        If .Count Then
            With .Item(1)
                If UCase(.Name) = "MPNUMBERS.STY" Then
                    .Delete
                End If
            End With
        End If
    End With
    
    Application.ScreenUpdating = True
    EchoOn
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
    EchoOn
    RaiseError "mdlApplication.ConvertPrivateStyFileIfNecessary"
    Exit Sub
End Sub

Public Sub EnsureCompatibilityOptions(oTemplate As Word.Template)
'resets Word compatibility options if necessary
    Dim iVersion As Integer
    Dim iCount As Integer
    Dim i As Integer
    Dim oDoc As Word.Document
    Dim iDocs As Integer
    
    'check doc prop for current compatibility
    On Error Resume Next
    iVersion = oTemplate.CustomDocumentProperties("WordVersion")
    On Error GoTo ProcError
    
    'exit if compatibility matches version running or
    'if file is in Word 12 format or if current version is Word 12 -
    'resetting to or from Word 2007 compatibility would require saving
    'in a different file format
    If (iVersion = g_iWordVersion) Or (iVersion = mpWordVersion_2007) Or _
            (g_iWordVersion >= mpWordVersion_2007) Then
        Exit Sub
    End If
    
    'get doc count
    iDocs = Application.Documents.Count
    
    'each version has different number of options
    Select Case g_iWordVersion
        Case mpWordVersion_2007
            iCount = 65
        Case mpWordVersion_2003
            iCount = 50
        Case mpWordVersion_XP
            iCount = 48
        Case mpWordVersion_2000
            iCount = 41
        Case Else
            iCount = 32
    End Select
    
    'open template for edit
    Set oDoc = oTemplate.OpenAsDocument
    
    With oDoc
        'cycle through options, turning off all
        For i = 1 To iCount
            If (g_iWordVersion = mpWordVersion_97) Or _
                    (g_iWordVersion >= mpWordVersion_2007) Then
                .Compatibility(i) = False
            ElseIf ((i > 12 And i < 17) Or (i = 21) Or (i = 36)) Then
                .Compatibility(i) = True
            Else
                .Compatibility(i) = False
            End If
        Next i
    
        'add/modify doc prop
        If iVersion = 0 Then
            .CustomDocumentProperties.Add "WordVersion", False, _
                msoPropertyTypeNumber, g_iWordVersion
        Else
            .CustomDocumentProperties("WordVersion").Value = g_iWordVersion
        End If
        
        'close/save template
        .Saved = False
        .Save
        .Close
    End With
    
    'opening template as doc will close unsaved document 1
    If iDocs > 0 Then
        With Application.Documents
            If .Count = 0 Then .Add
        End With
    End If
    
    Exit Sub
ProcError:
    RaiseError "mdlConversion.EnsureCompatibility"
    Exit Sub
End Sub

Private Sub GetGlobalTemplateNames()
    '9.9.2008 - the name of the main global template can now be specified in the ini
    '9.9.3 - if it's "MacPac Numbering.dotm" or "MacPac Numbering French.dotm", we
    'already have the name
    If g_xMPNDot = "" Then _
        g_xMPNDot = xGetAppIni("General", "GlobalTemplateName")
    
    If (InStr(Application.Version, "12.") <> 0) Or _
            (InStr(Application.Version, "14.") <> 0) Or _
            (InStr(Application.Version, "15.") <> 0) Or _
            (InStr(Application.Version, "16.") <> 0) Then
        If g_xMPNDot = "" Then _
            g_xMPNDot = "MacPac Numbering.dotm"
        g_xMPNAdminDot = "MacPac Numbering Administration.dotm"
        g_xMPNConvertDot = "MacPac Numbering Convert.dotm"
    Else
        If g_xMPNDot = "" Then _
            g_xMPNDot = "@@MPN.dot"
        g_xMPNAdminDot = "MPNAdmin.dot"
        g_xMPNConvertDot = "MPNConvert.dot"
    End If
End Sub

'********************************
'Benchmark functions - use these
'instead of Now - they're much
'more precise

Function CurrentTick() As Long
    CurrentTick = GetTickCount()
End Function

Function ElapsedTime(lStartTick As Long) As Single
'returns the time elapsed from lStartTick-
'precision in milliseconds
    ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
End Function
'********************************

Private Function GetUserDir(ByVal xAppPath As String) As String
    ' Checks for existence of path specified for UserDir in mpn90.ini
    ' Creates paths if necessary (for instance in <AppData> for a new user)
    Dim xUserDir As String
    Dim oFSO As Scripting.FileSystemObject
    Dim oFolder As Scripting.Folder
    Dim oFile As Scripting.File
    Dim xCacheFolder As String
    Dim lPos As Long
    
    On Error GoTo ProcError
    
    xUserDir = Word.System.PrivateProfileString(xAppPath & "\mpN90.ini", _
        "General", "UserDir")
    If xUserDir <> "" Then
        'trim trailing slash
        If Right(xUserDir, 1) = "\" Then _
            xUserDir = Left(xUserDir, Len(xUserDir) - 1)
    
        'evaluate variables
        If (InStr(UCase(xUserDir), "<USERNAME>") > 0) Or _
                (InStr(UCase(xUserDir), "<USER>") > 0) Then
            'use API to get Windows user name
            xUserDir = GetUserVarPath(xUserDir)
        Else
            'use environmental variable
            xUserDir = GetEnvironVarPath(xUserDir)
        End If
        
        Set oFSO = New Scripting.FileSystemObject
        'GLOG 5479 (9.9.6004) - check for NumTOC.ini as well
        If (Not oFSO.FileExists(xUserDir & "\mpNumbers.sty")) Or _
                (Not oFSO.FileExists(xUserDir & "\NumTOC.ini")) Then
            'attempt to copy personal files from cache folder
            'GLOG 5564 (9.9.6007) - first check in new public location
            Dim bCacheFound As Boolean
            If UCase$(Right$(xAppPath, 4)) = "\APP" Then
                xCacheFolder = Left$(xAppPath, Len(xAppPath) - 3) & "UserCache"
                bCacheFound = oFSO.FolderExists(xCacheFolder)
            End If
            
            'if not in new location, check in old
            If Not bCacheFound Then
                lPos = InStrRev(App.Path, "\")
                xCacheFolder = Left$(App.Path, lPos) & "Tools\UserCache"
                bCacheFound = oFSO.FolderExists(xCacheFolder)
            End If
            
            If bCacheFound Then
                'cache folder exists - create specified directory if necessary
                bCreatePath xUserDir
                
                'if specified folder exists, copy personal files from cache;
                'don't overwrite existing
                If oFSO.FolderExists(xUserDir) Then
                    On Error Resume Next
                    For Each oFile In oFSO.GetFolder(xCacheFolder).Files
                        oFSO.CopyFile xCacheFolder & "\" & oFile.Name, _
                            xUserDir & "\" & oFile.Name, False
                    Next oFile
                    On Error GoTo ProcError
                End If
            End If
        End If
    
        'ensure that sty file now exists
        If Not oFSO.FileExists(xUserDir & "\mpNumbers.sty") Then _
            xUserDir = ""
    End If
    
    If xUserDir = "" Then
        'use Word's default path
        xUserDir = Word.Application.Options.DefaultFilePath(wdUserTemplatesPath)
    End If
    
    GetUserDir = xUserDir
    
    Exit Function
ProcError:
    RaiseError "mpn90.mdlApplication.GetUserDir"
End Function

Private Function bCreatePath(xFullPath As String) As Boolean
    Dim oFSO As Scripting.FileSystemObject
    Dim xParent As String
    On Error GoTo ProcError
    Set oFSO = New Scripting.FileSystemObject
    If oFSO.FolderExists(xFullPath) Then  ' Folder already created
        bCreatePath = True
        Exit Function
    End If
    On Error Resume Next
    oFSO.CreateFolder xFullPath
    
    If Not oFSO.FolderExists(xFullPath) Then
        ' Couldn't create bottom level folder, call recursively using Parent Folder
        ' Only only the last level may created new
        xParent = oFSO.GetParentFolderName(xFullPath)
        If xParent = Empty Or Right(xParent, 1) = "\" Then ' Parent folder is Drive Root or nothing
            bCreatePath = False
        Else
            If bCreatePath(xParent) Then
                oFSO.CreateFolder xFullPath
                If oFSO.FolderExists(xFullPath) Then
                    bCreatePath = True
                End If
            End If
        End If
    Else
        bCreatePath = True
    End If
    Exit Function
ProcError:
    RaiseError "mpn90.mdlApplication.bCreatePath"
End Function

Public Function GetTemplate(xFile As String) As Word.Template
'added in 9.9.2009 to workaround native Word 2007 SP2 bug whereby
'Word.Templates() no longer takes a network path -
'moved from cShare in 9.9.3006
    Dim oTemplate As Word.Template
    Dim i As Integer
    Dim xName As String
    Dim lPos As Long
    
    On Error Resume Next
    Set oTemplate = Word.Templates(xFile)
    On Error GoTo ProcError
    
    If oTemplate Is Nothing Then
        lPos = InStrRev(xFile, "\")
        xName = Mid$(xFile, lPos + 1)
        For i = 1 To Word.Templates.Count
            If Word.Templates(i).Name = xName Then
                Set oTemplate = Word.Templates(i)
                Exit For
            End If
        Next i
    End If
    
    Set GetTemplate = oTemplate
    Exit Function
    
ProcError:
    Err.Raise Err.Number
End Function

Public Sub LaunchDocumentByExtension(xFileName As String)
    Dim iRet As Integer
    
    On Error GoTo ProcError
    
    iRet = ShellExecute(0, "open", xFileName, "", "", SW_SHOWNORMAL)
    If iRet <= 32 Then
        MsgBox "The document could not be opened.  " & _
            "There may not be an installed application associated with this extension.", vbExclamation
    End If
    Exit Sub
ProcError:
    RaiseError "frmSchemes.LaunchDocumentByExtension"
End Sub

Public Sub SendShiftKey()
'GLOG 5617 - an unqualified call to SendKeys causes a permission denied error
'when running in design on Windows 10 - needs to be error trapped for earlier
'operating systems without this object
    On Error Resume Next
    CreateObject("Wscript.Shell").SendKeys "+", True
    If Err <> 0 Then
        SendKeys "+", True
    End If
End Sub
