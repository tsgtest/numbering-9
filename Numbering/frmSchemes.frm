VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSchemes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Numbering Schemes"
   ClientHeight    =   5490
   ClientLeft      =   5280
   ClientTop       =   4065
   ClientWidth     =   7020
   Icon            =   "frmSchemes.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5490
   ScaleWidth      =   7020
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin MSComctlLib.TreeView tvwSchemes 
      Height          =   4065
      Left            =   75
      TabIndex        =   7
      Top             =   105
      Width           =   2800
      _ExtentX        =   4948
      _ExtentY        =   7170
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   265
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin VB.TextBox txtDescription 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   630
      Left            =   105
      MaxLength       =   255
      MultiLine       =   -1  'True
      TabIndex        =   9
      Top             =   4250
      Width           =   6775
   End
   Begin VB.CommandButton btnReload 
      Caption         =   "&Load Public Schemes"
      Height          =   380
      Left            =   90
      TabIndex        =   8
      ToolTipText     =   "Load current public schemes as administrator set."
      Top             =   5030
      Visible         =   0   'False
      Width           =   2085
   End
   Begin VB.VScrollBar vsbPreview 
      Height          =   4020
      LargeChange     =   994
      Left            =   6715
      Max             =   3975
      SmallChange     =   398
      TabIndex        =   4
      Top             =   135
      Value           =   375
      Width           =   225
   End
   Begin VB.Frame Frame1 
      Height          =   30
      Left            =   -30
      TabIndex        =   5
      Top             =   15
      Width           =   7200
   End
   Begin VB.CommandButton btnReset 
      Caption         =   "&Reset..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   380
      Left            =   5145
      TabIndex        =   2
      ToolTipText     =   "Reset selected scheme to public or private defaults."
      Top             =   5030
      Width           =   880
   End
   Begin VB.CommandButton btnChangeScheme 
      Caption         =   "Cha&nge to..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   380
      Left            =   4000
      TabIndex        =   1
      ToolTipText     =   "Change all numbered paragraphs or selected numbered paragraphs to this scheme."
      Top             =   5030
      Width           =   1080
   End
   Begin VB.CommandButton btnUse 
      Caption         =   "&Use"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   380
      Left            =   3055
      TabIndex        =   0
      ToolTipText     =   "Use selected scheme to insert numbers."
      Top             =   5030
      Width           =   880
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   380
      Left            =   6090
      TabIndex        =   3
      Top             =   5030
      Width           =   880
   End
   Begin VB.PictureBox imgContainer 
      BackColor       =   &H00FFFFFF&
      Height          =   4065
      Left            =   2980
      ScaleHeight     =   4005
      ScaleWidth      =   3930
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   105
      Width           =   3990
      Begin VB.Image Image1 
         Height          =   375
         Left            =   -15
         Picture         =   "frmSchemes.frx":058A
         Top             =   0
         Width           =   4365
      End
      Begin VB.Image imgPreview 
         Appearance      =   0  'Flat
         Height          =   3675
         Left            =   15
         ToolTipText     =   "Double-click this preview to edit the scheme."
         Top             =   330
         Width           =   3705
      End
   End
   Begin VB.Frame Frame2 
      Height          =   25
      Left            =   -10
      TabIndex        =   10
      Top             =   4925
      Width           =   7300
   End
   Begin VB.Menu mnuScheme 
      Caption         =   "&Scheme"
      Begin VB.Menu mnuScheme_New 
         Caption         =   "&New..."
      End
      Begin VB.Menu mnuScheme_Modify 
         Caption         =   "&Edit..."
      End
      Begin VB.Menu mnuScheme_Delete 
         Caption         =   "&Delete..."
      End
      Begin VB.Menu mnuScheme_Properties 
         Caption         =   "&Properties..."
      End
      Begin VB.Menu mnuScheme_Sep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuScheme_Use 
         Caption         =   "&Use"
      End
      Begin VB.Menu mnuScheme_ChangeTo 
         Caption         =   "C&hange to..."
      End
      Begin VB.Menu mnuScheme_Reset 
         Caption         =   "&Reset..."
      End
      Begin VB.Menu mnuScheme_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuScheme_Relink 
         Caption         =   "Re&link Document Schemes"
      End
      Begin VB.Menu mnuScheme_ConvertToHeadingStyles 
         Caption         =   "&Convert to Word Heading Styles"
      End
      Begin VB.Menu mnuScheme_ConvertToMacPacStyles 
         Caption         =   "&Convert to MacPac Styles"
      End
      Begin VB.Menu mnuScheme_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuScheme_SetAsDefault 
         Caption         =   "&Set as Default"
      End
      Begin VB.Menu mnuScheme_Favorites 
         Caption         =   "Add to &Favorites"
      End
      Begin VB.Menu mnuScheme_UseWordHeadings 
         Caption         =   "Use &Word Headings"
      End
      Begin VB.Menu mnuScheme_InsertLevel1Automatically 
         Caption         =   "&Insert Level 1 Automatically"
      End
   End
   Begin VB.Menu mnuSharing 
      Caption         =   "Sh&are"
      Begin VB.Menu mnuSharing_ExportToFile 
         Caption         =   "&Export To File"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSharing_ImportFromFile 
         Caption         =   "&Import From File..."
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSharing_Sep1 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSharing_ImportPublicScheme 
         Caption         =   "I&mport..."
      End
      Begin VB.Menu mnuSharing_ExportToPublic 
         Caption         =   "&Export..."
      End
      Begin VB.Menu mnuSharing_DeletePublicSchemes 
         Caption         =   "&Delete..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelp_PDF 
         Caption         =   ""
         Index           =   0
         Visible         =   0   'False
      End
      Begin VB.Menu mnuHelp_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelp_About 
         Caption         =   "&About MacPac Numbering..."
      End
   End
End
Attribute VB_Name = "frmSchemes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const mpInvalidValue As Integer = 380

Private vbControls() As String
Public m_iEditMode As Integer
Public m_bFinished As Boolean
Private m_sTop As Single
Private m_sHeight As Single
Private m_sLeft As Single
Private m_sWidth As Single
Private m_iDocSchemes As Integer
Private m_bSavePrivateStyFile As Boolean

Public Property Get Scheme() As String
    On Error Resume Next
    If Me.tvwSchemes.SelectedItem.Parent.Text = mpFavoriteSchemes Then
        Scheme = Mid(Me.tvwSchemes.SelectedItem.Key, 4)
    Else
        Scheme = Mid(Me.tvwSchemes.SelectedItem.Key, 2)
    End If
End Property

Public Property Get SchemeDisplayName() As String
    On Error Resume Next
    SchemeDisplayName = Me.tvwSchemes.SelectedItem.Text
End Property

Public Property Get SchemeType() As mpSchemeTypes
    Dim iIndex As Integer
    
    On Error Resume Next
    
    iIndex = Me.tvwSchemes.SelectedItem.Parent.Index
    
    'GLOG 5349 - account for absence of Private Schemes node
    If (iIndex > 2) And Not g_bShowPersonalSchemes Then _
        iIndex = iIndex + 1
        
    If iIndex = 4 Then
        'favorites node
        If Left$(Me.tvwSchemes.SelectedItem.Key, 3) = CStr(mpFavoritePublic) Then
            SchemeType = mpSchemeType_Public
        Else
            SchemeType = mpSchemeType_Private
        End If
    Else
        SchemeType = iIndex
    End If
    
'   this is a workaround for missing public node
    If g_bIsAdmin And (SchemeType = mpSchemeType_Public) Then _
        SchemeType = mpSchemeType_Private
    On Error GoTo 0
End Property

Private Sub btnCancel_Click()
    Me.m_iEditMode = mpModeSchemeDone
    Me.Hide
    On Error Resume Next
    Unload Me
    Word.Application.Activate
    mdlApplication.SendShiftKey
End Sub

Private Sub btnChangeScheme_Click()
    Dim xScheme As String
    Dim xAlias As String
    Dim xHScheme As String
    Dim bUseWordHeadings As Boolean
    Dim styScheme As Word.Style
    Dim oScheme As CNumScheme
    Dim xWindow As String
    Dim lListParas As Long
    Dim bTrackRevisions As Boolean
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    'prompt to save document if necessary
    If (Me.SchemeType <> mpSchemeType_Document) And g_bOrganizerSavePrompt And _
            (WordBasic.FileNameFromWindow$() = "") Then
        MsgBox mpOrganizerSavePrompt, vbInformation, g_xAppName
        Exit Sub
    End If
    
'   if doc scheme, ensure that proprietary styles exist;
'   if not, prompt to reset; this will be necessary after
'   copying numbered paras using Word Heading styles to new doc
'   in which scheme is not loaded but Headings 1-9 are "in use"
    If (Me.SchemeType = mpSchemeType_Document) And _
            (Me.Scheme <> "HeadingStyles") Then
        On Error Resume Next
        Set styScheme = ActiveDocument _
            .Styles(xGetStyleRoot(Me.Scheme) & "_L1")
        On Error GoTo ProcError
        
        If styScheme Is Nothing Then
            MsgBox "The " & Me.SchemeDisplayName & " document scheme is " & _
                "missing some of its components.  It may have been " & _
                "partially copied from another document.  Please " & _
                "click the Reset button to restore this scheme to full " & _
                "functionality.", vbCritical, g_xAppName
            Exit Sub
        End If
    End If

    'message user if doc isn't in Word 2010 compatibility mode and scheme requires it
    If (g_iWordVersion >= mpWordVersion_2010) And _
            (Me.SchemeType <> mpSchemeType_Document) Then
        If mdlWord14.GetCompatibilityMode(ActiveDocument) < 14 Then
            If ContainsWord2010NumberStyle(Me.Scheme, Me.SchemeType) Then
                MsgBox "The specified scheme cannot be loaded because it contains a " & _
                    "numbering style that is only available for documents that are " & _
                    "in Word 2010 compatibility mode.", vbInformation, g_xAppName
                Exit Sub
            End If
        End If
    End If

    'GLOG 5461 (9.9.5012)
    If ActiveDocument.TrackRevisions Then
        lListParas = ActiveDocument.ListParagraphs.Count
        If lListParas >= g_lTrackChangesWarningThreshold Then
            lRet = MsgBox("This document contains " & CStr(lListParas) & " numbered " & _
                "paragraphs.  In a document this size, we recommend turning off track " & _
                "changes during the Change To process to avoid a potentially lengthy wait." & vbCr & vbCr & _
                "Would you like us to turn off track changes before proceeding and then turn it " & _
                "back on at the end of the process?", vbYesNoCancel + vbQuestion, AppName)
            If lRet = vbYes Then
                bTrackRevisions = True
                ActiveDocument.TrackRevisions = False
            ElseIf lRet = vbCancel Then
                Exit Sub
            End If
        End If
    End If
    
    Me.m_iEditMode = mpModeSchemeDone
    bUseWordHeadings = Me.mnuScheme_UseWordHeadings.Checked
    Me.Hide
    DoEvents
    
'   store active window - prevents error 4601 below (see GLOG #3556)
    If bUseWordHeadings And (g_iWordVersion = mpWordVersion_2003) Then _
        xWindow = WordBasic.WindowName$()
    
'   change scheme
    Set oScheme = GetRecord(Me.Scheme, Me.SchemeType)
    iChangeScheme Me.Scheme, _
                  Me.SchemeType, _
                  xAlias, _
                  Me.SchemeType <> mpSchemeType_Document, _
                  , , , _
                   bUseWordHeadings, _
                   oScheme.DynamicFonts
    
    'GLOG 5461
    If bTrackRevisions Then _
        ActiveDocument.TrackRevisions = True
        
'   reactivate if necessary
    If xWindow <> "" Then _
        WordBasic.Activate xWindow
        
    Unload Me
    Word.Application.Activate
    mdlApplication.SendShiftKey
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Exit Sub
    
ProcError:
    'GLOG 5461
    If bTrackRevisions Then _
        ActiveDocument.TrackRevisions = True
    RaiseError "frmSchemes.btnChangeScheme_Click"
    Word.Application.Activate
    mdlApplication.SendShiftKey
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Exit Sub
End Sub

Private Sub btnReload_Click()
    If g_xPSchemes(0, 0) <> "" Then
        lRet = MsgBox("Are you sure you want to replace all schemes " & _
            "in your Admin directory with the current public schemes?  " & String(2, 13) & _
            "If you want to save the set of schemes on which you are currently working, " & _
            "you should instead click No, close the dialog box, and specify a new Admin directory " & _
            "in mpn90.ini.  You can later return to this set by pointing back to the old directory.", _
            vbQuestion + vbYesNo, g_xAppName)
        If lRet = vbNo Then _
            Exit Sub
    End If
    
    LoadPublicSchemesAsAdmin
    RefreshSchemesList Me.tvwSchemes
    tvwSchemes_NodeClick Me.tvwSchemes.Nodes(2)
    Me.tvwSchemes.SelectedItem.Expanded = True
End Sub

Private Sub btnReset_Click()
    Dim xScheme As String
    Dim styScheme As Word.Style
    Dim iOrigin As mpSchemeTypes
    Dim bOriginFound As Boolean
    Dim tmpSource As Word.Template
    Dim bIsHeading As Boolean
    Dim xMsg As String
    Dim ltSource As Word.ListTemplate
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    Me.m_iEditMode = mpModeSchemeDone
    
    'prompt to save document if necessary
    If g_bOrganizerSavePrompt And (WordBasic.FileNameFromWindow$() = "") Then
        MsgBox mpOrganizerSavePrompt, vbInformation, g_xAppName
        Exit Sub
    End If
    
    xScheme = Me.Scheme
    iOrigin = iGetSchemeOrigin(xScheme)
    Set tmpSource = Source(iOrigin)
    
'   check for scheme in origin mpNumbers.sty;
'   user may have deleted it or it may have
'   come from another user's mpNumbers.sty
    On Error Resume Next
    If (iOrigin = mpSchemeType_Private) And _
            (g_xPSchemes(0, 0) <> "") Then
        bOriginFound = bIsMPListTemplate( _
                tmpSource.ListTemplates(xScheme))
    End If
    
'   if not found in personal or if firm is the origin,
'   check for scheme in firm
    If Not bOriginFound Then
        iOrigin = mpSchemeType_Public
        If g_xFSchemes(0, 0) <> "" Then
            bOriginFound = bIsMPListTemplate(g_oFNumSty.ListTemplates(xScheme))
        End If
    End If
    
'   if still not found, alert user and exit
    If Not bOriginFound Then
        MsgBox "Could not reset selected scheme.  No " & _
            "private or public scheme with this name exists.", _
            vbInformation, g_xAppName
        Exit Sub
    Else
'       give user a chance to bail out
        If iOrigin = mpSchemeType_Private Then
            xMsg = "private"
        Else
            xMsg = "public"
        End If
        xMsg = "Are you sure you want to reset the " & _
            Me.SchemeDisplayName & " scheme to its " & xMsg & " defaults?"
        lRet = MsgBox(xMsg, vbYesNo + vbQuestion, g_xAppName)
        If lRet = vbNo Then _
            Exit Sub
    End If
    
    On Error GoTo 0
    Me.Hide
    
    With Application
        .ScreenRefresh
        .ScreenUpdating = False
    End With
    
'   check for scheme existence
    Dim bIsMPScheme As Boolean
    bIsMPScheme = IsMacPacScheme(xScheme, , ActiveDocument)
    
'   reload if scheme exists, else warn user
    If bIsMPScheme Then
        bIsHeading = bIsHeadingScheme(xScheme)
        bRet = iLoadScheme(ActiveDocument, xScheme, iOrigin)
        
'       relinking again is necessary, since copying styles
'       occasionally breaks links - need to relink even if
'       scheme appears to be linked; otherwise, schemes become
'       unlinked when copied into a new document
'        If bSchemeIsUnlinked(xScheme) Then
            bRet = bRelinkScheme(xScheme, _
                mpSchemeType_Document, False, False, False)
'        End If

        If bIsHeading Then
'           convert back to Word Heading Styles
            bRet = bConvertToHeadingStyles(xScheme, False)
        End If

'       ensure that left indent of numbered and para styles matches
        UpdateParaStyles xScheme

'       redo trailing characters
        ApplyModifiedScheme xScheme
        
'       backup scheme properties in case the user
'       writes over them using the Word UI
        BackupProps xScheme
    
'       adjust toolbar for added/deleted levels
        bSetNumberingBtnsEnable xScheme
    Else
        xMsg = "The " & xScheme & " scheme is not currently loaded. " & _
               "Click the ""Use"" button " & _
               "to insert numbers using this scheme."
        MsgBox xMsg, vbExclamation, g_xAppName
    End If
    Unload Me
    Word.Application.Activate
    mdlApplication.SendShiftKey
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Exit Sub
    
ProcError:
    Word.Application.Activate
    mdlApplication.SendShiftKey
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    RaiseError "frmSchemes.btnReset_Click"
    Exit Sub
End Sub

Private Sub btnUse_Click()
    Dim xHScheme As String
    Dim bUseWordHeadings As Boolean
    Dim ltP As Word.ListTemplate
    Dim xNewName As String
    Dim xAlias As String
    Dim xNewAlias As String
    Dim iLevels As Integer
    Dim i As Integer
    Dim xStyleRoot As String
    Dim bIsNew As Boolean
    
    Me.m_iEditMode = mpModeSchemeDone
    xNewAlias = Me.SchemeDisplayName
    xStyleRoot = xGetStyleRoot(Me.Scheme)
    
    If Me.SchemeType = mpSchemeType_Private Or _
            Me.SchemeType = mpSchemeType_Public Then
        If g_bOrganizerSavePrompt And (WordBasic.FileNameFromWindow$() = "") Then
            'prompt to save document
            MsgBox mpOrganizerSavePrompt, vbInformation, g_xAppName
            Exit Sub
        ElseIf bListTemplateIsMissing(ActiveDocument, Me.Scheme) And _
                (Not bListTemplateExists(xStyleRoot)) Then
            If bLTExistsIgnoreCasing(Me.Scheme) Then
'               scheme with same name, different casing
'               exists in document - alert and exit
                xMsg = "There is already a scheme in this document that uses the " & _
                        xStyleRoot & " style root.  Please select this " & _
                        "scheme from the list of Document Schemes in this dialog."
                MsgBox xMsg, vbExclamation, g_xAppName
                Exit Sub
            Else
'               styles exist without corresponding list template;
'               give user choice of exiting
                xMsg = "MacPac styles " & xStyleRoot & _
                    "_L1-x already exist in this document, but the " & _
                    xStyleRoot & " document scheme can no longer be found.  If numbered " & _
                    "paragraphs have been converted to text (this may have " & _
                    "occurred when saving changes in an earlier version of Word), " & _
                    "use of this scheme could result in the same numbers being applied " & _
                    "twice per paragraph.  Would you like to reload this scheme?"
                lRet = MsgBox(xMsg, vbInformation + vbYesNo, g_xAppName)
                If lRet = vbNo Then _
                    Exit Sub

'               add notification flag
                On Error Resume Next
                ActiveDocument.Variables.Add "CorruptionNotice", Date
                On Error GoTo 0
            End If
                
        ElseIf Word.ActiveDocument.ListTemplates.Count Then
'           test for scheme in doc with same name
            On Error Resume Next
            Set ltP = Word.ActiveDocument _
                .ListTemplates(xGetFullLTName(Me.Scheme))
            On Error GoTo ProcError
            
            If Not (ltP Is Nothing) Then
'               scheme with same name exists in
'               document alert and exit
                xAlias = GetField(Me.Scheme, mpRecField_Alias, _
                    mpSchemeType_Document)
                iLevels = iGetLevels(Me.Scheme, mpSchemeType_Document)
                xMsg = "There is already a scheme in this document that uses the " & _
                        xStyleRoot & "_L1-" & iLevels & " styles.  Please select the " & _
                        xAlias & " scheme from the list of Document Schemes in this dialog."
                MsgBox xMsg, vbExclamation, g_xAppName
                Exit Sub
            End If
        End If
        
        'message user if doc isn't in Word 2010 compatibility mode and scheme requires it
        If g_iWordVersion >= mpWordVersion_2010 Then
            If mdlWord14.GetCompatibilityMode(ActiveDocument) < 14 Then
                If ContainsWord2010NumberStyle(Me.Scheme, Me.SchemeType) Then
                    MsgBox "The specified scheme cannot be loaded because it contains a " & _
                        "numbering style that is only available for documents that are " & _
                        "in Word 2010 compatibility mode.", vbInformation, g_xAppName
                    Exit Sub
                End If
            End If
        End If

'       test for existing alias
        While Not bSchemeNameIsValid(xNewAlias, _
                                     True, _
                                     False, _
                                     mpSchemeType_Document)
            i = i + 1
            xNewAlias = Me.SchemeDisplayName & i
        Wend
    End If
    
'   test if there is already a MacPac
'   scheme that uses Word Heading styles
    xHScheme = xHeadingScheme()
    
'*****************************
'REMOVED 1/28/00
'    bUseWordHeadings = Me.mnuScheme_UseWordHeadings.Checked
'    If Me.SchemeType = mpSchemeType_Document Then
'        iUserChoice = vbYes
'    ElseIf Len(xHScheme) And bUseWordHeadings Then
''       prompt for redeinition
'        xMsg = "This document currently uses MS Word " & _
'               "Heading 1-9 styles for the " & xHScheme & _
'               " scheme." & vbCr & "Do you want to load the " & _
'               xAlias & " scheme using the " & _
'               Me.Scheme & "_L1-9 styles?"
'        iUserChoice = MsgBox(xMsg, vbQuestion + vbYesNo, g_xAppName)
'        bUseWordHeadings = False
'    Else
'        iUserChoice = vbYes
'    End If
'******************************
'REPLACED WITH
    If Me.SchemeType <> mpSchemeType_Document And _
            (Len(xHScheme) = 0) Then
        bUseWordHeadings = Me.mnuScheme_UseWordHeadings.Checked
    End If
    iUserChoice = vbYes
'******************************
    If iUserChoice = vbYes Then
        Me.Hide
        Application.ScreenRefresh
        bIsNew = UseScheme(bUseWordHeadings, xNewName, xNewAlias)
        If bUseWordHeadings Then
'           document may start with non-numbered Heading 1-9 paras
            ApplyModifiedScheme Me.Scheme
        End If
        If (Not g_bIsAdmin) And bIsNew And _
                Me.mnuScheme_InsertLevel1Automatically.Checked And _
                (Selection.Range.ListParagraphs.Count = 0) Then
            'insert level one if specified,
            'but only if selection is not already numbered
            bInsertNumberFromToolbar 1
        End If
        Application.ScreenUpdating = True
        Unload Me
    End If
    Word.Application.Activate
    mdlApplication.SendShiftKey
    EchoOn
    Exit Sub
ProcError:
    Word.Application.Activate
    mdlApplication.SendShiftKey
    EchoOn
    RaiseError "frmSchemes.btnUse_Click"
End Sub

Function UseScheme(ByVal bUseWordHeadings As Boolean, _
                   Optional ByVal xNewName As String, _
                   Optional ByVal xNewAlias As String) As Boolean
'returns TRUE is scheme was loaded, FALSE if just activated
    Dim xScheme As String
    Dim xAlias As String
    Dim styScheme As Word.Style
    Dim oProps As CSchemeProps
    Dim xLT As String
    Dim bIsNew As Boolean
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    Application.ScreenUpdating = False
    
    xScheme = Me.Scheme
    xAlias = Me.SchemeDisplayName
    
'   reload styles if specified or necessary
    If xScheme <> "HeadingStyles" Then
        On Error Resume Next
        Set styScheme = ActiveDocument.Styles( _
                xGetStyleRoot(xScheme) & "_L" & 1)
        On Error GoTo 0
    
        If (styScheme Is Nothing) Or _
                (Not bListTemplateExists(xScheme)) Then
'           could not find styles or list template for
'           scheme - load scheme
            bRet = iLoadScheme(ActiveDocument, _
                               xScheme, _
                               Me.SchemeType, _
                               xNewAlias, _
                               xNewName)

'           only convert to heading styles if
'           this functionality is allowed when
'           the user 'Uses' a scheme and if the
'           menu item is checked
            If bUseWordHeadings Then
                UseWordHeadingStyles Scheme
            End If
            
            bIsNew = True
        End If
    End If
        
'    If g_bAllowFullHeadingStyleUse Then
''       ensure that scheme is correctly linked
'        bRelinkScheme xScheme, _
'                      mpSchemeType_Document, _
'                      bUseWordHeadings
'    Else
'       ensure that scheme is correctly linked -
'       use heading styles only if the scheme is
'       already the heading scheme for the doc - need to relink even if
'       scheme appears to be linked; otherwise, schemes become
'       unlinked when copied into a new document
'        If bSchemeIsUnlinked(xScheme) Then
            bRelinkScheme xScheme, _
                          mpSchemeType_Document, _
                          bIsHeadingScheme(xScheme), _
                          False, _
                          Not bIsNew
'        End If
'    End If

'   backup scheme properties in case the user
'   writes over them using the Word UI
    BackupProps xScheme
    
'   set scheme of active doc - use key so that
'   type of scheme is carried in var
    bSetSelectedScheme ActiveDocument, _
        Me.tvwSchemes.SelectedItem.Key
        
    UseScheme = bIsNew

    Exit Function
    
ProcError:
    RaiseError "frmSchemes.btnUse_Click"
    Exit Function
End Function

Private Sub Form_Activate()
    On Error Resume Next
    With Me.tvwSchemes
        .SetFocus
        tvwSchemes_NodeClick .SelectedItem
    End With
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF6 Then
        frmAboutNew.Show vbModal
    End If
End Sub
Private Sub Form_Load()
    Dim xSelScheme As String
    Dim xSelSchemeType As String
    Dim xPath As String
    Dim bUseWordHeadings As Boolean
'    Dim iDocSchemes As Integer
    Dim xInterval As String
    Dim i As Integer
    Dim oNode As MSComctlLib.Node
    
    On Error GoTo Initialize_Error
    EchoOff
    Application.ScreenUpdating = False
    Me.m_bFinished = False
    Me.m_iEditMode = mpModeSchemeDone
    LoadStyFiles
    
    Set g_oCurScheme = New CNumScheme
    
'   get whether any scheme is being used with heading styles
    On Error Resume Next
    bUseWordHeadings = xGetAppIni("Numbering", _
                                    "UseWordHeadingStyles")
    On Error GoTo Initialize_Error
    
'    ConvertWordHeadingStyles
    
    Me.mnuScheme_UseWordHeadings.Checked = bUseWordHeadings
    
'   hide menu items if specified
    If g_bSchemeSelectionOnly Then
        'dialog is being used to set active scheme only
        Me.mnuHelp.Visible = False
        Me.mnuScheme.Visible = False
        Me.mnuSharing.Visible = False
        Me.btnChangeScheme.Visible = False
        Me.mnuScheme_ChangeTo.Visible = False
        Me.btnReset.Visible = False
        Me.mnuScheme_Reset.Visible = False
        Me.btnUse.Left = Me.btnReset.Left
        Me.Caption = "Select a Numbering Scheme"
'        Me.sbStatus.Top = Me.sbStatus.Top - 200
        Me.Height = Me.Height - 200
    Else
        Me.mnuScheme_Modify.Visible = g_bAllowSchemeEdit
        Me.mnuScheme_New.Visible = g_bAllowSchemeNew
        'Me.mnuScheme_Delete.Visible = (g_bAllowSchemeNew Or _
            'g_bAllowSchemeSharing)
        Me.mnuScheme_Sep2.Visible = (g_bAllowSchemeEdit Or _
            g_bAllowSchemeNew Or g_bAllowSchemeSharing)
        Me.mnuScheme_UseWordHeadings.Visible = g_bAllowFullHeadingStyleUse
    End If
    
'   check Insert Level 1 option if specified
    If xGetUserIni("Numbering", "InsertLevel1Automatically") = "1" Then _
        Me.mnuScheme_InsertLevel1Automatically.Checked = True
    
'   if no edits allowed, remove tooltip from preview
    If (Not g_bAllowSchemeEdit) Or g_bSchemeSelectionOnly Then _
        imgPreview.ToolTipText = ""
    
'   change form for admin mode
    If g_bIsAdmin Then
'       hide/move buttons
        Me.btnChangeScheme.Visible = False
        Me.btnReset.Visible = False
        Me.mnuScheme_ChangeTo.Visible = False
        Me.mnuScheme_Reset.Visible = False
        Me.btnUse.Left = Me.btnReset.Left
        Me.btnReload.Visible = True
        
'       hide menu items
        Me.mnuScheme_Relink.Visible = False
        Me.mnuScheme_SetAsDefault.Visible = False
        Me.mnuScheme_Sep1.Visible = False
        Me.mnuScheme_Sep2.Visible = False
        Me.mnuScheme_InsertLevel1Automatically.Visible = False
        Me.mnuScheme_Favorites.Visible = False
        
'       change caption
        Me.Caption = Me.Caption & " - Administrative Utility"
    End If
    
'   disable Sharing completely
    Me.mnuSharing.Visible = (g_bAllowSchemeSharing And (Not g_bSchemeSelectionOnly))
    
'   get document schemes
    m_iDocSchemes = iGetSchemes(g_xDSchemes(), mpSchemeType_Document)
    
    RefreshSchemesList Me.tvwSchemes, , , g_bShowPersonalSchemes
    
'   initialize to current scheme
    With Me.tvwSchemes
        xSelScheme = xActiveScheme(ActiveDocument)
        If xSelScheme = Empty Then
            If m_iDocSchemes Then
'               perhaps user copied numbered paras from another doc
                xSelScheme = g_xDSchemes(0, 0)
                xPath = mpSchemeType_Document
            Else
                xSelScheme = xGetUserIni("Numbering", _
                                             "DefaultScheme")
                xPath = xGetUserIni("Numbering", _
                                        "DefaultSchemeType")
            End If
        Else
            xPath = mpSchemeType_Document
        End If
        
        If m_iDocSchemes Then
'           expand document schemes node
            Me.tvwSchemes.Nodes(mpSchemeType_Category & _
                mpDocumentSchemes).Expanded = True
        Else
'           disable change button and relink menu item
'            Me.mnuScheme_Relink.Enabled = False
            Me.btnChangeScheme.Enabled = False
            Me.mnuScheme_ChangeTo.Enabled = False
        End If
        
'       if default is a favorite, select node in favorites folder
        If xPath <> CStr(mpSchemeType_Document) Then
            On Error Resume Next
            If xPath = CStr(mpSchemeType_Private) Then
                Set oNode = .Nodes(CStr(mpFavoritePrivate) & xSelScheme)
                If Not oNode Is Nothing Then _
                    xPath = CStr(mpFavoritePrivate)
            Else
                Set oNode = .Nodes(CStr(mpFavoritePublic) & xSelScheme)
                If Not oNode Is Nothing Then _
                    xPath = CStr(mpFavoritePublic)
            End If
            On Error GoTo Initialize_Error
        End If
        
'       select node
        On Error Resume Next
        If Not g_bIsAdmin Then _
            .Nodes(xPath & xSelScheme).Selected = True
        
'       if no selection occurred, select first public scheme
        If (.SelectedItem Is Nothing) Then
            If g_bIsAdmin Then
                .Nodes(mpSchemeType_Category & mpPersonalSchemes) _
                    .Selected = True
            Else
                .Nodes(mpSchemeType_Public & g_xFSchemes(0, 0)) _
                    .Selected = True
            End If
        End If
    End With
    
    'populate help menu
    Dim xCaption As String
    Dim lPos As Long
    For i = 0 To UBound(g_xHelpFiles)
        Load mnuHelp_PDF(i)
        
        'GLOG 5246 (9.9.5004) - strip everything after underscore
        xCaption = g_xHelpFiles(i)
        lPos = InStr(xCaption, "_")
        If lPos > 0 Then _
            xCaption = Left$(xCaption, lPos - 1)
        mnuHelp_PDF(i).Caption = xCaption
        
        mnuHelp_PDF(i).Visible = True
    Next i
    
    Me.txtDescription.Text = "Article scheme uses Article_L1-9 styles." & vbCrLf & _
        "1Abcd " & LCase$("EFGHI JKLMN OPQRS TUVWX ") & "2Abcd " & LCase$("EFGHI JKLMN OPQRS TUVWX ") & "3Abcd " & LCase$("EFGHI JKLMN OPQRS TUVWX ") & "4Abcd " & LCase$("EFGHI JKLMN OPQRS TUVWX ") & "5Abcd " & LCase$("EFGHI JKLMN OPQRS TUVWX ")
    
    EchoOn
    Application.ScreenUpdating = True
    Exit Sub
    
Initialize_Error:
    EchoOn
    Application.ScreenUpdating = True
    Select Case Err.Number
        Case mpInvalidValue
            Me.tvwSchemes.Nodes(2).Selected = True
        Case Else
            MsgBox Error(Err.Number)
    End Select
    Exit Sub
End Sub
Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    Set frmSchemes = Nothing
End Sub

Private Sub imgPreview_DblClick()
    If g_bAllowSchemeEdit And (Not g_bSchemeSelectionOnly) Then
        If Me.SchemeType = mpSchemeType_Document Then
'           doc scheme - edit scheme available unless turned off in 97
'            If Not g_bNoSchemeIndents Then
            mnuScheme_Modify_Click
        ElseIf Me.SchemeType = mpSchemeType_Private Then
'           private scheme - edit scheme always available
            mnuScheme_Modify_Click
        End If
    End If
End Sub

Private Sub mnuHelp_About_Click()
    frmAboutNew.Show vbModal
End Sub

Private Sub mnuHelp_PDF_Click(iIndex As Integer)
    LaunchDocumentByExtension g_xHelpPath & "\" & g_xHelpFiles(iIndex) & ".pdf"
End Sub

Private Sub mnuScheme_ChangeTo_Click()
    btnChangeScheme_Click
End Sub

Private Sub mnuScheme_ConvertToHeadingStyles_Click()
'change selected document scheme to use Heading Styles-
'change all paragraphs marked with MacPac styles of scheme
'to Heading styles
    On Error GoTo ProcError

'   ensure styles exist
    If Not bEnsureSchemeStyles(Me.Scheme) Then _
        Exit Sub
    
    Me.Hide
    Me.MousePointer = vbHourglass
    EchoOff
    
'   do conversion
    bRet = bConvertToHeadingStyles(Me.Scheme)
    
    If bRet Then
'       change menu items
        Me.mnuScheme_ConvertToHeadingStyles.Visible = False
        Me.mnuScheme_ConvertToMacPacStyles.Visible = True
        Me.mnuScheme_ConvertToMacPacStyles.Enabled = True
    End If
    
    Unload Me
    
    EchoOn
    With Word.Application
        .ScreenUpdating = True
        .ScreenRefresh
    End With
    Me.MousePointer = vbDefault
    Exit Sub
    
ProcError:
    EchoOn
    RaiseError "frmSchemes.mnuScheme_ConvertToHeadingStyles_Click"
End Sub

Private Sub mnuScheme_ConvertToMacPacStyles_Click()
'change selected document scheme to use MacPac Styles-
'change all paragraphs marked with Heading styles of schem
'to MacPac styles
    On Error GoTo ProcError

    Me.Hide
    Me.MousePointer = vbHourglass
    Application.ScreenUpdating = True
    
'   do conversion
    bRet = bConvertToMacPacStyles(Me.Scheme)
    
    If bRet Then
'       change menu items
        Me.mnuScheme_ConvertToHeadingStyles.Visible = True
        Me.mnuScheme_ConvertToHeadingStyles.Enabled = True
        Me.mnuScheme_ConvertToMacPacStyles.Visible = False
    End If
    
    Unload Me
    
'    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Me.MousePointer = vbDefault
    Exit Sub
    
ProcError:
    EchoOn
    RaiseError "frmSchemes.mnuScheme_ConvertToHeadingStyles_Click"
End Sub

Private Sub mnuScheme_Delete_Click()
    Dim xDefaultScheme As String
    Dim xMsg As String
    Dim xScheme As String
    Dim xAlias As String
    Dim iSchemeType As mpSchemeTypes
    Dim xSelScheme As String
    Dim lNumSchemes As Long
    Dim bIsActive As Boolean
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    xScheme = Me.Scheme
    xAlias = Me.SchemeDisplayName
    iSchemeType = Me.SchemeType
    bIsActive = (xActiveScheme(ActiveDocument) = xScheme)
    xDefaultScheme = xGetUserIni("Numbering", "DefaultScheme")
    
'   make sure user really wants to delete scheme
    xMsg = "Delete " & xAlias & " scheme?"
    If (iSchemeType = mpSchemeType_Private) And _
            (xScheme = xDefaultScheme) Then
        xMsg = xMsg & "  This is currently your default scheme."
    ElseIf iSchemeType = mpSchemeType_Document Then
        If bSchemeIsApplied(xScheme) Then
            xMsg = xAlias & " scheme is currently applied to text in " & _
                "this document.  If you delete the scheme, numbering " & _
                "will be removed from any paragraphs to which this " & _
                "scheme was previously applied"
            If Not bIsHeadingScheme(xScheme) Then
                xMsg = xMsg & ", and the paragraphs will be reformatted " & _
                    "to Normal style"
            End If
            xMsg = xMsg & ".  Delete " & xAlias & " scheme?"
        End If
    End If
    
    lRet = MsgBox(xMsg, vbYesNo + vbQuestion, g_xAppName)
    If lRet = vbNo Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass

'    Me.sbStatus.SimpleText = _
'        "Deleting " & xAlias & " scheme.  Please wait ..."
        
    bRet = bDeleteScheme(xScheme, xAlias, iSchemeType)
    lNumSchemes = UpdateSchemeArray(iSchemeType)
    If iSchemeType = mpSchemeType_Document Then _
        m_iDocSchemes = lNumSchemes
    If bRet Then
        With Me.tvwSchemes
            .Nodes.Remove .SelectedItem.Index
            
'            If iSchemeType = mpSchemeType_Document Then
'                If lNumSchemes Then
'                    If xActiveScheme(ActiveDocument) = "" Then
''                       deleted scheme was active;
''                       switch to another
'                        xSelScheme = g_xDSchemes(0, 0)
'                        bSetSelectedScheme ActiveDocument, _
'                                            xSelScheme
'                    End If
'
''                   select active scheme
'                    .Nodes(mpSchemeType_Category & _
'                        mpDocumentSchemes).Expanded = True
'                    .Nodes(iSchemeType & xSelScheme).Selected = True
'                Else
'                    xSelScheme = ""
'                End If
'
''               reset toolbar
'                bSetNumberingBtnsEnable xSelScheme
'            End If
            
'           delete favorite
            Dim oNode As MSComctlLib.Node
            On Error Resume Next
            Set oNode = .Nodes(CStr(mpFavoritePrivate) & xScheme)
            On Error GoTo ProcError
            If Not oNode Is Nothing Then
                .Nodes.Remove CStr(mpFavoritePrivate) & xScheme
                DeleteFavoriteScheme xScheme
            End If
            
'           refresh preview bitmap
            tvwSchemes_NodeClick .SelectedItem
        End With
    End If
    
'    sbStatus.SimpleText = "Ready"
    Screen.MousePointer = vbDefault
    
    'GLOG 5539 (9.9.6006) - sty file needs to be saved with form hidden - it
    'needs to be done outside of the form to avoid leaving the active control,
    'in this case the right-click menu, in suspended animation
    If iSchemeType = mpSchemeType_Private Then
        m_bSavePrivateStyFile = True
        Me.Hide
    End If
    
    Exit Sub
    
ProcError:
'    sbStatus.SimpleText = "Ready"
    Screen.MousePointer = vbDefault
    RaiseError "frmSchemes.btnDelete_Click"
    Exit Sub
End Sub

Private Sub mnuScheme_Favorites_Click()
    Dim xFavorite As String
    Dim vProps As Variant
    Dim i As Integer
    Dim xType As String
    Dim bFound As Boolean
    
    On Error GoTo ProcError
            
    If Me.tvwSchemes.SelectedItem.Parent.Text <> mpFavoriteSchemes Then
        'add if not already a favorite
        i = 1
        xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
        While (xFavorite <> "") And Not bFound
            vProps = Split(xFavorite, "|")
            bFound = (vProps(0) = Me.Scheme)
            If Not bFound Then
                i = i + 1
                xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
            End If
        Wend
        
        If Not bFound Then
            'get type
            If Me.SchemeType = mpSchemeType_Public Then
                xType = CStr(mpFavoritePublic)
            Else
                xType = CStr(mpFavoritePrivate)
            End If
    
            'add to user ini
            bSetUserIni "Numbering", "FavoriteScheme" & CStr(i), Me.Scheme & "|" & _
                Me.SchemeDisplayName & "|" & xType
                
            'add to tree
            Me.tvwSchemes.Nodes.Add Me.tvwSchemes.Nodes(mpSchemeType_Category & _
                mpFavoriteSchemes), tvwChild, xType & Me.Scheme, Me.SchemeDisplayName
        End If
    Else
        'delete key from ini
        DeleteFavoriteScheme Me.Scheme
        
        'remove node from tree
        Me.tvwSchemes.Nodes.Remove Me.tvwSchemes.SelectedItem.Key
    End If
    
    Exit Sub
ProcError:
    RaiseError "frmSchemes.mnuScheme_Favorites_Click"
    Exit Sub
End Sub

Private Sub mnuScheme_InsertLevel1Automatically_Click()
    On Error GoTo ProcError
    With Me.mnuScheme_InsertLevel1Automatically
        .Checked = Not .Checked
        bSetUserIni "Numbering", "InsertLevel1Automatically", Abs(.Checked)
    End With
    Exit Sub
ProcError:
    RaiseError "frmSchemes.mnuScheme_InsertLevel1Automatically_Click"
    Exit Sub
End Sub

Private Sub mnuScheme_Modify_Click()
'   ensure styles exist
    If Me.SchemeType = mpSchemeType_Document Then
        If Not bEnsureSchemeStyles(Me.Scheme) Then
            Exit Sub
        End If
    End If
                
    m_iEditMode = mpModeSchemeEdit
    Me.Hide
End Sub

Private Sub mnuScheme_New_Click()
    Dim xMsg As String

'   validate
    If (g_oCurScheme.SchemeType = mpSchemeType_Category) And _
           (Me.tvwSchemes.Nodes(2).Children = 0) Then
        If g_bIsAdmin Then
            xMsg = "There are no available public schemes " & _
                "on which to base a new scheme.  Start with " & _
                "a document scheme or click the Load " & _
                "Public Schemes button."
        Else
            xMsg = "There are no available public schemes " & _
                "on which to base a new scheme.  Start with " & _
                "a document or private scheme selected.  If " & _
                "no schemes of any type are available, please " & _
                "contact your system administrator."
        End If
        MsgBox xMsg, vbInformation, g_xAppName
        Exit Sub
    ElseIf Me.SchemeType = mpSchemeType_Document Then
'       ensure styles exist
        If Not bEnsureSchemeStyles(Me.Scheme) Then
            Exit Sub
        End If
    End If
    
    m_iEditMode = mpModeSchemeNew
    Me.Hide
End Sub
Private Sub mnuScheme_Properties_Click()
    Dim xFolder As String
    Dim oNode As MSComctlLib.Node
    Dim i As Integer
    Dim xFavorite As String
    Dim vProps As Variant
    Dim bFound As Boolean
    Dim xDescription As String '9.9.5001
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    'store existing description
    xDescription = g_oCurScheme.Description
    
    xFolder = Me.tvwSchemes.SelectedItem.Parent.Text
    EditSchemeProperties g_oCurScheme.Name, g_oCurScheme.SchemeType, False
    Me.tvwSchemes.SelectedItem.Text = g_oCurScheme.Alias
    
    'update favorite
    If xFolder = mpPersonalSchemes Then
        On Error Resume Next
        Set oNode = Me.tvwSchemes.Nodes(CStr(mpFavoritePrivate) & g_oCurScheme.Name)
        On Error GoTo ProcError
    ElseIf (xFolder = mpFavoriteSchemes) And _
            (g_oCurScheme.SchemeType = mpSchemeType_Private) Then
        On Error Resume Next
        Set oNode = Me.tvwSchemes.Nodes("3" & g_oCurScheme.Name)
        On Error GoTo ProcError
    End If
    
    If Not oNode Is Nothing Then
        oNode.Text = g_oCurScheme.Alias
        
        'update ini key
        i = 1
        xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
        While (xFavorite <> "") And Not bFound
            vProps = Split(xFavorite, "|")
            bFound = (vProps(0) = g_oCurScheme.Name)
            If Not bFound Then
                i = i + 1
                xFavorite = xGetUserIni("Numbering", "FavoriteScheme" & CStr(i))
            End If
        Wend
        If bFound Then
            bSetUserIni "Numbering", "FavoriteScheme" & CStr(i), _
                g_oCurScheme.Name & "|" & g_oCurScheme.Alias & "|" & _
                CStr(mpFavoritePrivate)
        End If
    End If
    
    'update description in dialog if necessary
    If g_oCurScheme.Description <> xDescription Then _
        DisplayAssociatedStyles

    'GLOG 5539 (9.9.6006) - sty file needs to be saved with form hidden - it
    'needs to be done outside of the form to avoid leaving the active control,
    'in this case the right-click menu, in suspended animation
    If g_oCurScheme.SchemeType = mpSchemeType_Private Then
        m_bSavePrivateStyFile = True
        Me.Hide
    End If
    
    Exit Sub
ProcError:
    RaiseError "numFunctions.EditSchemeProperties"
    Exit Sub
End Sub

Private Sub mnuScheme_Reset_Click()
    btnReset_Click
End Sub

Private Sub mnuScheme_SetAsDefault_Click()
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    bSetUserIni "Numbering", "DefaultScheme", Me.Scheme
    bSetUserIni "Numbering", "DefaultSchemeType", Me.SchemeType
'    sbStatus.SimpleText = "Default numbering scheme set to " & Me.SchemeDisplayName
    Exit Sub
ProcError:
    RaiseError "frmSchemes.mnuScheme_SetAsDefault_Click"
    Exit Sub
End Sub

Private Sub mnuScheme_Relink_Click()
'relink schemes in specified source file
    Dim iFound As Integer
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    Me.MousePointer = vbHourglass
    Application.ScreenUpdating = False
    Select Case Me.mnuScheme_Relink.Caption
        Case "Re&link Document Schemes"
'            sbStatus.SimpleText = _
'                "Relinking Document Schemes.  Please wait..."
            bRet = bRelinkSchemes(mpSchemeType_Document, True)
            iFound = iRenameLTsFromVars()
            If iFound Then
                m_iDocSchemes = UpdateSchemeArray(mpSchemeType_Document)
                RefreshSchemesList Me.tvwSchemes, Me.Scheme, Me.SchemeType, _
                    g_bShowPersonalSchemes
            End If
            If bRet Then
                If m_iDocSchemes Then
                    xMsg = "The schemes in the current " & _
                       "document have been relinked."
                Else
                    xMsg = "There are no schemes in the current document."
                End If
            Else
                '11/2/12
                xMsg = "The schemes in the current document could not be relinked."
            End If
        Case "Re&link Private Schemes"
'            sbStatus.SimpleText = _
'                "Relinking Private Schemes.  Please wait..."
            bRelinkSchemes mpSchemeType_Private
            xMsg = "Your Private Schemes have been relinked."
'           save personal numbers sty
            With Templates(g_xPNumSty)
                .Saved = False
                If g_iWordVersion >= mpWordVersion_2010 Then
                    'workaround for Word 2010 to avoid error 5986 -
                    '"this command is not available in an unsaved document"
                    .OpenAsDocument.Save
                    ActiveDocument.Close False
                Else
                    .Save
                End If
            End With
        Case "Re&link Public Schemes"
'            sbStatus.SimpleText = _
'                "Relinking Public Schemes.  Please wait..."
            bRelinkSchemes mpSchemeType_Public
            xMsg = "The schemes in the current " & _
                   "document have been relinked."
    End Select
    Application.ScreenUpdating = True
'    sbStatus.SimpleText = xMsg
    Me.MousePointer = vbDefault
    MsgBox xMsg, vbInformation, g_xAppName
    
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
'    sbStatus.SimpleText = "Ready"
    Me.MousePointer = vbDefault
    RaiseError "frmSchemes.mnuUtility_Relink_Click"
    Exit Sub
End Sub

Private Sub mnuScheme_Use_Click()
    btnUse_Click
End Sub

Private Sub mnuScheme_UseWordHeadings_Click()
    Dim xHScheme As String
    With Me.mnuScheme_UseWordHeadings
        .Checked = Not .Checked
'        bSetUserIni "Numbering", _
'                             "UseHeadingStyles", _
'                             .Checked
'        On Error Resume Next
'        xHScheme = xHeadingScheme()
'        On Error GoTo 0
'        Me.btnUse.Enabled = Not (Len(xHScheme) > 0 And .Checked)
    End With
End Sub

Private Sub ShowPublicSchemes(ByVal bImport As Boolean)
    Dim oShare As CShare
    Dim xFile As String
    Dim xDisplayName As String
    Dim udtDocEnv As mpDocEnvironment
    Dim udtAppEnv As mpAppEnvironment
    Dim bCancel As Boolean
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
        
    'GLOG 5350 - hide dialog during import to prevent
    'error #4198 (command failed) when attempting to
    'close the mpn file - this became
    'an issue with FileSite/Desksite 8.5 SP3
    If bImport Then _
        Me.Hide
    
'   get/set environment
    udtDocEnv = envGetDocEnvironment()
    udtAppEnv = envGetAppEnvironment()
    
    Err.Clear
    On Error Resume Next
    
    Load frmPublicSchemes
    If Err.Number = 0 Then
        With frmPublicSchemes
        
    '       set up caption
            If bImport Then
                .Caption = " Import Shared Schemes"
                .btnImport.Default = True
            Else
                .Caption = " Delete Shared Schemes"
                .btnDelete.Default = True
            End If
            
    '       set up btns
            .btnDelete.Visible = Not bImport
            .btnImport.Visible = bImport
            .Show vbModal
            bCancel = .Cancelled
            Unload frmPublicSchemes
        End With
    End If
    
'   reset environment
    bSetAppEnvironment udtAppEnv
    bSetDocEnvironment udtDocEnv
    
'   error is generated if for some
'   reason the form is released -
'   this happens eg when there are
'   no scheme categories
'GLOG 5350 - remmed to ensure that dialog is redisplayed -
'an error is indeed occurring when setting ShowBookmarks,
'but that appears to be without consequence
'    If Err.Number Then
'        Exit Sub
'    End If
    
    If bImport Then
'        sbStatus.SimpleText = "Imported " & g_oCurScheme.Alias & " scheme"
        RefreshSchemesList g_dlgScheme.tvwSchemes, _
                           g_oCurScheme.Name, _
                           mpSchemeType_Private
        'GLOG 5350 - redisplay dialog
        Me.Show vbModal
    Else
        Me.SetFocus
    End If
    
    Screen.MousePointer = vbDefault
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Set oShare = Nothing
    Exit Sub
ProcError:
'   reset environment
    bSetAppEnvironment udtAppEnv
    bSetDocEnvironment udtDocEnv
    
    Set oShare = Nothing
    RaiseError "frmSchemes.mnuSharing_ImportPublicScheme_Click"
    Exit Sub
End Sub

Private Sub mnuSharing_DeletePublicSchemes_Click()
    ShowPublicSchemes False
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
End Sub

Private Sub tvwSchemes_AfterLabelEdit(Cancel As Integer, NewString As String)
    Dim xScheme As String
    Dim lRet As Long
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #Else
        On Error GoTo 0
    #End If
    
    If bSchemeNameIsValid(NewString, True, True) Then
        xScheme = Mid(Me.tvwSchemes.SelectedItem.Key, 2)
        lRet = lRenameScheme(xScheme, _
                             NewString, _
                             Me.tvwSchemes.SelectedItem.Text)
        Me.btnCancel.Cancel = True
        If lRet Then
            Err.Raise lRet
        End If
    Else
        Cancel = True
    End If
    
    Exit Sub
ProcError:
    Cancel = True
    RaiseError "frmSchemes.tvwScheme_AfterLabelEdit"
    Exit Sub
End Sub

Private Sub tvwSchemes_BeforeLabelEdit(Cancel As Integer)
'allow only private schemes to be renamed
    Cancel = Me.SchemeType <> mpSchemeType_Private
    Me.btnCancel.Cancel = Cancel
End Sub

Private Sub tvwSchemes_Click()
'    sbStatus.SimpleText = "Ready"
End Sub

Private Sub tvwSchemes_Collapse(ByVal Node As MSComctlLib.Node)
'   enable/disable form controls/menus
'   based on selected item in tree
    SetControlsEnable
End Sub

Private Sub tvwSchemes_DblClick()
'   double-click is equivalent to
'   selecting scheme and clicking 'Use'
    If Me.SchemeType > mpSchemeType_Category Then
        btnUse_Click
    End If
End Sub

Private Sub tvwSchemes_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Me.tvwSchemes.StartLabelEdit
    End If
End Sub

Private Sub tvwSchemes_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If (Button = vbRightButton) And (Not g_bSchemeSelectionOnly) And _
            (Me.SchemeType <> mpSchemeType_Category) Then
        If Shift = vbCtrlMask Then
            Me.PopupMenu Me.mnuSharing
        Else
            Me.PopupMenu Me.mnuScheme
        End If
    End If
End Sub

Private Sub tvwSchemes_NodeClick(ByVal Node As MSComctlLib.Node)
'change preview
    Dim xPreviewFilePath As String
    Dim iSchemeType As mpSchemeTypes
    Dim oPreview As CPreview
    Dim xFile As String
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
        
'    bAppPathReInitialize
    
    On Error Resume Next
    iSchemeType = Me.SchemeType
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If

   If iSchemeType = mpSchemeType_Category Then
        With Me.tvwSchemes.SelectedItem
            .Expanded = Not .Expanded
        End With
    End If
    
    If g_bAllowSchemeEdit And (Not g_bSchemeSelectionOnly) And _
            (iSchemeType = mpSchemeType_Private Or _
            iSchemeType = mpSchemeType_Document) Then
        Me.imgPreview.ToolTipText = _
            "Double-click this preview area to edit the selected scheme."
    Else
        Me.imgPreview.ToolTipText = ""
    End If
    
'   enable/disable form controls/menus
'   based on selected item in tree
    SetControlsEnable
    
    Set oPreview = New CPreview
    Me.Image1.Visible = (iSchemeType <> mpSchemeType_Category)
    If (Not g_bSchemeSelectionOnly) Or (iSchemeType > mpSchemeType_Document) Then
        xFile = oPreview.GetFile(Me.SchemeDisplayName, iSchemeType)
    Else
        xFile = mpBase.GetAppPath & "\Blank.mpb"
    End If
    LoadBitmap xFile, iSchemeType
    
    Me.vsbPreview.Value = 0
    
    If Me.SchemeType = mpSchemeType_Private Then
'       if private scheme is unlinked...
        If Not bIsLinked(Me.Scheme) Then
'           relink scheme - we'll save on form unload
            bRelinkScheme Me.Scheme, _
                          mpSchemeType_Private, _
                          False
        End If
    End If
    
    If Me.SchemeType <> mpSchemeType_Category Then
        Set g_oCurScheme = GetRecord(Me.Scheme, Me.SchemeType)
    End If
    
'   update status bar
    DisplayAssociatedStyles
    
    Set oPreview = Nothing
    Exit Sub
ProcError:
    RaiseError "frmSchemes.tvwSchemes_NodeClick"
    Exit Sub
End Sub

Sub LoadBitmap(ByVal xFile As String, ByVal iSchemeType As mpSchemeTypes)
    With Me.imgPreview
        .Picture = Nothing
        If iSchemeType = mpSchemeType_Category Then
            .Top = 0
        Else
            .Top = 375
        End If
        On Error GoTo ProcError:
        .Picture = LoadPicture(xFile)
        .Refresh
    End With
    EchoOn
    
    Exit Sub
ProcError:
    xFile = mpBase.GetAppPath & "\NoPreview.mpb"
    Resume
End Sub

Private Sub SetControlsEnable()
'enable/disable buttons/menus
    Dim bIsDScheme As Boolean
    Dim bIsPScheme As Boolean
    Dim bIsFScheme As Boolean
    Dim bIsCategory As Boolean
    Dim bIsHScheme As Boolean
    Dim styHeading As Word.Style
    Dim bNoEnglishHeadings As Boolean
    Dim bIsFavorite As Boolean
    
'   Heading 1-9 may be missing on foreign system
    On Error Resume Next
    Set styHeading = ActiveDocument.Styles(wdStyleHeading1)
    If styHeading Is Nothing Then _
        bNoEnglishHeadings = True
    On Error GoTo 0
    
'   set controls
    With Me
        bIsCategory = (.SchemeType = mpSchemeType_Category)
        bIsPScheme = (.SchemeType = mpSchemeType_Private)
        bIsFScheme = (.SchemeType = mpSchemeType_Public)
        bIsDScheme = (.SchemeType = mpSchemeType_Document)
        bIsHScheme = bIsHeadingScheme(.Scheme)
        If Not bIsCategory Then _
            bIsFavorite = (.tvwSchemes.SelectedItem.Parent.Text = mpFavoriteSchemes)
        If bIsFavorite Then
            .mnuScheme_Favorites.Caption = "Remove from &Favorites"
        Else
            .mnuScheme_Favorites.Caption = "Add to &Favorites"
        End If
    
'       if no admin schemes loaded, disable all menu items
        If Dir(g_xPNumSty) = "" Then
            .mnuScheme_ConvertToHeadingStyles.Enabled = False
            .mnuScheme_ConvertToMacPacStyles.Enabled = False
            .mnuScheme_Delete.Enabled = False
            .mnuScheme_Modify.Enabled = False
            .mnuScheme_New.Enabled = False
            .mnuScheme_Properties.Enabled = False
            .mnuScheme_Relink.Enabled = False
            .mnuScheme_SetAsDefault.Enabled = False
            .mnuScheme_Favorites.Enabled = False
            .mnuSharing_DeletePublicSchemes.Enabled = False
            .mnuSharing_ExportToPublic.Enabled = False
            .mnuSharing_ImportPublicScheme.Enabled = False
        Else
            .mnuScheme_New.Enabled = True
            .mnuScheme_Modify.Enabled = ((bIsPScheme Or bIsDScheme) And Not bIsFavorite)
            .mnuScheme_Delete.Enabled = ((bIsPScheme Or bIsDScheme) And Not bIsFavorite)
            .mnuScheme_Properties.Enabled = Not bIsCategory
            .mnuScheme_SetAsDefault.Enabled = (bIsPScheme Or bIsFScheme)
            .mnuScheme_Favorites.Enabled = (bIsPScheme Or bIsFScheme)
            .mnuSharing_DeletePublicSchemes.Enabled = True
            .mnuSharing_ImportPublicScheme.Enabled = True
            .mnuSharing_ExportToPublic.Enabled = bIsPScheme
            
            If bNoEnglishHeadings Then
                .mnuScheme_ConvertToHeadingStyles.Visible = False
                .mnuScheme_ConvertToMacPacStyles.Visible = False
            Else
                If .mnuScheme_UseWordHeadings.Checked Then
                    .mnuScheme_ConvertToHeadingStyles _
                        .Visible = (bIsDScheme And Not bIsHScheme)
                    With .mnuScheme_ConvertToMacPacStyles
                        .Visible = Not (bIsDScheme And Not bIsHScheme)
                        .Enabled = (bIsDScheme And bIsHScheme)
                    End With
                Else
                    With .mnuScheme_ConvertToHeadingStyles
                        .Visible = Not (bIsDScheme And bIsHScheme)
                        .Enabled = (bIsDScheme And Not bIsHScheme)
                    End With
                    .mnuScheme_ConvertToMacPacStyles _
                        .Visible = (bIsDScheme And bIsHScheme)
                End If
            End If
        End If
        
        .btnUse.Enabled = Not bIsCategory
        .btnChangeScheme.Enabled = ((bIsCategory = False) And (m_iDocSchemes > 0))
        .btnReset.Enabled = bIsDScheme
        
        '9.9.5001 - added button functions to Schemes menu
        .mnuScheme_Use.Enabled = Not bIsCategory
        If bIsDScheme Then
            .mnuScheme_Use.Caption = "&Make Active"
        Else
            .mnuScheme_Use.Caption = "&Use"
        End If
        .mnuScheme_ChangeTo.Enabled = ((bIsCategory = False) And (m_iDocSchemes > 0))
        .mnuScheme_Reset.Enabled = bIsDScheme
    End With
End Sub

Private Sub mnuSharing_ExportToPublic_Click()
    Dim oShare As CShare
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
                    
    'GLOG 5350 - hide dialog during export to prevent
    'error #4198 (command failed) when attempting to
    'close the mpn file after saving it - this became
    'an issue with FileSite/Desksite 8.5 SP3
    Me.Hide
    
    Set oShare = New CShare
    oShare.ExportPublicScheme Me.Scheme, _
        Me.SchemeDisplayName
    Set oShare = Nothing
    
    'GLOG 5350 - redisplay dialog
    Me.Show vbModal

'    Me.sbStatus.SimpleText = "Ready"
    Exit Sub
ProcError:
    Set oShare = Nothing
    RaiseError "frmSchemes.mnuSharing_ExportToPublic_Click"
    Exit Sub
End Sub

Private Sub mnuSharing_ImportPublicScheme_Click()
    ShowPublicSchemes True
End Sub

Private Sub vsbPreview_Change()
    ScrollPreview
End Sub

Private Sub vsbPreview_Scroll()
    ScrollPreview
End Sub

Private Sub ScrollPreview()
    Dim iTopMin As Integer
    
    EchoOff
    If Me.SchemeType = mpSchemeType_Category Then
        iTopMin = 0
    Else
        iTopMin = 375
    End If

    Me.imgPreview.Top = _
        Me.imgContainer - Me.vsbPreview.Value + iTopMin
'    DoEvents
    EchoOn
End Sub

Public Sub ForcePreviewRefresh()
    tvwSchemes_NodeClick Me.tvwSchemes.SelectedItem
End Sub
Private Function RestorePos(sTopPos As Single, sLeftPos As Single, _
            sWidth As Single, sHeight As Single)
    On Error Resume Next
    With Me
        .Top = sTopPos
        .Left = sLeftPos
        .Width = sWidth
        .Height = sHeight
    End With
End Function

Private Sub DisplayAssociatedStyles()
    Dim iLevels As Integer
    Dim xStatus As String
    Dim xActive As String
    Dim bShowDescription As Boolean
    
    bShowDescription = (Me.SchemeType <> mpSchemeType_Category)
    
    If bShowDescription Then
        'selected scheme style info
        xStatus = Me.SchemeDisplayName & " "
        xActive = xActiveScheme(ActiveDocument)
        If xActive = "" Then _
            xStatus = xStatus & "scheme "
        If Me.SchemeType = mpSchemeType_Document Then
            xStatus = xStatus & "is using "
        ElseIf Me.mnuScheme_UseWordHeadings.Checked = True Then
            xStatus = xStatus & "will use "
        Else
            xStatus = xStatus & "uses "
        End If
        If ((Me.SchemeType = mpSchemeType_Document) And bIsHeadingScheme(Me.Scheme)) Or _
                ((Me.mnuScheme_UseWordHeadings.Checked = True) And (xHeadingScheme() = "")) Then
            xStatus = xStatus & "Word Heading Styles"
        Else
            iLevels = iGetLevels(Me.Scheme, Me.SchemeType)
            xStatus = xStatus & xGetStyleRoot(Me.Scheme) & _
                "_L1-" & iLevels & " styles"
        End If
    
        'append active scheme info
        If xActive <> "" Then
            If xStatus <> "" Then _
                xStatus = xStatus & ".  "
            If xActive = Me.Scheme Then
                xStatus = xStatus & "This is the active scheme."
            Else
                xActive = GetField(xActive, mpRecField_Alias, _
                    mpSchemeType_Document)
                xStatus = xStatus & xActive & " is the active scheme."
            End If
        End If
            
        'append scheme description to generic message
        If g_oCurScheme.Description <> "" Then
            If xStatus <> "" Then
                If Right$(xStatus, 1) <> "." Then _
                    xStatus = xStatus & "."
                xStatus = xStatus & vbCrLf
            End If
            xStatus = xStatus & g_oCurScheme.Description
        End If
    End If
    
    '9.9.5001 - status bar replaced with textbox
    Me.txtDescription = xStatus
    
'    'hide textbox and expand preview when nothing to show
'    Me.txtDescription.Visible = bShowDescription
'    Me.Frame2.Visible = bShowDescription
'    If Not bShowDescription Then
'        Me.vsbPreview.Height = 5010
'    Else
'        Me.vsbPreview.Height = 4020
'    End If
End Sub

Property Get SavePrivateStyFile() As Boolean
    SavePrivateStyFile = m_bSavePrivateStyFile
End Property

Property Let SavePrivateStyFile(ByVal bValue As Boolean)
    m_bSavePrivateStyFile = bValue
End Property
