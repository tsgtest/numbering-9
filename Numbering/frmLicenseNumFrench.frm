VERSION 5.00
Begin VB.Form frmLicenseNumFrench 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " License Number"
   ClientHeight    =   1410
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5025
   Icon            =   "frmLicenseNumFrench.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1410
   ScaleWidth      =   5025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton txtCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      Height          =   400
      Left            =   3870
      TabIndex        =   3
      Top             =   885
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   400
      Left            =   2775
      TabIndex        =   2
      Top             =   885
      Width           =   1000
   End
   Begin VB.TextBox txtKey 
      Height          =   360
      IMEMode         =   3  'DISABLE
      Left            =   120
      MaxLength       =   50
      TabIndex        =   1
      Top             =   390
      Width           =   4755
   End
   Begin VB.Label Label1 
      Caption         =   "Veuillez entrer un num�ro de licence MacPac valide:"
      Height          =   255
      Left            =   150
      TabIndex        =   0
      Top             =   150
      Width           =   4635
   End
End
Attribute VB_Name = "frmLicenseNumFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_xName As String
Private m_bCancelled As Boolean
Private m_xMessage As String

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Key() As String
    Key = m_xName
End Property

Public Property Let Key(xNew As String)
    m_xName = xNew
End Property

Public Property Get Message() As String
    Message = m_xMessage
End Property

Public Property Let Message(xNew As String)
    m_xMessage = xNew
    Me.Label1.Caption = xNew
End Property

Private Sub btnOK_Click()
    m_bCancelled = False
    Me.Key = Me.txtKey
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Load()
    m_bCancelled = True
End Sub

Private Sub txtCancel_Click()
    m_bCancelled = True
    Me.Key = Empty
    Me.Hide
    DoEvents
End Sub

Private Sub txtKey_Change()
'   enable OK btn only when there
'   is text in the Name txt box
    If txtKey.Text = "" Then
        btnOK.Enabled = False
    Else
        btnOK.Enabled = True
    End If
End Sub

Private Sub txtKey_GotFocus()
    With Me.txtKey
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub
