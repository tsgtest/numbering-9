VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMPN"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public Function xGetPropFld(oFont As Word.Font) As String
'returns the value the of the property that
'contains the level prop - either the NameFarEast
'field (Word 97) or the NameBi field (Word 2000)
    Dim xScheme As String
    Dim iLevel As Integer
    Dim oSource As Object
    Dim bIsSourceFile As Boolean
    
    Set oSource = oFont.Parent.Parent.Parent.Parent.Parent
    
    On Error Resume Next
    bIsSourceFile = Not (oSource _
        .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
    On Error GoTo 0
    
    If bIsSourceFile Then
        With oFont.Parent
            iLevel = .Index
            xScheme = .Parent.Parent.Name
        End With
        xGetPropFld = oSource.CustomDocumentProperties(xScheme & iLevel)
    Else
        If Application.Version Like "8.*" Then
            xGetPropFld = oFont.NameFarEast
        Else
            'use late bound object for post-97 property
            xGetPropFld = oFont.Parent.Font.NameBi
        End If
    End If
End Function

Public Function SetPropFld(oFont As Word.Font, ByVal xValue As String) As Long
'sets the value the of the property that
'contains the level prop - either the NameFarEast
'field (Word 97) or the NameBi field (Word 2000)
    Dim xScheme As String
    Dim iLevel As Integer
    Dim oSource As Object
    Dim bIsSourceFile As Boolean
    
    Set oSource = oFont.Parent.Parent.Parent.Parent.Parent
    On Error Resume Next
    bIsSourceFile = Not (oSource _
        .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
    On Error GoTo 0
    
    If bIsSourceFile Then
        With oFont.Parent
            iLevel = .Index
            xScheme = .Parent.Parent.Name
        End With
        On Error Resume Next
        oSource.CustomDocumentProperties( _
            xScheme & iLevel) = xValue
        If Err.Number Then
            oSource.CustomDocumentProperties.Add _
                xScheme & iLevel, False, 4, xValue
        End If
    Else
        If Application.Version Like "8.*" Then
            oFont.NameFarEast = xValue
        Else
            'use late bound object for post-97 property
            oFont.Parent.Font.NameBi = xValue
        End If
    End If
End Function

Public Function MPWDXX_DLLIsOK() As Boolean
'returns TRUE iff this dll is the correct
'version for the currently running version of Word
'    #If compWordVersion = "9" Then
'        If InStr(Word.Application.Version, "9.") Then
            MPWDXX_DLLIsOK = True
'        End If
'    #Else
'        If InStr(Word.Application.Version, "8.") Then
'            MPWDXX_DLLIsOK = True
'        End If
'    #End If
End Function

Public Function UpgradeLevelProp(llP As Word.ListLevel) As Boolean
'moves level prop for listlevel llp from NameFarEast to NameBi
    If Not Application.Version Like "8.*" Then
        'use late bound object for post-97 property
        llP.Parent.Parent.ListLevels(llP.Index).Font.NameBi = llP.Font.NameFarEast
    End If
End Function

Public Function RestorePropFldToTNR(oFont As Word.Font) As Long
    On Error Resume Next
    If Application.Version Like "8.*" Then
        oFont.NameFarEast = "Times New Roman"
    Else
        'use late bound object for post-97 property
        oFont.Parent.Font.NameBi = "Times New Roman"
    End If
End Function

Public Function bPropFldNeedsReset(oFont As Word.Font) As Boolean
    Dim xValue As String
    
    On Error Resume Next
    If Application.Version Like "8.*" Then
        xValue = oFont.NameFarEast
    Else
        'use late bound object for post-97 property
        xValue = oFont.Parent.Font.NameBi & oFont.NameFarEast
    End If
    
    If InStr(xValue, "|") Or (xValue = "###") Then
        bPropFldNeedsReset = True
    End If
End Function

Public Function GetResetOnHigherLevel(oLL As Word.ListLevel) As Long
    If Application.Version Like "8.*" Then
        'in Word 97, ResetOnHigher was a boolean
        GetResetOnHigherLevel = CLng(oLL.ResetOnHigher)
    Else
        'use late bound object for post-97 property
        GetResetOnHigherLevel = oLL.Parent.Parent.ListLevels(oLL.Index).ResetOnHigher
    End If
End Function

Public Sub SetResetOnHigherLevel(oLL As Word.ListLevel, lLevel As Long)
    If Application.Version Like "8.*" Then
        'in Word 97, ResetOnHigher was a boolean
        oLL.ResetOnHigher = CBool(lLevel)
    Else
        'use late bound object for post-97 property
        oLL.Parent.Parent.ListLevels(oLL.Index).ResetOnHigher = lLevel
    End If
End Sub

