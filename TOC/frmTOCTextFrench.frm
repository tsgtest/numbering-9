VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmInsertTOCTextFrench 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Ins�rer Table de Mati�res"
   ClientHeight    =   5280
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5340
   Icon            =   "frmTOCTextFrench.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5280
   ScaleWidth      =   5340
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   4600
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   75
      Width           =   5230
      _ExtentX        =   9234
      _ExtentY        =   8123
      _Version        =   393216
      Style           =   1
      TabHeight       =   609
      TabCaption(0)   =   "&Principal"
      TabPicture(0)   =   "frmTOCTextFrench.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraContent"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Options"
      TabPicture(1)   =   "frmTOCTextFrench.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTOCBody"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "fraSchemes"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "fraTOCScheme"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "&Avanc�"
      TabPicture(2)   =   "frmTOCTextFrench.frx":05C2
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "chkHyperlinks"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "btnEditHeaderFooter"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "cmdEdit"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "fraStyleList"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).ControlCount=   4
      Begin VB.Frame fraStyleList 
         Caption         =   "Inclure styles disponibles suivants"
         Height          =   2576
         Left            =   -74800
         TabIndex        =   26
         Top             =   480
         Width           =   4845
         Begin TrueDBList60.TDBList lstDesignate 
            Height          =   2140
            Left            =   150
            OleObjectBlob   =   "frmTOCTextFrench.frx":05DE
            TabIndex        =   27
            Top             =   285
            Width           =   3850
         End
      End
      Begin VB.CommandButton cmdEdit 
         Caption         =   "&�diter th�me personnalis� TM..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -74805
         TabIndex        =   30
         Top             =   4100
         Width           =   2400
      End
      Begin VB.CommandButton btnEditHeaderFooter 
         Caption         =   "Format &en-t�te/pied de page..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -74800
         TabIndex        =   29
         Top             =   3575
         Width           =   2400
      End
      Begin VB.CheckBox chkHyperlinks 
         Caption         =   "&Ins�rer entr�es TM en hyperliens"
         Height          =   270
         Left            =   -74800
         TabIndex        =   28
         Top             =   3175
         Width           =   4000
      End
      Begin VB.Frame fraTOCBody 
         Caption         =   "Autre"
         Height          =   1325
         Left            =   -74800
         TabIndex        =   17
         Top             =   1450
         Width           =   4845
         Begin VB.CheckBox chkApplyManualFormatsToTOC 
            Caption         =   "Appl&iquer format direct Titre au TM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   195
            TabIndex        =   19
            Top             =   560
            Width           =   3500
         End
         Begin VB.CheckBox chkApplyTOCScheme 
            Caption         =   "Apply TOC &Scheme:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   195
            TabIndex        =   36
            Top             =   3285
            Value           =   1  'Checked
            Visible         =   0   'False
            Width           =   1830
         End
         Begin VB.CheckBox chkApplyTOC9 
            Caption         =   "Appliquer TM style &9 �:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   195
            TabIndex        =   20
            Top             =   900
            Width           =   1950
         End
         Begin VB.CheckBox chkTwoColumn 
            Caption         =   "&Utiliser format 2 colonnes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   195
            TabIndex        =   18
            Top             =   225
            Width           =   3500
         End
         Begin TrueDBList60.TDBCombo cbxScheduleStyles 
            Height          =   564
            Left            =   2195
            OleObjectBlob   =   "frmTOCTextFrench.frx":2CAE
            TabIndex        =   21
            Top             =   880
            Width           =   1800
         End
      End
      Begin VB.Frame fraSchemes 
         Caption         =   "Comprend"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1630
         Left            =   -74800
         TabIndex        =   22
         Top             =   2830
         Width           =   4845
         Begin VB.OptionButton optInclude 
            Caption         =   "&Entr�es � partir du th�me suivant"
            Height          =   195
            Index           =   0
            Left            =   195
            TabIndex        =   23
            Top             =   285
            Value           =   -1  'True
            Width           =   3500
         End
         Begin VB.OptionButton optInclude 
            Caption         =   "&Appliquer styles individuellement (Onglet avanc�)"
            Height          =   195
            Index           =   1
            Left            =   195
            TabIndex        =   25
            Top             =   1315
            Width           =   3800
         End
         Begin TrueDBList60.TDBList lstStyles 
            Height          =   675
            Left            =   450
            OleObjectBlob   =   "frmTOCTextFrench.frx":4BDB
            TabIndex        =   24
            Top             =   550
            Width           =   3550
         End
      End
      Begin VB.Frame fraTOCScheme 
         Caption         =   "Format"
         Height          =   990
         Left            =   -74800
         TabIndex        =   13
         Top             =   400
         Width           =   4845
         Begin VB.OptionButton optTOCStyles 
            Caption         =   "Garder les styles TM &courants"
            Height          =   300
            Index           =   1
            Left            =   195
            TabIndex        =   16
            Top             =   615
            Width           =   2500
         End
         Begin VB.OptionButton optTOCStyles 
            Caption         =   "Mettre � jour &styles TM:"
            Height          =   300
            Index           =   0
            Left            =   195
            TabIndex        =   14
            Top             =   240
            Value           =   -1  'True
            Width           =   1950
         End
         Begin TrueDBList60.TDBCombo cbxTOCScheme 
            Height          =   564
            Left            =   2195
            OleObjectBlob   =   "frmTOCTextFrench.frx":72A8
            TabIndex        =   15
            Top             =   225
            Width           =   1800
         End
      End
      Begin VB.Frame fraContent 
         Caption         =   "Contenu"
         Height          =   3950
         Left            =   200
         TabIndex        =   1
         Top             =   450
         Width           =   4845
         Begin VB.OptionButton optCreateFrom 
            Caption         =   "First &Sentence (period and 2 spaces)"
            Height          =   225
            Index           =   0
            Left            =   1405
            TabIndex        =   35
            Top             =   5000
            Value           =   -1  'True
            Visible         =   0   'False
            Width           =   2900
         End
         Begin VB.OptionButton optCreateFrom 
            Caption         =   "First S&entence (period and 1 space)"
            Height          =   225
            Index           =   1
            Left            =   1405
            TabIndex        =   34
            Top             =   5000
            Visible         =   0   'False
            Width           =   2900
         End
         Begin VB.OptionButton optCreateFrom 
            Caption         =   "Whole &Paragraph"
            Height          =   225
            Index           =   2
            Left            =   1405
            TabIndex        =   33
            Top             =   5000
            Visible         =   0   'False
            Width           =   2900
         End
         Begin VB.CheckBox chkTCEntries 
            Caption         =   "Entr�es &TM"
            Height          =   270
            Left            =   300
            TabIndex        =   9
            Top             =   1820
            Width           =   2175
         End
         Begin VB.CheckBox chkStyles 
            Caption         =   "St&yles:"
            Height          =   285
            Left            =   300
            TabIndex        =   7
            Top             =   1380
            Value           =   1  'Checked
            Width           =   760
         End
         Begin VB.CommandButton btnSetDefaults 
            Caption         =   "&D�finir valeurs par d�faut"
            Height          =   400
            Left            =   2585
            TabIndex        =   12
            Top             =   3350
            Width           =   2100
         End
         Begin TrueDBList60.TDBCombo cbxMinLevel 
            Height          =   564
            Left            =   1620
            OleObjectBlob   =   "frmTOCTextFrench.frx":91D0
            TabIndex        =   3
            Top             =   380
            Width           =   570
         End
         Begin TrueDBList60.TDBCombo cbxMaxLevel 
            Height          =   564
            Left            =   2490
            OleObjectBlob   =   "frmTOCTextFrench.frx":B0F7
            TabIndex        =   5
            Top             =   380
            Width           =   570
         End
         Begin TrueDBList60.TDBCombo cbxHeadingDef 
            Height          =   564
            Left            =   1085
            OleObjectBlob   =   "frmTOCTextFrench.frx":D01E
            TabIndex        =   8
            Top             =   1350
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cbxInsertAt 
            Height          =   564
            Left            =   730
            OleObjectBlob   =   "frmTOCTextFrench.frx":EF47
            TabIndex        =   11
            Top             =   2400
            Width           =   2700
         End
         Begin VB.Label Label1 
            Caption         =   "Cr�er � partir de:"
            Height          =   300
            Left            =   200
            TabIndex        =   6
            Top             =   1000
            Width           =   1185
         End
         Begin VB.Label lblInsertAt 
            Caption         =   "&Ins�rer:"
            Height          =   225
            Left            =   200
            TabIndex        =   10
            Top             =   2450
            Width           =   530
         End
         Begin VB.Label lblIncludeLevels 
            Caption         =   "Co&mprend niveaux"
            Height          =   225
            Left            =   200
            TabIndex        =   2
            Top             =   430
            Width           =   1425
         End
         Begin VB.Label lblThrough 
            Caption         =   "�"
            Height          =   225
            Left            =   2275
            TabIndex        =   4
            Top             =   430
            Width           =   660
         End
      End
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      Height          =   400
      Left            =   4075
      TabIndex        =   32
      Top             =   4795
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   2905
      TabIndex        =   31
      Top             =   4795
      Width           =   1100
   End
End
Attribute VB_Name = "frmInsertTOCTextFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancelled As Boolean
Private m_bStepIndent As Boolean
Private xarStyles As xArray
Private xarDesignate As xArray
Private m_bTOCBold As Boolean
Private m_bTOCCap As Boolean
Private m_bTOCUnderline As Boolean
Private m_bPageBold As Boolean
Private m_bPageCap As Boolean
Private m_bPageUnderline As Boolean
Private m_iNumberStyle As Integer
Private m_iNumberPunctuation As Integer
Private m_xSchemes() As String
Private m_bRefreshStyleList As Boolean
Private m_bFormInit As Boolean
Private m_bNoJumpToAdvanced As Boolean
Private m_iTOCMarker As mpTOCExistsMarker
Private m_bContinuePageNumbering As Boolean

Public Property Get TOCScheme() As String
    If Me.cbxTOCScheme.SelectedItem = 0 Then
        TOCScheme = "Custom"
    Else
        TOCScheme = m_xSchemes(Me.cbxTOCScheme.SelectedItem - 1, 1)
    End If
End Property

Public Property Get BoldHeaderTOC() As Boolean
    BoldHeaderTOC = m_bTOCBold
End Property

Public Property Let BoldHeaderTOC(bNew As Boolean)
    m_bTOCBold = bNew
End Property

Public Property Get CapHeaderTOC() As Boolean
    CapHeaderTOC = m_bTOCCap
End Property

Public Property Let CapHeaderTOC(bNew As Boolean)
    m_bTOCCap = bNew
End Property

Public Property Get UnderlineHeaderTOC() As Boolean
    UnderlineHeaderTOC = m_bTOCUnderline
End Property

Public Property Let UnderlineHeaderTOC(bNew As Boolean)
    m_bTOCUnderline = bNew
End Property

Public Property Get BoldHeaderPage() As Boolean
    BoldHeaderPage = m_bPageBold
End Property

Public Property Let BoldHeaderPage(bNew As Boolean)
    m_bPageBold = bNew
End Property

Public Property Get CapHeaderPage() As Boolean
    CapHeaderPage = m_bPageCap
End Property

Public Property Let CapHeaderPage(bNew As Boolean)
    m_bPageCap = bNew
End Property

Public Property Get UnderlineHeaderPage() As Boolean
    UnderlineHeaderPage = m_bPageUnderline
End Property

Public Property Let UnderlineHeaderPage(bNew As Boolean)
    m_bPageUnderline = bNew
End Property

Public Property Get NumberStyle() As Integer
    NumberStyle = m_iNumberStyle
End Property

Public Property Let NumberStyle(iNew As Integer)
    m_iNumberStyle = iNew
End Property

Public Property Get NumberPunctuation() As Integer
    NumberPunctuation = m_iNumberPunctuation
End Property

Public Property Let NumberPunctuation(iNew As Integer)
    m_iNumberPunctuation = iNew
End Property

Public Property Let Exclusions(ByVal xNew As String)
'checks off the selected list
    Dim i As Integer

'   cycle through xarray
    With xarStyles
        For i = 0 To .Count(1) - 1
'           if scheme is in exclusion string, uncheck
            If InStr(xNew, "," & .Value(i, 2) & ",") Then
'               if so, add to comma delimited string
                .Value(i, 0) = 0
            End If
        Next i
    End With
    
End Property

Public Property Get Exclusions() As String
'returns a string of schemes that have been unchecked
    Dim i As Integer
    Dim xTemp As String
    
'   cycle through xarray
    With xarStyles
        For i = 0 To .Count(1) - 1
'           get if scheme should be excluded
            If (.Value(i, 0) = 0) Then
'               if so, add to comma delimited string
                xTemp = xTemp & .Value(i, 2) & ","
            End If
        Next i
    End With
    
    If Len(xTemp) Then
'       if list is not empty, add a preceding ','
        xTemp = "," & xTemp
    End If
    
    Exclusions = xTemp
End Property

Public Property Get Inclusions() As String
'returns the list of schemes to
'use to create toc entries - if
'user has specified that only marked
'text should be used, return an empty
'inclusions list
    Dim i As Integer
    Dim xTemp As String
    
'   cycle through xarray
    With xarStyles
        For i = 0 To .Count(1) - 1
'           get if scheme should be included
            If .Value(i, 0) = 1 Then
'               if so, add to comma delimited string
                xTemp = xTemp & .Value(i, 2) & ","
            End If
        Next i
    End With
    
    If Len(xTemp) Then
'       if list is not empty, add a preceding ','
        xTemp = "," & xTemp
    End If
    
    Inclusions = xTemp
End Property

Private Sub btnEditHeaderFooter_Click()
    With frmHeaderFooterFrench
        .chkPageBold = Abs(Me.BoldHeaderPage)
        .chkTOCBold = Abs(Me.BoldHeaderTOC)
        .chkPageCaps = Abs(Me.CapHeaderPage)
        .chkTOCCaps = Abs(Me.CapHeaderTOC)
        .chkPageUnderline = Abs(Me.UnderlineHeaderPage)
        .chkTOCUnderline = Abs(Me.UnderlineHeaderTOC)
        .NumberPunctuationDefault = Me.NumberPunctuation
        .NumberStyleDefault = Me.NumberStyle
        .chkContinue = Abs(Me.ContinuePageNumbering)
        
        .Show vbModal
        
        If Not .Cancelled Then
            Me.BoldHeaderPage = .chkPageBold
            Me.BoldHeaderTOC = .chkTOCBold
            Me.CapHeaderPage = .chkPageCaps
            Me.CapHeaderTOC = .chkTOCCaps
            Me.UnderlineHeaderPage = .chkPageUnderline
            Me.UnderlineHeaderTOC = .chkTOCUnderline
            Me.NumberPunctuation = .cmbPunctuation.SelectedItem
            Me.NumberStyle = .cmbNumberStyle.SelectedItem
            Me.ContinuePageNumbering = .chkContinue
            
'           reset current boilerplate
            If .cbxType.BoundText <> "" Then
                bAppSetUserINIValue "TOC", "CurrentBoilerplate", _
                    g_xBoilerplates(.cbxType.SelectedItem, 1)
            End If
        End If
    End With
    Unload frmHeaderFooterFrench
End Sub

Private Sub btnSetDefaults_Click()
    Dim xValue As String
    
    'create from
    bAppSetUserINIValue "TOC", "CreateFromStyles", CStr(Me.chkStyles)
    bAppSetUserINIValue "TOC", "CreateFromTCEntries", CStr(Me.chkTCEntries)
    
    'levels
    bAppSetUserINIValue "TOC", "LevelStart", Trim(Me.cbxMinLevel)
    bAppSetUserINIValue "TOC", "LevelEnd", Trim(Me.cbxMaxLevel)
    
    'insertion location
    With Me.cbxInsertAt
        If (.Text <> mpTOCLocationAboveTOAFrench) And (.Text <> mpTOCLocationAtCurrentFrench) Then _
            bAppSetUserINIValue "TOC", "DefaultLocation", .SelectedItem
    End With
    
    'heading definition
    If Me.cbxHeadingDef.SelectedItem = 1 Then
        'period and 2 spaces was old default
        xValue = "0"
    ElseIf Me.cbxHeadingDef.SelectedItem = 2 Then
        'whole paragraph
        xValue = "1"
    Else
        'period and 1 space
        xValue = "3"
    End If
    bAppSetUserINIValue "TOC", "CreateFrom", xValue
    
    Application.StatusBar = "Valeurs par d�faut d�finies dans l'onglet principal"
End Sub

Private Sub cbxHeadingDef_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxHeadingDef, Reposition
    Exit Sub
ProcError:
    RaiseError "frmInsertTOCText.cbxHeadingDef_Mismatch"
    Exit Sub
End Sub


Private Sub cbxInsertAt_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxInsertAt, Reposition
    Exit Sub
ProcError:
    RaiseError "frmInsertTOCText.cbxInsertAt_Mismatch"
    Exit Sub
End Sub

Private Sub cbxMaxLevel_ItemChange()
    If Not m_bFormInit Then _
        m_bRefreshStyleList = True
End Sub

Private Sub cbxMinLevel_ItemChange()
    If Not m_bFormInit Then _
        m_bRefreshStyleList = True
End Sub

Private Sub cbxMinLevel_LostFocus()
    If Me.cbxMinLevel > Me.cbxMaxLevel Then
        xMsg = "Niveau le plus bas doit �tre inf�rieur ou �gal au niveau le plus �lev�."
        MsgBox xMsg, vbExclamation, "Num�rotation MacPac"
        Me.cbxMinLevel = 1
        Me.cbxMinLevel.SetFocus
    ElseIf m_bRefreshStyleList Then
        xarGetStyles
    End If
End Sub

Private Sub cbxMaxLevel_LostFocus()
    If Me.cbxMinLevel > Me.cbxMaxLevel Then
        xMsg = "Niveau le plus �lev� doit �tre sup�rieur ou �gal au niveau le plus bas."
        MsgBox xMsg, vbExclamation, "Num�rotation MacPac"
        Me.cbxMaxLevel = 9
        Me.cbxMaxLevel.SetFocus
    ElseIf m_bRefreshStyleList Then
        xarGetStyles
    End If
End Sub

Private Sub cbxMaxLevel_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxMaxLevel, Reposition
    Exit Sub
ProcError:
    RaiseError "frmInsertTOCText.cbxMaxLevel_Mismatch"
    Exit Sub
End Sub

Private Sub cbxMinLevel_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxMinLevel, Reposition
    Exit Sub
ProcError:
    RaiseError "frmInsertTOCText.cbxMinLevel_Mismatch"
    Exit Sub
End Sub

Private Sub cbxScheduleStyles_ItemChange()
    If Not m_bFormInit Then _
        m_bRefreshStyleList = True
End Sub

Private Sub cbxScheduleStyles_LostFocus()
    If m_bRefreshStyleList Then
        If Me.optInclude(0) Then
            xarGetStyles
        ElseIf Me.chkApplyTOC9 = 1 Then
            SelectTOC9Style Me.cbxScheduleStyles
        End If
    End If
End Sub

Private Sub cbxTOCScheme_ItemChange()
    Me.cmdEdit.Enabled = Me.cbxTOCScheme.Text = "Personnalis�"
End Sub

Private Sub cbxTOCScheme_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxTOCScheme, Reposition
    Exit Sub
ProcError:
    RaiseError "frmInsertTOCText.cbxTOCScheme_Mismatch"
    Exit Sub
End Sub

Private Sub chkApplyTOC9_Click()
    If Not m_bFormInit Then
        If Me.optInclude(0) Then
'           refresh list
            xarGetStyles
        
'           force designation option
            If Me.chkApplyTOC9 = 1 Then
                m_bNoJumpToAdvanced = True
                Me.optInclude(1) = True
                m_bNoJumpToAdvanced = False
            End If
        Else
'           designate TOC9 style for inclusion
            SelectTOC9Style Me.cbxScheduleStyles
        End If
    End If
End Sub

Private Sub chkStyles_Click()
    Me.cbxHeadingDef.Enabled = CBool(Me.chkStyles)
End Sub

Private Sub cmdEdit_Click()
    Dim bDefaultCustom
    bRet = mpTOC.EditCustomTOCScheme()
    If bRet Then
'       user saved the edits to custom toc scheme

'       set current toc scheme to
'       custom if specified by user
        bDefaultCustom = xAppGetUserINIValue( _
                            "TOC", _
                            "DefaultToCustom")
        If bDefaultCustom Then
            Me.cbxTOCScheme.SelectedItem = 0
        End If
    End If
End Sub

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnOK_Click()
'   validate
    If bIsValid() Then
        Me.Cancelled = False
        Me.Hide
        DoEvents
        SaveCurInput
    End If
End Sub


Private Sub lstStyles_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim lRow As Long
        
    If KeyCode = vbKeySpace Then
'       toggle chkbox in Schemes list
        lRow = Me.lstStyles.Row
        If xarStyles.Value(lRow, 0) = 0 Then
            xarStyles.Value(lRow, 0) = 1
        Else
            xarStyles.Value(lRow, 0) = 0
        End If
        Me.lstStyles.Refresh
        m_bRefreshStyleList = True
    End If
End Sub

Private Sub lstStyles_LostFocus()
    If m_bRefreshStyleList Then
        xarGetStyles
    End If
End Sub

Private Sub optInclude_Click(Index As Integer)
    Me.lstStyles.Enabled = (Index = 0)
    Me.cbxMaxLevel.Enabled = (Index = 0)
    Me.cbxMinLevel.Enabled = (Index = 0)
    Me.lblIncludeLevels.Enabled = (Index = 0)
    Me.lblThrough.Enabled = (Index = 0)
    
    If Not m_bFormInit Then
        If Index = 0 Then
'           uncheck Apply TOC 9 - this will also reset defaults
            Me.chkApplyTOC9 = 0
            
''           reset defaults
'            xarGetStyles
        Else
''           take user to Advanced tab
'            If Not m_bNoJumpToAdvanced Then
'                Me.vsIndexTab1.CurrTab = 2
'                Me.lstDesignate.SetFocus
'            End If
        End If
    End If
End Sub

Private Sub Form_Activate()
    Dim xUserMin As String
    Dim xUserMax As String
    Dim xValue As String
    Dim xUserDef As String
    Dim iCurDefScheme As Integer
    Dim xTOCField As String
    
    On Error GoTo Initialize_Error
    
'   avoid form re-activating
    If Not m_bFormInit Then Exit Sub
    
'   levels - first try user ini, then firm ini
    xUserMin = xAppGetUserINIValue("TOC", "LevelStart")
    If xUserMin <> "" Then
        On Error Resume Next
        Me.cbxMinLevel.BoundText = xUserMin
        On Error GoTo Initialize_Error
        If Me.cbxMinLevel.BoundText = "" Then _
            Me.cbxMinLevel.BoundText = g_iDefaultLevelStart
    Else
        Me.cbxMinLevel.BoundText = g_iDefaultLevelStart
    End If
    
    xUserMax = xAppGetUserINIValue("TOC", "LevelEnd")
    If xUserMax <> "" Then
        On Error Resume Next
        Me.cbxMaxLevel.BoundText = xUserMax
        On Error GoTo Initialize_Error
        If Me.cbxMaxLevel.BoundText = "" Then _
            Me.cbxMaxLevel.BoundText = g_iDefaultLevelEnd
    Else
        Me.cbxMaxLevel.BoundText = g_iDefaultLevelEnd
    End If

    If m_iTOCMarker <> mpTOCExistsMarker_None Then
        Me.cbxInsertAt.SelectedItem = 0
        Me.cbxInsertAt.Enabled = False
        
        Me.lblInsertAt.Enabled = False
        Me.btnEditHeaderFooter.Enabled = False
        
        If m_iTOCMarker <> mpTOCExistsMarker_Field Then
'       there's an existing MacPac TOC;
'       default to keep current TOC styles
            Me.optTOCStyles(1) = True
        End If
    Else
        xUserDef = xAppGetUserINIValue("TOC", "DefaultLocation")
        If bTOAExists(ActiveDocument) Then
            Me.cbxInsertAt.SelectedItem = GetTDBListCount(Me.cbxInsertAt) - 1
        Else
            If (IsNull(xUserDef)) Or _
                    (Val(xUserDef) > GetTDBListCount(Me.cbxInsertAt) - 1) Then
                Me.cbxInsertAt.SelectedItem = 0
            Else
                Me.cbxInsertAt.SelectedItem = CLng(Val(xUserDef))
            End If
        End If
    End If
    
    With Me.cbxTOCScheme
        On Error Resume Next
        If xAppGetUserINIValue("TOC", "DefaultToCustom") = "1" Then
            .SelectedItem = 0
        Else
            iCurDefScheme = iGetCurDefTOCScheme()
            xTOCField = GetTOCField(iCurDefScheme, _
                                mpTOCRecField_Name, _
                                mpSchemeType_TOC)
            If xTOCField <> "" Then
                xTOCField = xTranslateTOCSchemeDisplayName(xTOCField)
                MatchCompleteInXArrayList xTOCField, Me.cbxTOCScheme
            Else
                .SelectedItem = 0
            End If
        End If
        On Error GoTo Initialize_Error
    End With
    
'   initialize to first in list
    Me.cbxScheduleStyles.SelectedItem = 0
    
    'set default checkbox value
    If g_bApplyTOC9StyleDefault Then
        Me.chkApplyTOC9 = 1
        Me.optInclude(1) = True
    End If

'   get heading definition default
    xValue = xAppGetUserINIValue("TOC", "CreateFrom")
    If xValue = "0" Then
        'period and 2 spaces
        Me.cbxHeadingDef.SelectedItem = 1
    ElseIf xValue = "1" Then
        'whole paragraph
        Me.cbxHeadingDef.SelectedItem = 2
    Else
        '10/1/12 - period and 1 space is now the default
        Me.cbxHeadingDef.SelectedItem = 0
    End If

    GetPrevInput
        
'   load available styles
    DoEvents
    xarGetStyles
    
    On Error Resume Next
    Me.StyleExclusions = ActiveDocument.Variables("StyleExclusions")
    Me.StyleInclusions = ActiveDocument.Variables("StyleInclusions")
    On Error GoTo Initialize_Error
    
    m_bFormInit = False
    
    Exit Sub
    
Initialize_Error:
    Select Case Err
        Case Else
            MsgBox Error(Err.Number), vbExclamation, AppName
    End Select
    
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim iNumSchemes As Integer
    Dim xStyleSchemes() As String
    Dim iCurDefScheme As Integer
    Dim xHeadingStyles As String
    Dim xItemLocations() As String
    Dim iNumStyleSchemes As Integer
    Dim xUserDef As String
    Dim iBoilerplate As Integer
    Dim xUserMin As String
    Dim xUserMax As String
    Dim xValue As String
    Dim xarLevels As xArray
    Dim xarInsertAt As xArray
    Dim xarTOCScheme As xArray
    Dim xarScheduleStyles As xArray
    Dim xarHeadingDef As xArray
    
    On Error GoTo Initialize_Error
    m_bFormInit = True
    Me.Cancelled = True
    
    Set xarLevels = New xArray
    xarLevels.ReDim 0, 8, 0, 1
    For i = 0 To 8
        xarLevels(i, 0) = i + 1
        xarLevels(i, 1) = i + 1
    Next i
    
    Me.cbxMinLevel.Array = xarLevels
    Me.cbxMaxLevel.Array = xarLevels
    ResizeTDBCombo Me.cbxMinLevel, 9
    ResizeTDBCombo Me.cbxMaxLevel, 9
    
'   load insert at
    Set xarInsertAt = New xArray
    m_iTOCMarker = iTOCExists(ActiveDocument)
    If m_iTOCMarker <> mpTOCExistsMarker_None Then
        xarInsertAt.ReDim 0, 0, 0, 1
        xarInsertAt(0, 0) = mpTOCLocationAtCurrentFrench
        xarInsertAt(0, 1) = mpTOCLocationAtCurrentFrench
        
        Me.cbxInsertAt.Array = xarInsertAt
        ResizeTDBCombo Me.cbxInsertAt, 1
    Else
        If bTOAExists(ActiveDocument) Then
            xarInsertAt.ReDim 0, UBound(g_xTOCLocations) + 1, 0, 1
            For i = 0 To UBound(g_xTOCLocations)
                xarInsertAt(i, 0) = g_xTOCLocations(i)
                xarInsertAt(i, 1) = g_xTOCLocations(i)
            Next i
'            add and default to "above TOA"
            xarInsertAt(i, 0) = mpTOCLocationAboveTOAFrench
            xarInsertAt(i, 1) = mpTOCLocationAboveTOAFrench
            
            Me.cbxInsertAt.Array = xarInsertAt
            ResizeTDBCombo Me.cbxInsertAt, i + 1
        Else
            xarInsertAt.ReDim 0, UBound(g_xTOCLocations), 0, 1
            For i = 0 To UBound(g_xTOCLocations)
                xarInsertAt(i, 0) = g_xTOCLocations(i)
                xarInsertAt(i, 1) = g_xTOCLocations(i)
            Next i
        
            Me.cbxInsertAt.Array = xarInsertAt
            ResizeTDBCombo Me.cbxInsertAt, i + 1
        End If
    
'       sync current boilerplate with default - this should
'       ultimately be a property of the form
        On Error Resume Next
        iBoilerplate = xAppGetUserINIValue("TOC", _
            "DefaultBoilerplateIndex")
        If g_xBoilerplates(iBoilerplate, 1) <> "" Then
            bAppSetUserINIValue "TOC", "CurrentBoilerplate", _
                g_xBoilerplates(iBoilerplate, 1)
        End If
        On Error GoTo Initialize_Error
    End If

    iNumSchemes = iGetTOCSchemes(m_xSchemes())
        
'   load TOC schemes
    Set xarTOCScheme = New xArray
    xarTOCScheme.ReDim 0, UBound(m_xSchemes) + 1, 0, 1
    
    xarTOCScheme(0, 0) = "Personnalis�"
    xarTOCScheme(0, 1) = "Personnalis�"
    For i = 0 To UBound(m_xSchemes)
        xarTOCScheme(i + 1, 0) = m_xSchemes(i, 0)
        xarTOCScheme(i + 1, 1) = m_xSchemes(i, 1)
    Next i
    
    Me.cbxTOCScheme.Array = xarTOCScheme
    ResizeTDBCombo Me.cbxTOCScheme, 4
    
'   load Heading Def
    Set xarHeadingDef = New xArray
    xarHeadingDef.ReDim 0, 2, 0, 1
    
    xarHeadingDef(0, 0) = "Premi�re phrase (point et au moins 1 espace)"
    xarHeadingDef(0, 1) = "0"
    xarHeadingDef(1, 0) = "Premi�re phrase (point et au moins 2 espaces)"
    xarHeadingDef(1, 1) = "1"
    xarHeadingDef(2, 0) = "Paragraphe entier"
    xarHeadingDef(2, 1) = "3"
    
    Me.cbxHeadingDef.Array = xarHeadingDef
    ResizeTDBCombo Me.cbxHeadingDef, 3

'   load schedule styles
    If g_xScheduleStyles(0) = "" Then
        Me.chkApplyTOC9.Visible = False
        Me.cbxScheduleStyles.Visible = False
        Me.fraTOCBody.Height = 1000
        Me.fraSchemes.Top = 2185
    Else
        Set xarScheduleStyles = New xArray
        xarScheduleStyles.ReDim 0, UBound(g_xScheduleStyles), 0, 1
        
        For i = 0 To UBound(g_xScheduleStyles)
            xarScheduleStyles(i, 0) = g_xScheduleStyles(i)
            xarScheduleStyles(i, 1) = g_xScheduleStyles(i)
        Next i
                
        Me.cbxScheduleStyles.Array = xarScheduleStyles
        ResizeTDBCombo Me.cbxScheduleStyles, i + 1
    End If
        
    With Me
        '"create from" checkboxes
        xValue = xAppGetUserINIValue("TOC", "CreateFromStyles")
        If xValue <> "" Then _
            .chkStyles = Val(xValue)
        xValue = xAppGetUserINIValue("TOC", "CreateFromTCEntries")
        If xValue <> "" Then _
            .chkTCEntries = Val(xValue)
        If .chkStyles + .chkTCEntries = 0 Then _
            .chkStyles = 1
            
'       load schemes in use
        xarGetSchemes
                
'       get defaults for Header/Footer
        On Error Resume Next
        .BoldHeaderTOC = xAppGetUserINIValue("TOC", "HeaderTOCBold")
        .CapHeaderTOC = xAppGetUserINIValue("TOC", "HeaderTOCCaps")
        .UnderlineHeaderTOC = xAppGetUserINIValue("TOC", "HeaderTOCUnderline")
        .BoldHeaderPage = xAppGetUserINIValue("TOC", "HeaderPageBold")
        .CapHeaderPage = xAppGetUserINIValue("TOC", "HeaderPageCaps")
        .UnderlineHeaderPage = xAppGetUserINIValue("TOC", "HeaderPageUnderline")
        .NumberStyle = xAppGetUserINIValue("TOC", "PageNoStyle")
        .NumberPunctuation = xAppGetUserINIValue("TOC", "PageNoPunctuation")
        .ContinuePageNumbering = xAppGetUserINIValue("TOC", "ContinuePageNoFromPrevious")
        
        .btnEditHeaderFooter.Visible = g_bAllowHeaderFooterEdit
    End With
        
'   hyperlink switch is not available in Word 97
    '9.9.5001 - this checkbox should never be displayed in text dialog
'    If (Not g_bAllowTOCAsField) Or _
'            (InStr(Word.Application.Version, "8.") <> 0) Then
        Me.chkHyperlinks.Visible = False
        Me.chkHyperlinks.Value = 0
'       fill space on advanced tab
        Me.fraStyleList.Height = 3000
        Me.lstDesignate.Height = 2544
'    ElseIf Me.chkInsertAsField.Value = 0 Then
'        'GLOG 4714 (9/30/11)
'        Me.chkHyperlinks.Enabled = False
'    End If
    
    Exit Sub
    
Initialize_Error:
    Select Case Err
        Case Else
            MsgBox Error(Err.Number), vbExclamation, "Num�rotation MacPac"
    End Select
    Exit Sub
End Sub

Property Let bStepIndent(bStepIndent As Boolean)
    m_bStepIndent = bStepIndent
End Property

Property Get bStepIndent() As Boolean
    bStepIndent = m_bStepIndent
End Property


Private Function xarGetSchemes()
    Dim i As Integer
    Dim iNumSchemes As Integer
    Dim xDocSchemes() As String
    Dim bHSchemeExists As Boolean
    Dim bHStylesUsed As Boolean
    Dim iSchedule As Integer
    Dim bFirstRun As Boolean
    
'   position dropdown will be disabled on subsequent runs
    bFirstRun = Me.cbxInsertAt.Enabled
    
'   get numbering schemes in active doc
    iNumSchemes = iGetSchemes(xDocSchemes(), _
                              mpSchemeType_Document)
    
'   check if any schedule styles are listed in ini
'    If g_xScheduleStyles(0) <> "" Then
'        iSchedule = 1
'    End If
        
'   get if there is a heading scheme in the doc
    bHSchemeExists = (Len(xHeadingScheme()) > 0)
    
    Set xarStyles = New xArray
    With xarStyles
        .ReDim 0, iNumSchemes + iSchedule, 0, 2
        
'       add schedule option only if at least one style is listed in ini
        If iSchedule = 1 Then
            .Value(iNumSchemes + 1, 2) = "Schedule"
            .Value(iNumSchemes + 1, 1) = "Schedule/Exhibit Styles"
            .Value(iNumSchemes + 1, 0) = Abs(g_bIncludeSchedule Or Not bFirstRun)
        End If
        
''       add other styles option
'        .Value(iNumSchemes + 1, 2) = "Other"
'        .Value(iNumSchemes + 1, 1) = "Other Outline Level Styles"
'        .Value(iNumSchemes + 1, 0) = Abs(Not bFirstRun)
        
'       get all numbering schemes in document
        For i = 1 To iNumSchemes
            .Value(i, 2) = xDocSchemes(i - 1, 0)
            .Value(i, 1) = xDocSchemes(i - 1, 1)
            If .Value(i, 1) = "" Then
'               if scheme props are missing for some reason,
'               display style root rather than an empty string
                .Value(i, 1) = xGetStyleRoot(.Value(i, 2))
            End If
            If bIsHeadingScheme(.Value(i, 2)) Then
                .Value(i, 1) = "Styles Titre Word (" & _
                    .Value(i, 1) & ")"
            End If
            .Value(i, 0) = 1
        Next i
    
        If bHSchemeExists Then
'           there is a MacPac scheme
'           that uses heading styles...
'           delete row reserved for
'           Word Heading Styles
            .Delete 1, 0
        Else
'           if there is no MacPac scheme
'           that uses heading styles...
'           add heading styles
            .Value(0, 2) = "Heading"
            .Value(0, 1) = "Styles Titre Word"
'           only default on if no schemes
            .Value(0, 0) = Abs(iNumSchemes = 0 Or Not bFirstRun)
        End If
    End With
    
'   load list
    Me.lstStyles.Array = xarStyles
    Me.lstStyles.Rebind
    
End Function

Private Function xarGetStyles()
    Dim styP As Word.Style
    Dim i As Integer
    Dim iCount As Integer
    Dim iLevel As Integer
    Dim iPos As Integer
    Dim xScheme As String
    Dim bOn As Boolean
    Dim bIncludeHScheme As Boolean
    Dim xHScheme As String
    Dim xInclusions As String
    Dim xBase As String
    Dim xarTemp As xArray
    Dim bIsReUse As Boolean
    Dim xTest As String
    
    On Error GoTo ProcError
    
    Set xarTemp = New xArray
    xarTemp.ReDim 0, 1000, 0, 2
    
'   test to see whether previous exclusion list is stored
    On Error Resume Next
    xTest = ActiveDocument.Variables("StyleExclusions")
    bIsReUse = (Err = 0)
    On Error GoTo ProcError
    
'   get inclusions from Options tab
    xInclusions = UCase(Me.Inclusions)
    
'   get scheme using Word Heading styles and whether to include
    xHScheme = UCase(xHeadingScheme())
    If xHScheme <> "" Then
        bIncludeHScheme = InStr(xInclusions, "," & xHScheme & ",")
    End If
    
    For Each styP In Word.ActiveDocument.Styles
        bOn = False
        With styP
            If .Type = wdStyleTypeParagraph Then
'               get outline level;
'               retrieving outline level will make style "in use",
'               so only do this if style is already in use; use
'               different method to check Word Heading styles,
'               which should be listed in any case
                iLevel = iGetWordHeadingLevel(.NameLocal)
                If iLevel = 0 Then
                    If .InUse Then
                        iLevel = .ParagraphFormat.OutlineLevel
                    Else
                        iLevel = wdOutlineLevelBodyText
                    End If
                End If
                
'               strip alias
                xBase = StripStyleAlias(.NameLocal)
                
                If iLevel <> wdOutlineLevelBodyText Then
'                   it's an outline level style
                    iPos = InStr(UCase(xBase), "_L")
                    If iPos Then
'                       it's a MacPac numbering style
                        xScheme = "ZZMP" & UCase(Left(xBase, iPos - 1))
'                       if scheme uses Word Heading styles, do not list
'                       proprietary style
                        If xScheme = xHScheme Then GoTo labNextSty
                    End If
                    
'                   add style to list
                    xarTemp.Value(iCount, 2) = xBase
                    xarTemp.Value(iCount, 1) = .NameLocal
                                        
'                   default style to ON if TOC 9 style or if level is in designated range
'                   and scheme is on inclusion list
                    If (xBase = Me.cbxScheduleStyles) And _
                            (Me.chkApplyTOC9 = 1) Then
'                       TOC9 Style
                        bOn = True
                    ElseIf (iLevel >= Val(Me.cbxMinLevel)) And _
                            (iLevel <= Val(Me.cbxMaxLevel)) Then
                        If bIsHeadingStyle(.NameLocal) Then
'                           Word Heading style
                            bOn = bIncludeHScheme Or InStr(xInclusions, ",HEADING,")
                        ElseIf iPos Then
'                           MacPac numbering style
                            bOn = InStr(xInclusions, "," & xScheme & ",")
'                        ElseIf bIsScheduleStyle(xBase) Then
''                           Schedule/Exhibit style
'                            bOn = InStr(xInclusions, ",SCHEDULE,")
                        End If
                    End If
                    xarTemp.Value(iCount, 0) = Abs(bOn)
                    
'                   increment count
                    iCount = iCount + 1
                ElseIf bIsScheduleStyle(xBase) Then
'                   schedule/exhibit styles may not be defined as outline level
                    xarTemp.Value(iCount, 2) = xBase
                    xarTemp.Value(iCount, 1) = .NameLocal
                                        
''                   default style to ON if level is in designated range
''                   and scheme is on inclusion list
'                    If (.NameLocal = Me.cbxScheduleStyles) And _
'                            (Me.chkApplyTOC9 = 1) Then
''                       designated for inclusion as TOC 9 - always in range
'                        iLevel = Val(Me.cbxMinLevel)
'                    Else
''                       treat others as level one
'                        iLevel = 1
'                    End If
'
'                    If (iLevel >= Val(Me.cbxMinLevel)) And _
'                            (iLevel <= Val(Me.cbxMaxLevel)) Then
'                        xarTemp.Value(iCount, 0) = Abs(InStr(xInclusions, _
'                            ",SCHEDULE,") <> 0)
'                    End If

'                   default TOC9 style ON, others OFF
                    xarTemp.Value(iCount, 0) = _
                        Abs((xBase = Me.cbxScheduleStyles) And (Me.chkApplyTOC9 = 1))
                    
'                   increment count
                    iCount = iCount + 1
                End If
            End If
        End With
labNextSty:
    Next styP
    
'   dim array to actual size
    Set xarDesignate = New xArray
    xarDesignate.ReDim 0, iCount - 1, 0, 2
    
    For i = 0 To iCount - 1
        xarDesignate.Value(i, 0) = xarTemp.Value(i, 0)
        xarDesignate.Value(i, 1) = xarTemp.Value(i, 1)
        xarDesignate.Value(i, 2) = xarTemp.Value(i, 2)
    Next i
    
'   load list
    Me.lstDesignate.Array = xarDesignate
    Me.lstDesignate.Rebind
    
    m_bRefreshStyleList = False
    Exit Function
ProcError:
    RaiseError "frmInsertTOCTextFrench.xarGetStyles"
    Exit Function
End Function

Private Sub lstStyles_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lRow As Long
    Dim lBkmk As Long
    
    lRow = Me.lstStyles.RowContaining(Y)
    lBkmk = Me.lstStyles.RowBookmark(lRow)
    
    If xarStyles.Value(lBkmk, 0) = 0 Then
        xarStyles.Value(lBkmk, 0) = 1
    Else
        xarStyles.Value(lBkmk, 0) = 0
    End If
    Me.lstStyles.Refresh
    m_bRefreshStyleList = True
End Sub

Private Function SaveCurInput()
    With ActiveDocument.Variables
        .Item("cbxMinLevel") = Me.cbxMinLevel
        .Item("cbxMaxLevel") = Me.cbxMaxLevel
        
        If Me.cbxHeadingDef.SelectedItem = 1 Then
            'period and 2 spaces was old default
            .Item("optCreateFrom") = "0"
        ElseIf Me.cbxHeadingDef.SelectedItem = 2 Then
            'whole paragraph
            .Item("optCreateFrom") = "1"
        Else
            'period and 1 space
            .Item("optCreateFrom") = "3"
        End If
    
        If Me.optInclude(0) Then
            .Item("optInclude") = 0
        Else
            .Item("optInclude") = 1
        End If
        
        .Item("cbxTOCScheme") = Me.cbxTOCScheme.SelectedItem
        .Item("chkApplyManualFormatsToTOC") = Me.chkApplyManualFormatsToTOC
        .Item("chkStyles") = Me.chkStyles
        .Item("chkTCEntries") = Me.chkTCEntries
        .Item("Exclusions") = Me.Exclusions
        .Item("StyleExclusions") = Me.StyleExclusions
        .Item("StyleInclusions") = Me.StyleInclusions
        .Item("cbxScheduleStyles") = Me.cbxScheduleStyles.SelectedItem
        .Item("chkApplyTOC9") = Me.chkApplyTOC9
        .Item("chkHyperlinks") = Me.chkHyperlinks
        .Item("chkTwoColumn") = Me.chkTwoColumn
    End With
End Function

Private Function GetPrevInput()
    Dim xValue As String
    
    With ActiveDocument.Variables
        On Error Resume Next
        Me.cbxMinLevel.SelectedItem = CLng(.Item("cbxMinLevel")) - 1
        Me.cbxMaxLevel.SelectedItem = CLng(.Item("cbxMaxLevel")) - 1
        
        xValue = .Item("optCreateFrom")
        If xValue <> "" Then
            If xValue = "0" Then
                Me.cbxHeadingDef.SelectedItem = 1
            ElseIf xValue = "1" Then
                Me.cbxHeadingDef.SelectedItem = 2
            Else
                Me.cbxHeadingDef.SelectedItem = 0
            End If
        End If
        
        xValue = ""
        xValue = .Item("optInclude")
        If xValue = "0" Then
            Me.optInclude(0) = True
        ElseIf xValue = "1" Then
            Me.optInclude(1) = True
        End If
        
        Me.cbxTOCScheme.SelectedItem = CLng(.Item("cbxTOCScheme"))
        Me.chkApplyManualFormatsToTOC = .Item("chkApplyManualFormatsToTOC")
        Me.chkStyles = .Item("chkStyles")
        Me.chkTCEntries = .Item("chkTCEntries")
        Me.chkTwoColumn = .Item("chkTwoColumn")
        Me.Exclusions = .Item("Exclusions")
        Me.cbxScheduleStyles.SelectedItem = CLng(.Item("cbxScheduleStyles"))
        Me.chkApplyTOC9 = .Item("chkApplyTOC9")
        Me.chkHyperlinks = .Item("chkHyperlinks")
    End With
End Function

Function bIsValid() As Boolean
'returns TRUE if user choices in
'dlg can produce a toc with entries
    Dim xMsg As String
    
    If Me.chkStyles + Me.chkTCEntries = 0 Then
'       at least one must be selected
        xMsg = "La Table des mati�res ne peut pas �tre g�n�r�e selon votre s�lection.  " & _
            vbCr & "Veuillez pr�ciser de cr�er la TM � partir de styles, d'entr�es de table ou les deux."
        MsgBox xMsg, vbExclamation, "Num�rotation MacPac"
        On Error Resume Next
        SSTab1.Tab = 0
        Me.chkStyles.SetFocus
        Exit Function
    ElseIf Me.chkStyles And Me.optInclude(0) And Me.Inclusions = "" Then
'       user specified 'styles', but no styles have been included
        xMsg = "Aucun style n'a �t� s�lectionn� pour �tre ajout� dans la TM.  " & _
               vbCr & "S�lectionnez au moins un th�me parmi les styles dans l'onglet 'Options'."
        MsgBox xMsg, vbExclamation, "Num�rotation MacPac"
        On Error Resume Next
        SSTab1.Tab = 1
        Me.lstStyles.SetFocus
        Exit Function
    ElseIf Me.chkStyles And Me.optInclude(1) And Me.StyleInclusions = "" Then
'       user specified 'styles', but no styles have been included
        xMsg = "Aucun style n'a �t� s�lectionn� pour �tre ajout� dans la TM.  " & _
               vbCr & "S�lectionnez au moins un style dans l'onglet 'Avanc�'."
        MsgBox xMsg, vbExclamation, "Num�rotation MacPac"
        On Error Resume Next
        SSTab1.Tab = 2
        Me.lstDesignate.SetFocus
        Exit Function
    End If
    bIsValid = True
End Function

Private Sub lstDesignate_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lRow As Long
    Dim lBkmk As Long
    
    lRow = Me.lstDesignate.RowContaining(Y)
    lBkmk = Me.lstDesignate.RowBookmark(lRow)
    
    If xarDesignate.Value(lBkmk, 0) = 0 Then
        xarDesignate.Value(lBkmk, 0) = 1
    Else
        xarDesignate.Value(lBkmk, 0) = 0
    End If
    Me.lstDesignate.Refresh
    Me.optInclude(1) = True
End Sub

Private Sub lstDesignate_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim lRow As Long
        
    If KeyCode = vbKeySpace Then
'       toggle chkbox in Schemes list
        lRow = Me.lstDesignate.Row
        If xarDesignate.Value(lRow, 0) = 0 Then
            xarDesignate.Value(lRow, 0) = 1
        Else
            xarDesignate.Value(lRow, 0) = 0
        End If
        Me.lstDesignate.Refresh
        Me.optInclude(1) = True
    End If
End Sub

Public Property Let StyleExclusions(ByVal xNew As String)
'checks off the selected list
    Dim i As Integer

'   cycle through xarray
    With xarDesignate
        For i = 0 To .Count(1) - 1
'           if scheme is in exclusion string, uncheck
            If .Value(i, 2) <> Empty Then
                If InStr(xNew, "," & .Value(i, 2) & ",") Then
                    .Value(i, 0) = 0
                End If
            Else
                Exit For
            End If
        Next i
    End With
    
End Property

Public Property Get StyleExclusions() As String
'returns a string of styles that have been unchecked
    Dim i As Integer
    Dim xTemp As String
    
'   cycle through xarray
    With xarDesignate
        For i = 0 To .Count(1) - 1
'           get if scheme should be excluded
            If .Value(i, 2) <> Empty Then
                If (.Value(i, 0) = 0) Then
'                   if so, add to comma delimited string
                    xTemp = xTemp & .Value(i, 2) & ","
                End If
            Else
                Exit For
            End If
        Next i
    End With
    
    If Len(xTemp) Then
'       if list is not empty, add a preceding ','
        xTemp = "," & xTemp
    End If
    
    StyleExclusions = xTemp
End Property

Public Property Get StyleInclusions() As String
'returns the list of styles to
'use to create toc entries
    Dim i As Integer
    Dim xTemp As String
    
'   cycle through xarray
    With xarDesignate
        For i = 0 To .Count(1) - 1
'           get if scheme should be included
            If .Value(i, 2) <> Empty Then
                If .Value(i, 0) = 1 Then
'                   if so, add to comma delimited string
                    xTemp = xTemp & .Value(i, 2) & ","
                End If
            Else
                Exit For
            End If
        Next i
    End With
    
    If Len(xTemp) Then
'       if list is not empty, add a preceding ','
        xTemp = "," & xTemp
    End If
    
    StyleInclusions = xTemp
End Property

Public Property Let StyleInclusions(ByVal xNew As String)
'checks on the selected list
    Dim i As Integer

'   cycle through xarray
    With xarDesignate
        For i = 0 To .Count(1) - 1
'           if scheme is in inclusion string, check
            If .Value(i, 2) <> Empty Then
                If InStr(xNew, "," & .Value(i, 2) & ",") Then
                    .Value(i, 0) = 1
                End If
            Else
                Exit For
            End If
        Next i
    End With
    
End Property

Private Sub SelectTOC9Style(xTOC9Style As String)
'ensures that TOC 9 style is selected
    Dim i As Integer
    
    With xarDesignate
        For i = 0 To .Count(1) - 1
            If .Value(i, 2) = xTOC9Style Then
                .Value(i, 0) = 1
            End If
        Next i
    End With
    Me.lstDesignate.Rebind
    m_bRefreshStyleList = False
End Sub

Private Sub optTOCStyles_Click(Index As Integer)
    Me.cbxTOCScheme.Enabled = (Index = 0)
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab = 0 Then
        If Me.cbxMinLevel.Enabled Then
            Me.cbxMinLevel.SetFocus
        Else
            Me.chkStyles.SetFocus
        End If
    ElseIf SSTab1.Tab = 1 Then
        If Me.optTOCStyles(0) Then
            Me.optTOCStyles(0).SetFocus
        Else
            Me.optTOCStyles(1).SetFocus
        End If
    Else
        Me.lstDesignate.SetFocus
    End If
    
End Sub

Public Property Get ContinuePageNumbering() As Boolean
    ContinuePageNumbering = m_bContinuePageNumbering
End Property

Public Property Let ContinuePageNumbering(bNew As Boolean)
    m_bContinuePageNumbering = bNew
End Property

