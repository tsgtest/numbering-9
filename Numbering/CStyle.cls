VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CStyle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function EditParagraph(ByVal xStyle As String, ByVal xNextStyle As String) As Boolean
'edits the para props of next para style
'based on user choices in custom form
'returns true if saved
    Dim styWord As Word.Style
    Dim styNextCont As Word.Style
    Dim pfNext As Word.ParagraphFormat
    Dim iAlign As Integer
    Dim sFirstLineIndent As Single
    Dim sLeftIndent As Single
    Dim sRightIndent As Single
    Dim sLineSpace As Single
    Dim iLineSpaceRule As Integer
    Dim iLineSpaceRuleNext As Integer
    Dim sSpaceAfter As Single
    Dim sSpaceBefore As Single
    Dim bWidowCtl As Boolean
    Dim bKeepTogether As Boolean
    Dim bKeepWithNext As Boolean
    Dim bRestoreNext As Boolean
    Dim lUnit As Long
    Dim sIncrement As Single
    Dim iAlignment As Integer
    Dim iLevel As Integer
    Dim iTrailUnderline As Integer
    
    On Error Resume Next
    Set styWord = ActiveDocument.Styles(xStyle)
    On Error GoTo 0
    
    If styWord Is Nothing Then
        If g_bCreateUnlinkedStyles Then
            '9.9.4010
            Set styWord = mdlCC.AddUnlinkedParagraphStyle(ActiveDocument, xStyle)
        Else
            Set styWord = ActiveDocument.Styles.Add(xStyle)
        End If
        styWord.BaseStyle = wdStyleNormal
    End If
    
'   get continue style of subsequent level;
'   xNextStyle = "" when xStyle is last level of scheme
    If xNextStyle <> "" Then
        Set styNextCont = ActiveDocument.Styles(xNextStyle)
        
'       the following conditional is to avoid messing with
'       styles e.g. Normal or Body Text
        If styNextCont.BaseStyle = styWord Then
            bRestoreNext = True
            Set pfNext = styNextCont.ParagraphFormat
            
            With pfNext
'               store current values of next para paraformat
                iAlign = .Alignment
                sFirstLineIndent = .FirstLineIndent
                sLeftIndent = .LeftIndent
                sRightIndent = .RightIndent
                iLineSpaceRuleNext = .LineSpacingRule
                sLineSpace = .LineSpacing
                sSpaceAfter = .SpaceAfter
                sSpaceBefore = .SpaceBefore
                bWidowCtl = .WidowControl
                bKeepWithNext = .KeepWithNext
                bKeepTogether = .KeepTogether
            End With
        End If
    End If
    
'   set spinners to display Word unit of measurement
    lUnit = Word.Options.MeasurementUnit
    sIncrement = GetStandardIncrement(lUnit)
    With frmNextPara
        With .spnLeftIndent
            .DisplayUnit = lUnit
            .IncrementValue = sIncrement
        End With
        With .spnRightIndent
            .DisplayUnit = lUnit
            .IncrementValue = sIncrement
        End With
        With .spnBy
            .DisplayUnit = lUnit
            .IncrementValue = sIncrement
        End With
    End With

'   get whether to honor right aligned styles
    iLevel = Val(Right(xStyle, 1))
    iTrailUnderline = xGetLevelProp(g_oCurScheme.Name, _
                                    iLevel, _
                                    mpNumLevelProp_TrailUnderline, _
                                    mpSchemeType_Document)

'   load current style properties into form
    With styWord.ParagraphFormat
        frmNextPara.bAllowChange = False
        If bBitwisePropIsTrue(iTrailUnderline, _
                mpTrailUnderlineField_AdjustContToNormal) Then
'           base on normal style
            frmNextPara.cmbAlignment.ListIndex = mpBaseAlignmentOnNormal
        Else
            frmNextPara.cmbAlignment.ListIndex = .Alignment
        End If
        If .FirstLineIndent > 0 Then
            frmNextPara.cmbSpecial = "First line"
            frmNextPara.spnLeftIndent.Value = .LeftIndent
        ElseIf .FirstLineIndent < 0 Then
            frmNextPara.cmbSpecial = "Hanging"
            frmNextPara.spnLeftIndent.Value = .LeftIndent + .FirstLineIndent
        Else
            frmNextPara.cmbSpecial = "(none)"
            frmNextPara.spnLeftIndent.Value = .LeftIndent
        End If
        frmNextPara.spnBy.Value = Abs(.FirstLineIndent)
        frmNextPara.spnRightIndent.Value = .RightIndent
        
'        iLineSpaceRule = ConvertLineSpace(.LineSpacingRule, .LineSpacing)
'        If iLineSpaceRule = -1 Then
'            Err.Raise mpError_InvalidSchemePropValue
'        End If
        frmNextPara.cmbLineSpacing.ListIndex = .LineSpacingRule
        If .LineSpacingRule = wdLineSpaceMultiple Then
            frmNextPara.spnAt.Value = .LineSpacing / 12
        Else
            frmNextPara.spnAt.Value = .LineSpacing
        End If
        DoEvents
        If .LineSpacingRule < 3 Then
            frmNextPara.spnAt.DisplayText = ""
        Else
            frmNextPara.spnAt.Refresh
        End If
                    
        frmNextPara.spnSpaceAfter.Value = .SpaceAfter
        frmNextPara.spnSpaceBefore.Value = .SpaceBefore
        frmNextPara.chkWidowOrphan = Abs(.WidowControl)
        frmNextPara.chkKeepWithNext = Abs(.KeepWithNext)
        frmNextPara.chkKeepTogether = Abs(.KeepTogether)
    
'       show form
        With frmNextPara
            .Caption = "Edit " & xStyle & " Style Paragraph"
            .bAllowChange = True
            .Show vbModal
            If Not .bSave Then
                DoEvents
                Application.ScreenUpdating = True
                Unload frmNextPara
                EditParagraph = False
                Exit Function
            End If
        End With
        
        EchoOn
        
'       modify level prop
        If bBitwisePropIsTrue(iTrailUnderline, mpTrailUnderlineField_Underline) Then
            iAlignment = iAlignment Or mpTrailUnderlineField_Underline
        End If
        If bBitwisePropIsTrue(iTrailUnderline, mpTrailUnderlineField_HonorAlignment) Then
            iAlignment = iAlignment Or mpTrailUnderlineField_HonorAlignment
        End If
        If bBitwisePropIsTrue(iTrailUnderline, mpTrailUnderlineField_AdjustToNormal) Then
            iAlignment = iAlignment Or mpTrailUnderlineField_AdjustToNormal
        End If
        If frmNextPara.cmbAlignment.ListIndex = mpBaseAlignmentOnNormal Then
            iAlignment = iAlignment Or mpTrailUnderlineField_AdjustContToNormal
        End If
        lSetLevelProp g_oCurScheme.Name, _
                      iLevel, _
                      mpNumLevelProp_TrailUnderline, _
                      CStr(iAlignment), _
                      mpSchemeType_Document
        
'       edit style
        If frmNextPara.cmbAlignment.ListIndex = mpBaseAlignmentOnNormal Then
            .Alignment = wdAlignParagraphLeft
        Else
            .Alignment = frmNextPara.cmbAlignment.ListIndex
        End If
        
        If frmNextPara.cmbSpecial = "Hanging" Then
            .FirstLineIndent = -(frmNextPara.spnBy.Value)
            .LeftIndent = frmNextPara.spnLeftIndent.Value - _
                .FirstLineIndent
        Else
            .FirstLineIndent = frmNextPara.spnBy.Value
            .LeftIndent = frmNextPara.spnLeftIndent.Value
        End If
        .RightIndent = frmNextPara.spnRightIndent.Value
        iLineSpaceRule = frmNextPara.cmbLineSpacing.ListIndex
        .LineSpacingRule = iLineSpaceRule
        If iLineSpaceRule = wdLineSpaceMultiple Then
            .LineSpacing = LinesToPoints(frmNextPara.spnAt.Value)
        Else
            .LineSpacing = frmNextPara.spnAt.Value
        End If
        .SpaceAfter = frmNextPara.spnSpaceAfter.Value
        .SpaceBefore = frmNextPara.spnSpaceBefore.Value
        .WidowControl = CBool(frmNextPara.chkWidowOrphan)
        .KeepWithNext = CBool(frmNextPara.chkKeepWithNext)
        .KeepTogether = CBool(frmNextPara.chkKeepTogether)
    End With
        
    If bRestoreNext Then
        With pfNext
'           reset values of next para paraformat
            .Alignment = iAlign
            .FirstLineIndent = sFirstLineIndent
            .LeftIndent = sLeftIndent
            .RightIndent = sRightIndent
            .LineSpacingRule = iLineSpaceRuleNext
            .LineSpacing = sLineSpace
            .SpaceAfter = sSpaceAfter
            .SpaceBefore = sSpaceBefore
            .WidowControl = bWidowCtl
            .KeepWithNext = bKeepWithNext
            .KeepTogether = bKeepTogether
        End With
    End If
    
    DoEvents
    EchoOff
    Application.ScreenUpdating = True
    Unload frmNextPara
    EditParagraph = True

End Function

