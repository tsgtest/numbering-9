VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmSchemeProfile 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Scheme Profile"
   ClientHeight    =   4365
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6090
   Icon            =   "frmSchemeProfile.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4365
   ScaleWidth      =   6090
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   385
      Left            =   4755
      TabIndex        =   7
      Top             =   3870
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   385
      Left            =   3555
      TabIndex        =   6
      Top             =   3870
      Width           =   1100
   End
   Begin VB.TextBox txtDescription 
      Height          =   1365
      Left            =   1200
      MaxLength       =   255
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   1155
      Width           =   4600
   End
   Begin VB.TextBox txtPostedBy 
      Height          =   315
      Left            =   1200
      TabIndex        =   5
      Top             =   2670
      Width           =   4600
   End
   Begin TrueDBList60.TDBCombo cbxCategories 
      Height          =   315
      Left            =   1200
      OleObjectBlob   =   "frmSchemeProfile.frx":058A
      TabIndex        =   1
      Top             =   675
      Width           =   4600
   End
   Begin VB.Label lblPostedOnText 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "###"
      Height          =   315
      Left            =   1200
      TabIndex        =   11
      Top             =   3165
      Width           =   4605
   End
   Begin VB.Label lblPostedOn 
      Caption         =   "Posted On:"
      Height          =   240
      Left            =   135
      TabIndex        =   10
      Top             =   3210
      Width           =   900
   End
   Begin VB.Label Label1 
      Caption         =   "&Category:"
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   930
   End
   Begin VB.Label lblDescription 
      Caption         =   "&Description:"
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   1170
      Width           =   930
   End
   Begin VB.Label lblPostedBy 
      Caption         =   "&Posted By:"
      Height          =   240
      Left            =   135
      TabIndex        =   4
      Top             =   2730
      Width           =   900
   End
   Begin VB.Label lblSchemeText 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "###"
      Height          =   315
      Left            =   1200
      TabIndex        =   9
      Top             =   225
      Width           =   4605
   End
   Begin VB.Label lblScheme 
      Caption         =   "Name:"
      Height          =   270
      Left            =   120
      TabIndex        =   8
      Top             =   270
      Width           =   840
   End
End
Attribute VB_Name = "frmSchemeProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    Me.Hide
    Me.Cancelled = True
End Sub

Private Sub btnOK_Click()
    If Me.ActiveControl.Name = "txtDescription" Then _
        Exit Sub
    If bProfileIsValid() Then
        Me.Hide
        DoEvents
    End If
End Sub

Private Sub cbxCategories_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxCategories, Reposition
    Exit Sub
ProcError:
    RaiseError "frmSchemeProfile.cbxCategories_Mismatch"
    Exit Sub
End Sub

Private Sub Form_Activate()
    '9.9.5001 - limit newly exported schemes to 120 characters
    If Not Me.txtDescription.Locked Then
        Me.txtDescription.MaxLength = 120
        Me.txtDescription.Height = Me.txtDescription.Height - 600
        Me.lblPostedBy.Top = Me.lblPostedBy.Top - 600
        Me.txtPostedBy.Top = Me.txtPostedBy.Top - 600
        Me.lblPostedOn.Top = Me.lblPostedOn.Top - 600
        Me.lblPostedOnText.Top = Me.lblPostedOnText.Top - 600
        Me.btnCancel.Top = Me.btnCancel.Top - 600
        Me.btnOK.Top = Me.btnOK.Top - 600
        Me.Height = Me.Height - 600
    End If
End Sub

Private Sub Form_Load()
    On Error GoTo ProcError
    
    Me.Cancelled = False
    
'   get shared scheme categories
    GetPublicCategories
    
'   set user name and posted date
    Me.txtPostedBy = Word.Application.UserName
    Me.lblPostedOnText.Caption = Format(Now, "mmmm d, yyyy")
    
    Exit Sub
ProcError:
    RaiseError "frmSchemeProfile.Form_Load"
    Exit Sub
End Sub

Private Function bProfileIsValid() As Boolean
    Dim xFileExport As String
    Dim xFile As String
    Dim xMsg As String
    Dim iUserChoice As Integer
    
    If Me.txtPostedBy = "" Then
        xMsg = "Posted By is required information."
        Me.txtPostedBy.SetFocus
        MsgBox xMsg, vbExclamation, g_xAppName
        Exit Function
    End If
    
'   check for existence of scheme by same name
    xFileExport = Me.lblSchemeText & mpFilePrefixSep
    xFileExport = xSubstitute(xFileExport, "/", "=")
    xFile = Dir(PublicSchemesDir() & Me.cbxCategories & "\" & "*.mpn")
    While xFile <> ""
'       scheme already exists if the left chars match
'       the prefix of the proposed export file
        If Left(xFile, Len(xFileExport)) = xFileExport Then
            xMsg = "A shared scheme with this name " & _
                   "already exists.  If you " & _
                   "continue this operation, " & vbCr & "the shared " & _
                   "schemes folder will display " & _
                   "multiple schemes with this name.  " & vbCr & "Do " & _
                   "you want to continue?"
            iUserChoice = MsgBox(xMsg, vbQuestion + vbYesNo, g_xAppName)
            bProfileIsValid = (iUserChoice = vbYes)
            Exit Function
        End If
        xFile = Dir()
    Wend
    bProfileIsValid = True
End Function

Private Function GetPublicCategories()
'   adds all scheme categories to tree
    Dim xDir As String
    Dim xCategories As String
    Dim aCategories As Variant
    Dim xarCategories As xArray
    Dim i As Integer
    
'   cycle through all dirs in shared scheme dir
    xDir = Dir(PublicSchemesDir() & "*.*", vbDirectory)
    While xDir <> ""
        If (xDir <> ".") And (xDir <> "..") Then
'           add
            xCategories = xCategories & xDir & "|"
        End If
        xDir = Dir()
    Wend
    If xCategories <> "" Then
        xCategories = Left$(xCategories, Len(xCategories) - 1)
        aCategories = Split(xCategories, "|")
        Set xarCategories = New xArray
        xarCategories.ReDim 0, UBound(aCategories), 0, 1
        For i = 0 To UBound(aCategories)
            xarCategories(i, 0) = aCategories(i)
            xarCategories(i, 1) = aCategories(i)
        Next i
        Me.cbxCategories.Array = xarCategories
        ResizeTDBCombo Me.cbxCategories, 4
        Me.cbxCategories.Text = aCategories(0)
    Else
        Err.Raise mpError_NoSharedSchemeCategories
    End If
End Function
Private Sub txtDescription_GotFocus()
    bEnsureSelectedContent txtDescription, True
End Sub
Private Sub txtDescription_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then _
        KeyAscii = 0
End Sub
Private Sub txtPostedBy_GotFocus()
    bEnsureSelectedContent txtPostedBy, True
End Sub
