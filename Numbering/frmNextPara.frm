VERSION 5.00
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Begin VB.Form frmNextPara 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Edit Next Paragraph Style"
   ClientHeight    =   4905
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6000
   Icon            =   "frmNextPara.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4905
   ScaleWidth      =   6000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin mpControls3.SpinTextInternational spnLeftIndent 
      Height          =   330
      Left            =   1725
      TabIndex        =   3
      Top             =   1050
      Width           =   1300
      _ExtentX        =   2302
      _ExtentY        =   582
      Appearance      =   1
      IncrementValue  =   0.1
      MinValue        =   -576
      MaxValue        =   576
      AppendSymbol    =   -1  'True
      DisplayUnit     =   0
   End
   Begin VB.ComboBox cmbAlignment 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmNextPara.frx":058A
      Left            =   1725
      List            =   "frmNextPara.frx":059D
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   165
      Width           =   2340
   End
   Begin VB.ComboBox cmbSpecial 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmNextPara.frx":05DB
      Left            =   4200
      List            =   "frmNextPara.frx":05E8
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   1050
      Width           =   1300
   End
   Begin VB.ComboBox cmbLineSpacing 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmNextPara.frx":0609
      Left            =   4470
      List            =   "frmNextPara.frx":061F
      Style           =   2  'Dropdown List
      TabIndex        =   15
      Top             =   2265
      Width           =   1035
   End
   Begin VB.CheckBox chkWidowOrphan 
      Caption         =   "&Widow/Orphan Control"
      Height          =   255
      Left            =   1170
      TabIndex        =   18
      Top             =   3465
      Width           =   2040
   End
   Begin VB.CheckBox chkKeepWithNext 
      Caption         =   "Keep With Ne&xt"
      Height          =   255
      Left            =   3870
      TabIndex        =   20
      Top             =   3465
      Width           =   1635
   End
   Begin VB.CheckBox chkKeepTogether 
      Caption         =   "Keep Lines &Together"
      Height          =   255
      Left            =   1170
      TabIndex        =   19
      Top             =   3765
      Width           =   1950
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "O&K"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   3570
      TabIndex        =   21
      Top             =   4380
      Width           =   1100
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   4710
      TabIndex        =   22
      Top             =   4380
      Width           =   1100
   End
   Begin mpControls3.SpinTextInternational spnRightIndent 
      Height          =   330
      Left            =   1725
      TabIndex        =   5
      Top             =   1500
      Width           =   1300
      _ExtentX        =   2302
      _ExtentY        =   582
      Appearance      =   1
      IncrementValue  =   0.1
      MinValue        =   -576
      MaxValue        =   576
      AppendSymbol    =   -1  'True
      DisplayUnit     =   0
   End
   Begin mpControls3.SpinTextInternational spnSpaceBefore 
      Height          =   330
      Left            =   1725
      TabIndex        =   11
      Top             =   2220
      Width           =   1300
      _ExtentX        =   2302
      _ExtentY        =   582
      Appearance      =   1
      IncrementValue  =   6
      MaxValue        =   1584
      AppendSymbol    =   -1  'True
   End
   Begin mpControls3.SpinTextInternational spnSpaceAfter 
      Height          =   330
      Left            =   1725
      TabIndex        =   13
      Top             =   2670
      Width           =   1300
      _ExtentX        =   2302
      _ExtentY        =   582
      Appearance      =   1
      IncrementValue  =   6
      MaxValue        =   1584
      AppendSymbol    =   -1  'True
   End
   Begin mpControls3.SpinTextInternational spnBy 
      Height          =   330
      Left            =   4200
      TabIndex        =   9
      Top             =   1500
      Width           =   1300
      _ExtentX        =   2302
      _ExtentY        =   582
      Appearance      =   1
      IncrementValue  =   0.1
      MaxValue        =   576
      AppendSymbol    =   -1  'True
      DisplayUnit     =   0
   End
   Begin mpControls3.SpinTextInternational spnAt 
      Height          =   330
      Left            =   4470
      TabIndex        =   17
      Top             =   2670
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   582
      Appearance      =   1
      IncrementValue  =   6
      MaxValue        =   1584
      AppendSymbol    =   -1  'True
   End
   Begin VB.Label lblAt 
      Alignment       =   1  'Right Justify
      Caption         =   "&At:"
      Height          =   240
      Left            =   3350
      TabIndex        =   16
      Top             =   2700
      Width           =   1000
   End
   Begin VB.Label lblSpaceAfter 
      Alignment       =   1  'Right Justify
      Caption         =   "&After:"
      Height          =   240
      Left            =   1040
      TabIndex        =   12
      Top             =   2700
      Width           =   600
   End
   Begin VB.Label lblSpaceBefore 
      Alignment       =   1  'Right Justify
      Caption         =   "&Before:"
      Height          =   240
      Left            =   1040
      TabIndex        =   10
      Top             =   2280
      Width           =   600
   End
   Begin VB.Line Line7 
      BorderColor     =   &H00808080&
      X1              =   195
      X2              =   5795
      Y1              =   4170
      Y2              =   4170
   End
   Begin VB.Line Line8 
      BorderColor     =   &H00FFFFFF&
      X1              =   195
      X2              =   5795
      Y1              =   4185
      Y2              =   4185
   End
   Begin VB.Line Line3 
      BorderColor     =   &H00FFFFFF&
      X1              =   1125
      X2              =   5790
      Y1              =   3225
      Y2              =   3225
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00808080&
      X1              =   1110
      X2              =   5790
      Y1              =   3210
      Y2              =   3210
   End
   Begin VB.Line Line6 
      BorderColor     =   &H00FFFFFF&
      X1              =   1110
      X2              =   5790
      Y1              =   1995
      Y2              =   1995
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00808080&
      X1              =   1095
      X2              =   5790
      Y1              =   1980
      Y2              =   1980
   End
   Begin VB.Line Line4 
      BorderColor     =   &H00FFFFFF&
      X1              =   1155
      X2              =   5790
      Y1              =   825
      Y2              =   825
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00808080&
      X1              =   1140
      X2              =   5790
      Y1              =   810
      Y2              =   810
   End
   Begin VB.Label lblAlignment 
      Caption         =   "Ali&gnment:"
      Height          =   270
      Left            =   270
      TabIndex        =   0
      Top             =   210
      Width           =   810
   End
   Begin VB.Label lblIndentation 
      Caption         =   "Indentation"
      Height          =   240
      Left            =   255
      TabIndex        =   25
      Top             =   705
      Width           =   1020
   End
   Begin VB.Label lblLeftIndent 
      Alignment       =   1  'Right Justify
      Caption         =   "&Left:"
      Height          =   315
      Left            =   1040
      TabIndex        =   2
      Top             =   1110
      Width           =   600
   End
   Begin VB.Label lblRightIndent 
      Alignment       =   1  'Right Justify
      Caption         =   "&Right:"
      Height          =   330
      Left            =   1040
      TabIndex        =   4
      Top             =   1560
      Width           =   600
   End
   Begin VB.Label lblSpecial 
      Alignment       =   1  'Right Justify
      Caption         =   "&Special:"
      Height          =   210
      Left            =   3480
      TabIndex        =   6
      Top             =   1110
      Width           =   600
   End
   Begin VB.Label lblBy 
      Alignment       =   1  'Right Justify
      Caption         =   "B&y:"
      Height          =   210
      Left            =   3480
      TabIndex        =   8
      Top             =   1560
      Width           =   600
   End
   Begin VB.Label lblSpacing 
      Caption         =   "Spacing"
      Height          =   330
      Left            =   255
      TabIndex        =   24
      Top             =   1875
      Width           =   675
   End
   Begin VB.Label lblLineSpacing 
      Alignment       =   1  'Right Justify
      Caption         =   "Li&ne Spacing:"
      Height          =   270
      Left            =   3350
      TabIndex        =   14
      Top             =   2325
      Width           =   1000
   End
   Begin VB.Label lblPagination 
      Caption         =   "Pagination"
      Height          =   315
      Left            =   255
      TabIndex        =   23
      Top             =   3105
      Width           =   885
   End
End
Attribute VB_Name = "frmNextPara"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bSave As Boolean
Private m_bAllowChange As Boolean

Private Sub cmbLineSpacing_Click()
    Dim iSpacing As WdLineSpacing

    iSpacing = Me.cmbLineSpacing.ListIndex
    Me.lblAt.Enabled = (iSpacing > 2)
    With Me.spnAt
        .Enabled = (iSpacing > 2)
        .AppendSymbol = (iSpacing <> wdLineSpaceMultiple)
        If iSpacing = wdLineSpaceMultiple Then
            .MinValue = 0.5
            .IncrementValue = 0.5
        Else
            .MinValue = 1
            .IncrementValue = 1
        End If
        If m_bAllowChange Then
            Select Case iSpacing
                Case wdLineSpaceSingle, wdLineSpaceAtLeast, wdLineSpaceExactly
                    .Value = 12
                Case wdLineSpace1pt5
                    .Value = 18
                Case wdLineSpaceDouble
                    .Value = 24
                Case wdLineSpaceMultiple
                    .Value = 3
            End Select
            If iSpacing < 3 Then
                .DisplayText = ""
            Else
                .Refresh
            End If
        End If
    End With
End Sub

Private Sub cmbSpecial_Click()
    If m_bAllowChange Then
        m_bAllowChange = False
        If cmbSpecial = "(none)" Then
            Me.spnBy.Value = 0
        ElseIf Me.spnBy.Value = 0 Then
            Me.spnBy.Value = 36
        End If
        m_bAllowChange = True
    End If
End Sub

Private Sub cmdCancel_Click()
    Me.Hide
    DoEvents
End Sub

Private Sub cmdSave_Click()
    m_bSave = True
    Me.Hide
    DoEvents
End Sub

Property Get bSave() As Boolean
    bSave = m_bSave
End Property

Property Let bAllowChange(bAllowChange As Boolean)
    m_bAllowChange = bAllowChange
End Property

Private Sub Form_Load()
    m_bSave = False
End Sub

Private Sub spnBy_Change()
    If m_bAllowChange Then
        m_bAllowChange = False
        If Me.cmbSpecial = "(none)" And _
                Me.spnBy.DisplayText <> "" Then
            Me.cmbSpecial = "First line"
        End If
        m_bAllowChange = True
    End If
End Sub

Private Sub spnBy_SpinUp()
    m_bAllowChange = False
    If Me.cmbSpecial = "(none)" Then _
        Me.cmbSpecial = "First line"
    m_bAllowChange = True
End Sub

Private Sub spnRightIndent_Validate(Cancel As Boolean)
    If Not Me.spnRightIndent.IsValid Then _
        Cancel = True
End Sub

Private Sub spnLeftIndent_Validate(Cancel As Boolean)
    If Not Me.spnLeftIndent.IsValid Then _
        Cancel = True
End Sub

Private Sub spnSpaceBefore_Validate(Cancel As Boolean)
    If Not Me.spnSpaceBefore.IsValid Then _
        Cancel = True
End Sub

Private Sub spnSpaceAfter_Validate(Cancel As Boolean)
    If Not Me.spnSpaceAfter.IsValid Then _
        Cancel = True
End Sub

Private Sub spnBy_Validate(Cancel As Boolean)
    If Not Me.spnBy.IsValid Then _
        Cancel = True
End Sub

Private Sub spnAt_Validate(Cancel As Boolean)
    If Not Me.spnAt.IsValid Then _
        Cancel = True
End Sub


