VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.Form frmEditTOCScheme 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edit TOC Scheme"
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6060
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   6060
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cmbScheme 
      Height          =   315
      Left            =   750
      TabIndex        =   44
      Text            =   "Combo1"
      Top             =   75
      Width           =   2025
   End
   Begin VB.Frame frmTOC 
      BorderStyle     =   0  'None
      Height          =   4080
      Left            =   75
      TabIndex        =   14
      Top             =   450
      Width           =   6000
      Begin VB.Frame fraTOCParaFormat 
         Caption         =   "Paragraph"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3090
         Left            =   30
         TabIndex        =   21
         Top             =   45
         Width           =   5880
         Begin VB.ComboBox cmbTOCLineSpacing 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmEditTOCScheme.frx":0000
            Left            =   150
            List            =   "frmEditTOCScheme.frx":000D
            Style           =   2  'Dropdown List
            TabIndex        =   29
            Top             =   1160
            Width           =   2570
         End
         Begin VB.ComboBox cmbTOCTextAlign 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmEditTOCScheme.frx":002C
            Left            =   150
            List            =   "frmEditTOCScheme.frx":0036
            Style           =   2  'Dropdown List
            TabIndex        =   28
            Top             =   480
            Width           =   2570
         End
         Begin VB.TextBox txtTOCLeftIndent 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   3760
            TabIndex        =   27
            Top             =   480
            Width           =   940
         End
         Begin VB.CheckBox chkIncludePageNumbers 
            Caption         =   "&Include Page Numbers"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   105
            TabIndex        =   26
            Top             =   2565
            Width           =   2010
         End
         Begin VB.TextBox txtTOCFirstLineIndent 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   3760
            TabIndex        =   25
            Top             =   1160
            Width           =   940
         End
         Begin VB.TextBox txtSpaceBetweenEntries 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2800
            TabIndex        =   24
            Top             =   1650
            Width           =   500
         End
         Begin VB.TextBox txtSpaceBeforeEntries 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2800
            TabIndex        =   23
            Top             =   2090
            Width           =   500
         End
         Begin VB.CheckBox chkDotLeaders 
            Caption         =   "Use D&ot Leaders"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2300
            TabIndex        =   22
            Top             =   2565
            Width           =   1815
         End
         Begin ComCtl2.UpDown udTOCLeftIndent 
            Height          =   315
            Left            =   4710
            TabIndex        =   30
            Tag             =   ".1"
            Top             =   480
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   556
            _Version        =   327681
            BuddyControl    =   "udTOCLeftIndent"
            BuddyDispid     =   196643
            OrigLeft        =   4725
            OrigTop         =   480
            OrigRight       =   4965
            OrigBottom      =   795
            Max             =   8
            Enabled         =   -1  'True
         End
         Begin ComCtl2.UpDown udTOCFirstLineIndent 
            Height          =   315
            Left            =   4710
            TabIndex        =   31
            Tag             =   ".1"
            Top             =   1155
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   556
            _Version        =   327681
            BuddyControl    =   "cmbTOCTextAlign"
            BuddyDispid     =   196613
            OrigLeft        =   4725
            OrigTop         =   1160
            OrigRight       =   4965
            OrigBottom      =   1475
            Max             =   8
            Enabled         =   -1  'True
         End
         Begin ComCtl2.UpDown udSpaceBetweenEntries 
            Height          =   315
            Left            =   3300
            TabIndex        =   32
            Tag             =   "1"
            Top             =   1650
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   556
            _Version        =   327681
            BuddyControl    =   "txtTOCLeftIndent"
            BuddyDispid     =   196614
            OrigLeft        =   3315
            OrigTop         =   1650
            OrigRight       =   3555
            OrigBottom      =   1965
            Max             =   48
            Enabled         =   -1  'True
         End
         Begin ComCtl2.UpDown udSpaceBeforeEntries 
            Height          =   315
            Left            =   3300
            TabIndex        =   33
            Tag             =   "1"
            Top             =   2085
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   556
            _Version        =   327681
            BuddyControl    =   "chkIncludePageNumbers"
            BuddyDispid     =   196615
            OrigLeft        =   3315
            OrigTop         =   2090
            OrigRight       =   3555
            OrigBottom      =   2405
            Max             =   48
            Enabled         =   -1  'True
         End
         Begin VB.Label lblTOCLeftIndentIn 
            Caption         =   "inches"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   5025
            TabIndex        =   43
            Top             =   525
            Width           =   555
         End
         Begin VB.Label lblTOCLineSpacing 
            Caption         =   "Line &Spacing:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   150
            TabIndex        =   42
            Top             =   945
            Width           =   1395
         End
         Begin VB.Label lblTOCTextAlign 
            Caption         =   "Ali&gnment:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   150
            TabIndex        =   41
            Top             =   270
            Width           =   840
         End
         Begin VB.Label lblTOCLeftIndent 
            Caption         =   "&Left Indent:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   3790
            TabIndex        =   40
            Top             =   270
            Width           =   915
         End
         Begin VB.Label lblTOCFirstLineIndentIn 
            Caption         =   "inches"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   5025
            TabIndex        =   39
            Top             =   1200
            Width           =   660
         End
         Begin VB.Label lblTOCFirstLineIndent 
            Caption         =   "&Hanging By:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   3790
            TabIndex        =   38
            Top             =   945
            Width           =   1575
         End
         Begin VB.Label Label7 
            Caption         =   "Space bet&ween entries of this level:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   37
            Top             =   1708
            Width           =   2715
         End
         Begin VB.Label Label8 
            Caption         =   "Space be&fore entries of this level:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   36
            Top             =   2151
            Width           =   2500
         End
         Begin VB.Label Label3 
            Caption         =   "pts"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3645
            TabIndex        =   35
            Top             =   1710
            Width           =   495
         End
         Begin VB.Label Label4 
            Caption         =   "pts"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3645
            TabIndex        =   34
            Top             =   2130
            Width           =   495
         End
      End
      Begin VB.Frame fraTOCFontFormat 
         Caption         =   "Font"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   720
         Left            =   30
         TabIndex        =   15
         Top             =   3225
         Width           =   5880
         Begin VB.CheckBox chkTOCItalic 
            Caption         =   "&Italic"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   250
            Left            =   1173
            TabIndex        =   20
            Top             =   315
            Width           =   675
         End
         Begin VB.CheckBox chkTOCAllCaps 
            Caption         =   "All &Caps"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   250
            Left            =   4680
            TabIndex        =   19
            Top             =   315
            Width           =   945
         End
         Begin VB.CheckBox chkTOCBold 
            Caption         =   "&Bold"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   250
            Left            =   195
            TabIndex        =   18
            Top             =   315
            Width           =   735
         End
         Begin VB.CheckBox chkTOCUnderline 
            Caption         =   "&Underline"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   250
            Left            =   2091
            TabIndex        =   17
            Top             =   315
            Width           =   1000
         End
         Begin VB.CheckBox chkTOCSmallCaps 
            Caption         =   "S&mall Caps"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   250
            Left            =   3334
            TabIndex        =   16
            Top             =   315
            Width           =   1100
         End
      End
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&9"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   8
      Left            =   5610
      Style           =   1  'Graphical
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   60
      Width           =   335
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&8"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   7
      Left            =   5280
      Style           =   1  'Graphical
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   60
      Width           =   335
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&7"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   6
      Left            =   4950
      Style           =   1  'Graphical
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   60
      Width           =   335
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&6"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   5
      Left            =   4620
      Style           =   1  'Graphical
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   60
      Width           =   335
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   4
      Left            =   4290
      Style           =   1  'Graphical
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   60
      Width           =   335
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   3
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   60
      Width           =   335
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   2
      Left            =   3630
      Style           =   1  'Graphical
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   60
      Width           =   335
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   1
      Left            =   3300
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   60
      Width           =   335
   End
   Begin VB.CheckBox chkLevel 
      Caption         =   "&1"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Index           =   0
      Left            =   2970
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   60
      Value           =   1  'Checked
      Width           =   335
   End
   Begin VB.ComboBox cmbLevel 
      Height          =   315
      ItemData        =   "frmEditTOCScheme.frx":004A
      Left            =   60
      List            =   "frmEditTOCScheme.frx":004C
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   5445
      Visible         =   0   'False
      Width           =   750
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   375
      TabIndex        =   0
      Top             =   5040
      Width           =   3210
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Sa&ve"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   3705
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   4575
      Width           =   1100
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   4845
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   4575
      Width           =   1100
   End
   Begin VB.Label Label2 
      Caption         =   "&Scheme:"
      Height          =   285
      Left            =   75
      TabIndex        =   45
      Top             =   105
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "&Level:"
      Height          =   315
      Left            =   405
      TabIndex        =   3
      Top             =   5025
      Visible         =   0   'False
      Width           =   585
   End
End
Attribute VB_Name = "frmEditTOCScheme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_xCode As String
Private xUserAll As String
Private bRestore As Boolean
Private bFormInit As Boolean
Private bNoChangeEvent As Boolean
Private bNoSelChangeEvent As Boolean
Private m_bIsPrevLevels As Boolean
Private m_iSelStart As Integer
Private xScheme As String
Private bLevelChanged As Boolean
Private bLevelIsDirty As Boolean
Private bTCIsDirty As Boolean
Private bTOCIsDirty As Boolean
Private bDocPropIsDirty As Boolean
Private bParaIsDirty As Boolean
Private bNextParaIsDirty As Boolean
Private iLastSelectedLevel As Integer
Private m_bSelChangeRunning As Boolean

'these are exposed as properties
Private m_iInitialLevels As Integer
Private m_iCurrentLevels As Integer
Private m_bSave As Boolean
Private m_xarIsDirty As xArray


Private Sub chkDotLeaders_Click()
    bTOCIsDirty = True
End Sub

Private Sub chkIncludePageNumbers_Click()
    bDocPropIsDirty = True
End Sub

Private Sub chkLegalStyle_Click()
    bLevelIsDirty = True
End Sub

Private Sub chkLevel_Click(Index As Integer)
    Dim i As Integer
    
'   select first control on current tab
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            Me.cmbNumberStyle.SetFocus
        Case 1
            Me.cmbTextAlign.SetFocus
        Case 2
            Me.cmbTOCTextAlign.SetFocus
    End Select
    
    If Me.chkLevel.Item(Index).Value = vbChecked Then
'       uncheck all buttons other than the one clicked
        For i = 0 To m_iCurrentLevels - 1
            With Me.chkLevel.Item(i)
                If i <> Index Then
                    .Value = vbUnchecked
                    .Enabled = True
                Else
                    .Enabled = False
                End If
            End With
        Next
        
        DoEvents
        
'       set cmblevel to the clicked level -
'       cmblevel exists because these chkboxes
'       were added after the app was almost
'       completely built.
        Me.cmbLevel.ListIndex = Index
    End If
End Sub

Private Sub chkNumberBold_Click()
    bLevelIsDirty = True
End Sub

Private Sub chkNumberCaps_Click()
    bLevelIsDirty = True
End Sub

Private Sub chkNumberItalic_Click()
    bLevelIsDirty = True
End Sub

Private Sub chkNumberUnderline_Click()
    bLevelIsDirty = True
    If Me.rdStyle Then _
        Me.chkTrailingUnderline = Me.chkNumberUnderline
End Sub

Private Sub chkPreviousLevels_Click()
'   fill array if invisible and array is empty
    If (Not Me.lstPrevLevels.Visible) And _
            (m_xarPrevLevels.Count(1) = 0) Then
        FillPrevLevelsList xScheme
    End If
    
    Me.lstPrevLevels.Visible = (Me.chkPreviousLevels.Value = vbChecked)
    Me.lblPreviousLevels.Visible = (Me.chkPreviousLevels.Value = vbChecked)
    
End Sub

Private Sub chkReset_Click()
    bLevelIsDirty = True
End Sub

Private Sub chkRightAlign_Click()
    bLevelIsDirty = True
End Sub

Private Sub chkTCBold_Click()
    bTCIsDirty = True
    If Me.rdStyle Then _
        bParaIsDirty = True
End Sub

Private Sub chkTCCaps_Click()
    bTCIsDirty = True
    If Me.rdStyle Then _
        bParaIsDirty = True
    If chkTCCaps = 1 Then _
        chkTCSmallCaps = 0
End Sub

Private Sub chkTCItalic_Click()
    bTCIsDirty = True
    If Me.rdStyle Then _
        bParaIsDirty = True
End Sub

Private Sub chkTCSmallCaps_Click()
    bTCIsDirty = True
    If Me.rdStyle Then _
        bParaIsDirty = True
    If chkTCSmallCaps = 1 Then _
        chkTCCaps = 0
End Sub

Private Sub chkTCUnderline_Click()
    bTCIsDirty = True
    If Me.rdStyle Then _
        bParaIsDirty = True
End Sub

Private Sub chkTOCAllCaps_Click()
    bTOCIsDirty = True
    If Me.chkTOCAllCaps = 1 Then _
        Me.chkTOCSmallCaps = 0
End Sub

Private Sub chkTOCBold_Click()
    bTOCIsDirty = True
End Sub

Private Sub chkTOCItalic_Click()
    bTOCIsDirty = True
End Sub

Private Sub chkTOCSmallCaps_Click()
    bTOCIsDirty = True
    If Me.chkTOCSmallCaps = 1 Then _
        Me.chkTOCAllCaps = 0
End Sub

Private Sub chkTOCUnderline_Click()
    bTOCIsDirty = True
End Sub

Private Sub chkTrailingUnderline_Click()
    bDocPropIsDirty = True
End Sub

Private Sub cmbLevel_Click()
    Dim i As Integer
    Dim iPrevLevel As Integer
    Dim xNum As String
    Dim xNumFormat As String
    Dim bIsDirty As Boolean
    
    If cmbLevel = iLastSelectedLevel Then _
        Exit Sub
        
    bNoChangeEvent = True
    
    Application.ScreenUpdating = False
    
'   create level based on user choices
    bIsDirty = ((bLevelIsDirty + bTCIsDirty + bParaIsDirty + _
        bDocPropIsDirty + bNextParaIsDirty + bTOCIsDirty) < 0)
    If (Not bFormInit) And bIsDirty Then
'       reformat rtf value
        xNumFormat = xParseNumberFormat(m_xCode, _
            Me.rtfNumberFormat.Text)
        
'       implement changes
        bRet = bCreateLevel(Me, _
                            xScheme, _
                            iLastSelectedLevel, _
                            xNumFormat, _
                            bLevelIsDirty, _
                            bTCIsDirty, _
                            bTOCIsDirty, _
                            bDocPropIsDirty, _
                            bParaIsDirty)
        
'       update dirty flag array
        bRet = bUpdateDirtyFlagArray(iLastSelectedLevel, _
                                     bLevelIsDirty, _
                                     bTCIsDirty, _
                                     bTOCIsDirty, _
                                     bDocPropIsDirty, _
                                     bParaIsDirty, _
                                     bNextParaIsDirty)
    End If
    
'   load properties of level
    lRet = lLoadLevelProperties
        
'   fill previous level list if it's visible
    With Me.lstPrevLevels
        If .Visible Then
            FillPrevLevelsList xScheme
        Else
'           clear out by redimming as empty array
            m_xarPrevLevels.ReDim 0, -1, 0, 1
            .Rebind
        End If
    End With
    
'   disable previous level checkbox in level one
    If Me.cmbLevel = 1 Then
        Me.chkPreviousLevels = 0
        Me.chkPreviousLevels.Enabled = False
    Else
'       also disable when number format
'       textbox is disabled, e.g. bullet style
        Me.chkPreviousLevels.Enabled = (Me.cmbNumberStyle <> "Bullet")
    End If
    
    On Error Resume Next
    If (Not bFormInit) And bIsDirty Then
        oPreview.RefreshPreviewLevel xScheme, iLastSelectedLevel
    End If
    oPreview.HighlightLevel Me.cmbLevel
    
'   reset flags
    bNoChangeEvent = False
    bLevelIsDirty = False
    bTCIsDirty = False
    bTOCIsDirty = False
    bDocPropIsDirty = False
    bParaIsDirty = False
    bNextParaIsDirty = False
    
'   set as last selected level
    iLastSelectedLevel = cmbLevel
End Sub

Private Sub cmbLineSpacing_Click()
    bParaIsDirty = True
End Sub

Private Sub cmbNextParagraph_Click()
    Dim bEnabled As Boolean
    
    bParaIsDirty = True
    
'   allow only scheme-specific next para styles to be edited
    bEnabled = InStr(Me.cmbNextParagraph.Text, xScheme & " Cont ")
    Me.cmdEditNextParaPara.Enabled = bEnabled
End Sub

Private Sub cmbNumberStyle_Click()
    Dim iNumberStyle As Integer
    Dim iLevel As Integer
    Dim iStartAt As Integer
    Dim iPos As Integer
    Dim iOldNumLength As Integer
    Dim xNewNumber As String
    
    bLevelIsDirty = True
    iNumberStyle = cmbNumberStyle.ListIndex
    iLevel = Me.cmbLevel
        
'   fill StartAt
    If bLevelChanged Then
        iStartAt = ActiveDocument.ListTemplates(xScheme) _
            .ListLevels(iLevel).StartAt
    Else
        iStartAt = 1
    End If
    udStartAt.Value = iStartAt
    Select Case iNumberStyle
        Case mpListNumberStyleNone, mpListNumberStyleBullet
            Me.lblStartAtValue.Caption = ""
            Me.udStartAt.Enabled = False
        Case mpListNumberStyleOrdinal, mpListNumberStyleOrdinalText, _
                mpListNumberStyleCardinalText
            Me.lblStartAtValue.Caption = xIntToListNumStyle(iStartAt, _
                wdListNumberStyleArabic) & Space(1)
            Me.udStartAt.Enabled = True
        Case Else
            Me.lblStartAtValue.Caption = xIntToListNumStyle(iStartAt, _
                iMapNumberStyle_MPToWord(iNumberStyle)) & Space(1)
            Me.udStartAt.Enabled = True
    End Select
    
'   enable/disable legal style
    If iNumberStyle = mpListNumberStyleArabic Or _
            iNumberStyle = mpListNumberStyleArabicLZ Then
        Me.chkLegalStyle.Enabled = True
    Else
        Me.chkLegalStyle.Enabled = False
    End If
    
'   enable/disable restart numbering
    With Me.chkReset
        If iNumberStyle = mpListNumberStyleNone Or _
                iNumberStyle = mpListNumberStyleBullet Then
            .Value = 0
            .Enabled = False
        Else
            .Enabled = True
        End If
        If Not bLevelChanged Then _
            .Value = Abs(.Enabled)
    End With
    
'   enable/disable previous levels
    If iNumberStyle = mpListNumberStyleBullet Then
        Me.chkPreviousLevels = 0
        Me.chkPreviousLevels.Enabled = False
    Else
        Me.chkPreviousLevels.Enabled = True
    End If
    
'   if no number, default to no trailing character
    If iNumberStyle = mpListNumberStyleNone And _
            Not bLevelChanged Then
       Me.cmbTrailingChar.ListIndex = mpTrailingChar_None
    End If
    
'   update number format
    If Not bLevelChanged Then
        bNoChangeEvent = True
        If iNumberStyle = mpListNumberStyleBullet Then
            With Me.rtfNumberFormat
                .Enabled = False
                .Font.Name = "Symbol"
                .Text = Chr(183)
                m_xCode = Chr(183)
            End With
        Else        'anything other than bullet
            xNewNumber = xIntToListNumStyle(iStartAt, _
                iMapNumberStyle_MPToWord(iNumberStyle))
            iPos = InStr(m_xCode, LTrim(Str(iLevel)))
            If iPos = 0 Then    'no number
                iPos = 1
            End If
            iOldNumLength = lCountChrs(m_xCode, _
                LTrim(Str(iLevel)))
            
'           insert and format new number
            With Me.rtfNumberFormat
'               if style was previously bullet
'               clear and enable text box
                If .Enabled = False Then
                    .Enabled = True
                    .Font.Name = "Arial"
                    .Text = ""
                    m_xCode = ""
                    .SelColor = vbBlue
                End If
                .SelStart = iPos - 1
                .SelLength = iOldNumLength
                .SelText = xNewNumber
                If iOldNumLength = 0 Then
                    .SelStart = iPos - 1
                    .SelLength = Len(xNewNumber)
                    .SelColor = vbBlue
                    .SelStart = 0
                End If
                xUserAll = .TextRTF
'                .SelColor = vbBlue
            End With
            
            If m_xCode = "" Then
'               start new code
                m_xCode = String(Len(xNewNumber), LTrim(Str(iLevel)))
            Else
'               edit existing code
                m_xCode = Left(m_xCode, iPos - 1) & _
                    String(Len(xNewNumber), LTrim(Str(iLevel))) & _
                    Right(m_xCode, Len(m_xCode) - iPos - iOldNumLength + 1)
            End If
        End If
        bNoChangeEvent = False
    End If
End Sub

Private Sub cmbSpaceBeforeEntries_Click()
    bTOCIsDirty = True
End Sub

Private Sub cmbSpaceBetweenEntries_Click()
    bTOCIsDirty = True
End Sub

Private Sub cmbTextAlign_Click()
    Dim bEnabled As Boolean
    
    bParaIsDirty = True
    
'   enable/disable indent controls
    bEnabled = Not (Me.cmbTextAlign = "Centered")
    Me.lblTextPosition.Enabled = bEnabled
    Me.lblLeftIndentIn.Enabled = bEnabled
    Me.txtTextPosition.Enabled = bEnabled
    Me.udTextPosition.Enabled = bEnabled
    Me.lblTabPosition.Enabled = bEnabled
    Me.lblTabIndentIn.Enabled = bEnabled
    Me.txtTabPosition.Enabled = bEnabled
    Me.udTabPosition.Enabled = bEnabled
    Me.lblNumberPosition.Enabled = bEnabled
    Me.lblNumberIndentIn.Enabled = bEnabled
    Me.txtNumberPosition.Enabled = bEnabled
    Me.udNumberPosition.Enabled = bEnabled
    Me.chkRightAlign.Enabled = bEnabled
    If Not bEnabled Then _
        Me.chkRightAlign = 0
End Sub

Private Sub cmbTOCLineSpacing_Click()
    bTOCIsDirty = True
End Sub

Private Sub cmbTOCTextAlign_Click()
    Dim bEnabled As Boolean
    bTOCIsDirty = True
        
    bEnabled = (Me.cmbTOCTextAlign = "Left")
    Me.chkDotLeaders.Enabled = bEnabled
    Me.lblTOCFirstLineIndent.Enabled = bEnabled
    Me.txtTOCFirstLineIndent.Enabled = bEnabled
    Me.lblTOCLeftIndent.Enabled = bEnabled
    Me.txtTOCLeftIndent.Enabled = bEnabled
    Me.lblTOCLeftIndentIn.Enabled = bEnabled
    Me.lblTOCFirstLineIndentIn.Enabled = bEnabled
End Sub

Private Sub cmbTrailingChar_Click()
    Dim bEnabled As Boolean
    
    bLevelIsDirty = True
    bDocPropIsDirty = True
    
    bEnabled = (Me.cmbTrailingChar = "Tab")
    Me.txtTabPosition.Enabled = bEnabled
    Me.lblTabPosition.Enabled = bEnabled
    Me.lblTabIndentIn.Enabled = bEnabled

End Sub

Private Sub cmdAddLevel_Click()
    Dim xNumberFormat As String
    
'   can't add tenth level
    If m_iCurrentLevels = 9 Then
        MsgBox "The current scheme already has nine levels.", _
            vbInformation, AppName
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
'   update current level
    If (bLevelIsDirty + bTCIsDirty + bTOCIsDirty + _
            bDocPropIsDirty + bParaIsDirty + bNextParaIsDirty) Then

'       reformat rtf value
        xNumberFormat = xParseNumberFormat(m_xCode, _
            Me.rtfNumberFormat.Text)

'       implement changes
        bRet = bCreateLevel(Me, _
                            xScheme, _
                            Me.cmbLevel, _
                            xNumberFormat, _
                            bLevelIsDirty, _
                            bTCIsDirty, _
                            bTOCIsDirty, _
                            bDocPropIsDirty, _
                            bParaIsDirty)
        
'       update dirty flag array
        bRet = bUpdateDirtyFlagArray(Me.cmbLevel, _
                                     bLevelIsDirty, _
                                     bTCIsDirty, _
                                     bTOCIsDirty, _
                                     bDocPropIsDirty, _
                                     bParaIsDirty, _
                                     bNextParaIsDirty)
'       reset dirty flags
        bLevelIsDirty = False
        bTCIsDirty = False
        bTOCIsDirty = False
        bDocPropIsDirty = False
        bParaIsDirty = False
        bNextParaIsDirty = False
    End If
    
'   add new level
    m_iCurrentLevels = m_iCurrentLevels + 1
    lRet = lAddLevel(xScheme, m_iCurrentLevels)

'   update dirty flag array
    bRet = bUpdateDirtyFlagArray(m_iCurrentLevels, _
                True, True, True, True, True, True)

'   update level combobox
    Me.cmbLevel.AddItem Str(m_iCurrentLevels)
    Me.chkLevel.Item(m_iCurrentLevels - 1).Value = vbChecked
    
'   recreate preview
    ActiveDocument.Content.Delete
    oPreview.ShowPreview xScheme, mpSchemeType_Document
        
'   enable/disable add/delete level buttons
    Me.cmdAddLevel.Enabled = Not Me.chkLevel(8).Visible
    Me.cmdDeleteLevel.Enabled = True
    
'   move buttons
    RedrawLevelButtons
        
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdCancel_Click()
    Me.Hide
    DoEvents
    Application.ScreenRefresh
End Sub

Private Sub cmdDeleteLevel_Click()
    Dim iLevel As Integer
    Dim i As Integer
    
'   disallow deletion of only level
    If m_iCurrentLevels = 1 Then
        MsgBox "The current scheme has only one level.", _
            vbInformation, AppName
        Exit Sub
    End If
    
'   get level
    iLevel = Me.cmbLevel
    
'   prompt for confirmation
    lRet = MsgBox("Delete level " & iLevel & "?", _
        vbYesNo + vbQuestion, AppName)
        
    If lRet = vbYes Then
        Screen.MousePointer = vbHourglass
        
'       delete level
        lRet = lDeleteLevel(xScheme, m_iCurrentLevels, iLevel)
                
'       update dirty flag array
        If iLevel < m_iCurrentLevels Then
            For i = iLevel To m_iCurrentLevels - 1
                bRet = bUpdateDirtyFlagArray(i, True, True, _
                    True, True, True, True)
            Next i
        End If
        
'       remove level from list
        m_iCurrentLevels = m_iCurrentLevels - 1
        Me.cmbLevel.RemoveItem m_iCurrentLevels

'       uncheck last level
        Me.chkLevel.Item(m_iCurrentLevels).Value = vbUnchecked
        
'       select appropriate level chkbox
        If iLevel > m_iCurrentLevels Then
'           select last level
            Me.chkLevel.Item(m_iCurrentLevels - 1).Value = vbChecked
        Else
            iLastSelectedLevel = iLastSelectedLevel + 1
'           refresh dlg for selected level
            cmbLevel_Click
        End If
        
'       recreate preview
        ActiveDocument.Content.Delete
        oPreview.ShowPreview xScheme, mpSchemeType_Document
        
'       enable/disable add/delete level buttons
        Me.cmdDeleteLevel.Enabled = Me.chkLevel(1).Visible
        Me.cmdAddLevel.Enabled = True
        
'       redraw buttons
        RedrawLevelButtons
        
        Screen.MousePointer = vbDefault
    End If
End Sub

Private Sub cmdSave_Click()
    Dim xNumberFormat As String
    
    bNoChangeEvent = True
    
'   create current level
    If (bLevelIsDirty + bTCIsDirty + bTOCIsDirty + _
            bDocPropIsDirty + bParaIsDirty + bNextParaIsDirty) Then
            
'       refresh current level for any changes
        oPreview.RefreshPreviewLevel xScheme, Me.cmbLevel

'       reformat rtf value
        xNumberFormat = xParseNumberFormat(m_xCode, _
            Me.rtfNumberFormat.Text)
        
'       implement changes
        bRet = bCreateLevel(Me, _
                            xScheme, _
                            Me.cmbLevel, _
                            xNumberFormat, _
                            bLevelIsDirty, _
                            bTCIsDirty, _
                            bTOCIsDirty, _
                            bDocPropIsDirty, _
                            bParaIsDirty)
        
'       update dirty flag array
        bRet = bUpdateDirtyFlagArray(Me.cmbLevel, _
                                    bLevelIsDirty, _
                                    bTCIsDirty, _
                                    bTOCIsDirty, _
                                    bDocPropIsDirty, _
                                    bParaIsDirty, _
                                    bNextParaIsDirty)
    End If
    
    Me.Hide
    DoEvents
    m_bSave = True
End Sub

Private Sub Form_Activate()
'   enable/disable add/delete level buttons
    Me.cmdDeleteLevel.Enabled = Me.chkLevel(1).Visible
    Me.cmdAddLevel.Enabled = Not Me.chkLevel(8).Visible
    RedrawLevelButtons
    
    With dlgScheme
        .Top = 20000
        .mnuScheme.Enabled = True
        .mnuSharing.Enabled = True
        .vseStatus.Visible = False
    
        Me.Refresh
        .Refresh
    End With
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim xSchemeAlias As String
    bFormInit = True
    m_bSave = False
    
    m_iSelStart = -1
    
    Set oPreview = New CPreview
    Set m_xarIsDirty = New xArray
    Set m_xarPrevLevels = New xArray
    
    Screen.MousePointer = vbHourglass
    
'   get scheme
    With dlgScheme
        xScheme = .Scheme
        xSchemeAlias = .SchemeDisplayName
    End With
        
'   get number of levels
    m_iInitialLevels = iGetLevels(xScheme, mpSchemeType_Document)
    m_iCurrentLevels = m_iInitialLevels
    
'   disable chkboxes for non-existent levels
    For i = m_iCurrentLevels To 8
        With Me.chkLevel.Item(i)
            .Visible = False
            .Caption = ""
        End With
    Next i
    
'   set caption
    Me.Caption = "Edit " & xSchemeAlias & " Scheme"
    
'   fill next paragraph list
    With Me.cmbNextParagraph
        .Clear
'       these are from doc prop in firm's mpNumbers.sty
        If xNextParaStyles(0) <> "" Then
            For i = 0 To UBound(xNextParaStyles)
                .AddItem xNextParaStyles(i)
            Next i
        End If
'       these are offered for every scheme
        For i = 1 To m_iInitialLevels
            .AddItem xScheme & " Cont " & i
        Next i
    End With
    
'   fill level list
    iLastSelectedLevel = 0
    With Me.cmbLevel
        For i = 1 To m_iInitialLevels
            .AddItem Str(i)
        Next i
'       this will trigger loading of all properties
        .ListIndex = 0
    End With
    
'   initialize m_xarIsDirty Array
    m_xarIsDirty.ReDim 1, 9, 1, 6
    
    vsIndexTab1.CurrTab = 0
    
'   create preview in doc
    With oPreview
        .ShowPreview xScheme, dlgScheme.SchemeType
        .HighlightLevel Me.cmbLevel
    End With
    
'   update flags
    bFormInit = False
    bLevelIsDirty = False
    bTCIsDirty = False
    bTOCIsDirty = False
    bDocPropIsDirty = False
    bParaIsDirty = False
    bNextParaIsDirty = False
    bSchemeIsDirty = False
    Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oPreview = Nothing
    Set m_xarPrevLevels = Nothing
    Set m_xarIsDirty = Nothing
End Sub

Private Sub lstPrevLevels_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not (Me.ActiveControl Is Me.lstPrevLevels) Then
        Me.lstPrevLevels.SetFocus
    End If
    
End Sub

Private Sub rdMark_Click()
    bDocPropIsDirty = True
    bTCIsDirty = True
    bLevelIsDirty = True
    bParaIsDirty = True

    Me.chkTrailingUnderline.Enabled = True
End Sub

Private Sub rdStyle_Click()
    bDocPropIsDirty = True
    bTCIsDirty = True
    bLevelIsDirty = True
    bParaIsDirty = True
    
    If Not bNoChangeEvent And _
            (Me.chkTrailingUnderline <> Me.chkNumberUnderline) Then
        lRet = MsgBox("If this change is made, trailing character underlining " & _
            "will need to be adjusted to match number underlining.  " & _
            "Trailing character and number underlining can only be " & _
            "different when heading is manually designated." & String(2, vbCr) & _
            "Proceed with automatically designating entire paragraph as heading?", _
            vbYesNo, AppName)
        If lRet = vbNo Then
            Me.rdMark = True
            Exit Sub
        End If
    End If
            
    Me.chkTrailingUnderline = Me.chkNumberUnderline
    Me.chkTrailingUnderline.Enabled = False
    
End Sub

Private Sub rtfNumberFormat_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iSelStart As Integer
    Dim iSelLen As Integer
    Dim xSelCode As String
    Dim iLevel As Integer
    Dim bNumIsInSel As Boolean
    Dim bCharIsNum As Boolean
    Dim xPreSel As String
    Dim xPostSel As String
    Dim iKey As Integer
    Dim bIsAllowed As Boolean
    Dim bIsNavigationKey As Boolean
    Dim xNew As String
    Dim xChar As String
    Dim iPrevLevel As Integer
    Dim i As Integer

'   exit if ctl or alt is down
    If Shift = vbCtrlMask Or Shift = vbAltMask Then
        Exit Sub
    End If

'   define valid characters -
'   shift = 16
    iKey = UCase(KeyCode)
    bIsAllowed = (iKey >= 32 And iKey <= 96) Or _
                 (iKey >= 123 And iKey <= 126) Or _
                 (iKey = vbKeyDelete) Or _
                 (iKey = vbKeyBack) Or (iKey = 190)

    bIsNavigationKey = (iKey = vbKeyLeft) Or _
                       (iKey = vbKeyRight) Or _
                       (iKey = vbKeyHome) Or _
                       (iKey = vbKeyEnd) Or _
                       (iKey = vbKeyInsert) Or _
                       (iKey = vbKeyUp) Or _
                       (iKey = vbKeyDown) Or _
                       (iKey = vbKeyPageUp) Or _
                       (iKey = vbKeyPageDown)

'   allow non-input keys to go through unmodified
    If Not bIsAllowed Then
        Exit Sub
    ElseIf bIsNavigationKey Then
        Exit Sub
    End If

    iLevel = Me.cmbLevel


    With Me.rtfNumberFormat
'       get selection detail
        iSelStart = .SelStart
        iSelLen = .SelLength
        xSelCode = Mid(m_xCode, iSelStart + 1, iSelLen)

'       different actions for selection/non-selection
        If iSelLen Then
'           there is a selection - test for existence of
'           current level number in selection
            bNumIsInSel = (InStr(xSelCode, iLevel) > 0)
            If bNumIsInSel Then
'               prevent any key from going through and alert user
                KeyCode = 0
                xMsg = "Cannot delete current level number. Please modify " & _
                    "your selection to exclude the Level " & iLevel & " number."
                MsgBox xMsg, vbExclamation, AppName
            Else
'               delete the corresponding chars
'               in m_xCode and insert new character
                xPreSel = Left(m_xCode, iSelStart)
                xPostSel = Mid(m_xCode, iSelStart + iSelLen + 1)
                m_xCode = xPreSel & xPostSel
            End If
        Else
'           there is no selection - only
'           delete and backspace need to be addressed
            If KeyCode = vbKeyDelete Then
                If iSelStart > Len(.Text) - 1 Then
                    Exit Sub
                End If

'               get char to delete
                xChar = Mid(m_xCode, iSelStart + 1, 1)

'               test if the char to be deleted is current level number
                bCharIsNum = (xChar = CStr(iLevel))

                If bCharIsNum Then
'                   prevent key from going through and alert user
                    KeyCode = 0
                    xMsg = "Cannot delete the current level number."
                    MsgBox xMsg, vbExclamation, AppName
                Else
'                   test if char is a previous level
                    If IsNumeric(xChar) Then
                        iPrevLevel = xChar
                        i = 0
'                       modify selection to select all chars in number
                        While xChar = CStr(iPrevLevel)
                            i = i + 1
                            xChar = Mid(m_xCode, iSelStart + 1 + i, 1)
                        Wend
                        .SelLength = i

                        xPostSel = Mid(m_xCode, iSelStart + .SelLength + 1)
                    Else
                        xPostSel = Mid(m_xCode, iSelStart + 2)
                    End If

'                   delete the corresponding chars in m_xCode
                    xPreSel = Left(m_xCode, iSelStart)
                    m_xCode = xPreSel & xPostSel
                End If

            ElseIf KeyCode = vbKeyBack Then
                If iSelStart = 0 Then
                    Exit Sub
                End If

'               get char to delete
                xChar = Mid(m_xCode, iSelStart, 1)

'               test if the char to be deleted is current level number
                bCharIsNum = (xChar = CStr(iLevel))

                If bCharIsNum Then
'                   prevent key from going through and alert user
                    KeyCode = 0
                    xMsg = "Cannot delete the current level number."
                    MsgBox xMsg, vbExclamation, AppName
                Else
'                   test if char is a previous level
                    If IsNumeric(xChar) Then
                        iPrevLevel = xChar
                        i = 0
'                       modify selection to select all chars in number
                        While (xChar = CStr(iPrevLevel))
                            i = i + 1
                            On Error Resume Next
                            xChar = ""
                            xChar = Mid(m_xCode, iSelStart - i, 1)
                            On Error GoTo 0
                        Wend
                        iSelStart = .SelStart - i
                        .SelStart = iSelStart
                        .SelLength = i
                        If iSelStart Then
                            xPreSel = Left(m_xCode, iSelStart)
                        End If
                        xPostSel = Mid(m_xCode, iSelStart + .SelLength + 1)
                    Else
                        xPreSel = Left(m_xCode, iSelStart - 1)
                        xPostSel = Mid(m_xCode, iSelStart + 1)
                    End If

'                   delete the corresponding chars in m_xCode
                    m_xCode = xPreSel & xPostSel
                End If
            End If
'           prevent rtNumberFormat_SelChange event from running
'           before m_xCode is updated by rtfNumberFormat_Change
            bNoSelChangeEvent = True
        End If
    End With
End Sub

Private Sub rtfNumberFormat_Change()
'deal with number format backend string
    Dim iStart As Integer
    Dim xCodeSelection As String
    Dim iLevel As Integer
    Dim iAdded As Integer
    Dim xAdded As String
    Dim i As Integer
    Dim iPrevLevel As Integer
    Dim xLevelCode As String
    Dim xPreCode As String
    Dim xPostCode As String
    Dim iColor As Integer

    bLevelIsDirty = True
    iLevel = Me.cmbLevel
    If bFormInit Or bNoChangeEvent Then Exit Sub
    With Me.rtfNumberFormat
'       get num of chars added
        DoEvents
        iAdded = Len(.Text) - Len(m_xCode)

'       get start pos from current selection
'       pos minus the number of chars entered
        iStart = mpMax(.SelStart - iAdded, 0)

'       if chars were added...
        If iAdded > 0 Then
            If m_bIsPrevLevels Then
'               the added chars were previous levels
                With Me.lstPrevLevels.SelBookmarks
'                   cycle through all added levels
                    For i = 0 To .Count - 1
                        iPrevLevel = .Item(i) + 1
'                       converts selected level number to
'                       appropriate code - each char in the
'                       number is represented by the number's
'                       level - eg the level 2 number 'One'
'                       is represented as '222'
                        xLevelCode = String(Len(xGetListNumber( _
                            xScheme, iPrevLevel)), CStr(iPrevLevel))
'                       build code string
                        xAdded = xAdded & xLevelCode
                    Next
                End With
                iColor = vbRed
            Else
'               the added chars were straight text -
'               a straight text char is represented by '$'
                xAdded = String(iAdded, mpTextCode)
                iColor = vbBlack
            End If

'           modify number code representation as appropriate
            If iStart Then
                xPreCode = Left(m_xCode, iStart)
                xPostCode = Mid(m_xCode, iStart + 1)
                m_xCode = xPreCode & xAdded & xPostCode
            Else
                m_xCode = xAdded & m_xCode
            End If

            bNoChangeEvent = True

'           ensure that the added text displays the
'           correct color for the type of text that
'           was added - prev levels are red,
'           straight text is black
            .SelStart = iStart
            .SelLength = iAdded
            .SelColor = iColor


'           set selection point to the pos
'           after the inserted chars
            .SelStart = .SelStart + iAdded
            .SelLength = 0
            .SetFocus
            bNoChangeEvent = False
        End If
    End With
End Sub

Private Sub rtfNumberFormat_SelChange()
    If (bFormInit Or bNoChangeEvent Or bNoSelChangeEvent) Then
        bNoSelChangeEvent = False
        Exit Sub
    End If
    
'   m_bSelChangeRunning prevents the SelChange
'   event from firing a second time
    If Not m_bSelChangeRunning Then
        m_bSelChangeRunning = True
        bModifySelection
        m_bSelChangeRunning = False
    End If
    
'   set tooltip text for current selection
    SetNumFormatTooltip
End Sub

Private Function SetNumFormatTooltip()
'   set tooltip text to descriptor of selection
    Dim xText As String
    Dim xCode As String
    Dim xChr1 As String
    
    With Me.rtfNumberFormat
        Select Case .SelLength
            Case 0
                xText = ""
            Case 1
'               get code behind selection
                xCode = Mid(m_xCode, .SelStart + 1, 1)
                Select Case xCode
                    Case mpTextCode
                        xText = "Text"
                    Case Else
                        xText = "Level " & xCode
                End Select
            Case Else
'               get code behind selection
                xCode = Mid(m_xCode, .SelStart + 1, .SelLength)

'               get code of first chr in selection
                xChr1 = Mid(m_xCode, .SelStart + 1, 1)
                
                
'               check if code string is all same chr
                If Len(xChr1) = 0 Then
                    xText = ""
                ElseIf String(.SelLength, xChr1) = xCode Then
                    xText = "Level " & xChr1
                Else
                    xText = ""
                End If
        End Select
        .ToolTipText = xText
    End With
End Function

Private Function bModifySelection(Optional bGoForward As Boolean = False, _
                                  Optional bGoBack As Boolean = False) As Boolean
'modifies current selection to include the selection
'of entire numbers - prevents numbers from being
'split apart
    Dim xLeftChar As String
    Dim xRightChar As String
    Dim xNextChar As String
    Dim xPrevChar As String
    Dim bIsSameLevel As Boolean
    Dim iStart As Integer
    Dim iEnd As Integer
    Dim bInNumber As Boolean
    
    With Me.rtfNumberFormat
        If .SelStart = 0 Then
            Exit Function
        End If
        
'       test for selection
        If Len(.SelText) = 0 Then
            xLeftChar = Mid(m_xCode, .SelStart, 1)
            xRightChar = Mid(m_xCode, .SelStart + 1, 1)
            bInNumber = IsNumeric(xLeftChar) And _
                        IsNumeric(xRightChar) And _
                        (xLeftChar = xRightChar)
            If bInNumber Then
                iStart = .SelStart
                iEnd = .SelStart
            Else
                Exit Function
            End If
        Else
'           get start and end points
            iStart = .SelStart
            iEnd = .SelStart + .SelLength
        End If
        
'       check left border of selection for a
'       prev level or current level number
        xLeftChar = Mid(m_xCode, iStart + 1, 1)
            
'       check right border of selection for a
'       prev level or current level number
        xRightChar = Mid(m_xCode, iEnd, 1)
            
'       if left side is a level, extend
'       the selection to the left
'       for as far as that number goes
        If IsNumeric(xLeftChar) And iStart Then
            xPrevChar = Mid(m_xCode, iStart, 1)
            
            While (xPrevChar = xLeftChar) And iStart
                iStart = iStart - 1
                On Error Resume Next
                xPrevChar = Mid(m_xCode, iStart, 1)
                On Error GoTo 0
            Wend
        End If
        
'       if right side is a level, extend
'       the selection to the right
'       for as far as that number goes
        If IsNumeric(xRightChar) And iEnd <> (.SelStart + .SelLength + 1) Then
            xNextChar = Mid(m_xCode, iEnd + 1, 1)
            
            While (xNextChar = xRightChar) And _
                    iEnd <> Len(.Text)
                iEnd = iEnd + 1
                xNextChar = Mid(m_xCode, iEnd + 1, 1)
            Wend
        End If
        
'       make new selection
        .SelStart = iStart
        .SelLength = iEnd - .SelStart
    End With
    m_bSelChangeRunning = True
End Function

Function lLoadLevelProperties() As Long
    Dim xStyle As String
    Dim xTCStyle As String
    Dim xTOCStyle As String
    Dim iLevel As Integer
    Dim iNumberStyle As Integer
    Dim xNumberFormat As String
    Dim xNumber As String
    Dim iLoc As Integer
    Dim iSelLevel As Integer
    Dim iSelStart As Integer
    Dim xNextPara As String
    Dim bEnabled As Boolean
    Dim fntTCHeading As Word.Font
    
    bLevelChanged = True
    
    iLevel = Me.cmbLevel
    xStyle = xScheme & "_L" & iLevel
    xTCStyle = xScheme & "TCEntryL" & iLevel
    xTOCStyle = xScheme & "TOC" & iLevel
    
    With ActiveDocument.ListTemplates(xScheme).ListLevels(iLevel)
'       fill number format rtf text box
        m_xCode = ""
        xNumberFormat = Trim(.NumberFormat)
        If .NumberStyle = wdListNumberStyleBullet Then
            With Me.rtfNumberFormat
                .Enabled = False
                .Font.Name = "Symbol"
                .Text = Chr(183)
                .SelStart = 0
                .SelLength = 1
                .SelColor = vbBlack
                .SelLength = 0
                m_xCode = Chr(183)
            End With
        Else    'anything other than bullet
            With Me.rtfNumberFormat
                .Enabled = True
                .Font.Name = "MS Sans Serif"
                .Text = xNumberFormat
                .SelStart = 0
                .SelLength = Len(.Text)
                .SelColor = vbBlack
                iLoc = InStr(.Text, "%")
                While iLoc
                    .SelStart = iLoc - 1
                    iSelStart = .SelStart
                    .SelLength = 2
                    iSelLevel = Mid(.Text, iLoc + 1, 1)
                    xNumber = LTrim(xGetListNumber(xScheme, iSelLevel))
                    .SelText = xNumber
                    .SelStart = iSelStart
                    .SelLength = Len(xNumber)
                    If iSelLevel = iLevel Then
                        .SelColor = vbBlue
                    Else
                        .SelColor = vbRed
                    End If
                    m_xCode = m_xCode & _
                        String(iLoc - 1 - Len(m_xCode), mpTextCode) & _
                        String(Len(xNumber), LTrim(Str(iSelLevel)))
                    iLoc = InStr(.Text, "%")
                Wend
                m_xCode = m_xCode & _
                    String(Len(.Text) - Len(m_xCode), mpTextCode)
                xUserAll = .TextRTF
                .SelStart = 0
            End With
        End If
        
'       get font properties
        With .Font
            Me.chkNumberBold = Abs(.Bold)
            Me.chkNumberCaps = Abs(.AllCaps)
            Me.chkNumberItalic = Abs(.Italic)
            Me.chkNumberUnderline = Abs(.Underline = wdUnderlineSingle)
        End With
        
'       get number style
        iNumberStyle = iMapNumberStyle_WordToMP(.NumberStyle)
        Select Case iNumberStyle
            Case mpListNumberStyleArabic, mpListNumberStyleArabicLZ
                Me.chkLegalStyle = 0
            Case mpListNumberStyleLegal
                iNumberStyle = mpListNumberStyleArabic
                Me.chkLegalStyle = 1
            Case mpListNumberStyleLegalLZ
                iNumberStyle = mpListNumberStyleArabicLZ
                Me.chkLegalStyle = 1
            Case Else
                Me.chkLegalStyle = 0
        End Select
        Me.cmbNumberStyle.ListIndex = iNumberStyle
                
'       get other number properties
        Me.chkReset = Abs(CInt(.ResetOnHigher))
        Me.chkRightAlign = Abs(.Alignment = wdListLevelAlignRight)
        Me.txtNumberPosition = .NumberPosition / 72
        If .TabPosition = 9999999 Then
            Me.txtTabPosition = 0
        Else
            Me.txtTabPosition = (.TabPosition - .NumberPosition) / 72
        End If
        Me.txtTextPosition = .TextPosition / 72
    End With
    
'   get trailing character from doc prop
    Me.cmbTrailingChar.ListIndex = Val(xGetLevelProperty(xScheme & "_L", _
        iLevel, mpDPTrailingChar, mpSchemeType_Document))
    bEnabled = (Me.cmbTrailingChar = "Tab")
    Me.txtTabPosition.Enabled = bEnabled
    Me.lblTabPosition.Enabled = bEnabled
    Me.lblTabIndentIn.Enabled = bEnabled

'   get trailing char underlining from doc prop
    Me.chkTrailingUnderline = Val(xGetLevelProperty(xScheme & "_L", _
        iLevel, mpDPTrailingUnderline, mpSchemeType_Document))
    
'   get tc format
    With Word.ActiveDocument.Styles
        If xGetLevelProperty(xScheme & "_L", iLevel, _
                mpDPUseStyle, mpSchemeType_Document) = 1 Then
            Set fntTCHeading = .Item(xScheme & "_L" & iLevel).Font
            Me.rdStyle = True
        Else
            Set fntTCHeading = .Item(xScheme & "TCEntryL" & iLevel).Font
            Me.rdMark = True
        End If
    End With
        
    With fntTCHeading
        Me.chkTCBold = Abs(.Bold)
        Me.chkTCCaps = Abs(.AllCaps)
        Me.chkTCItalic = Abs(.Italic)
        Me.chkTCSmallCaps = Abs(.SmallCaps)
        Me.chkTCUnderline = .Underline
    End With
    
'   get TOC format
    With ActiveDocument.Styles(xTOCStyle)
        With .ParagraphFormat
            Me.cmbTOCTextAlign.ListIndex = .Alignment
            Me.cmbTOCLineSpacing.ListIndex = .LineSpacingRule
            Me.txtTOCLeftIndent = (.LeftIndent + .FirstLineIndent) / 72
            Me.txtTOCFirstLineIndent = Abs(.FirstLineIndent / 72)
            Me.txtSpaceBetweenEntries = .SpaceAfter
            Me.chkDotLeaders = Abs(.TabStops(1).Leader = wdTabLeaderDots)
        End With
        
        With .Font
            Me.chkTOCAllCaps = Abs(.AllCaps)
            Me.chkTOCBold = Abs(.Bold)
            Me.chkTOCItalic = Abs(.Italic)
            Me.chkTOCSmallCaps = Abs(.SmallCaps)
            Me.chkTOCUnderline = Abs(.Underline = wdUnderlineSingle)
        End With
    End With
    
    Me.txtSpaceBeforeEntries = xGetLevelProperty(xScheme & "_L", _
        iLevel, mpDPSpaceBeforeTOCLevel, mpSchemeType_Document)
    Me.chkIncludePageNumbers = xGetLevelProperty(xScheme & "_L", _
        iLevel, mpDPPageNumberInTOC, mpSchemeType_Document)
    
    With ActiveDocument.Styles(xStyle)
'       get paragraph format
        With .ParagraphFormat
            If .Alignment = wdAlignParagraphJustify Then
                Me.cmbTextAlign = "Justified"
            ElseIf .Alignment = wdAlignParagraphCenter Then
                Me.cmbTextAlign = "Centered"
            ElseIf .Alignment = wdAlignParagraphRight Then
'               we're not offering right alignment,
'               so we can use it to store this option
                Me.cmbTextAlign = "Base on Document"
            Else
                Me.cmbTextAlign = "Left"
            End If
            Me.cmbLineSpacing.ListIndex = .LineSpacingRule

'           space after is really space between
            Me.txtSpaceAfter = .SpaceBefore + .SpaceAfter
        End With

'       get next paragraph style
        xNextPara = .NextParagraphStyle
        On Error GoTo AddStyleToNextParaList
        Me.cmbNextParagraph = xNextPara
        On Error GoTo 0
    End With
    
    bLevelChanged = False
    DoEvents
    Exit Function
    
AddStyleToNextParaList:
    Me.cmbNextParagraph.AddItem xNextPara
    Resume
    
End Function

Private Sub txtNumberPosition_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtNumberPosition, True)
End Sub

Private Sub txtSpaceAfter_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtSpaceAfter, False)
End Sub

Private Sub txtSpaceBeforeEntries_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtSpaceBeforeEntries, False)
End Sub

Private Sub txtSpaceBetweenEntries_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtSpaceBetweenEntries, False)
End Sub

Private Sub txtTabPosition_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtTabPosition, True)
End Sub

Private Sub txtTextPosition_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtTextPosition, True)
End Sub

Private Sub txtTOCFirstLineIndent_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtTOCFirstLineIndent, True)
End Sub

Private Sub txtTOCLeftIndent_KeyPress(KeyAscii As Integer)
    KeyAscii = AllowDigitsOnly(KeyAscii, Me.txtTOCLeftIndent, True)
End Sub

Private Sub udStartAt_Change()
    Dim iNumberStyle As Integer
    Dim iLevel As Integer
    Dim iStartAt As Integer
    Dim iPos As Integer
    Dim iOldNumLength As Integer
    Dim xNewNumber As String
    Dim xStartAt As String
    
    bLevelIsDirty = True
    
'   convert appropriate format
    iNumberStyle = iMapNumberStyle_MPToWord(Me.cmbNumberStyle.ListIndex)
    iStartAt = mpMax(mpMin(Me.udStartAt.Value, 1000), 1)
    xStartAt = xIntToListNumStyle(iStartAt, iNumberStyle)

'   update start at textbox
    If (iNumberStyle = wdListNumberStyleOrdinal) Or _
            (iNumberStyle = wdListNumberStyleOrdinalText) Or _
            (iNumberStyle = wdListNumberStyleCardinalText) Then
        Me.lblStartAtValue.Caption = LTrim(Str(iStartAt)) & Space(1)
    Else
        Me.lblStartAtValue.Caption = xStartAt & Space(1)
    End If

'   update number format
    If Not bLevelChanged Then
        iLevel = Me.cmbLevel
        bNoChangeEvent = True
        xNewNumber = xIntToListNumStyle(iStartAt, _
            iNumberStyle)
        If m_xCode <> Chr(183) Then
            iPos = InStr(m_xCode, LTrim(Str(iLevel)))
        Else
            iPos = 1
        End If
        iOldNumLength = lCountChrs(m_xCode, LTrim(Str(iLevel)))
        With Me.rtfNumberFormat
            .SelStart = iPos - 1
            .SelLength = iOldNumLength
            .SelText = xNewNumber
            .SelColor = vbBlue
            xUserAll = .TextRTF
        End With
        m_xCode = Left(m_xCode, iPos - 1) & _
            String(Len(xNewNumber), LTrim(Str(iLevel))) & _
            Right(m_xCode, Len(m_xCode) - iPos - iOldNumLength + 1)
        bNoChangeEvent = False
    End If

End Sub

Private Sub ClearListingSelectionsButLast()
    Dim i As Integer
    
'   remove listing selections
    With Me.lstPrevLevels.SelBookmarks
        .Add Me.lstPrevLevels.Bookmark
        For i = .Count - 2 To 0 Step -1
            .Remove i
        Next
        DoEvents
    End With
End Sub

Private Sub lstPrevLevels_DblClick()
'insert selected previous levels directly
'before current level number
    Dim i As Integer
    Dim iLevel As Integer
    Dim iPos As Integer
    Dim xPrePos As String
    Dim xPostPos As String
    Dim xPrev As String
    Dim xNumFormat As String
    
    iLevel = Me.cmbLevel
    
'   build string of numbers
    With Me.lstPrevLevels.SelBookmarks
        For i = (.Count - 1) To 0 Step -1
            xPrev = xPrev & xGetListNumber( _
                xScheme, .Item(i) + 1)
            If Len(xPrev) Then
                Exit For
            End If
        Next i
    End With
    
'   do only if there are selected previous levels
    If Len(xPrev) Then
'       get position of current level number
        iPos = InStr(m_xCode, iLevel)
        
        With Me.rtfNumberFormat
'           if current level number exists...
            If iPos Then
'               insert previous levels directly
'               before current level number
                .SelStart = iPos - 1
            Else
'               insert previous levels at end of text
                .SelStart = Len(.Text)
            End If
            m_bIsPrevLevels = True
            m_iSelStart = .SelStart
            .SelText = xPrev
            m_bIsPrevLevels = False
        End With
    End If
End Sub

Private Sub lstPrevLevels_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim dtoDataObj As MSForms.DataObject
    Dim xPrev As String
    Dim i As Integer
    
'   do only if user is initiating a drag
    If Button = vbLeftButton Then
        With Me.lstPrevLevels
        
'           make appropriate selections
            SelPrevLevels Y
            
'           start drag
            .Drag vbBeginDrag
            
            For i = 0 To .SelBookmarks.Count - 1
                If Me.chkLegalStyle Then
                    xPrev = xPrev & _
                        ActiveDocument.ListTemplates(xScheme) _
                        .ListLevels(.SelBookmarks(i) + 1).StartAt
                Else
                    xPrev = xPrev & xGetListNumber(xScheme, _
                                        .SelBookmarks(i) + 1)
                                        
'                   disallow bullet
                    If Right(xPrev, 1) = Chr(183) Then _
                        xPrev = Left(xPrev, Len(xPrev) - 1)
                End If
            Next i
        End With
        If xPrev <> "" Then
            m_bIsPrevLevels = True
            Set dtoDataObj = New MSForms.DataObject
            dtoDataObj.SetText xPrev
            dtoDataObj.StartDrag
        End If
    End If

    m_bIsPrevLevels = False
End Sub

Private Sub lstPrevLevels_RowChange()
    If Not (IsPressed(KEY_CTL) Or IsPressed(KEY_SHIFT)) Then
        ClearListingSelectionsButLast
    End If
End Sub

Private Sub txtNumberPosition_Change()
    bLevelIsDirty = True
End Sub

Private Sub txtNumberPosition_GotFocus()
    bEnsureSelectedContent Me.txtNumberPosition
End Sub

Private Sub txtNumberPosition_LostFocus()
    ValidatePositionInput Me.txtNumberPosition
End Sub

Private Sub txtSpaceAfter_Change()
    bParaIsDirty = True
End Sub

Private Sub txtSpaceAfter_GotFocus()
    bEnsureSelectedContent Me.txtSpaceAfter
End Sub

Private Sub txtSpaceAfter_LostFocus()
    ValidateSpaceInput Me.txtSpaceAfter
End Sub

Private Sub txtSpaceBeforeEntries_Change()
    bDocPropIsDirty = True
End Sub

Private Sub txtSpaceBeforeEntries_GotFocus()
    bEnsureSelectedContent Me.txtSpaceBeforeEntries
End Sub

Private Sub txtSpaceBeforeEntries_LostFocus()
    ValidateSpaceInput Me.txtSpaceBeforeEntries
End Sub

Private Sub txtSpaceBetweenEntries_Change()
    bTOCIsDirty = True
End Sub

Private Sub txtSpaceBetweenEntries_GotFocus()
    bEnsureSelectedContent Me.txtSpaceBetweenEntries
End Sub

Private Sub txtSpaceBetweenEntries_LostFocus()
    ValidateSpaceInput Me.txtSpaceBetweenEntries
End Sub

Private Sub lblStartAtValue_Change()
    bLevelIsDirty = True
End Sub

Private Sub txtTabPosition_Change()
    bLevelIsDirty = True
End Sub

Private Sub txtTabPosition_GotFocus()
    bEnsureSelectedContent Me.txtTabPosition
End Sub

Private Sub txtTabPosition_LostFocus()
    ValidatePositionInput Me.txtTabPosition
End Sub

Private Sub txtTextPosition_Change()
    bLevelIsDirty = True
End Sub

Private Sub txtTextPosition_GotFocus()
    bEnsureSelectedContent Me.txtTextPosition
End Sub

Private Sub txtTextPosition_LostFocus()
    ValidatePositionInput Me.txtTextPosition
End Sub

Property Get InitialLevels() As Integer
    InitialLevels = m_iInitialLevels
End Property

Property Get CurrentLevels() As Integer
    CurrentLevels = m_iCurrentLevels
End Property

Property Get bSave() As Integer
    bSave = m_bSave
End Property

Property Get xIsDirty() As xArray
    Set xIsDirty = m_xarIsDirty
End Property

Function bUpdateDirtyFlagArray(iLevel As Integer, _
                               bSaveLevel As Boolean, _
                               bSaveTC As Boolean, _
                               bSaveTOC As Boolean, _
                               bSaveDocProp As Boolean, _
                               bSavePara As Boolean, _
                               bSaveNextPara As Boolean) As Boolean
'appropriate element is switched on once any change
'to that level component has been implemented in buffer;
'elements will never be set to "False"
    
    If bSaveLevel Then _
        xIsDirty(iLevel, mpLevelIsDirty) = "True"
    If bSaveTC Then _
        xIsDirty(iLevel, mpTCIsDirty) = "True"
    If bSaveTOC Then _
        xIsDirty(iLevel, mpTOCIsDirty) = "True"
    If bSaveDocProp Then _
        xIsDirty(iLevel, mpDocPropIsDirty) = "True"
    If bSavePara Then _
        xIsDirty(iLevel, mpParaIsDirty) = "True"
    If bSaveNextPara Then _
        xIsDirty(iLevel, mpNextParaIsDirty) = "True"
        
End Function

Function bPositionIsValid(xText As String) As Boolean
'validates all indent/position fields measured in inches
    If Not (IsNumeric(xText) And _
            (xText >= 0) And _
            (xText <= mpMaxPositionalValue)) Then
        MsgBox "The value for inches must be a number between " & _
            "0 and " & mpMaxPositionalValue & ".", _
            vbExclamation, AppName
        Exit Function
    End If
    
    bPositionIsValid = True
End Function

Function bPointsAreValid(xText As String) As Boolean
'validates all spacing fields measured in points
    If (Not IsNumeric(xText)) Or _
            (xText < 0) Or xText > 48 Then
        MsgBox "The value for points must be a number " & _
            "between 0 and 48.", vbExclamation, AppName
        Exit Function
    End If
    
    bPointsAreValid = True

End Function

Private Sub txtTOCFirstLineIndent_Change()
    bTOCIsDirty = True
End Sub

Private Sub txtTOCFirstLineIndent_GotFocus()
    bEnsureSelectedContent Me.txtTOCFirstLineIndent
End Sub

Private Sub txtTOCFirstLineIndent_LostFocus()
    ValidatePositionInput Me.txtTOCFirstLineIndent
End Sub

Private Sub txtTOCLeftIndent_Change()
    bTOCIsDirty = True
End Sub

Private Sub txtTOCLeftIndent_GotFocus()
    bEnsureSelectedContent Me.txtTOCLeftIndent
End Sub

Private Sub txtTOCLeftIndent_LostFocus()
    ValidatePositionInput Me.txtTOCLeftIndent
End Sub

Function bTabIsValidRelativeToNumber() As Boolean
'makes sure number is not positioned right of tab
    If Me.txtTabPosition.Enabled Then
        If Val(Me.txtTabPosition) < Val(Me.txtNumberPosition) Then
            MsgBox "The number for the current level is set to be followed " & _
                "by a tab, but the tab is positioned to the left of the " & _
                "number.  The value for first line indent must be greater " & _
                "than the value for number indent.", vbExclamation, AppName
            Exit Function
        End If
    End If
    bTabIsValidRelativeToNumber = True
End Function

Private Sub cmdEditNextParaPara_Click()
'edit the para props of the selected next para style
    Dim oCStyle As New CStyle

    bRet = oCStyle.EditParagraph(Me.cmbNextParagraph)
    If bRet Then _
        bNextParaIsDirty = True
End Sub

Private Sub SetTCHeadingFormats(fntSource As Word.Font)
    With fntSource
        Me.chkTCBold = Abs(.Bold)
        Me.chkTCCaps = Abs(.AllCaps)
        Me.chkTCItalic = Abs(.Italic)
        Me.chkTCSmallCaps = Abs(.SmallCaps)
        Me.chkTCUnderline = .Underline
    End With
End Sub

Private Sub FillPrevLevelsList(xScheme As String)
'fill previous level list

    Dim iPrevLevel As Integer
    Dim i As Integer
    Dim xNum As String
    
    iPrevLevel = Val(Me.cmbLevel.Text) - 1
    
    If iPrevLevel Then
        m_xarPrevLevels.ReDim 0, iPrevLevel - 1, 0, 1
        For i = 0 To iPrevLevel - 1
            xNum = xGetListNumber(xScheme, i + 1)
            m_xarPrevLevels(i, 0) = " Level " & i + 1
            m_xarPrevLevels(i, 1) = xNum
        Next i
    Else
        m_xarPrevLevels.ReDim 0, -1, 0, 1
    End If
    
    With Me.lstPrevLevels
        Set .Array = m_xarPrevLevels
        .Rebind
        If m_xarPrevLevels.Count(1) Then
            .Row = 0
            .SelBookmarks.Add 1
        End If
    End With
End Sub

Sub RedrawLevelButtons()
    Const mpRight As Integer = 5945
    Const mpBtnWidth As Integer = 330
    
    Dim i As Integer
    
    With Me.chkLevel
        For i = 0 To (m_iCurrentLevels - 1)
            .Item(i).Left = mpRight - _
                (m_iCurrentLevels - i) * mpBtnWidth
            .Item(i).Visible = True
        Next i
        
        For i = m_iCurrentLevels To 8
            .Item(i).Visible = False
        Next
    End With
End Sub

Sub SelPrevLevels(iYRow As Single)
    Dim i As Integer
    Dim iRow As Integer
    Dim bSelected As Boolean
        Dim iLastSel As Integer
    
'   get row that is currently being selected
    iRow = Me.lstPrevLevels.RowContaining(iYRow)
            
    With Me.lstPrevLevels.SelBookmarks
'       see if this row is in selection
        For i = 0 To .Count - 1
            If .Item(i) = iRow Then
                bSelected = True
                Exit For
            End If
        Next i
        
'       if not already in selection...
        If Not bSelected Then
'           clear all selections unless ctl
'           or shift has been pressed
            If Not (IsPressed(KEY_CTL) Or _
                IsPressed(KEY_SHIFT)) Then
'               clear all selections
                While .Count
                    .Remove 0
                Wend
            ElseIf IsPressed(KEY_SHIFT) Then
                While .Count > 1
                    .Remove 0
                Wend
            End If

'           if shift was pressed, add all rows
'           between last selection and row clicked on
            If IsPressed(KEY_SHIFT) Then
'               get last selection
                iLastSel = .Item(.Count - 1)
                
                If iRow > iLastSel Then
'                   select downward
                    For i = .Item(.Count - 1) To iRow
                        .Add i
                    Next i
                Else
'                   select upward
                    For i = iRow To .Item(.Count - 1)
                        .Add iRow
                    Next i
                End If
            Else
'               add to selection
                .Add iRow
            End If
            
'           set new current row
            Me.lstPrevLevels.Row = iRow
'*****
'the branch below does not work because the
'row will be selected on mouse up anyway
'*****
'        Else
''           the row clicked on is
''           already in selection - remove
''           if ctl has been clicked
'            If IsPressed(KEY_CTL) Then
'                .Remove iRow
'            End If
        End If
    End With
End Sub

Private Sub udNumberPosition_DownClick()
    DecrementBuddyCtl Me.udNumberPosition
End Sub

Private Sub udNumberPosition_UpClick()
    IncrementBuddyCtl Me.udNumberPosition
End Sub

Private Sub udSpaceAfter_DownClick()
    DecrementBuddyCtl Me.udSpaceAfter
End Sub

Private Sub udSpaceAfter_UpClick()
    IncrementBuddyCtl Me.udSpaceAfter
End Sub

Private Sub udSpaceBeforeEntries_DownClick()
    DecrementBuddyCtl Me.udSpaceBeforeEntries
End Sub

Private Sub udSpaceBeforeEntries_UpClick()
    IncrementBuddyCtl Me.udSpaceBeforeEntries
End Sub

Private Sub udSpaceBetweenEntries_DownClick()
    DecrementBuddyCtl Me.udSpaceBetweenEntries
End Sub

Private Sub udSpaceBetweenEntries_UpClick()
    IncrementBuddyCtl Me.udSpaceBetweenEntries
End Sub

Private Sub udTabPosition_DownClick()
    DecrementBuddyCtl Me.udTabPosition
End Sub

Private Sub udTabPosition_UpClick()
    IncrementBuddyCtl Me.udTabPosition
End Sub

Private Sub udTextPosition_DownClick()
    DecrementBuddyCtl Me.udTextPosition
End Sub

Private Sub udTextPosition_UpClick()
    IncrementBuddyCtl Me.udTextPosition
End Sub

Private Sub udTOCLeftIndent_DownClick()
    DecrementBuddyCtl Me.udTOCLeftIndent
End Sub

Private Sub udTOCLeftIndent_UpClick()
    IncrementBuddyCtl Me.udTOCLeftIndent
End Sub

Private Sub udTOCFirstLineIndent_DownClick()
    DecrementBuddyCtl Me.udTOCFirstLineIndent
End Sub

Private Sub udTOCFirstLineIndent_UpClick()
    IncrementBuddyCtl Me.udTOCFirstLineIndent
End Sub

Private Sub ValidatePositionInput(ctlText As VB.TextBox)
'alerts to invalid numeric input, else formats
'to correct number of significant digits
    Dim xFormat As String
    
    With ctlText
        If Not bPositionIsValid(.Text) Then
            On Error Resume Next
            .SetFocus
            On Error GoTo 0
        Else
            .Text = Format(.Text, "#,##0.00")
        End If
    End With
End Sub
Private Sub ValidateSpaceInput(ctlText As VB.TextBox)
'alerts to invalid numeric input, else formats
'to correct number of significant digits
    With ctlText
        If Not bPointsAreValid(.Text) Then
            On Error Resume Next
            .SetFocus
            On Error GoTo 0
        Else
            .Text = Format(.Text, "#,##0")
        End If
    End With
End Sub

