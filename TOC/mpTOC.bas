Attribute VB_Name = "mpTOC"
Option Explicit

Private Const mpCGSSkipItem As Integer = -1
Private Const mpTCCodeStatic As String = " \z"
Private Const mpEndOfTOCWarning = "End of TOC - Do not delete this paragraph!"
Private Const mpNoEntriesWarning As String = _
    "THIS IS THE TABLE OF CONTENTS SECTION. " & vbCr & _
    "NO TABLE OF CONTENTS ENTRIES WERE FOUND!" & vbCr
Private Const mpNoEntriesWarningFrench As String = _
    "CECI EST LA SECTION DE LA TABLE DES MATI�RES. " & vbCr & _
    "AUCUNE ENTR�E TM N'A �T� TROUV�E !" & vbCr
Private Const mpDummyStyle As String = "zzmpTOCEntry"

Private Enum mpTOCContent
    mpTOCContent_FirstSentence = 0
    mpTOCContent_Paragraph = 1
    mpTOCContent_TCEntries = 2
End Enum

Private Type TOCFormat
    IncludePageNumber(1 To 9) As Boolean
    SpaceBefore(1 To 9) As String
    IncludeDotLeaders(1 To 9) As Boolean
    CenterLevel1 As Boolean
    AllCaps(1 To 9) As Boolean
    Bold(1 To 9) As Boolean
    LineSpacing As Integer
    SpaceBetween As String
End Type

Public Enum mpTOCExistsMarker
    mpTOCExistsMarker_None = 0
    mpTOCExistsMarker_Bookmark = 1
    mpTOCExistsMarker_Placeholder = 2
    mpTOCExistsMarker_Field = 3
End Enum

Public Enum mpMarkActions
    mpMarkAction_None = 0
    mpMarkAction_Mark = 1
    mpMarkAction_Unmark = 2
End Enum

Public Enum mpFormatActions
    mpFormatAction_None = 0
    mpFormatAction_Format = 1
    mpFormatAction_Unformat = 2
End Enum

Public Enum mpMarkingModes
    mpMarkingMode_TCCodes = 0
    mpMarkingMode_StyleSeparators = 1
End Enum

Public Enum mpMarkFormatScopes
    mpMarkFormatScope_Document = 0
    mpMarkFormatScope_Section = 1
    mpMarkFormatScope_Selection = 2
End Enum

Public Enum mpTOCDialogStyles
    mpTOCDialogStyle_Text = 0
    mpTOCDialogStyle_Field = 1
    mpTOCDialogStyle_Legacy = 2
End Enum

Public Enum mpMarkingReplacementModes
    mpMarkingReplacementMode_None = 0
    mpMarkingReplacementMode_Prompt = 1
    mpMarkingReplacementMode_Automatic = 2
End Enum

Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)

Function bMarkForTOCRemove(rngSelection As Word.Range) As Boolean
'removes TC code and TCEntryLX
'style from TC Heading

    Dim rngTCField As Word.Range
    Dim fldP As Word.Field
    Dim rngTCHeading As Word.Range
    Dim rngPara As Word.Range
    Dim iLevel As Integer
    Dim fntTCHeading As Word.Font
    Dim paraP As Paragraph
    Dim bIsStyleBased As Boolean
    Dim xScheme As String
    Dim iUnformat As Integer
    Dim bDirectFound As Boolean
    Dim styFont As Word.Font
    Dim bTCFound As Boolean
    
'   cycle through all paras in selection range
    For Each paraP In rngSelection.Paragraphs
    
        Set rngPara = paraP.Range
        
'       get range of TC Field -
'       this is faster than FIND
        Set rngTCField = Nothing
        For Each fldP In rngPara.Fields
            If fldP.Type = wdFieldTOCEntry Then
                Set rngTCField = rngGetField(fldP.Code)
                bTCFound = True
                Exit For
            End If
        Next fldP
        
        If Not (rngTCField Is Nothing) Then
''           prompt to unformat only if there are directly applied
''           ttributes somewhere in the selection;
''           test isn't perfect, but it will eliminate some
''           embarrassing prompts
'            If Not bDirectFound Then
'                With rngPara.Font
'                    If .Bold = wdUndefined Or _
'                            .AllCaps = wdUndefined Or _
'                            .SmallCaps = wdUndefined Or _
'                            .Underline = wdUndefined Or _
'                            .Italic = wdUndefined Then
'                        bDirectFound = True
'                    Else
'                        Set styFont = rngPara.Style.Font
'                        If .Bold <> styFont.Bold Or _
'                                .AllCaps <> styFont.AllCaps Or _
'                                .SmallCaps <> styFont.SmallCaps Or _
'                                .Underline <> styFont.Underline Or _
'                                .Italic <> styFont.Italic Then
'                            bDirectFound = True
'                        End If
'                    End If
'                End With
'                If bDirectFound Then
'                    iUnformat = MsgBox("Would you like to remove " & _
'                        "directly applied attributes from " & _
'                        "formerly marked headings?", _
'                        vbQuestion + vbYesNo + vbDefaultButton2, AppName)
'                End If
'            End If

'           delete field
            rngTCField.Delete
        
''           unformat TCEntry Style
'            If iUnformat = vbYes Then
'                On Error Resume Next
'                xScheme = xGetLTRoot(rngPara.ListFormat.ListTemplate.Name)
'                On Error GoTo 0
'
'                If IsMacPacScheme(xScheme, mpSchemeType_Document) Then
'                    iLevel = rngPara.ListFormat.ListLevelNumber
'                    bIsStyleBased = bTOCLevelIsStyleBased(xScheme, iLevel)
'                End If
'
'                Set rngTCHeading = ActiveDocument.Range(rngPara.Start, _
'                    rngTCField.Start)
'
'                With rngTCHeading
''                   remove all applied manual formats
'                    If Not bIsStyleBased Then
'                        .Font.Reset
'                    Else
'                        If g_bApplyHeadingColor Then _
'                            .Font.ColorIndex = wdAuto
'                    End If
'                End With
'            End If
        End If
    Next paraP
    
'   selection prevents text typed immediately
'   after rngTCHeading from being formatted
'   with previous format
    Selection.Range.Select
    
    bMarkForTOCRemove = bTCFound
End Function


Function bUserChangeTOCTabs() As Long
    Dim oForm As VB.Form
    If g_lUILanguage = wdFrenchCanadian Then
        Set oForm = New frmChangeTOCTabsFrench
    Else
        Set oForm = New frmChangeTOCTabs
    End If
    oForm.Show vbModal
End Function

Function rngFormatTCHeading_Styles(rngTCCode As Word.Range, _
                          iListLevel As Integer, _
                          bReformat As Boolean) As Word.Range
'formats heading by applying TCEntry
'style - this function exists for
'possible future customization of heading
'formatting that cannot be accomodated by
'TCEntry styles

    Dim rngHeading As Word.Range
    
    
'   get TC heading
    Set rngHeading = rngGetTCHeading(rngTCCode)

'   apply style if specified
    If bReformat Then _
        rngHeading.Style = "TCEntryL" & iListLevel

'   return heading
    Set rngFormatTCHeading_Styles = rngHeading
    
End Function

Function bSetTOCTabs(Optional ByVal sIncrement As Single = 3.6, _
                     Optional ByVal iLevel As Integer = 0) As Boolean

'expands or contracts space between
'tab settings for TOC 1-9 styles
    Dim i As Integer
    Dim j As Integer
    Dim styTOC As Word.Style
    Dim xTOCStyle As String
    Dim tabTab As TabStop
    Dim sglModifyBy As Single
    Dim tocExisting As Word.TableOfContents
    Dim iMinLevel As Integer
    Dim iMaxLevel As Integer
    
    Application.ScreenUpdating = False

'   set scope of changes - if iLevel
'   is 0 do all levels, else do
'   level iLevel only
    If iLevel = 0 Then
        iMinLevel = 1
        iMaxLevel = 9
    Else
        iMinLevel = iLevel
        iMaxLevel = iLevel
    End If
    
'   cycle through specified TOC styles
    For i = iMinLevel To iMaxLevel
        xTOCStyle = xTranslateTOCStyle(i)
        Set styTOC = ActiveDocument.Styles(xTOCStyle)
        With styTOC.ParagraphFormat
'           modify second line indent
            .FirstLineIndent = .FirstLineIndent - sIncrement
            .LeftIndent = .LeftIndent + sIncrement
            With .TabStops
'               cycle through all tab stops, excluding
'               last one (that's the page number tab)
                For j = 1 To .Count - 1
                    sglModifyBy = sIncrement * j
                    .Item(j).Position = .Item(j).Position + _
                                                sglModifyBy
                Next j
            End With
        End With
    Next i
    
    Application.ScreenUpdating = True
    
End Function

Function bReApplyTOCStyles() As Boolean
        Dim i As Integer
    Dim j As Integer
    Dim styTOC As Word.Style
    Dim xTOCStyle As String
    Dim tabTab As Word.TabStop
    Dim sglModifyBy As Single
    
    With Application
        If g_lUILanguage = wdFrenchCanadian Then
            .StatusBar = "Mise � jour des Styles TM..."
        Else
            .StatusBar = "Updating TOC Styles..."
        End If
        .ScreenUpdating = False
'       cycle through TOC 1-9
        For i = 1 To 9
            xTOCStyle = xTranslateTOCStyle(i)
            With ActiveDocument.Bookmarks("mpTableOfContents").Range.Find
                .ClearFormatting
                .Style = xTOCStyle
                .Replacement.Style = xTOCStyle
                .Execute Replace:=wdReplaceAll
            End With
        Next i
        
        .StatusBar = ""
    End With
End Function

Function xResetTOCStyles(xScheme As String, _
                        Optional secTOC As Word.Section, _
                        Optional bCenterLevel1 As Boolean = False, _
                        Optional bIncludeSchedule As Boolean = False, _
                        Optional bDoScheduleOnly As Boolean) As String
'redefines TOC 1-9 styles based
'on scheme xScheme - TOC styles are
'now stored in document itself.
'returns scheme applied- this
'is not necessarily xScheme. if xScheme
'is not valid, return value will be "Generic"

    Dim xStyleSourceFile As String
    Dim xStyleDestFile As String
    Dim xStyle As String
    Dim iRightTabPos As Integer
    Dim xTOCStyle As String
    Dim stySourceStyle As Word.Style
    Dim styTOCStyle As Word.Style
    Dim styNormal As Word.Style
    Dim bDeleteStyle As Boolean
    Dim sSpacing As Single
    Dim i As Integer
    Dim udtFormat As TOCFormat
    Dim iStart As Integer
    
    On Error GoTo xResetTOCStyles_Error
    
    'exit if nothing to do
    If (Not bIncludeSchedule) And bDoScheduleOnly Then Exit Function
        
    With Application
        .ScreenUpdating = False
        xStyleSourceFile = xTOCSTY
    End With
    
'   use WordBasic function to bypass ODMA based return
    xStyleDestFile = WordBasic.FileName$()
    
'   get normal style for future use
    Set styNormal = ActiveDocument.Styles(wdStyleNormal)
    
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "Applique th�me " & xScheme & " TM"
    Else
        Application.StatusBar = "Applying " & xScheme & " TOC Scheme"
    End If
    g_oStatus.Show 4, g_xTOCStatusMsg
    
'*****revised for 2000*****
'style used to be always copied from mpNumbers.sty
'and then deleted at the end of the routine
'now only do this if style isn't in doc, e.g. "Generic"
    If bDoScheduleOnly Then
        iStart = 9
    Else
        iStart = 1
    End If
    
    For i = iStart To 9
        If i = 9 And bIncludeSchedule Then
'           copy schedule prototype in place of level 9 of scheme
            xStyle = "SpecialTOC9"
        Else
            If xScheme = "Custom" Then
                If bCenterLevel1 Then
                    xStyle = "CenteredL1TOC" & i
                Else
                    xStyle = "StandardTOC" & i
                End If
            Else
                xStyle = xScheme & "TOC" & i
            End If
        End If
        
        xTOCStyle = xTranslateTOCStyle(i)
        
        On Error Resume Next
        Set stySourceStyle = Nothing
        Set stySourceStyle = ActiveDocument.Styles(xStyle)
        On Error GoTo xResetTOCStyles_Error
        
        If stySourceStyle Is Nothing Then
'           copy style into active document
            If xStyle = "SpecialTOC9" Then
                On Error Resume Next
            End If
            Application.OrganizerCopy xStyleSourceFile, _
                                      xStyleDestFile, _
                                      xStyle, _
                                      wdOrganizerObjectStyles
            If xStyle = "SpecialTOC9" Then
                If Err = mpStyleDoesNotExist Then
'                   if new TOC 9 style isn't in mpTOC.sty, exit w/o modifying level
                    On Error GoTo xResetTOCStyles_Error
                    Exit For
                Else
                    On Error GoTo xResetTOCStyles_Error
                End If
            End If
            bDeleteStyle = True
        End If
                           
        With ActiveDocument.Styles
            Set styTOCStyle = .Item(xTOCStyle)
            Set stySourceStyle = .Item(xStyle)
        End With
        
'       set TOC style to "look like" copied style
        With styTOCStyle
            .BaseStyle = stySourceStyle.BaseStyle
            If Not g_bIsXP Then
'               this property was "unimplemented" in the June 2001
'               release of Word XP
                .Borders = stySourceStyle.Borders
            End If
            .Font = stySourceStyle.Font
            With .Font
                .Name = styNormal.Font.Name
                .Size = styNormal.Font.Size
            End With
            .NextParagraphStyle = stySourceStyle.NextParagraphStyle
            .ParagraphFormat = stySourceStyle.ParagraphFormat
            .LanguageID = styNormal.LanguageID

'           adjust for exact line spacing
            If styNormal.ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
                sSpacing = styNormal.ParagraphFormat.LineSpacing
                With .ParagraphFormat
                    .LineSpacingRule = wdLineSpaceExactly
                    Select Case stySourceStyle.ParagraphFormat.LineSpacingRule
                        Case wdLineSpaceSingle
                            .LineSpacing = sSpacing
                        Case wdLineSpace1pt5
                            .LineSpacing = 1.5 * sSpacing
                        Case wdLineSpaceDouble
                            .LineSpacing = 2 * sSpacing
                    End Select
                End With
            End If
            
            '2/18/13 - temporary workaround for Word 2103 bug in which tab stops
            'beyond the right indent are ignored
            'GLOG 5177 (7/16/13) - Microsoft fixed this bug
'            If InStr(Word.Application.Version, "15.") <> 0 Then
'                If mdlWord14.GetCompatibilityMode(ActiveDocument) = 15 Then
'                    .ParagraphFormat.RightIndent = 0
'                End If
'            End If
        End With
                
'       delete copied style
        If bDeleteStyle Then
            Application.OrganizerDelete xStyleDestFile, _
                                        xStyle, _
                                        wdOrganizerObjectStyles
        End If
    Next i
    
'   redefine generic TOC styles based on values in user.ini
    If xScheme = "Custom" Then
        udtFormat = udtGetTOCFormat("Custom", bIncludeSchedule)
        bRet = bCustomizeGenericStyles(udtFormat, bIncludeSchedule)
    End If
    
'   clean up
    Application.StatusBar = ""

'   return
    xResetTOCStyles = xScheme
    
    Exit Function
    
xResetTOCStyles_Error:
    Select Case Err.Number
        Case mpStyleDoesNotExist
'           prompt user that Generic
'           TOC Scheme will be applied
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Th�me TM " & xScheme & _
                        " non d�fini. Le th�me TM de base sera appliqu�.", _
                        vbExclamation, "Num�rotation MacPac"
            Else
                MsgBox "The TOC scheme " & xScheme & _
                        " is not fully defined.  " & _
                        "The Generic TOC Scheme will be applied.", _
                        vbExclamation, AppName
            End If

'           redefine from level 1 onward
            i = 1
            
            xScheme = TOCSchemeGeneric
            xStyle = xScheme & "TOC" & i
            xTOCStyle = xTranslateTOCStyle(i)
            Resume
        Case Else
            MsgBox Error(Err.Number)
    End Select
    
    Exit Function
    
End Function
Function bSetFormatHeadingsButton() As Boolean
'   changes button position
'   based on INI value

    Dim bCurrentSetting As Boolean
    Dim btnTbrFormatHeadings As CommandBarButton
    
    bCurrentSetting = System.PrivateProfileString(xFirmIni, "Numbering", "FormatHeadings")
    
'   get toolbar button that calls this function
    Set btnTbrFormatHeadings = CommandBars("MacPac Numbering").Controls("TC Format")
    
    If bCurrentSetting = True Then
        btnTbrFormatHeadings.State = msoButtonDown
    Else
        btnTbrFormatHeadings.State = msoButtonUp
    End If
End Function


Function bGetParaNumberInfo(rngCurPara As Word.Range, _
                            ltCurrent As ListTemplate, _
                            iListLevel As Integer, _
                            iDefaultListLevel As Integer) As Boolean
                       
'   gets list template and list level
'   of range rngCurPara.  Different techniques
'   for differently numbered paras.
    
    With rngCurPara.ListFormat
        If .ListType = wdListOutlineNumbering Or _
                .ListType = wdListSimpleNumbering Then
                
            iListLevel = .ListLevelNumber
            Set ltCurrent = .ListTemplate
            
        ElseIf iDefaultListLevel > 0 Then
            iListLevel = iDefaultListLevel
            Set ltCurrent = Nothing
        Else
            iListLevel = iGetTOCLevel(rngCurPara)
            Set ltCurrent = Nothing
        End If
    End With
End Function

Function bMarkForTOC(Optional ByVal bFormat As Boolean = False) As Boolean
'   inserts empty TC field at insertion and
'   applies appropriate TCEntryLX style -

    Dim fldCurField As Word.Field
    Dim fldExisting As Word.Field
    Dim iLevel As Integer
    Dim rngTCCodeStart As Word.Range
    Dim bAutoFormat As Boolean
    Dim bShowAllStart As Boolean
    Dim ltCurrent As Word.ListTemplate
    Dim xStyle As String
    Dim xScheme As String
    Dim bIsOutlineLevel As Boolean
    Dim bIsMPLT As Boolean
    
'******VALIDATE ENVIRONMENT******
    With Selection
'       ensure that selection is insertion
        If .Type <> wdSelectionIP Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Ne pas s�lectionner le texte avant d'effectuer cette proc�dure. Placer le curseur � la fin du texte qui doit �tre marqu� comme TM et r�essayer.", _
                    vbExclamation, "Num�rotation MacPac"
            Else
                MsgBox "Do not select text before running this procedure.  " & _
                       "Position cursor after the text to be marked " & _
                       "for TOC and try again.", vbExclamation, AppName
            End If
            Exit Function
        End If
    End With
'******END VALIDATE ENVIRONMENT******
    
    'remove existing style separator
    bRemoveStyleSeparators Selection.Range
    
'   test for existence of tc code-
'   remove if there is one
    For Each fldExisting In Selection.Paragraphs(1).Range.Fields
        If fldExisting.Type = wdFieldTOCEntry Then
'           unmark for TOC
            bMarkForTOCRemove Selection.Range
'           xMsg = "Paragraph is already marked for table of contents."
'           MsgBox xMsg, vbExclamation, AppName
'           Exit Function
        End If
    Next fldExisting
        
    With ActiveDocument
        With Selection
            Set rngTCCodeStart = ActiveDocument.Range(.Start, .End)
        End With

'       force show all - this mimics Word's
'       behavior with TC marking
        With ActiveWindow.View
            bShowAllStart = .ShowAll
            .ShowAll = True
        End With
            
'       get paragraph level
        With rngTCCodeStart
            iLevel = .ParagraphFormat.OutlineLevel
            bIsOutlineLevel = iLevel <> wdOutlineLevelBodyText
            If bIsOutlineLevel Then
                On Error Resume Next 'GLOG 2847 - moved up a line
                xStyle = .Style
                xScheme = xGetLTRoot(.ListFormat.ListTemplate.Name)
                On Error GoTo 0
                
                If xScheme <> "" Then
                    bIsMPLT = bIsMPListTemplate(.ListFormat _
                        .ListTemplate)
                End If
            End If
        End With
            
        If bIsMPLT Or _
                bIsHeadingStyle(xStyle) Or _
                (xConditionalScheme(rngTCCodeStart) <> "") Then
            Application.ScreenUpdating = False
'           add tc field  - do not insert any text in code
            Set fldCurField = .Fields.Add(rngTCCodeStart, _
                                              wdFieldTOCEntry, _
                                              , _
                                              False)
        Else
            If Not bIsOutlineLevel Then
'               prompt for level
                iLevel = iGetParagraphLevel(rngTCCodeStart, 0)
            End If
            
            If iLevel > 0 Then
                Application.ScreenUpdating = False
'               insert tc field with specified level as text
                Set fldCurField = .Fields.Add(rngTCCodeStart, _
                    wdFieldTOCEntry, _
                    "\l " & Chr(34) & iLevel & Chr(34), _
                    False)
            Else
                ActiveWindow.View.ShowAll = bShowAllStart
                Application.ScreenUpdating = True
                Exit Function
            End If
            
        End If
        
'       remove formatting from TC field
        With rngTCCodeStart
            .MoveEnd wdParagraph
            .Fields(1).Code.Font.Reset
        End With
        
        If iLevel And bFormat Then
'           format heading and number to appropriate level
            rngFormatTCHeading rngTCCodeStart, _
                               iLevel, _
                               bFormat
        End If
        
        Application.ScreenUpdating = True
    End With
    Exit Function
bMarkForTOC_Error:
    Application.ScreenUpdating = True
    bMarkForTOC = Err
End Function

Function bInsertTCEntry()
    Dim rngLocation As Word.Range
    Dim rngHeading As Word.Range
    Dim xCurNumber As String
    Dim lSelStartPos As Long
    Dim lParaStartPos As Long
    Dim xTCEntry As String
    Dim iOutlineLevel As Integer
    
    Set rngLocation = Selection.Range
    
    With rngLocation
    
'       get number and number format if any
        With .ListFormat
            If .ListType = wdListOutlineNumbering Or _
                .ListType = wdListMixedNumbering Or _
                .ListType = 1 Then
                 xCurNumber = .ListString
                 iOutlineLevel = .ListLevelNumber
                 
            End If
        End With
        
'       get text from insertion to start of para
        lSelStartPos = .Start
        lParaStartPos = .Paragraphs(1).Range.Start
        Set rngHeading = ActiveDocument.Range(lParaStartPos, lSelStartPos)
        
'       concatenate
        xTCEntry = xCurNumber & vbTab & rngHeading
        
'       insert TC field
        ActiveDocument.Fields.Add rngLocation, _
                                  wdFieldTOCEntry, _
                                  xTCEntry, _
                                  True
                                  
        
    End With
    
End Function

Function bResetTCEntries()
    Dim fldP As Word.Field
    Dim i As Integer
    Dim l As Long
    Dim lFields As Long
    Dim iLevel As Integer
    Dim iPos As Integer
    Dim iPosStatic As Integer
    Dim rngC As Word.Range
    
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "R�initialisation Entr�es de TM: 0%"
    Else
        Application.StatusBar = "Resetting TC Entries: 0%"
    End If
    g_oStatus.Show 98, g_xTOCStatusMsg
    
    lFields = ActiveDocument.Fields.Count
    
    For Each fldP In ActiveDocument.Fields
        If fldP.Type = wdFieldTOCEntry Then
        
'           clear out only mpTCCodeStatic
'           mark from static tc codes
            Set rngC = fldP.Code
            iPosStatic = InStr(UCase(rngC), UCase(mpTCCodeStatic))
            If iPosStatic Then
'               delete static marking
                rngC.MoveStart wdCharacter, iPosStatic - 1
                rngC.Text = ""
            Else
'               get level
                iPos = InStr(UCase(rngC), "\L """)
                If iPos Then
'                   is filled
                    rngC = _
                        " " & g_xTCPrefix & " \l """ & Mid(rngC, iPos + 4, 1) & """"
                Else
                    rngC = " " & g_xTCPrefix & " "
                End If
            End If
        End If
        l = l + 1
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = "R�initialisation Entr�es de TM: " & _
                                    Format(l / lFields, "0%")
        Else
            Application.StatusBar = "Resetting TC Entries: " & _
                                    Format(l / lFields, "0%")
        End If
        g_oStatus.Show 98, g_xTOCStatusMsg
    Next fldP
    
    Application.StatusBar = ""
End Function

Function bInsertTOCAsField() As Long
'inserts a MacPac TOC
    Dim iMaxLevel As Integer
    Dim iMinLevel As Integer
    Dim rngLocation As Word.Range
    Dim fldTOC As Word.Field
    Dim xTOCParams As String
    Dim rngTOC As Word.Range
    Dim xLevRange As String
    Dim iNumStyles As Integer
    Dim xStyleList As String
    Dim bShowAll As Boolean
    Dim bShowHidden As Boolean
    Dim iView As Integer
    Dim ds As Date
    Dim xMsg As String
    Dim iEntryType As mpTOCEntryTypes
    Dim udtTOCFormat As TOCFormat
    Dim i As Integer
    Dim xTestContent As String
    Dim xTimeMsg As String
    Dim lNumParas As Long
    Dim lWarningThreshold As Long
    Dim iLoc As Integer
    Dim bkmkTOCEntry As Word.Bookmark
    Dim bShowHiddenBkmks As Boolean
    Dim bSmartQuotes As Boolean
    Dim lZoom As Long
    Dim bIncludeSchedule As Boolean
    Dim bIncludeOther As Boolean
    Dim xInclusions As String
    Dim xExclusions As String
    Dim xTOC9Style As String
    Dim oDummyStyle As Word.Style
    Dim rngDummyPara As Word.Range
    Dim bIsCentered As Boolean
    Dim iDummySec As Integer
    Dim bColumnsReformatted As Boolean
    Dim lStartTick As Long
    Dim sElapsedTime As Single
    Dim lShowTags As Long
    Dim dlgTOC As VB.Form
    Dim xScheme As String
    Dim bPrintHiddenText As Boolean
    Dim iHeadingDef As Integer
    Dim oDoc As Word.Document
    Dim xLevel9Style As String '9.9.5001
    Dim iMarkReplacementMode As mpMarkingReplacementModes
    Dim rngShiftReturn As Word.Range
    Dim lPos As Long
    Dim lSearchPos As Long
    Dim lParaStart As Long 'GLOG 5495
    Dim lXMLSafeParaStart As Long 'GLOG 5495
    Dim iInlineTags As Integer 'GLOG 5495
    Dim rngTest As Word.Range 'GLOG 5495
    
    On Error GoTo ProcError
    
    'OutputDebugString "Start of TOC macro"
    
'   validate environment
    If g_bOrganizerSavePrompt And (WordBasic.FileNameFromWindow$() = "") Then
        'prompt to save document
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "Cette macro n'est pas disponible dans un document non sauf-gard�.  Veuillez sauf-garder votre document avant d�ex�cuter cette macro." & _
                vbCr & vbCr & "Nous vous prions de nous excuser pour tout inconv�nient mais c'est un workaround provisoire pour un bogue de Microsoft.  Le bogue a �t� introduit r�cemment dans Word 2007 SP2 et affecte la copie des styles par l'interm�diaire du code utilisant la fonction de l�Organisateur de Word.  Le correctif de Microsoft devrait �tre disponible dans les deux prochains mois.", _
                vbInformation, "Num�rotation MacPac"
        Else
            MsgBox "This macro is not available in an unsaved document.  " & _
                "Please save your document and run this macro again." & vbCr & vbCr & _
                "We apologize for the inconvenience but this is a temporary " & _
                "workaround for a Microsoft bug that affects copying styles via code using Word's Organizer " & _
                "feature.  Microsoft's hotfix should be available within a couple months.", _
                vbInformation, AppName
        End If
        Exit Function
    ElseIf g_xTOCLocations(0) = "" Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Ne peut cr�er TM, il n'y a pas d'emplacement d�sign� dans mpn90.ini.  Contactez votre administrateur."
        Else
            xMsg = "Cannot create TOC because there are no TOC locations specified in " & _
                "mpn90.ini.  Please see your system administrator."
        End If
    Else
        On Error Resume Next
        xTestContent = ActiveDocument.Characters(2).Text
        If Err <> 0 Then
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "Ne peut cr�er TM.  Le pr�sent document est vide."
            Else
                xMsg = "Cannot create TOC.  The " & _
                       "active document is empty."
            End If
        End If
        On Error GoTo ProcError
    End If
    
    If Len(xMsg) Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox xMsg, vbExclamation, "Num�rotation MacPac"
        Else
            MsgBox xMsg, vbExclamation, AppName
        End If
        Exit Function
    End If
    
'   warn about large document
    lWarningThreshold = Val(xAppGetFirmINIValue("TOC", _
        "NumberOfParasWarningThreshold"))
    lNumParas = ActiveDocument.Paragraphs.Count
    
    If lWarningThreshold > 0 And _
            lNumParas > lWarningThreshold Then
        If g_lUILanguage = wdFrenchCanadian Then
            lRet = MsgBox("Ce document contient " & lNumParas & " paragraphes.  MacPac TM macro fait beaucoup plus que les fonctions natives TM Word.  La mise en forme du document prendra quelques temps.  D�sirez-vous continuer ?", _
                vbYesNo + vbQuestion, "Num�rotation MacPac")
        Else
            lRet = MsgBox("This document contains " & lNumParas & " paragraphs.  " & _
                "The MacPac TOC macro does a lot more work than " & _
                "the native Word TOC function.  Formatting a document " & _
                "of this size will take some time.  Do you wish to continue?", _
                vbYesNo + vbQuestion, AppName)
        End If
        If lRet = vbNo Then Exit Function
    End If
    
    Set oCustTOC = New mpctoc.CCustomTOC
    
    If g_lUILanguage = wdFrenchCanadian Then
        Set dlgTOC = New frmInsertTOCFrench
    Else
        Set dlgTOC = New frmInsertTOC
    End If

'   mpTOC.sty is now only loaded as needed (12/10/01)
    LoadTOCSty
    
'   call any custom code
    lRet = oCustTOC.lBeforeDialogShow(dlgTOC)
    If lRet Then
        Err.Raise lRet, _
            "oCustTOC.lBeforeDialogShow", _
            Err.Description
    End If
    
    'OutputDebugString "Before form display"
    
    dlgTOC.Show vbModal
    
    'OutputDebugString "After form display"
    lStartTick = CurrentTick()
        
    If dlgTOC.Cancelled Then
        UnloadTOCSty
        Unload dlgTOC
        Set dlgTOC = Nothing
        Set oCustTOC = Nothing
        Exit Function
    End If
    
'   call any custom code
    lRet = oCustTOC.lAfterDialogShow()
    If lRet Then
        Err.Raise lRet, _
            "oCustTOC.lAfterDialogShow", _
            Err.Description
    End If

    'GLOG 5147
    Set oDoc = ActiveDocument
    
'   display status message
    Set g_oStatus = New CStatus
    With g_oStatus
        If g_lUILanguage = wdFrenchCanadian Then
            .Title = "Ins�rer Table des mati�res"
        Else
            .Title = "Inserting Table of Contents"
        End If
        .ProgressBarVisible = True
        .Show 2, g_xTOCStatusMsg
    End With

    'GLOG 5057 (2/1/12) - disable Print Hidden Text option in order
    'to prevent inaccurate page numbers
    bPrintHiddenText = Application.Options.PrintHiddenText
    If bPrintHiddenText Then _
        Application.Options.PrintHiddenText = False
    
'   delete hidden TOC bookmarks - prevents potential
'   "insufficient memory" error when generating TOC
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "G�n�re TM.  Veuillez patienter..."
    Else
        Application.StatusBar = "Generating TOC.  Please wait..."
    End If
    
    With ActiveDocument.Bookmarks
        bShowHiddenBkmks = .ShowHidden
        .ShowHidden = True
    End With
    For Each bkmkTOCEntry In ActiveDocument.Bookmarks
        With bkmkTOCEntry
            If Left(.Name, 4) = "_Toc" Then
                .Delete
                If Not g_bPreserveUndoListTOC Then _
                    ActiveDocument.UndoClear
            End If
        End With
    Next bkmkTOCEntry
    ActiveDocument.Bookmarks.ShowHidden = bShowHiddenBkmks
    
'   necessary for speed - this function executes
'   a large number of Word actions.  the undo buffer
'   fills up after 2 iterations, so to avoid a
'   msg to the user, clear out first.
    If Not g_bPreserveUndoListTOC Then _
        ActiveDocument.UndoClear
    
'   get/set environment
    With Application
        .ScreenRefresh
        .ScreenUpdating = False
    End With
    
    EchoOff
    With ActiveWindow.View
'       get original settings
        bShowAll = .ShowAll
        bShowHidden = .ShowHiddenText
        iView = .Type
        lZoom = .Zoom.Percentage
        
'       modify environment
        .ShowAll = True
        .ShowHiddenText = True
        .Type = wdNormalView
        .Zoom.Percentage = 100
    End With

    'hide xml tags
    If g_bXMLSupport Then _
        lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, False)

'   turn off track changes
    ActiveDocument.TrackRevisions = False
    
'   turn off smart quotes
    With Application.Options
        bSmartQuotes = .AutoFormatAsYouTypeReplaceQuotes
        .AutoFormatAsYouTypeReplaceQuotes = False
    End With
                    
    With dlgTOC
'       convert TOC location from string to integer
        Select Case .cbxInsertAt.Text
            Case mpTOCLocationEOF, mpTOCLocationEOFFrench
                iLoc = mpAtEOF
            Case mpTOCLocationBOF, mpTOCLocationBOFFrench
                iLoc = mpAtBOF
            Case mpTOCLocationAboveTOA, mpTOCLocationAboveTOAFrench
                iLoc = mpAboveTOA
            Case Else
                iLoc = mpAtInsertion
        End Select
        
'       store user choices
        iMaxLevel = .cbxMaxLevel
        iMinLevel = .cbxMinLevel
        
'       get inclusions/exclusions
        If .optInclude(0) Then
'           schemes
            xInclusions = .Inclusions
            xExclusions = .Exclusions
        ElseIf .optInclude(1) Then
'           individual styles
            xInclusions = .StyleInclusions
            xExclusions = .StyleExclusions
        End If
        bIncludeSchedule = InStr(UCase(xInclusions), ",SCHEDULE,")
        bIncludeOther = InStr(UCase(xInclusions), ",OTHER,")
        If .chkApplyTOC9 Then
            xTOC9Style = .cbxScheduleStyles
        End If
        
'       if including Schedule styles, ensure that outline level is in range
        If .optInclude(1) Or bIncludeSchedule Then
            SetScheduleLevels iMinLevel, xTOC9Style
        End If

'       get TOC format
        udtTOCFormat = udtGetTOCFormat(.TOCScheme, .chkApplyTOC9)
    
'       Word only allows 17 styles to be
'       explicitly included in toc  - if there
'       are more than 17, tag all outline styles
'       between min and max levels, else tag
'       only those outline level styles in the
'       inclusion list that are between the min
'       and max levels
'       GLOG 5113 (9/24/12) - Word long ago eliminated this limit - we now only need to
'       worry about this when it's specified in the ini
        '10/29/12 (dm) - I was wrong about this native limit having been eliminated -
        'g_bUseTOCStyleLimitWorkaround has been hardcoded to TRUE
        If g_bUseTOCStyleLimitWorkaround Or bIncludeOther Then
            If .optInclude(1) Then
                iNumStyles = lCountChrs(xInclusions, ",") - 1
            ElseIf .optInclude(2) Then
                iNumStyles = 18
            Else
                iNumStyles = iMaxLevel * (lCountChrs(xInclusions, ",") - 1)
                If bIncludeOther Then
    '               we need to bring in all styles
                    iNumStyles = 18
                ElseIf bIncludeSchedule Then
    '               schedule styles are not one per level - adjust total
                    iNumStyles = iNumStyles - (iMaxLevel - iMinLevel) + UBound(g_xScheduleStyles)
                End If
            End If
        End If
        
        If iNumStyles <= 17 Then
            If .optInclude(1) Then
                '9.9.5001 - Apply TOC 9 option needs to be implemented
                'via field switches - this option was previously disabled
                'when inserting as field
                xLevel9Style = xTOC9Style
                xStyleList = xGetDesignatedStyles(xInclusions, xLevel9Style)
            Else
                xStyleList = xGetStyleList(xInclusions, _
                                           iMinLevel, _
                                           iMaxLevel, _
                                           .chkApplyTOC9, _
                                           .cbxScheduleStyles)
            End If
        End If
        
        'GLOG 5174 - replace shift-returns that aren't at start of paragraph -
        'note positioning of this code to avoid shift-returns in TC codes
        '9.9.5009 - moved this block up to before section insertion to prevent
        'crashing after inserting pleading paper in Forte 11.0.0.9
        If .chkStyles Then
            lPos = -1
            With ActiveDocument.Content.Find
                .ClearFormatting
                .Text = Chr(11)
                .Wrap = wdFindContinue
                .Execute
                Do While .Found
                    Set rngShiftReturn = .Parent
                    
                    'GLOG 5495 - account for block-level bounding objects
                    lParaStart = mdlCC.GetCCSafeParagraphStart(rngShiftReturn)
                    lXMLSafeParaStart = mdlWordXML.GetTagSafeParagraphStart( _
                        rngShiftReturn)
                    If lXMLSafeParaStart > lParaStart Then _
                        lParaStart = lXMLSafeParaStart
                    
                    'GLOG 5495 - account for inline bounding objects
                    'between start of para and shift-return
                    Set rngTest = rngShiftReturn.Duplicate
                    rngTest.SetRange lParaStart, rngTest.Start
                    iInlineTags = mdlCC.CountCCsEndingInRange(rngTest)
                    iInlineTags = (iInlineTags + _
                        mdlWordXML.CountTagsEndingInRange(rngTest)) * 2
                    
                    With rngShiftReturn
                        If .Start <= lSearchPos Then
                            'we're back at the start
                            'GLOG 5263 - changed from < to <=
                            Exit Do
                        ElseIf (.Start = lParaStart + iInlineTags) Or _
                                (.Start = lPos + 1) Then
                            'skip trailing character
                            lPos = .Start
                        Else
                            'replace
                            .Text = "zzmpShiftReturn"
                            lPos = -1
                        End If
                        lSearchPos = .Start
                    End With
                    .Execute
                Loop
            End With
        End If
            
'       call any custom code
        lRet = oCustTOC.lBeforeTOCSectionInsert()
        If lRet Then
            Err.Raise lRet, _
                "oCustTOC.lBeforeTOCSectionInsert", _
                Err.Description
        End If
        
'       set location for insertion of TOC -
'       MacPac TOC is bookmarked as mpTableOfContents
        'OutputDebugString "Before section insert"
        Set rngLocation = rngGetTOCTarget(ActiveDocument, _
                                          iLoc, _
                                          .BoldHeaderTOC, _
                                          .BoldHeaderPage, _
                                          .CapHeaderTOC, _
                                          .CapHeaderPage, _
                                          .UnderlineHeaderTOC, _
                                          .UnderlineHeaderPage, _
                                          .NumberStyle, _
                                          .NumberPunctuation, _
                                          Not .ContinuePageNumbering)
                                          
'       format columns
        With rngLocation.Sections(1).PageSetup.TextColumns
            If dlgTOC.chkTwoColumn And (.Count = 1) Then
                .SetCount 2
                .Spacing = InchesToPoints(0.5)
                .EvenlySpaced = True
                bColumnsReformatted = True
            ElseIf (dlgTOC.chkTwoColumn = 0) And (.Count = 2) Then
                .SetCount 1
                bColumnsReformatted = True
            End If
        End With
        
        'OutputDebugString "After section insert"
        
'       call any custom code
        lRet = oCustTOC.lAfterTOCSectionInsert(rngLocation, _
            bColumnsReformatted)
        If lRet Then
            Err.Raise lRet, _
                "oCustTOC.lAfterTOCSectionInsert", _
                Err.Description
        End If
        
        'new mark all option
        If g_iTOCDialogStyle <> mpTOCDialogStyle_Text Then
            'get heading definition
            iHeadingDef = .cbxHeadingDef.SelectedItem
            
            'add TC codes or style seps if specified
            If .chkMark Then
                Dim xTargetStyles As String
                Dim iMarkingType As mpMarkingModes
                Dim xDelimiter As String
                
                If .optInclude(2) Then
                    'GLOG 5298 - if "All Paragraphs of Specified Outline Levels" option
                    'is selected, use wildcard-type format - we were previously failing to
                    'mark at all with this option
                    For i = iMinLevel To iMaxLevel
                        xTargetStyles = xTargetStyles & "*_L" & CStr(i) & "|"
                    Next i
                Else
                    'replace commas with pipes
                    xTargetStyles = Replace(xStyleList, ",", "|")
                End If
                
                'get marking type
                If .chkStyles Then
                    iMarkingType = mpMarkingMode_StyleSeparators
                Else
                    iMarkingType = mpMarkingMode_TCCodes
                End If
                
                'get heading delimiter
                If iHeadingDef = 0 Then
                    xDelimiter = mpSentencePeriodOne
                Else
                    xDelimiter = mpSentencePeriodTwo
                End If
                
                'get replacement mode - convert automatically unless creating from
                'both style separators and TC entries
                If .chkStyles + .chkTCEntries = 1 Then
                    iMarkReplacementMode = mpMarkingReplacementMode_Automatic
                Else
                    iMarkReplacementMode = mpMarkingReplacementMode_None
                End If
                
                'mark all specified styles
                bMarkAndFormatHeadings xTargetStyles, mpMarkAction_Mark, _
                    mpFormatAction_None, iMarkingType, xDelimiter, _
                    mpMarkFormatScope_Document, False, iMarkReplacementMode
            End If
        End If
        
'       Reset TOC scheme based on user choice if specified
        If .optTOCStyles(0) Or .chkApplyTOC9 Then
            xResetTOCStyles .TOCScheme, _
                            rngLocation.Sections(1), _
                            udtTOCFormat.CenterLevel1, _
                            .chkApplyTOC9, _
                            .optTOCStyles(1)
        End If
        
'       reset right tab stop if switching column format or if
'       style update was requested by user
        If .optTOCStyles(0) Or bColumnsReformatted Then
            ResetRightTabStop rngLocation.Sections(1)
        End If
        'OutputDebugString "After resetting styles"
        
        '9/7/12 - this is used for FillTCCodes and CleanUpTOCField
        bIsCentered = (ActiveDocument.Styles(wdStyleTOC1) _
            .ParagraphFormat.Alignment = wdAlignParagraphCenter)
        
        'if generating from TC fields and leaving TOC field linked,
        'we need to fill tc codes; since we're also advertising this
        'as a shortcut to avoid having to run the "Convert To Word TC Codes"
        'after running mark all, we need to fill dynamic codes even when
        'these entries were actually generated from styles; but since
        'CleanUpTOCField can't delete the style list after generation from
        'both TC fields and styles, because it can't assume that all
        'numbered paras have been marked, it becomes a training issue
        'to generate from TC fields ONLY in this situation, to avoid double
        'entries after a native update; the real answer is to offer filled TC
        'codes as an option in Mark All and to only fill partially dynamic codes here
        '9/27/12 (dm) - don't replace shift-returns in centered level one -
        'for this reason, moved this block to after xResetTOCStyles
        If .chkTCEntries Then _
            RestoreMPTCCodes
            
        'fill tc codes
        If .chkTCEntries Then
            FillTCCodes .chkApplyManualFormatsToTOC, False, Not bIsCentered
        
            'fill partially dynamic codes with text
            'from left of code to beginning of para
            '9.9.5005 - made conditional on .chkTCEntries
            iRefreshPDynCodes 0, "", , .chkApplyManualFormatsToTOC, Not bIsCentered
        End If
        
        'OutputDebugString "After updating TC entries"
    End With
    
'   count number of styles to be included for TOC Entries
    If dlgTOC.optInclude(1) Then
        xLevRange = "1-9"
    Else
        xLevRange = iMinLevel & "-" & iMaxLevel
    End If
    
'   Word only allows 17 styles to be
'   explicitly included in toc - go figure
    If dlgTOC.chkStyles = 1 Then
        If iNumStyles > 17 Then
'           create TOC based on TC Entries
'           and outline level paragraphs
            xTOCParams = "\o """ & xLevRange & """" & "\w"
            
            '9.9.5002 - if generating from outline levels, add paragraphs
            If dlgTOC.optInclude(2) Then _
                xTOCParams = xTOCParams & " \u"
        Else
'           if system decimal separator is comma, use semi-colons in field
            '9.9.3004 - get system list separator and adjust string if necessary -
            'we were previously inferring the list separator from the decimal separator
            If Application.International(wdListSeparator) <> "," Then
                xStyleList = xSubstitute(xStyleList, ",", _
                    Application.International(wdListSeparator))
            End If
            
'           create TOC from explicit
'           style list and tc entries
            xTOCParams = "\t """ & xStyleList & """" & "\w"
        End If
    End If
    
'   preserve line breaks
    xTOCParams = xTOCParams & " \x"
    
'   bring in static tc codes if specified
    If dlgTOC.chkTCEntries Then
        xTOCParams = xTOCParams & " \l """ & _
                     xLevRange & """"
    End If
    
'   use hyperlinks if specified
    If dlgTOC.chkHyperlinks Then
        xTOCParams = xTOCParams & " \h"
    End If
    
'   exclude page numbers if specified - do only for field, so as not to throw off
'   the deeply integrated expections in bReworkTOC by altering the number of tabs
    Dim xSwitch As String
    For i = 1 To 9
        If Not udtTOCFormat.IncludePageNumber(i) Then
            If xSwitch = "" Then
                xSwitch = " \n " & CStr(i) & "-" & CStr(i)
            Else
                xSwitch = Left$(xSwitch, 6) & CStr(i)
            End If
        End If
    Next i
    xTOCParams = xTOCParams & xSwitch
    
'   prevent hidden text from
'   affecting pagination of TOC
    With ActiveWindow.View
        .ShowAll = False
        .ShowHiddenText = False
        .ShowFieldCodes = False
    End With
    'OutputDebugString "Before inserting field"
                        
'   add toc
    g_oStatus.Show 4, g_xTOCStatusMsg
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "Word ins�re un champ Table des mati�res..."
    Else
        Application.StatusBar = "Word is inserting Table of Contents field..."
    End If
    
    Set fldTOC = ActiveDocument.Fields.Add( _
        rngLocation, wdFieldTOC, xTOCParams)
    'OutputDebugString "After inserting field"

'   call any custom code
    lRet = oCustTOC.lAfterTOCFieldInsert(fldTOC)
    If lRet Then
        Err.Raise lRet, "oCustTOC.lAfterTOCFieldInsert", Err.Description
    End If
       
'   bookmark toc
    rngGetField(fldTOC.Code).Bookmarks.Add "mpTableOfContents"
    Set rngTOC = ActiveDocument.Bookmarks("mpTableOfContents").Range
    
    With rngTOC
        If Asc(.Characters.Last) = 12 Then
            rngTOC.MoveEnd wdCharacter, -1
        End If
        
'       update page numbers
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = "Mise � jour les num�ros de page..."
        Else
            Application.StatusBar = "Word is updating page numbers in Table of Contents..."
        End If
        g_oStatus.Show 42, g_xTOCStatusMsg
        ActiveDocument.TablesOfContents(1) _
            .UpdatePageNumbers
        'OutputDebugString "After updating page numbers"
        
'       show hidden text prior to
'       cleaning up doc and toc - this will
'       guarantee that codes that are hidden
'       are deleted if specified
        With ActiveWindow.View
            .ShowHiddenText = True
            .ShowAll = True
        End With
        
'       do only if TOC has entries
        If Not bIsTOCStyle(.Paragraphs.First.Style) Then
'           return doc to original state
            CleanUpDoc
            With rngTOC
                If .Characters.Last.Text = vbCr Then _
                    .MoveEnd wdCharacter, -1
                .Delete
            End With
        Else
'           change content of toc and format
'           based on user dlg choices
'           remove manual font formats if specified
            If dlgTOC.chkApplyManualFormatsToTOC = 0 Then
                If rngTOC.Font.Name <> "" Then
                    rngTOC.Font.Reset
                Else
                    'GLOG 2793 - inconsistent font - do targeted
                    'reset to preserve bullets
                    ResetTOCFont rngTOC
                End If
            End If
            
'           call any custom code
            lRet = oCustTOC.lBeforeTOCRework(rngTOC)
            If lRet Then
                Err.Raise lRet, _
                    "oCustTOC.lBeforeTOCRework", _
                    Err.Description
            End If
            
            'OutputDebugString "Before reworking"
            With dlgTOC
                bReworkTOCField rngTOC, _
                           .cbxTOCScheme, _
                           xExclusions, _
                           .chkStyles, _
                           .chkTCEntries, _
                           udtTOCFormat, _
                           xTOC9Style, _
                           .optInclude(1), _
                           .chkApplyManualFormatsToTOC
            End With
            'OutputDebugString "After reworking"
    
'           call any custom code
            lRet = oCustTOC.lAfterTOCRework()
            If lRet Then
                Err.Raise lRet, _
                    "oCustTOC.lAfterTOCRework", _
                    Err.Description
            End If
    
            
            'prepare TOC field for potential native update
            'OutputDebugString "Before cleaning up TOC field"
            CleanUpTOCField rngTOC, Not bIsCentered
            'OutputDebugString "After cleaning up TOC field"

'           ensure that toc is not colored
            .Font.ColorIndex = wdAuto
            
'           add warning not to delete trailing para
            If .Sections.Last.Index = _
                    ActiveDocument.Sections.Count Then
                .EndOf
                If g_iWordVersion < mpWordVersion_2003 Then
                    '9.9.2 - deleting the trailing papagraph mark
                    'doesn't impact the TOC bookmark in Word 2003 or 2007,
                    'so there's no longer any need for this warning
                    .Move
                    .InsertAfter mpEndOfTOCWarning
                    .Font.Hidden = True
                    .HighlightColorIndex = wdYellow
                    .InsertParagraphAfter
                    With .Characters.Last
                        .Font.Hidden = False
                        .HighlightColorIndex = wdNoHighlight
                    End With
                    .EndOf
                    .Style = wdStyleNormal
                    .HighlightColorIndex = wdNoHighlight
                End If
            Else
                'GLOG 5393 - this code appears to be obsolete and
                'is problematic when the document starts with an
                'intentional empty paragraph between the existing TOC
                'and subsequent text in the same section - I haven't
                'been able to recreate any scenario in which we currently
                'insert an extraneous paragraph
''               delete extraneous paragraph
'                On Error Resume Next
'                If .Characters.Count > 1 Then
'                    If .Next(wdParagraph).Text = vbCr Then _
'                        .Next(wdParagraph).Delete
'                End If
'                On Error GoTo ProcError
            End If

'           return doc to original state
            If Not g_bPreserveUndoListTOC Then _
                ActiveDocument.UndoClear
        End If
    End With
    
    'GLOG 5174 - restore replaced shift-returns -
    '4/15/13 - moved this line out of the above block, so that the cleanup
    'will occur regardless of whether entries are found
    ReplaceText ActiveDocument.Content, "zzmpShiftReturn", Chr(11)
            
'   in Word 97, all caps and bold need to be done at end
    If g_lUILanguage = wdFrenchCanadian Then
        xScheme = "Personnalis�"
    Else
        xScheme = "Custom"
    End If
    If (dlgTOC.cbxTOCScheme = xScheme) And _
            (dlgTOC.optTOCStyles(0) = True) Then
        Application.ScreenRefresh
        Application.ScreenUpdating = False
        For i = 1 To 9
            With ActiveDocument.Styles(xTranslateTOCStyle(i)).Font
                .AllCaps = udtTOCFormat.AllCaps(i)
                .Bold = udtTOCFormat.Bold(i)
            End With
        Next i
    End If
            
'   clear tc codes if specified
    'OutputDebugString "Before resetting TC entries"
    '9.9.4019 - this control defaulted checked and was hidden -
    'got rid of it with the switchover to the native tab control
'    If dlgTOC.chkClearTCCodes Then
    '9.9.5005 - made conditional on .chkTCEntries
    If dlgTOC.chkTCEntries Then _
        bRet = bResetTCEntries()
    'OutputDebugString "After resetting TC entries"
    
'   All TOC entries may have been marked for deletion
    If Not ActiveDocument.Bookmarks.Exists("mpTableOfContents") Then _
        ActiveDocument.Bookmarks.Add "mpTableOfContents", rngTOC
    
'   hide status display
    g_oStatus.Hide
    Set g_oStatus = Nothing
    
    'GLOG 5147 - ensure that focus remains on active document
    Application.Activate
    oDoc.Activate
    
'   alert if toc is empty
    If Len(ActiveDocument.Bookmarks("mpTableOfContents").Range) < 2 Then
        With rngTOC
'           move before "Do Not Delete" warning
            On Error Resume Next
            If .Previous(wdParagraph).Text = _
                    mpEndOfTOCWarning & vbCr Then
                .Move wdParagraph, -1
            End If
            On Error GoTo ProcError
'           insert warning in document
            If g_lUILanguage = wdFrenchCanadian Then
                .InsertBefore mpNoEntriesWarningFrench
            Else
                .InsertBefore mpNoEntriesWarning
            End If
            .Style = wdStyleNormal
            .HighlightColorIndex = wdNoHighlight
            .Bold = True
        End With
'       rebookmark
        ActiveDocument.Bookmarks.Add "mpTableOfContents", rngTOC
'       present dlg alert
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "La Table des mati�res est vide.  " & vbCr & _
                   "Aucune entr�e TM n'a �t� trouv� dans ce document.", _
                    vbExclamation, "Num�rotation MacPac"
        Else
            MsgBox "The Table Of Contents is empty.  " & vbCr & _
                   "No valid Table Of Contents entries " & _
                   "could be found in the document.", _
                   vbExclamation, AppName
        End If
    End If
    
    Set oCustTOC = Nothing
'   select start of TOC - force
'   vertical scroll to selection
    With ActiveDocument
        .Range(0, 0).Select
        .Bookmarks("mpTableOfContents").Select
    End With
    Selection.StartOf
    
'   restore user's smart quotes setting
    Application.Options _
        .AutoFormatAsYouTypeReplaceQuotes = bSmartQuotes
        
'   restore outline levels of schedule styles
    If dlgTOC.optInclude(1) Or bIncludeSchedule Then
        RestoreScheduleLevels
    End If
                
'   mpTOC.sty is now only loaded as needed (12/10/01);
'   9.8.1007 - mpTOC.sty is now left loaded
'    UnloadTOCSty
    
'   reset environment
    'OutputDebugString "Before resetting environment"
    With ActiveWindow.View
        .ShowAll = bShowAll
        .ShowHiddenText = bShowHidden
        .Type = iView
        .Zoom.Percentage = lZoom
    End With
    
    'restore xml tags
    If g_bXMLSupport Then _
        lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, lShowTags)
        
    'GLOG 5057 - restore setting
    If bPrintHiddenText Then _
        Application.Options.PrintHiddenText = True
    
    EchoOn
    bInsertTOCAsField = True
    
    'OutputDebugString "End of TOC macro"
    
    On Error Resume Next
    Unload dlgTOC
    Set dlgTOC = Nothing
    
    '9.9.5005
    If g_bDisplayBenchmarks Then
        sElapsedTime = ElapsedTime(lStartTick)
        MsgBox CStr(sElapsedTime)
    End If
    Exit Function
    
ProcError:
    g_oStatus.Hide
    Set g_oStatus = Nothing
    
    'GLOG 5057 - restore setting
    If bPrintHiddenText Then _
        Application.Options.PrintHiddenText = True
    
    Unload dlgTOC
    EchoOn
    RaiseError "MPTOC90.mpTOC.bInsertTOCAsField"
    bInsertTOCAsField = Err
    Exit Function
End Function
#If False Then
Function iUpdateTCCodes(ByVal iDefaultTOCLevel As Integer, _
                        ByVal xNumberTrailChar As String, _
                        Optional ByVal bReApplyTCEntryStyles As Boolean = False, _
                        Optional ByVal bApplyHeadingFormats As Boolean = False) As Integer

'   cycles through all tc codes,
'   filling each appropriately -
'   returns number of codes found

    Dim rngTCCode As Word.Range
    Dim rngTCCodeStart As Word.Range
    Dim rngTCFieldTextStart As Word.Range
    Dim lTCCodeStartPos As Long
    Dim i As Integer
    Dim iListLevel As Integer
    Dim fldField As Word.Field
    Dim ltCurrent As ListTemplate
    Dim rngHeading As Word.Range
    Dim iNumHeadingNumbers As Integer
    Dim iUserChoice As Integer
    Dim xPrompt As String
    
    If g_lUILanguage = wdFrenchCanadian Then
        xPrompt = "Mise � jour des entr�s de TM:"
    Else
        xPrompt = "Updating TOC Entries:"
    End If
    Application.StatusBar = xPrompt
    
'   find first tc code
    For Each fldField In ActiveDocument.Fields

'       cycle through all tc codes
        If fldField.Type = wdFieldTOCEntry Then
        
'           set status
            i = i + 1
            Application.StatusBar = xPrompt & " " & i

'           get range of field
            With fldField
                Set rngTCCode = .Code
                
                lTCCodeStartPos = .Code.Start - 1
                Set rngTCCodeStart = ActiveDocument.Range(lTCCodeStartPos, _
                                                          lTCCodeStartPos)
            End With
                       
'           get list level, if any
            iListLevel = iGetParagraphLevel(rngTCCodeStart, _
                                            iDefaultTOCLevel)
                                      
'           if user cancelled out of TCEntry
'           level dlg, either remove code
'           or attempt to get level again
            While iListLevel = 0
                If g_lUILanguage = wdFrenchCanadian Then
                    iUserChoice = MsgBox("Enlever marquages pour TM ?", _
                        vbExclamation + vbYesNoCancel, "Num�rotation MacPac")
                Else
                    iUserChoice = MsgBox("Remove Mark For TOC?", _
                        vbExclamation + vbYesNoCancel, AppName)
                End If
                If iUserChoice = vbYes Then
                    iListLevel = mpRemoveTOCMarking
                ElseIf iUserChoice = vbNo Then
                    iListLevel = mpSkipTOCMarking
                Else
                    iUpdateTCCodes = -1
                    Exit Function
                End If
            Wend

            If iListLevel = mpRemoveTOCMarking Then
'               remove TOC Marking
                bRet = bMarkForTOCRemove(rngTCCodeStart)
            ElseIf iListLevel > mpSkipTOCMarking Then
'               fill TC Code -
'               set starting point for TC field content
                Set rngTCFieldTextStart = rngTCCode.Characters.Last
                rngTCFieldTextStart.EndOf
            
'               format heading and number
                Set rngHeading = rngFormatTCHeading(rngTCCodeStart, _
                                                    iListLevel, _
                                                    bReApplyTCEntryStyles)
            
'               get numbered items
                iNumHeadingNumbers = rngHeading.ListFormat _
                                    .CountNumberedItems
            
'               fill tc code -
'               MacPac uses two different techniques for filling
'               TC codes (see below).  The different techniques are
'               necessary for two reasons: 1)we need a technique to
'               get formats into TC Entries, 2) we need a technique
'               to get multiple numbers into TC Entries (WORD can't
'               do this on its own).  The function that does these
'               things is signifcantly slower than the other routine,
'               so we use it only when necessary, as specified by
'               the following conditional:
                If (bApplyHeadingFormats Or _
                    iNumHeadingNumbers > 1) Then
                    
                    bRet = bInsertTCEntryText(rngHeading, _
                                              rngTCFieldTextStart, _
                                              iListLevel, _
                                              xNumberTrailChar)
                                                  
                        If Not bApplyHeadingFormats Then
                            fldField.Code.Font.Reset
                        End If
                        
                Else
                
                    rngTCFieldTextStart.InsertAfter xGetTCEntryText _
                                                    (rngHeading, _
                                                    iListLevel, _
                                                    xNumberTrailChar)
                                                    
                    If Not bApplyHeadingFormats Then
                        fldField.Code.Font.Reset
                    End If
                
                End If
            End If
        End If
    Next fldField
    
'   clean up
    bRet = bDeleteAllBookmarks(bMacPacOnly:=True)
    
'   return
    iUpdateTCCodes = i
End Function
#End If

Function iRefreshPDynCodes(ByVal iDefaultTOCLevel As Integer, _
                        ByVal xNumberTrailChar As String, _
                        Optional ByVal bReApplyTCEntryStyles As Boolean = False, _
                        Optional ByVal bApplyHeadingFormats As Boolean = False, _
                        Optional bReplaceShiftReturns As Boolean = True) As Integer
'   cycles through all tc codes,
'   filling each appropriately -
'   returns number of codes found
'9/27/12 (dm) - added bReplaceShiftReturns argument
    Dim rngTCCode As Word.Range
    Dim rngTCCodeStart As Word.Range
    Dim rngTCFieldTextStart As Word.Range
    Dim lTCCodeStartPos As Long
    Dim i As Integer
    Dim iLevel As Integer
    Dim fldField As Word.Field
    Dim ltCurrent As ListTemplate
    Dim rngHeading As Word.Range
    Dim iNumHeadingNumbers As Integer
    Dim iUserChoice As Integer
    Dim l As Long
    Dim lFields As Long
    Dim iPos As Integer
    Dim bLevelChanged As Boolean
    Dim bIsMarkedFilled As Boolean
    Dim bIsMarkedDyn As Boolean
    Dim bIsTCPDyn As Boolean
    
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "Mise � jour des entr�s de TM:"
    Else
        Application.StatusBar = "Updating TOC Entries:"
    End If
    g_oStatus.Show 4, g_xTOCStatusMsg
    
    lFields = ActiveDocument.Fields.Count
    
    For Each fldField In ActiveDocument.Fields
'       cycle through all tc codes
        If fldField.Type = wdFieldTOCEntry Then
        
'           get range of field
            With fldField
                Set rngTCCode = .Code
                lTCCodeStartPos = .Code.Start - 1
                Set rngTCCodeStart = ActiveDocument.Range( _
                        lTCCodeStartPos, lTCCodeStartPos)
            End With
                       
            bIsMarkedFilled = InStr(UCase(rngTCCode), _
                UCase(g_xTCPrefix) & " " & Chr(34))
                
'           may be using smart quotes
            If Not bIsMarkedFilled Then
                bIsMarkedFilled = InStr(UCase(rngTCCode), _
                    UCase(g_xTCPrefix) & " " & ChrW(&H201C))
            End If
            
            bIsMarkedDyn = (rngTCCode = " " & g_xTCPrefix & " ")
            
'           update code only if there is no outline level
'           for para or if tc code is already filled
'            If (rngTCCode.ParagraphFormat.OutlineLevel = _
'                wdOutlineLevelBodyText) Or bIsMarkedFilled Then
            If Not bIsMarkedDyn Then
'               attempt to get level from code
                iPos = InStr(LCase(rngTCCode), "\l """)
                
'               may be using smart quotes
                If iPos = 0 Then
                    iPos = InStr(LCase(rngTCCode), "\l " & ChrW(&H201C))
                End If
                
                If iPos Then
'                   level param exists in tc code
                    On Error Resume Next
'                   extract number - iLevel will be 0 if
'                   the character succeeding "\l """ is not
'                   an integer
                    iLevel = Mid(rngTCCode.Text, iPos + 4, 1)
                    If iLevel < 1 Or iLevel > 9 Then
'                       invalid level - alert and prompt for level
                        If g_lUILanguage = wdFrenchCanadian Then
                            MsgBox "Niveau TM invalide. S�lectionnez un niveau dans la bo�te de dialogue.", _
                                vbExclamation, "Num�rotation MacPac"
                        Else
                            MsgBox "Invalid TOC level.  Please choose " & _
                                   "a level from the following dialog.", vbExclamation, AppName
                        End If
                        
'                       get list level, if any
                        iLevel = iGetParagraphLevel(rngTCCodeStart, _
                                                        iDefaultTOCLevel)
                        bLevelChanged = (iLevel <> 0)
                    End If
                End If
                
                If iLevel = 0 Then
'                   invalid level - alert and prompt for level
                    If g_lUILanguage = wdFrenchCanadian Then
                        MsgBox "Niveau TM invalide ou manquant.  S�lectionnez un niveau dans la bo�te de dialogue.", _
                            vbExclamation, "Num�rotation MacPac"
                    Else
                        MsgBox "Invalid or missing TOC level.  Please choose " & _
                            "a level from the following dialog.", vbExclamation, AppName
                    End If
                        
'                   get list level, if any
                    iLevel = iGetParagraphLevel(rngTCCodeStart, _
                                                    iDefaultTOCLevel)
                    bLevelChanged = (iLevel <> 0)
                End If
                
'               if user cancelled out of TCEntry
'               level dlg, either remove code
'               or attempt to get level again
                While iLevel = 0
                    If g_lUILanguage = wdFrenchCanadian Then
                        iUserChoice = MsgBox("Enlever marquages pour TM ?", _
                                             vbExclamation + vbYesNoCancel, _
                                             "Num�rotation MacPac")
                    Else
                        iUserChoice = MsgBox("Remove Mark For TOC?", _
                                             vbExclamation + vbYesNoCancel, _
                                             AppName)
                    End If
                    If iUserChoice = vbYes Then
                        iLevel = mpRemoveTOCMarking
                    ElseIf iUserChoice = vbNo Then
                        iLevel = mpSkipTOCMarking
                    Else
                        iRefreshPDynCodes = -1
                        Exit Function
                    End If
                Wend

                If iLevel = mpRemoveTOCMarking Then
'                   remove TOC Marking
                    bRet = bMarkForTOCRemove(rngTCCodeStart)
                ElseIf bIsMarkedFilled Then
'                   replace shift-returns with a space in pre-9.7.8 partially dynamic codes;
'                   this is no longer handled in bReworkTOC (7/1/03)
                    bIsTCPDyn = (InStr(UCase(rngTCCode.Text), UCase(mpTCCode_PDyn)) > 0)
                    If bIsTCPDyn Then
                        If bReplaceShiftReturns Or (iLevel <> 1) Then
                            rngTCCode.Text = xSubstitute(rngTCCode.Text, _
                                String(2, 11), Chr(32))
                            rngTCCode.Text = xSubstitute(rngTCCode.Text, _
                                Chr(11), Chr(32))
                        Else
                            'replace shift-returns with single shift return
                            rngTCCode.Text = xSubstitute(rngTCCode.Text, _
                                String(2, 11), Chr(11))
                        End If
                    End If
                    
'                   append MacPac tag
                    rngTCCode.InsertAfter mpTCCodeStatic
                ElseIf iLevel > mpSkipTOCMarking Then
'                   fill TC Code -
'                   set starting point for TC field content
                    rngTCCode.Text = " " & g_xTCPrefix & " "
                    Set rngTCFieldTextStart = rngTCCode.Characters(4)
                    rngTCFieldTextStart.EndOf
            
                    If bLevelChanged Then
'                       prompt to format
                        If g_lUILanguage = wdFrenchCanadian Then
                            iUserChoice = MsgBox("D�sirez-vous d�finir ce titre comme niveau " & iLevel & " ?", _
                                vbQuestion + vbYesNo, "Num�rotation MacPac")
                        Else
                            iUserChoice = MsgBox("Do you want to format this heading " & _
                                   "as a level " & iLevel & " heading?", vbQuestion + vbYesNo, AppName)
                        End If
                        
                        If iUserChoice = vbYes Then
'                           level of heading has changed - format heading and number
                            Set rngHeading = rngFormatTCHeading(rngTCCodeStart, _
                                                                iLevel, _
                                                                True)
                        Else
'                           just get the range of the heading
                            Set rngHeading = ActiveDocument.Range( _
                                rngTCCode.Paragraphs(1).Range.Start, _
                                rngTCCode.Start - 1)
                        End If
                    Else
'                       just get the range of the heading
                        Set rngHeading = ActiveDocument.Range( _
                            rngTCCode.Paragraphs(1).Range.Start, _
                            rngTCCode.Start - 1)
                    End If
                    
'                   get numbered items
                    iNumHeadingNumbers = rngHeading.ListFormat _
                                        .CountNumberedItems
                
'                   fill tc code -
'                   MacPac uses two different techniques for filling
'                   TC codes (see below).  The different techniques are
'                   necessary for two reasons: 1)we need a technique to
'                   get formats into TC Entries, 2) we need a technique
'                   to get multiple numbers into TC Entries (WORD can't
'                   do this on its own).  The function that does these
'                   things is signifcantly slower than the other routine,
'                   so we use it only when necessary, as specified by
'                   the following conditional:
                    If (bApplyHeadingFormats Or _
                        iNumHeadingNumbers > 1) Then
                        
                        bRet = bInsertTCEntryText(rngHeading, _
                                                  rngTCFieldTextStart, _
                                                  iLevel, _
                                                  xNumberTrailChar, _
                                                  True)
                                                      
                            If Not bApplyHeadingFormats Then
                                fldField.Code.Font.Reset
                            End If
                            
                    Else
                    
                        rngTCFieldTextStart.InsertAfter xGetTCEntryText _
                                                        (rngHeading, _
                                                        iLevel, _
                                                        xNumberTrailChar, _
                                                        True)
                                                        
                        If Not bApplyHeadingFormats Then
                            fldField.Code.Font.Reset
                        End If
                    
                    End If
                End If
            End If
        End If
        iLevel = Empty
        bIsMarkedFilled = Empty
        iPos = Empty
        iNumHeadingNumbers = Empty
        bRet = Empty
        bLevelChanged = Empty
        
'       set status
        l = l + 1
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = "Mise � jour des entr�s de TM: " & Format(l / lFields, "0%")
        Else
            Application.StatusBar = "Updating TOC Entries: " & Format(l / lFields, "0%")
        End If
        g_oStatus.Show 4, g_xTOCStatusMsg
        
    Next fldField
    
'   clean up
    '9.9.5005 - no need to cycle through all bookmarks when we're after one in particular
'    bRet = bDeleteAllBookmarks(bMacPacOnly:=True)
    If ActiveDocument.Bookmarks.Exists("zzmpTCEntryTemp") Then _
        ActiveDocument.Bookmarks("zzmpTCEntryTemp").Delete
    
'   return
    iRefreshPDynCodes = i
End Function

Function iGetTOCLevel(rngTCLocation As Word.Range) As Integer
    Dim dlgLevel As VB.Form
    Dim rngDupe As Word.Range
    Dim bShowAll As Boolean
    Dim bShowHidden As Boolean
    Dim bManageStatus As Boolean
    
    On Error GoTo iGetTOCLevel_Error
    
    'hide status screen while prompting (9.9.6014)
    bManageStatus = Not g_oStatus Is Nothing
    If bManageStatus Then
        g_oStatus.Hide
        Set g_oStatus = Nothing
    End If
                
    If g_lUILanguage = wdFrenchCanadian Then
        Set dlgLevel = New frmTOCEntryLevelFrench
    Else
        Set dlgLevel = New frmTOCEntryLevel
    End If

'   hide all visible MacPac codes
    HideTags
    
    With ActiveWindow.View
        bShowAll = .ShowAll
        bShowHidden = .ShowHiddenText
        .ShowAll = False
        .ShowHiddenText = False
    End With
    
    Set rngDupe = rngTCLocation.Duplicate
    
    With rngDupe
        .SetRange .Paragraphs(1).Range.Start, _
                  .End
        .MoveStartWhile Chr(12)
        Application.ScreenUpdating = True
        .Select
    End With
    
    With dlgLevel
        .Show vbModal
        If Not .Cancelled Then
            iGetTOCLevel = .cbxTOCLevels
        End If
    End With
    
    Application.ScreenUpdating = False
    
    With ActiveWindow.View
        .ShowAll = bShowAll
        .ShowHiddenText = bShowHidden
    End With
    
    rngTCLocation.Select
    Unload dlgLevel
    
    'restore status screen (9.9.6014)
    If bManageStatus Then
        Set g_oStatus = New CStatus
        With g_oStatus
            If g_lUILanguage = wdFrenchCanadian Then
                .Title = "Ins�rer Table des mati�res"
            Else
                .Title = "Inserting Table of Contents"
            End If
            .ProgressBarVisible = True
            .Show 3, g_xTOCStatusMsg
        End With
    End If
                    
    Exit Function
    
iGetTOCLevel_Error:
    Select Case Err.Number
        Case mpBlockNotSet
        Case Else
    End Select
    
    Unload dlgLevel
    rngTCLocation.Select
    Exit Function

End Function

Function rngInsertTOCTOASection(Optional vLocation As Variant, _
                                Optional bInsertTOA As Boolean = False, _
                                Optional bRestartPageNumbering As Boolean = True, _
                                Optional bBoldTOC As Boolean = True, _
                                Optional bBoldPage As Boolean = True, _
                                Optional bCapTOC As Boolean = True, _
                                Optional bCapPage As Boolean = False, _
                                Optional bUnderlineTOC As Boolean = False, _
                                Optional bUnderlinePage As Boolean = False, _
                                Optional iPageNoStyle As Integer = wdPageNumberStyleLowercaseRoman, _
                                Optional iPunctuation As mpTOCPagePunctuation = mpTOCPagePunctuationHyphens, _
                                Optional bRetagMP10HeadersFooters As Boolean = False) _
                                As Word.Range
'bRetagMP10HeadersFooters parameter added in 9.9.5008
    Dim rngLocation As Word.Range
    Dim secTOC As Word.Section
    Dim iRightTabPos As Integer
    Dim rngTOCStart As Word.Range
    Dim bSecIsAtEOF As Boolean
    Dim rngPage As Word.Range
    Dim styNormal As Word.Style
    Dim sWidth As Single
    Dim rngFNTarget As Word.Range
    Dim rngHF As Word.Range
    Dim xWarning As String
    Dim xPreceding As String
    Dim xTrailing As String
    Dim rFirmName As Word.Range
    Dim bExtraSection As Boolean
    Dim rngLabel As Word.Range
    Dim styTOCHeader As Word.Style
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #Else
        On Error GoTo 0
    #End If
    
    If g_lUILanguage = wdFrenchCanadian Then
        If bInsertTOA Then
            Application.StatusBar = "Insertion Section TR..."
        Else
            Application.StatusBar = "Insertion Section TM..."
            g_oStatus.Show 2, g_xTOCStatusMsg
        End If
    Else
        If bInsertTOA Then
            Application.StatusBar = "Inserting TOA Section..."
        Else
            Application.StatusBar = "Inserting TOC Section..."
            g_oStatus.Show 2, g_xTOCStatusMsg
        End If
    End If
    
    EchoOff
    
'   if location is not specified,
'   create section at end of doc,
'   else at location vLocation
    If IsMissing(vLocation) Then
        With ActiveDocument
            Set rngLocation = .Content
            rngLocation.EndOf
        End With
    Else
        Set rngLocation = vLocation
    End If
    
'   Is TOC going to be inserted at end of doc?
    bSecIsAtEOF = (rngLocation.End >= ActiveDocument.Content.End - 1)
    
'   If EOF, insert additional section?
    bExtraSection = xAppGetFirmINIValue("TOC", "InsertExtraSectionAfterTOC")
    
    With rngLocation
        If bSecIsAtEOF And bExtraSection Then
'           insert blank paragraph at end of document to prevent
'           section break from appearing on same line as text
            .InsertParagraphAfter
            .Style = wdStyleNormal
            .EndOf
            
            .InsertBreak wdSectionBreakNextPage
            
            xWarning = "WARNING:  This section retains the original formatting, " & _
                    "including headers and footers, of the main document.  " & _
                    "If you delete the section break above this message " & _
                    "(which is visible ONLY in Normal View), any special " & _
                    "formatting, including headers and footers for the " & _
                    "Table of Contents/Authorities section will be lost." & vbCrLf & _
                    "If you delete the section break above the Table of " & _
                    "Contents/Authorities, you will overwrite the headers and " & _
                    "footers of the main document with Table of Contents/Authorities " & _
                    "headers and footers." & vbCrLf & _
                    "To delete the Table of Contents/Authorities, begin your " & _
                    "selection at the section break above the TOC/TOA section and " & _
                    "continue through the end of this message."

            .InsertAfter xWarning
            .ListFormat.RemoveNumbers
            .Style = wdStyleBodyText
            
'           unlink headers/footers
            bUnlinkHeadersFooters .Sections(1)
            With .Sections(1).Footers(wdHeaderFooterPrimary).PageNumbers
                'GLOG 5609 - new option to continue numbering
                .RestartNumberingAtSection = bRestartPageNumbering
                If bRestartPageNumbering Then _
                    .StartingNumber = 1
            End With
            
'           redefine range to be before last section break
            .StartOf
            .Move wdCharacter, -1
        End If
        
'       create section
        'GLOG 5609 - pass argument whether to continue numbering
        Set secTOC = secCreateSection(rngLocation, , , _
            bRestartPageNumbering, bRetagMP10HeadersFooters)
        
    End With
    
    With ActiveDocument
'       set ranges for TOC section
        Set rngTOCStart = .Range(secTOC.Range.Start, _
                            secTOC.Range.Start)
    End With
    
'   edit section Headers/Footers
    With secTOC
        With .PageSetup
            .DifferentFirstPageHeaderFooter = True
            .TopMargin = InchesToPoints(oCustTOC.TopMargin)
            .BottomMargin = InchesToPoints(oCustTOC.BottomMargin)
            .HeaderDistance = InchesToPoints(oCustTOC.HeaderDistance)
            .FooterDistance = InchesToPoints(oCustTOC.FooterDistance)
            .VerticalAlignment = wdAlignVerticalTop
            .Orientation = wdOrientPortrait
            
            If oCustTOC.PleadingPaperType = mpPleadingPaperNone Then
'               get position of the tab stop for the "Page" heading
                iRightTabPos = .PageWidth - _
                    (.LeftMargin + .RightMargin + .Gutter)
            Else
'               get appropriate width of second
'               col of TOC Header from margins
                sWidth = .PageWidth - _
                    (.LeftMargin + .RightMargin + .Gutter) + 12
            End If
        End With
        
        With .Headers
            With .Item(wdHeaderFooterFirstPage)
                .LinkToPrevious = False
                .Range.Delete
                
'---            insert boilerplate
                With .Range
                    .InsertFile oCustTOC.BPFile, oCustTOC.HeaderBookmark, , , False
                    .WholeStory
                    If oCustTOC.PleadingPaperType <> mpPleadingPaperNone Then _
                        .Tables(1).Columns.Last.Width = sWidth
                    
'---                apply attributes; relabel TOC
                    Set rngLabel = rngHeaderBookmark(.Duplicate, _
                        "zzmpHeaderText_TOC")
                    If rngLabel Is Nothing Then
                        With .Find
                            .ClearFormatting
                            .Text = "Table of Contents"
                            .Execute
                            If .Found Then
                                With .Parent
                                    If bInsertTOA Then _
                                        .Text = "Table of Authorities"
                                    If g_bAllowHeaderFooterEdit Then
                                        If bBoldTOC Then _
                                            .Bold = True
                                        If bCapTOC Then _
                                            .Font.AllCaps = True
                                        If bUnderlineTOC Then _
                                            .Font.Underline = wdUnderlineSingle
                                    End If
                                End With
                            End If
                        End With
                    Else
                        With rngLabel
                            If bInsertTOA Then _
                                .Text = "Table of Authorities"
                            If g_bAllowHeaderFooterEdit Then
                                If bBoldTOC Then _
                                    .Bold = True
                                If bCapTOC Then _
                                    .Font.AllCaps = True
                                If bUnderlineTOC Then _
                                    .Font.Underline = wdUnderlineSingle
                            End If
                        End With
                    End If
                    .WholeStory
                    
                    Set rngLabel = rngHeaderBookmark(.Duplicate, _
                        "zzmpHeaderText_Page")
                    If rngLabel Is Nothing Then
                        With .Find
                            .ClearFormatting
                            .Text = "Page"
                            .Execute
                            If .Found Then
                                With .Parent
                                    If bInsertTOA Then _
                                        .Text = "Page(s)"
                                    If g_bAllowHeaderFooterEdit Then
                                        If bBoldPage Then _
                                            .Bold = True
                                        If bCapPage Then _
                                            .Font.AllCaps = True
                                        If bUnderlinePage Then _
                                            .Font.Underline = wdUnderlineSingle
                                    End If
                                End With
                            End If
                        End With
                    Else
                        With rngLabel
                            If bInsertTOA Then _
                                .Text = "Page(s)"
                            If g_bAllowHeaderFooterEdit Then
                                If bBoldPage Then _
                                    .Bold = True
                                If bCapPage Then _
                                    .Font.AllCaps = True
                                If bUnderlinePage Then _
                                    .Font.Underline = wdUnderlineSingle
                            End If
                        End With
                    End If
                    .WholeStory
                End With
                
'               look for "(continued)" bookmark or text
'               and delete from first page
                Set rngLabel = rngHeaderBookmark(.Range.Duplicate, _
                    "zzmpHeaderText_Continued")
                If rngLabel Is Nothing Then
                    rngInsertAt xLabel:="(continued)", _
                                xText:="", _
                                vSearch:=.Range
                Else
                    rngLabel.Delete
                End If
                          
                If oCustTOC.PleadingPaperType = mpPleadingPaperNone Then
'                   adjust right tab
'                    Set rngPage = rngInsertAt(xLabel:="Page", _
'                                              xText:="Page", _
'                                              vSearch:=.Range)
'
'                    If Not (rngPage Is Nothing) Then
'                        With rngPage.ParagraphFormat.TabStops
'                            .Item(.Count).Position = iRightTabPos
'                        End With
'                    End If

'                   replace above with full tab adjustment
                    .Range.WholeStory
                    bRet = rngAdjustHeaderTabs(.Range)
                End If
            End With

            With .Item(wdHeaderFooterPrimary)
                .LinkToPrevious = False
                .Range.Delete
                
'---            insert boilerplate
                With .Range
                    .InsertFile oCustTOC.BPFile, oCustTOC.HeaderBookmark, , , False
                    .WholeStory
                    If oCustTOC.PleadingPaperType <> mpPleadingPaperNone Then _
                        .Tables(1).Columns.Last.Width = sWidth
                
'---                apply attributes; relabel TOC
                    Set rngLabel = rngHeaderBookmark(.Duplicate, _
                        "zzmpHeaderText_TOC")
                    If rngLabel Is Nothing Then
                        With .Find
                            .ClearFormatting
                            .Text = "Table of Contents"
                            .Execute
                            If .Found Then
                                With .Parent
                                    If bInsertTOA Then _
                                        .Text = "Table of Authorities"
                                    If g_bAllowHeaderFooterEdit Then
                                        If bBoldTOC Then _
                                            .Bold = True
                                        If bCapTOC Then _
                                            .Font.AllCaps = True
                                        If bUnderlineTOC Then _
                                            .Font.Underline = wdUnderlineSingle
                                    End If
                                End With
                            End If
                        End With
                    Else
                        With rngLabel
                            If bInsertTOA Then _
                                .Text = "Table of Authorities"
                            If g_bAllowHeaderFooterEdit Then
                                If bBoldTOC Then _
                                    .Bold = True
                                If bCapTOC Then _
                                    .Font.AllCaps = True
                                If bUnderlineTOC Then _
                                    .Font.Underline = wdUnderlineSingle
                            End If
                        End With
                    End If
                    .WholeStory
                    
                    Set rngLabel = rngHeaderBookmark(.Duplicate, _
                        "zzmpHeaderText_Page")
                    If rngLabel Is Nothing Then
                        With .Find
                            .ClearFormatting
                            .Text = "Page"
                            .Execute
                            If .Found Then
                                With .Parent
                                    If bInsertTOA Then _
                                        .Text = "Page(s)"
                                    If g_bAllowHeaderFooterEdit Then
                                        If bBoldPage Then _
                                            .Bold = True
                                        If bCapPage Then _
                                            .Font.AllCaps = True
                                        If bUnderlinePage Then _
                                            .Font.Underline = wdUnderlineSingle
                                    End If
                                End With
                            End If
                        End With
                    Else
                        With rngLabel
                            If bInsertTOA Then _
                                .Text = "Page(s)"
                            If g_bAllowHeaderFooterEdit Then
                                If bBoldPage Then _
                                    .Bold = True
                                If bCapPage Then _
                                    .Font.AllCaps = True
                                If bUnderlinePage Then _
                                    .Font.Underline = wdUnderlineSingle
                            End If
                        End With
                    End If
                    .WholeStory
                End With
                
'                If bInsertTOA Then
'                    rngInsertAt xLabel:="Table of Contents", _
'                                xText:="TABLE OF AUTHORITIES", _
'                                vSearch:=.Range
'                    .Range.WholeStory
'                    rngInsertAt xLabel:="Page", _
'                                xText:="Page(s)", _
'                                vSearch:=.Range
'                End If

                If oCustTOC.PleadingPaperType = mpPleadingPaperNone Then
'                   adjust right tab
'                    Set rngPage = rngInsertAt(xLabel:="Page", _
'                                              xText:="Page", _
'                                              vSearch:=.Range)
'
'                    If Not (rngPage Is Nothing) Then
'                        With rngPage.ParagraphFormat.TabStops
'                            .Item(.Count).Position = iRightTabPos
'                        End With
'                    End If

'                   replace above with full tab adjustment
                    .Range.WholeStory
                    bRet = rngAdjustHeaderTabs(.Range)
                End If
            End With
        End With

    
        'modify footers
        '9.9.5005 - skip footers entirely if mp9 or mp10 pleading paper
        If oCustTOC.FooterBookmark <> "zzmpNothing" Then
            If g_bAllowHeaderFooterEdit Then
                Select Case iPunctuation
                    Case mpTOCPagePunctuationNone
                        xPreceding = ""
                        xTrailing = ""
                    Case mpTOCPagePunctuationHyphens
                        xPreceding = "-"
                        xTrailing = "-"
                    Case mpTOCPagePunctuationHyphensWithSpaces
                        xPreceding = "- "
                        xTrailing = " -"
                End Select
            Else
                xPreceding = g_xPageNoIntroChar
                xTrailing = g_xPageNoTrailingChar
            End If
            
            With .Footers
                With .Item(wdHeaderFooterFirstPage)
                    .LinkToPrevious = False
                    With .Range
                        .Delete
                        .InsertFile oCustTOC.BPFile, oCustTOC.FooterBookmark, , , False
                        .WholeStory
                    End With
                    
                    Set rngLocation = rngInsertAt(xLabel:="(page)", _
                                                  xText:="", _
                                                  vSearch:=.Range)
                    If Not rngLocation Is Nothing Then
                        With rngLocation
                            .Style = wdStylePageNumber
                            .InsertAfter xPreceding & xTrailing
                            .EndOf
                            .Move wdCharacter, 0 - Len(xTrailing)
                            .Fields.Add rngLocation, wdFieldPage
                        End With
                    End If
                    
                    If g_bVariablesInBoilerplate Then '9.9.5005
                        Set rngLocation = .Range
                        With rngLocation
                            .WholeStory
                            rngInsertAt "(Doc Title)", oCustTOC.FooterTitle, , , , rngLocation
                        End With
                    End If
                    
        '           Note: this will not work w/current MacPac TOC footer,
        '           which isn't centered but probably should be
                    If oCustTOC.bIsCenteredFooter Then
                        bAdjustCenteredFooter .Range
                    End If
                End With
        
                With .Item(wdHeaderFooterPrimary)
                    .LinkToPrevious = False
                    With .Range
                        .Delete
                        .InsertFile oCustTOC.BPFile, oCustTOC.FooterBookmark, , , False
                        .WholeStory
                    End With
                    
                    Set rngLocation = rngInsertAt(xLabel:="(page)", _
                                                  xText:="", _
                                                  vSearch:=.Range)
                    If Not rngLocation Is Nothing Then
                        With rngLocation
                            .Style = wdStylePageNumber
                            .InsertAfter xPreceding & xTrailing
                            .EndOf
                            .Move wdCharacter, 0 - Len(xTrailing)
                            .Fields.Add rngLocation, wdFieldPage
                        End With
                    End If
                    
                    If g_bVariablesInBoilerplate Then '9.9.5005
                        Set rngLocation = .Range
                        With rngLocation
                            .WholeStory
                            rngInsertAt "(Doc Title)", oCustTOC.FooterTitle, , , , rngLocation
                        End With
                    End If
                    
        '           Note: this will not work w/current MacPac TOC footer,
        '           which isn't centered but probably should be
                    If oCustTOC.bIsCenteredFooter Then
                        bAdjustCenteredFooter .Range
                    End If
                End With
            End With
        
        '---add firm name detail to pleading paper
            If g_bVariablesInBoilerplate Then '9.9.5005
                Select Case oCustTOC.PleadingPaperType
                    Case mpPleadingPaper26LineFNLand, _
                            mpPleadingPaper28LineFNLand
                            '---delete FN detail from footer if LS
                            Set rngHF = .Footers.Item(wdHeaderFooterPrimary).Range
                            Set rngFNTarget = oCustTOC.rngGetFirmNameTarget _
                                                (rngHF, _
                                                secTOC)
                            If Not rngFNTarget Is Nothing Then _
                                rngFNTarget.Delete
                            Set rngHF = .Footers.Item(wdHeaderFooterFirstPage).Range
                            Set rngFNTarget = oCustTOC.rngGetFirmNameTarget _
                                                (rngHF, _
                                                secTOC)
                            If Not rngFNTarget Is Nothing Then _
                                rngFNTarget.Delete
                        
                        With .Headers
                            With .Item(wdHeaderFooterFirstPage)
                                rngInsertAt xLabel:="(Firm Name)", _
                                            xText:=oCustTOC.FirmName, _
                                            vSearch:=.Range
                                .Range.WholeStory
                                rngInsertAt xLabel:="(Office)", _
                                            xText:=oCustTOC.Office, _
                                            vSearch:=.Range
                            End With
                
                            With .Item(wdHeaderFooterPrimary)
                                rngInsertAt xLabel:="(Firm Name)", _
                                            xText:=oCustTOC.FirmName, _
                                            vSearch:=.Range
                            
                                .Range.WholeStory
                                rngInsertAt xLabel:="(Office)", _
                                            xText:=oCustTOC.Office, _
                                            vSearch:=.Range
                            End With
                        
                        End With
                    
                        With .Footers
                            With .Item(wdHeaderFooterFirstPage)
                                Set rFirmName = rngInsertAt(xLabel:="(Firm Name)", _
                                            xText:="", _
                                            vSearch:=.Range)
                                If Not rFirmName Is Nothing Then _
                                    rFirmName.Cells(1).Range.Delete
                            End With
                
                            With .Item(wdHeaderFooterPrimary)
                                Set rFirmName = rngInsertAt(xLabel:="(Firm Name)", _
                                            xText:="", _
                                            vSearch:=.Range)
                                If Not rFirmName Is Nothing Then _
                                    rFirmName.Cells(1).Range.Delete
                            End With
                        End With
                    
                    Case mpPleadingPaper26LineFNPort, _
                            mpPleadingPaper28LineFNPort, _
                            mpPleadingPaper26Line, _
                            mpPleadingPaper28Line, _
                            mpPleadingPaperNone
                    
                    '---add firm name detail if indicated
                        If oCustTOC.PleadingPaperType < mpPleadingPaper26LineFNPort Then
                            
                            Set rngHF = .Footers.Item(wdHeaderFooterPrimary).Range
                            Set rngFNTarget = oCustTOC.rngGetFirmNameTarget _
                                                (rngHF, _
                                                secTOC)
                            If Not rngFNTarget Is Nothing Then _
                                rngFNTarget.Delete
                            Set rngHF = .Footers.Item(wdHeaderFooterFirstPage).Range
                            Set rngFNTarget = oCustTOC.rngGetFirmNameTarget _
                                                (rngHF, _
                                                secTOC)
                            If Not rngFNTarget Is Nothing Then _
                                rngFNTarget.Delete
                        End If
                                
                        With .Footers
                            With .Item(wdHeaderFooterFirstPage)
                                rngInsertAt xLabel:="(Firm Name)", _
                                            xText:=oCustTOC.FirmName, _
                                            vSearch:=.Range
                                .Range.WholeStory
                                rngInsertAt xLabel:="(Office)", _
                                            xText:=oCustTOC.Office, _
                                            vSearch:=.Range
                            End With
                
                            With .Item(wdHeaderFooterPrimary)
                                rngInsertAt xLabel:="(Firm Name)", _
                                            xText:=oCustTOC.FirmName, _
                                            vSearch:=.Range
                            
                                .Range.WholeStory
                                rngInsertAt xLabel:="(Office)", _
                                            xText:=oCustTOC.Office, _
                                            vSearch:=.Range
                            End With
                        End With
                
                Case Else
                End Select
            End If
        End If
        
        '9.9.5005
        With .Footers.Item(wdHeaderFooterPrimary).PageNumbers
            'GLOG 5609 - new option to continue numbering
            .RestartNumberingAtSection = bRestartPageNumbering
            If bRestartPageNumbering Then _
                .StartingNumber = 1
            If g_bAllowHeaderFooterEdit Then
                .NumberStyle = iPageNoStyle
            Else
                .NumberStyle = g_iPageNoStyle
            End If
        End With
    End With
                            
'   this is a patch for MP2K;
'   force TOC header font to match normal style;
'   if this isn't desired, use different style
    With ActiveDocument
        Set styNormal = .Styles(wdStyleNormal)
        On Error Resume Next
        Set styTOCHeader = .Styles("TOC Header")
        On Error GoTo ProcError
        If Not styTOCHeader Is Nothing Then
            With styTOCHeader.Font
                .Name = styNormal.Font.Name
                .Size = styNormal.Font.Size
            End With
        End If
    End With

    Set rngInsertTOCTOASection = rngTOCStart
'    Application.StatusBar = ""
    EchoOn
    Exit Function
    
ProcError:
    EchoOn
    RaiseError "mpTOC.rngInsertTOCTOASection"
    Exit Function
End Function

Function rngGetTOCTarget(docCurrent As Word.Document, _
                         Optional iLoc As Integer = mpAtEOF, _
                         Optional bBoldTOC As Boolean = True, _
                         Optional bBoldPage As Boolean = True, _
                         Optional bCapTOC As Boolean = True, _
                         Optional bCapPage As Boolean = False, _
                         Optional bUnderlineTOC As Boolean = False, _
                         Optional bUnderlinePage As Boolean = False, _
                         Optional iPageNoStyle As Integer = wdPageNumberStyleLowercaseRoman, _
                         Optional iPunctuation As mpTOCPagePunctuation = mpTOCPagePunctuationHyphens, _
                         Optional bRestartPageNumbering As Boolean = True) _
                         As Word.Range
'returns proposed location for TOC -
'first checks for existing TOC bookmark,
'then for TOC placeholder (earlier MP versions),
'then actual TOC.  if not found, checks for
'existing TOA bookmark, then actual TOA. if
'none found, then inserts at EOF.
'GLOG 5609 - added argument for new option to continue page numbering
    Const mpTOCPlaceholder As String = "TABLE OF CONTENTS PLACEHOLDER"
    
    Dim rngTOAStart As Word.Range
    Dim rngTOCStart As Word.Range
    Dim rngLOEStart As Word.Range
    Dim rngEOF As Word.Range
    Dim rngContent As Word.Range
    Dim toaExisting As Word.TableOfAuthorities
    Dim ftrExisting As HeaderFooter
    Dim rngLoc As Word.Range
    Dim rngMarker As Word.Range
    Dim xTOA As String
    Dim lStart As Long
    Dim lShowTags As Long
    Dim bAtStartOfBlockTag As Boolean
    Dim bParaAdded As Boolean
    
    With docCurrent
    
        Set rngContent = .Content
        
'       first check for existing bookmark, then for
'       placeholder, then for existing TOC -
        If .Bookmarks.Exists("mpTableOfContents") Then
            Set rngGetTOCTarget = .Bookmarks("mpTableOfContents").Range
            
'           ensure that bookmark doesn't include trailing section break
            If Asc(rngGetTOCTarget.Characters.Last) = 12 Then
                rngGetTOCTarget.MoveEnd wdCharacter, -1
            End If
            
'           ensure that bookmark doesn't include preceding section break
            If Asc(rngGetTOCTarget.Characters.First) = 12 Then
                rngGetTOCTarget.MoveStart wdCharacter, 1
            End If
            
'           TOC at start of doc
            If rngGetTOCTarget.Start = ActiveDocument.Range.Start Then
                'GLOG 5149 - modified conditional to fix problem whereby warning was
                'getting only partially deleted with TOC at start of document
                While (rngGetTOCTarget.Paragraphs.Count > 1) And _
                        (Not bIsTOCStyle(rngGetTOCTarget.Paragraphs(1).Style)) And _
                        (InStr(rngGetTOCTarget.Text, mpNoEntriesWarning) = 0) And _
                        (InStr(rngGetTOCTarget.Text, mpNoEntriesWarningFrench) = 0)
                    rngGetTOCTarget.MoveStart wdParagraph, 1
                    If rngGetTOCTarget.End > .Bookmarks("mpTableOfContents").Range.End Then
                        Set rngGetTOCTarget = .Bookmarks("mpTableOfContents").Range
                        If Asc(rngGetTOCTarget.Characters.Last) = 12 Then
                            rngGetTOCTarget.MoveEnd wdCharacter, -1
                        End If
                        rngGetTOCTarget.Collapse wdCollapseEnd
                    End If
                Wend
                If Asc(rngGetTOCTarget.Characters.First) = 12 Then
                    rngGetTOCTarget.MoveStart wdCharacter, 1
                End If
            End If
            
            With rngGetTOCTarget
'               if end of TOC marker exists, expand range to include it
                On Error Resume Next
                If .Fields.Count = 0 Then
                    Set rngMarker = .Next(wdParagraph)
                Else
'                   incredibly, if TOC is a field, .Next skips a paragraph
                    Set rngMarker = .Next(wdParagraph).Previous(wdParagraph)
                End If
                On Error GoTo 0
                
                If Not rngMarker Is Nothing Then
                    If rngMarker.Text = mpEndOfTOCWarning & vbCr Then _
                        .MoveEnd wdParagraph
                End If
                    
'               delete existing TOC
                If Len(rngGetTOCTarget) > 1 Then
'                   attempting to delete a native TOC field when
'                   when there's preceding text in the range
'                   can cause an error - unlink as a precaution
                    On Error Resume Next
                    .Fields.Unlink
                    On Error GoTo 0
                    
                    .Delete
                End If
            End With
        ElseIf .Bookmarks.Exists("TableOfContents") Then
            Set rngGetTOCTarget = .Bookmarks("TableOfContents").Range
            If Asc(rngGetTOCTarget.Characters.Last) = 12 Then
                rngGetTOCTarget.MoveEnd wdCharacter, -1
            End If
            If rngGetTOCTarget.Start = ActiveDocument.Range.Start Then
                While Not bIsTOCStyle(rngGetTOCTarget.Paragraphs(1).Style)
                    rngGetTOCTarget.MoveStart wdParagraph, 1
                    If rngGetTOCTarget.End > .Bookmarks("TableOfContents").Range.End Then
                        Set rngGetTOCTarget = .Bookmarks("TableOfContents").Range
                        If Asc(rngGetTOCTarget.Characters.Last) = 12 Then
                            rngGetTOCTarget.MoveEnd wdCharacter, -1
                        End If
                        rngGetTOCTarget.Collapse wdCollapseEnd
                    End If
                Wend
                If Asc(rngGetTOCTarget.Characters.First) = 12 Then
                    rngGetTOCTarget.MoveStart wdCharacter, 1
                End If
            End If
            If Len(rngGetTOCTarget) > 1 Then
                rngGetTOCTarget.Delete
            End If
        ElseIf .Bookmarks.Exists("PTOC") Then
            Set rngGetTOCTarget = .Bookmarks("PTOC").Range
            If Asc(rngGetTOCTarget.Characters.Last) = 12 Then
                rngGetTOCTarget.MoveEnd wdCharacter, -1
            End If
            If Len(rngGetTOCTarget) > 1 Then
                rngGetTOCTarget.Delete
            End If
        ElseIf .Bookmarks.Exists("TOC") Then
            Set rngGetTOCTarget = .Bookmarks("TOC").Range
            If Asc(rngGetTOCTarget.Characters.Last) = 12 Then
                rngGetTOCTarget.MoveEnd wdCharacter, -1
            End If
            If Len(rngGetTOCTarget) > 1 Then
                rngGetTOCTarget.Delete
            End If
        ElseIf rngContent.Find.Execute(mpTOCPlaceholder) Then
'           check for TOC placeholder
            Set rngGetTOCTarget = rngContent.Paragraphs(1).Range
            If Asc(rngGetTOCTarget.Characters.Last) = 12 Then
                rngGetTOCTarget.MoveEnd wdCharacter, -1
            End If
            If Len(rngGetTOCTarget) > 1 Then
                rngGetTOCTarget.Delete
            End If
        ElseIf .TablesOfContents.Count Then
'           check for existing TOC, if it exists, insert after
            Set rngGetTOCTarget = .TablesOfContents(1).Range
            rngGetTOCTarget.Delete

'           check for end of TOC marker - user may have replaced
'           MacPac TOC field with native one
            With rngGetTOCTarget.Paragraphs(1).Range
                If .Text = mpEndOfTOCWarning & vbCr Then _
                    .Delete
            End With
        ElseIf iLoc = mpAboveTOA Then
'           insert above TOA
            xTOA = xGetTOABookmark()
            If xTOA <> "" Then
                Set rngTOAStart = .Bookmarks(xTOA).Range
            ElseIf .TablesOfAuthorities.Count Then
                Set rngTOAStart = .TablesOfAuthorities(1).Range
            End If
            With rngTOAStart
                .StartOf
'                For Each ftrExisting In .Sections(1).Footers
'                    ftrExisting.PageNumbers.RestartNumberingAtSection = False
'                Next ftrExisting
                If .Start = 0 Then
                    lStart = 0
                Else
                    lStart = .Start - 1
                End If
                Set rngTOCStart = docCurrent.Range(lStart, lStart)
            End With
            
            '9.9.4004 - prevent "outside of block-level tag" error
            If g_iWordVersion = mpWordVersion_2007 Then
                rngTOCStart.Select
                If mdlCC.BeforeBlockLevelCC() Then
                    bParaAdded = True
                    Selection.StartOf wdParagraph
                ElseIf mdlCC.AfterBlockLevelCC() Then
                    bParaAdded = True
                    Selection.EndOf wdParagraph
                End If
                If bParaAdded Then
                    Selection.InsertParagraph
                    Selection.StartOf
                    Selection.Style = wdStyleNormal
                End If
                Set rngTOCStart = Selection.Range
            End If
            
            'GLOG 5726 (9.9.6017) - get page numbering style from TOA
            iPageNoStyle = CInt(rngTOAStart.Sections(1).Footers(wdHeaderFooterPrimary) _
                .PageNumbers.NumberStyle)
                
            Set rngGetTOCTarget = rngInsertTOCTOASection(rngTOCStart, False, _
                bRestartPageNumbering, , , , , , , iPageNoStyle)
            
            'GLOG 5355 (9.9.5007) - continue page numbering in TOA section
            'after inserting TOC section, not before (as we were doing previously)
            With rngTOAStart
                .StartOf
                For Each ftrExisting In .Sections(1).Footers
                    ftrExisting.PageNumbers.RestartNumberingAtSection = False
                Next ftrExisting
            End With
            
'        ElseIf .Bookmarks.Exists("mpListOfExhibits") Then
''           toc not found - check for MP List of Exhibits,
''           if it exists, insert before
'            Set rngLOEStart = .Bookmarks("mpListOfExhibits").Range
'
'            With rngLOEStart
'                .StartOf
'                For Each ftrExisting In .Sections(1).Footers
'                    ftrExisting.PageNumbers.RestartNumberingAtSection = False
'                Next ftrExisting
'                Set rngTOCStart = docCurrent.Range(.Start - 1, .Start - 1)
'            End With
'
'            Set rngGetTOCTarget = rngInsertTOCTOASection(rngTOCStart)
        Else
'           force eof or bof is selection is at end
'           or start of doc and user says at insertion point
            If iLoc = mpAtInsertion Then
                Selection.Collapse wdCollapseEnd
                
                'move to start of row
                If Selection.Information(wdWithInTable) Then _
                    Selection.StartOf wdRow
                
                'determine whether at start of block tag
                If g_bXMLSupport Then _
                    bAtStartOfBlockTag = mdlWordXML.CursorIsAtStartOfBlockTag()

'               if cursor is in middle of paragraph, move to end -
                '9.9.4003 - modified conditional to check for text at start of paragraph,
                'rather than looking at the range - this now seems simpler than worrying
                'about content controls and xml tags
'                If (Selection.Start <> Selection.Paragraphs(1).Range.Start) And _
'                        (Not bAtStartOfBlockTag) Then
                If ActiveDocument.Range(Selection.Paragraphs(1).Range.Start, _
                        Selection.Start).Text <> "" Then
                    Selection.SetRange Start:=Selection.Paragraphs(1).Range.End - 1, _
                        End:=Selection.Paragraphs(1).Range.End - 1
                Else
                    '9.9.4003 - move to start of paragraph if necessary
                    Selection.SetRange Start:=Selection.Paragraphs(1).Range.Start, _
                        End:=Selection.Paragraphs(1).Range.Start
                End If
                
                If (Selection.Start = 0) Or InRowAtStartOfDoc() Then
                    iLoc = mpAtBOF
                ElseIf (Selection.End >= ActiveDocument.Content.End - 1) Then
                    iLoc = mpAtEOF
                ElseIf Selection.Paragraphs(1).Range.Text = vbCr And _
                        Selection.Start = Selection.Sections(1).Range.Start Then
'                   in this case, i.e. empty para at start of section, moving a
'                   character will result in an extra section break
                ElseIf (Selection.Start = Selection.Paragraphs(1).Range.Characters.Last.Start) And _
                        (Not bAtStartOfBlockTag) Then
                    Selection.MoveStart wdCharacter, 1
                End If
            End If
            
'           neither TOA nor TOC found - insert at eof
            Select Case iLoc
                Case mpAtBOF
                    Set rngLoc = docCurrent.Content
                    rngLoc.StartOf
                    
                    'ensure that not at start of block tag
                    If g_bXMLSupport Then
                        rngLoc.Select
                        If Not InRowAtStartOfDoc() Then
                            If mdlWordXML.CursorIsAtStartOfBlockTag() Then
                                bParaAdded = True
                                lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, True)
                                Selection.StartOf wdParagraph
                                Selection.InsertParagraph
                                Selection.StartOf
                                Selection.Style = wdStyleNormal
                                If lShowTags Then
                                    lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, _
                                        False)
                                End If
                            ElseIf g_iWordVersion = mpWordVersion_2007 Then
                                '9.9.4003 - prevent "outside of block-level tag" error
                                If mdlCC.BeforeBlockLevelCC() Then
                                    bParaAdded = True
                                    Selection.StartOf wdParagraph
                                    Selection.InsertParagraph
                                    Selection.StartOf
                                    Selection.Style = wdStyleNormal
                                End If
                            End If
                        End If
                    End If
                Case mpAtEOF
                    Set rngLoc = docCurrent.Content
                    rngLoc.EndOf
'                   insert paragraph if necessary
                    If rngLoc.Paragraphs.Last.Range.Characters.Count > 1 Then
                        rngLoc.InsertParagraphAfter
                        rngLoc.EndOf
                    End If
                Case mpAtInsertion
                    'ensure that not at start of block tag
                    If g_bXMLSupport And (Not InRowAtStartOfDoc()) Then
                        If mdlWordXML.CursorIsAtStartOfBlockTag() Then
                            bParaAdded = True
                            lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, True)
                            Selection.StartOf wdParagraph
                            On Error Resume Next
                            Selection.InsertParagraph
                            If Err <> 0 Then
                                'an error will occur if block tag spans more than row
                                If Selection.Information(wdWithInTable) Then
                                    Dim rngBeforeTable As Word.Range
                                    Set rngBeforeTable = rngAddParaBeforeTable(Selection.Tables(1))
                                    rngBeforeTable.Select
                                Else
                                    Err.Raise Err.Number, "mpTOC.rngGetTOCTarget", Err.Description
                                End If
                                On Error GoTo 0
                            End If
                            Selection.StartOf
                            Selection.Style = wdStyleNormal
                            If lShowTags Then
                                lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, _
                                    False)
                            End If
                        ElseIf g_iWordVersion = mpWordVersion_2007 Then
                            '9.9.4003 - prevent "outside of block-level tag" error
                            If mdlCC.BeforeBlockLevelCC() Then
                                bParaAdded = True
                                Selection.StartOf wdParagraph
                            ElseIf mdlCC.AfterBlockLevelCC() Then
                                bParaAdded = True
                                Selection.EndOf wdParagraph
                            End If
                            If bParaAdded Then
                                Selection.InsertParagraph
                                Selection.StartOf
                                Selection.Style = wdStyleNormal
                            End If
                        End If
                    End If
                    
                    Set rngLoc = Selection.Range
            End Select
            
            '9.9.5008 - restore mp10 bookmarks when inserting at cursor
            Set rngGetTOCTarget = rngInsertTOCTOASection(rngLoc, _
                                                         False, _
                                                         bRestartPageNumbering, _
                                                         bBoldTOC, _
                                                         bBoldPage, _
                                                         bCapTOC, _
                                                         bCapPage, _
                                                         bUnderlineTOC, _
                                                         bUnderlinePage, _
                                                         iPageNoStyle, _
                                                         iPunctuation, _
                                                         (iLoc = mpAtInsertion))
            
'            If iLoc = mpAtBOF Then rngGetTOCTarget.Style = wdStyleNormal
        End If
                
        With rngGetTOCTarget
            If .Style <> ActiveDocument.Styles(wdStyleNormal) Then _
                .Style = wdStyleNormal
                
            'delete added paragraph
            If bParaAdded Then
                With .Next(wdSection).Characters(1)
                    If .Text = vbCr Then _
                        .Delete
                End With
            End If
        End With
    End With
End Function
Function bTOAExists(docCurrent As Word.Document) As Boolean
'returns true if TOC exists -
'GLOG 5022 (10/31/11) - added "zzmpFIXED_TableOfAuthorities" and "zzmpTEMP_TOA"
    With docCurrent
        bTOAExists = .Bookmarks.Exists("mpTableOfAuthorities") Or _
                     .Bookmarks.Exists("_zzmpFIXED_TableOfAuthorities") Or _
                     .Bookmarks.Exists("zzmpFIXED_TableOfAuthorities") Or _
                     .Bookmarks.Exists("TableOfAuthorities") Or _
                     .Bookmarks.Exists("PTOA") Or _
                     .Bookmarks.Exists("TOA") Or _
                     .Bookmarks.Exists("zzmpTEMP_TOA") Or _
                     .TablesOfAuthorities.Count
    End With
End Function

Function xGetTOABookmark() As String
'returns name of TOA bookmark
    With ActiveDocument.Bookmarks
        If .Exists("_zzmpFIXED_TableOfAuthorities") Then
            xGetTOABookmark = "_zzmpFIXED_TableOfAuthorities"
        ElseIf .Exists("zzmpFIXED_TableOfAuthorities") Then
            'GLOG 5022 (10/31/11)
            xGetTOABookmark = "zzmpFIXED_TableOfAuthorities"
        ElseIf .Exists("mpTableOfAuthorities") Then
            xGetTOABookmark = "mpTableOfAuthorities"
        ElseIf .Exists("TableOfAuthorities") Then
            xGetTOABookmark = "TableOfAuthorities"
        ElseIf .Exists("PTOA") Then
            xGetTOABookmark = "PTOA"
        ElseIf .Exists("TOA") Then
            xGetTOABookmark = "TOA"
        ElseIf .Exists("zzmpTEMP_TOA") Then
            'GLOG 5022 (10/31/11)
            xGetTOABookmark = "zzmpTEMP_TOA"
        End If
    End With
End Function

Function iTOCExists(docCurrent As Word.Document) As mpTOCExistsMarker
'returns marker type for existing TOC
    
    Const mpTOCPlaceholder As String = "TABLE OF CONTENTS PLACEHOLDER"
    Dim rngContent As Word.Range
    
    With docCurrent
        Set rngContent = docCurrent.Content
        If .Bookmarks.Exists("mpTableOfContents") Or _
                .Bookmarks.Exists("TableOfContents") Or _
                .Bookmarks.Exists("PTOC") Or _
                .Bookmarks.Exists("TOC") Then
            iTOCExists = mpTOCExistsMarker_Bookmark
        ElseIf rngContent.Find.Execute(mpTOCPlaceholder) Then
            iTOCExists = mpTOCExistsMarker_Placeholder
        ElseIf .TablesOfContents.Count Then
            iTOCExists = mpTOCExistsMarker_Field
        End If
    End With
End Function

Function xGetTCEntryText(rngHeading As Word.Range, _
                         iListLevel As Integer, _
                         xNumberTrailChar As String, _
                         Optional bUseQuotePlaceholders As Boolean = False) As String
'   returns unformatted text for non-numbered
'   or single numbered headings. use
'   bInsertTCEntryText for other situations
    Dim mpQ As String
    Dim xLevelSwitch As String
    Dim xCurNumber As String
    Dim xTemp As String
    Dim xHeading As String
    
    mpQ = Chr(34)
    
    xLevelSwitch = " \l " & mpQ & _
                iListLevel & mpQ
    
'   word automatically converts allcaps to ucase,
'   so ensure that allcaps is removed before retrieval
'   then reapplied
    With rngHeading
        If (Not ActiveDocument.Styles(.Style).Font.AllCaps) _
            And .Font.AllCaps Then
            .Font.AllCaps = False
            xHeading = .Text
            .Font.AllCaps = True
        Else
            xHeading = .Text
        End If
    End With
                    
    If bUseQuotePlaceholders Then
'       due to native Word bug that removes preceding quotes from TOC entries,
'       TOC macro now looks for proprietary placeholders
        xTemp = xSubstitute(xHeading, mpQ, "zzmp34h")
        xTemp = xSubstitute(xTemp, ChrW(&H201C), "zzmp147h")
        xTemp = xSubstitute(xTemp, ChrW(&H201D), "zzmp148h")
    Else
'       in filled TC codes, special chrs need to be preceded by "\"
        xTemp = xSubstitute(xHeading, mpQ, "\" & mpQ)
        xTemp = xSubstitute(xTemp, ChrW(&H201C), "\" & ChrW(&H201C))
        xTemp = xSubstitute(xTemp, ChrW(&H201D), "\" & ChrW(&H201D))
    End If
    
'   get number
    xCurNumber = rngHeading.ListFormat.ListString

'   concatenate
    If Len(xCurNumber) Then
        xTemp = mpQ & xCurNumber & _
                xNumberTrailChar & xTemp & _
                mpQ & xLevelSwitch
    Else
        xTemp = mpQ & xTemp & mpQ & _
                xLevelSwitch
    End If
            
'   return
    xGetTCEntryText = xTemp
End Function

Function bInsertTCEntryText(rngHeading As Word.Range, _
                            rngTCEntryTextStart As Word.Range, _
                            iListLevel As Integer, _
                            xNumberTrailChar As String, _
                            Optional bUseQuotePlaceholders As Boolean = False) As Boolean
'   inserts rngHeading as TCEntry
'   text. Use when heading formatting is
'   transferred to TOC, or when heading
'   contains multiple numbers. Use
'   xGetTCEntryText otherwise (it's faster)

    Dim rngRef As Word.Range
    Dim fldRef As Word.Field
    Dim xListLevelSwitch As String
    Dim xCurNumber As String
    Dim lXRefPos As Long
    Dim bmkHeading As Bookmark
    Dim xHeadingText As String
    Dim bReset34 As Boolean
    Dim bReset147 As Boolean
    Dim bReset148 As Boolean
        
    xListLevelSwitch = " \l " & Chr(34) & _
                iListLevel & Chr(34)
    
'   mark heading for later reference
    With rngHeading
    '   if last character in heading is a straight quote,
    '   use placeholder (fixes a native Word bug);
    '   otherwise, quotes need to be preceded by "\"
'        If Right(.Text, 1) = Chr(34) Then
'            .Characters.Last.Text = "zzmp34h"
'            .MoveEnd wdCharacter, 7
'        End If

        'GLOG 5620 - if range ends with a quote and space, shrink it
        'to exclude these characters - they will result in a backslash
        'in the TOC entry and the trailing quote won't get included in
        'the TOC in any case (see GLOG 5576)
        xHeadingText = .Text
        If (Right$(xHeadingText, 2) = Chr(34) & " ") Or _
                (Right$(xHeadingText, 2) = ChrW(&H201D) & " ") Then
            .MoveEnd wdCharacter, -2
        End If
        
        .Bookmarks.Add "zzmpTCEntryTemp"
        Set bmkHeading = ActiveDocument.Bookmarks("zzmpTCEntryTemp")
        xHeadingText = .Text
    End With

'   place slash in front of special chrs -
'   these operations are time hogs,
'   so do only when necessary
    With bmkHeading.Range.Find
        .ClearFormatting
        .Wrap = wdFindStop
                
        If InStr(xHeadingText, Chr(34)) Then
            .Text = "^0034"
            If bUseQuotePlaceholders Then
                .Replacement.Text = "zzmp34h"
            Else
                .Replacement.Text = "\" & Chr(34)
            End If
            .Execute Replace:=wdReplaceAll
            bReset34 = True
        End If
            
        If InStr(xHeadingText, ChrW(&H201C)) Then
            .Text = ChrW(&H201C)
            If bUseQuotePlaceholders Then
                .Replacement.Text = "zzmp147h"
            Else
                .Replacement.Text = "\" & ChrW(&H201C)
            End If
            .Execute Replace:=wdReplaceAll
            bReset147 = True
        End If
            
        If InStr(xHeadingText, ChrW(&H201D)) Then
            .Text = ChrW(&H201D)
            If bUseQuotePlaceholders Then
                .Replacement.Text = "zzmp148h"
            Else
                .Replacement.Text = "\" & ChrW(&H201D)
            End If
            .Execute Replace:=wdReplaceAll
            bReset148 = True
        End If
    End With
    
'   insert formatted text
    With rngTCEntryTextStart
        .InsertAfter Chr(34)
        
'       insert number if list template is applied
        With rngHeading.ListFormat
            If .ListType <> wdListListNumOnly And _
                        .ListType <> wdListNoNumbering Then
'               get number
                xCurNumber = rngHeading.ListFormat.ListString
        
'               insert number and trailing char
                rngTCEntryTextStart.InsertAfter xCurNumber & xNumberTrailChar
            End If
        End With
        
'       mark position for ref field insert below
        lXRefPos = rngTCEntryTextStart.End
        
        .InsertAfter Chr(34) & xListLevelSwitch
        
        Set rngRef = ActiveDocument.Range(lXRefPos, lXRefPos)
        Set fldRef = .Fields.Add(rngRef, _
                                 wdFieldRef, _
                                 "zzmpTCEntryTemp", _
                                 True)
                                 
    End With

'   remove slashes if necessary
    With bmkHeading.Range
        With .Find
            .ClearFormatting
            .Wrap = wdFindStop
            
            If bReset34 Then
                If bUseQuotePlaceholders Then
                    .Text = "zzmp34h"
                Else
                    .Text = "\^0034"
                End If
                .Replacement.Text = Chr(34)
                .Execute Replace:=wdReplaceAll
            End If
            
            If bReset147 Then
                If bUseQuotePlaceholders Then
                    .Text = "zzmp147h"
                Else
                    .Text = "\" & ChrW(&H201C)
                End If
                .Replacement.Text = ChrW(&H201C)
                .Execute Replace:=wdReplaceAll
            End If
            
            If bReset148 Then
                If bUseQuotePlaceholders Then
                    .Text = "zzmp148h"
                Else
                    'GLOG 5619 - search text previously left out slash
                    .Text = "\" & ChrW(&H201D)
                End If
                .Replacement.Text = ChrW(&H201D)
                .Execute Replace:=wdReplaceAll
            End If
        End With
    End With
    
'   delete bookmark
    On Error Resume Next
    bmkHeading.Delete
    
End Function

Public Function bIsPleadingDocument(docDoc As Word.Document) As Boolean
    With docDoc
        If InStr(UCase(.AttachedTemplate), "PLEAD") Or _
                InStr(UCase(.AttachedTemplate), TemplatePOS) Or _
                InStr(UCase(.AttachedTemplate), "VER") Then
            bIsPleadingDocument = True
        End If
    End With
End Function

Function iGetParagraphLevel(rngLocation As Word.Range, _
                            iDefaultLevel As Integer) As Integer
'returns the paragraph outline level
'of the first paragraph of rnglocation

    Dim iLevel As Integer

'   get list type
    Select Case iGetListType(rngLocation)
        Case mpListTypeNative
            iLevel = rngLocation.ListFormat.ListLevelNumber
        Case mpListTypeMixed
            iLevel = rngLocation.ListFormat.ListLevelNumber
        Case mpListTypeNoNumbering
            If iDefaultLevel > 0 Then
'               set level to the default
                iLevel = iDefaultLevel
            Else
'               prompt user for level
                iLevel = iGetTOCLevel(rngLocation)
            End If
    End Select

    iGetParagraphLevel = iLevel
    
End Function

Function xNoPageNumberRange(xTOCScheme As String, _
                            iMinLevel As Integer, _
                            iMaxLevel As Integer) As String
'returns string for use with page number switch of TOC field
'if not contiguous, returns "NC", indicating that
'page numbers have to be removed manually where appropriate

    Dim i As Integer
    Dim iNumBreaks As Integer
    Dim xPageNo As String
    Dim xRange As String
    Dim iStartRange As Integer
    Dim iEndRange As Integer
    
    For i = iMinLevel To iMaxLevel
        xPageNo = xGetLevelProp(xTOCScheme & "_L", _
                                i, _
                                mpTOCLevelProp_IncludePgNum, _
                                mpSchemeType_Document)
'       a break occurs when going from level with page no
'       to level w/o page no or vice versa; if more than
'       one break, range isn't contiguous
        If xPageNo = "0" Then
            If iEndRange > 0 And iEndRange < (i - 1) Then
'               there's a previous range w/o page numbers
                iNumBreaks = iNumBreaks + 1
            End If
            If iStartRange = 0 Then _
                iStartRange = i
            iEndRange = i
        Else
            If iEndRange > 0 And iEndRange = (i - 1) Then
'               the previous level had no page number
                iNumBreaks = iNumBreaks + 1
            End If
        End If
    Next i
    
    If iNumBreaks > 1 Then
        xRange = "NC"
    ElseIf iStartRange > 0 Then
        xRange = Trim(Str(iStartRange))
        If iEndRange > iStartRange Then _
            xRange = xRange & "-" & Trim(Str(iEndRange))
    End If
    
    xNoPageNumberRange = xRange
End Function

Function bMarkAll() As Boolean
'marks selected paragraphs as specified
'by user - ignores currently marked paras
    Dim i As Integer
    Dim iLevel As Integer
    Dim bShowAllStart As Boolean
    Dim rngP As Word.Range
    Dim rngStart As Word.Range
    Dim xScheme As String
    Dim iNumSchemes As Integer
    Dim xDelimiter As String
    Dim oForm As VB.Form
    Dim iMarkAction As mpMarkActions
    Dim iFormatAction As mpFormatActions
    Dim iType As mpMarkingModes
    Dim iScope As mpMarkFormatScopes
    Dim xTargetStyles As String
    Dim xKey As String
    Dim xStyle As String
    Dim iHeadingFormat As Integer
    
    On Error GoTo ProcError
    
'   get document schemes
    iNumSchemes = iGetSchemes(g_xDSchemes(), _
                              mpSchemeType_Document)

'   if no schemes, don't display dialog
    If iNumSchemes = 0 Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "Aucun th�me de num�rotation MacPac dans ce document.", vbInformation, "Num�rotation MacPac"
        Else
            MsgBox "There are no MacPac numbering schemes " & _
                "in this document.", vbInformation, "MacPac Numbering"
        End If
        Exit Function
    End If
    
    If g_lUILanguage = wdFrenchCanadian Then
        Set oForm = New frmMarkAllFrench
    Else
        Set oForm = New frmMarkAll
    End If
    
    With oForm
        .Show vbModal
        
'       exit if cancelled
        If .m_bFinished = False Then
            Unload oForm
            Exit Function
        End If
        
'       get current selection for later reselection
        Set rngStart = Selection.Range
        Set rngP = Selection.Range
        
        Application.ScreenUpdating = False
        
        With ActiveDocument.ActiveWindow.View
            bShowAllStart = .ShowAll
            .ShowAll = True
        End With
        
        'set actions
        If .chkMarkTC Then
            iMarkAction = mpMarkAction_Mark
        ElseIf .chkUnmark Then
            iMarkAction = mpMarkAction_Unmark
        Else
            iMarkAction = mpMarkAction_None
        End If
        If .chkFormat Then
            iFormatAction = mpFormatAction_Format
        ElseIf .chkUnformat Then
            iFormatAction = mpFormatAction_Unformat
        Else
            iFormatAction = mpFormatAction_None
        End If
        
        'set type
        If g_bMarkWithStyleSeparators Then
            iType = mpMarkingMode_StyleSeparators
        Else
            iType = mpMarkingMode_TCCodes
        End If
        
        'set scope
        Select Case .cbxScope.Text
            Case "Current Document", "Document en cours"
                iScope = mpMarkFormatScope_Document
            Case "Current Section", "Section en cours"
                iScope = mpMarkFormatScope_Section
            Case Else
                iScope = mpMarkFormatScope_Selection
        End Select
        
        'set heading definition - spaces are displayed in dlg as bullets
        xDelimiter = xSubstitute(.txtDefinition, ChrW(&HB7), Chr(32))
        xDelimiter = xSubstitute(xDelimiter, Chr(183), Chr(32))
        
        'set styles and formatting options for selected levels
        For iLevel = 1 To 9
            'styles
            If .chkLevel(iLevel - 1) Then
                xScheme = g_xDSchemes(.cbxScheme.Bookmark, 0)
                If bLevelExists(xScheme, iLevel, mpSchemeType_Document) Then _
                    xTargetStyles = xTargetStyles & xGetStyleName(xScheme, iLevel) & "|"
            
                If .optAllSchemes Then
                    For i = 0 To UBound(g_xDSchemes)
                        If i <> .cbxScheme.Bookmark Then
                            xScheme = g_xDSchemes(i, 0)
                            If bLevelExists(xScheme, iLevel, mpSchemeType_Document) Then _
                                xTargetStyles = xTargetStyles & xGetStyleName(xScheme, iLevel) & "|"
                        End If
                    Next i
                End If
            End If
        Next iLevel
        
        'apply formatting changes
        If iFormatAction = mpFormatAction_Format Then
            Dim oFormattingChanges As Scripting.Dictionary
            Set oFormattingChanges = .FormattingChanges
            For i = 1 To oFormattingChanges.Count
                xKey = oFormattingChanges.Keys(i - 1)
                xScheme = Left$(xKey, Len(xKey) - 1)
                iLevel = CInt(Right$(xKey, 1))
                iHeadingFormat = CInt(oFormattingChanges(xKey))
                lSetLevelProp xScheme, iLevel, mpNumLevelProp_HeadingFormat, _
                    CStr(iHeadingFormat), mpSchemeType_Document
            Next i
        End If
                                
        'mark/format headings
        bMarkAndFormatHeadings xTargetStyles, iMarkAction, iFormatAction, iType, _
            xDelimiter, iScope, True, mpMarkingReplacementMode_Prompt
    End With
    
'   clean up
    Unload oForm
    rngStart.Select
    ActiveDocument.ActiveWindow.View.ShowAll = bShowAllStart
    EchoOn
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    bMarkAll = True
    Exit Function
    
ProcError:
    rngP.Select
    ActiveDocument.ActiveWindow.View.ShowAll = bShowAllStart
    EchoOn
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    Select Case Err.Number
        Case Else
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox Error(Err.Number), vbExclamation, "Num�rotation MacPac"
            Else
                MsgBox Error(Err.Number), vbExclamation, AppName
            End If
    End Select
    Exit Function
End Function

Function EditCustomTOCScheme() As Boolean
'displays TOC customization form.
'if xScheme is "custom", sets defaults in ini;
'otherwise, modifies document scheme.
    Dim i As Integer
    Dim xStyle As String
    Dim styTOC As Word.Style
    Dim oForm As VB.Form
    
    If g_lUILanguage = wdFrenchCanadian Then
        Set oForm = New frmEditTOCFrench
    Else
        Set oForm = New frmEditTOC
    End If
    
    With oForm
        .EditCustomScheme = True
        .Show vbModal

        If .Cancelled Then
            Unload oForm
            Exit Function
        End If
        
'       set defaults
        bAppSetUserINIValue "TOC", "LineSpacing", _
            .cmbLineSpacing.SelectedItem
        bAppSetUserINIValue "TOC", "PointsBefore", _
            .spnBefore.Value
        bAppSetUserINIValue "TOC", "PointsBetween", _
            .spnBetween.Value
        bAppSetUserINIValue "TOC", "PageNumbers", _
            .cmbPageNums.SelectedItem
        bAppSetUserINIValue "TOC", "DotLeaders", _
            .cmbDotLeaders.SelectedItem
        bAppSetUserINIValue "TOC", "AllCaps", _
            .cmbAllCaps.SelectedItem
        bAppSetUserINIValue "TOC", "Bold", _
            .cmbBold.SelectedItem
        bAppSetUserINIValue "TOC", "CenterLevel1", _
            .chkCenterLevel1
        bAppSetUserINIValue "TOC", "DefaultToCustom", _
            .chkDefaultScheme
    End With

    EditCustomTOCScheme = True
    Unload oForm
    Application.ScreenUpdating = True
End Function

Sub Modify()
'displays TOC customization form and
'modifies the active document TOC scheme.
    Dim i As Integer
    Dim xStyle As String
    Dim styTOC As Word.Style
    
    On Error GoTo ProcError
        
    With frmEditTOC
        .EditCustomScheme = False
        .Show vbModal
        
        If .Cancelled Then
            Unload frmEditTOC
            Application.ScreenUpdating = True
            Exit Sub
        End If
        
        Application.ScreenUpdating = False
        
'       modify TOC styles and doc props
        For i = 1 To 9
            xStyle = xTranslateTOCStyle(i)
            Set styTOC = ActiveDocument.Styles(xStyle)
                
'           paragraph
            With styTOC.ParagraphFormat
                If Len(frmEditTOC.cmbLineSpacing.Text) Then _
                    .LineSpacingRule = frmEditTOC.cmbLineSpacing.Bookmark
                .SpaceAfter = frmEditTOC.spnBetween.Value
                If Len(frmEditTOC.cmbDotLeaders) Then
                    Select Case frmEditTOC.cmbDotLeaders.Bookmark
                        Case mpTOCLevels_All
                            .TabStops(1).Leader = 1
                        Case mpTOCLevels_1
                            .TabStops(1).Leader = Abs(i = 1)
                        Case mpTOCLevels_Subsequent
                            .TabStops(1).Leader = Abs(i > 1)
                        Case mpTOCLevels_None
                            .TabStops(1).Leader = 0
                    End Select
                End If
                
                If (frmEditTOC.chkCenterLevel1 = vbChecked) And (i = 1) Then
                    .Alignment = wdAlignParagraphCenter
                End If
                
'               do step indents
'                If frmEditTOC.chkUseStepIndents = vbChecked Then
'                    .Alignment = wdAlignParagraphLeft
'                    .LeftIndent = i * 36
'                    .FirstLineIndent = -36
'                End If
            End With
                
'           font
            If Len(frmEditTOC.cmbAllCaps) Then
                Select Case frmEditTOC.cmbAllCaps.Bookmark
                    Case mpTOCLevels_All
                        styTOC.Font.AllCaps = True
                    Case mpTOCLevels_1
                        styTOC.Font.AllCaps = (i = 1)
                    Case mpTOCLevels_Subsequent
                        styTOC.Font.AllCaps = (i > 1)
                    Case mpTOCLevels_None
                        styTOC.Font.AllCaps = False
                End Select
            End If

''          THIS NEEDS TO GO BACK IN
''           doc props
'            If Len(frmEditTOC.txtBefore) Then
'                lSetLevelProperty xScheme & "_L", _
'                                  i, _
'                                  mpDPSpaceBeforeTOCLevel, _
'                                  .txtBefore, _
'                                  mpSchemeType_Document
'            End If
'
''           page numbers
'            If Len(frmEditTOC.cmbPageNums) Then
'                Dim bDo As Boolean
'                Select Case frmEditTOC.cmbPageNums.ListIndex
'                    Case mpTOCLevels_All
'                        bDo = True
'                    Case mpTOCLevels_1
'                        bDo = (i = 1)
'                    Case mpTOCLevels_Subsequent
'                        bDo = (i > 1)
'                End Select
'                lSetLevelProperty xScheme & "_L", _
'                                  i, _
'                                  mpDPPageNumberInTOC, _
'                                  bDo, _
'                                  mpSchemeType_Document
'            End If
        Next i
    End With
    
    Unload frmEditTOC
    Application.ScreenUpdating = True
    Exit Sub
ProcError:
    Unload frmEditTOC
    RaiseError "MPTOC90.mpTOC.Modify"
    Exit Sub
End Sub

Function bCustomizeGenericStyles(udtFormat As TOCFormat, _
                                 bIncludeSchedule As Boolean) As Boolean
'redefines TOC 1-9 based on values in user ini
    
    Dim i As Integer
    Dim iSpacing As Integer
    Dim sBetween As Single
    Dim iDotLeaders As Integer
    Dim iDotSubsequent As Integer
    Dim iAllCaps As Integer
    Dim iCapSubsequent As Integer
    Dim xIniValue As String
    
'   redefine styles
    For i = 1 To 9
        If (i = 9) And bIncludeSchedule Then Exit Function
        With ActiveDocument.Styles(xTranslateTOCStyle(i))
            With .ParagraphFormat
                If udtFormat.LineSpacing <> mpCGSSkipItem Then _
                    .LineSpacingRule = udtFormat.LineSpacing
                If udtFormat.SpaceBetween <> mpCGSSkipItem Then _
                    .SpaceAfter = CSng(udtFormat.SpaceBetween)
                .TabStops(1).Leader = Abs(udtFormat.IncludeDotLeaders(i))
            End With

'            If iAllCaps <> mpCGSSkipItem Then
'                .Font.AllCaps = udtFormat.AllCaps(i)
'            End If
        End With
    Next i
End Function

Sub TagTOCEntries(iStartLevel As Integer, _
                  iEndLevel As Integer, _
                  iEntryType As mpTOCEntryTypes, _
                  bTagAllOutlineParas As Boolean, _
                  xStyleList As String, _
                  bApplyTOC9 As Boolean, _
                  xTOC9Style As String, _
                  bDesignate As Boolean)
'tags document para entries in prep for TOC - will tag
'all outline level paras if specified, else will tag
'those outline level paras in Style List.
    Dim paraP As Word.Paragraph
    Dim rngPara As Word.Range
    Dim bIsMarked As Boolean
    Dim bIsMarkedDyn As Boolean
    Dim bIsMarkedPDyn As Boolean
    Dim bIsMarkedStatic As Boolean
    Dim lPos As Long
    Dim l As Long
    Dim lParas As Long
    Dim iLevel As Integer
    Dim rngCode As Word.Range
    Dim bIncludeTCEntries As Boolean
    Dim xStyle As String
    Dim xScheme As String
    Dim bKeepDynamic As Boolean
    Dim bIsMPLT As Boolean
    Dim xStyTest As String
    Dim iDisplay As Integer
    Dim lShowTags As Long
    
'   set progress indicator
    g_oStatus.Show 3, g_xTOCStatusMsg
    
    'show xml tags if necessary
    If g_bXMLSupport Then
        lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, True)
    End If
    
    lParas = Word.ActiveDocument.Paragraphs.Count
    
'   cycle through all paras
    For Each paraP In Word.ActiveDocument.Paragraphs
        bIsMarked = False
        bIsMarkedStatic = False
        bIsMarkedDyn = False
        bIsMarkedPDyn = False
        bKeepDynamic = False
        xStyle = ""
        xScheme = ""
        
        Set rngPara = paraP.Range
        If ((rngPara.ListFormat.ListType = wdListNoNumbering) And _
                (rngPara = vbCr)) Or (rngPara = Chr(12)) Then
            GoTo NextPara
        End If
        
'       check for existing tc code
        lPos = InStr(rngPara.Text, Chr(19) & " " & g_xTCPrefix & " ")
        If lPos Then
'           it's marked - get type of mark
            Set rngCode = rngPara.Duplicate
            rngCode.MoveStart wdCharacter, lPos
            Set rngCode = rngCode.Fields(1).Code
            bIsMarked = True
            
'           static mark will have 'tc "'
            bIsMarkedStatic = InStr(UCase(rngCode), UCase(" " & g_xTCPrefix & " " & Chr(34)))
            
            If Not bIsMarkedStatic Then
'               partial dynamic markings have level but no text
                bIsMarkedPDyn = InStr(UCase(rngCode), UCase(" " & g_xTCPrefix & " \l """))
                If Not bIsMarkedPDyn Then
'                   fully dynamic markings don't have either level or text;
'                   determine whether TC code should actually be fully
'                   dynamic, i.e. it may be from Numbering 8.0
                    With rngCode
                        If .ParagraphFormat.OutlineLevel <> wdOutlineLevelBodyText Then
                            On Error Resume Next 'GLOG 2847 - moved up a line
                            xStyle = .Style
                            xScheme = xGetLTRoot(.ListFormat.ListTemplate.Name)
                            On Error GoTo 0
                            If xScheme <> "" Then
                                bIsMPLT = bIsMPListTemplate(.ListFormat.ListTemplate)
                            End If
                        End If
                    End With
                    If xStyle = "" Then
                        bKeepDynamic = False
                    ElseIf bIsMPLT Then
                        bKeepDynamic = True
                    ElseIf bIsHeadingStyle(xStyle) Then
                        bKeepDynamic = True
                    ElseIf xConditionalScheme(rngCode) <> "" Then
                        bKeepDynamic = True
                    End If
                    
                    If bKeepDynamic Then
'                       include only specified levels
                        bIsMarkedDyn = (paraP.OutlineLevel >= iStartLevel) And _
                            (paraP.OutlineLevel <= iEndLevel)
                    Else
'                       this should be partially dynamic - assume level one
                        rngCode.InsertAfter "\l " & Chr(34) & "1" & Chr(34) & " "
                    End If
                End If
            End If
        Else
            bIsMarked = False
        End If
        
'       marked schedule styles should be tagged as TC entries;
'       bReworkTOC will delete duplicates
        If bIsMarked And bApplyTOC9 And (xTOC9Style <> "") Then
'           note - rngPara.Style can be Nothing in Word XP; in this case,
'           rngPara.Paragraphs(1).Style will usually yield a value; this
'           alternative needs to be error trapped as well, however, because
'           a single paragraph may contain multiple styles in some field results,
'           e.g. native TOA in Word XP (log item 3185)
            On Error Resume Next
            xStyTest = rngPara.Style
            If xStyTest = "" Then _
                xStyTest = rngPara.Paragraphs(1).Style
            On Error GoTo 0
            If xStyTest = xTOC9Style Then
                bIsMarkedDyn = (paraP.OutlineLevel >= iStartLevel) And _
                    (paraP.OutlineLevel <= iEndLevel)
            End If
            xStyTest = ""
        End If

'       odd entries mean include tc entries
        bIncludeTCEntries = (iEntryType Mod 2 = 1)
        
        If (iEntryType = mpTOCEntryType_TCEntries) Then
            If bIsMarkedDyn Then
                TagAsTCEntry rngPara, lPos, xTOC9Style, bDesignate
            ElseIf paraP.OutlineLevel <> wdOutlineLevelBodyText Then
                TagToDelete paraP
            End If
        Else
'           tag for sentence or para if para is:
'           1)not marked, 2) marked static
'           3)marked dynamic and tc entries are
'           not being included the toc
            If bIsMarkedDyn And bIncludeTCEntries Then
                TagAsTCEntry rngPara, lPos, xTOC9Style, bDesignate
            ElseIf paraP.OutlineLevel <> wdOutlineLevelBodyText Then
                If (bIsMarkedStatic Or bIsMarkedPDyn) And bIncludeTCEntries Then
'                   this will prevent duplicates
                    TagToDelete paraP
                Else
'                   tag for either first sentence
'                   or whole para - ignore any tc codes
                    TagAsStyle paraP, _
                               iEntryType, _
                               bTagAllOutlineParas, _
                               xStyleList, _
                               iStartLevel, _
                               iEndLevel, _
                               bApplyTOC9, _
                               xTOC9Style, _
                               bDesignate
                End If
            End If
        End If
        
'       tag hard page break immediately preceding paragraph mark;
'       this leads to extra TOC entries which need to be removed
        With rngPara
            lPos = InStr(.Text, Chr(12) & mpEndHeading)
            If lPos = 0 Then _
                lPos = InStr(.Text, Chr(12) & mpEndPara)
            If lPos = 0 Then _
                lPos = InStr(.Text, mpStartPara & Chr(12))
            If lPos Then
                .MoveEndUntil Chr(12), wdBackward
                .InsertAfter mpPageBreakMarker
            End If
        End With

'       free up resources
        If Not g_bPreserveUndoListTOC Then _
            ActiveDocument.UndoClear
NextPara:
'       update status - % complete
        l = l + 1
        If ((l / lParas) * 100) > (iDisplay + 10) Then
            iDisplay = iDisplay + 10
            If g_lUILanguage = wdFrenchCanadian Then
                Application.StatusBar = "Pr�paration du document pour TM: " & iDisplay & "%"
            Else
                Application.StatusBar = "Preparing document for TOC: " & iDisplay & "%"
            End If
        End If
        Set paraP = Nothing
    Next paraP
    
    'rehide xml tags if necessary
    If lShowTags Then
        lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, False)
    End If
    
    Application.StatusBar = ""
End Sub

Private Function TagAsTCEntry(rngPara As Word.Range, _
                              lPos As Long, _
                              xTOC9Style As String, _
                              bDesignate As Boolean)
    Dim xScheme As String
    Dim rngP As Word.Range
    Dim lPos2 As Long
    Dim bIsMPLT As Boolean
    Dim xStyle As String
    Dim oStyle As Word.Style
    Dim lStart As Long
    Dim lEnd As Long
    Dim iTags As Integer
    Dim lCCSafeStart As Long
    
    With rngPara
'       para is marked with tc code-
        xScheme = Empty
        
'       strip alias from style name
        Set oStyle = .Style
        
        'GLOG 2847
        If oStyle Is Nothing Then _
            Exit Function
            
        xStyle = StripStyleAlias(oStyle.NameLocal)
        
'       scheme is either "Heading" or name of list template
        If bDesignate Then
'           if designating individually, use style name sans alias
            xScheme = xStyle
        ElseIf bIsHeadingStyle(xStyle) Then
            xScheme = "Heading"
        ElseIf xStyle = xTOC9Style Then
            xScheme = xTOC9Style
        Else
            On Error Resume Next
            xScheme = xGetLTRoot(.ListFormat.ListTemplate.Name)
            On Error GoTo 0
            
            If xScheme <> "" Then
                bIsMPLT = bIsMPListTemplate(.ListFormat.ListTemplate)
            End If

            If xScheme = "" Then
'               check if it's a conditional paragraph
                xScheme = xConditionalScheme(rngPara)
            ElseIf Not bIsMPLT Then
'               don't label non-MacPac scheme
                xScheme = ""
            End If
        End If
            
        'hide tags if necessary
        If g_bXMLSupport Then
            mdlWordXML.SetXMLMarkupState ActiveDocument, False
        End If
        
        'GLOG 5075 - content control at start of paragraph was
        'causing entry to get mistagged - not an issue with tags
        If g_iWordVersion = mpWordVersion_2007 Then _
            lCCSafeStart = mdlCC.GetCCSafeParagraphStart(rngPara)

'       add 'tc marked' tag directly before the tc code
        Set rngP = .Duplicate
        With rngP
            If lCCSafeStart > 0 Then 'GLOG 5075
                .SetRange lCCSafeStart, lCCSafeStart
            Else
                .StartOf
            End If
            .Move wdCharacter, lPos - 1
            .InsertAfter mpTCMarked
            .StartOf wdParagraph
        End With
        
'       lPos2 is closing brace of tc code (chr 21)
        lPos2 = InStr(lPos + Len(mpStartPara), .Text, Chr(21))
        
'       add EndHeading tag after tc code -
'       include 'level' tag if level was
'       present in TOC code - if not present
'       we'll get the outline level later.
        Set rngP = .Duplicate
        With rngP
            If lCCSafeStart > 0 Then 'GLOG 5075
                .SetRange lCCSafeStart, lCCSafeStart
            Else
                .StartOf
            End If
            .Move wdCharacter, lPos2
            .InsertAfter mpEndHeading & _
                    mpSchemeNameStart & _
                    xScheme & _
                    mpSchemeNameEnd
        End With
        
'       add EndPara tag before para mark
        Set rngP = .Duplicate
        With rngP
            'get xml tag/cc adjusted paragraph end
            If g_bXMLSupport Then
                mdlWordXML.SetXMLMarkupState ActiveDocument, True
                lEnd = mdlWordXML.GetTagSafeParagraphEnd(rngPara, iTags)
                '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                If (g_iWordVersion = mpWordVersion_2007) And (iTags = 0) Then _
                    lEnd = mdlCC.GetCCSafeParagraphEnd(rngPara)
                .SetRange .Start, lEnd
            End If
            .EndOf
            If Asc(.Previous(wdCharacter)) = 13 Then _
                .Move wdCharacter, -1
            .InsertAfter mpEndPara
        End With
        
        Set rngP = .Duplicate
        With rngP
            'get xml tag/cc adjusted paragraph start
            If g_bXMLSupport Then
                lStart = mdlWordXML.GetTagSafeParagraphStart(rngPara, iTags)
                '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                If (g_iWordVersion = mpWordVersion_2007) And (iTags = 0) Then _
                    lStart = lCCSafeStart
                .SetRange lStart, .End
            End If
            If .Characters(1).Text = Chr(12) Then _
                .MoveStart
            .InsertBefore mpStartPara
        End With
   End With
End Function

Private Function TagToDelete(ByVal paraP As Word.Paragraph)
'inserts 'deletion' tag at end of para
    Dim rngP As Word.Range
    Dim lStart As Long
    Dim lEnd As Long
    Dim iTagsAtStart As Integer
    Dim iTagsAtEnd As Integer
    
    Set rngP = paraP.Range

    With rngP
        'get xml tag/cc adjusted paragraph start
        If g_bXMLSupport Then
            lStart = mdlWordXML.GetTagSafeParagraphStart(rngP, iTagsAtStart)
            lEnd = mdlWordXML.GetTagSafeParagraphEnd(rngP, iTagsAtEnd)
            If (g_iWordVersion = mpWordVersion_2007) And (iTagsAtStart + iTagsAtEnd = 0) Then
                '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                lStart = mdlCC.GetCCSafeParagraphStart(rngP)
                lEnd = mdlCC.GetCCSafeParagraphEnd(rngP)
            End If
            .SetRange lStart, lEnd
        End If
    
'       marking start of para is necessary to distinguish second part of
'       entry split by Word 97 page break bug - see bReworkTOC
        If .Characters(1).Text = Chr(12) Then _
            .MoveStart
        .InsertBefore mpStartPara
    
        .EndOf
        If Asc(.Previous(wdCharacter)) = 13 Then _
            .Move wdCharacter, -1
        .InsertAfter mpEndHeading & _
                mpSchemeNameStart & _
                "zzmpDel" & _
                mpSchemeNameEnd & _
                mpEndPara
    End With
End Function

Public Sub TagAsStyle(paraP As Word.Paragraph, _
                      ByVal iEntryType As mpTOCEntryTypes, _
                      ByVal bTagAllOutlineParas As Boolean, _
                      ByVal xStyleList As String, _
                      ByVal iStartLevel As Integer, _
                      ByVal iEndLevel As Integer, _
                      ByVal bApplyTOC9 As Boolean, _
                      ByVal xTOC9Style As String, _
                      ByVal bDesignate As Boolean)
'tags paraP for either a sentence or paragraph
    Dim rngPara As Word.Range
    Dim rngP As Word.Range
    Dim bIsIncluded As Boolean
    Dim xScheme As String
    Dim rngSentence As Word.Range
    Dim iStyleType As mpTOCEntryTypes
    Dim lPos As Long
    Dim lPos2 As Long
    Dim lPos3 As Long
    Dim iPos As Integer
    Dim xStyle As String
    Dim i As Integer
    Dim xTest As String
    Dim bIsMPLT As Boolean
    Dim bIsQuote As Boolean
    Dim oStyle As Word.Style
    Dim lStart As Long
    Dim lEnd As Long
    Dim iCount As Integer
    Dim iTagsAtStart As Integer
    Dim iTagsAtEnd As Integer
    
    Set rngPara = paraP.Range
    
    With rngPara
'       first strip alias from heading style name
        Set oStyle = paraP.Style
        
        'GLOG 2847
        If oStyle Is Nothing Then _
            Exit Sub
            
        xStyle = StripStyleAlias(oStyle.NameLocal)
        
        If bTagAllOutlineParas Then
'           tag if the outline level of the para
'           is in the level range of the TOC
            bIsIncluded = bDesignate Or _
                          ((paraP.OutlineLevel <= iEndLevel) And _
                          (paraP.OutlineLevel >= iStartLevel))
        Else
'           tag para if para style is in style list
            bIsIncluded = InStr(xStyleList, xStyle)
        End If
        
        If bIsIncluded Then
'           get style type by subtracting value for tc entries
'           if they are being included in toc
            If iEntryType Mod 2 Then
                iStyleType = iEntryType - mpTOCEntryType_TCEntries
            Else
                iStyleType = iEntryType
            End If
            
'           scheme is either "Heading" or name of list template
            If bDesignate Then
'               if designating individually, use style name sans alias
                xScheme = xStyle
            ElseIf bIsHeadingStyle(xStyle) Then
                xScheme = "Heading"
            ElseIf bApplyTOC9 And (xStyle = xTOC9Style) Then
                xScheme = xTOC9Style
'            ElseIf bIsScheduleStyle(xStyle) Then
'                xScheme = "Schedule"
            Else
                On Error Resume Next
                xScheme = xGetLTRoot(rngPara.ListFormat.ListTemplate.Name)
                On Error GoTo 0
            
                If xScheme <> "" Then
                    bIsMPLT = bIsMPListTemplate(.ListFormat.ListTemplate)
                End If
                
                If xScheme = "" Then
'                   check if it's a conditional paragraph
                    xScheme = xConditionalScheme(rngPara)
                    
                    'GLOG 4922 - if unlinked MacPac style, infer scheme name from style
                    If xScheme = "" Then
                        lPos = InStr(xStyle, "_L")
                        If (lPos > 0) And (lPos = Len(xStyle) - 2) Then
                            xScheme = "zzmp" & Left$(xStyle, lPos - 1)
                        End If
                    End If
                ElseIf Not bIsMPLT Then
'                   don't label non-MacPac scheme
                    xScheme = ""
                End If
                
'               "other" outline levels option
'                If xScheme = "" Then _
'                    xScheme = "Other"
            End If
                           
            'get xml tag/CC adjusted paragraph start and end
            If g_bXMLSupport Then
                lStart = mdlWordXML.GetTagSafeParagraphStart(rngPara, iTagsAtStart)
                lEnd = mdlWordXML.GetTagSafeParagraphEnd(rngPara, iTagsAtEnd)
                If (g_iWordVersion = mpWordVersion_2007) And (iTagsAtStart + iTagsAtEnd = 0) Then
                    '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                    lStart = mdlCC.GetCCSafeParagraphStart(rngPara)
                    lEnd = mdlCC.GetCCSafeParagraphEnd(rngPara)
                End If
            Else
                lStart = .Start
                lEnd = .End
            End If
    
'           if type is Sentence,
            If iStyleType = mpTOCEntryType_Sentence Or _
                    iStyleType = mpTOCEntryType_PeriodSpace Then
'               add EndHeading tag after first sentence -
'               follow this immediately with the name
'               of the applied scheme
                bIsQuote = False
                Set rngSentence = rngPara.Duplicate
                With rngSentence
                    .SetRange lStart, lEnd
                    If .Characters(1).Text = Chr(12) Then _
                        .MoveStart
                    .InsertBefore mpStartPara
                End With
                
'               look for Chinese full stop
                lPos = InStr(.Text, ChrW(&H3002))
                
                If lPos Then
'                   there's a Chinese full stop
                    With rngSentence
                        .StartOf
                        .MoveEnd wdCharacter, lPos
                        If g_bXMLSupport Then
                            'account for empty xml tags (12/17/09)
                            '9.9.4009 - account for all tags ending in range
                            iCount = mdlWordXML.CountTagsEndingInRange(rngSentence)
                            If iCount > 0 Then
                                .MoveEnd wdCharacter, iCount
                            ElseIf g_iWordVersion = mpWordVersion_2007 Then
                                .MoveEnd wdCharacter, _
                                    mdlCC.CountCCsEndingInRange(rngSentence)
                            End If
                        End If
                    End With
                ElseIf iStyleType = mpTOCEntryType_Sentence Then
'                   period and 2 spaces
                    lPos = InStr(.Text, mpSentencePeriodTwo)
                    
'                   try with end quote
                    lPos2 = InStr(.Text, mpSentenceQuoteTwo)
                    If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                        lPos = lPos2
                        bIsQuote = True
                    End If

'                   try with smart quote
                    lPos2 = InStr(.Text, g_xSmartTwo)
                    If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                        lPos = lPos2
                        bIsQuote = True
                    End If
                                        
                    If lPos Then
                        With rngSentence
                            .StartOf
                            .MoveEnd wdCharacter, lPos
                            If g_bXMLSupport Then
                                'account for empty xml tags (12/17/09)
                                '9.9.4009 - account for all tags ending in range
                                iCount = mdlWordXML.CountTagsEndingInRange(rngSentence)
                                If iCount > 0 Then
                                    .MoveEnd wdCharacter, iCount
                                ElseIf g_iWordVersion = mpWordVersion_2007 Then
                                    .MoveEnd wdCharacter, _
                                        mdlCC.CountCCsEndingInRange(rngSentence)
                                End If
                            End If
                        End With
                    End If
                Else
'                   period and 1 space
                    lPos = InStr(.Text, mpSentencePeriodOne)
                    
'                   try with end quote
                    lPos2 = InStr(.Text, mpSentenceQuoteOne)
                    If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                        lPos = lPos2
                        bIsQuote = True
                    End If

'                   try with smart quote
                    lPos2 = InStr(.Text, g_xSmartOne)
                    If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                        lPos = lPos2
                        bIsQuote = True
                    End If
                    
                    While lPos
                        With rngSentence
                            .StartOf
                            .MoveEnd wdCharacter, lPos
                            If g_bXMLSupport Then
                                'account for empty xml tags (12/17/09)
                                '9.9.4009 - account for all tags ending in range
                                iCount = mdlWordXML.CountTagsEndingInRange(rngSentence)
                                If iCount > 0 Then
                                    .MoveEnd wdCharacter, iCount
                                ElseIf g_iWordVersion = mpWordVersion_2007 Then
                                    .MoveEnd wdCharacter, _
                                        mdlCC.CountCCsEndingInRange(rngSentence)
                                End If
                            End If
                            xTest = .Text
                        End With
                            
'                       skip common abbreviations
                        If bIsAbbreviation(xTest) Then
                            bIsQuote = False
                            rngSentence.Expand wdParagraph
                            lPos2 = InStr(lPos + 1, .Text, mpSentencePeriodOne)
                            
'                           try with end quote
                            lPos3 = InStr(lPos + 1, .Text, mpSentenceQuoteOne)
                            If (lPos3 <> 0) And _
                                    ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                lPos2 = lPos3
                                bIsQuote = True
                            End If
                                    
'                           try with smart quote
                            lPos3 = InStr(lPos + 1, .Text, g_xSmartOne)
                            If (lPos3 <> 0) And _
                                    ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                lPos2 = lPos3
                                bIsQuote = True
                            End If
                            
                            lPos = lPos2
                        Else
                            lPos = 0
                        End If
                    Wend
                End If
                
                With rngSentence
                    If Right(.Text, 1) = " " Then
'                       trim trailing spaces off range
                        .MoveEndWhile " ", -10000
                    ElseIf Right(.Text, 1) = vbCr Then
'                       trim trailing para off range
                        .MoveEnd wdCharacter, -1
                    End If
                    
                    If Right(.Text, 1) = "." Or _
                            Right(.Text, 1) = ChrW(&H3002) Then
                        If bIsQuote Then
'                           include quote
                            .MoveEnd wdCharacter, 1
                        Else
'                           trim period or full stop
                            .MoveEnd wdCharacter, -1
                        End If
                    End If
                    
                    .InsertAfter mpEndHeading & _
                                mpSchemeNameStart & _
                                xScheme & _
                                mpSchemeNameEnd
                End With
                
'               add end para tag before para mark
                Set rngP = rngPara.Duplicate
                With rngP
                    If g_bXMLSupport Then
                        lEnd = mdlWordXML.GetTagSafeParagraphEnd(rngP, iTagsAtEnd)
                        '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                        If (g_iWordVersion = mpWordVersion_2007) And (iTagsAtEnd = 0) Then _
                            lEnd = mdlCC.GetCCSafeParagraphEnd(rngP)
                        .SetRange rngP.Start, lEnd
                    Else
                        If Right(.Text, 1) = Chr(7) Then
'                           para is in table
                            .EndOf wdCell
                        Else
                            .EndOf
                        End If
                        If (.Previous(wdCharacter).Text = vbCr) Or _
                                (.Previous(wdCharacter).Text = Chr(12)) Then
                            .Move wdCharacter, -1
                        End If
                    End If
                    .InsertAfter mpEndPara
                End With
            Else
'               add EndHeading tag before para mark -
'               follow this immediately with the
'               name of the applied list template
                Set rngP = rngPara.Duplicate
                rngP.SetRange lStart, lEnd
                rngP.InsertBefore mpStartPara
                rngP.EndOf
                If (rngP.Previous(wdCharacter).Text = vbCr) Or _
                        (rngP.Previous(wdCharacter).Text = Chr(12)) Then
                    rngP.Move wdCharacter, -1
                End If
                
                If Right(.Text, 1) = "." Then
'                   trim period
                    .MoveEnd wdCharacter, -1
                End If
                    
                rngP.InsertAfter mpEndHeading & _
                    mpSchemeNameStart & _
                    xScheme & _
                    mpSchemeNameEnd & _
                    mpEndPara
            End If
        End If
    End With
End Sub

Public Sub ReplaceText(ByVal rngP As Word.Range, _
                       ByVal xSearch As String, _
                       Optional ByVal xReplace As String = "", _
                       Optional ByVal bMatchWildcards As Boolean = False, _
                       Optional ByVal iWrap As Integer = wdFindStop)
'replaces xSearch with xReplace
    With rngP.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = xSearch
        .MatchWildcards = bMatchWildcards
        .Wrap = iWrap
        .Replacement.Text = xReplace
        .Execute Replace:=wdReplaceAll
        .Replacement.Text = ""
    End With
    
End Sub

Public Sub HideUnhideText(ByVal rngP As Word.Range, _
                          ByVal xSearch As String, _
                          ByVal bHide As Boolean, _
                          Optional ByVal bMatchWildcards As Boolean = False, _
                          Optional ByVal iWrap As Integer = wdFindStop)
'replaces xSearch with xReplace

    'in Word 2007, a toggle is necessary to hide/unhide
    If g_iWordVersion = mpWordVersion_2007 Then
        With rngP.Find
            .ClearFormatting
            .Text = xSearch
            .MatchWildcards = bMatchWildcards
            .Format = True
            .Wrap = iWrap
            .Replacement.Font.Hidden = Not bHide
            .Execute Replace:=wdReplaceAll
        End With
    End If
    
    With rngP.Find
        .ClearFormatting
        .Text = xSearch
        .MatchWildcards = bMatchWildcards
        .Format = True
        .Wrap = iWrap
        .Replacement.Font.Hidden = bHide
        .Execute Replace:=wdReplaceAll
    End With
    
End Sub

Private Sub CleanUpDoc()
'   remove all tags in doc and
'   reset tc codes if specified
    Dim bShowAll As Boolean
    Dim bShowHidden As Boolean
    Dim rngContent As Word.Range
    Dim xPrompt As String
    
    On Error GoTo ProcError
    
    If g_lUILanguage = wdFrenchCanadian Then
        xPrompt = "Nettoyage du document"
    Else
        xPrompt = "Cleaning up document"
    End If
    Application.StatusBar = xPrompt
    g_oStatus.Show 67, g_xTOCStatusMsg
    
    EchoOff
    
    Set rngContent = ActiveDocument.Content
    
    ReplaceText rngContent, _
                mpSchemeNameStart & "*" & mpSchemeNameEnd, _
                , True
    Application.StatusBar = xPrompt & ": 33%"
    g_oStatus.Show 77, g_xTOCStatusMsg
    ReplaceText rngContent, _
                "|MP??|", , True
    Application.StatusBar = xPrompt & ": 67%"
    g_oStatus.Show 87, g_xTOCStatusMsg
    ReplaceText rngContent, _
                "z11H^011", Chr(11), True
ProcError:
    Application.StatusBar = ""
    EchoOn
    Exit Sub
End Sub

Private Sub HideTags(Optional rngP As Word.Range, _
                    Optional bShowStatus As Boolean = False)
'set all tags in doc to hidden text-
'this prevents any pagination issues when
'creating the TOC
    Dim xPrompt As String
    
    On Error GoTo ProcError
    EchoOff
    
    If rngP Is Nothing Then
        Set rngP = ActiveDocument.Content
    End If
    
    If g_lUILanguage = wdFrenchCanadian Then
        xPrompt = "Activer Rechercher et Remplacer..."
    Else
        xPrompt = "Running search and replace..."
    End If
    
    If bShowStatus Then
        Application.StatusBar = xPrompt
        g_oStatus.Show 9, g_xTOCStatusMsg
    End If
    HideUnhideText rngP, _
                   mpSchemeNameStart & "*" & mpSchemeNameEnd, _
                   True, True
    If bShowStatus Then
        Application.StatusBar = xPrompt
        g_oStatus.Show 25, g_xTOCStatusMsg
    End If
    HideUnhideText rngP, "|MP??|", True, True
ProcError:
    EchoOn
    Exit Sub
End Sub

Private Sub CleanUpTOC(ByVal rngTOC As Word.Range)
    Dim xWild As String
    Dim xPrompt As String
    
    On Error GoTo ProcError
    
    If g_lUILanguage = wdFrenchCanadian Then
        xPrompt = "Nettoyage de TM"
    Else
        xPrompt = "Cleaning up TOC"
    End If
    Application.StatusBar = xPrompt
    g_oStatus.Show 60, g_xTOCStatusMsg
    EchoOff
    
'   localize wildcard used below -
'   modified in 9.9.3004 - we were previously inferring the list
'   separator from the decimal separator
    xWild = " {1" & Application.International(wdListSeparator) & "}"
    
'   remove stray spaces after tabs that
'   result from chr(11)s in doc
    ReplaceText rngTOC, _
        xWild & mpStartPara, vbTab, True
    Application.StatusBar = xPrompt & ": 10%"
    g_oStatus.Show 60, g_xTOCStatusMsg
    ReplaceText rngTOC, _
        "^009" & xWild, vbTab, True
    Application.StatusBar = xPrompt & ": 20%"
    g_oStatus.Show 60, g_xTOCStatusMsg
    ReplaceText rngTOC, _
        mpEndHeading & "*" & mpEndPara, , True
    Application.StatusBar = xPrompt & ": 30%"
    g_oStatus.Show 62, g_xTOCStatusMsg
        
'   remove any stray spaces after text in TOC Entry
    ReplaceText rngTOC, _
        xWild & "^009", vbTab, True
    Application.StatusBar = xPrompt & ": 40%"
    g_oStatus.Show 62, g_xTOCStatusMsg
            
'   convert zzmp11Holders to tabs
    ReplaceText rngTOC, " z11H ", vbTab
    Application.StatusBar = xPrompt & ": 50%"
    g_oStatus.Show 64, g_xTOCStatusMsg
    ReplaceText rngTOC, "z11H ", vbTab
    Application.StatusBar = xPrompt & ": 60%"
    g_oStatus.Show 64, g_xTOCStatusMsg

'   remove chr(160) - inserted by
'   underline to longest line function
    ReplaceText rngTOC, "^32" & ChrW(&HA0)
    Application.StatusBar = xPrompt & ": 70%"
    g_oStatus.Show 66, g_xTOCStatusMsg
        
'   remove any possible multiple tabs
    ReplaceText rngTOC, "^009{2,}", vbTab, True
    ReplaceText rngTOC, "zzmpMultiTabs"
    Application.StatusBar = xPrompt & ": 80%"
    g_oStatus.Show 66, g_xTOCStatusMsg
        
'   unhide quote placeholders before removing
    HideUnhideText rngTOC, "zzmp34h", False
    HideUnhideText rngTOC, "zzmp147h", False
    HideUnhideText rngTOC, "zzmp148h", False
    
'   remove quote placeholders
    ReplaceText rngTOC, "zzmp34h", Chr(34)
    ReplaceText rngTOC, "zzmp147h", ChrW(&H201C)
    ReplaceText rngTOC, "zzmp148h", ChrW(&H201D)
    Application.StatusBar = xPrompt & ": 90%"
    g_oStatus.Show 70, g_xTOCStatusMsg
        
'   force chr 183 to be symbol
    With rngTOC.Find
        .ClearFormatting
        .Text = g_xBullet
        .MatchWildcards = False
        .Replacement.Text = g_xBullet
        .Replacement.Font.Name = "Symbol"
        .Wrap = wdFindStop
        .Execute Replace:=wdReplaceAll
    End With
    
ProcError:
    Application.StatusBar = ""
    EchoOn
    Exit Sub
End Sub

Private Sub AddDummyFirstPara(rngTOC As Word.Range)
'adds a first para in toc so that we
'can delete the 'real' first para if
'necessary (can't delete 1st para in TOC field
'-  this para is deleted later
'in bInsertTOC routine

    Dim rngTOCStart As Word.Range
    Dim rngText As Word.Range
    Dim xPara1 As String
    Dim xStyle As String
    Dim xChar1 As String
    
    Set rngTOCStart = rngTOC.Paragraphs(1).Range
    With rngTOCStart
        xChar1 = .Characters.First
        .StartOf
        .Move wdCharacter
        .InsertAfter xChar1
        .InsertBefore vbCr
    End With
End Sub

Private Sub DeleteDummyFirstPara(rngTOC As Word.Range)
'deletes the first para of TOC -
'this should be a copy of the actual
'first para - see AddDummyFirstPara, above

    rngTOC.Paragraphs(1).Range.Delete
End Sub

Private Function xGetStyleList(ByVal xInclusions As String, _
                               ByVal iMinLevel As Integer, _
                               ByVal iMaxLevel As Integer, _
                               ByVal bApplyTOC9 As Boolean, _
                               ByVal xTOC9Style As String) As String
    Dim xTemp As String
    Dim iPos As Integer
    Dim xStyleList As String
    Dim xScheme As String
    Dim i As Integer
    Dim xStyleRoot As String
    Dim xHeading As String
    Dim jPos As Integer
    Dim iLevel As Integer
    
    xTemp = xInclusions
    
    If Len(xTemp) Then
'       trim trailing and preceding commas
        xTemp = xTrimTrailingChrs(xTemp, ",", True, False)
        iPos = InStr(xTemp, ",")
        While iPos
'           add Heading 1-9 styles
            xScheme = Left(xTemp, iPos - 1)
            If xScheme = "Heading" Or bIsHeadingScheme(xScheme) Then
                For i = iMinLevel To iMaxLevel
'                   strip alias from style name
                    xHeading = StripStyleAlias(xTranslateHeadingStyle(i))
                    xStyleList = xStyleList & xHeading & _
                                "," & i & ","
                Next i
            ElseIf xScheme = "Schedule" Then
                For i = 0 To UBound(g_xScheduleStyles)
                    If bApplyTOC9 And (g_xScheduleStyles(i) = xTOC9Style) Then
'                       ensure inclusion regardless of range
                        iLevel = iMinLevel
                    ElseIf Val(g_xScheduleLevels(i, 1)) = wdOutlineLevelBodyText Then
'                       we're treating non-outline level schedule style as level 1
                        iLevel = wdOutlineLevel1
                    Else
'                       use actual level
                        iLevel = Val(g_xScheduleLevels(i, 1))
                    End If
                    
'                   include if in range
                    If (iLevel >= iMinLevel) And (iLevel <= iMaxLevel) Then
                        xStyleList = xStyleList & g_xScheduleStyles(i) & _
                            "," & iLevel & ","
                    End If
                Next i
            Else
                xStyleRoot = xGetStyleRoot(xScheme)
                For i = iMinLevel To iMaxLevel
                    xStyleList = xStyleList & xStyleRoot & _
                                "_L" & i & "," & i & ","
                Next i
            End If
            xTemp = Mid(xTemp, iPos + 1)
            iPos = InStr(xTemp, ",")
        Wend
    End If
    
    xGetStyleList = xStyleList

End Function

Function bReworkTOC(rngTOC As Word.Range, _
                    xScheme As String, _
                    xExclusions As String, _
                    bIncludeStyles As Boolean, _
                    bIncludeTCEntries As Boolean, _
                    udtTOCFormat As TOCFormat, _
                    xTOC9Style As String, _
                    bDesignate As Boolean, _
                    bApplyManualFormats As Boolean) As Boolean
'cycles through TOC paras, cleaning
'up in any number of ways
    Const mpErrorMargin As Integer = 5
    
    Dim i As Integer
    Dim rngP As Word.Range
    Dim iLevel As Integer
    Dim bIsCentered As Boolean
    Dim paraTOC As Word.Paragraph
    Dim xTabFlag(8) As String
    Dim iNumTabs As Integer
    Dim paraP As Word.Paragraph
    Dim l As Long
    Dim lNumParas As Long
    Dim pfPrev As Word.ParagraphFormat
    Dim pfCur As Word.ParagraphFormat
    Dim xPara As String
    Dim bIsMarked As Boolean
    Dim bExclude As Boolean
    Dim bIsMarkedDel As Boolean
    Dim xPageNo As String
    Dim sProgress As Single
    Dim sTab As Single
    Dim xNextPara As String
    Dim iPos As Integer
    Dim xParaNum As String
    Dim xHeading As String
    Dim rngHeading As Word.Range
    Dim bIsSchedule As Boolean
    Dim bIsField As Boolean
    Dim bIsFirstPara As Boolean
    Dim xStyle As String
    Dim xStylePrev As String
    Dim iDisplay As Integer
    Dim bIs2007 As Boolean
    Dim sHPos As Single
    
    bIs2007 = (g_iWordVersion = mpWordVersion_2007)
    lNumParas = rngTOC.Paragraphs.Count
    bIsField = (rngTOC.Fields.Count > 0)
    
'   substitute tabs for soft returns
    ReplaceText rngTOC, String(2, 11), vbTab
    ReplaceText rngTOC, Chr(11), vbTab

'   native TOC automatically 1) inserts space when there's
'   no trailing character and 2) removes trailing space in
'   number format (e.g. with our old method of doing two spaces);
'   substitute tab, leaving mpStartPara for use in Word 97
'   split para test below
    ReplaceText rngTOC, " " & mpStartPara, mpStartPara & vbTab
    
'   hide tags in TOC - this will permit
'   accurate measurements for tabs below
    HideTags rngTOC
    
'   hide quote placeholders
    HideUnhideText rngTOC, "zzmp34h", True
    HideUnhideText rngTOC, "zzmp147h", True
    HideUnhideText rngTOC, "zzmp148h", True
    
'   turn off hidden text, allowing ShowAll to control
    If bIs2007 Then _
        ActiveWindow.View.ShowHiddenText = False
    
'---cycle through TOC paragraphs
    For Each paraTOC In rngTOC.Paragraphs
'OutputDebugString "bReworkTOC - Start new paragraph"
        Set rngP = paraTOC.Range
        
'       first paragraph of field result will be deleted at end
'       of this function - it's just a dummy entry necessitated
'       by inability to freely edit first paragraph of TOC field
        bIsFirstPara = (rngP = rngTOC.Paragraphs.First.Range)
'OutputDebugString "bReworkTOC - After first para comparison"
        If bIsField And bIsFirstPara Then
            GoTo NextPara
        End If
        
        With rngP
            Set pfCur = .ParagraphFormat
'OutputDebugString "bReworkTOC - After set pfCur"
            xPara = .Text
'OutputDebugString xPara
'           skip empty paras
            If Len(xPara) < 2 Then
                If paraTOC.Range = rngTOC.Paragraphs.Last.Range Then
                    Exit For
                Else
                    GoTo NextPara
                End If
            End If
            
'           this will avoid a problematic second trip through a split
'           Word 97 para set to exclude page number
            If Mid(xPara, Len(xPara) - 1, 1) = "|" Then
                GoTo NextPara
            End If
            
'           this fixes a native Word 97 bug that splits
'           paragraphs that are broken by a page break into
'           two entries; first part of split para will have
'           only start marker
            If (InStr(Word.Application.Version, "8.") <> 0) Then
                If (InStr(xPara, mpStartPara) <> 0) And _
                        (InStr(xPara, mpEndPara) = 0) Then
'                   do only if next entry has only end marker
                    On Error Resume Next
                    xNextPara = .Next(wdParagraph).Text
                    On Error GoTo 0
                    If (InStr(xNextPara, mpStartPara) = 0) And _
                            (InStr(xNextPara, mpEndPara) <> 0) Then
'                       get paragraph number
                        iPos = InStr(xPara, mpStartPara)
                        xParaNum = Left(xPara, iPos - 1)
                    
'                       get correct page number
                        .EndOf
                        .MoveStartUntil vbTab, wdBackward
                        xPageNo = Left(.Text, Len(.Text) - 1)
                    
'                       check for second appearance of paragraph number
                        .EndOf
                        .MoveEnd wdCharacter, iPos - 1
                        If .Text = xParaNum Then
'                           combine entries
                            .MoveEnd wdCharacter, 1
                            .MoveStartUntil vbTab, wdBackward
                            .MoveStart wdCharacter, -1
                            .Text = Chr(32)
                    
'                           fix page number
                            .Expand wdParagraph
                            .EndOf
                            .Move wdCharacter, -1
                            .MoveStartUntil vbTab, wdBackward
                            .Text = xPageNo
                            .Expand wdParagraph
                        Else
'                           return to previous paragraph
                            .StartOf
                            .MoveStart wdCharacter, -1
                            .Expand wdParagraph
                        End If
                    End If
                End If
            End If
            
'           delete entry under certain conditions -
'           this depends on the type of entry that it is,
'           and what's been requested by the user for the TOC
            xPara = .Text
            If xTOC9Style <> "" Then
                bIsSchedule = (InStr(UCase(xPara), _
                    mpSchemeNameStart & UCase(xTOC9Style) & mpSchemeNameEnd) <> 0)
            End If
            bIsMarked = (InStr(UCase(xPara), UCase(mpTCMarked)) > 0)
            bIsMarkedDel = (InStr(UCase(xPara), UCase(mpMarkedDel)) > 0)
            
            If bIsSchedule And bIsMarked Then
'               next entry should be a duplicate
                With ActiveWindow.View
                    .ShowAll = False
                    If Not bIs2007 Then _
                        .ShowHiddenText = False
                    With rngP
                        If UCase(.Text) = UCase(.Next(wdParagraph).Text) Then
                            .Next(wdParagraph).Style = "TOC 9"
                            .Text = ""
                            GoTo NextPara
                        End If
                    End With
                    .ShowAll = True
                    If Not bIs2007 Then _
                        .ShowHiddenText = True
                End With
            End If
            
            If bIsMarkedDel Then
                .Text = ""
                GoTo NextPara
            ElseIf bIncludeStyles And bIncludeTCEntries Then
'               delete if base scheme is in exclusion list
                If ExcludeTOCEntry(xPara, xExclusions, xTOC9Style, bDesignate) Then
                    .Text = ""
                    GoTo NextPara
                End If
            ElseIf bIncludeStyles Then
'               delete if base scheme is in exclusion list
                If ExcludeTOCEntry(xPara, xExclusions, xTOC9Style, bDesignate) Then
                    .Text = ""
                    GoTo NextPara
                End If
            ElseIf bIncludeTCEntries Then
'               delete if begins with page break
                If InStr(.Text, vbTab & mpPageBreakMarker) Then
                    .Text = ""
                    GoTo NextPara
                End If
            Else
                .Text = ""
                GoTo NextPara
            End If
'OutputDebugString "bReworkTOC - After inclusion/exclusion"
'           apply TOC 9 to Schedule entry
            If bIsSchedule Then
                .Style = "TOC 9"
            End If
            
'           get style
            xStyle = .Style
            
'           remove extra tabs - do if more than two or if it's
'           a non-numbered entry - added last conditional on 11/30/01;
'           removed last conditional on 7/1/03 to fix log item #3038 -
'           shift-returns in non-numbered entries are now stripped from TC
'           code, so these no longer come into TOC as tabs that need to be removed
            iNumTabs = lCountChrs(xPara, vbTab)
            If (iNumTabs > 2) Or _
                    (Left(xPara, Len(mpStartPara)) = mpStartPara) Then
'                    Or (InStr(xPara, "|MP") = 0) Then
                RemoveExtraTabs rngP
            End If
'OutputDebugString "bReworkTOC - After extra tabs"
'           get level
            iLevel = iGetTOCStyleLevel(xStyle)
'OutputDebugString "bReworkTOC - After get level"
'           adjust hanging indent of style based on
'           first representative entry for this level
'           determine if it's a numbered entry and
'           whether level has already been checked;
'           hide tags for accurate measurement
            With ActiveWindow.View
                .ShowAll = False
                If Not bIs2007 Then _
                    .ShowHiddenText = False
            End With
            xPara = .Text
            iNumTabs = lCountChrs(xPara, vbTab)
            If iNumTabs > 1 Then
                If g_bUseNewTabAdjustmentRules Then
                    'new option checks every entry for tightness and adjusts only as
                    'much as needed, rather than taking first entry as representative
                    'and leaving a cushion for potentially longer entries
                    Set rngHeading = rngP.Duplicate
                    sHPos = MeasureHeading(rngHeading, pfCur, xPara)
                    
                    'adjust flag if longest entry found for level
                    If xTabFlag(iLevel - 1) = "" Then
                        xTabFlag(iLevel - 1) = CStr(sHPos)
                    ElseIf sHPos > CSng(xTabFlag(iLevel - 1)) Then
                        xTabFlag(iLevel - 1) = CStr(sHPos)
                    End If
                ElseIf xTabFlag(iLevel - 1) = "" Then
                    'set flag
                    xTabFlag(iLevel - 1) = "X"
                    AdjustStyleIndents rngP, pfCur, iLevel, xPara
                End If
            ElseIf iNumTabs = 1 Then
                If bIs2007 Then
                    'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                    'repeated selections from producing a visual "creeping" effect, and
                    'Range.Information has not been problematic since Word 2000
                    Dim oRange As Word.Range
                    Set oRange = .Characters.First
                    With oRange
                        .EndOf wdParagraph
                        .Move wdCharacter, -1
                        'style is used, since tabstops method of selection
                        'will fail when document's default tab stop is 0"
                        With ActiveDocument.Styles(xStyle).ParagraphFormat.TabStops
                            If .Count > 0 Then _
                                sTab = .Item(.Count).Position
                        End With
                        If .Information(wdHorizontalPositionRelativeToTextBoundary) < _
                                sTab - InchesToPoints(0.25) Then
                            'measurement is not accurate to the point, so allow 1/4"
                            'leeway - this won't be a problem because hanging indent
                            'or preceding tab stop will never be set this close to right tab
                            .MoveUntil vbTab, wdBackward
                            'include placemarker to prevent extra tab
                            'from being deleted in CleanUpTOC
                            .InsertAfter "zzmpMultiTabs" & vbTab
                        End If
                    End With
                Else
                    .Characters.First.Select
                    With Selection
                        .EndOf wdParagraph
                        .Move wdCharacter, -1
                        'style is used, since tabstops method of selection
                        'will fail when document's default tab stop is 0"
                        With ActiveDocument.Styles(xStyle).ParagraphFormat.TabStops
                            If .Count > 0 Then _
                                sTab = .Item(.Count).Position
                        End With
                        If .Information(wdHorizontalPositionRelativeToTextBoundary) < _
                                sTab - InchesToPoints(0.25) Then
                            'measurement is not accurate to the point, so allow 1/4"
                            'leeway - this won't be a problem because hanging indent
                            'or preceding tab stop will never be set this close to right tab
                            .MoveUntil vbTab, wdBackward
                            'include placemarker to prevent extra tab
                            'from being deleted in CleanUpTOC
                            .InsertAfter "zzmpMultiTabs" & vbTab
                        End If
                    End With
                End If
            End If
'OutputDebugString "bReworkTOC - After tab set"
'           show all again
            With ActiveWindow.View
                .ShowAll = True
                If Not bIs2007 Then _
                    .ShowHiddenText = True
            End With
            
'           delete leading quote if it's the only quote in entry
            Set rngHeading = rngP.Duplicate
            With rngHeading
                .Expand wdParagraph
                iPos = InStr(.Text, mpEndHeading)
                If iPos Then
'                   MacPac numbered entry - get portion of entry that will be retained
                    xHeading = Left(.Text, iPos - 1)
                    If lCountChrs(xHeading, Chr(34)) + _
                            lCountChrs(xHeading, ChrW(&H201C)) + _
                            lCountChrs(xHeading, ChrW(&H201D)) = 1 Then
'                       there's only one double quote in the entry
                        iPos = InStr(.Text, mpStartPara)
                        If iPos Then
                            .MoveStart wdCharacter, iPos + Len(mpStartPara) - 1
'                           if the lone double quote is at the start of the heading, delete it
                            With .Characters(1)
                                If (.Text = Chr(34)) Or (.Text = ChrW(&H201C)) Then
                                    .Delete
                                End If
                            End With
                        End If
                    End If
                Else
'                   non-numbered entry
                    xHeading = .Text
                    If lCountChrs(xHeading, "zzmp34h") + _
                            lCountChrs(xHeading, "zzmp147h") + _
                            lCountChrs(xHeading, "zzmp148h") = 1 Then
'                       if the lone double quote is at the start of the heading, delete it
                        .StartOf
                        If Left(xHeading, 7) = "zzmp34h" Then
                            .MoveEnd wdCharacter, 7
                            .Delete
                        ElseIf Left(xHeading, 8) = "zzmp147h" Then
                            .MoveEnd wdCharacter, 8
                            .Delete
                        End If
                    End If
                End If
            End With
'OutputDebugString "bReworkTOC - After quotes"
'           get alignment
            bIsCentered = pfCur.Alignment

'           format centered paras
            If bIsCentered Then
                FormatCenteredLevel rngP, pfCur
            End If
            
'           format para to have or not have page number
            FormatPageNumber rngP, _
                             bIsCentered, _
                             pfCur, _
                             udtTOCFormat.IncludePageNumber(iLevel)
'OutputDebugString "bReworkTOC - After page number"
'           if first para of new level,
'           adjust space after previous paragraph -
'           pfPrev is set below, so it will be
'           nothing the first time around
            If Not (pfPrev Is Nothing) Then
                If (xStyle <> xStylePrev) Then
                    If (udtTOCFormat.SpaceBefore(iLevel) <> "") And _
                            (udtTOCFormat.SpaceBefore(iLevel) <> "mpNA") Then
                        pfPrev.SpaceAfter = udtTOCFormat.SpaceBefore(iLevel)
                    End If
                End If
            End If
'OutputDebugString "bReworkTOC - After space adjustment"
'           call any custom code
            lRet = oCustTOC.lInTOCRework(rngP, _
                                         xScheme, _
                                         xExclusions, _
                                         bIncludeStyles, _
                                         bIncludeTCEntries)
            If lRet Then
                Err.Raise lRet, _
                    "oCustTOC.lInTOCRework", _
                    Err.Description
            End If
        End With
NextPara:
'       free up resources
        If Not g_bPreserveUndoListTOC Then _
            ActiveDocument.UndoClear

'       store current para format for
'       next iteration through loop
        xStylePrev = xStyle
        Set pfPrev = pfCur
        Set rngP = Nothing
        Set paraTOC = Nothing
        l = l + 1
        If ((l / lNumParas) * 100) > (iDisplay + 10) Then
            iDisplay = iDisplay + 10
            If g_lUILanguage = wdFrenchCanadian Then
                Application.StatusBar = "Refaire TM: " & iDisplay & "%"
            Else
                Application.StatusBar = "Reworking TOC: " & iDisplay & "%"
            End If
            sProgress = 48 + (12 * (l / lNumParas))
            g_oStatus.Show sProgress, g_xTOCStatusMsg
        End If
'OutputDebugString "bReworkTOC - Paragraph l=" & l
    Next paraTOC
    
    'adjust indents as necessary
    If g_bUseNewTabAdjustmentRules Then
        For i = 0 To 8
            If xTabFlag(i) <> "" And xTabFlag(i) <> "0" Then
                AdjustStyleIndentsNew i + 1, CSng(xTabFlag(i)), True
            End If
        Next i
    End If
    
'   first paragraph of TOC field can't be deleted in normal manner
    If bIsField Then
        If rngTOC.Paragraphs.Count = 2 Then
'           delete field - there were no bona fide TOC entries
            rngTOC.Fields(1).Delete
        Else
            Set rngP = rngTOC.Paragraphs(1).Range
            With rngP
                .MoveEnd wdCharacter, -1
                .MoveStart
                .Select
                With Selection
                    WordBasic.EditClear
                    .Move
''                   preserve style before deleting paragraph mark
'                    xStyle = .Style
'                   1st character and para mark should be all that's left;
'                   delete them now
                    WordBasic.DeleteBackWord
'                   reapply style
                    .Style = xStyle
'                   first character doesn't always get deleted
                    On Error Resume Next
                    If .Previous(wdCharacter, 1).Style = xStyle Then _
                        WordBasic.DeleteBackWord
                    On Error GoTo 0
                End With
            End With
        End If
    End If
    
    If bApplyManualFormats Then
'       remove underlining of any underlined tabs
        With rngTOC.Find
            .ClearFormatting
            .Format = True
            .Font.Underline = wdUnderlineSingle
            .Text = vbTab
            With .Replacement.Font
                .Underline = wdUnderlineNone
                .Hidden = False
            End With
            .Execute Replace:=wdReplaceAll
        End With
    End If
    
'   ensure no space before first entry
    rngTOC.Paragraphs(1).SpaceBefore = 0
    
'   turn hidden text back on - this will ensure that the pipes get cleaned up
    If bIs2007 Then _
        ActiveWindow.View.ShowHiddenText = True
End Function

Function udtGetTOCFormat(xScheme As String, _
                         bIncludeSchedule As Boolean) As TOCFormat
    Dim udtFormat As TOCFormat
    Dim xSpaceBefore As String
    Dim xSpaceBetween As String
    Dim xLineSpacing As String
    Dim xInclPgNum As Integer
    Dim xAllCaps As Integer
    Dim xDotLeaders As Integer
    Dim i As Integer
    Dim xBold As String
    Dim xValue As String
    Dim iPos As Integer
    
    With udtFormat
'       get points before values for all levels
        If xScheme = "Custom" Then
'           get user defaults or, if missing
'           from user ini, set to skip
            On Error Resume Next
            xSpaceBefore = xAppGetUserINIValue("TOC", "PointsBefore")
            xInclPgNum = xAppGetUserINIValue("TOC", "PageNumbers")
            xDotLeaders = xAppGetUserINIValue("TOC", "DotLeaders")
            xAllCaps = xAppGetUserINIValue("TOC", "AllCaps")
            xBold = xAppGetUserINIValue("TOC", "Bold")
            .CenterLevel1 = xAppGetUserINIValue("TOC", "CenterLevel1")
            xLineSpacing = xAppGetUserINIValue("TOC", "LineSpacing")
            xSpaceBetween = xAppGetUserINIValue("TOC", "PointsBetween")
            
            On Error GoTo 0
            
            If xLineSpacing = "" Then
                .LineSpacing = mpCGSSkipItem
            Else
                .LineSpacing = xLineSpacing
            End If
            
            If xSpaceBetween = "" Then
                .SpaceBetween = mpCGSSkipItem
            Else
                .SpaceBetween = xLocalizeNumericString(xSpaceBetween)
            End If
            
            If xSpaceBefore = "" Then
                xSpaceBefore = "6"
            Else
                xSpaceBefore = xLocalizeNumericString(xSpaceBefore)
            End If
            
            For i = 1 To 9
                .SpaceBefore(i) = xSpaceBefore
    
'               get whether to include page numbers
                Select Case xInclPgNum
                    Case mpTOCLevels_All
                        .IncludePageNumber(i) = True
                    Case mpTOCLevels_1
                        .IncludePageNumber(i) = (i = 1)
                    Case mpTOCLevels_Subsequent
                        .IncludePageNumber(i) = (i > 1)
                    Case mpTOCLevels_None
                        .IncludePageNumber(i) = False
                    Case Else
                        .IncludePageNumber(i) = True
                End Select
            
'               get whether to include all caps;
'               subsequent option was removed from list
                If xAllCaps = mpTOCLevels_Subsequent Then _
                    xAllCaps = mpTOCLevels_None
                Select Case xAllCaps
                    Case mpTOCLevels_All
                        .AllCaps(i) = True
                    Case mpTOCLevels_1
                        .AllCaps(i) = (i = 1)
                    Case mpTOCLevels_Subsequent
                        .AllCaps(i) = (i > 1)
                    Case mpTOCLevels_None
                        .AllCaps(i) = False
                    Case Else
                        .AllCaps(i) = True
                End Select
                
'               get whether to include bold
                Select Case xBold
                    Case Trim(Str(mpTOCLevels_All))
                        .Bold(i) = True
                    Case Trim(Str(mpTOCLevels_1))
                        .Bold(i) = (i = 1)
                    Case Trim(Str(mpTOCLevels_Subsequent))
'                       subsequent is not an option for bold
                        .Bold(i) = False
                    Case Else
'                       custom scheme hasn't been edited since Bold
'                       property added - default to false
                        .Bold(i) = False
                End Select
            
'               get whether to include dot leaders
                Select Case xDotLeaders
                    Case mpTOCLevels_All
                        .IncludeDotLeaders(i) = True
                    Case mpTOCLevels_1
                        .IncludeDotLeaders(i) = (i = 1)
                    Case mpTOCLevels_Subsequent
                        .IncludeDotLeaders(i) = (i > 1)
                    Case mpTOCLevels_None
                        .IncludeDotLeaders(i) = False
                    Case Else
                        .IncludeDotLeaders(i) = True
                End Select
            Next i
        Else
            For i = 1 To 9
'               get from Numbering Scheme TOC properties
                .SpaceBefore(i) = xGetLevelProp(xScheme, _
                                        i, _
                                        mpTOCLevelProp_SpaceBeforeLevel, _
                                        mpSchemeType_TOC)
                                            
                .IncludePageNumber(i) = xGetLevelProp(xScheme, _
                                            i, _
                                            mpTOCLevelProp_IncludePgNum, _
                                            mpSchemeType_TOC)
                                            
'               skip all these formats - when using a predefined scheme
'               these values are taken directly from the style definitions
                .IncludeDotLeaders(i) = mpCGSSkipItem
                .AllCaps(i) = mpCGSSkipItem
            Next i
            .LineSpacing = mpCGSSkipItem
            .SpaceBetween = mpCGSSkipItem
        End If
        
'       get Schedule props
        If bIncludeSchedule Then
            On Error Resume Next
            xValue = Application.Templates(xTOCSTY) _
                .CustomDocumentProperties("SpecialTOC9")
            On Error GoTo 0
            If xValue <> "" Then
                iPos = InStr(2, xValue, "|")
                .SpaceBefore(9) = Mid(xValue, 2, iPos - 2)
                .IncludePageNumber(9) = Mid(xValue, Len(xValue) - 1, 1)
                .IncludeDotLeaders(9) = mpCGSSkipItem
                .AllCaps(9) = mpCGSSkipItem
            End If
        End If
        
    End With
    udtGetTOCFormat = udtFormat
End Function

Private Function FormatPageNumber(rngP As Word.Range, _
                                  bIsCentered As Boolean, _
                                  pfCur As Word.ParagraphFormat, _
                                  bInclude As Boolean, _
                                  Optional ByVal bIsField As Boolean)
    Dim sHPos As Single
    
    With rngP
        If bInclude Then
'           if centered with page no, replace last tab with 2 spaces
            If bIsCentered Then
                '9/28/12 - do only if paragraph contains a tab
                If InStr(rngP.Paragraphs(1).Range, vbTab) > 0 Then
                    .EndOf wdParagraph
                    .MoveUntil vbTab, wdBackward
                    .MoveStart wdCharacter, -1
                    .MoveStartWhile Chr(11), wdBackward
                    .Text = Chr(11)
                End If
            ElseIf Not bIsField Then
'               if any EOL's are not at the right tab position,
'               then add a tab using tab set as determination
'                .EndOf wdParagraph
                With .Characters.Last
                    If g_iWordVersion = mpWordVersion_2007 Then
                        'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                        'repeated selections from producing a visual "creeping" effect, and
                        'Range.Information has not been problematic since Word 2000
                        sHPos = .Information(wdHorizontalPositionRelativeToPage)
                    Else
                        .Select
                        sHPos = Selection.Information(wdHorizontalPositionRelativeToPage)
                    End If
                    If (Int(sHPos) + 1 < _
                        Int(pfCur.TabStops(1).Position)) And _
                        (sHPos > -1) Then
                            .MoveEndUntil vbTab, wdBackward
                            .InsertAfter vbTab
                    End If
                End With
            End If
        ElseIf Not bIsField Then
'           remove page #, tab, and any extraneous shift-returns
'           7/6/12 (dm) - field option uses switch to accomplish this
            .EndOf wdParagraph
            .Move wdCharacter, -1
            .MoveStartUntil vbTab, wdBackward
            .MoveStart wdCharacter, -1
            .MoveStartWhile Chr(11), wdBackward
            .Delete
        End If
        
    End With
End Function

Function ExcludeTOCEntry(ByVal xPara As String, _
                         ByVal xExclusions As String, _
                         ByVal xTOC9Style As String, _
                         ByVal bDesignate As Boolean) As Boolean
'   returns TRUE if scheme name is in
'   exclusion list or heading is empty
    Dim lPos1 As Long
    Dim lPos2 As Long
    Dim xBaseScheme As String
    
'   marked for deletion or scheme name is empty
    If InStr(xPara, "zzmpDel") Or _
            InStr(xPara, mpSchemeNameStart & mpSchemeNameEnd) Then
        ExcludeTOCEntry = True
        Exit Function
    End If
    
'   return TRUE if the heading is empty - in
'   this case the '|MPEH|' tag will start the para
    If Left(xPara, 6) = "|MPEH|" Then
        ExcludeTOCEntry = True
        Exit Function
    End If
    
'   return TRUE if page break marker immediately follows tab
    If InStr(xPara, vbTab & mpPageBreakMarker) Then
        ExcludeTOCEntry = True
        Exit Function
    End If
    
'   parse out scheme name - held between
'   'name start' and 'name end' tags
    lPos1 = InStr(UCase(xPara), UCase(mpSchemeNameStart))
    If lPos1 Then
'       get base scheme from codes
        lPos2 = InStr(lPos1 + Len(lPos1) + 1, _
                      UCase(xPara), _
                      UCase(mpSchemeNameEnd))
        xBaseScheme = Mid(xPara, lPos1 + Len(mpSchemeNameStart), _
                        lPos2 - lPos1 - Len(mpSchemeNameStart))
    Else
        xBaseScheme = ""
    End If
            
'   TOC9 Style is subset of "Schedule"
    If Not bDesignate And _
            (UCase(xBaseScheme) = UCase(xTOC9Style)) And _
            (InStr(UCase(xExclusions), ",SCHEDULE,") <> 0) Then
        ExcludeTOCEntry = True
        Exit Function
    End If
    
    ExcludeTOCEntry = InStr(UCase(xExclusions), _
            "," & UCase(xBaseScheme) & ",")
End Function

Function RemoveExtraTabs(rngP As Word.Range)
    With rngP
        If (Left(.Text, Len(mpStartPara)) = mpStartPara) Or _
                (InStr(.Text, "|MP") = 0) Then
'           non-numbered paragraph - move before last tab
            .MoveEndUntil vbTab, wdBackward
            .MoveEnd wdCharacter, -1

'           replace all extra tabs with a space
            While lCountChrs(.Text, vbTab) > 0
                .MoveStartUntil vbTab
                .Characters(1) = " "
            Wend
        Else
'           move after first tab pos
            .MoveStartUntil vbTab
            .MoveStart wdCharacter

'           replace all extra tabs with a space
            While lCountChrs(.Text, vbTab) > 1
                .MoveStartUntil vbTab
                .Characters(1) = " "
            Wend
        End If

'       reset range to whole para again
        .Expand wdParagraph
    End With
End Function

Function AdjustStyleIndents(rngP As Word.Range, _
                            pfCur As Word.ParagraphFormat, _
                            ByVal iLevel As Integer, _
                            ByVal xPara As String)
    Const mpErrorMargin As Integer = 5
    Dim sHPos As Single
    Dim xStart As String
    Dim lPos As Long
    Dim sLeftIndent As Single
    Dim sChange As Single
    Dim bShowAll As Boolean
    Dim bShowHidden As Boolean
    
    If pfCur.Alignment <> wdAlignParagraphCenter Then
        With rngP
            sLeftIndent = pfCur.LeftIndent
'           get start position of text after tab;
'           changed to before tab by DM on 12/14/99
            lPos = InStr(xPara, vbTab)
            .StartOf
            .Move wdCharacter, lPos - 1
            
            If g_iWordVersion = mpWordVersion_2007 Then
                'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                'repeated selections from producing a visual "creeping" effect, and
                'Range.Information has not been problematic since Word 2000
                sHPos = .Information(wdHorizontalPositionRelativeToTextBoundary)
            Else
                .Select
                sHPos = Selection.Information(wdHorizontalPositionRelativeToTextBoundary)
            End If
            
'           if past left indent, needs adjusting
            If sHPos > pfCur.LeftIndent Then
'                Selection.Move wdCharacter, -1
'               locate the .25" sector of document that contains end of number;
'               tab s/b .5" from start of that sector
                If g_iWordVersion = mpWordVersion_2007 Then
                    sHPos = .Information(wdHorizontalPositionRelativeToTextBoundary)
                Else
                    sHPos = Selection.Information(wdHorizontalPositionRelativeToTextBoundary)
                End If
                xStart = LTrim(Partition(sHPos, 0, 612, 18))
                xStart = Left(xStart, InStr(xStart, ":") - 1)
'               redefine style
                'GLOG 5615 - ensure that not referencing character style
                With ActiveDocument.Styles(.Paragraphs(1).Style).ParagraphFormat
                    .LeftIndent = CSng(xStart) + 36
                    sChange = pfCur.LeftIndent - sLeftIndent
                    .FirstLineIndent = .FirstLineIndent - sChange
                End With
            ElseIf sHPos = pfCur.LeftIndent And lCountChrs(Left(xPara, lPos - 1), ".") > 2 Then
                ' Make sure there's some extra space for multi-level numbers
'                Selection.Move wdCharacter, -1
                If g_iWordVersion = mpWordVersion_2007 Then
                    sHPos = .Information(wdHorizontalPositionRelativeToTextBoundary)
                Else
                    sHPos = Selection.Information(wdHorizontalPositionRelativeToTextBoundary)
                End If
                If pfCur.LeftIndent - sHPos < 10 Then
                    xStart = LTrim(Partition(sHPos, 0, 612, 18))
                    xStart = Left(xStart, InStr(xStart, ":") - 1)
    '               redefine style
                    'GLOG 5615 - ensure that not referencing character style
                    With ActiveDocument.Styles(.Paragraphs(1).Style).ParagraphFormat
                        .LeftIndent = CSng(xStart) + 36
                        sChange = pfCur.LeftIndent - sLeftIndent
                        .FirstLineIndent = .FirstLineIndent - sChange
                    End With
                End If
            End If
            .Expand wdParagraph
        End With
    End If
    
End Function

Function AdjustStyleIndentsNew(ByVal iLevel As Integer, ByVal sHeadingPos As Single, _
        ByVal bKeepNextLevelAligned As Boolean)
    Const mpErrorMargin As Integer = 5
    Dim sChange As Single
    Dim oStyle As Word.Style
    Dim bUseNextLevel As Boolean
    Dim sStartIndent As Single
    Dim sMinIndent As Single
    Dim i As Integer
    Dim oStyleNext As Word.Style
    
    On Error Resume Next
    Set oStyle = ActiveDocument.Styles(xTranslateTOCStyle(iLevel))
    On Error GoTo ProcError
        
    If Not oStyle Is Nothing Then
        sStartIndent = oStyle.ParagraphFormat.LeftIndent
        sMinIndent = sHeadingPos + 3.6
            
        'if past left indent, needs adjusting
        If sStartIndent < sMinIndent Then
            'use next level if it satisfies minimum indent and is not more
            'than 1/4" greater than in it
            If iLevel < 9 Then
                On Error Resume Next
                Set oStyleNext = ActiveDocument.Styles(xTranslateTOCStyle(iLevel + 1))
                On Error GoTo ProcError
                If Not oStyleNext Is Nothing Then
                    bUseNextLevel = ((oStyleNext.ParagraphFormat.LeftIndent > sMinIndent) And _
                        (oStyleNext.ParagraphFormat.LeftIndent < sMinIndent + 18))
                End If
            End If
            
            'adjust
            With oStyle.ParagraphFormat
                If bUseNextLevel Then
                    'redefine style to align with next level
                    .LeftIndent = oStyleNext.ParagraphFormat.LeftIndent
                Else
                    'expand by 0.1 until minimum is reached
                    While .LeftIndent < sMinIndent
                        .LeftIndent = .LeftIndent + 7.2
                    Wend
                End If
                sChange = .LeftIndent - sStartIndent
                .FirstLineIndent = .FirstLineIndent - sChange
                
                'GLOG 5146 (2/26/13) - decided to no longer adjust subsequent levels at all -
                'the rules were arbitary and we're guessing at user intent
'                If (Not bUseNextLevel) And bKeepNextLevelAligned And (sChange <= 36) Then
'                    'if this level previously aligned with subsequent levels,
'                    'adjust them as well
'                    'GLOG 5146 (2/21/13) - limited to change of 0.5" or less and
'                    'removed unwarranted inferences about the first line indent
'                    For i = iLevel + 1 To 9
'                        Set oStyleNext = ActiveDocument.Styles(xTranslateTOCStyle(i))
'                        On Error GoTo ProcError
'                        If Not oStyleNext Is Nothing Then
'                            With oStyleNext.ParagraphFormat
'                                If (.LeftIndent + .FirstLineIndent = sStartIndent) Or _
'                                        (.LeftIndent = sStartIndent) Then
'                                    'keep headings aligned
'                                    sStartIndent = .LeftIndent
'                                    .LeftIndent = sStartIndent + sChange
'                                Else
'                                    Exit For
'                                End If
'                            End With
'                        End If
'                    Next i
'                End If
            End With
        End If
    End If
    
    Exit Function
ProcError:
    RaiseError "MPTOC90.mpTOC.AdjustStyleIndentsNew"
    Exit Function
End Function

Function MeasureHeading(ByVal rngP As Word.Range, ByVal pfCur As Word.ParagraphFormat, _
        ByVal xPara As String) As Single
    Dim sHPos As Single
    Dim lPos As Long
    
    On Error GoTo ProcError
    
    If pfCur.Alignment <> wdAlignParagraphCenter Then
        With rngP
'           get start position of text after tab;
'           changed to before tab by DM on 12/14/99
            lPos = InStr(xPara, vbTab)
            .StartOf
            .Move wdCharacter, lPos - 1
            
            If g_iWordVersion = mpWordVersion_2007 Then
                'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                'repeated selections from producing a visual "creeping" effect, and
                'Range.Information has not been problematic since Word 2000
                sHPos = .Information(wdHorizontalPositionRelativeToTextBoundary)
            Else
                .Select
                sHPos = Selection.Information(wdHorizontalPositionRelativeToTextBoundary)
            End If
        End With
    End If
    
    MeasureHeading = sHPos
    
    Exit Function
ProcError:
    RaiseError "MPTOC90.mpTOC.MeasureHeading"
    Exit Function
End Function

Function FormatCenteredLevel(rngP As Word.Range, pfCur As Word.ParagraphFormat, _
        Optional ByVal bIsField As Boolean)
    Dim xTrailChr As String
    Dim iNumTabs As Integer
    Dim bDo As Boolean
    
    iNumTabs = lCountChrs(rngP.Text, vbTab)
    If bIsField Then
        bDo = (iNumTabs > 0)
    Else
        bDo = (iNumTabs > 1)
    End If
    If bDo Then
        With rngP
'           if centered, replace remaining tab with shift-return(s)
'           base # of shift-returns on line spacing
'            With pfCur
'                If .LineSpacingRule = wdLineSpaceDouble Or _
'                        .LineSpacing > 14 Then
                    xTrailChr = Chr(11)
'                Else
'                    xTrailChr = String(2, 11)
'                End If
'            End With

'           replace first tab with trailing chr
            .StartOf wdParagraph
            .MoveUntil vbTab
            .MoveEnd wdCharacter
            If .Next(wdCharacter).Text = Chr(32) Then _
                .MoveEnd wdCharacter
            .Text = xTrailChr
        End With
    End If
End Function

Function IncrementTOCTabs(Optional ByVal bDecrement As Boolean = False) As Long
'increases/decreases hanging indent of style of
'first para in selection
    Dim rngSel As Word.Range
    Dim iLevel As Integer
    Dim xStyle As String
    Dim sIncrement As Single
    
    On Error GoTo ProcError
    
'   set appropriate increment for Word unit
    Select Case Word.Options.MeasurementUnit
        Case wdInches
            sIncrement = 3.6
        Case wdCentimeters, wdMillimeters
            sIncrement = 1.4
        Case Else
            sIncrement = 3
    End Select
    
'   get style to modify
    xStyle = Selection.Range.Paragraphs(1).Style
    
'   do only for TOC styles
    If bIsTOCStyle(xStyle) Then
        If bDecrement Then
            bSetTOCTabs -sIncrement, Right(xStyle, 1)
        Else
            bSetTOCTabs sIncrement, Right(xStyle, 1)
        End If
    End If
    Exit Function
ProcError:
    IncrementTOCTabs = Err.Number
    Exit Function
End Function

Function FillTCCodes(Optional bFillFormatted As Boolean = False, _
                     Optional bScreenUpdating As Boolean = True, _
                     Optional bReplaceShiftReturns As Boolean = True) As Long
'fills tc codes in doc with formatted
'text to beginning of para- adds the
'appropriate codes for restoring them
'in the future
'9/27/12 (dm) - added bReplaceShiftReturns argument
    Dim fldP As Word.Field
    Dim rngCode As Word.Range
    Dim bIsTC As Boolean
    Dim bIsTCStatic As Boolean
    Dim bIsTCPDyn As Boolean
    Dim bIsTCDyn As Boolean
    Dim rngHeading As Word.Range
    Dim iLevel As Integer
    Dim lCodes As Long
    Dim l As Long
    Dim iView As WdViewType
    Dim xText As String
    
    On Error GoTo ProcError
    
    Application.ScreenUpdating = False
    iView = Word.ActiveWindow.View.Type
    Word.ActiveWindow.View.Type = wdNormalView
    
'   cycle through tc codes
    lCodes = Word.ActiveDocument.Fields.Count
    For Each fldP In Word.ActiveDocument.Fields
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = _
                "Classement Codes TM: " & _
                    Format(l / lCodes, "0%")
        Else
            Application.StatusBar = _
                "Filling TC Codes: " & _
                    Format(l / lCodes, "0%")
        End If
        l = l + 1
        bIsTCDyn = Empty
        bIsTCPDyn = Empty
        bIsTCStatic = Empty
        iLevel = Empty
        
        Set rngCode = fldP.Code
        bIsTC = UCase(Left(rngCode, 4)) = " " & g_xTCPrefix & " "
        If bIsTC Then
'           get type of tc code
            bIsTCDyn = (UCase(rngCode) = " " & g_xTCPrefix & " ")
            If Not bIsTCDyn Then
                bIsTCStatic = _
                    UCase(Left(rngCode, 5) = _
                        " " & g_xTCPrefix & " " & Chr(34))
            End If
            If Not (bIsTCStatic Or bIsTCDyn) Then
                bIsTCPDyn = True
            End If
        
'           if dynamic code...
            If Not bIsTCStatic Then
'               get level
                If bIsTCDyn Then
                    iLevel = iGetDynLevel(rngCode)
                Else
'                   code is partial dynamic - get level from code
                    iLevel = iGetPDynLevel(rngCode)
                End If
                
'               bInsertTCEntryText, which is used below for formatted
'               codes, adds level switch; strip from partially dynamic code (7/1/03)
                If bIsTCPDyn And bFillFormatted Then _
                    rngCode.Text = " " & g_xTCPrefix & " "
            
'               get heading
                Set rngHeading = rngCode.Duplicate
            
                With rngHeading
'                   collapse range to start
                    .StartOf
                    
'                   if there's no heading, don't attempt to fill code
                    If .Start = .Paragraphs(1).Range.Start Then _
                        GoTo labNextfield
                
'                   extend to start of para
                    .StartOf wdParagraph, wdExtend
                    .MoveStartWhile Chr(12)
                    
'                   this will exclude list num field code
'                    .TextRetrievalMode.IncludeFieldCodes = False
                End With
            
'               insert field code text
                If bFillFormatted Then
                    bRet = bInsertTCEntryText(rngHeading, _
                                              rngCode, _
                                              iLevel, _
                                              vbTab)
                Else
                    rngCode.Text = " " & g_xTCPrefix & " " & xGetTCEntryText _
                                                (rngHeading, _
                                                iLevel, _
                                                vbTab)
                End If
            
'               function above inserts as ref field
                fldP.Unlink
                
                If bIsTCDyn Then
'                   remove shift-returns and spaces after tabs
                    If bReplaceShiftReturns Or (iLevel <> 1) Then
                        'replace shift-returns with single tab
                        xText = fldP.Code.Text
                        xText = xSubstitute(xText, vbTab & String(2, 11), vbTab)
                        xText = xSubstitute(xText, vbTab & vbVerticalTab, vbTab)
                    Else
                        'replace shift-returns with single shift-return
                        xText = fldP.Code.Text
                        xText = xSubstitute(xText, vbTab & String(2, 11), vbVerticalTab)
                        xText = xSubstitute(xText, vbTab & vbVerticalTab, vbVerticalTab)
                    End If
                    xText = xSubstitute(xText, vbTab & String(2, 32), vbTab)
                    xText = xSubstitute(xText, vbTab & Chr(32), vbTab)
                    fldP.Code.Text = xText
                    
'                   tag as dynamic
                    fldP.Code.InsertAfter " " & mpTCCode_Dyn
                Else
'                   replace shift-returns with a space;
'                   this is no longer handled in bReworkTOC (7/1/03)
                    If bReplaceShiftReturns Or (iLevel <> 1) Then
                        fldP.Code.Text = xSubstitute(fldP.Code.Text, String(2, 11), Chr(32))
                        fldP.Code.Text = xSubstitute(fldP.Code.Text, Chr(11), Chr(32))
                    Else
                        'replace shift-returns with single shift-return
                        fldP.Code.Text = xSubstitute(fldP.Code.Text, String(2, 11), Chr(11))
                    End If
                    
'                   tag as partially dynamic
                    fldP.Code.InsertAfter " " & mpTCCode_PDyn
                End If
            End If
        End If
labNextfield:
    Next fldP
    
'   restore original view
    Word.ActiveWindow.View.Type = iView
    If bScreenUpdating Then _
        Application.ScreenUpdating = True
    Application.StatusBar = ""
    Exit Function
ProcError:
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    FillTCCodes = Err.Number
    RaiseError "mpTOC.FillTCCodes"
End Function

Function RestoreMPTCCodes() As Long
'fills tc codes in doc with formatted
'text to beginning of para- adds the
'appropriate codes for restoring them
'in the future
    Const mpTCPrefix  As String = " TC "
    
    Dim fldP As Word.Field
    Dim rngCode As Word.Range
    Dim bIsTC As Boolean
    Dim bIsTCStatic As Boolean
    Dim bIsTCPDyn As Boolean
    Dim bIsTCDyn As Boolean
    Dim rngHeading As Word.Range
    Dim iLevel As Integer
    Dim lCodes As Long
    Dim l As Long
    
    On Error GoTo ProcError
    Application.ScreenUpdating = False
    
'   cycle through tc codes
    lCodes = Word.ActiveDocument.Fields.Count
    For Each fldP In Word.ActiveDocument.Fields
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = _
                "R�initialisation Codes TM: " & _
                    Format(l / lCodes, "0%")
        Else
            Application.StatusBar = _
                "Restoring TC Codes: " & _
                    Format(l / lCodes, "0%")
        End If
        l = l + 1
        bIsTCDyn = Empty
        bIsTCPDyn = Empty
        bIsTCStatic = Empty
        iLevel = Empty
        
        Set rngCode = fldP.Code
        bIsTC = UCase(Left(rngCode, 4)) = " " & g_xTCPrefix & " "
        If bIsTC Then
'           get type of tc code
            bIsTCDyn = (InStr(UCase(fldP.Code), _
                UCase(mpTCCode_Dyn)) > 0)
            If Not bIsTCDyn Then
                bIsTCPDyn = (InStr(UCase(fldP.Code), _
                    UCase(mpTCCode_PDyn)) > 0)
            End If
        End If
        
        If bIsTCPDyn Then
'           partial dynamic code
            iLevel = iGetPDynLevel(rngCode)
'           insert code with level only
            fldP.Code.Text = " " & g_xTCPrefix & " \l """ & iLevel & Chr(34)
        ElseIf bIsTCDyn Then
'           insert dynamic (empty) tc code
            fldP.Code.Text = " " & g_xTCPrefix & " "
        End If
    Next fldP
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    Exit Function
ProcError:
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    RestoreMPTCCodes = Err.Number
    RaiseError "mpTOC.RestoreMPTCCodes"
End Function

Function iGetPDynLevel(rngCode As Word.Range) As Integer
'returns level of a partially dynamic code -
'prompts if none can be found
    Dim lPos As Long
    Dim iLevel As Integer
    
'   search for level switch
    'GLOG 5453 - convert to lower case
    lPos = InStr(LCase(rngCode), "\l """)
    If lPos Then
'       level integer should be 5 chars
'       to right of start of switch
        On Error Resume Next
        iLevel = Mid(rngCode, lPos + 4, 1)
        On Error GoTo 0
        If (iLevel < 1) Or (iLevel > 9) Then
            While (iLevel < 1) Or (iLevel > 9)
                iLevel = iGetParagraphLevel(rngCode, 0)
            Wend
        End If
    Else
        While (iLevel < 1) Or (iLevel > 9)
            iLevel = iGetParagraphLevel(rngCode, 0)
        Wend
    End If
    iGetPDynLevel = iLevel
End Function

Function iGetDynLevel(rngCode As Word.Range) As Integer
    Dim iLevel As Integer
    
'   dynamic levels should have an
'   outline level- prompt if none
    iLevel = rngCode.ParagraphFormat.OutlineLevel
    While (iLevel < 1) Or (iLevel > 9)
        iLevel = iGetParagraphLevel(rngCode, 0)
    Wend
    iGetDynLevel = iLevel
End Function

Function ConvertHiddenParas() As Long
    Dim paraP As Word.Paragraph
    Dim fldP As Word.Field
    Dim rngParaMark As Word.Range
    Dim lNumParas As Long
    Dim l As Long
    Dim styHidden As Word.Style
    Dim styVisible As Word.Style
    Dim rngHeading As Word.Range
    Dim bShowHidden As Boolean
    Dim bBold As Boolean
    Dim bCaps As Boolean
    Dim bSmallCaps As Boolean
    Dim bItalic As Boolean
    Dim bUnderline As Boolean
    Dim xStyleMarker As String
    Dim bStyIsConverted As Boolean
    
    On Error GoTo ProcError
    Application.ScreenUpdating = False

'   turn on hidden text
    With ActiveWindow.View
        bShowHidden = .ShowHiddenText
        .ShowHiddenText = True
    End With
    
    lNumParas = Word.ActiveDocument.Paragraphs.Count
    
'   set default toc scheme
    Word.ActiveDocument.Variables("cbxTOCScheme") = 2
    
'   cycle through paras
    For Each paraP In Word.ActiveDocument.Paragraphs
        Set rngParaMark = paraP.Range.Characters.Last
        With rngParaMark
            If .Font.Hidden Then
'               store styles and font attributes
                Set styHidden = paraP.Style
                If InStr(xStyleMarker, "*" & paraP.Style & "*") Then
                    bStyIsConverted = True
                Else
                    bStyIsConverted = False
                    xStyleMarker = xStyleMarker & "*" & paraP.Style & "*"
                End If
                
                Set styVisible = rngParaMark.Next(wdParagraph).Style
                
                With styHidden
                    With .Font
                        bBold = .Bold
                        bCaps = .AllCaps
                        bSmallCaps = .SmallCaps
                        bItalic = .Italic
                        bUnderline = (.Underline = wdUnderlineSingle)
                    End With
                    .Font = styVisible.Font
                End With
                
'               delete para
'                .Delete
                .Text = ""
                .Style = styHidden
                
'               insert tc code to the left of any trailing spaces
                .MoveWhile " ", wdBackward
                
'               insert tc code
                Set fldP = Word.ActiveDocument.Fields.Add( _
                    rngParaMark, wdFieldTOCEntry)
                fldP.Code.Text = " " & g_xTCPrefix & " "
                
'               ensure that tc code is unformatted
                rngGetField(fldP.Code).Font.Reset
                
'               apply direct attributes to heading
                .MoveStart wdParagraph, -1
                With .Font
                    .Bold = bBold
                    .AllCaps = bCaps
                    .SmallCaps = bSmallCaps
                    .Italic = bItalic
                    .Underline = Abs(bUnderline)
                End With
                
                If .ListFormat.ListType <> wdListNoNumbering Then
                    With .ListFormat
                        With .ListTemplate.ListLevels(.ListLevelNumber).Font
                            .Bold = bBold
                            .AllCaps = bCaps
                            .SmallCaps = bSmallCaps
                            .Italic = bItalic
                            .Underline = Abs(bUnderline)
                        End With
                    End With
                End If
            End If
        End With
        l = l + 1
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = _
                "Les marques de paragraphes ont �t� masqu�es et remplac�es par les codes TM MacPac:  " & _
                Format(l / lNumParas, "0%")
        Else
            Application.StatusBar = _
                "Converting hidden paragraph marks to MacPac TC codes:  " & _
                Format(l / lNumParas, "0%")
        End If
    Next paraP
    
    ActiveWindow.View.ShowHiddenText = bShowHidden
    
    With Word.Application
        .ScreenUpdating = True
        .StatusBar = ""
    End With
    
    If g_lUILanguage = wdFrenchCanadian Then
        MsgBox "Les marques de paragraphes ont �t� masqu�es et remplac�es par les codes TM MacPac. " & _
              vbCr & vbCr & "Nous recommandons de supprimer toutes Tables des mati�res avant d'ins�rer une TM MacPac. Si vous cr�ez une TM MacPac sans supprimer une TM existante, MacPac remplacera la TM. Il n'y aura pas de nouvelle section TM avec MacPac en-t�te et pied de page.", _
              vbInformation, "Num�rotation MacPac"
    Else
        MsgBox "Hidden paragraph marks have been replaced with MacPac TC codes. " & _
              vbCr & vbCr & "We recommend that you delete any " & _
              "existing Tables Of Contents before " & _
              vbCr & "inserting a MacPac TOC.  Should " & _
              "you choose to create a MacPac TOC " & _
              vbCr & "without first deleting an existing TOC, " & _
              "MacPac will simply replace the TOC." & _
              vbCr & "It will not create a new TOC section with " & _
              "MacPac headers and footers.", vbInformation, AppName
    End If
    Exit Function
    
ProcError:
    ConvertHiddenParas = Err.Number
    Exit Function
End Function

Function bResetTOCTabs(rngTOC As Word.Range)
    Dim paraTOC As Word.Paragraph
    Dim pfPrev As Word.ParagraphFormat
    Dim pfCur As Word.ParagraphFormat
    Dim rngP As Word.Range
    Dim xPara As String
    Dim xTabFlag(8) As String
    Dim iNumTabs As Integer
    Dim iLevel As Integer
    Dim bShowAll As Boolean
    Dim bShowHidden As Boolean
    Dim iView As Integer
    Dim i As Integer
    Dim s As Single
    Dim sHPos As Single
    Dim xStyle As String
    
    EchoOff
    With ActiveWindow.ActivePane.View
        bShowAll = .ShowAll
        bShowHidden = .ShowHiddenText
        iView = .Type
        .ShowAll = False
        .ShowHiddenText = False
        .Type = wdNormalView
    End With
    
    s = 36
    On Error Resume Next
    For i = 1 To 9
        With ActiveDocument.Styles(xTranslateTOCStyle(i))
            .ParagraphFormat.LeftIndent = s
            .ParagraphFormat.FirstLineIndent = -36
        End With
        s = s + 36
    Next i
    On Error GoTo EndOfTOC
    For Each paraTOC In rngTOC.Paragraphs
        Set rngP = paraTOC.Range
        With rngP
            Set pfCur = .ParagraphFormat

            xPara = .Text

'           skip empty paras
            If Len(xPara) < 2 Then
                If paraTOC.Range = rngTOC.Paragraphs.Last.Range Then
                    Exit For
                Else
                    GoTo NextPara
                End If
            End If
            
'           get level
            On Error GoTo EndOfTOC
            xStyle = pfCur.Style
            iLevel = iGetTOCStyleLevel(xStyle)
            On Error GoTo 0
            
            iNumTabs = lCountChrs(xPara, vbTab)
'           adjust hanging indent of style based on
'           first representative entry for this level
'           determine if it's a numbered entry and
'           whether level has already been checked
            If iNumTabs > 1 Then
                If g_bUseNewTabAdjustmentRules Then
                    'new option checks every entry for tightness and adjusts only as
                    'much as needed, rather than taking first entry as representative
                    'and leaving a cushion for potentially longer entries
                    sHPos = MeasureHeading(rngP, pfCur, xPara)
                    
                    'adjust flag if longest entry found for level
                    If xTabFlag(iLevel - 1) = "" Then
                        xTabFlag(iLevel - 1) = CStr(sHPos)
                    ElseIf sHPos > CSng(xTabFlag(iLevel - 1)) Then
                        xTabFlag(iLevel - 1) = CStr(sHPos)
                    End If
                ElseIf xTabFlag(iLevel - 1) = "" Then
                    'set flag
                    xTabFlag(iLevel - 1) = "X"
                    AdjustStyleIndents rngP, pfCur, iLevel, xPara
                End If
            End If

        End With
NextPara:
'       store current para format for
'       next iteration through loop
        Set pfPrev = pfCur
        Set rngP = Nothing
        Set paraTOC = Nothing
    Next paraTOC

EndOfTOC:
    'adjust indents as necessary
    If g_bUseNewTabAdjustmentRules Then
        For i = 0 To 8
            If xTabFlag(i) <> "" And xTabFlag(i) <> "0" Then
                AdjustStyleIndentsNew i + 1, CSng(xTabFlag(i)), False
            End If
        Next i
    End If
    
'   reset environment
    With ActiveWindow.ActivePane.View
        .ShowAll = bShowAll
        .ShowHiddenText = bShowHidden
        .Type = iView
    End With
    EchoOn
    
End Function

Private Function TagAsTCEntry_NonDyn(rngPara As Word.Range, _
                                    lPos As Long)
    Dim rngP As Word.Range
    Dim lStart As Long
    Dim iTags As Integer
    
    With rngPara
        'get Word XML adjusted paragraph start
        If g_bXMLSupport Then
            lStart = mdlWordXML.GetTagSafeParagraphStart(rngPara, iTags)
            '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
            If (g_iWordVersion = mpWordVersion_2007) And (iTags = 0) Then _
                lStart = mdlCC.GetCCSafeParagraphStart(rngPara)
            .SetRange lStart, .End
        End If
    
'       add 'tc marked' tag directly before the tc code
        Set rngP = .Duplicate
        With rngP
            .StartOf
            .Move wdCharacter, lPos - 1
            .InsertAfter mpTCMarked
        End With
   End With
End Function

Private Function xConditionalScheme(rngPara As Word.Range) As String
'returns name of MP scheme of which this is a bracketed variant
    Dim xStyle As String
    Dim iPos As Integer
    Dim xLT As String

    With rngPara.Paragraphs(1).Range
        With .ListFormat
            If .ListType <> wdListOutlineNumbering And _
                    .ListType <> wdListSimpleNumbering Then
                Exit Function
            End If
            
            If Left(.ListString, 1) <> "[" Then
                Exit Function
            End If
        End With
        
        'GLOG 2847
        On Error Resume Next
        xStyle = .Style
        On Error GoTo 0
        
        If Left(xStyle, 9) = "Heading_L" Then
            xLT = "HeadingStyles"
        ElseIf bIsHeadingStyle(xStyle) Then
            xLT = xHeadingScheme()
        Else
            iPos = InStr(xStyle, "_L")
            If iPos Then _
                xLT = "zzmp" & Left(xStyle, iPos - 1)
        End If
        
        If bListTemplateExists(xLT) Then
            If xLT = "HeadingStyles" Then
                xConditionalScheme = "Heading"
            Else
                xConditionalScheme = xLT
            End If
        End If
    End With
End Function

Function bDeleteMPTOC() As Boolean
    Dim rngTOC As Word.Range
    
    With ActiveDocument
        If .Bookmarks.Exists("mpTableOfContents") Then
            If g_lUILanguage = wdFrenchCanadian Then
                lRet = MsgBox("�tes-vous s�re de vouloir supprimer la table des mati�res ?", vbQuestion + vbYesNo, "Num�rotation MacPac")
            Else
                lRet = MsgBox("Are you sure you want to delete the existing " & _
                    "Table of Contents?", vbQuestion + vbYesNo, "MacPac Numbering")
            End If
            If lRet = vbNo Then _
                Exit Function
        
            Set rngTOC = .Bookmarks("mpTableOfContents").Range
            If Asc(rngTOC.Characters.Last) = 12 Then
                rngTOC.MoveEnd wdCharacter, -1
            End If
            If rngTOC.Start = .Range.Start Then
                While (rngTOC.Paragraphs.Count > 1) And _
                        (Not bIsTOCStyle(rngTOC.Paragraphs(1).Style))
                    rngTOC.MoveStart wdParagraph, 1
                    If rngTOC.End > .Bookmarks("mpTableOfContents").Range.End Then
                        Set rngTOC = .Bookmarks("mpTableOfContents").Range
                        If Asc(rngTOC.Characters.Last) = 12 Then
                            rngTOC.MoveEnd wdCharacter, -1
                        End If
                        rngTOC.Collapse wdCollapseEnd
                    End If
                Wend
                If Asc(rngTOC.Characters.First) = 12 Then
                    rngTOC.MoveStart wdCharacter, 1
                End If
            End If
            
            If Len(rngTOC) > 1 Then
                With rngTOC
                    On Error Resume Next
'                    If .Next(wdParagraph).Text = _
'                            mpEndOfTOCWarning & vbCr Then
'                        .MoveEnd wdParagraph
'                    End If
                    
'                   attempting to delete a native TOC field when
'                   when there's preceding text in the range
'                   can cause an error - unlink as a precaution
                    .Fields.Unlink
                    On Error GoTo 0
                    
'                   leave final paragraph mark
                    If .Characters.Last = vbCr Then _
                        .MoveEnd wdCharacter, -1
                    
'                   delete TOC
                    .Delete
                    
'                   format paragraph as normal
                    If .Style <> ActiveDocument.Styles(wdStyleNormal) Then _
                        .Style = wdStyleNormal
                End With
            End If
            
            bDeleteMPTOC = True
        Else
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Aucune table des mati�res MacPac dans ce document ou signet TM manquant.", vbInformation, _
                    "Num�rotation MacPac"
            Else
                MsgBox "There is no MacPac Table of Contents in this " & _
                    "document or the TOC bookmark is missing.", vbInformation, _
                    "MacPac Numbering"
            End If
        End If
    End With
End Function

Function rngHeaderBookmark(rngHeader As Word.Range, _
                            xPrefix As String) As Word.Range
'returns "Table of Contents" or "Page" range for formatting
    Dim bkmk As Word.Bookmark
    Dim xName As String
    
'   at Murtaugh only, after saving in ProLaw, the main header bookmark
'   can be part of the collection, but not accessible, even in the
'   Word interface.  This causes bkmk.name below to generate error 5825,
'   "object has been deleted".  Strangely, this issue doesn't appear to cause
'   problems elsewhere in MacPac -
'   9.9.3004 - replaced this workaround with error handling below - I found
'   that when rngHeader is the primary header story, the main header bookmark
'   (e.g. zzmpTOCHeader_Primary) is seen by Word as in the first page header story -
'   this resulted in rngHeader.Bookmarks.Count = 1 and the primary header not getting
'   formatted correctly
'    If rngHeader.Bookmarks.Count < 2 Then Exit Function
    
    For Each bkmk In rngHeader.Bookmarks
        On Error Resume Next
        xName = bkmk.Name
        On Error GoTo 0
        If InStr(xName, xPrefix) Then
            Set rngHeaderBookmark = bkmk.Range
            bkmk.Delete
            Exit Function
        End If
    Next bkmk
End Function

Function bGetCommonAbbrevs(xlistarray() As String) As Boolean
    Dim xDatFile As String
    Dim TextLine As String
    
    On Error GoTo Title_ERROR
        
    ReDim xlistarray(0)
    xDatFile = GetAppPath & "\mpnAbbreviations.dat"
    Open xDatFile For Input As #1 ' Open file.
    Do While Not EOF(1) ' Loop until end of file.
        Line Input #1, TextLine ' Read line into variable.
        If TextLine <> "" Then
''           trim periods
'            If Right(TextLine, 1) = "." Then _
'                TextLine = Left(TextLine, Len(TextLine) - 1)
            xlistarray(UBound(xlistarray)) = TextLine
        End If
        ReDim Preserve xlistarray(UBound(xlistarray) + 1)
    Loop
    If xlistarray(UBound(xlistarray)) = "" Then ReDim Preserve xlistarray(UBound(xlistarray) - 1)
Title_ERROR:
    Close #1    ' Close file.
End Function

Function bIsAbbreviation(xTest As String) As Boolean
'Returns TRUE if xTest ends with a common abbreviation
'which is not to be confused with a sentence delimiter
    Dim i As Integer
    Dim iLen As Integer
    Dim xPrev As String
    
    For i = 0 To UBound(g_xAbbreviations)
        iLen = Len(g_xAbbreviations(i))
        If Len(xTest) > iLen Then
'           compare last characters of segment if previous character is
'           a space or pipe, i.e. if they're not part of a longer word
            xPrev = Mid(xTest, (Len(xTest) - iLen), 1)
            If xPrev = " " Or xPrev = "|" Then
                If UCase(Right(xTest, iLen)) = UCase(g_xAbbreviations(i)) Then
'                   this is a listed abbreviation - look
'                   for next potential sentence
                    bIsAbbreviation = True
                    Exit Function
                End If
            End If
        ElseIf Len(xTest) = iLen Then
'           compare entire segment
            If UCase(xTest) = UCase(g_xAbbreviations(i)) Then
'               this is a listed abbreviation - look
'               for next potential sentence
                bIsAbbreviation = True
                Exit Function
            End If
        End If
    Next i

End Function

Sub SetScheduleLevels(iMin As Integer, xTOC9Style As String)
'ensure that ouline level is in selected range;
'store existing settings in an array
    Dim i As Integer
    Dim iStyles As Integer
    Dim stySchedule As Word.Style
    
    On Error GoTo ProcError
    
    iStyles = UBound(g_xScheduleStyles)
    ReDim g_xScheduleLevels(0 To iStyles, 0 To 1)
    
    For i = 0 To iStyles
        On Error Resume Next
        Set stySchedule = ActiveDocument.Styles(g_xScheduleStyles(i))
        On Error GoTo ProcError
        
        If Not stySchedule Is Nothing Then
            g_xScheduleLevels(i, 0) = g_xScheduleStyles(i)
            With stySchedule.ParagraphFormat
                g_xScheduleLevels(i, 1) = .OutlineLevel
                If (g_xScheduleStyles(i) = xTOC9Style) Then
'                   ensure inclusion regardless of range
                    .OutlineLevel = iMin
                ElseIf (.OutlineLevel = wdOutlineLevelBodyText) Then
'                   treat as level one
                    .OutlineLevel = wdOutlineLevel1
                End If
            End With
        End If
    Next i

    Exit Sub
ProcError:
    RaiseError "mpTOC.SetScheduleLevels"
    Exit Sub
End Sub

Sub RestoreScheduleLevels()
    Dim i As Integer
    Dim stySchedule As Word.Style

    On Error GoTo ProcError
    
    For i = 0 To UBound(g_xScheduleLevels)
        If g_xScheduleLevels(i, 1) <> "" Then
            On Error Resume Next
            Set stySchedule = ActiveDocument.Styles(g_xScheduleLevels(i, 0))
            On Error GoTo ProcError
            
            If Not stySchedule Is Nothing Then
                stySchedule.ParagraphFormat.OutlineLevel = Val(g_xScheduleLevels(i, 1))
            End If
        End If
    Next i
    
    Exit Sub
ProcError:
    RaiseError "mpTOC.RestoreScheduleLevels"
    Exit Sub
End Sub

Function bIsScheduleStyle(xStyle As String) As Boolean
    Dim i As Integer
    
    If xStyle <> "" Then
        For i = 0 To UBound(g_xScheduleStyles)
            If g_xScheduleStyles(i) = xStyle Then
                bIsScheduleStyle = True
                Exit Function
            End If
        Next i
    End If
End Function

Function bGoToMPTOC() As Boolean
    Dim rngTOC As Word.Range
    
    With ActiveDocument
        If .Bookmarks.Exists("mpTableOfContents") Then
            Selection.GoTo wdGoToBookmark, Name:="mpTableOfContents"
            Selection.Collapse wdCollapseStart
            bGoToMPTOC = True
        Else
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Aucune table des mati�res MacPac dans ce document ou signet TM manquant.", vbInformation, _
                    "Num�rotation MacPac"
            Else
                MsgBox "There is no MacPac Table of Contents in this " & _
                    "document or the TOC bookmark is missing.", vbInformation, _
                    "MacPac Numbering"
            End If
        End If
    End With
End Function

Private Function xGetDesignatedStyles(ByVal xInclusions As String, _
                                      ByVal xLevel9Style As String) As String
    Dim xTemp As String
    Dim iPos As Integer
    Dim xStyleList As String
    Dim xStyle As String
    Dim iLevel As Integer
    
    xTemp = xInclusions
    If Len(xTemp) Then
'       trim trailing and preceding commas
        xTemp = xTrimTrailingChrs(xTemp, ",", True, False)
        iPos = InStr(xTemp, ",")
        While iPos
            xStyle = Left(xTemp, iPos - 1)
            If xStyle <> xLevel9Style Then
                iLevel = ActiveDocument.Styles(xStyle).ParagraphFormat.OutlineLevel
            Else
                '9.9.5001 - for field TOC, this option is implemented in field itself
                iLevel = 9
            End If
            xStyleList = xStyleList & xStyle & "," & iLevel & ","
            xTemp = Mid(xTemp, iPos + 1)
            iPos = InStr(xTemp, ",")
        Wend
    End If
    xGetDesignatedStyles = xStyleList
End Function

Private Sub CleanUpTOCField(ByVal rngTOC As Word.Range, _
                            ByVal bReplaceShiftReturns As Boolean)
'prepare TOC field for potential native update
    Dim iPos As Integer
    Dim rngCode As Word.Range
    
    On Error GoTo ProcError
    
'   if no real entries, field will have been deleted in bReworkTOC
    If rngTOC.Fields.Count = 0 Then _
        Exit Sub
        
    'GLOG 5456 - remove chr(160) inserted by
    'underline to longest line function
    If InStr(rngTOC.Text, Chr(32) & Chr(160)) > 0 Then
        ReplaceText rngTOC, "^32" & ChrW(&HA0) & "{1,}", " ", True
    End If
        
    ActiveWindow.View.ShowFieldCodes = True
    
    Set rngCode = rngTOC.Fields(1).Code.Duplicate

    If bReplaceShiftReturns Then
        'remove \x switch
        With rngCode
            iPos = InStr(UCase(.Text), "\X")
            If iPos Then
                .MoveStart wdCharacter, iPos - 1
                .Collapse wdCollapseStart
                .Delete wdCharacter, 3
            End If
            iPos = 0
        End With
    Else
        'add \n switch to exclude page # - style needs to remain centered and
        'there's no way to make centered w/page # look right after native update
        With rngCode
            If InStr(UCase(.Text), "\N") = 0 Then
                iPos = InStrRev(.Text, "\")
                .MoveStart wdCharacter, iPos
                .Collapse wdCollapseStart
                .InsertAfter "n1-1 \"
            End If
        End With
    End If
    
    ActiveWindow.View.ShowFieldCodes = False
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpTOC.CleanUpTOCField"
    Exit Sub
End Sub

Sub ResetRightTabStop(secTOC As Word.Section)
    Dim sRightTabPos As Single
    Dim xTOCStyle As String
    Dim i As Integer
    Dim styTOC As Word.Style
    Dim styTOA As Word.Style
    Dim lColWidth As Single
    
    On Error GoTo ProcError
    
'   get proper position of page number tab stop
    With secTOC.PageSetup
        If .TextColumns.Count = 1 Then
            sRightTabPos = .PageWidth - .LeftMargin - _
                .RightMargin - .Gutter - InchesToPoints(0.05)
        ElseIf .TextColumns.Count = 2 Then
            lColWidth = mpMin(.TextColumns(1).Width, _
                .TextColumns(2).Width)
            sRightTabPos = lColWidth - InchesToPoints(0.01)
        Else
            sRightTabPos = .TextColumns(1).Width - InchesToPoints(0.01)
        End If
    End With
    
    For i = 1 To 9
'       modify the last tab position to proper position
        xTOCStyle = xTranslateTOCStyle(i)
        Set styTOC = ActiveDocument.Styles(xTOCStyle)
        With styTOC.ParagraphFormat.TabStops
            .Item(.Count).Position = sRightTabPos
        End With
    Next i
    
'   Also set TOA Page Number tab to match TOC
    Set styTOA = ActiveDocument.Styles(wdStyleTableOfAuthorities)
    With styTOA.ParagraphFormat.TabStops
        If .Count Then
            .Item(.Count).Position = sRightTabPos
        End If
    End With
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpTOC.ResetRightTabStop"
    Exit Sub
End Sub

Public Function iGetTOCStyleLevel(xStyle As String) As Integer
'returns the level of the specified TOC style
    With ActiveDocument
        Select Case xStyle
            Case .Styles(wdStyleTOC1).NameLocal
                iGetTOCStyleLevel = 1
            Case .Styles(wdStyleTOC2).NameLocal
                iGetTOCStyleLevel = 2
            Case .Styles(wdStyleTOC3).NameLocal
                iGetTOCStyleLevel = 3
            Case .Styles(wdStyleTOC4).NameLocal
                iGetTOCStyleLevel = 4
            Case .Styles(wdStyleTOC5).NameLocal
                iGetTOCStyleLevel = 5
            Case .Styles(wdStyleTOC6).NameLocal
                iGetTOCStyleLevel = 6
            Case .Styles(wdStyleTOC7).NameLocal
                iGetTOCStyleLevel = 7
            Case .Styles(wdStyleTOC8).NameLocal
                iGetTOCStyleLevel = 8
            Case .Styles(wdStyleTOC9).NameLocal
                iGetTOCStyleLevel = 9
            Case Else
                iGetTOCStyleLevel = 0
        End Select
    End With
End Function

Private Function InRowAtStartOfDoc() As Boolean
    If Selection.Information(wdWithInTable) Then
        InRowAtStartOfDoc = (Selection.Rows(1).Range.Start = 0)
    Else
        InRowAtStartOfDoc = False
    End If
End Function

Function bReworkTOCField(rngTOC As Word.Range, _
                    xScheme As String, _
                    xExclusions As String, _
                    bIncludeStyles As Boolean, _
                    bIncludeTCEntries As Boolean, _
                    udtTOCFormat As TOCFormat, _
                    xTOC9Style As String, _
                    bDesignate As Boolean, _
                    bApplyManualFormats As Boolean) As Boolean
'cycles through TOC paras, cleaning
'up in any number of ways
    Const mpErrorMargin As Integer = 5
    
    Dim i As Integer
    Dim rngP As Word.Range
    Dim iLevel As Integer
    Dim bIsCentered As Boolean
    Dim paraTOC As Word.Paragraph
    Dim xTabFlag(8) As String
    Dim iNumTabs As Integer
    Dim paraP As Word.Paragraph
    Dim l As Long
    Dim lNumParas As Long
    Dim pfPrev As Word.ParagraphFormat
    Dim pfCur As Word.ParagraphFormat
    Dim xPara As String
    Dim bIsMarked As Boolean
    Dim bExclude As Boolean
    Dim bIsMarkedDel As Boolean
    Dim xPageNo As String
    Dim sProgress As Single
    Dim sTab As Single
    Dim xNextPara As String
    Dim iPos As Integer
    Dim xParaNum As String
    Dim xHeading As String
    Dim rngHeading As Word.Range
    Dim bIsSchedule As Boolean
    Dim bIsField As Boolean
    Dim bIsFirstPara As Boolean
    Dim xStyle As String
    Dim xStylePrev As String
    Dim iDisplay As Integer
    Dim bIs2007 As Boolean
    Dim bPreserveLineBreaks As Boolean
    Dim sHPos As Single
    Dim bIncludePageNumber As Boolean
    
    bIs2007 = (g_iWordVersion = mpWordVersion_2007)
    lNumParas = rngTOC.Paragraphs.Count
    
'   substitute tabs for soft returns unless ini says otherwise for this TOC scheme -
'   before we added bReworkTOCField in 9.9.5, and switched from never preserving shift-
'   returns for "TOC as Field" to always preserving them (as we always have for the text
'   TOC), this ini key was implemented via the native switch - it was a workaround added
'   solely for Torys in 2008 to account for even earlier custom code to accomplish a
'   non-centered level with shift-returns in one of their TOC schemes
    For i = 0 To UBound(g_vPreserveLineBreaks)
        If xTranslateTOCSchemeDisplayName(CStr(g_vPreserveLineBreaks(i))) = xScheme Then
            bPreserveLineBreaks = True
            Exit For
        End If
    Next i
    If Not bPreserveLineBreaks Then
        ReplaceText rngTOC, String(2, 11), vbTab
        ReplaceText rngTOC, Chr(11), vbTab
    End If

    'GLOG 5495 - remove spaces before tabs - this can be an issue when
    'there's a bounding object before the number's trailing character(s)
    ReplaceText rngTOC, " " & vbTab, vbTab
    
    'GLOG 5174 - replace shift-return substitutes with spaces
    ReplaceText rngTOC, "zzmpShiftReturn", " "
    
'---cycle through TOC paragraphs
    For Each paraTOC In rngTOC.Paragraphs
        Set rngP = paraTOC.Range
        With rngP
            'exit if we're no longer in the body of the TOC -
            'this line is necessary because the range will include the paragraph
            'following the field, which is problematic when there's subsequent text
            'in the section - this does not occur with the text TOC
            If .End > rngTOC.End Then
                Exit For
            End If
                
            Set pfCur = .ParagraphFormat
            xPara = .Text

'           skip empty paras
            If Len(xPara) < 2 Then
                If paraTOC.Range = rngTOC.Paragraphs.Last.Range Then
                    Exit For
                Else
                    GoTo NextPara
                End If
            End If

'           get level
            xStyle = pfCur.Style
            iLevel = iGetTOCStyleLevel(xStyle)

'           get alignment
            bIsCentered = pfCur.Alignment
            
'           get include page number setting
            bIncludePageNumber = udtTOCFormat.IncludePageNumber(iLevel)

'           adjust hanging indent of style based on
'           first representative entry for this level
'           determine if it's a numbered entry and
'           whether level has already been checked;
            iNumTabs = lCountChrs(xPara, vbTab)
            If iNumTabs > 1 Then
                'remove extra tabs
                If iNumTabs > Abs(CInt(bIncludePageNumber)) + 1 Then
                    With rngP
                        'move after first tab pos
                        .MoveStartUntil vbTab
                        .MoveStart wdCharacter
            
                        'replace all extra tabs with a space
                        While lCountChrs(.Text, vbTab) > Abs(CInt(bIncludePageNumber))
                            .MoveStartUntil vbTab
                            .Characters(1) = " "
                        Wend
                        .Expand wdParagraph
                    End With
                End If
                
                If g_bUseNewTabAdjustmentRules Then
                    'new option checks every entry for tightness and adjusts only as
                    'much as needed, rather than taking first entry as representative
                    'and leaving a cushion for potentially longer entries
                    Set rngHeading = rngP.Duplicate
                    sHPos = MeasureHeading(rngHeading, pfCur, xPara)
                    
                    'adjust flag if longest entry found for level
                    If xTabFlag(iLevel - 1) = "" Then
                        xTabFlag(iLevel - 1) = CStr(sHPos)
                    ElseIf sHPos > CSng(xTabFlag(iLevel - 1)) Then
                        xTabFlag(iLevel - 1) = CStr(sHPos)
                    End If
                ElseIf xTabFlag(iLevel - 1) = "" Then
                    'set flag
                    xTabFlag(iLevel - 1) = "X"
                    AdjustStyleIndents rngP, pfCur, iLevel, xPara
                End If
            ElseIf (iNumTabs = 1) And Not bIsCentered Then
                If bIs2007 Then
                    'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                    'repeated selections from producing a visual "creeping" effect, and
                    'Range.Information has not been problematic since Word 2000
                    Dim oRange As Word.Range
                    Set oRange = .Characters.First
                    With oRange
                        .EndOf wdParagraph
                        .Move wdCharacter, -1
                        'style is used, since tabstops method of selection
                        'will fail when document's default tab stop is 0"
                        With ActiveDocument.Styles(xStyle).ParagraphFormat.TabStops
                            If .Count > 0 Then _
                                sTab = .Item(.Count).Position
                        End With
                        If .Information(wdHorizontalPositionRelativeToTextBoundary) < _
                                sTab - InchesToPoints(0.25) Then
                            'measurement is not accurate to the point, so allow 1/4"
                            'leeway - this won't be a problem because hanging indent
                            'or preceding tab stop will never be set this close to right tab
                            .MoveUntil vbTab, wdBackward
                            .InsertAfter vbTab
                        End If
                    End With
                Else
                    .Characters.First.Select
                    With Selection
                        .EndOf wdParagraph
                        .Move wdCharacter, -1
                        'style is used, since tabstops method of selection
                        'will fail when document's default tab stop is 0"
                        With ActiveDocument.Styles(xStyle).ParagraphFormat.TabStops
                            If .Count > 0 Then _
                                sTab = .Item(.Count).Position
                        End With
                        If .Information(wdHorizontalPositionRelativeToTextBoundary) < _
                                sTab - InchesToPoints(0.25) Then
                            'measurement is not accurate to the point, so allow 1/4"
                            'leeway - this won't be a problem because hanging indent
                            'or preceding tab stop will never be set this close to right tab
                            .MoveUntil vbTab, wdBackward
                            .InsertAfter vbTab
                        End If
                    End With
                End If
            End If

'           format centered paras
            If bIsCentered Then
                FormatCenteredLevel rngP, pfCur, True
            End If
            
'           format para to have or not have page number
            FormatPageNumber rngP, bIsCentered, pfCur, bIncludePageNumber, True

'           if first para of new level,
'           adjust space after previous paragraph -
'           pfPrev is set below, so it will be
'           nothing the first time around
            If Not (pfPrev Is Nothing) Then
                If (xStyle <> xStylePrev) Then
                    If (udtTOCFormat.SpaceBefore(iLevel) <> "") And _
                            (udtTOCFormat.SpaceBefore(iLevel) <> "mpNA") Then
                        pfPrev.SpaceAfter = udtTOCFormat.SpaceBefore(iLevel)
                    End If
                End If
            End If

'           call any custom code
            lRet = oCustTOC.lInTOCRework(rngP, _
                                         xScheme, _
                                         xExclusions, _
                                         bIncludeStyles, _
                                         bIncludeTCEntries)
            
            If lRet Then
                Err.Raise lRet, _
                    "oCustTOC.lInTOCRework", _
                    Err.Description
            End If
        End With
NextPara:
'       free up resources
        If Not g_bPreserveUndoListTOC Then _
            ActiveDocument.UndoClear

'       store current para format for
'       next iteration through loop
        xStylePrev = xStyle
        Set pfPrev = pfCur
        Set rngP = Nothing
        Set paraTOC = Nothing
        l = l + 1
        If ((l / lNumParas) * 100) > (iDisplay + 10) Then
            iDisplay = iDisplay + 10
            If g_lUILanguage = wdFrenchCanadian Then
                Application.StatusBar = "Refaire TM: " & iDisplay & "%"
            Else
                Application.StatusBar = "Reworking TOC: " & iDisplay & "%"
            End If
            sProgress = 48 + (12 * (l / lNumParas))
            g_oStatus.Show sProgress, g_xTOCStatusMsg
        End If
'OutputDebugString "bReworkTOC - Paragraph l=" & l
    Next paraTOC
        
    'adjust indents as necessary
    If g_bUseNewTabAdjustmentRules Then
        For i = 0 To 8
            If xTabFlag(i) <> "" And xTabFlag(i) <> "0" Then
                AdjustStyleIndentsNew i + 1, CSng(xTabFlag(i)), True
            End If
        Next i
    End If
    
    If bApplyManualFormats Then
'       remove underlining of any underlined tabs
        With rngTOC.Find
            .ClearFormatting
            .Format = True
            .Font.Underline = wdUnderlineSingle
            .Text = vbTab
            With .Replacement.Font
                .Underline = wdUnderlineNone
                .Hidden = False
            End With
            .Execute Replace:=wdReplaceAll
        End With
    End If
    
'   ensure no space before first entry
    rngTOC.Paragraphs(1).SpaceBefore = 0
End Function

Public Function InsertStyleSeparator(Optional ByVal bFormat As Boolean = False) As Long
'inserts style separator at cursor
    Dim bShowAll As Boolean
    Dim xStyle As String
    Dim fldExisting As Word.Field
    Dim oStyle As Word.Style
    Dim xNewStyle As String
    Dim oRange As Word.Range
    Dim xScheme As String
    Dim iLevel As Integer
    Dim oLT As Word.ListTemplate
    Dim xStyleRoot As String
    Dim bWarn As Boolean
    Dim oPara As Word.Paragraph
    Dim lPos As Long
    Dim bIsMPLT As Boolean
    
    On Error GoTo ProcError
        
    'ensure that selection is insertion
    If Selection.Type <> wdSelectionIP Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "Ne pas s�lectionner le texte avant d'effectuer cette proc�dure. Placer le curseur � la fin du texte qui doit �tre marqu� comme TM et r�essayer.", _
                vbExclamation, "Num�rotation MacPac"
        Else
            MsgBox "Do not select text before running this procedure.  " & _
                   "Position cursor after the text to be marked " & _
                   "for TOC and try again.", vbExclamation, AppName
        End If
        Exit Function
    End If
        
    'warn if no outline level
    On Error Resume Next
    Set oStyle = Selection.Paragraphs(1).Style
    On Error GoTo ProcError
    If Not oStyle Is Nothing Then
        '4/24/13 - don't warn about non-outline level mp numbering styles
        Set oLT = Selection.Paragraphs(1).Range.ListFormat.ListTemplate
        If Not oLT Is Nothing Then
            bIsMPLT = bIsMPListTemplate(oLT)
            Set oLT = Nothing
        End If
        bWarn = ((oStyle.ParagraphFormat.OutlineLevel = wdOutlineLevelBodyText) And _
            Not bIsScheduleStyle(oStyle.NameLocal) And Not bIsMPLT)
    End If
    
    'don't warn if paragraph already contains a style separator -
    'user should be able to freely move it
    If bWarn Then
        'check selected paragraph
        bWarn = Not mdlWordXML.IsStyleSeparator(Selection.Paragraphs(1))
        If bWarn Then
            'check trailing paragraph
            On Error Resume Next
            Set oRange = Selection.Previous(wdParagraph)
            On Error GoTo ProcError
            If Not oRange Is Nothing Then
                bWarn = Not mdlWordXML.IsStyleSeparator(oRange.Paragraphs(1))
            End If
        End If
    End If
    
    If bWarn Then
        If g_lUILanguage = wdFrenchCanadian Then
            lRet = MsgBox("L'insertion des s�parateurs de style ne marquera pas le texte comme TM car le style appliqu� n'a pas de niveau hi�rarchique. Utiliser l'une de ces m�thodes pour permettre d'ajouter le texte dans la TM:" & _
                vbCr & vbCr & "1. Marquer avec des codes TM au lieu des s�parateurs de style." & vbCr & _
                "2. Continuer avec l'insertion de s�parateur de style et changer ou modifier le style qui pr�c�de les s�parateurs afin d'obtenir un niveau hi�rarchique." & _
                vbCr & vbCr & "Continuer avec l'insertion de s�parateurs de style?", vbOKCancel, "Num�rotation MacPac")
        Else
            lRet = MsgBox("Inserting a style separator will not mark this text for TOC because " & _
                "the style applied to it does not have an outline level.  " & _
                "There are two ways to prepare this text for inclusion in the TOC:" & _
                vbCr & vbCr & "1. Mark with a TC code instead of a style separator." & vbCr & _
                "2. Continue with style separator insertion and change or edit the style " & _
                "preceding the separator to have an outline level." & _
                vbCr & vbCr & "Continue with style separator insertion?", vbOKCancel, AppName)
        End If
        
        If lRet = vbCancel Then _
            Exit Function
    End If
    
    'move to end of inline tag or content control
    lPos = mdlWordXML.GetPositionAfterInlineTag(Selection.Range)
    If lPos = 0 Then _
        lPos = mdlCC.GetPositionAfterInlineCC(Selection.Range)
    If lPos > 0 Then _
        Selection.SetRange lPos, lPos
    
    'remove existing style separator
    bRemoveStyleSeparators Selection.Range
    
    'remove existing TC code
    bMarkForTOCRemove Selection.Range
    
    'force show all
    With ActiveWindow.View
        bShowAll = .ShowAll
        .ShowAll = True
    End With
            
    'insert style separator
    Selection.InsertParagraphAfter
    mdlWordXML.InsertStyleSeparator
    With Selection.Previous(wdCharacter)
        If .Text = " " Then .Delete
    End With
    
    'restyle if mp numbering
    Set oRange = Selection.Previous(wdParagraph)
    Set oLT = oRange.ListFormat.ListTemplate
    If Not oLT Is Nothing Then
        If bIsMPListTemplate(oLT) Then
            xScheme = xGetLTRoot(oLT.Name)
            iLevel = oRange.ListFormat.ListLevelNumber
            
'            'apply heading style before separator
'            oRange.Style = GetSplitHeadingStyle(xScheme, iLevel)
            
            'apply para style after separator
            Selection.Style = GetSplitParaStyle(xScheme, iLevel)
            
            'add direct formatting if specified
            If bFormat Then
                oRange.EndOf
                oRange.Move wdCharacter, -1
                rngFormatTCHeading oRange, iLevel, True
            End If
        End If
    End If
    
    'apply blue font to style separator
    '10/29/12 - can't do this because numbers turn blue as well
    'after saving, closing, and reopening
'    oRange.Characters.Last.Font.ColorIndex = wdBlue
            
    ActiveWindow.View.ShowAll = bShowAll
    
    Exit Function
ProcError:
    ActiveWindow.View.ShowAll = bShowAll
    RaiseError "MPTOC90.mpTOC.InsertStyleSeparator"
    InsertStyleSeparator = Err
    Exit Function
End Function

Public Function bRemoveStyleSeparators(ByVal oRange As Word.Range) As Boolean
    Dim oPara As Paragraph
    Dim rngPara As Word.Range
    Dim bFound As Boolean
    Dim bShowAll As Boolean
    Dim rngDuplicate As Word.Range
    
    On Error GoTo ProcError
    
    'earlier versions don't support style separators
    If g_iWordVersion < mpWordVersion_2003 Then _
        Exit Function
    
    'force show all
    With ActiveWindow.View
        bShowAll = .ShowAll
        .ShowAll = True
    End With
            
    'expand to include previous paragraph - range may start with the
    'paragraph trailing a style separator
    Set rngDuplicate = oRange.Duplicate
    rngDuplicate.Expand wdParagraph
    rngDuplicate.MoveStart wdParagraph, -1
        
    'cycle through all paragraphs in range
    For Each oPara In rngDuplicate.Paragraphs
        If mdlWordXML.IsStyleSeparator(oPara) Then
            bFound = True
            Set rngPara = oPara.Range
            rngPara.Characters.Last.Delete
'            rngPara.Expand wdParagraph
'            If InStr(rngPara.Style, "Heading_L") > 0 Then
'                'remove heading style
'                rngPara.Style = ActiveDocument.Styles(rngPara.Style).BaseStyle
'            End If
        End If
    Next oPara
    
    ActiveWindow.View.ShowAll = bShowAll
    
    bRemoveStyleSeparators = bFound
    
    Exit Function
ProcError:
    ActiveWindow.View.ShowAll = bShowAll
    RaiseError "MPTOC90.mpTOC.bRemoveStyleSeparators"
    Exit Function
End Function

Public Function bMarkAndFormatHeadings(ByVal xTargetStyles As String, _
        ByVal iMarkAction As mpMarkActions, ByVal iFormatAction As mpFormatActions, _
        ByVal iType As mpMarkingModes, ByVal xHeadingDelimiter As String, _
        ByVal iScope As mpMarkFormatScopes, ByVal bPromptIfNoResults As Boolean, _
        ByVal iReplacementMode As mpMarkingReplacementModes) As Boolean
    Dim i As Integer
    Dim iNumParas As Integer
    Dim paraP As Word.Paragraph
    Dim iLevel As Integer
    Dim rngPara As Word.Range
    Dim bHeadingsMarked As Boolean
    Dim rngScope As Word.Range
    Dim bFormat As Boolean
    Dim bMark As Boolean
    Dim bIsOutlineLevel As Boolean
    Dim bIsMarked As Boolean
    Dim iMarkPos As Integer
    Dim lPos As Long
    Dim bUnmark As Boolean
    Dim bUnformat As Boolean
    Dim rngTCField As Word.Range
    Dim fldP As Word.Field
    Dim bIsStyleBased As Boolean
    Dim xLT As String
    Dim rngHeading As Word.Range
    Dim rngLocation As Word.Range
    Dim xTest As String
    Dim iUserChoice As Integer
    Dim bDelimiterFound As Boolean
    Dim xEndQuote As String
    Dim xSmartQuote As String
    Dim bIsStandard As Boolean
    Dim bIsEndQuote As Boolean
    Dim bIsSmartQuote As Boolean
    Dim lPos2 As Long
    Dim lPos3 As Long
    Dim oPrompt As VB.Form
    Dim xStyle As String
    Dim oLT As Word.ListTemplate
    Dim bIsMPLT As Boolean
    Dim bIsStyleSep As Boolean
    Dim iUserReplaceChoice As Integer
    Dim bReplace As Boolean
    Dim bManageStatus As Boolean
    
    On Error GoTo ProcError
    
    'determine whether to manage progess bar (9.9.6006)
    bManageStatus = Not g_oStatus Is Nothing

    'get actions to execute
    bMark = (iMarkAction = mpMarkAction_Mark)
    bUnmark = (iMarkAction = mpMarkAction_Unmark)
    bFormat = (iFormatAction = mpFormatAction_Format)
    bUnformat = (iFormatAction = mpFormatAction_Unformat)
        
    'set scope
    Select Case iScope
        Case mpMarkFormatScopes.mpMarkFormatScope_Document
            Set rngScope = ActiveDocument.Content
        Case mpMarkFormatScopes.mpMarkFormatScope_Section
            Set rngScope = ActiveDocument.Sections(Selection _
                .Information(wdActiveEndSectionNumber)).Range
        Case Else
            Set rngScope = Selection.Range
    End Select
        
    'get number of numbers in rngScope
    iNumParas = rngScope.ListParagraphs.Count
        
    'period - double quote - space(s) should be recognized as standard;
    'we're also accepting smart end quotes as a match for straight quotes
    bIsStandard = (xHeadingDelimiter = mpSentencePeriodTwo) Or _
        (xHeadingDelimiter = mpSentencePeriodOne)
    If xHeadingDelimiter = mpSentencePeriodTwo Then
        xEndQuote = mpSentenceQuoteTwo
        xSmartQuote = g_xSmartTwo
    ElseIf xHeadingDelimiter = mpSentencePeriodOne Then
        xEndQuote = mpSentenceQuoteOne
        xSmartQuote = g_xSmartOne
    ElseIf InStr(xHeadingDelimiter, Chr(34)) <> 0 Then
        xSmartQuote = xSubstitute(xHeadingDelimiter, Chr(34), ChrW(&H201D))
    End If
    
    'ensure leading and trailing pipes
    xTargetStyles = "|" & xTargetStyles & "|"

    'cycle through numbered paragraphs
    For Each paraP In rngScope.ListParagraphs
        bReplace = False
        bIsStyleSep = False
        bIsEndQuote = False
        bIsSmartQuote = False
        Set rngTCField = Nothing
        bIsMPLT = False
        Set rngPara = paraP.Range
                
        'get paragraph level
        iLevel = rngPara.ParagraphFormat.OutlineLevel
            
        'get style
        xStyle = ""
        On Error Resume Next
        xStyle = rngPara.Style
        On Error GoTo ProcError
        
        'do only specified styles -
        'GLOG 5298 - added a way to target all mp styles of a given outline level ("*_L#")
        If (xStyle <> "") And ((InStr(UCase$(xTargetStyles), _
                "|" & UCase$(xStyle) & "|") > 0) Or (InStr(UCase$(xTargetStyles), _
                "|*_L" & CStr(iLevel) & "|") > 0)) Then
            'skip if not part of a MacPac scheme
            Set oLT = rngPara.ListFormat.ListTemplate
            If Not oLT Is Nothing Then _
                bIsMPLT = bIsMPListTemplate(oLT)
            If Not bIsMPLT Then _
                GoTo NextPara
                
            'if we're here, then doc contains at least one target heading
            bHeadingsMarked = True
            
            'determine if para is already marked
            iMarkPos = InStr(rngPara, Chr(19) & " " & g_xTCPrefix & " ")
            If iMarkPos = 0 Then
                'check for style separator
                If g_iWordVersion > mpWordVersion_XP Then
                    If mdlWordXML.IsStyleSeparator(paraP) Then
                        iMarkPos = Len(rngPara)
                        bIsStyleSep = True
                    End If
                End If
            End If
            bIsMarked = iMarkPos
            
            'prompt to replace mark of other type
            If bMark And bIsMarked And (((iType = mpMarkingMode_StyleSeparators) And _
                    Not bIsStyleSep) Or ((iType = mpMarkingMode_TCCodes) And _
                    bIsStyleSep)) Then
                If (iUserReplaceChoice = vbYes) Or _
                        (iReplacementMode = mpMarkingReplacementMode_Automatic) Then
                    bReplace = True
                ElseIf (iUserReplaceChoice = vbNo) Or _
                        (iReplacementMode = mpMarkingReplacementMode_None) Then
                    bReplace = False
                Else
                    'hide status screen while prompting (9.9.6006)
                    If bManageStatus Then
                        g_oStatus.Hide
                        Set g_oStatus = Nothing
                    End If
                
                    'propose replacement to user
                    Application.ScreenUpdating = True
                    Application.ScreenRefresh
                    DoEvents
                        
                    'select proposed heading
                    rngPara.Select
                
                    'prompt user
                    If g_lUILanguage = wdFrenchCanadian Then
                        Set oPrompt = New frmMarkPromptFrench
                    Else
                        Set oPrompt = New frmMarkPrompt
                    End If
                    With oPrompt
                        .MarkingMode = iType
                        .IsAmbiguousHeading = False
                        .Show vbModal
                        iUserReplaceChoice = .m_iRetval
                        Unload oPrompt
                        Set oPrompt = Nothing
                    End With
                    
                    'restore status screen (9.9.6006)
                    If bManageStatus Then
                        Set g_oStatus = New CStatus
                        With g_oStatus
                            If g_lUILanguage = wdFrenchCanadian Then
                                .Title = "Ins�rer Table des mati�res"
                            Else
                                .Title = "Inserting Table of Contents"
                            End If
                            .ProgressBarVisible = True
                            .Show 3, g_xTOCStatusMsg
                        End With
                    End If
                    
                    Application.ScreenUpdating = False

                    If iUserReplaceChoice = vbCancel Then
                        'clean up
                        Application.StatusBar = ""
                        bMarkAndFormatHeadings = False
                        Exit Function
                    Else
                        bReplace = ((iUserReplaceChoice = vbYes) Or _
                            (iUserReplaceChoice = vbAbort))
                    End If
                End If
                
                If bReplace Then
                    bIsMarked = False
                    
                    If iType = mpMarkingMode_TCCodes Then
                        'TC code removal is built in to style separator
                        'insertion, but not vice versa
                        bRemoveStyleSeparators rngPara
                        rngPara.Expand wdParagraph
                    Else
                        'if existing TC code is at end of para, just remove it
                        For Each fldP In rngPara.Fields
                            If fldP.Type = wdFieldTOCEntry Then
                                Set rngTCField = rngGetField(fldP.Code)
                                If rngPara.End = rngTCField.End + 1 Then
                                    fldP.Delete
                                    bIsMarked = True
                                End If
                                Exit For
                            End If
                        Next fldP
                    End If
                End If
            End If
                
            'set heading range
            With rngPara
                'search for delimiter
                lPos = InStr(.Text, xHeadingDelimiter)
                
                If bIsStandard Then
                    'try with end quote
                    lPos2 = InStr(.Text, xEndQuote)
                    If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                        lPos = lPos2
                        bIsEndQuote = True
                        bIsSmartQuote = False
                    End If
                End If
                
                If xSmartQuote <> "" Then
                    'try with smart quote
                    lPos2 = InStr(.Text, xSmartQuote)
                    If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                        lPos = lPos2
                        bIsEndQuote = False
                        bIsSmartQuote = True
                    End If
                End If
                
                If bIsMarked Or bReplace Then
                    'expand to start of mark
                    .StartOf
                    .MoveEnd wdCharacter, iMarkPos - 1
                            
                    'GLOG 5202 - account for inline content controls
                    If g_iWordVersion = mpWordVersion_2007 Then
                        .MoveEnd wdCharacter, _
                            (mdlCC.CountCCsEndingInRange(rngPara) * 2)
                    End If
                ElseIf lPos Then
                    'expand to end of first sentence def mark
                    If xHeadingDelimiter = mpSentencePeriodOne Then
                        'if delimiter is period and 1 space,
                        'skip common abbreviations
                        While lPos
                            .StartOf
                            .MoveEnd wdCharacter, lPos
                            
                            'GLOG 5202 - account for inline content controls
                            If g_iWordVersion = mpWordVersion_2007 Then
                                .MoveEnd wdCharacter, _
                                    (mdlCC.CountCCsEndingInRange(rngPara) * 2)
                            End If
                            
                            If bIsAbbreviation(.Text) Then
                                bIsEndQuote = False
                                bIsSmartQuote = False
                                .Expand wdParagraph
                                
                                'search for delimiter
                                lPos2 = InStr(lPos + 1, .Text, xHeadingDelimiter)
                                
                                'try with end quote
                                lPos3 = InStr(lPos + 1, .Text, xEndQuote)
                                If (lPos3 <> 0) And _
                                        ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                    lPos2 = lPos3
                                    bIsEndQuote = True
                                    bIsSmartQuote = False
                                End If
                                
                                'try with smart quote
                                lPos3 = InStr(lPos + 1, .Text, xSmartQuote)
                                If (lPos3 <> 0) And _
                                        ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                    lPos2 = lPos3
                                    bIsEndQuote = False
                                    bIsSmartQuote = True
                                End If
                                
                                lPos = lPos2
                            Else
                                lPos = 0
                            End If
                        Wend
                    Else
                        'all other delimiters are final
                        .StartOf
                        .MoveEnd wdCharacter, lPos
                            
                        'GLOG 5202 - account for inline content controls
                        If g_iWordVersion = mpWordVersion_2007 Then
                            .MoveEnd wdCharacter, _
                                (mdlCC.CountCCsEndingInRange(rngPara) * 2)
                        End If
                    End If
                ElseIf (xHeadingDelimiter = mpSentencePeriodTwo) And _
                        (iUserChoice <> vbNo) And _
                        (bMark Or bFormat Or bUnformat) Then
                    'period and 2 spaces not found; check
                    'for instances of period and 1 space
                    lPos = InStr(.Text, mpSentencePeriodOne)
                    
                    'try with end quote
                    lPos2 = InStr(.Text, mpSentenceQuoteOne)
                    If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                        lPos = lPos2
                        bIsEndQuote = True
                        bIsSmartQuote = False
                    End If

                    'try with smart quote
                    lPos2 = InStr(.Text, g_xSmartOne)
                    If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                        lPos = lPos2
                        bIsEndQuote = False
                        bIsSmartQuote = True
                    End If
                    
                    'when using style separators, a standalone heading is as good as marked
                    If (lPos = 0) And (iType = mpMarkingMode_StyleSeparators) Then _
                        bIsMarked = True
                    
                    While lPos
                        .StartOf
                        .MoveEnd wdCharacter, lPos
                            
                        'GLOG 5202 - account for inline content controls
                        If g_iWordVersion = mpWordVersion_2007 Then
                            .MoveEnd wdCharacter, _
                                (mdlCC.CountCCsEndingInRange(rngPara) * 2)
                        End If
                        
                        If bIsAbbreviation(.Text) Then
                            'skip common abbreviations
                            bIsEndQuote = False
                            bIsSmartQuote = False
                            .Expand wdParagraph
                            
                            'search for delimiter
                            lPos2 = InStr(lPos + 1, .Text, mpSentencePeriodOne)

                            'try with end quote
                            lPos3 = InStr(lPos + 1, .Text, mpSentenceQuoteOne)
                            If (lPos3 <> 0) And _
                                    ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                lPos2 = lPos3
                                bIsEndQuote = True
                                bIsSmartQuote = False
                            End If
                                
                            'try with smart quote
                            lPos3 = InStr(lPos + 1, .Text, g_xSmartOne)
                            If (lPos3 <> 0) And _
                                    ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                lPos2 = lPos3
                                bIsEndQuote = False
                                bIsSmartQuote = True
                            End If
                            
                            lPos = lPos2
                        ElseIf iUserChoice = vbYes Then
                            '"Yes to all" - mark sentence
                            lPos = 0
                        Else
                            'hide status screen while prompting (9.9.6006)
                            If bManageStatus Then
                                g_oStatus.Hide
                                Set g_oStatus = Nothing
                            End If
                
                            'propose potential heading to user
                            Application.ScreenUpdating = True
                            Application.ScreenRefresh
                            DoEvents
                                
                            'select proposed heading
                            .Select
                        
                            'prompt user
                            If g_lUILanguage = wdFrenchCanadian Then
                                Set oPrompt = New frmMarkPromptFrench
                            Else
                                Set oPrompt = New frmMarkPrompt
                            End If
                            With oPrompt
                                .MarkingMode = iType
                                .IsAmbiguousHeading = True
                                .Show vbModal
                                iUserChoice = .m_iRetval
                                Unload oPrompt
                                Set oPrompt = Nothing
                            End With
                            
                            'restore status screen (9.9.6006)
                            If bManageStatus Then
                                Set g_oStatus = New CStatus
                                With g_oStatus
                                    If g_lUILanguage = wdFrenchCanadian Then
                                        .Title = "Ins�rer Table des mati�res"
                                    Else
                                        .Title = "Inserting Table of Contents"
                                    End If
                                    .ProgressBarVisible = True
                                    .Show 3, g_xTOCStatusMsg
                                End With
                            End If
                    
                            Application.ScreenUpdating = False

                            If iUserChoice = vbCancel Then
                                'clean up
                                Application.StatusBar = ""
                                bMarkAndFormatHeadings = False
                                Exit Function
                            ElseIf iUserChoice = vbRetry Then
                                '"No" - look for next possibility
                                bIsEndQuote = False
                                bIsSmartQuote = False
                                .Expand wdParagraph
                                lPos = InStr(lPos + 1, .Text, mpSentencePeriodOne)
                                If (lPos = 0) And (iType = mpMarkingMode_StyleSeparators) Then _
                                    bIsMarked = True
                            ElseIf iUserChoice = vbNo Then
                                '"No to all" - mark whole paragraph
                                .Expand wdParagraph
                                lPos = 0
                                If iType = mpMarkingMode_StyleSeparators Then _
                                    bIsMarked = True
                            ElseIf iUserChoice = vbAbort Then
                                '"Yes" - mark sentence
                                lPos = 0
                            ElseIf iUserChoice = vbIgnore Then
                                'skip paragraph
                                GoTo NextPara
                            End If
                        End If
                    Wend
                ElseIf Not bIsStandard Then
                    'if we're not looking for periods, don't mark
                    'entire paragraph when delimiter isn't found
                    GoTo NextPara
                ElseIf iType = mpMarkingMode_StyleSeparators Then
                    'when using style separators, a standalone heading is as good as marked
                    bIsMarked = True
                End If
                
                'if we're here, then at least one heading
                'matches sentence definition
                bDelimiterFound = True

                'exclude trailing para, end of cell
                If .Characters.Last = vbCr Then
                    .MoveEnd wdCharacter, -1
                ElseIf Len(.Text) Then
                    If Asc(Right(.Text, 1)) = 7 Then
                        .MoveEnd wdCharacter, -1
                    End If
                End If
                
                'exclude trailing character
                If .Characters.Last = Left(xHeadingDelimiter, 1) Then
                    If bIsEndQuote Or bIsSmartQuote Then
                        'include quote
                        .MoveEnd wdCharacter, 1
                    Else
                        .MoveEnd wdCharacter, -1
                    End If
                ElseIf bIsSmartQuote And _
                        (.Characters.Last = ChrW(&H201D)) Then
                    'non-standard delimiter beginning with quote
                    .MoveEnd wdCharacter, -1
                End If
                
                .Collapse wdCollapseEnd
                
            End With
            
            Application.ScreenUpdating = False
            
            'get TC field
            If bIsMarked Then
                For Each fldP In rngPara.Paragraphs(1).Range.Fields
                    If fldP.Type = wdFieldTOCEntry Then
                        Set rngTCField = rngGetField(fldP.Code)
                        Exit For
                    End If
                Next fldP
            End If
            
            If bMark And Not bIsMarked Then
                If iType = mpMarkingMode_TCCodes Then
                    'GLOG 5404 - prevent error when after block level cc
                    If g_iWordVersion = mpWordVersion_2007 Then
                        rngPara.Select
                        While mdlCC.AfterBlockLevelCC()
                            rngPara.Move wdCharacter, -1
                            Selection.Move wdCharacter, -1
                        Wend
                    End If
                    
                    'add tc field  - do not insert any text in code
                    rngPara.Fields.Add rngPara, _
                                       wdFieldTOCEntry, _
                                       , _
                                       False
                                       
                    'remove formatting from TC field
                    Set rngLocation = rngPara.Duplicate
                    With rngLocation
                        .MoveEnd wdParagraph
                        .Fields(1).Code.Font.Reset
                    End With
                Else
                    'insert style separator
                    rngPara.Select
                    InsertStyleSeparator
                End If
            ElseIf bUnmark And bIsMarked Then
                'unmark - added 8/10/00
                If bIsStyleSep Then
                    bRemoveStyleSeparators rngPara
                ElseIf Not rngTCField Is Nothing Then
                    rngTCField.Delete
                End If
            End If
            
            '4/24/13 - account for non-outline level mp numbering styles
            'GLOG 5294 - we accounted for non-outline level styles as far as not erring,
            'but they still weren't getting formatted/unformatted
            If iLevel = 10 Then _
                iLevel = iGetParagraphLevel(rngPara, 0)
                
            If (iLevel > 0) And (iLevel < 10) Then
                If bFormat Then
                    'format heading and number to appropriate level
                    rngFormatTCHeading rngPara, _
                                       iLevel, _
                                       bFormat
                ElseIf bUnformat Then
                    'unformat - added 8/10/00
                    If Not rngTCField Is Nothing Then
                        'move to end of TC field
                        rngPara.SetRange rngTCField.End, _
                            rngTCField.End
                    End If
                    Set rngHeading = rngGetTCHeading(rngPara)
                    rngHeading.Font.Reset
                End If
            End If
        
        End If
            
NextPara:
        'update status
        i = i + 1
        If g_lUILanguage = wdFrenchCanadian Then
            If bMark And bFormat Then
                xMsg = "Marquer et Formater: "
            ElseIf bMark Then
                xMsg = "Marquer: "
            ElseIf bFormat Then
                xMsg = "Formater: "
            ElseIf bUnmark And bUnformat Then
                xMsg = "Non marquer et non formater: "
            ElseIf bUnmark Then
                xMsg = "Non marquer: "
            ElseIf bUnformat Then
                xMsg = "Non formater: "
            End If
        Else
            If bMark And bFormat Then
                xMsg = "Marking and Formatting: "
            ElseIf bMark Then
                xMsg = "Marking: "
            ElseIf bFormat Then
                xMsg = "Formatting: "
            ElseIf bUnmark And bUnformat Then
                xMsg = "Unmarking and Unformatting: "
            ElseIf bUnmark Then
                xMsg = "Unmarking: "
            ElseIf bUnformat Then
                xMsg = "Unformatting: "
            End If
        End If
        
        Application.StatusBar = _
            xMsg & Format(i / iNumParas, "0%")
    Next paraP
    
    'display message if no headings marked
    If bPromptIfNoResults Then
        If Not bHeadingsMarked Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Aucune action n'a �t� effectu�e selon les options s�lectionn�es. V�rifiez vos s�lections et apportez les changements n�cessaires.", _
                    vbExclamation + vbOKOnly, "Num�rotation MacPac"
            Else
                MsgBox "No action could be taken based on the " & _
                       "selections made in the dialog box.  Please " & _
                       "check selections and make the necessary changes.", _
                       vbExclamation + vbOKOnly, AppName
            End If
        ElseIf Not bDelimiterFound Then
            If g_lUILanguage = wdFrenchCanadian Then
                MsgBox "Aucun �l�ment de cette d�finition n'a �t� trouv� dans le document.", _
                    vbExclamation + vbOKOnly, "Num�rotation MacPac"
            Else
                MsgBox "No instances of the specified sentence definition " & _
                    "were found in this document.", vbExclamation + vbOKOnly, AppName
            End If
        End If
    End If
    
    'clean up
    Application.StatusBar = ""
    bMarkAndFormatHeadings = True
    Exit Function
ProcError:
    RaiseError "MPTOC90.mpTOC.bMarkAndFormatHeadings"
    Exit Function
End Function

Public Sub ConvertToStyleSeparators()
'replaces TC codes in MacPac numbered paragraphs with style separators
    On Error GoTo ProcError
    Dim fldP As Word.Field
    Dim rngCode As Word.Range
    Dim lCodes As Long
    Dim l As Long
    Dim oLT As Word.ListTemplate
    Dim rngStart As Word.Range
    
    On Error GoTo ProcError
    
    Application.ScreenUpdating = False
    
    'get current selection for later reselection
    Set rngStart = Selection.Range
        
'   cycle through tc codes
    lCodes = Word.ActiveDocument.Fields.Count
    For Each fldP In Word.ActiveDocument.Fields
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = _
                "Convertir codes TM: " & _
                    Format(l / lCodes, "0%")
        Else
            Application.StatusBar = _
                "Converting TC Codes: " & _
                    Format(l / lCodes, "0%")
        End If
        l = l + 1
        
        If fldP.Type = wdFieldTOCEntry Then
            Set rngCode = fldP.Code
            Set oLT = rngCode.ListFormat.ListTemplate
            If Not oLT Is Nothing Then
                If bIsMPListTemplate(oLT) Then
                    fldP.Delete
                    If rngCode.End < rngCode.Paragraphs(1).Range.End - 1 Then
                        'run-in heading - add style separator
                        rngCode.Select
                        InsertStyleSeparator
                    End If
                End If
            End If
        End If
    Next fldP
    
    rngStart.Select
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    Exit Sub
ProcError:
    rngStart.Select
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    RaiseError "MPTOC90.mpTOC.ConvertToStyleSeparators"
    Exit Sub
End Sub

Public Sub ConvertToTCCodes()
'replaces style separators in MacPac numbered paragraphs with TC codes
    On Error GoTo ProcError
    Dim lNumParas As Long
    Dim l As Long
    Dim oLT As Word.ListTemplate
    Dim rngStart As Word.Range
    Dim oPara As Word.Paragraph
    Dim rngPara As Word.Range
    Dim oFld As Word.Field
    
    On Error GoTo ProcError
    
    Application.ScreenUpdating = False
    
    'get current selection for later reselection
    Set rngStart = Selection.Range
        
    lNumParas = ActiveDocument.ListParagraphs.Count

'   cycle through tc codes
    For Each oPara In Word.ActiveDocument.ListParagraphs
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = _
                "Convertir s�parateurs de style: " & _
                    Format(l / lNumParas, "0%")
        Else
            Application.StatusBar = _
                "Converting Style Separators: " & _
                    Format(l / lNumParas, "0%")
        End If
        l = l + 1
        
        If mdlWordXML.IsStyleSeparator(oPara) Then
            Set rngPara = oPara.Range
            Set oLT = rngPara.ListFormat.ListTemplate
            If Not oLT Is Nothing Then
                If bIsMPListTemplate(oLT) Then
                    'move to end of paragraph
                    rngPara.EndOf
                    rngPara.MoveEnd wdCharacter, -1
                    
                    'delete style separator
                    bRemoveStyleSeparators rngPara
                    
                    'insert TC code
                    Set oFld = rngPara.Fields.Add(rngPara, wdFieldTOCEntry, , False)
                                       
                    'remove formatting from TC code
                    oFld.Code.Font.Reset
                End If
            End If
        End If
    Next oPara
    
    rngStart.Select
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    Exit Sub
ProcError:
    rngStart.Select
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    RaiseError "MPTOC90.mpTOC.ConvertToTCCodes"
    Exit Sub
End Sub

Function bInsertTOC() As Long
'inserts a MacPac TOC
    Dim iMaxLevel As Integer
    Dim iMinLevel As Integer
    Dim iDefaultTCLevel As Integer
    Dim rngLocation As Word.Range
    Dim fldTOC As Word.Field
    Dim xTOCParams As String
    Dim rngTOC As Word.Range
    Dim xLevRange As String
    Dim iNumStyles As Integer
    Dim xStyleList As String
    Dim bShowAll As Boolean
    Dim bShowHidden As Boolean
    Dim iView As Integer
    Dim ds As Date
    Dim xMsg As String
    Dim iEntryType As mpTOCEntryTypes
    Dim udtTOCFormat As TOCFormat
    Dim i As Integer
    Dim xTestContent As String
    Dim xTimeMsg As String
    Dim lNumParas As Long
    Dim lWarningThreshold As Long
    Dim iLoc As Integer
    Dim bkmkTOCEntry As Word.Bookmark
    Dim bShowHiddenBkmks As Boolean
    Dim bSmartQuotes As Boolean
    Dim lZoom As Long
    Dim bIncludeSchedule As Boolean
    Dim bIncludeOther As Boolean
    Dim xInclusions As String
    Dim xExclusions As String
    Dim xTOC9Style As String
    Dim oDummyStyle As Word.Style
    Dim rngDummyPara As Word.Range
    Dim bIsCentered As Boolean
    Dim iDummySec As Integer
    Dim bColumnsReformatted As Boolean
    Dim lStartTick As Long
    Dim sElapsedTime As Single
    Dim bPreserveLineBreaks As Boolean
    Dim lShowTags As Long
    Dim dlgTOC As VB.Form
    Dim xScheme As String
    Dim bPrintHiddenText As Boolean
    Dim bInsertAsField As Boolean
    Dim iHeadingDef As Integer
    Dim oDoc As Word.Document
    
    On Error GoTo ProcError
    
    'OutputDebugString "Start of TOC macro"
    
'   validate environment
    If g_bOrganizerSavePrompt And (WordBasic.FileNameFromWindow$() = "") Then
        'prompt to save document
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "Cette macro n'est pas disponible dans un document non sauf-gard�.  Veuillez sauf-garder votre document avant d�ex�cuter cette macro." & _
                vbCr & vbCr & "Nous vous prions de nous excuser pour tout inconv�nient mais c'est un workaround provisoire pour un bogue de Microsoft.  Le bogue a �t� introduit r�cemment dans Word 2007 SP2 et affecte la copie des styles par l'interm�diaire du code utilisant la fonction de l�Organisateur de Word.  Le correctif de Microsoft devrait �tre disponible dans les deux prochains mois.", _
                vbInformation, "Num�rotation MacPac"
        Else
            MsgBox "This macro is not available in an unsaved document.  " & _
                "Please save your document and run this macro again." & vbCr & vbCr & _
                "We apologize for the inconvenience but this is a temporary " & _
                "workaround for a Microsoft bug that affects copying styles via code using Word's Organizer " & _
                "feature.  Microsoft's hotfix should be available within a couple months.", _
                vbInformation, AppName
        End If
        Exit Function
    ElseIf g_xTOCLocations(0) = "" Then
        If g_lUILanguage = wdFrenchCanadian Then
            xMsg = "Ne peut cr�er TM, il n'y a pas d'emplacement d�sign� dans mpn90.ini.  Contactez votre administrateur."
        Else
            xMsg = "Cannot create TOC because there are no TOC locations specified in " & _
                "mpn90.ini.  Please see your system administrator."
        End If
    Else
        On Error Resume Next
        xTestContent = ActiveDocument.Characters(2).Text
        If Err <> 0 Then
            If g_lUILanguage = wdFrenchCanadian Then
                xMsg = "Ne peut cr�er TM.  Le pr�sent document est vide."
            Else
                xMsg = "Cannot create TOC.  The " & _
                       "active document is empty."
            End If
        End If
        On Error GoTo ProcError
    End If
    
    If Len(xMsg) Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox xMsg, vbExclamation, "Num�rotation MacPac"
        Else
            MsgBox xMsg, vbExclamation, AppName
        End If
        Exit Function
    End If
    
'   warn about large document
    lWarningThreshold = Val(xAppGetFirmINIValue("TOC", _
        "NumberOfParasWarningThreshold"))
    lNumParas = ActiveDocument.Paragraphs.Count
    
    If lWarningThreshold > 0 And _
            lNumParas > lWarningThreshold Then
        If g_lUILanguage = wdFrenchCanadian Then
            lRet = MsgBox("Ce document contient " & lNumParas & " paragraphes.  MacPac TM macro fait beaucoup plus que les fonctions natives TM Word.  La mise en forme du document prendra quelques temps.  D�sirez-vous continuer ?", _
                vbYesNo + vbQuestion, "Num�rotation MacPac")
        Else
            lRet = MsgBox("This document contains " & lNumParas & " paragraphs.  " & _
                "The MacPac TOC macro does a lot more work than " & _
                "the native Word TOC function.  Formatting a document " & _
                "of this size will take some time.  Do you wish to continue?", _
                vbYesNo + vbQuestion, AppName)
        End If
        If lRet = vbNo Then Exit Function
    End If
    
    Set oCustTOC = New mpctoc.CCustomTOC
    
    If g_iTOCDialogStyle = mpTOCDialogStyle_Legacy Then
        If g_lUILanguage = wdFrenchCanadian Then
            Set dlgTOC = New frmInsertTOCOldFrench
        Else
            Set dlgTOC = New frmInsertTOCOld
        End If
    Else
        If g_lUILanguage = wdFrenchCanadian Then
            Set dlgTOC = New frmInsertTOCTextFrench
        Else
            Set dlgTOC = New frmInsertTOCText
        End If
    End If
    
'   mpTOC.sty is now only loaded as needed (12/10/01)
    LoadTOCSty
    
'   call any custom code
    lRet = oCustTOC.lBeforeDialogShow(dlgTOC)
    If lRet Then
        Err.Raise lRet, _
            "oCustTOC.lBeforeDialogShow", _
            Err.Description
    End If
    
    'OutputDebugString "Before form display"
    
    dlgTOC.Show vbModal
    
    'OutputDebugString "After form display"
    lStartTick = CurrentTick()
        
    If dlgTOC.Cancelled Then
        UnloadTOCSty
        Unload dlgTOC
        Set dlgTOC = Nothing
        Set oCustTOC = Nothing
        Exit Function
    End If
    
'   call any custom code
    lRet = oCustTOC.lAfterDialogShow()
    If lRet Then
        Err.Raise lRet, _
            "oCustTOC.lAfterDialogShow", _
            Err.Description
    End If

    'GLOG 5147
    Set oDoc = ActiveDocument
    
'   display status message
    Set g_oStatus = New CStatus
    With g_oStatus
        If g_lUILanguage = wdFrenchCanadian Then
            .Title = "Ins�rer Table des mati�res"
        Else
            .Title = "Inserting Table of Contents"
        End If
        .ProgressBarVisible = True
        .Show 2, g_xTOCStatusMsg
    End With

    'GLOG 5057 (2/1/12) - disable Print Hidden Text option in order
    'to prevent inaccurate page numbers
    bPrintHiddenText = Application.Options.PrintHiddenText
    If bPrintHiddenText Then _
        Application.Options.PrintHiddenText = False
    
'   delete hidden TOC bookmarks - prevents potential
'   "insufficient memory" error when generating TOC
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "G�n�re TM.  Veuillez patienter..."
    Else
        Application.StatusBar = "Generating TOC.  Please wait..."
    End If
    
    With ActiveDocument.Bookmarks
        bShowHiddenBkmks = .ShowHidden
        .ShowHidden = True
    End With
    For Each bkmkTOCEntry In ActiveDocument.Bookmarks
        With bkmkTOCEntry
            If Left(.Name, 4) = "_Toc" Then
                .Delete
                If Not g_bPreserveUndoListTOC Then _
                    ActiveDocument.UndoClear
            End If
        End With
    Next bkmkTOCEntry
    ActiveDocument.Bookmarks.ShowHidden = bShowHiddenBkmks
    
'   necessary for speed - this function executes
'   a large number of Word actions.  the undo buffer
'   fills up after 2 iterations, so to avoid a
'   msg to the user, clear out first.
    If Not g_bPreserveUndoListTOC Then _
        ActiveDocument.UndoClear
    
'   get/set environment
    With Application
        .ScreenRefresh
        .ScreenUpdating = False
    End With
    
    EchoOff
    With ActiveWindow.View
'       get original settings
        bShowAll = .ShowAll
        bShowHidden = .ShowHiddenText
        iView = .Type
        lZoom = .Zoom.Percentage
        
'       modify environment
        .ShowAll = True
        .ShowHiddenText = True
        .Type = wdNormalView
        .Zoom.Percentage = 100
    End With

    'hide xml tags
    If g_bXMLSupport Then _
        lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, False)

'   turn off track changes
    ActiveDocument.TrackRevisions = False
    
'   turn off smart quotes
    With Application.Options
        bSmartQuotes = .AutoFormatAsYouTypeReplaceQuotes
        .AutoFormatAsYouTypeReplaceQuotes = False
    End With
                    
    With dlgTOC
        'determine whether inserting as field
        If g_iTOCDialogStyle = mpTOCDialogStyle_Legacy Then _
            bInsertAsField = .chkInsertAsField
            
'       convert TOC location from string to integer
        Select Case .cbxInsertAt.Text
            Case mpTOCLocationEOF, mpTOCLocationEOFFrench
                iLoc = mpAtEOF
            Case mpTOCLocationBOF, mpTOCLocationBOFFrench
                iLoc = mpAtBOF
            Case mpTOCLocationAboveTOA, mpTOCLocationAboveTOAFrench
                iLoc = mpAboveTOA
            Case Else
                iLoc = mpAtInsertion
        End Select
        
'       store user choices
        iMaxLevel = .cbxMaxLevel
        iMinLevel = .cbxMinLevel
        
'       get inclusions/exclusions
        If .optInclude(0) Then
'           schemes
            xInclusions = .Inclusions
            xExclusions = .Exclusions
        Else
'           individual styles
            xInclusions = .StyleInclusions
            xExclusions = .StyleExclusions
        End If
        bIncludeSchedule = InStr(UCase(xInclusions), ",SCHEDULE,")
        bIncludeOther = InStr(UCase(xInclusions), ",OTHER,")
        If .chkApplyTOC9 Then
            xTOC9Style = .cbxScheduleStyles
        End If
        
'       if including Schedule styles, ensure that outline level is in range
        If .optInclude(1) Or bIncludeSchedule Then
            SetScheduleLevels iMinLevel, xTOC9Style
        End If

'       get TOC format
        udtTOCFormat = udtGetTOCFormat(.TOCScheme, .chkApplyTOC9)
    
'       get content type from option btns & chk boxes
        If .chkStyles Then
            If g_iTOCDialogStyle = mpTOCDialogStyle_Legacy Then
                If .optCreateFrom(0) Then
                    iEntryType = mpTOCEntryType_Sentence
                ElseIf .optCreateFrom(1) Then
                    iEntryType = mpTOCEntryType_PeriodSpace
                ElseIf .optCreateFrom(2) Then
                    iEntryType = mpTOCEntryType_Para
                End If
            Else
                iHeadingDef = .cbxHeadingDef.SelectedItem
                If iHeadingDef = 1 Then
                    iEntryType = mpTOCEntryType_Sentence
                ElseIf iHeadingDef = 0 Then
                    iEntryType = mpTOCEntryType_PeriodSpace
                Else
                    iEntryType = mpTOCEntryType_Para
                End If
            End If
        End If
        
        If .chkTCEntries Then
            iEntryType = iEntryType + mpTOCEntryType_TCEntries
        End If
        
        iDefaultTCLevel = 0
        
'       Word only allows 17 styles to be
'       explicitly included in toc  - if there
'       are more than 17, tag all outline styles
'       between min and max levels, else tag
'       only those outline level styles in the
'       inclusion list that are between the min
'       and max levels
        If .optInclude(1) Then
            iNumStyles = lCountChrs(xInclusions, ",") - 1
        Else
            iNumStyles = iMaxLevel * (lCountChrs(xInclusions, ",") - 1)
            If bIncludeOther Then
'               we need to bring in all styles
                iNumStyles = 18
            ElseIf bIncludeSchedule Then
'               schedule styles are not one per level - adjust total
                iNumStyles = iNumStyles - (iMaxLevel - iMinLevel) + UBound(g_xScheduleStyles)
            End If
        End If
        
        If iNumStyles <= 17 Then
            If .optInclude(1) Then
                xStyleList = xGetDesignatedStyles(xInclusions, "")
            Else
                xStyleList = xGetStyleList(xInclusions, _
                                           iMinLevel, _
                                           iMaxLevel, _
                                           .chkApplyTOC9, _
                                           .cbxScheduleStyles)
            End If
        End If
        
'       call any custom code
        lRet = oCustTOC.lBeforeTOCSectionInsert()
        If lRet Then
            Err.Raise lRet, _
                "oCustTOC.lBeforeTOCSectionInsert", _
                Err.Description
        End If
        
'       set location for insertion of TOC -
'       MacPac TOC is bookmarked as mpTableOfContents
        'OutputDebugString "Before section insert"
        Set rngLocation = rngGetTOCTarget(ActiveDocument, _
                                          iLoc, _
                                          .BoldHeaderTOC, _
                                          .BoldHeaderPage, _
                                          .CapHeaderTOC, _
                                          .CapHeaderPage, _
                                          .UnderlineHeaderTOC, _
                                          .UnderlineHeaderPage, _
                                          .NumberStyle, _
                                          .NumberPunctuation, _
                                          Not .ContinuePageNumbering)
                                          
'       format columns
        With rngLocation.Sections(1).PageSetup.TextColumns
            If dlgTOC.chkTwoColumn And (.Count = 1) Then
                .SetCount 2
                .Spacing = InchesToPoints(0.5)
                .EvenlySpaced = True
                bColumnsReformatted = True
            ElseIf (dlgTOC.chkTwoColumn = 0) And (.Count = 2) Then
                .SetCount 1
                bColumnsReformatted = True
            End If
        End With
        
        'OutputDebugString "After section insert"
        
'       call any custom code
        lRet = oCustTOC.lAfterTOCSectionInsert(rngLocation, _
            bColumnsReformatted)
        If lRet Then
            Err.Raise lRet, _
                "oCustTOC.lAfterTOCSectionInsert", _
                Err.Description
        End If
        
        If bInsertAsField Then
           'if generating from TC fields and leaving TOC field linked,
           'we need to fill tc codes; since we're also advertising this
           'as a shortcut to avoid having to run the "Convert To Word TC Codes"
           'after running mark all, we need to fill dynamic codes even when
           'these entries were actually generated from styles; but since
           'CleanUpTOCField can't delete the style list after generation from
           'both TC fields and styles, because it can't assume that all
           'numbered paras have been marked, it becomes a training issue
           'to generate from TC fields ONLY in this situation, to avoid double
           'entries after a native update; the real answer is to offer filled TC
           'codes as an option in Mark All and to only fill partially dynamic codes here
            If .chkTCEntries Then
                RestoreMPTCCodes
                FillTCCodes .chkApplyManualFormatsToTOC, False
            End If
        Else
'           tag document paras with macpac codes
            'OutputDebugString "Before tagging TOC entries"
            TagTOCEntries iMinLevel, _
                          iMaxLevel, _
                          iEntryType, _
                          (iNumStyles > 17), _
                          xStyleList, _
                          .chkApplyTOC9, _
                          .cbxScheduleStyles, _
                          .optInclude(1)
            'OutputDebugString "After tagging TOC entries"
        End If
        
'       fill partially dynamic codes with text
'       from left of code to beginning of para
        '9.9.5005 - made conditional on .chkTCEntries
        If .chkTCEntries Then _
            iRefreshPDynCodes 0, "", , .chkApplyManualFormatsToTOC
        'OutputDebugString "After updating TC entries"
        
'       Reset TOC scheme based on user choice if specified
        If .optTOCStyles(0) Or .chkApplyTOC9 Then
            xResetTOCStyles .TOCScheme, _
                            rngLocation.Sections(1), _
                            udtTOCFormat.CenterLevel1, _
                            .chkApplyTOC9, _
                            .optTOCStyles(1)
        End If
        
'       reset right tab stop if switching column format or if
'       style update was requested by user
        If .optTOCStyles(0) Or bColumnsReformatted Then
            ResetRightTabStop rngLocation.Sections(1)
        End If
        
        'OutputDebugString "After resetting styles"
    End With
    
''   if leaving TOC as a field, add dummy entry at top of doc;
''   this will make it easier to work around issues related
''   to editing first paragraph of TOC field result
'    If dlgTOC.chkInsertAsField Then
''       create style
'        On Error Resume Next
'        Set oDummyStyle = ActiveDocument.Styles(mpDummyStyle)
'        On Error GoTo ProcError
'        If oDummyStyle Is Nothing Then
'            Set oDummyStyle = ActiveDocument.Styles.Add(mpDummyStyle)
'        End If
'        With oDummyStyle
'            .BaseStyle = wdStyleNormal
'            .ParagraphFormat.OutlineLevel = iMinLevel
'            .Font.Hidden = False
'        End With
'
''       insert para
'        If rngLocation.Sections(1).Index = 1 Then
'            iDummySec = 2
'        Else
'            iDummySec = 1
'        End If
'        Set rngDummyPara = ActiveDocument.Sections(iDummySec).Range
'        With rngDummyPara
'            .StartOf
'            .InsertParagraphAfter
'            .StartOf
'            .Style = mpDummyStyle
'            .Text = "x"
'            .Expand wdParagraph
'        End With
'
''       add to front of style list
'        xStyleList = mpDummyStyle & "," & iMinLevel & "," & xStyleList
'    End If
    
'   count number of styles to be included for TOC Entries
    If dlgTOC.optInclude(1) Then
        xLevRange = "1-9"
    Else
        xLevRange = iMinLevel & "-" & iMaxLevel
    End If
    
'   Word only allows 17 styles to be
'   explicitly included in toc - go figure
    If (dlgTOC.chkStyles = 1) Or Not bInsertAsField Then
        If iNumStyles > 17 Then
'           create TOC based on TC Entries
'           and outline level paragraphs
            xTOCParams = "\o """ & xLevRange & """" & "\w"
        Else
'           if system decimal separator is comma, use semi-colons in field
            '9.9.3004 - get system list separator and adjust string if necessary -
            'we were previously inferring the list separator from the decimal separator
            If Application.International(wdListSeparator) <> "," Then
                xStyleList = xSubstitute(xStyleList, ",", _
                    Application.International(wdListSeparator))
            End If
            
'           create TOC from explicit
'           style list and tc entries
            xTOCParams = "\t """ & xStyleList & """" & "\w"
        End If
    End If
    
'   preserve line breaks
    If bInsertAsField Then
        'TOC field - preserve only for schemes specified in ini
        For i = 0 To UBound(g_vPreserveLineBreaks)
            If xTranslateTOCSchemeDisplayName(CStr(g_vPreserveLineBreaks(i))) = _
                    dlgTOC.cbxTOCScheme Then
                bPreserveLineBreaks = True
                Exit For
            End If
        Next i
    Else
        'text TOC - always preserve
        bPreserveLineBreaks = True
    End If
    If bPreserveLineBreaks Then _
        xTOCParams = xTOCParams & " \x"
    
'   bring in static tc codes if specified
    If dlgTOC.chkTCEntries Then
        xTOCParams = xTOCParams & " \l """ & _
                     xLevRange & """"
    End If
    
'   use hyperlinks if specified
    If dlgTOC.chkHyperlinks Then
        xTOCParams = xTOCParams & " \h"
    End If
    
'   prevent hidden text from
'   affecting pagination of TOC
    With ActiveWindow.View
        .ShowAll = False
        .ShowHiddenText = False
        .ShowFieldCodes = False
    End With
    'OutputDebugString "Before inserting field"
                        
'   add toc
    g_oStatus.Show 4, g_xTOCStatusMsg
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "Word ins�re un champ Table des mati�res..."
    Else
        Application.StatusBar = "Word is inserting Table of Contents field..."
    End If
    
    Set fldTOC = ActiveDocument.Fields.Add( _
        rngLocation, wdFieldTOC, xTOCParams)
    'OutputDebugString "After inserting field"

'   call any custom code
    lRet = oCustTOC.lAfterTOCFieldInsert(fldTOC)
    If lRet Then
        Err.Raise lRet, "oCustTOC.lAfterTOCFieldInsert", Err.Description
    End If
       
'   bookmark toc
    rngGetField(fldTOC.Code).Bookmarks.Add "mpTableOfContents"
    Set rngTOC = ActiveDocument.Bookmarks("mpTableOfContents").Range
    
    With rngTOC
        If Asc(.Characters.Last) = 12 Then
            rngTOC.MoveEnd wdCharacter, -1
        End If
        
'       set all tags as hidden
'       text to prevent them from
'       affecting pagination
        'OutputDebugString "Before hiding tags"
        If Not bInsertAsField Then
            If Not g_bPreserveUndoListTOC Then _
                ActiveDocument.UndoClear
            HideTags bShowStatus:=True
            If Not g_bPreserveUndoListTOC Then _
                ActiveDocument.UndoClear
        End If
        'OutputDebugString "After hiding tags"
        
''       hide dummy paragraph
'        If dlgTOC.chkInsertAsField Then _
'            rngDummyPara.Font.Hidden = True
    
'       update page numbers
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = "Mise � jour les num�ros de page..."
        Else
            Application.StatusBar = "Word is updating page numbers in Table of Contents..."
        End If
        g_oStatus.Show 42, g_xTOCStatusMsg
        ActiveDocument.TablesOfContents(1) _
            .UpdatePageNumbers
        'OutputDebugString "After updating page numbers"
        
''       show dummy paragraph
'        If dlgTOC.chkInsertAsField Then _
'            rngDummyPara.Font.Hidden = False

'       make toc plain text
        If Not bInsertAsField Then
            If g_lUILanguage = wdFrenchCanadian Then
                Application.StatusBar = "Conversion de la Table des mati�res en texte..."
            Else
                Application.StatusBar = "Converting Table of Contents to text..."
            End If
            g_oStatus.Show 47, g_xTOCStatusMsg
            .Fields.Unlink
            'OutputDebugString "After unlinking"
        End If
        
'       show hidden text prior to
'       cleaning up doc and toc - this will
'       guarantee that codes that are hidden
'       are deleted if specified
        With ActiveWindow.View
            .ShowHiddenText = True
            .ShowAll = True
        End With
        
'       do only if TOC has entries
        If Not bIsTOCStyle(.Paragraphs.First.Style) Then
'           return doc to original state
            CleanUpDoc
            With rngTOC
                If .Characters.Last.Text = vbCr Then _
                    .MoveEnd wdCharacter, -1
                .Delete
            End With
'        ElseIf dlgTOC.chkInsertAsField And (.Paragraphs.Count = 2) Then
''           field contains only dummy entry - return doc to original state
'            CleanUpDoc
'            rngTOC.Fields(1).Delete
        Else
'           change content of toc and format
'           based on user dlg choices
'           remove manual font formats if specified
            If dlgTOC.chkApplyManualFormatsToTOC = 0 Then
                If rngTOC.Font.Name <> "" Then
                    rngTOC.Font.Reset
                Else
                    'GLOG 2793 - inconsistent font - do targeted
                    'reset to preserve bullets
                    ResetTOCFont rngTOC
                End If
            End If
            
'           call any custom code
            lRet = oCustTOC.lBeforeTOCRework(rngTOC)
            If lRet Then
                Err.Raise lRet, _
                    "oCustTOC.lBeforeTOCRework", _
                    Err.Description
            End If
            'OutputDebugString "Before reworking"
        
            With dlgTOC
                If Not bInsertAsField Then
                    bReworkTOC rngTOC, _
                               .cbxTOCScheme, _
                               xExclusions, _
                               .chkStyles, _
                               .chkTCEntries, _
                               udtTOCFormat, _
                               xTOC9Style, _
                               .optInclude(1), _
                               .chkApplyManualFormatsToTOC
                End If
            End With
            'OutputDebugString "After reworking"
    
'           call any custom code
            lRet = oCustTOC.lAfterTOCRework()
            If lRet Then
                Err.Raise lRet, _
                    "oCustTOC.lAfterTOCRework", _
                    Err.Description
            End If
    
'           clean up placeholder text in TOC
            If Not bInsertAsField Then _
                CleanUpTOC rngTOC
            'OutputDebugString "After cleaning up TOC"
            
''           prepare TOC field for potential native update
'            If dlgTOC.chkInsertAsField Then
'                bIsCentered = (ActiveDocument.Styles(wdStyleTOC1) _
'                    .ParagraphFormat.Alignment = wdAlignParagraphCenter)
'                CleanUpTOCField rngTOC, Not bIsCentered, _
'                    (iEntryType = mpTOCEntryType_TCEntries)
'                'OutputDebugString "After cleaning up TOC field"
'            End If

'           ensure that toc is not colored
            .Font.ColorIndex = wdAuto
            
'           add warning not to delete trailing para
            If .Sections.Last.Index = _
                    ActiveDocument.Sections.Count Then
                .EndOf
                If g_iWordVersion < mpWordVersion_2003 Then
                    '9.9.2 - deleting the trailing papagraph mark
                    'doesn't impact the TOC bookmark in Word 2003 or 2007,
                    'so there's no longer any need for this warning
                    If bInsertAsField Then .Move
                    .InsertAfter mpEndOfTOCWarning
                    .Font.Hidden = True
                    .HighlightColorIndex = wdYellow
                    .InsertParagraphAfter
                    With .Characters.Last
                        .Font.Hidden = False
                        .HighlightColorIndex = wdNoHighlight
                    End With
                    .EndOf
                    .Style = wdStyleNormal
                    .HighlightColorIndex = wdNoHighlight
                End If
            Else
                'GLOG 5393 - this code appears to be obsolete and
                'is problematic when the document starts with an
                'intentional empty paragraph between the existing TOC
                'and subsequent text in the same section - I haven't
                'been able to recreate any scenario in which we currently
                'insert an extraneous paragraph
''               delete extraneous paragraph
'                On Error Resume Next
'                If .Characters.Count > 1 Then
'                    If .Next(wdParagraph).Text = vbCr Then _
'                        .Next(wdParagraph).Delete
'                End If
'                On Error GoTo ProcError
            End If

'           return doc to original state
            If Not g_bPreserveUndoListTOC Then _
                ActiveDocument.UndoClear
            If Not bInsertAsField Then _
                CleanUpDoc
            'OutputDebugString "After cleaning up document"

        End If
    End With
    
''   delete dummy para and style
'    If dlgTOC.chkInsertAsField Then
'        With rngDummyPara
'            If .Style = oDummyStyle Then
'                .Delete
'                oDummyStyle.Delete
'            End If
'        End With
'    End If
    
'   in Word 97, all caps and bold need to be done at end
    If g_lUILanguage = wdFrenchCanadian Then
        xScheme = "Personnalis�"
    Else
        xScheme = "Custom"
    End If
    If (dlgTOC.cbxTOCScheme = xScheme) And _
            (dlgTOC.optTOCStyles(0) = True) Then
        Application.ScreenRefresh
        Application.ScreenUpdating = False
        For i = 1 To 9
            With ActiveDocument.Styles(xTranslateTOCStyle(i)).Font
                .AllCaps = udtTOCFormat.AllCaps(i)
                .Bold = udtTOCFormat.Bold(i)
            End With
        Next i
    End If
            
'   clear tc codes if specified
    'OutputDebugString "Before resetting TC entries"
    '9.9.4019 - this control defaulted checked and was hidden -
    'got rid of it with the switchover to the native tab control
    'If dlgTOC.chkClearTCCodes Then
    '9.9.5005 - made conditional on .chkTCEntries
    If dlgTOC.chkTCEntries Then _
        bRet = bResetTCEntries()
    'OutputDebugString "After resetting TC entries"
    
'   All TOC entries may have been marked for deletion
    If Not ActiveDocument.Bookmarks.Exists("mpTableOfContents") Then _
        ActiveDocument.Bookmarks.Add "mpTableOfContents", rngTOC
    
'   hide status display
    g_oStatus.Hide
    Set g_oStatus = Nothing
    
    'GLOG 5147 - ensure that focus remains on active document
    Application.Activate
    oDoc.Activate
    
'   alert if toc is empty
    If Len(ActiveDocument.Bookmarks("mpTableOfContents").Range) < 2 Then
        With rngTOC
'           move before "Do Not Delete" warning
            On Error Resume Next
            If .Previous(wdParagraph).Text = _
                    mpEndOfTOCWarning & vbCr Then
                .Move wdParagraph, -1
            End If
            On Error GoTo ProcError
'           insert warning in document
            If g_lUILanguage = wdFrenchCanadian Then
                .InsertBefore mpNoEntriesWarningFrench
            Else
                .InsertBefore mpNoEntriesWarning
            End If
            .Style = wdStyleNormal
            .HighlightColorIndex = wdNoHighlight
            .Bold = True
        End With
'       rebookmark
        ActiveDocument.Bookmarks.Add "mpTableOfContents", rngTOC
'       present dlg alert
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "La Table des mati�res est vide.  " & vbCr & _
                   "Aucune entr�e TM n'a �t� trouv� dans ce document.", _
                   vbExclamation, "Num�rotation MacPac"
        Else
            MsgBox "The Table Of Contents is empty.  " & vbCr & _
                   "No valid Table Of Contents entries " & _
                   "could be found in the document.", vbExclamation, AppName
        End If
    End If
    
    Set oCustTOC = Nothing
'   select start of TOC - force
'   vertical scroll to selection
    With ActiveDocument
        .Range(0, 0).Select
        .Bookmarks("mpTableOfContents").Select
    End With
    Selection.StartOf
    
'   restore user's smart quotes setting
    Application.Options _
        .AutoFormatAsYouTypeReplaceQuotes = bSmartQuotes
        
'   restore outline levels of schedule styles
    If dlgTOC.optInclude(1) Or bIncludeSchedule Then
        RestoreScheduleLevels
    End If
                
'   mpTOC.sty is now only loaded as needed (12/10/01);
'   9.8.1007 - mpTOC.sty is now left loaded
'    UnloadTOCSty
    
'   reset environment
    'OutputDebugString "Before resetting environment"
    With ActiveWindow.View
        .ShowAll = bShowAll
        .ShowHiddenText = bShowHidden
        .Type = iView
        .Zoom.Percentage = lZoom
    End With
    
    'restore xml tags
    If g_bXMLSupport Then _
        lShowTags = mdlWordXML.SetXMLMarkupState(ActiveDocument, lShowTags)
        
    'GLOG 5057 - restore setting
    If bPrintHiddenText Then _
        Application.Options.PrintHiddenText = True
    
    EchoOn
    bInsertTOC = True
    
    'OutputDebugString "End of TOC macro"
    
    On Error Resume Next
    Unload dlgTOC
    Set dlgTOC = Nothing
    
    '9.9.5005
    If g_bDisplayBenchmarks Then
        sElapsedTime = ElapsedTime(lStartTick)
        MsgBox CStr(sElapsedTime)
    End If
    Exit Function
    
ProcError:
    g_oStatus.Hide
    Set g_oStatus = Nothing
    
    'GLOG 5057 - restore setting
    If bPrintHiddenText Then _
        Application.Options.PrintHiddenText = True
    
    Unload dlgTOC
    EchoOn
    RaiseError "MPTOC90.mpTOC.bInsertTOC"
    bInsertTOC = Err
    Exit Function
End Function

Private Sub ResetTOCFont(ByVal rngTOC As Word.Range)
'added for GLOG 2793 - resets font paragraph by paragraph
'in order to avoid changing font of bullets
    Dim oPara As Word.Paragraph
    Dim rngTarget As Word.Range
    Dim oStyle As Word.Style
    Dim xAppliedFont As String
    Dim rngCharacter As Word.Range
    
    On Error GoTo ProcError
    
    For Each oPara In rngTOC.Paragraphs
        Set rngTarget = oPara.Range
        Set rngCharacter = rngTarget.Characters(1)
        xAppliedFont = rngCharacter.Font.Name
        If (xAppliedFont = "") Or (xAppliedFont = "Symbol") Or _
                (xAppliedFont = "Webdings") Or _
                (Left$(xAppliedFont, 9) = "Wingdings") Then
            Set oStyle = oPara.Style
            rngCharacter.Font.Bold = oStyle.Font.Bold
            rngCharacter.Font.Underline = oStyle.Font.Underline
            rngTarget.MoveStart
        End If
        rngTarget.Font.Reset
    Next oPara
    
    Exit Sub
ProcError:
    RaiseError "mpTOC.ResetTOCFont"
    Exit Sub
End Sub

