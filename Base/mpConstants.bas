Attribute VB_Name = "mpConstants"
'general
Public Const mpAppName As String = "MacPac Numbering"
Public Const mpSlogan As String = "A Professional Corporation"
Public Const mpDisplayBenchmarks As Boolean = False
Public Const mpPointsPerIn As Integer = 72
Public Const mpTwipsPerPoint As Integer = 1440 / 72

'templates
Public Const mpTemplateBusiness As String = "Business"
Public Const mpTemplateFax As String = "Fax"
Public Const mpTemplateDPhrase As String = "DPhrase"
Public Const mpTemplateLetter As String = "Letter"
Public Const mpTemplateMemo As String = "Memo"
Public Const mpTemplatePleading As String = "Pleading"
Public Const mpTemplatePOS As String = "POS"
Public Const mpPOSTitle As String = "Proof of Service"
Public Const mpTemplateVer As String = "Verification"
Public Const mpTemplateNotary As String = "Notary"
Public Const mpTemplateDepo As String = "Depo Summary"
Public Const mpTemplatePleadingIL As String = "PleadingIL"
Public Const mpPleadingILTitle As String = "Pleading IL"
Public Const mpTemplateCert As String = "Cert"
Public Const mpCertTitle As String = "Certificate of Service"
Public Const mpTemplateAppellate As String = "PleadingAppellate"
Public Const mpAppellateTitle As String = "Pleading Appellate"
Public Const mpPropNotAvailable As String = "|mpNA|"
Public Const mpPrefix As String = "zzmp"
Public Const mpNativeHeadingScheme As String = "HeadingStyles"

'PleadingPaper
Public Const mpHeaderDistDefault As String = ".5"
Public Const mpFooterDistDefault As String = ".5"

Public Const mpPleadingHeaderDistance As String = ".3"
Public Const mpPleadingFooterDistance As String = ".25"

Public Const mpTopMarginDefault As String = "1"
Public Const mpBottomMarginDefault As String = "1"
Public Const mpLeftMarginDefault As String = "1"
Public Const mpRightMarginDefault As String = "1"

Public Const mpPleading26TopMargin As String = "-1.35"
Public Const mpPleading26BotMargin As String = "1.12"
Public Const mpPleading26LeftMargin As String = "1.45"
Public Const mpPleading26RightMargin As String = ".5"

Public Const mpPleading28LeftMargin As String = "1.45"
Public Const mpPleading28RightMargin As String = ".5"
Public Const mpPleading28TopMargin As String = "-.92"
Public Const mpPleading28BotMargin As String = ".75"

'msgs
Public Const mpMsgAppInitializing As String = "Initializing MacPac, please wait..."
Public Const mpMsgInitializing As String = "Initializing.  Please wait..."
Public Const mpMsgReady As String = "Ready"
Public Const mpMsgEditingDPhrase As String = "Editing Delivery Phrases.  Please wait..."
Public Const mpMsgEditingBSig As String = "Editing Business Signature.  Please wait..."
Public Const mpMsgInsertingRogs As String = "Inserting Interrogatories.  Please wait..."
Public Const mpMsgCreatingDoc As String = "Creating document.  Please wait..."
Public Const mpMsgWritingTrailer As String = "Updating trailer.  Please wait..."
Public Const mpMsgBeginTyping As String = "Begin typing"
Public Const mpMsgDefAuthorSet As String = "Default author set to "
Public Const mpMsgBeginInput As String = "Please enter a name"
Public Const mpMsgUpdatingDBTables As String = "Saving record.  Please wait..."
Public Const mpMsgEditingDBTables As String = "Updating table.  Please wait..."
Public Const mpMsgRequiredField As String = "Required field.  Please enter a value."
Public Const mpMsgFinished As String = "Finished"
Public Const mpMsgLSigCreate As String = "Inserting Letter Signature.  Please wait..."
Public Const mpMsgInsertingPPaper As String = "Inserting Pleading Paper.  Please wait..."
Public Const mpMsgRemovingPPaper As String = "Removing Pleading Paper.  Please wait..."

'page number format
Public Const mpPageNumFormatTOC As String = "\* roman"
Public Const mpPageNumIntroChrDefault As String = "-"
Public Const mpPageNumTrailChrDefault As String = "-"


