Attribute VB_Name = "mdlTypes"
Option Explicit

'defines a scheme record -
'held as doc var
Type mpScheme
    Name As String
    Alias As String
    Type As mpSchemeTypes
    Origin As mpSchemeTypes
    TOCScheme As String
End Type

Type mpDocEnvironment
    bShowAll As Boolean
    sVScroll As Single
    sHScroll As Single
    iView As Integer
    bFieldCodes As Boolean
    bBookmarks As Boolean
    bTabs As Boolean
    bSpaces As Boolean
    bHyphens As Boolean
    bHiddenText As Boolean
    bParagraphs As Boolean
    bTextBoundaries As Boolean
    iProtectionType As Integer
    iSelectionStory As Integer
    lSelectionStartPos As Long
    lSelectionEndPos As Long
End Type

Type mpAppEnvironment
    lBrowserTarget As Long
End Type

Type DRAGRECIP
    Field1 As String
    Field2 As String
    Field3 As String
    Field4 As String
    Field5 As String
    Field6 As String
    Field7 As String
    Field8 As String
    Field9 As String
    Field10 As String
    Field11 As String
End Type

