VERSION 5.00
Begin VB.Form frmTimer 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Timer"
   ClientHeight    =   375
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   570
   ControlBox      =   0   'False
   Icon            =   "frmTimer.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   375
   ScaleWidth      =   570
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   -30
      Top             =   -30
   End
End
Attribute VB_Name = "frmTimer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Form_Activate()
    Me.Visible = False
    Me.Timer1.Enabled = True
End Sub
Private Sub Form_Load()
    On Error Resume Next
    'GLOG 5160 - this is now a global variable
'    Dim xInterval As String
'    xInterval = xGetAppIni("Timer", "Delay")
    If IsNumeric(g_xTimerDelay) Then
        If Val(g_xTimerDelay) > 0 And Val(g_xTimerDelay) < 21 Then
           Me.Timer1.Interval = 60 * Val(g_xTimerDelay)
        End If
    End If
    Me.Top = -1000
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Set frmTimer = Nothing
End Sub
Private Sub Timer1_Timer()
    Timer1.Enabled = False
    Unload Me
    ShowSchemesDialogs
End Sub
