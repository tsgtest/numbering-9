Attribute VB_Name = "mdlAdmin"
Option Explicit

Private Sub ConvertToNameBi()
'sets origin field of all MacPac schemes in
'public mpNumbers.sty to be mpSchemeType_Public
    Dim ltP As Word.ListTemplate
    Dim oPublicSty As Word.Document
    Dim xPublicSty As String
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    xPublicSty = App.Path & "\mpNumbers.sty"
    On Error Resume Next
    
'   open public sty file
    Set oPublicSty = Word.Documents.Open(xPublicSty)
    
    If (oPublicSty Is Nothing) Then
'       file could not be opened
        xMsg = "Could not open the file '" & xPublicSty & "'.  " & _
            "Please ensure that both mpNumbers.sty and MPN90Admin.exe " & _
            "are located in the numbering 'App' directory."
        MsgBox xMsg, vbExclamation, App.Title
        Exit Sub
    Else
        Word.AddIns(xPublicSty).Installed = False
    End If
    
    On Error GoTo ProcError
    Screen.MousePointer = vbHourglass
    
    For Each ltP In ActiveDocument.ListTemplates
        If mpBase.bIsMPListTemplate(ltP) Then
            mpBase.SetField ltP.Name, _
                            mpRecField_Origin, _
                            mpSchemeType_Public, , _
                            ActiveDocument
        End If
    Next ltP
    
    oPublicSty.Close wdSaveChanges
    Word.AddIns(xPublicSty).Installed = True
    Screen.MousePointer = vbDefault
    MsgBox "Schemes are now public.", vbInformation, App.Title
    
    Exit Sub
ProcError:
    MsgBox "The following error occurred while converting to public schemes:" & vbCr & _
        Err.Number & " :: " & Err.Description & " :: ConvertPrivateToPublic", vbCritical, App.Title
    Me.MousePointer = vbDefault
    Exit Sub
End Sub


