VERSION 5.00
Begin VB.Form frmCustomize 
   Caption         =   "Table of Contents - Customize Body"
   ClientHeight    =   4950
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4950
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkDefaultScheme 
      Caption         =   "Set as default TOC scheme"
      Height          =   240
      Left            =   135
      TabIndex        =   26
      Top             =   4450
      Width           =   2300
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "Save"
      Default         =   -1  'True
      Height          =   393
      Left            =   2850
      TabIndex        =   25
      Top             =   4450
      Width           =   800
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   393
      Left            =   3750
      TabIndex        =   24
      Top             =   4450
      Width           =   800
   End
   Begin VB.CheckBox chkCapLevelOne 
      Caption         =   "Level One"
      Height          =   270
      Left            =   2600
      TabIndex        =   23
      Top             =   3600
      Width           =   1080
   End
   Begin VB.CheckBox chkCapSubsequent 
      Caption         =   "Subsequent Levels"
      Height          =   300
      Left            =   2600
      TabIndex        =   22
      Top             =   3870
      Width           =   1845
   End
   Begin VB.CheckBox chkDotLevelOne 
      Caption         =   "Level One"
      Height          =   270
      Left            =   2600
      TabIndex        =   21
      Top             =   2900
      Width           =   1080
   End
   Begin VB.CheckBox chkDotSubsequent 
      Caption         =   "Subsequent Levels"
      Height          =   300
      Left            =   2600
      TabIndex        =   20
      Top             =   3170
      Width           =   1845
   End
   Begin VB.CheckBox chkPageSubsequent 
      Caption         =   "Subsequent Levels"
      Height          =   300
      Left            =   2600
      TabIndex        =   19
      Top             =   2470
      Width           =   1845
   End
   Begin VB.CheckBox chkPageLevelOne 
      Caption         =   "Level One"
      Height          =   270
      Left            =   2600
      TabIndex        =   18
      Top             =   2200
      Width           =   1080
   End
   Begin VB.TextBox txtBefore 
      Height          =   315
      Left            =   3200
      TabIndex        =   15
      Top             =   1650
      Width           =   540
   End
   Begin VB.TextBox txtBetween 
      Height          =   315
      Left            =   3200
      TabIndex        =   14
      Top             =   1000
      Width           =   540
   End
   Begin VB.ComboBox cmbCaps 
      Height          =   315
      ItemData        =   "frmCustomize.frx":0000
      Left            =   120
      List            =   "frmCustomize.frx":0007
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   3800
      Width           =   2115
   End
   Begin VB.ComboBox cmbDotLeaders 
      Height          =   315
      ItemData        =   "frmCustomize.frx":0013
      Left            =   120
      List            =   "frmCustomize.frx":001A
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   3100
      Width           =   2115
   End
   Begin VB.ComboBox cmbPageNumbers 
      Height          =   315
      ItemData        =   "frmCustomize.frx":0026
      Left            =   120
      List            =   "frmCustomize.frx":002D
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   2400
      Width           =   2115
   End
   Begin VB.ComboBox cmbBefore 
      Height          =   315
      ItemData        =   "frmCustomize.frx":0039
      Left            =   120
      List            =   "frmCustomize.frx":0040
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   1650
      Width           =   2820
   End
   Begin VB.ComboBox cmbBetween 
      Height          =   315
      ItemData        =   "frmCustomize.frx":004C
      Left            =   120
      List            =   "frmCustomize.frx":0053
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1000
      Width           =   2820
   End
   Begin VB.ComboBox cmbLineSpacing 
      Height          =   315
      ItemData        =   "frmCustomize.frx":005F
      Left            =   2450
      List            =   "frmCustomize.frx":006C
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   350
      Width           =   2115
   End
   Begin VB.ComboBox cmbIndents 
      Height          =   315
      ItemData        =   "frmCustomize.frx":008B
      Left            =   120
      List            =   "frmCustomize.frx":0092
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   350
      Width           =   2115
   End
   Begin VB.Label lblPointsBefore 
      Caption         =   "pts"
      Height          =   180
      Left            =   3800
      TabIndex        =   17
      Top             =   1700
      Width           =   360
   End
   Begin VB.Label lblPointsBetween 
      Caption         =   "pts"
      Height          =   180
      Left            =   3800
      TabIndex        =   16
      Top             =   1050
      Width           =   360
   End
   Begin VB.Label lblCaps 
      Caption         =   "All Caps:"
      Height          =   285
      Left            =   150
      TabIndex        =   12
      Top             =   3600
      Width           =   1215
   End
   Begin VB.Label lblDotLeaders 
      Caption         =   "Use Dot Leaders:"
      Height          =   285
      Left            =   150
      TabIndex        =   10
      Top             =   2900
      Width           =   3000
   End
   Begin VB.Label lblPageNumbers 
      Caption         =   "Include Page Numbers:"
      Height          =   285
      Left            =   150
      TabIndex        =   8
      Top             =   2200
      Width           =   3000
   End
   Begin VB.Label lblBefore 
      Caption         =   "Space Before New Level:"
      Height          =   285
      Left            =   150
      TabIndex        =   6
      Top             =   1450
      Width           =   3000
   End
   Begin VB.Label lblBetween 
      Caption         =   "Space Between Entries of Same Level:"
      Height          =   285
      Left            =   150
      TabIndex        =   4
      Top             =   800
      Width           =   3000
   End
   Begin VB.Label lblSpacing 
      Caption         =   "Line Spacing:"
      Height          =   285
      Left            =   2465
      TabIndex        =   2
      Top             =   150
      Width           =   1215
   End
   Begin VB.Label lblIndents 
      Caption         =   "Indents:"
      Height          =   285
      Left            =   150
      TabIndex        =   0
      Top             =   150
      Width           =   1215
   End
End
Attribute VB_Name = "frmCustomize"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bSave As Boolean
Private m_bCustom As Boolean

Property Let bCustom(bCustom As Boolean)
    m_bCustom = bCustom
End Property

Property Get bCustom() As Boolean
    bCustom = m_bCustom
End Property

Property Get bSave() As Boolean
    bSave = m_bSave
End Property

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
End Sub

Private Sub btnSave_Click()
    m_bSave = True
    Me.Hide
    DoEvents
End Sub

Private Sub cmbBefore_Click()
'if custom, load ini value;
'otherwise, disable and clear controls
    Dim bEnabled As Boolean
    Dim xBefore As String
    
    bEnabled = (cmbBefore = "Custom")

    Me.txtBefore.Enabled = bEnabled
    Me.lblPointsBefore.Enabled = bEnabled
    
    If bEnabled Then
        xBefore = xnulltostring(xAppGetUserINIValue("TOC", _
            "PointsBefore"))
        If xBefore = "" Then _
            xBefore = "0"
    End If
    Me.txtBefore = xBefore
End Sub

Private Sub cmbBetween_Click()
'if custom, load ini value;
'otherwise, disable and clear controls
    Dim bEnabled As Boolean
    Dim xBetween As String
    
    bEnabled = (cmbBetween = "Custom")

    Me.txtBetween.Enabled = bEnabled
    Me.lblPointsBetween.Enabled = bEnabled
    
    If bEnabled Then
        xBetween = xnulltostring(xAppGetUserINIValue("TOC", _
            "PointsBetween"))
        If xBetween = "" Then _
            xBetween = "0"
    End If
    Me.txtBetween = xBetween
End Sub

Private Sub cmbCaps_Click()
'if custom, load ini value;
'otherwise, disable and clear controls
    Dim bEnabled As Boolean
    Dim xDo As String
    
    bEnabled = (cmbCaps = "Custom")
    
    Me.chkCapLevelOne.Enabled = bEnabled
    Me.chkCapSubsequent.Enabled = bEnabled
    
    If bEnabled Then
        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
            "CapLevelOne"))
        Me.chkCapLevelOne = Val(xDo)
        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
            "CapSubsequent"))
        Me.chkCapSubsequent = Val(xDo)
    Else
        Me.chkCapLevelOne = 0
        Me.chkCapSubsequent = 0
    End If
End Sub

Private Sub cmbDotLeaders_Click()
'if custom, load ini value;
'otherwise, disable and clear controls
    Dim bEnabled As Boolean
    Dim xDo As String
    
    bEnabled = (cmbDotLeaders = "Custom")
    
    Me.chkDotLevelOne.Enabled = bEnabled
    Me.chkDotSubsequent.Enabled = bEnabled
    
    If bEnabled Then
        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
            "DotLevelOne"))
        Me.chkDotLevelOne = Val(xDo)
        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
            "DotSubsequent"))
        Me.chkDotSubsequent = Val(xDo)
    Else
        Me.chkDotLevelOne = 0
        Me.chkDotSubsequent = 0
    End If
End Sub

Private Sub cmbPageNumbers_Click()
'if custom, load ini value;
'otherwise, disable and clear controls
    Dim bEnabled As Boolean
    Dim xDo As String
    
    bEnabled = (cmbPageNumbers = "Custom")
    
    Me.chkPageLevelOne.Enabled = bEnabled
    Me.chkPageSubsequent.Enabled = bEnabled
    
    If bEnabled Then
        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
            "PageLevelOne"))
        Me.chkPageLevelOne = Val(xDo)
        xDo = xnulltostring(xAppGetUserINIValue("TOC", _
            "PageSubsequent"))
        Me.chkPageSubsequent = Val(xDo)
    Else
        Me.chkPageLevelOne = 0
        Me.chkPageSubsequent = 0
    End If
End Sub

Private Sub Form_Load()
    Dim ctlControl As Control
    
'   if using doc scheme, default controls to "use scheme";
'   if custom, setting listindex to 0 will get ini value
    For Each ctlControl In Me.Controls
        If TypeOf ctlControl Is ComboBox Then
            With ctlControl
                If Not m_bCustom Then _
                    .AddItem "-Use Scheme Definition-", 0
                .ListIndex = 0
            End With
        End If
    Next ctlControl
        
    If m_bCustom Then
        On Error Resume Next
'       this is the only dropdown w/real choices
        Me.cmbLineSpacing.ListIndex = _
            xAppGetUserINIValue("TOC", "LineSpacing")
        Me.chkDefaultScheme = _
            xAppGetUserINIValue("TOC", "DefaultToCustom")
    Else
        Me.chkDefaultScheme.Visible = False
    End If
End Sub
