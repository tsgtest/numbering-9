Attribute VB_Name = "mdlGlobal"
'**********************************************************
'   mpCOM.mdlGlobal
'   created 2/26/04 by Doug Miller
'
'**********************************************************
Option Explicit

Public Const mpEndOfValue As String = "�"
Public Const mpEndOfField As String = "|"
Public Const mpEndOfRecord As String = "�"
Public Const mpInsertUnderlinedTab As Integer = -1
Public Const mpNamespace As String = "urn-legalmacpac-data/10"
Public Const mpStartParaTag As String = "<w:p>"
Public Const mpEndParaTag As String = "</w:p>"
Public Const mpEmptyParaTag As String = "<w:p />"

'Important note: do not change the order of enumeration below -
'currently, our other components can only refer to these errors by their numbers
Public Enum mpErrors
    mpError_NotImplemented = vbError + 512 + 4000
    mpError_FailedDBConnection
    mpError_InvalidFieldCode
    mpError_FailedStoredProcedure
    mpError_FailedSQLStatement
    mpError_ItemNotInCollection
    mpError_MatchingItemNotInCollection
    mpError_InvalidData
    mpError_RecordUpdateFailed
    mpError_MissingObjectID
    mpError_NoUserName
    mpError_InvalidStringArrayElementIndex
    mpError_InvalidStringArrayRecordIndex
    mpError_InvalidStringArrayFieldIndex
    mpError_StringArrayTextNotSet
    mpError_StringArraySeparatorsNotUnique
    mpError_IncorrectParameterCount
    mpError_NamedItemExistsInCollection
    mpError_NumericItemKeyNotAllowed
    mpError_TableKeyViolation
    mpError_InvalidValue
    mpError_OwnerRequired
    mpError_InvalidPersonID
    mpError_InvalidUser
    mpError_EnumerationTypeMismatch
    mpError_InvalidObjectTypeID
    mpError_DocPropertyIDRequired
    mpError_InvalidDocPropertyID
    mpError_ControlIDRequired
    mpError_InvalidControlID
    mpError_UnknownLocaleID
    mpError_ItemNotEditable
    mpError_SaveRequired
    mpError_InvalidControlTypeID
    mpError_ItemCannotBeDeleted
    mpError_InvalidObjectID
    mpError_UserObjectAssignmentExists
    mpError_UserObjectAssignmentDoesNotExist
    mpError_NoPermission
    mpError_PublicPersonRequired
    mpError_InvalidAction
    mpError_InvalidEntityID
    mpError_InvalidKeySet
    mpError_InvalidOrMissingDefinitionObject
    mpError_InvalidAddressID
    mpError_InvalidField
    mpError_NoTranslationExists
    mpError_ItemAlreadyInCollection
    mpError_ApplicationNotRunning
    mpError_NoActiveWordDocument
    mpError_InvalidWordDocumentVariableName
    mpError_InvalidWordStyle
    mpError_InvalidWordBookmarkName
    mpError_NoAuthorsProvided
    mpError_InvalidObjectPropertyName
    mpError_InvalidExpression
    mpError_InvalidArgument
    mpError_InvalidPropertyName
    mpError_ObjectParameterIsNothing
    mpError_InvalidWordDocument
    mpError_InvalidWordPropertyReference
    mpError_InvalidWordObjectReference
    mpError_CallByNameFailed
    mpError_ErrorDetailLost
    mpError_InvalidEXECCode
    mpError_FileDoesNotExist
    mpError_CouldNotExecuteMacro
    mpError_InvalidDirectory
    mpError_BookmarkDoesNotExist
    mpError_CouldNotCreateCOMObject
    mpError_CouldNotRunMethod
    mpError_InvalidDocumentSection
    mpError_CouldNotInsertBoilerplate
    mpError_PropertyCountMismatch
    mpError_NoObjectPropertiesDefined
    mpError_LimitExceeded
    mpError_TypeIDRequired
    mpError_FieldCodeNotValidInCurrentContext
    mpError_ControlDoesNotHaveValue
    mpError_MissingParent
    mpError_ControlDoesNotHaveCaption
    mpError_ControlDoesNotSupportProperty
    mpError_ControlDoesNotSupportAction
    mpError_InvalidList
    mpError_InvalidUnprotectPassword
    mpError_PleadingCaptionsCollectionFull
    mpError_PleadingCounselsCollectionFull
    mpError_PleadingSignaturesCollectionFull
    mpError_InvalidXML
    mpError_CouldNotCreateXMLNode
    mpError_XMLNodeDoesNotExist
    mpError_InvalidOrMissingProgID
    mpError_InvalidAuthorIndex
    mpError_SubObjectDoesNotExist
    mpError_InvalidDocPropertyName
    mpError_InvalidSubObjectReference
    mpError_InvalidKeyword
    mpError_CouldNotSetProperty
    mpError_InvalidPositionInTable
    mpError_InvalidWordDocumentPropertyName
    mpError_CouldNotAddBorder
    mpError_InvalidDeleteScope
    mpError_InsertionLocationRequired
    mpError_Generic
    mpError_CannotInsertTableIntoTable
    mpError_MissingTable
End Enum

Public Sub RaiseError(ByVal xNewSource As String)
'raises the current error, appending the source
    Static xStack As String
    With Err
        'clear the stack if this is a
        'new error that's being raised
        If InStr(.Source, ".") = 0 And (.Source <> "mpError") Then
            xStack = Empty
        End If
        
        'append current source to stack
        xStack = xStack & xNewSource & ":"
        
        'raise the error with the updated stack
        If .Number <> 0 Then
            .Raise .Number, xStack, .Description
        Else
            .Raise mpError_ErrorDetailLost, xStack, "<Error_ErrorDetailLost>"
        End If
    End With
End Sub

Public Function SetXMLMarkupState(ByVal oDocument As Word.Document, _
                                  ByVal bShowXML As Boolean) As Long
' There is a bug in Word 2007 that causes the ShowAll state to be changed when
' opening a new document if ShowXMLMarkup is changed to not match ShowAll
' Need to toggle ShowAll to reset the correct state
    Const mpThisFunction As String = "mpnWordXML.mdlGlobal.SetXMLMarkupState"
    
    On Error GoTo ProcError
    
    With oDocument.ActiveWindow.View
        If .ShowXMLMarkup <> bShowXML Then
            .ShowXMLMarkup = bShowXML
            SetXMLMarkupState = -1
        End If
    End With
    
    Exit Function
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Function
End Function

