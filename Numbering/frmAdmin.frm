VERSION 5.00
Begin VB.Form frmAdmin 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   1740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4500
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAdmin.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1740
   ScaleWidth      =   4500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnMakePublic 
      Caption         =   "Make Public"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   135
      Width           =   1215
   End
   Begin VB.Label lblStatus 
      Caption         =   "Please wait while function executes."
      Height          =   315
      Left            =   1530
      TabIndex        =   1
      Top             =   225
      Visible         =   0   'False
      Width           =   3105
   End
End
Attribute VB_Name = "frmAdmin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ConvertPrivateToPublic()
'sets origin field of all MacPac schemes in
'public mpNumbers.sty to be mpSchemeType_Public
    Dim ltP As Word.ListTemplate
    Dim oPublicSty As Word.Document
    Dim xPublicSty As String
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    xPublicSty = App.Path & "\mpNumbers.sty"
    On Error Resume Next
    
'   open public sty file
    Set oPublicSty = Word.Documents.Open(xPublicSty)
    
    If (oPublicSty Is Nothing) Then
'       file could not be opened
        xMsg = "Could not open the file '" & xPublicSty & "'.  " & _
            "Please ensure that both mpNumbers.sty and MPN90Admin.exe " & _
            "are located in the numbering 'App' directory."
        MsgBox xMsg, vbExclamation, App.Title
        Exit Sub
    Else
        Word.AddIns(xPublicSty).Installed = False
    End If
    
    On Error GoTo ProcError
    Screen.MousePointer = vbHourglass
    
    For Each ltP In ActiveDocument.ListTemplates
        If mpBase.bIsMPListTemplate(ltP) Then
            mpBase.SetField ltP.Name, _
                            mpRecField_Origin, _
                            mpSchemeType_Public, , _
                            ActiveDocument
        End If
    Next ltP
    
    oPublicSty.Close wdSaveChanges
    Word.AddIns(xPublicSty).Installed = True
    Screen.MousePointer = vbDefault
    MsgBox "Schemes are now public.", vbInformation, App.Title
    
    Exit Sub
ProcError:
    MsgBox "The following error occurred while converting to public schemes:" & vbCr & _
        Err.Number & " :: " & Err.Description & " :: ConvertPrivateToPublic", vbCritical, App.Title
    Me.MousePointer = vbDefault
    Exit Sub
End Sub



Private Sub btnGetOrigin_Click()
    ShowOrigin
End Sub

Private Sub btnMakePublic_Click()
    Me.lblStatus.Visible = True
    ConvertPrivateToPublic
    Me.lblStatus.Visible = False
End Sub

Private Sub ShowOrigin()
    MsgBox mpBase.GetField("zzmpArticle", mpRecField_Origin, , ActiveDocument)
End Sub

Private Sub Form_Load()
    Me.Caption = App.Title
End Sub

