VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNumScheme"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CNumScheme Collection Class
'   created 6/3/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   top level data for a numbering scheme
'**********************************************************

Private m_xName As String
Private m_xAlias As String
Private m_iOrigin As mpSchemeTypes
Private m_iType As mpSchemeTypes
Private m_xTOCScheme As String
Private m_iDynamicFonts As Integer
Private m_iDynamicSpacing As Integer
Private m_xDescription As String

'**********************************************************
'Properties
'**********************************************************
Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
Attribute Name.VB_UserMemId = 0
    Name = m_xName
End Property

Public Property Let Alias(xNew As String)
    m_xAlias = xNew
End Property

Public Property Get Alias() As String
    Alias = m_xAlias
End Property

Public Property Let TOCScheme(xNew As String)
    m_xTOCScheme = xNew
End Property

Public Property Get DynamicFonts() As Integer
    DynamicFonts = m_iDynamicFonts
End Property

Public Property Let DynamicFonts(xNew As Integer)
    m_iDynamicFonts = xNew
End Property

Public Property Get DynamicSpacing() As Integer
    DynamicSpacing = m_iDynamicSpacing
End Property

Public Property Let DynamicSpacing(xNew As Integer)
    m_iDynamicSpacing = xNew
End Property

Public Property Get TOCScheme() As String
    TOCScheme = m_xTOCScheme
End Property

Public Property Let SchemeType(iNew As mpSchemeTypes)
    m_iType = iNew
End Property

Public Property Get SchemeType() As mpSchemeTypes
    SchemeType = m_iType
End Property

Public Property Let Origin(iNew As mpSchemeTypes)
    m_iOrigin = iNew
End Property

Public Property Get Origin() As mpSchemeTypes
    Origin = m_iOrigin
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

