VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cContentControls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function BeforeBlockLevelCC() As Boolean
'returns true if immediately before a block-level content control
    Const mpThisFunction As String = "mpnCC.cContentControls.BeforeBlockLevelCC"
    Dim oRange As Word.Range
    Dim oCC As Word.ContentControl
    Dim lParaEnd As Long
    
    On Error Resume Next
    Set oRange = Selection.Next(wdCharacter)
    On Error GoTo ProcError
    If oRange Is Nothing Then Exit Function
    
    Set oRange = Selection.Range
    oRange.Move
    Set oCC = oRange.ParentContentControl
    If Not oCC Is Nothing Then
        If oRange.Start = oCC.Range.Start Then
            lParaEnd = Selection.Paragraphs(1).Range.End - 1
            If oCC.Range.End < lParaEnd Then
                oRange.SetRange oCC.Range.End, lParaEnd
                BeforeBlockLevelCC = (oRange.Text = "")
            Else
                BeforeBlockLevelCC = True
            End If
        End If
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Public Function AfterBlockLevelCC() As Boolean
'returns true if immediately after a block-level content control
    Const mpThisFunction As String = "mpnCC.cContentControls.AfterBlockLevelCC"
    Dim oRange As Word.Range
    Dim oCC As Word.ContentControl
    Dim lParaStart As Long
    
    On Error GoTo ProcError
    
    Set oRange = Selection.Range
    If oRange.Start = 0 Then Exit Function
    oRange.Move wdCharacter, -1
    Set oCC = oRange.ParentContentControl
    If Not oCC Is Nothing Then
        If oRange.End = oCC.Range.End Then
            lParaStart = Selection.Paragraphs(1).Range.Start
            If oCC.Range.Start > lParaStart Then
                oRange.SetRange lParaStart, oCC.Range.Start
                AfterBlockLevelCC = (oRange.Text = "")
            Else
                AfterBlockLevelCC = True
            End If
        End If
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Public Function GetCCSafeParagraphStart(ByVal oInsertionRange As Word.Range) As Long
'gets "start of paragraph" insertion location, ensuring that inside any multiparagraph
'content controls that would otherwise force a new paragraph to be created above the controls()
    Const mpThisFunction As String = "mpnCC.cContentControls.GetCCSafeParagraphStart"
    Dim i As Integer
    Dim rngPara As Word.Range
    Dim iCount As Integer
    Dim oCC As Word.ContentControl
    
    On Error GoTo ProcError
    
    Set rngPara = oInsertionRange.Paragraphs(1).Range
    
    'count multiparagraph tags at start of paragraph
    For i = 1 To rngPara.ContentControls.Count
        Set oCC = rngPara.ContentControls(i)
        If (oCC.Range.Start = rngPara.Start + i) And _
                (oCC.Range.Paragraphs.Count > 1) Then
            iCount = iCount + 1
        Else
            Exit For
        End If
    Next i
    
    GetCCSafeParagraphStart = rngPara.Start + iCount
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Public Function GetCCSafeParagraphEnd(ByVal oInsertionRange As Word.Range) As Long
'gets "end of paragraph" insertion location, ensuring that inside any multiparagraph
'content controls that would otherwise force a new paragraph to be created below the control(s)
    Const mpThisFunction As String = "mpnCC.cContentControls.GetCCSafeParagraphEnd"
    Dim i As Integer
    Dim rngPara As Word.Range
    Dim iCount As Integer
    Dim oCC As Word.ContentControl
    
    On Error GoTo ProcError
    
    Set rngPara = oInsertionRange.Paragraphs(1).Range
    With rngPara
'        'move to end of paragraph
        .MoveEnd wdCharacter, -1
    
        'count multiparagraph tags at start of paragraph
        'mp10 GLOG 3914 - instead of # of paragraphs, check tag level - we
        'should be staying inside of and preserving all block tags
        For i = 1 To .ContentControls.Count
            Set oCC = .ContentControls(i)
            If (oCC.Range.End = .End - i) And _
                    (oCC.Range.Paragraphs.Count > 1) Then
                iCount = iCount + 1
            Else
                Exit For
            End If
        Next i
    End With
    
    GetCCSafeParagraphEnd = rngPara.End - iCount
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Public Function CountCCsEndingInRange(ByVal oRange As Word.Range) As Integer
'returns the number of content controls in the range that end before the end of the range
    Const mpThisFunction As String = "mpnCC.cContentControls.CountCCsEndingInRange"
    Dim i As Integer
    Dim iCount As Integer

    On Error GoTo ProcError
    
    For i = 1 To oRange.ContentControls.Count
        If oRange.ContentControls(i).Range.End < oRange.End Then _
            iCount = iCount + 1
    Next i
    
    CountCCsEndingInRange = iCount
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Public Function AddUnlinkedParagraphStyle(ByVal oDoc As Word.Document, _
                                          ByVal xName As String) As Word.Style
'creates style of wdStyleTypeParagraphOnly type, which was introduced in Word 2007
    Const mpThisFunction As String = "mpnCC.cContentControls.AddUnlinkedParagraphStyle"
    On Error GoTo ProcError
    Set AddUnlinkedParagraphStyle = oDoc.Styles.Add(xName, wdStyleTypeParagraphOnly)
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Public Function GetPositionAfterInlineCC(ByVal oRange As Word.Range) As Long
'if specified range is inside an inline content control,
'returns the position immediately after it
    Const mpThisFunction As String = "mpnCC.cContentControls.GetPositionAfterInlineCC"
    Dim oCC As Word.ContentControl
    Dim lPos As Integer
    
    On Error GoTo ProcError
    
    Set oCC = oRange.ParentContentControl
    If Not oCC Is Nothing Then
        If ContentControlIsInline(oCC) Then
            lPos = oCC.Range.End + 1
        End If
    End If
    
    GetPositionAfterInlineCC = lPos
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Private Function ContentControlIsInline(ByVal oCC As Word.ContentControl) As Boolean
'returns TRUE if functionally inline, i.e. contains no text, or other content controls
'that start and end before or after it in the paragraph
    Const mpThisFunction As String = "mpnCC.cContentControls.ContentControlIsInline"
    Dim oParaRange As Word.Range
    Dim oCC2 As Word.ContentControl
    Dim oTagRange As Word.Range
    Dim oPrecedingRange As Word.Range
    Dim oTrailingRange As Word.Range
    
    On Error GoTo ProcError
    
    Set oTagRange = oCC.Range
    
    'check whether oCC contains multiple paragraphs
    If oTagRange.Paragraphs.Count > 1 Then
        ContentControlIsInline = False
        Exit Function
    ElseIf oTagRange.Characters.Last = vbCr Then
        'GLOG 4891 (dm) - the above test will miss a 2-paragraph
        'cc in which the second paragraph is empty
        ContentControlIsInline = False
        Exit Function
    End If
    
    Set oParaRange = oTagRange.Paragraphs(1).Range
    
    'check whether there's preceding text in the paragraph
    Set oPrecedingRange = oTagRange.Duplicate
    oPrecedingRange.SetRange oParaRange.Start, oTagRange.Start - 1
    If oPrecedingRange.Text <> "" Then
        ContentControlIsInline = True
        Exit Function
    End If
    
    'check whether there's trailing text in the paragraph
    Set oTrailingRange = oTagRange.Duplicate
    oTrailingRange.SetRange oTagRange.End + 1, oParaRange.End - 1
    If oTrailingRange.Text <> "" Then
        ContentControlIsInline = True
        Exit Function
    End If
    
    'check whether there are preceding tags in the paragraph
    For Each oCC2 In oPrecedingRange.ContentControls
        If oCC2.Range.End < oPrecedingRange.End Then
            ContentControlIsInline = True
            Exit Function
        End If
    Next oCC2
    
    'check whether there are trailing tags in the paragraph
    For Each oCC2 In oTrailingRange.ContentControls
        If oCC2.Range.Start > oTrailingRange.Start Then
            ContentControlIsInline = True
            Exit Function
        End If
    Next oCC2
    
    ContentControlIsInline = False
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Public Sub ToggleRibbon()
    ActiveWindow.ToggleRibbon
End Sub

Public Function AddTempParaToContentControl(ByVal oRange As Word.Range) As Boolean
'if oRange ends in a paragraph that ends with a content control,
'adds a temp para and returns TRUE
    Const mpThisFunction As String = "mpnCC.cContentControls.AddTempParaToContentControl"
    Dim oCC As Word.ContentControl
    Dim lEnd As Long
    Dim oParaRange As Word.Range
    
    On Error GoTo ProcError
    
    Set oParaRange = oRange.Paragraphs.Last.Range
    lEnd = oParaRange.End - 2
    oParaRange.SetRange lEnd, lEnd
    
    Set oCC = oParaRange.ParentContentControl
    While Not oCC Is Nothing
        If oCC.Range.End = lEnd Then
            oCC.Range.EndOf
            oCC.Range.InsertAfter vbCr
            AddTempParaToContentControl = True
            Exit Function
        Else
            Set oCC = oCC.ParentContentControl
        End If
    Wend
    
    AddTempParaToContentControl = False
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

Public Function COMAddInIsLoaded(ByVal xName As String) As Boolean
'added in 9.9.6006 for GLOG 5551
    Const mpThisFunction As String = "mpnCC.cContentControls.COMAddInIsLoaded"
    Dim oAddIn As COMAddIn
    
    On Error Resume Next
    Set oAddIn = Application.COMAddIns(xName)
    On Error GoTo ProcError
    
    If Not oAddIn Is Nothing Then
        COMAddInIsLoaded = oAddIn.Connect
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function
