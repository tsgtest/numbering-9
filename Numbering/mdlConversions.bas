Attribute VB_Name = "mdlConversion"
Option Explicit
Private Const mpRetainHeadingDefaults As Integer = 9999
Private xTimeMsg As String

'*****************************************
'contains conversion functions for MPN90
'*****************************************

Sub DoConversions()
'performs any necessary numbering
'conversions on current doc - there
'are 3 types of conversion that could
'occur:
'1)converting a doc from MacPac Numbering 8.0 to 9.0
'2)converting a 9.0 doc to latest method of storing scheme props
'3)converting MS Word Heading 1-9 scheme to a MacPac heading scheme

    Dim x80Scheme As String
    Dim x90Scheme As String
    Dim bRet As Boolean
    Dim xLastEditMSWordVer As String
    Dim bIsClean As Boolean
    Dim iDocSchemes As Integer
    Dim styScheme As Word.Style
    
'    xTimeMsg = "Started at " & Now & vbCr
    
    If ActiveDocument.Name = "mpNumbers.sty" Then _
        Exit Sub
    
    With Word.ActiveDocument
        On Error Resume Next
'       get MacPac Numbering 8.0 scheme
        x80Scheme = .Variables(mpActiveScheme80DocVar)
'       get MacPac Numbering 9.0 scheme
        x90Scheme = .Variables(mpActiveSchemeDocVar)
        On Error GoTo 0
    End With
    
    If (x80Scheme = Empty) And (x90Scheme = Empty) Then
'       if there are 9.0 schemes, ensure that one is active;
'       this is for MacPac 2K reuse
        iDocSchemes = iGetSchemes(g_xDSchemes(), _
            mpSchemeType_Document)
        If iDocSchemes > 0 Then
            x90Scheme = "1" & g_xDSchemes(0, 0)
            bSetSelectedScheme ActiveDocument, x90Scheme
        End If
    ElseIf (x80Scheme <> Empty) And (x90Scheme <> Empty) Then
        If Not bListTemplateExists(Mid(x90Scheme, 2)) Then
'           ensure that 9.0 variable represents a bona fide scheme
'           before determining that 8.0 to 9.0 conversion need not run
            If Not bListTemplateExists(Mid(x90Scheme, 6)) Then
                ActiveDocument.Variables(mpActiveSchemeDocVar).Delete
                x90Scheme = ""
            End If
        ElseIf (Mid(x90Scheme, 2) = "HeadingStyles") Then
'           if the active 9.0 scheme is "Heading", and the active
'           8.0 scheme is something else (meaning that "Heading" has
'           never actually been used), give conversion another chance
            On Error Resume Next
            If Right(x80Scheme, 9) <> "ingStyles" Then
                ActiveDocument.Variables(mpActiveSchemeDocVar).Delete
                x90Scheme = ""
            End If
            On Error GoTo 0
        End If
    End If
    
    If (x80Scheme <> Empty) And (x90Scheme = Empty) Then
'       doc is MacPac Numbering 8.0 -
'       convert to MacPac Numbering 9.0 - will
'       convert any native Heading 1-9 schemes
'       to a MacPac Heading Styles scheme
        LoadStyFiles
        ConvMPN80To90
        UnLoadStyFiles
        bIsClean = True
    ElseIf x90Scheme <> Empty Then
'       do upgrade/maintenance of existing 9.0 schemes
        UpdateMPListTemplates
        bIsClean = g_bResetFonts
    Else
'       convert any native Heading 1-9 scheme
'       to a MacPac Heading Styles scheme
        bRet = ConvertWordHeadingStyles()
        If bRet Then ActiveDocument.UndoClear
        bIsClean = True
    End If
    
'   flag not to rerun fonts cleanup
    On Error Resume Next
    If bIsClean Then _
        ActiveDocument.Variables.Add mpLTFontsClean, "True"
    On Error GoTo 0
    
'   set word heading styles as active scheme
'   if no active scheme exists in doc
    If Len(x90Scheme) = 0 And bRet Then
        Word.ActiveDocument _
            .Variables(mpActiveSchemeDocVar) = "3HeadingStyles"
        x90Scheme = "3HeadingStyles"
    End If
    
'   the following is to compensate for improperly added 9.0
'   variable - if no list template corresponding to active scheme,
'   doc was not properly converted and numbering functions
'   should not be available
    On Error Resume Next
    If x90Scheme <> Empty Then
        If Not bListTemplateExists(Mid(x90Scheme, 2)) Then
            With ActiveDocument
                .Variables(mpActiveSchemeDocVar).Delete

'               this is a patch for MP2K reuse, create new document;
'               if no numbered paragraphs are selected, variables will
'               be copied to new document without the scheme itself;
'               only delete 8.0 variable if corresponding styles are
'               missing - list template may be missing for a different
'               reason (see bRepairActive80Scheme)
                If x80Scheme <> Empty Then
                    Set styScheme = .Styles(x80Scheme & "_L1")
                    If styScheme Is Nothing Then
                        .Variables(mpActiveScheme80DocVar).Delete
                    End If
                End If
            End With
        End If
    End If
    On Error GoTo 0
    
'   force colors to print as black on non-color printers
    On Error Resume Next
    If iHeadingColor() Then _
        Word.ActiveDocument.Compatibility(wdPrintColBlack) = True
    On Error GoTo 0
        
'   mark doc as converted
    MarkAsConverted

'    xTimeMsg = xTimeMsg & "Finished at = " & Now & vbCr
'    Selection.InsertAfter xTimeMsg

End Sub

Sub ConvWordVersions()
'converts a document that was last
'edited in a different version of Word
    RestoreProps
End Sub

Public Function ConvMPN80To90() As Long
'converts a MacPac Numbering 80 document
'to MacPac Numbering 90
    Dim o80Sty As Word.Template
    Dim ltP As Word.ListTemplate
    Dim xScheme As String
    Dim styP As Word.Style
    Dim ltFirm As Word.ListTemplate
    Dim i As Integer
    Dim xStyL1 As String
    Dim x90Scheme As String
    Dim iLevels As Integer
    Dim bIsActive As Boolean
    Dim iHFormat As Integer
    Dim xLT As String
    Dim j As Integer
    Dim xProps As String
    
    On Error GoTo ProcError
    
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "Convertir la num�rotation du document � la num�rotation MacPac v9.0"
    Else
        Application.StatusBar = "Converting Document Numbering to " & _
                                "MacPac Numbering v9.0"
    End If
    Application.ScreenUpdating = False
    
'   cycle through list templates
    For Each ltP In Word.ActiveDocument.ListTemplates
'       get scheme
        xScheme = ltP.Name
        
'       before copying props from sty file, see if 9.0 backups are in doc -
'       9.0 prefixes may have been stripped for 8.0 usability; starting with v.9.7.7,
'       mpNumShare80.dot deletes 9.x active scheme variable to force this routine
'       to run again; this is in an effort to catch newly added 8.x schemes
        If xScheme <> "" Then
            xProps = ""
            
            On Error Resume Next
            xProps = ActiveDocument.Variables("zzmp" & xScheme).Value
            On Error GoTo 0
                
            If xProps <> "" Then
                xScheme = "zzmp" & xScheme
'               first ensure that there's not already a piped version of scheme in doc
                If xGetFullLTName(xScheme) = xScheme Then
                    AddPropsToLTName ltP, ActiveDocument, xScheme
                End If
                GoTo lab_NextLT
            End If
        End If
        
'       determine whether it's the active scheme
        bIsActive = _
            (ActiveDocument.Variables(mpActiveScheme80DocVar) = xScheme)
        
'       check for L1 style of MacPac form
'       if there is no scheme name - this
'       will correct customized MacPac schemes
'       that have incorrectly used a non-named
'       list template
        If Len(xScheme) = 0 Then
'           get name of level 1 linked style
            xStyL1 = ltP.ListLevels(1).LinkedStyle
'           check for correct MacPac form (ie XXX_LY)
            If Len(xStyL1) > 3 Then
                If Right(xStyL1, 3) = "_L1" Then
'                   style name has MacPac form -
'                   change name of List template
'                   to the characters to the left of "_L1"
                    ltP.Name = Left(xStyL1, Len(xStyL1) - 3)
                    xScheme = ltP.Name
                End If
            End If
        End If
        
'       if still no name, go to next list template
        If Len(xScheme) = 0 Then _
            GoTo lab_NextLT
            
'       test for corresponding public scheme
        On Error Resume Next
        Set ltFirm = Templates(g_xFNumSty) _
            .ListTemplates(mpPrefix & xScheme)
        Err.Clear
            
'       if no corresponding public scheme,
'       test for corresponding scheme in
'       backup 80 sty file.
        If (ltFirm Is Nothing) And (Dir(g_xFNum80Sty) <> "") Then
            On Error Resume Next
'           load 80 public sty file if not already loaded
'            Set o80Sty = Word.Templates(g_xFNum80Sty)
            If o80Sty Is Nothing Then
                Word.AddIns.Add g_xFNum80Sty, True
                Set o80Sty = GetTemplate(g_xFNum80Sty)
            End If
            Err.Clear
            
'           test for corresponding public scheme
            Set ltFirm = o80Sty _
                .ListTemplates(mpPrefix & xScheme)
            Err.Clear
        End If
            
'       do if not already a MacPac scheme and there's
'       a corresponding MacPac public scheme
        If Not (bIsMPListTemplate(ltP) Or _
                (ltFirm Is Nothing)) Then
'           if level one style is missing, delete list template
            Set styP = Nothing
            On Error Resume Next
            Set styP = ActiveDocument.Styles(xScheme & "_L1")
            On Error GoTo ProcError
            If styP Is Nothing Then
                ltP.Name = ""
                GoTo lab_NextLT
            End If
        
'           add macpac prefix if necessary
            If Left(xScheme, 4) <> mpPrefix Then
'               prefix scheme with 'zzmp'
                xScheme = mpPrefix & xScheme
            End If
            
'           build list template name
            xLT = ""
            For i = 1 To 9
                xLT = xLT & xGetNumLevelProps(ltFirm, i)
            Next i
            ltP.Name = xScheme & "|" & xLT & "|"
            
'           update props to match customized scheme
            iLevels = iGetLevels(xScheme, mpSchemeType_Document)
            For i = 1 To iLevels
                Set styP = Nothing
                On Error Resume Next
                Set styP = ActiveDocument.Styles(xGetStyleName(xScheme, i))
                On Error GoTo ProcError
                If styP Is Nothing Then
'                   doc scheme has less levels than public version
                    For j = i To iLevels
                        lSetLevelProps xScheme, _
                                       j, _
                                       mpSchemeType_Document, _
                                       PropertyNotAvailable
                    Next j
                    Exit For
                Else
'                   outline levels were assigned in generic 8.0,
'                   but we've seen some mpNumbers.stys where they weren't
                    styP.ParagraphFormat.OutlineLevel = i
                    
'                   attempt to match customized format;
'                   can only do this for active scheme
                    If bIsActive Then
                        iHFormat = iGetHeadingFormats(xScheme, i)
                        If iHFormat <> mpRetainHeadingDefaults Then
                            lSetLevelProp xScheme, _
                                          i, _
                                          mpNumLevelProp_HeadingFormat, _
                                          Trim(Str(iHFormat)), _
                                          mpSchemeType_Document
                        End If
                    End If
                End If
            Next i
        End If
        
        If Not ltFirm Is Nothing Then
'           backup scheme properties in case the user
'           writes over them using the Word UI - this is
'           also used when docs are opened in 97, then 2000,
'           and visa versa.
            BackupProps ltFirm.Name
            Set ltFirm = Nothing
        End If
lab_NextLT:
    Next ltP
    
'   add 'zzmpFixedCurScheme_9.0' doc variable and fill
'   with 'zzmp' & name of cur scheme 8.0 doc.
    x90Scheme = mpPrefix & ActiveDocument.Variables _
        .Item(mpActiveScheme80DocVar)
    If bListTemplateExists(x90Scheme) Then
        bSetSelectedScheme ActiveDocument, "1" & x90Scheme
        ActiveDocument.UndoClear
    End If
         
'   unload 80 sty as add-in if currently loaded
    If Not (o80Sty Is Nothing) Then
        Word.AddIns(g_xFNum80Sty).Delete
    End If
    
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    mdlApplication.SendShiftKey
    Exit Function
    
ProcError:
    ConvMPN80To90 = Err.Number
    RaiseError "numFunctions.ConvMPN80To90"
    Exit Function
End Function

Public Function bRepairActive80Scheme() As Boolean
'if scheme is available and user approves, attempts to
'convert formally active 8.0 scheme with missing
'list template - this occurs in Word 97 documents
'that are later saved in an earlier version of Word
'Returns TRUE unless user disapproves
    Dim x80Scheme As String
    Dim x90Scheme As String
    Dim xMsg As String
    Dim ltFirm As Word.ListTemplate
    Dim rngLocation As Word.Range
    
    Application.ScreenUpdating = False
    
    With Word.ActiveDocument
        On Error Resume Next
'       get MacPac Numbering 8.0 scheme
        x80Scheme = .Variables(mpActiveScheme80DocVar)
'       get MacPac Numbering 9.0 scheme
        x90Scheme = .Variables(mpActiveSchemeDocVar)
        On Error GoTo 0
    End With
    
    If (x80Scheme <> Empty) And (x90Scheme = Empty) Then
'       we should only be here if list template is
'       missing, but double check
        x90Scheme = mpPrefix & x80Scheme
        If Not bListTemplateExists(x90Scheme) Then
'           ensure that sty files are loaded
            LoadStyFiles
            
'           test for corresponding public scheme
            On Error Resume Next
            Set ltFirm = Templates(g_xFNumSty) _
                .ListTemplates(x90Scheme)
            On Error GoTo 0
            
            If ltFirm Is Nothing Then
'               display message
                If g_lUILanguage = wdFrenchCanadian Then
                    xMsg = "Le th�me de document " & x80Scheme & " ne puet pas �tre charg� parce qu'il ne peut plus �tre li� � la liste de mod�les.  D�sirez-vous charg� un autre th�me ?"
                Else
                    xMsg = "The " & x80Scheme & " document scheme could not be loaded " & _
                        "because it can no longer be linked to its list template.  " & _
                        "Would you like to load a different scheme?"
                End If
                lRet = MsgBox(xMsg, vbYesNo + vbInformation, g_xAppName)
                If lRet = vbNo Then
                    UnLoadStyFiles
                    Exit Function
                End If
            Else
'               load 9.0 public scheme
                iLoadScheme ActiveDocument, _
                            x90Scheme, _
                            mpSchemeType_Public

'               ensure that scheme is correctly linked
                bRelinkScheme x90Scheme, _
                            mpSchemeType_Document, _
                            False, False, False

'               backup scheme properties in case the user
'               writes over them using the Word UI
                BackupProps x90Scheme
    
'               set scheme of active doc
                bSetSelectedScheme ActiveDocument, _
                    "1" & x90Scheme
                    
'               test for need to run Change
                Set rngLocation = ActiveDocument.Content
                With rngLocation.Find
                    .ClearFormatting
                    .Format = True
                    .Style = xGetStyleRoot(x90Scheme) & "_L1"
                    .Text = ""
                    .Execute
                    If .Found Then
                        If rngLocation.ListFormat.ListTemplate _
                                .Name <> x90Scheme Then
                            iChangeScheme x90Scheme, _
                                        mpSchemeType_Public, _
                                        "", _
                                        True, _
                                        bDisplayMsg:=False
                        End If
                    End If
                End With
            End If
        End If
    End If
    
    bRepairActive80Scheme = True
End Function

Function iGetHeadingFormats(xScheme As String, iLevel As Integer) As Integer
'returns an integer defining the heading formats
'for scheme xScheme level iLevel

    Dim styP As Word.Style
    Dim iFormat As Integer
    Dim xStyle As String
    
    xStyle = xGetStyleRoot(xScheme)
    
    On Error Resume Next
    Set styP = Word.ActiveDocument.Styles(xStyle & "_L" & iLevel)
    On Error GoTo 0
    If Not (styP Is Nothing) Then
        If styP.Font.Bold Then _
            iFormat = iFormat Or mpTCFormatField_Bold
        If styP.Font.Italic Then _
            iFormat = iFormat Or mpTCFormatField_Italic
        If styP.Font.Underline Then _
            iFormat = iFormat Or mpTCFormatField_Underline
        If styP.Font.AllCaps Then _
            iFormat = iFormat Or mpTCFormatField_AllCaps
        If styP.Font.SmallCaps Then _
            iFormat = iFormat Or mpTCFormatField_SmallCaps
            
        If iFormat <> 0 Then
'           there are formats held in the _LX style -
'           format are thus stored in the styles themselves-
'           set bit that represents this
            iFormat = iFormat Or mpTCFormatField_Type
        Else
'           check TCEntryLX style
            On Error Resume Next
            Set styP = Nothing
            Set styP = Word.ActiveDocument.Styles("zzmpTCEntryL" & iLevel)
            On Error GoTo 0
            If styP Is Nothing Then
'               if TC styles are missing,
'               flag to retain public defaults
                iFormat = mpRetainHeadingDefaults
            Else
                If styP.Font.Bold Then _
                    iFormat = iFormat Or mpTCFormatField_Bold
                If styP.Font.Italic Then _
                    iFormat = iFormat Or mpTCFormatField_Italic
                If styP.Font.Underline Then _
                    iFormat = iFormat Or mpTCFormatField_Underline
                If styP.Font.AllCaps Then _
                    iFormat = iFormat Or mpTCFormatField_AllCaps
                If styP.Font.SmallCaps Then _
                    iFormat = iFormat Or mpTCFormatField_SmallCaps
            End If
        End If
    End If
    iGetHeadingFormats = iFormat
End Function

Public Function AddPropsToLTName(ltScheme As Word.ListTemplate, _
                                 oSource As Object, _
                                 Optional xScheme As String) As Long
    Dim bIsSourceFile As Boolean
    Dim i As Integer
    Dim xDetail As String
    Dim iDblPipes As Integer
    
    On Error GoTo ProcError
    
    If xScheme = "" Then _
        xScheme = ltScheme.Name
    
    On Error Resume Next
    bIsSourceFile = Not (oSource _
        .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
    If Not bIsSourceFile Then _
        xDetail = ActiveDocument.Variables(xScheme).Value
    On Error GoTo ProcError
    
    If xDetail = "" Then
        For i = 1 To 9
            If bIsSourceFile Then
                xDetail = xDetail & oSource _
                    .CustomDocumentProperties(xScheme & i).Value
            Else
                xDetail = xDetail & _
                    xGetNumLevelProps(ltScheme, i, True)
            End If
'           if first level is empty, no reason to check others
            If xDetail = "" Then Exit For
        Next i
        xDetail = "|" & xDetail & "|"
    ElseIf (Right(xDetail, 1) = "*") Or _
            (Right(xDetail, 1) = "z") Then
'       strip 9.7 new scheme flags
        xDetail = Left(xDetail, Len(xDetail) - 1)
    End If
    
    iDblPipes = lCountChrs(xDetail, "||")
    If iDblPipes = 10 Then
        ltScheme.Name = xScheme & xDetail
    ElseIf (iDblPipes > 1) And _
            (iDblPipes < 10) And _
            (InStr(xDetail, "|mpNA|") = 0) Then
'       added 3/23/01 to account for doc variables that were
'       created before we started using "mpNA" for unused levels
        For i = 1 To 10 - iDblPipes
            xDetail = xDetail & "mpNA||"
        Next i
        ltScheme.Name = xScheme & xDetail
        BackupProps xScheme
    ElseIf ltScheme.Name = "HeadingStyles" Then
'       scheme props couldn't be found;
'       ConvertWordHeadingStyles no longer runs on
'       every doc change, so call here
        ConvertWordHeadingStyles
    End If

    Exit Function
ProcError:
    If Err.Number = mpErrListNumNameInUse Then
'       list name is unusable - modify slightly
        ltScheme.Name = ModifyUnusableListName(xScheme & xDetail)
        
'       backup new name
        BackupProps xScheme
    Else
        RaiseError "mdlConversion.ModifyUnusableListName"
    End If
    Exit Function
End Function

Public Function UpdateMPListTemplates() As Long
'   rename list templates; clean up font substitution; prevent reversion of
'   scheme indents edited in Word 97
    Dim ltScheme As Word.ListTemplate
    Dim xScheme As String
    Dim xProps As String
    Dim bIsClean As Boolean
    
'    xTimeMsg = xTimeMsg & "Start renaming at " & Now & vbCr
    
'   reset fonts only if 1) specified in ini; 2) hasn't already been
'   done; and 3) doc currently requires font substitution
    On Error Resume Next
    bIsClean = Not g_bResetFonts
    bIsClean = (bIsClean Or _
        (ActiveDocument.Variables(mpLTFontsClean) = "True"))
    bIsClean = (bIsClean Or _
        (Dialogs(wdDialogFontSubstitution).unavailablefont = ""))
    On Error GoTo 0
        
    If Not bIsClean Then
        If g_lUILanguage = wdFrenchCanadian Then
            Application.StatusBar = "Mise � jour de la num�rotation dans ce document pour une efficacit� am�lior�e.  Veuillez patienter..."
        Else
            Application.StatusBar = "Updating numbering in this document for improved " & _
                "efficiency.  Please wait..."
        End If
    End If
    
'   rename and clean list templates
    For Each ltScheme In ActiveDocument.ListTemplates
        If ltScheme.Name <> "" Then
'           restore 9.0 prefixes that were stripped for 8.0 usability
            xProps = ""
            xScheme = "zzmp" & ltScheme.Name
            
            On Error Resume Next
            xProps = ActiveDocument.Variables(xScheme).Value
            On Error GoTo 0
            
            If xProps <> "" Then
                ltScheme.Name = xScheme
            End If
        
'           validate
            If Right(ltScheme.Name, 2) = "|z" Then
'               this is a new list template created in PreventLTReversion;
'               strip flag and proceed to next scheme
                ltScheme.Name = Left(ltScheme.Name, Len(ltScheme.Name) - 1)
            ElseIf bIsMPListTemplate(ltScheme) Then
                If ltScheme.ListLevels.Count <> 9 Then
'                   list template has less than nine levels
                    ltScheme.Name = ""
                ElseIf InStr(ltScheme.Name, "|") = 0 Then
                    If xGetFullLTName(ltScheme.Name) <> _
                            ltScheme.Name Then
'                       remove old 9.0 list template for which there
'                       is already a piped version
                        ltScheme.Name = ""
                    Else
'                       add props to name
                        AddPropsToLTName ltScheme, ActiveDocument
                    End If
                ElseIf Right(ltScheme.Name, 1) = "*" Then
'                   this corrects for a bug in MP2k (RenameLTsFromVars)
'                   that may affect 9.7.0-9.7.2 schemes reused in MacPac 9.3.0-9.4.0;
'                   code inadvertently added flag to LT name
                    ltScheme.Name = Left(ltScheme.Name, Len(ltScheme.Name) - 1)
                ElseIf Right(ltScheme.Name, 1) <> "|" Then
'                   remove list templates with characters
'                   after last pipe - Word may have created duplicate
                    ltScheme.Name = ""
                End If
                
                If ltScheme.Name <> "" Then
'                   get scheme before PreventLTReversion unnames original list template
                    xProps = ""
                    xScheme = xGetLTRoot(ltScheme.Name)
                    
                    On Error Resume Next
                    xProps = ActiveDocument.Variables(xScheme).Value
                    On Error GoTo 0
                    
'                   prevent reversion of indents in Word 97
                    PreventLTReversion ltScheme.Name
                    
'                   ensure existence/integrity of backup props
                    If xProps = "" Then
                        BackupProps xScheme
                    ElseIf Right(xProps, 1) = "*" Then
'                       original 9.7 flag was not compatible with mpNumShare80 and
'                       pre-9.7 versions - move to independent doc var
                        ActiveDocument.Variables(xScheme) = Left(xProps, Len(xProps) - 1)
                        AddSchemeFlag xScheme, mpFlagMSIndentsBug970
                    End If
                End If
'                g_bLeaveDocDirty = True
            End If
'    xTimeMsg = xTimeMsg & "Finished renaming " & ltScheme.Name & " at " & Now & vbCr
        End If

'       get rid of font substitution
        If Not bIsClean Then
            If bPropFldNeedsReset(ltScheme.ListLevels(1).Font) Then
'    xTimeMsg = xTimeMsg & "Started cleaning " & ltScheme.Name & " at " & Now & vbCr
                ResetLLFontsToThemselves ltScheme
                g_bLeaveDocDirty = True
'    xTimeMsg = xTimeMsg & "Finished cleaning " & ltScheme.Name & " at " & Now & vbCr
            End If
        End If
    Next ltScheme
    
'   name far east and/or name bi may still be in document
    If (bIsClean = False) And _
            (Dialogs(wdDialogFontSubstitution) _
            .unavailablefont <> "") Then
'        ResetDocFontProps
'    xTimeMsg = xTimeMsg & "Started cleaning doc at " & Now & vbCr
        RestorePropFldToTNR ActiveDocument.Content.Font
'    xTimeMsg = xTimeMsg & "Finished cleaning doc at " & Now & vbCr
    End If
    
'   cleanup only runs when doc is opened, so clear undo list
    If Not bIsClean Then _
        ActiveDocument.UndoClear
    
    Application.StatusBar = ""
End Function

Function ResetDocFontProps() As Long
'this does a targeted cleanup of list nums and TOC,
'rather than cycle through entire document
    Dim fldField As Word.Field
    Dim rngCode As Word.Range
    Dim bDoTOC As Boolean
    Dim paraTOC As Word.Paragraph
    Dim rngLocation As Word.Range
    
'   list nums
    For Each fldField In ActiveDocument.Fields
        With fldField
            If .Type = wdFieldListNum Then
                Set rngCode = rngGetField(.Code)
                lSetFontPropToTNR rngCode
            End If
        End With
    Next fldField
    
    If Dialogs(wdDialogFontSubstitution) _
            .unavailablefont = "" Then
        Exit Function
    End If

'   TOC
    On Error Resume Next
    With ActiveDocument
        bDoTOC = (.Content.Bookmarks _
            .Exists("mpTableOfContents")) And _
            (CBool(.Variables("chkApplyManualFormatsToTOC")))
    End With
    On Error GoTo 0
    
    If bDoTOC Then
        For Each paraTOC In ActiveDocument _
                .Bookmarks("mpTableOfContents").Range.Paragraphs
            Set rngLocation = paraTOC.Range
            rngLocation.StartOf
            lSetFontPropToTNR rngLocation
        Next paraTOC
    End If
    
End Function

Public Function BackupProps(Optional ByVal xScheme As String) As Variant
'creates/updates doc vars that hold backups
'of all scheme and level properties
'for all schemes in active document;
'moved from mpBase to avoid breaking compatibility with bIsNew argument (9.7.0);
'removed argument in 9.7.3
    Dim ltP As Word.ListTemplate
    Dim llp As Word.ListLevel
    Dim xDetail As String
    
    With Word.ActiveDocument
        If Len(xScheme) Then
'           backup specified scheme
            On Error GoTo ProcError
            Set ltP = .ListTemplates(xGetFullLTName(xScheme))
            On Error GoTo 0
            
            For Each llp In ltP.ListLevels
'               build string of level props
                xDetail = xDetail & xGetNumLevelProps(ltP, llp.Index)
            Next llp
            
'           create/update var with scheme name
            xDetail = "|" & xDetail & "|"
            
'           create/update variable
            .Variables(xScheme) = xDetail
        Else
'           backup all schemes in doc
            For Each ltP In .ListTemplates
                If bIsMPListTemplate(ltP) Then
                    For Each llp In ltP.ListLevels
'                       build string of level props
                        xDetail = xDetail & xGetNumLevelProps(ltP, llp.Index)
                    Next llp
'                   create/update var with scheme name
                    .Variables(xGetLTRoot(ltP.Name)) = "|" & xDetail & "|"
                    xDetail = ""
                End If
            Next ltP
        End If
    End With
    Exit Function
    
ProcError:
    Exit Function
End Function

Public Function PreventLTReversion(Optional xLT As String = "", _
                                   Optional xScheme As String = "") As Long
'this is a fix for the problem whereby the number and text position of document schemes
'created in Word 97 prior to v.9.7.3 will revert to original settings when the
'document is opened in Word 2k or XP
    Dim ltP As Word.ListTemplate
    Dim bIsHScheme As Boolean
    Dim iLevels As Integer
    Dim xVar As String
    Dim i As Integer
    Dim ltNative As Word.ListTemplate

    On Error GoTo ProcError
    
'   need one or the other to proceed
    If xLT & xScheme = "" Then _
        Exit Function
    
'   only do in Word 97
    If InStr(Word.Application.Version, "8.") = 0 Then _
        Exit Function
        
'   only do if specified in ini
    If Not g_bCodeForMSBug Then _
        Exit Function
        
'   get scheme
    If xScheme = "" Then _
        xScheme = xGetLTRoot(xLT)

'   check for flag that already fixed
    If bSchemeIsFlagged(xScheme, mpFlagMSIndentsBug973) Then _
        Exit Function
        
'   check doc vars for old 9.7 flag;
'   if scheme was created with 9.7.0 - 9.7.2, client may have
'   had some preventative in place, e.g. 9.7.0 didn't edit
'   list templates in Word 97 and 9.7.2 offered the option
'   of disabling the indent controls
    If g_bAcceptAllFlags Then
        If bSchemeIsFlagged(xScheme, mpFlagMSIndentsBug970) Then
            Exit Function
        Else
            On Error Resume Next
            xVar = ActiveDocument.Variables(xScheme)
            On Error GoTo ProcError
        
            If xVar <> "" Then
                If Right(xVar, 1) = mpFlagMSIndentsBug970 Then
                    Exit Function
                End If
            End If
        End If
    End If
    
    Application.ScreenUpdating = False
    
'   determine if the scheme is using Word Heading styles
    bIsHScheme = bIsHeadingScheme(xScheme)
    
'   get number of levels
    iLevels = iGetLevels(xScheme, mpSchemeType_Document)

'   get existing list template and unname
    If xLT = "" Then _
        xLT = xGetFullLTName(xScheme)
    Set ltP = ActiveDocument.ListTemplates(xLT)
    ltP.Name = ""
    
'   add native list template and redefine to match existing one
    Set ltNative = ActiveDocument.ListTemplates.Add(True)
    For i = 1 To 9
        iCopyListLevel xScheme, ltP.ListLevels(i), ltNative.ListLevels(i), True
    Next i
    ltNative.Name = xLT
    
'   relink styles to native list template
    RelinkPreserveIndents ActiveDocument, ltNative, xScheme, 1, iLevels, bIsHScheme
    
'   flag as fixed
    AddSchemeFlag xScheme, mpFlagMSIndentsBug973
    
'   add flag to skip for remainder of conversion cycle
    ltNative.Name = xLT & "z"
        
    Application.ScreenUpdating = True
        
    Exit Function
    
ProcError:
    RaiseError "mdlConversion.PreventLTReversion"
    Exit Function
End Function

Public Sub AddSchemeFlag(xScheme As String, xFlag As String)
    Dim xVar As String
    
    On Error Resume Next
    xVar = ActiveDocument.Variables(xScheme & "_Ver")
    On Error GoTo ProcError
    
    xFlag = "|" & xFlag & "|"
    If InStr(xVar, xFlag) = 0 Then
        ActiveDocument.Variables(xScheme & "_Ver") = xVar & xFlag
    End If
    
    Exit Sub
ProcError:
    RaiseError "mdlConversion.AddSchemeFlag"
    Exit Sub
End Sub

Public Function bSchemeIsFlagged(xScheme As String, xFlag As String) As Boolean
    Dim xVar As String
    
    On Error Resume Next
    xVar = ActiveDocument.Variables(xScheme & "_Ver")
    On Error GoTo ProcError
    
    xFlag = "|" & xFlag & "|"
    bSchemeIsFlagged = (InStr(UCase(xVar), UCase(xFlag)) <> 0)
    
    Exit Function
ProcError:
    RaiseError "mdlConversion.bSchemeIsFlagged"
    Exit Function
End Function

Public Function iRenameLTsFromVars() As Integer
'cycles through backup props, seeing if any are needed;
'runs as part of Relink Document Schemes menu item;
'doesn't worry about list template linked to Heading 1-9
'because if this list template did get unnamed, it will already have been
'assigned generic "Heading" props on initialization of Schemes dialog;
'returns # of schemes "recovered"
    Dim xActive As String
    Dim oVar As Word.Variable
    Dim oStyle As Word.Style
    Dim xLT As String
    Dim xStyle As String
    Dim xScheme As String
    Dim xValue As String
    Dim oLT As Word.ListTemplate
    Dim i As Integer
    
    On Error GoTo ProcError
    
    For Each oVar In ActiveDocument.Variables
        Set oStyle = Nothing
        Set oLT = Nothing
        
        xScheme = oVar.Name
        xValue = oVar.Value
        If Len(xScheme) < 5 Then _
            GoTo labNext
        If (Left(xScheme, 4) = mpPrefix) And _
                (InStr(xValue, "|") <> 0) Then
'           check if heading scheme
            If bIsHeadingScheme(xScheme) Then _
                GoTo labNext
                    
'           build list template name
            xLT = xScheme & xValue
                    
'           get level 1 style
            xStyle = xGetStyleName(xScheme, 1)
                    
'           ensure that level 1 style exists
            On Error Resume Next
            Set oStyle = ActiveDocument.Styles(xStyle)
            On Error GoTo ProcError
            
            If Not oStyle Is Nothing Then
'               check if style is outline numbered
                On Error Resume Next
                Set oLT = oStyle.ListTemplate
                On Error GoTo ProcError
                If Not oLT Is Nothing Then
                    If oLT.Name = "" Then
'                       style is linked to unnamed LT - before renaming,
'                       ensure that name isn't already in use
                        If Not bListTemplateExists(xScheme) Then
                            AddPropsToLTName oLT, ActiveDocument, xScheme
                            i = i + 1
                        End If
                    End If
                    GoTo labNext
                End If
            End If
        End If
labNext:
    Next oVar
    
    iRenameLTsFromVars = i
    
    Exit Function
ProcError:
    RaiseError "mdlConversion.iRenameLTsFromVars"
    Exit Function
End Function

Sub RenameActiveLTFromVar()
    Dim xActive As String
    Dim oStyle As Word.Style
    Dim xLT As String
    Dim xStyle As String
    Dim xScheme As String
    Dim xBack As String
    Dim oLT As Word.ListTemplate
    
    On Error GoTo ProcError
    
'   get active scheme
    On Error Resume Next
    xActive = ActiveDocument.Variables(mpActiveSchemeDocVar)
    On Error GoTo ProcError
    
    If xActive = "" Then
'       no active scheme
        Exit Sub
    Else
'       parse out scheme name
        xScheme = Mid(xActive, 2)
        
'       get backup
        On Error Resume Next
        xBack = ActiveDocument.Variables(xScheme)
        On Error GoTo ProcError
        
'       no backup
        If xBack = "" Then Exit Sub
    End If
    
    If Not bListTemplateExists(xScheme) Then
'       no list template found for active scheme
        xLT = xScheme & xBack
                    
'       get level 1 style
        xStyle = xGetStyleName(xScheme, 1)
                    
'       ensure that level 1 style exists
        On Error Resume Next
        Set oStyle = ActiveDocument.Styles(xStyle)
        On Error GoTo ProcError
                    
        If Not oStyle Is Nothing Then
'           check if style is outline numbered
            On Error Resume Next
            Set oLT = oStyle.ListTemplate
            On Error GoTo ProcError
            If Not oLT Is Nothing Then
                If oLT.Name = "" Then
'                   style is linked to unnamed LT - rename
                    AddPropsToLTName oLT, ActiveDocument, xScheme
                Else
'                   style is linked to a different list template;
'                   relink all schemes
                    bRelinkSchemes mpSchemeType_Document
                End If
                Exit Sub
            End If
        ElseIf xStyle <> "Heading_L1" Then
'           style not found
            Exit Sub
        End If
        
'       scheme may be using Heading 1-9;
'       see if Heading 1 is linked to unnamed LT
        On Error Resume Next
        Set oLT = ActiveDocument.Styles(wdStyleHeading1).ListTemplate
        On Error GoTo ProcError
        If Not oLT Is Nothing Then
            If oLT.Name = "" Then
'               style is linked to unnamed LT - rename
                oLT.Name = xLT
            End If
        End If
    End If

    Exit Sub
ProcError:
    RaiseError "mdlConversion.RenameActiveLTFromVar"
    Exit Sub
End Sub

Function ConvertToMacPac() As Long
'converts proprietary styles linked to outline numbering into MacPac numbering
    Dim xDatFile As String
    Dim xMappings() As String
    Dim iCount As Integer
    Dim lRet As Long
    Dim i As Integer
    Dim iConverted As Integer
    Dim oStyle As Word.Style
    Dim oStyleMP As Word.Style
    Dim xStyNew As String
    Dim oLT As Word.ListTemplate
    Dim xLT As String
    Dim j As Integer
    Dim iLevels As Integer
    Dim xMsg As String
    Dim xScheme As String
    Dim oScheme As New CSchemeRecords
    Dim oRecord As CNumScheme
    Dim xProps As String
    Dim iFormats As Integer
    Dim iTrailChar As Integer
    Dim bHasMP As Boolean
    Dim xLevel As String
    Dim xUnlinked As String
    Dim xApplied As String
    Dim bIsUnlinked As Boolean
    Dim bIsConvertible As Boolean
    Dim iDynamic As Integer
    
    Const mpMappingsScheme As Integer = 0
    Const mpMappingsLevel1 As Integer = 1
    Const mpMappingsConvert As Integer = 10
    Const mpMappingsApplied As Integer = 11
    
    On Error GoTo ProcError
    
'   check for ini file
    xDatFile = mpBase.GetAppPath & "\mpnSchemeMappings.ini"
    If Dir(xDatFile) = "" Then Exit Function
    
'   count mappings
    While System.PrivateProfileString(xDatFile, "Scheme" & (iCount + 1), "Level1") <> ""
        iCount = iCount + 1
    Wend
    
'   no mappings
    If iCount = 0 Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "Aucun plan defini dans mpnSchemeMappings.ini.  Veuillez contacter votre administrateur.", vbCritical, g_xAppName
        Else
            MsgBox "There are no mappings defined in mpnSchemeMappings.ini.  Please " & _
                "contact your system administrator.", vbCritical, g_xAppName
        End If
        Exit Function
    End If
    
'   prompt for conversion
    If g_lUILanguage = wdFrenchCanadian Then
        lRet = MsgBox("D�sirez-vous convertir la num�rotation hi�rarchis�e dans ce document en num�rotation MacPac ?", vbYesNo, g_xAppName)
    Else
        lRet = MsgBox("Would you like to convert the outline numbering in this document " & _
            "into MacPac numbering?", vbYesNo, g_xAppName)
    End If
    If lRet = vbNo Then Exit Function
    
    Application.ScreenUpdating = False
    
'   put mappings in an array
    ReDim xMappings(iCount - 1, 11)
    For i = 0 To iCount - 1
        xScheme = "Scheme" & (i + 1)
        xMappings(i, mpMappingsScheme) = _
            System.PrivateProfileString(xDatFile, xScheme, "MacPacRoot")
        For j = 1 To 9
            xLevel = "Level" & j
            xMappings(i, j) = System.PrivateProfileString(xDatFile, xScheme, xLevel)
        Next j
    Next i
    
'   initial cycle to identify and analyze convertible schemes in doc
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "Analyse des th�mes dudocument.  Veuillez patienter..."
    Else
        Application.StatusBar = "Analyzing schemes in document.  Please wait..."
    End If
    
    For i = 0 To iCount - 1
        Set oStyle = Nothing
        Set oLT = Nothing
        
'       see if old level 1 style is in doc
        On Error Resume Next
        Set oStyle = ActiveDocument.Styles(xMappings(i, mpMappingsLevel1))
        On Error GoTo ProcError
        
        If Not oStyle Is Nothing Then
'           get list template linked to old level 1 style
            On Error Resume Next
            Set oLT = oStyle.ListTemplate
            On Error GoTo ProcError
            
'           exit if style isn't numbered
            If oLT Is Nothing Then
                If g_lUILanguage = wdFrenchCanadian Then
                    MsgBox xMappings(i, mpMappingsLevel1) & " et les styles apparent�s ne peuvent pas �tre convertis � MacPac parce que " & xMappings(i, mpMappingsLevel1) & " n'est pas li� � la liste des mod�les de num�rotation Word.", _
                        vbInformation, g_xAppName
                Else
                    MsgBox xMappings(i, mpMappingsLevel1) & " and related styles cannot be converted to " & _
                        "MacPac because " & xMappings(i, mpMappingsLevel1) & " is not linked to a " & _
                        "Word numbering list template.", vbInformation, g_xAppName
                End If
                bHasMP = True
                GoTo labNextScheme
            End If
            
'           exit if style isn't outline numbered
            If (oLT.OutlineNumbered = False) Or _
                    (oLT.ListLevels.Count <> 9) Then
                If g_lUILanguage = wdFrenchCanadian Then
                    MsgBox xMappings(i, mpMappingsLevel1) & " et les styles apparent�s ne peuvent pas �tre convertis � MacPac parce que les th�mes MacPac requi�rent une num�rotation hi�rarchis�e e " & _
                        xMappings(i, mpMappingsLevel1) & " est li� � la liste des mod�les de num�rotation Word avec un seul niveau.", vbInformation, g_xAppName
                Else
                    MsgBox xMappings(i, mpMappingsLevel1) & " and related styles cannot be converted to " & _
                        "MacPac because MacPac schemes require outline numbering and " & _
                        xMappings(i, mpMappingsLevel1) & " is linked to a Word numbering list template with " & _
                        "only one level.", vbInformation, g_xAppName
                End If
                bHasMP = True
                GoTo labNextScheme
            End If
            
'           exit if target scheme is already in the document
            xScheme = mpPrefix & xMappings(i, mpMappingsScheme)
            If bListTemplateExists(xScheme) Then
                If g_lUILanguage = wdFrenchCanadian Then
                    MsgBox xMappings(i, mpMappingsLevel1) & " et les styles apparent�s ne peuvent pas �tre convertis � MacPac parce que le th�me " & xMappings(i, mpMappingsScheme) & _
                        " est d�j� dans le document.  Pour convertir les paragraphes num�rot�s utilisant ces styles � la num�rotation de paragraphes MacPac, cliquez sur le bouton Th�mes, s�lectionnez le th�me de document " & _
                        xMappings(i, mpMappingsScheme) & ", puis cliquez sur le bouton Changer �.", _
                        vbInformation, g_xAppName
                Else
                    MsgBox xMappings(i, mpMappingsLevel1) & " and related styles cannot be converted to " & _
                        "MacPac because the " & xMappings(i, mpMappingsScheme) & " scheme is already in the " & _
                        "document.  To convert numbered paragraphs using these styles " & _
                        "to MacPac numbered paragraphs, click the Schemes button, select the " & _
                        xMappings(i, mpMappingsScheme) & " document scheme, and then click the Change To button.", _
                        vbInformation, g_xAppName
                End If
                bHasMP = True
                GoTo labNextScheme
            Else
'               check for stray MacPac styles
                For j = 1 To 9
                    Set oStyleMP = Nothing
                    xStyNew = xMappings(i, mpMappingsScheme) & "_L" & j
                    
                    On Error Resume Next
                    Set oStyleMP = ActiveDocument.Styles(xStyNew)
                    On Error GoTo ProcError
                    
                    If Not oStyleMP Is Nothing Then
                        If g_lUILanguage = wdFrenchCanadian Then
                            MsgBox xMappings(i, mpMappingsLevel1) & " et les styles apparent�s ne peuvent pas �tre convertis � MacPac parce que les styles " & xMappings(i, mpMappingsScheme) & _
                                " sont d�j� dans le document. Pour convertir les paragraphes num�rot�s utilisant ces styles � la num�rotation de paragraphes MacPac, cliquez sur le bouton Th�mes et s�lectionnez un autre th�me.", vbInformation, g_xAppName
                        Else
                            MsgBox xMappings(i, mpMappingsLevel1) & " and related styles cannot be converted to " & _
                               "MacPac because " & xMappings(i, mpMappingsScheme) & " styles are already in the " & _
                               "document.  To convert numbered paragraphs using these styles " & _
                               "to MacPac numbered paragraphs, click the Schemes button and then " & _
                               "change to the desired scheme.", vbInformation, g_xAppName
                       End If
                        bHasMP = True
                        GoTo labNextScheme
                    End If
                Next j
            End If
            
'           check if style is applied to text in doc
            With ActiveDocument.Content.Find
                .ClearFormatting
                .Format = True
                .Wrap = wdFindContinue
                .Style = xMappings(i, mpMappingsLevel1)
                .Execute
                If .Found Then
'                   flag as applied
                    xMappings(i, mpMappingsApplied) = "1"
                End If
            End With
            
'           ensure that multiple sets of styles are not
'           flagged for conversion to the same Macpac scheme
            For j = 0 To iCount - 1
                If i <> j Then
                    If xMappings(j, mpMappingsConvert) = "1" And _
                            xMappings(j, mpMappingsScheme) = xMappings(i, mpMappingsScheme) Then
'                       another scheme is already flagged for conversion to the same MacPac scheme
                        If xMappings(i, mpMappingsApplied) = "1" And _
                                xMappings(j, mpMappingsApplied) = "" Then
'                           since only this scheme is applied in doc,
'                           unflag the previous one
                            xMappings(j, mpMappingsConvert) = ""
                        Else
                            If xMappings(i, mpMappingsApplied) = "1" Then
'                               both schemes are applied in doc - warn user that
'                               only the previous one will be converted
                                If g_lUILanguage = wdFrenchCanadian Then
                                    MsgBox xMappings(i, mpMappingsLevel1) & " et les styles apparent�s ne peuvent pas �tre convertis � MacPac parce " & xMappings(j, mpMappingsLevel1) & _
                                        " et les styles apparent�s sont aussi li�s � " & xMappings(i, mpMappingsScheme) & _
                                        ", et de multiples ensembles ne peuvent �tre convertis � un m�me th�me.", vbInformation, g_xAppName
                                Else
                                    MsgBox xMappings(i, mpMappingsLevel1) & " and related styles cannot be converted to " & _
                                        "MacPac because " & xMappings(j, mpMappingsLevel1) & " and related styles are " & _
                                        "also mapped to " & xMappings(i, mpMappingsScheme) & ", and multiple sets of " & _
                                        "styles cannot be converted to the same scheme.", vbInformation, g_xAppName
                                End If
                            End If
                            GoTo labNextScheme
                        End If
                    End If
                End If
            Next j

'           flag scheme for conversion
            xMappings(i, mpMappingsConvert) = "1"
            bIsConvertible = True
            
'           if any levels of any scheme are unlinked, we'll warn before converting
            If Not bIsUnlinked Then
                For j = 1 To 9
'                    If xMappings(i, j) <> "" Then
                        If UCase(oLT.ListLevels(j).LinkedStyle) <> UCase(xMappings(i, j)) Then
                            bIsUnlinked = True
                            Exit For
                        End If
'                    End If
                Next j
            End If
            
'******************************************************************************************
'in a future version, we may provide a detailed analysis of problem styles/levels;
'perhaps we'll add a help button that will present this info on another screen
'            xUnlinked = ""
'            xApplied = ""
'            For j = 1 To 9
'                If xMappings(i, j) <> "" Then
'                    If oLT.ListLevels(j).LinkedStyle <> xMappings(i, j) Then
''                       level is unlinked - add to message string
'                        xUnlinked = xUnlinked & xMappings(i, j) & ", "
'
''                       is unlinked level applied to text?
'                        With ActiveDocument.Content.Find
'                            .ClearFormatting
'                            .Format = True
'                            .Wrap = wdFindContinue
'                            .Style = xMappings(i, j)
'                            .Execute
'                            If .Found Then
''                               add to message string
'                                xApplied = xApplied & xMappings(i, j) & ", "
'                            End If
'                        End With
'                    End If
'                End If
'
''               warn user that they are converting an unlinked scheme
'                If xUnlinked <> "" Then
''                   strip trailing commas
'                    xUnlinked = Left(xUnlinked, Len(xUnlinked) - 2)
'                    If xApplied <> "" Then
'                        xApplied = Left(xApplied, Len(xApplied) - 2)
'                    End If
'
''                   prompt
'                    xMsg = "The following styles in this document are not linked " & _
'                        "properly to Word's list template.  Converting to MacPac may " & _
'                        "result in numbered paragraphs"
'                End If
'            Next j
'******************************************************************************************

        End If
labNextScheme:
    Next i
    
'   warn before converting unlinked schemes
    If Not bIsConvertible Then
        GoTo labDisplayResults
    ElseIf bIsUnlinked Then
        If g_lUILanguage = wdFrenchCanadian Then
            lRet = MsgBox("Ce document contient des styles de num�rotation qui ne sont pas correctement li�s � la liste des mod�les de num�rotation Word.  Continuer avec cette macro pourra affecter l'apparence de la num�rotation des paragraphes dans le document.  D�sirez-vous continuer ?", _
               vbQuestion + vbYesNo, g_xAppName)
        Else
            lRet = MsgBox("This document contains numbered styles which are not properly linked " & _
               "to Word's numbering list template.  Continuing with this macro may affect the look " & _
               "of numbered paragraphs in this document.  Do you wish to continue?", _
               vbQuestion + vbYesNo, g_xAppName)
        End If
        If lRet = vbNo Then
            Application.StatusBar = ""
            Application.ScreenUpdating = True
            Exit Function
        End If
    End If
            
'   load sty files
    LoadStyFiles
    
'   convert flagged schemes
    If g_lUILanguage = wdFrenchCanadian Then
        Application.StatusBar = "Conversion des th�mes. Veuillez patienter ..."
    Else
        Application.StatusBar = "Converting schemes.  Please wait..."
    End If
    
    For i = 0 To iCount - 1
        If xMappings(i, mpMappingsConvert) = "1" Then
'           start building LT name from props in mpNumbers.sty
            xScheme = mpPrefix & xMappings(i, mpMappingsScheme)
            Set oScheme = New CSchemeRecords
            Set oRecord = oScheme.GetRecord(xScheme, mpSchemeType_Public)
            With oRecord
                iDynamic = oScheme.GetDynamicFieldValue(.DynamicFonts, _
                    .DynamicSpacing)
                xProps = "||" & .Alias & "|" & .Origin & "|" & _
                    .TOCScheme & "|" & iDynamic
            End With
            
'           get list template
            Set oLT = ActiveDocument.Styles(xMappings(i, mpMappingsLevel1)).ListTemplate
            
'           cycle through levels
            For j = 1 To 9
                If xMappings(i, j) <> "" Then
'                   ini specifies a style for this level
                    Set oStyle = Nothing
                    xStyNew = xMappings(i, mpMappingsScheme) & "_L" & j
                
'                   check for style in doc
                    On Error Resume Next
                    Set oStyle = ActiveDocument.Styles(xMappings(i, j))
                    On Error GoTo ProcError
                    
                    If Not oStyle Is Nothing Then
'                       rename style
                        oStyle.NameLocal = xStyNew
                        
'                       set outline level
                        oStyle.ParagraphFormat.OutlineLevel = j
                        
'                       add trailing character props for level
                        Select Case oLT.ListLevels(j).TrailingCharacter
                            Case wdTrailingTab
                                iTrailChar = mpTrailingChar_Tab
                            Case wdTrailingSpace
                                iTrailChar = mpTrailingChar_Space
                            Case Else
                                iTrailChar = GetCustomTrailChrs(xStyNew, xScheme, j)
                        End Select
                        xProps = xProps & "|" & iTrailChar & "|0|"
                        
'                       set Heading prop based on font formats in the style
                        iFormats = iGetFontFormat(oStyle.Font)
                        xProps = xProps & iFormats & "|"
                    Else
'                       style for this level is not in doc
                        Exit For
                    End If
                Else
'                   scheme does not have this level
                    Exit For
                End If
            Next j
            
'           tally levels
            iLevels = j - 1
            
'           finish conversion
            If iLevels > 0 Then
'               add to converted schemes tally
                iConverted = iConverted + 1
                
'               set props for unused levels
                For j = iLevels To 8
                    xProps = xProps & "|mpNA|"
                Next j
                
'               name list template
                xProps = xProps & "|"
                oLT.Name = xScheme & xProps
                
'               create backup variable
                ActiveDocument.Variables(xScheme) = xProps
                
'               ensure tab stops
                AddTabStops xScheme
                
'               create cont styles
                For j = 1 To iLevels
                    CreateContStyles xScheme, j
                Next j
            End If
        End If
    Next i
    
'   unload sty files
    UnLoadStyFiles
    
'   finishing touches
    If iConverted > 0 Then
'       relink if necessary
        If bIsUnlinked Then _
            bRelinkSchemes mpSchemeType_Document

'       ensure active scheme
        If xActiveScheme(ActiveDocument) = "" Then
'           clear legacy 8.x variable
            ActiveDocument.Variables(mpActiveScheme80DocVar) = ""
'           activate first scheme
            xActivateFirstScheme
        End If
    End If
                
'   display results
labDisplayResults:
    If g_lUILanguage = wdFrenchCanadian Then
        If iConverted = 1 Then
            xMsg = iConverted & " th�mes de num�rotation ont �t� trouv�s et convertis."
        ElseIf iConverted = 0 Then
            If Not bHasMP Then
                xMsg = "Aucun th�me de num�rotation non converti n'a �t� trouv� dans ce document."
            End If
        Else
            xMsg = iConverted & " th�mes de num�rotation ont �t� trouv�s et convertis."
        End If
    Else
        If iConverted = 1 Then
            xMsg = iConverted & " numbering scheme was found and converted."
        ElseIf iConverted = 0 Then
            If Not bHasMP Then
                xMsg = "No unconverted numbering schemes were found in this document."
            End If
        Else
            xMsg = iConverted & " numbering schemes were found and converted."
        End If
    End If
    
    If xMsg <> "" Then
        MsgBox xMsg, vbInformation, g_xAppName
    End If
    
    Application.ScreenUpdating = True
    Application.StatusBar = ""
    
    Exit Function
ProcError:
    RaiseError "mdlConversion.ConvertToMacPac"
    Exit Function
End Function

Function AddTabStops(xScheme As String) As Long
'ensures that numbered styles have a tab stop and
'that list level tab position is in sync with it
    Dim i As Integer
    Dim xStyle As String
    Dim oStyle As Word.Style
    Dim xLT As String
    Dim oLT As Word.ListTemplate
    Dim sPos As Single
    Dim rngSelection As Word.Range
    Dim lVScrolled As Long
    Dim lView As Long
    
    On Error GoTo ProcError
    
'   move selection to end of doc and insert a paragraph;
'   we'll use this paragraph to measure actual tab position
    EchoOff
    Application.ScreenUpdating = False
    With ActiveWindow
        lView = .View
        lVScrolled = .VerticalPercentScrolled
        .View = wdNormalView
    End With
    
    With Selection
        Set rngSelection = .Range.Duplicate
        .EndKey wdStory
        .InsertParagraphAfter
        .EndOf
        .Style = wdStyleNormal
    End With
    
    xLT = xGetFullLTName(xScheme)
    Set oLT = ActiveDocument.ListTemplates(xLT)
    
    For i = 1 To 9
        xStyle = xGetStyleName(xScheme, i)
        
        On Error Resume Next
        Set oStyle = ActiveDocument.Styles(xStyle)
        On Error GoTo ProcError
        
        If oStyle Is Nothing Then Exit For
        
        With oStyle
            With .ParagraphFormat
                sPos = -1
                If oLT.ListLevels(i).TrailingCharacter = wdTrailingTab Then
'                   apply style to measure actual tab position
                    With Selection
                        .Style = xStyle
                        sPos = .Information(wdHorizontalPositionRelativeToTextBoundary)
                    End With
                ElseIf .TabStops.Count = 0 Then
'                   get generic tab position
                    sPos = .LeftIndent + .FirstLineIndent + ActiveDocument.DefaultTabStop
                End If
                    
                If .TabStops.Count = 0 Then
'                   add tab stop
                    .TabStops.Add sPos, wdAlignTabLeft, wdTabLeaderSpaces
                ElseIf sPos <> -1 Then
'                   reset first tab stop to actual position
                    .TabStops(1).Position = sPos
                End If

'               force list level tab position to match first tab stop
                oLT.ListLevels(i).TabPosition = .TabStops(1).Position
            End With
        End With
        
        Set oStyle = Nothing
    Next i

'   restore original selection and delete test paragraph
    rngSelection.Select
    ActiveDocument.Paragraphs.Last.Range.Delete
    With ActiveWindow
        .View = lView
        .VerticalPercentScrolled = lVScrolled
    End With
    EchoOn
    Application.ScreenUpdating = True
    
    Exit Function
ProcError:
    EchoOn
    RaiseError "mdlConversion.AddTabStops"
    Exit Function
End Function

Function GetCustomTrailChrs(xStyle As String, _
                            xScheme As String, _
                            iLevel As Integer) As mpTrailingChars
'if style is applied in doc, returns actual characters at start of
'paragraph of first found instance of style; if style isn't applied,
'returns trailing character specified in sty file for public version
    Dim rngPara As Word.Range
    
'   check if style is applied to text in doc
    With ActiveDocument.Content.Find
        .ClearFormatting
        .Format = True
        .Wrap = wdFindContinue
        .Style = xStyle
        .Execute
        If .Found Then
            Set rngPara = .Parent
        End If
    End With
    
    If rngPara Is Nothing Then
'       look in sty file
        GetCustomTrailChrs = xGetLevelProp(xScheme, iLevel, _
            mpNumLevelProp_TrailChr, mpSchemeType_Public)
    Else
'       get from paragraph
        With rngPara
            If Left(.Text, 2) = String(2, 11) Then
                GetCustomTrailChrs = mpTrailingChar_DoubleShiftReturn
            ElseIf Left(.Text, 1) = Chr(11) Then
                GetCustomTrailChrs = mpTrailingChar_ShiftReturn
            ElseIf Left(.Text, 2) = String(2, 32) Then
                GetCustomTrailChrs = mpTrailingChar_DoubleSpace
            Else
                GetCustomTrailChrs = mpTrailingChar_None
            End If
        End With
    End If
End Function

Public Function ModifyUnusableListName(xOldName As String) As String
'handles 5890 error ("list name in use") by appending a space
'to the end of display name
    Dim iPos As Integer
    
    On Error GoTo ProcError
    
    iPos = InStr(xOldName, "||")
    If iPos = 0 Then
        GoTo ProcError
    End If
    
    iPos = InStr(iPos + 2, xOldName, "|")
    If iPos = 0 Then
        GoTo ProcError
    End If
    
    ModifyUnusableListName = Left(xOldName, iPos - 1) & " " & Mid(xOldName, iPos)
    Exit Function
ProcError:
    RaiseError "mdlConversion.ModifyUnusableListName"
    Exit Function
End Function
