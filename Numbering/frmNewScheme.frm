VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmNewScheme 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Create a New Scheme"
   ClientHeight    =   6000
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7380
   Icon            =   "frmNewScheme.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6000
   ScaleWidth      =   7380
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.TreeView tvwSchemes 
      Height          =   4880
      Left            =   150
      TabIndex        =   1
      ToolTipText     =   "This is the scheme on which the new scheme will be based."
      Top             =   345
      Width           =   2825
      _ExtentX        =   4974
      _ExtentY        =   8599
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   573
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin VB.Frame Frame1 
      Height          =   4250
      Left            =   3160
      TabIndex        =   12
      Top             =   255
      Width           =   4100
      Begin VB.TextBox txtDescription 
         Height          =   700
         Left            =   165
         MaxLength       =   120
         MultiLine       =   -1  'True
         TabIndex        =   11
         ToolTipText     =   "This is the description of the scheme as displayed in all MacPac Numbering dialogs."
         Top             =   3410
         Width           =   3800
      End
      Begin VB.CheckBox chkAdjustSpacing 
         Caption         =   "&Change single/double line spacing to match that of Normal style for either exact or multiple spacing"
         Height          =   400
         Left            =   165
         TabIndex        =   9
         ToolTipText     =   "Change single/double to exact spacing when Normal style is exactly spaced."
         Top             =   2650
         Value           =   1  'Checked
         Width           =   3800
      End
      Begin VB.CheckBox chkBaseOnNormal 
         Caption         =   "&Use Normal style font"
         Height          =   300
         Left            =   165
         TabIndex        =   8
         ToolTipText     =   "Change font name and font size of all levels to the Normal style font."
         Top             =   2240
         Value           =   1  'Checked
         Width           =   2115
      End
      Begin VB.TextBox txtSchemeName 
         Height          =   315
         Left            =   165
         TabIndex        =   3
         ToolTipText     =   "This is the name of the scheme as displayed in all MacPac Numbering dialogs."
         Top             =   495
         Width           =   3800
      End
      Begin VB.TextBox txtStyleName 
         Height          =   315
         Left            =   165
         MaxLength       =   10
         TabIndex        =   5
         ToolTipText     =   "This is the name that will be used by all styles in the scheme (e.g. 'Article' in 'Article_L1-9')."
         Top             =   1230
         Width           =   3800
      End
      Begin TrueDBList60.TDBCombo cmbLevels 
         Bindings        =   "frmNewScheme.frx":058A
         Height          =   564
         Left            =   1596
         OleObjectBlob   =   "frmNewScheme.frx":0595
         TabIndex        =   7
         ToolTipText     =   "This is the number of levels that will be present in the new scheme."
         Top             =   1788
         Width           =   552
      End
      Begin VB.Label lblDescription 
         Caption         =   "&Description (up to 120 characters):"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   195
         TabIndex        =   10
         ToolTipText     =   "This is the name of the scheme as displayed in all MacPac Numbering dialogs."
         Top             =   3200
         Width           =   2760
      End
      Begin VB.Label lblSchemeName 
         Caption         =   "&Scheme Name:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   195
         TabIndex        =   2
         ToolTipText     =   "This is the name of the scheme as displayed in all MacPac Numbering dialogs."
         Top             =   285
         Width           =   2760
      End
      Begin VB.Label lblStyleName 
         Caption         =   "Style &Name (up to 10 characters):"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   195
         TabIndex        =   4
         ToolTipText     =   "This is the name that will be used by all styles in the scheme (e.g. 'Article' in 'Article_L1-9')."
         Top             =   1020
         Width           =   2685
      End
      Begin VB.Label lblLevels 
         Caption         =   "Number of &Levels:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   195
         TabIndex        =   6
         ToolTipText     =   "This is the number of levels that will be present in the new scheme."
         Top             =   1825
         Width           =   1395
      End
   End
   Begin VB.OptionButton rdCurrentDoc 
      Caption         =   "Document"
      Enabled         =   0   'False
      Height          =   210
      Left            =   900
      TabIndex        =   20
      Top             =   6500
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.OptionButton rdNumbers 
      Caption         =   "mpNumbers.sty"
      Enabled         =   0   'False
      Height          =   210
      Left            =   870
      TabIndex        =   19
      Top             =   6640
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.PictureBox imgPreview 
      Enabled         =   0   'False
      Height          =   855
      Left            =   465
      ScaleHeight     =   795
      ScaleWidth      =   900
      TabIndex        =   17
      Top             =   6445
      Width           =   960
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   6160
      TabIndex        =   16
      Top             =   5500
      Width           =   1100
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   385
      Left            =   4950
      TabIndex        =   15
      Top             =   5500
      Width           =   1100
   End
   Begin TrueDBList60.TDBCombo cmbTOCScheme 
      Height          =   585
      Left            =   3345
      OleObjectBlob   =   "frmNewScheme.frx":24B6
      TabIndex        =   14
      ToolTipText     =   "This is the default TOC format for a document that uses this new scheme."
      Top             =   4905
      Width           =   3795
   End
   Begin VB.Label lblTOCScheme 
      Caption         =   "Default &TOC Scheme:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3355
      TabIndex        =   13
      ToolTipText     =   "This is the default TOC format for a document that uses this new scheme."
      Top             =   4700
      Width           =   2385
   End
   Begin VB.Label lblView 
      Caption         =   "View Schemes in"
      Enabled         =   0   'False
      Height          =   210
      Left            =   1380
      TabIndex        =   18
      Top             =   6355
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.Label lblBasedOn 
      Caption         =   "&Based On:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   165
      TabIndex        =   0
      Top             =   135
      Width           =   1005
   End
End
Attribute VB_Name = "frmNewScheme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private bNoChange As Boolean
Private m_bCancelled As Boolean
Private m_bAliasEdited As Boolean
Private m_xDefaultBasedOnScheme As String
Private m_xarTOCSchemes As xArray

Public Property Get BasedOnScheme() As String
    On Error Resume Next
    
    'GLOG 5531 - add handling for favorites
    If Me.tvwSchemes.SelectedItem.Parent.Text = mpFavoriteSchemes Then
        BasedOnScheme = Mid(Me.tvwSchemes.SelectedItem.Key, 4)
    Else
        BasedOnScheme = Mid(Me.tvwSchemes.SelectedItem.Key, 2)
    End If
End Property

Public Property Get BasedOnSchemeDisplayName() As String
    On Error Resume Next
    BasedOnSchemeDisplayName = Me.tvwSchemes.SelectedItem.Text
End Property

Public Property Get BasedOnSchemeType() As mpSchemeTypes
    Dim iIndex As Integer 'GLOG 5531
    
    On Error Resume Next
    
    'GLOG 5531 - add handling for favorites
    iIndex = Me.tvwSchemes.SelectedItem.Parent.Index
    If iIndex = 4 Then
        If Left$(Me.tvwSchemes.SelectedItem.Key, 3) = CStr(mpFavoritePublic) Then
            BasedOnSchemeType = mpSchemeType_Public
        Else
            BasedOnSchemeType = mpSchemeType_Private
        End If
    Else
        BasedOnSchemeType = iIndex
    End If
    
'   this is a workaround for missing public node
    If g_bIsAdmin And (BasedOnSchemeType = mpSchemeType_Public) Then _
        BasedOnSchemeType = mpSchemeType_Private
End Property

Public Property Let DefaultBasedOnScheme(xPath As String)
    m_xDefaultBasedOnScheme = xPath
End Property

Public Property Get DefaultBasedOnScheme() As String
    DefaultBasedOnScheme = m_xDefaultBasedOnScheme
End Property

Private Sub cmbLevels_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLevels, Reposition
    Exit Sub
ProcError:
    RaiseError "frmNewScheme.cmbLevels_Mismatch"
    Exit Sub
End Sub

Private Sub cmbTOCScheme_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbTOCScheme, Reposition
    Exit Sub
ProcError:
    RaiseError "frmNewScheme.cmbTOCScheme_Mismatch"
    Exit Sub
End Sub

Private Sub cmdCancel_Click()
    m_bCancelled = True
    Me.Hide
    Application.ScreenRefresh
End Sub

Private Sub cmdOK_Click()
    Dim i As Integer
    Dim iNumSchemes As Integer
    Dim xNewScheme As String
    Dim xNewAlias As String
    Dim bStyleNameValid As Boolean
    Dim bSchemeNameValid As Boolean
    Dim bIsValidBasis As Boolean
    
    xNewScheme = Me.txtStyleName
    xNewAlias = Me.txtSchemeName
    
    bSchemeNameValid = bSchemeNameIsValid(xNewAlias, True)
    If bSchemeNameValid Then
        bStyleNameValid = bStyleNameIsValid(xNewScheme, True)
    End If
    
    On Error Resume Next
    bIsValidBasis = Not (Me.tvwSchemes.SelectedItem.Parent Is Nothing)
    On Error GoTo 0
    
'   verify that name and alias are valid
    If bStyleNameValid And bSchemeNameValid Then
        If Not bIsValidBasis Then
            Me.tvwSchemes.SetFocus
            xMsg = "Please select a valid 'Based On' scheme."
            MsgBox xMsg, vbExclamation, g_xAppName
            Screen.MousePointer = vbDefault
        Else
            m_bCancelled = False
            Me.Hide
            Application.ScreenRefresh
        End If
    ElseIf Not bStyleNameValid Then
        With Me.txtSchemeName
            .SelStart = 0
            .SelLength = Len(.Text)
            .SetFocus
        End With
        Screen.MousePointer = vbDefault
    Else
        With Me.txtStyleName
            .SelStart = 0
            .SelLength = Len(.Text)
            .SetFocus
        End With
        Screen.MousePointer = vbDefault
    End If
End Sub

Private Sub Form_Activate()
    Dim xTOCScheme As String
    
    Me.cmbLevels.SelectedItem = iGetLevels(Me.BasedOnScheme, _
                                Me.BasedOnSchemeType) - 1
    On Error Resume Next
    tvwSchemes_NodeClick Me.tvwSchemes.SelectedItem
    Me.txtSchemeName.SetFocus
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim bTOCInstalled As Boolean
    Dim xarLevels As xArray

    #If compHandleErrors Then
        On Error GoTo ProcError
    #Else
        On Error GoTo 0
    #End If
    
    m_bCancelled = True
    
    Me.cmbTOCScheme.Visible = g_bAllowTOCLink
    Me.lblTOCScheme.Visible = g_bAllowTOCLink
    
    If g_bAllowTOCLink Then
'       first ensure TOC link - may not be there if
'       Word was started with a protected document
        lInitializeTOCLink
                        
        Set m_xarTOCSchemes = New xArray
        m_xarTOCSchemes.ReDim 0, UBound(g_xTOCSchemes), 0, 1
        For i = LBound(g_xTOCSchemes) To UBound(g_xTOCSchemes)
            m_xarTOCSchemes(i, 0) = g_xTOCSchemes(i, 0)
            m_xarTOCSchemes(i, 1) = g_xTOCSchemes(i, 1)
        Next i
        Me.cmbTOCScheme.Array = m_xarTOCSchemes
        ResizeTDBCombo Me.cmbTOCScheme, 3
    End If
            
    Set xarLevels = New xArray
    xarLevels.ReDim 0, 8, 0, 1
    For i = 0 To 8
        xarLevels(i, 0) = i + 1
        xarLevels(i, 1) = i + 1
    Next i
    Me.cmbLevels.Array = xarLevels
    ResizeTDBCombo Me.cmbLevels, 9
        
    RefreshSchemesList tvwSchemes
    Exit Sub
    
ProcError:
    RaiseError "frmNewScheme.Form_Load"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmNewScheme = Nothing
End Sub

Private Sub tvwSchemes_NodeClick(ByVal Node As MSComctlLib.Node)
    Dim bEnabled As Boolean
    Dim oScheme As CNumScheme
    
    If Me.BasedOnSchemeType <> mpSchemeType_Category Then
        Me.cmbLevels.Text = iGetLevels(Me.BasedOnScheme, _
                                  Me.BasedOnSchemeType)
        If g_bAllowTOCLink Then
'            Me.cmbTOCScheme.ListIndex = _
'                iGetTOCSchemeID() - 1
            On Error Resume Next
            Me.cmbTOCScheme.BoundText = GetTOCField(iGetTOCSchemeID(), _
                                                mpTOCRecField_Name, _
                                                mpSchemeType_TOC)
            If Me.cmbTOCScheme.BoundText = "" Then _
                Me.cmbTOCScheme.SelectedItem = 0
            On Error GoTo 0
        End If
        
'       default checkboxes as per based on scheme (new to 9.8)
        Set oScheme = GetRecord(Me.BasedOnScheme, Me.BasedOnSchemeType)
        Me.chkAdjustSpacing = oScheme.DynamicSpacing
        Me.chkBaseOnNormal = oScheme.DynamicFonts
        
        'description (9.9.5001)
        Me.txtDescription = oScheme.Description
        
'        bEnabled = (Me.BasedOnSchemeType = mpSchemeType_Document)
'
'       enable Dynamic fonts only when 'based on' scheme
'       is document scheme - also check by default
'        With Me.chkBaseOnNormal
'            .Enabled = bEnabled
''            .Value = Abs(Not bEnabled)
'        End With
    End If
End Sub

Private Sub txtSchemeName_GotFocus()
    bEnsureSelectedContent Me.txtSchemeName
End Sub

Private Sub txtSchemeName_KeyPress(KeyAscii As Integer)

    xMsg = "Invalid character.  Names cannot contain any of" & _
        vbCrLf & "the following characters: \ : * ? " & _
        Chr(34) & " < > | ="
        
    If KeyAscii = 8 Then
'       backspace key pressed
        Exit Sub
    ElseIf KeyAscii = Asc("|") Then
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    ElseIf KeyAscii = Asc("*") Then
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    ElseIf KeyAscii = Asc("?") Then
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    ElseIf KeyAscii = Asc("\") Then
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    ElseIf KeyAscii = Asc(":") Then
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    ElseIf KeyAscii = 34 Then
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    ElseIf KeyAscii = Asc("<") Then
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    ElseIf KeyAscii = Asc(">") Then
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    ElseIf KeyAscii = Asc("=") Then
'       Windows allows this character, but we're
'       using as a substitute for "/"
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
'removed character limit in 9.9.3
'    ElseIf Len(Me.txtSchemeName) = 15 Then
'        xMsg = "Scheme names are limited to 15 characters."
'        MsgBox xMsg, vbExclamation, g_xAppName
'        KeyAscii = 0
    End If
End Sub

Private Sub txtSchemeName_LostFocus()
    Me.txtStyleName = xCreateStyleName(Me.txtSchemeName)
End Sub

Private Sub txtStyleName_GotFocus()
    bEnsureSelectedContent Me.txtStyleName
End Sub

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Function iGetTOCSchemeID() As String
'   get linked toc scheme of 'based on' scheme
    iGetTOCSchemeID = GetField(Me.BasedOnScheme, _
                             mpRecField_TOCScheme, _
                             Me.BasedOnSchemeType)
                             
End Function

Private Function xCreateStyleName(ByVal xSchemeName As String) As String
'creates a valid Style Name from the scheme name
    Dim xTemp As String
    Dim xChar As String
    Dim iChar As Integer
    Dim i As Integer
    Dim bIsUnicode As Boolean
    
'   remove non-alphanumeric characters
    For i = 1 To Len(xSchemeName)
        xChar = Mid(xSchemeName, i, 1)
        iChar = Asc(LCase(xChar))
        bIsUnicode = (AscW(xChar) <> Asc(xChar))
        
'       must be numeric, or lowercase alpha -
        If IsNumeric(xChar) Or _
                (iChar >= 97 And iChar <= 122) Or _
                bIsUnicode Then
            xTemp = xTemp & xChar
'           stop concatenation when
'           style name is 10 chars
            If Len(xTemp) = 10 Then
                Exit For
            End If
        End If
    Next i
    
    xCreateStyleName = xMatchFirmSchemeCase(xTemp)
End Function

Private Sub txtStyleName_KeyPress(KeyAscii As Integer)
'allow only alphanumeric characters
    Dim xChar As String
    Dim iSelLength As Integer
    Dim bIsUnicode As Boolean
    
    xChar = LCase(Chr(KeyAscii))
    If KeyAscii = 8 Then
'       backspace key pressed
        Exit Sub
    End If
    
    iSelLength = Me.txtStyleName.SelLength
    bIsUnicode = (AscW(xChar) <> Asc(xChar))
    
    If (Len(Me.txtStyleName) = 10) And (iSelLength = 0) Then
        xMsg = "Style names are limited to 10 characters."
        MsgBox xMsg, vbExclamation, g_xAppName
    ElseIf Not (IsNumeric(xChar) Or _
            (Asc(xChar) >= 97 And Asc(xChar) <= 122) Or _
            bIsUnicode) Then
        xMsg = "Invalid character.  Style names can " & _
               "contain only alphanumeric characters."
        MsgBox xMsg, vbExclamation, g_xAppName
        KeyAscii = 0
    End If
End Sub

