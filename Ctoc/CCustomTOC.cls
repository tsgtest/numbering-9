VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomTOC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'***************************************
'Custom TOC Class
'write any code that customizes the TOC
'here - the names of the methods below
'indicate the point at which the method
'is invoked by MPTOC90!mpTOC.bInsertTOC -
'PLEASE let me know if this is insufficient
'for TOC customization - DF
'***************************************

'use this object to retrieve any dlg
'settings - WARNING: you must be very
'careful when using this object, as it
'is late bound.
Private m_oTOCDlg As Object
Private m_xBP As String
Private m_xBP2 As String
Private m_xHeaderBookmark As String
Private m_xFooterBookmark As String
Private m_xFirmName As String
Private m_xOffice As String
Private m_sTopMargin As Single
Private m_sBottomMargin As Single
Private m_sLeftMargin As Single
Private m_sRightMargin As Single
Private m_sHeaderDistance As Single
Private m_sFooterDistance As Single
Private m_bLinkFooters As Boolean
Private m_xFooterTitle As String
Private m_iPleadingPaperType As Integer
Private m_xUserIni As String
Private m_xAppIni As String
Private m_bTOCSectionInserted As Boolean

Property Get PleadingPaperType() As Integer
    PleadingPaperType = m_iPleadingPaperType
End Property
Property Get BPFile() As String
    BPFile = m_xBP
End Property
Property Get HeaderBookmark() As String
    HeaderBookmark = m_xHeaderBookmark
End Property
Property Get FooterBookmark() As String
    FooterBookmark = m_xFooterBookmark
End Property
Property Get FirmName() As String
    FirmName = m_xFirmName
End Property
Property Get Office() As String
    Office = m_xOffice
End Property
Property Get TopMargin() As Single
    TopMargin = m_sTopMargin
End Property
Property Get BottomMargin() As Single
    BottomMargin = m_sBottomMargin
End Property
Property Get LeftMargin() As Single
    LeftMargin = m_sLeftMargin
End Property
Property Get RightMargin() As Single
    RightMargin = m_sRightMargin
End Property
Property Get HeaderDistance() As Single
    HeaderDistance = m_sHeaderDistance
End Property
Property Get FooterDistance() As Single
    FooterDistance = m_sFooterDistance
End Property
Property Get FooterTitle() As String
    FooterTitle = m_xFooterTitle
End Property
Function lBeforeDialogShow(oTOCDlg As Object) As Long
    On Error GoTo ProcError
'   custom code starts here

    Exit Function
ProcError:
    lBeforeDialogShow = Err.Number
    Exit Function
End Function

Function lAfterDialogShow() As Long
    Dim xBP As String
    Dim xAppPath As String
    
    On Error GoTo ProcError
'   custom code starts here
'   if firm is using multiple boilerplate files, switch to one specified in TOC dialog
    If m_xBP2 <> "" Then
        xBP = System.PrivateProfileString(m_xUserIni, "TOC", "CurrentBoilerplate")
        xAppPath = mpBase.GetAppPath
        If (xBP <> "") And _
                (Dir(xAppPath & "\" & xBP) <> "") Then
            m_xBP = xAppPath & "\" & xBP
        End If
    End If
    
    Exit Function
ProcError:
    lAfterDialogShow = Err.Number
    Exit Function
End Function

Function lBeforeTOCSectionInsert() As Long
    On Error GoTo ProcError
'   custom code starts here

    Exit Function
ProcError:
    lBeforeTOCSectionInsert = Err.Number
    Exit Function
End Function

Function lAfterTOCSectionInsert(rngSection As Word.Range, _
                                Optional bColsReformatted As Boolean) As Long
    Dim oSection As Word.Section
    Dim rngLocation As Word.Range
    Dim oHeader As Word.HeaderFooter
    Dim bSecondTry As Boolean
    
'   custom code starts here
    On Error GoTo ProcError
    
    Set oSection = rngSection.Sections(1)
    
'   check for Mp2k
    If m_bTOCSectionInserted And (g_xMP90Dir <> "") Then
        '9.9.5005 - we now test for existence of mp2k paper when
        'mpCTOC is initialized
        If Not g_oPleadingPaper Is Nothing Then
            With g_oPleadingPaper
'                If .Exists Then
                    '---9.4.0 refresh all the properties.
                    .PleadingPaperDef.ID = .PleadingPaperDef.ID
                    .StartPageNumberingAt = 1
                    .StartingPageNumber = 1
                    .SideBarType = .SideBarType
                    .FirmName = .FirmName
                    .OfficeLocation = .OfficeLocation
                    .CaseNumber = .CaseNumber
                    .CaseNumberSeparator = .CaseNumberSeparator
                    .DocTitle = .DocTitle
                    .State = .State
                    .CustomProperties.RefreshValues
                    .Update oSection.Index, True, True, False
'                End If
            End With
            Set g_oPleadingPaper = Nothing
        End If
    End If
    
'   reformat header to match columns
    If bColsReformatted Then
        For Each oHeader In oSection.Headers
            bSecondTry = False
            Set rngLocation = oHeader.Range
            With rngLocation
labSearch:
                With .Find
                    .ClearFormatting
                    .Forward = True
                    .Text = "Page"
                    .Execute
                    If .Found Then
                        With .Parent
                            .Expand wdParagraph
'                           don't mistake page # in header for page label
                            If .Fields.Count = 0 Then
'                               if multi-column, hide label
                                .Font.Hidden = (oSection.PageSetup _
                                    .TextColumns.Count > 1)
                            Else
                                If Not bSecondTry Then
'                                   look for another instance
                                    bSecondTry = True
                                    .EndOf
                                    GoTo labSearch
                                End If
                            End If
                        End With
                    End If
                End With
            End With
        Next oHeader
    End If
    
    Exit Function
ProcError:
    lAfterTOCSectionInsert = Err.Number
    Exit Function
End Function

Function lAfterTOCFieldInsert(fldTOC As Word.Field) As Long
    On Error GoTo ProcError
'   custom code starts here

    Exit Function
ProcError:
    lAfterTOCFieldInsert = Err.Number
    Exit Function
End Function

Function lBeforeTOCRework(rngTOC As Word.Range) As Long
'runs before the paragraphs of the
'toc are modified by MacPac code
    On Error GoTo ProcError
'   custom code starts here

    Exit Function
ProcError:
    lBeforeTOCRework = Err.Number
    Exit Function
End Function

Function lInTOCRework(rngPara As Word.Range, _
                      xScheme As String, _
                      xExclusions As String, _
                      bIncludeStyles As Boolean, _
                      bIncludeTCEntries As Boolean) As Long
'this function is called X times
'by MPTOC9!mpTOC.bReworkTOC - ie
'it is called once per toc para -
'rngPara points to the current toc
'paragraph - if necessary, expand
'rngPara to whole paragraph (left out
'for speed reasons)
    On Error GoTo ProcError
'   custom code starts here
    
    Exit Function
ProcError:
    lInTOCRework = Err.Number
    Exit Function
End Function
Function lAfterTOCRework() As Long
    On Error GoTo ProcError
'   custom code starts here

    Exit Function
ProcError:
    lAfterTOCRework = Err.Number
    Exit Function
End Function
Function lGetTOCDetail(docTOC As Word.Document) As Long
    On Error GoTo ProcError
'   custom code starts here

    Exit Function
ProcError:
    lGetTOCDetail = Err.Number
End Function

Private Sub Class_Initialize()
    Dim xUserPath As String
    Dim oReg As CRegistry
    Dim xBP As String
    Dim xAppPath As String
    
'   this to avoid running lAfterTOCSectionInsert when section was already in place
    m_bTOCSectionInserted = Not bTOCExists(ActiveDocument)
        
'   check for Mp2k
    g_xMP90Dir = mpBase.GetMacPacAppPath
    If g_xMP90Dir <> "" Then
        g_iSeed = iGetSeed(System.PrivateProfileString(g_xMP90Dir & _
            "MacPac.ini", "General", "Seed"))
    End If
    
    '9.9.5005 - check for mp2k pleading paper at the outset -
    'this will avoid unnecessary footer processing if it exists
    If m_bTOCSectionInserted And (g_xMP90Dir <> "") Then
        On Error Resume Next
        Set g_oPleadingPaper = New MPO.CPleadingPaper
        On Error GoTo 0
        If Not g_oPleadingPaper Is Nothing Then
            If Not g_oPleadingPaper.Exists Then
                Set g_oPleadingPaper = Nothing
            End If
        End If
    End If
    
    xAppPath = mpBase.GetAppPath
    m_xAppIni = xAppPath & "\mpN90.ini"
    xUserPath = Word.System.PrivateProfileString(m_xAppIni, "General", "UserDir")
    If (InStr(UCase(xUserPath), "<USERNAME>") > 0) Or _
            (InStr(UCase(xUserPath), "<USER>") > 0) Then
'       use API to get Windows user name
        xUserPath = GetUserVarPath(xUserPath)
    Else
'       use environmental variable
        xUserPath = GetEnvironVarPath(xUserPath)
    End If
    If Dir(xUserPath & "\mpNumbers.sty") = "" Then
'       use Word's default path
        xUserPath = Word.Application.Options _
            .DefaultFilePath(wdUserTemplatesPath)
    ElseIf Right(xUserPath, 1) = "\" Then
        xUserPath = Left(xUserPath, Len(xUserPath) - 1)
    End If
    m_xUserIni = xUserPath & "\NumTOC.ini"
    
    'GLOG 5299 - added ini key to disable boilerplate pleading paper
    If UCase$(System.PrivateProfileString(m_xAppIni, "TOC", _
            "DisableBoilerplatePleadingPaper")) = "TRUE" Then
        m_iPleadingPaperType = mpPleadingPaperNone
    Else
        m_iPleadingPaperType = iGetPleadingPaperType(ActiveDocument)
    End If
    
    mdlPleadingPaper.bDetail m_iPleadingPaperType, _
        m_xBP, m_xHeaderBookmark, m_xFooterBookmark, m_sTopMargin, _
        m_sBottomMargin, m_sLeftMargin, m_sRightMargin, m_sHeaderDistance, _
        m_sFooterDistance
    
'   get main boilerplate file
    xBP = System.PrivateProfileString(m_xAppIni, "TOC", "BoilerplateName1")
    If xBP <> "" Then
        xBP = "\" & xBP
    Else
       xBP = "\mpnBoilerpl.ate"
    End If
    If Dir(xAppPath & xBP) <> "" Then
        m_xBP = xAppPath & xBP
    Else
        m_xBP = xAppPath & "\bp.doc"
    End If
    
'   see if firm is using multiple boilerplate files
    m_xBP2 = System.PrivateProfileString(m_xAppIni, "TOC", "BoilerplateName2")
        
    m_xFirmName = System.PrivateProfileString(m_xUserIni, "Firm", "Name")
    m_xOffice = System.PrivateProfileString(m_xUserIni, "Firm", "Office")
    
    Set oReg = Nothing
End Sub

Private Sub Class_Terminate()
    Set m_oTOCDlg = Nothing
End Sub
Function bIsCenteredFooter() As Boolean
    ' True if using a three column footer with 1st and 3rd columns equal width
    If m_xFooterBookmark = "zzmpTOCFooter" Or _
            m_xFooterBookmark = "zzmpPleadingFooter_AppTOC" Then
        bIsCenteredFooter = True
    End If
End Function
Function rngGetFirmNameTarget(rngTarget As Word.Range, _
        secTOC As Word.Section) As Word.Range
    Set rngGetFirmNameTarget = mdlPleadingPaper.rngSetFirmNameTarget(rngTarget, secTOC)
End Function

Function bTOCExists(docCurrent As Word.Document) As Boolean
'returns true if there's an existing TOC
    
    Const mpTOCPlaceholder As String = "TABLE OF CONTENTS PLACEHOLDER"
    Dim rngContent As Word.Range
    With docCurrent
        Set rngContent = docCurrent.Content
        bTOCExists = (.Bookmarks.Exists("mpTableOfContents") Or _
            .Bookmarks.Exists("TableOfContents") Or _
            .Bookmarks.Exists("PTOC") Or _
            .Bookmarks.Exists("TOC") Or _
            rngContent.Find.Execute(mpTOCPlaceholder) Or _
            .TablesOfContents.Count)
    End With
End Function


