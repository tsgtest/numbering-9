VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CApp"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Property Get SkipTOCMarking() As Long
     SkipTOCMarking = mpSkipTOCMarking
End Property

Public Property Get RemoveTOCMarking() As Long
     RemoveTOCMarking = mpRemoveTOCMarking
End Property

Public Property Get TopMarginTOC() As Single
     TopMarginTOC = mpTopMarginTOC
End Property

Public Property Get BottomMarginTOC() As Single
     BottomMarginTOC = mpBottomMarginTOC
End Property

Public Property Get LeftMarginTOC() As Single
     LeftMarginTOC = mpLeftMarginTOC
End Property

Public Property Get RightMarginTOC() As Single
     RightMarginTOC = mpRightMarginTOC
End Property

Public Property Get HeaderDistTOC() As Single
     HeaderDistTOC = mpHeaderDistTOC
End Property

Public Property Get FooterDistTOC() As Single
     FooterDistTOC = mpFooterDistTOC
End Property

Public Property Get PleadingTOCTopMargin() As String
     PleadingTOCTopMargin = mpPleadingTOCTopMargin
End Property

Public Property Get Pleading26TOCTopMargin() As String
     Pleading26TOCTopMargin = mpPleading26TOCTopMargin
End Property

Public Property Get SchemeTypeTCCodeBased() As Integer
     SchemeTypeTCCodeBased = mpSchemeTypeTCCodeBased
End Property

Public Property Get SchemeTypeStyleBased() As Integer
     SchemeTypeStyleBased = mpSchemeTypeStyleBased
End Property

