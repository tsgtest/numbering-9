VERSION 5.00
Begin VB.Form frmSchemeSelector 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Select Scheme to Edit"
   ClientHeight    =   2115
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3405
   Icon            =   "frmSchemeSelector.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2115
   ScaleWidth      =   3405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2167
      TabIndex        =   2
      Top             =   1670
      Width           =   1035
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1047
      TabIndex        =   1
      Top             =   1670
      Width           =   1025
   End
   Begin VB.ListBox lstSchemes 
      Height          =   1230
      Left            =   202
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   200
      Width           =   3000
   End
End
Attribute VB_Name = "frmSchemeSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub cmdCancel_Click()
    m_bCancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub cmdOK_Click()
    m_bCancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim xActive As String
    
    m_bCancelled = True
    
    'iGetSchemes sorts by display name, so order of list box and g_xDSchemes
    'will always match - no need for item data
    iGetSchemes g_xDSchemes(), mpSchemeType_Document
    xActive = xActiveScheme(ActiveDocument)
    
    For i = 0 To UBound(g_xDSchemes)
        With Me.lstSchemes
            .AddItem g_xDSchemes(i, 1)
            If g_xDSchemes(i, 0) = xActive Then _
                .ListIndex = i
        End With
    Next i
End Sub
