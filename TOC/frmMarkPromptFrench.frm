VERSION 5.00
Begin VB.Form frmMarkPromptFrench 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Prompt for Mark"
   ClientHeight    =   2385
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5145
   Icon            =   "frmMarkPromptFrench.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2385
   ScaleWidth      =   5145
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      Height          =   315
      Left            =   3352
      TabIndex        =   5
      Top             =   1980
      Width           =   1500
   End
   Begin VB.CommandButton cmdYesAll 
      Caption         =   "Oui pour &tout"
      Height          =   315
      Left            =   392
      TabIndex        =   3
      Top             =   1980
      Width           =   1400
   End
   Begin VB.CommandButton cmdNoAll 
      Caption         =   "N&on pour tout"
      Height          =   315
      Left            =   1872
      TabIndex        =   4
      Top             =   1980
      Width           =   1400
   End
   Begin VB.CommandButton cmdYes 
      Caption         =   "&Oui"
      Default         =   -1  'True
      Height          =   315
      Left            =   392
      TabIndex        =   0
      Top             =   1530
      Width           =   1400
   End
   Begin VB.CommandButton cmdNo 
      Caption         =   "&Non"
      Height          =   315
      Left            =   1872
      TabIndex        =   1
      Top             =   1530
      Width           =   1400
   End
   Begin VB.CommandButton cmdSkip 
      Caption         =   "&Ignorer paragraphe"
      Height          =   315
      Left            =   3352
      TabIndex        =   2
      Top             =   1530
      Width           =   1500
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   60
      Picture         =   "frmMarkPromptFrench.frx":058A
      ScaleHeight     =   615
      ScaleWidth      =   645
      TabIndex        =   6
      Top             =   150
      Width           =   645
   End
   Begin VB.Label lblMessage 
      Caption         =   "Label1"
      Height          =   1275
      Left            =   720
      TabIndex        =   7
      Top             =   150
      Width           =   4275
   End
End
Attribute VB_Name = "frmMarkPromptFrench"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public m_iRetval As Integer
Private m_iMode As mpMarkingModes
Private m_bIsAmbiguousHeading As Boolean

Private Sub cmdCancel_Click()
    Me.Hide
    m_iRetval = vbCancel
End Sub

Private Sub cmdNo_Click()
    Me.Hide
    m_iRetval = vbRetry
End Sub

Private Sub cmdNoAll_Click()
    Me.Hide
    m_iRetval = vbNo
End Sub

Private Sub cmdSkip_Click()
    Me.Hide
    m_iRetval = vbIgnore
End Sub

Private Sub cmdYes_Click()
    Me.Hide
    m_iRetval = vbAbort
End Sub

Private Sub cmdYesAll_Click()
    Me.Hide
    m_iRetval = vbYes
End Sub

Private Sub Form_Load()
    Dim xMsg As String
    With Me
        .Caption = "Num�rotation MacPac"
        If m_bIsAmbiguousHeading Then
            xMsg = "D�finir la phrase s�lectionn�e comme Titre ?"
            If m_iMode = mpMarkingMode_TCCodes Then
                'this text isn't appropriate for style separators
                xMsg = xMsg & vbCrLf & vbCrLf & "Si vous s�lectionnez Non, une autre tentative permettra de trouver un titre appropri� dans le paragraphe.  Si aucun ne peut �tre trouv�, le paragraphe entier sera marqu�/format� comme titre."
            End If
        ElseIf m_iMode = mpMarkingMode_StyleSeparators Then
            xMsg = "Remplacer les codes TM dans les paragraphes MacPac num�rot�s par des s�parateurs de style?"
        Else
            xMsg = "Remplacer les s�parateurs de style dans les paragraphes MacPac num�rot�s par des codes TM?"
        End If
        
        .lblMessage = xMsg
        
        'shrink dialog to account for shorter message
        If (m_iMode = mpMarkingMode_StyleSeparators) Or Not m_bIsAmbiguousHeading Then
            .lblMessage.Top = 250
            .cmdSkip.Top = .cmdSkip.Top - 750
            .cmdCancel.Top = .cmdCancel.Top - 750
            .cmdNo.Top = .cmdNo.Top - 750
            .cmdNoAll.Top = .cmdNoAll.Top - 750
            .cmdYes.Top = .cmdYes.Top - 750
            .cmdYesAll.Top = .cmdYesAll.Top - 750
            .cmdSkip.Left = .cmdSkip.Left + 225
            .cmdCancel.Left = .cmdCancel.Left + 225
            .cmdNo.Left = .cmdNo.Left + 225
            .cmdNoAll.Left = .cmdNoAll.Left + 225
            .cmdYes.Left = .cmdYes.Left + 225
            .cmdYesAll.Left = .cmdYesAll.Left + 225
            .Height = .Height - 750
        End If
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmMarkPromptFrench = Nothing
End Sub

Public Property Let MarkingMode(iMode As mpMarkingModes)
    m_iMode = iMode
End Property

Public Property Get MarkingMode() As mpMarkingModes
    MarkingMode = m_iMode
End Property

Public Property Let IsAmbiguousHeading(bIsAmbiguousHeading As Boolean)
    m_bIsAmbiguousHeading = bIsAmbiguousHeading
End Property

Public Property Get IsAmbiguousHeading() As Boolean
    IsAmbiguousHeading = m_bIsAmbiguousHeading
End Property

