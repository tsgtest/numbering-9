Attribute VB_Name = "mdlVars"
'global environment vars
Public g_oWord As Word.Application
Public g_envMpApp As mpAppEnvironment
Public g_envMpDoc As mpDocEnvironment
Public g_bPreviouslyInitialized As Boolean
Public g_bPreventDocChangeEvent As Boolean
Public g_dlgScheme As VB.Form
Public g_bIsAdmin As Boolean
Public g_sStyleArea As Single
Public g_bLeaveDocDirty As Boolean
Public g_bAdminIsDirty As Boolean
Public g_bForceWindowsInTaskbar As Boolean

'dialogs/shift-tabbing
'Dialog
Public g_bInVBALostFocus As Boolean
Public g_ActivatingTabProgrammatically As Boolean
Public g_VShiftDown As Boolean
Public g_bTabbed As Boolean
Public g_xSelNode As String
Public g_oStatus As mpSCR.CStatus

'Options
Public g_bAllowSchemeSharing As Boolean
Public g_bAllowSchemeEdit As Boolean
Public g_bAllowSchemeNew As Boolean
Public g_bAllowPublicSchemes As Boolean
Public g_bAllowTOCLink As Boolean
Public g_bShowPersonalSchemes As Boolean
Public g_bAllowFullHeadingStyleUse As Boolean
Public g_bDefaultToNumContinue As Boolean
Public g_iUpgradeFrom As Integer
Public g_xRemoveNumberStyle As String
Public g_bResetFonts As Boolean
Public g_bApplyHeadingColor As Boolean
Public g_bDocumentChange As Boolean
Public g_xListNum As String
Public g_bNoSchemeIndents As Boolean
Public g_bAcceptAllFlags As Boolean
Public g_bCodeForMSBug As Boolean
Public g_bPost97AdminWarning As Boolean
Public g_bPreserveRestarts As Boolean
Public g_bGenerateBufferFromCopy As Boolean
Public g_lActiveSchemeIcon As Long
Public g_bIsMP10DesignMode As Boolean
Public g_bIsMP10 As Boolean
Public g_xBufferTemplate As String

'paths
Public g_xStartPath As String
Public g_xWorkgroupPath As String
Public g_xUserPath As String
Public g_xAdminSchemesPath As String
Public g_xMPBPath As String
Public g_xHelpPath As String

'files
Public g_xBP As String
Public g_xUserIni As String
Public g_xFirmIni As String
Public g_xPNumSty As String
Public g_xFNumSty As String
Public g_xFNum80Sty As String
Public g_docPreview As Word.Document
Public g_docStart As Word.Document
Public g_xMPNDot As String
Public g_xMPNAdminDot As String
Public g_xMPNConvertDot As String
Public g_oPNumSty As Word.Template
Public g_oFNumSty As Word.Template

'misc
Public bRet As Variant
Public lRet As Long
Public xMsg As String
Public iUserChoice  As Integer
Public xFirmName As String
Public datTestStartTime As Date
Public datTestEndTime As Date
Public xRestart As Variant
Public bfrmInitializing As Boolean
Public bPleadingDBoxActive As Boolean
Public g_xPreviewLevelText As String

'Misc
Public g_xBasedOn As String
Public g_xPSchemes() As String
Public g_xFSchemes() As String
Public g_xDSchemes() As String
Public g_xNextParaStyles() As String
Public g_xUnderlineFormats() As String
Public g_xPermittedFonts() As String
Public g_oCurScheme As CNumScheme
Public g_xCurDest As String
Public g_bCurSchemeIsHeading As Boolean
Public g_sNumberFontSizes() As Single
Public g_iDMS As Integer
Public g_xMP90Dir As String
Public g_xSyncContStyles() As String
Public g_bIsXP As Boolean
Public g_xPleadingSchemes() As String
Public g_bAlwaysAdjustSpacing As Boolean
Public g_iWordVersion As mpWordVersions
Public g_xSupplementalNumberStyles() As String
Public g_bSchemeSelectionOnly As Boolean
Public g_bXMLSupport As Boolean
Public g_bRefreshTaskPane As Boolean
Public g_lUILanguage As Long
Public g_bOrganizerSavePrompt As Boolean
Public g_bPreserveUndoList As Boolean
Public g_xHelpFiles() As String
Public g_bCreateUnlinkedStyles As Boolean
Public g_bPromptBeforeAutoRelinking As Boolean
Public g_xTimerDelay As String
Public g_lWinMajorVersion As Long
Public g_lWinMinorVersion As Long
Public g_xAppName As String
Public g_lTrackChangesWarningThreshold As Long
Public g_xFavoriteSchemes() As String
Public g_iLoadContStyles As mpLoadContStyles '9.9.6004
Public g_lRulerDisplayDelay_New As Long '9.9.6012
Public g_lRulerDisplayDelay_Edit As Long '9.9.6012
Public g_lRulerDisplayDelay_EditDirect As Long '9.9.6012
Public g_lDefaultZoomPercentage As Long '9.9.6012
Public g_bCollapseRibbon As Boolean '9.9.6014

'Forms
Public g_bSchemeIsDirty As Boolean
Public g_iSchemeEditMode As Integer

'TOC
Public g_xTCPrefix As String

