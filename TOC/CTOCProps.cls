VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTOCProps"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTOCProps Class
'   created 4/23/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   CTOCProps Class - the class that contains and implements
'   scheme-level format properties of a toc
'**********************************************************
Public Enum mpTOCLevels
    mpTOCLevels_UseCurrent = -1
    mpTOCLevels_All = 0
    mpTOCLevels_1 = 1
    mpTOCLevels_Subsequent = 2
    mpTOCLevels_None = 3
End Enum

Public Enum mpLineSpacing
    mpLineSpacing_UseCurrent = -1
    mpLineSpacing_1 = 0
    mpLineSpacing_15 = 1
    mpLineSpacing_2 = 2
End Enum

Private Const mpTOCUseCurrent As Integer = -1

Private m_iLineSpacing As Word.WdLineSpacing
Private m_iAllCaps As MPTOC90.mpTOCLevels
Private m_iPageNumbers As MPTOC90.mpTOCLevels
Private m_iDotLeaders As MPTOC90.mpTOCLevels
Private m_bUseStepIndents As Boolean
Private m_bCenterLevel1 As Boolean
Private m_sSpaceBefore As Single
Private m_sSpaceBetween As Single

'**********************************************************
'PROPERTIES
'**********************************************************
Public Property Let LineSpacing(iNew As mpLineSpacing)
    m_iLineSpacing = iNew
End Property

Public Property Get LineSpacing() As mpLineSpacing
    LineSpacing = m_iLineSpacing
End Property

Public Property Let AllCaps(iNew As MPTOC90.mpTOCLevels)
    m_iAllCaps = iNew
End Property

Public Property Get AllCaps() As MPTOC90.mpTOCLevels
    AllCaps = m_iAllCaps
End Property

Public Property Let PageNumbers(iNew As MPTOC90.mpTOCLevels)
    m_iPageNumbers = iNew
End Property

Public Property Get PageNumbers() As MPTOC90.mpTOCLevels
    PageNumbers = m_iPageNumbers
End Property

Public Property Let DotLeaders(iNew As MPTOC90.mpTOCLevels)
    m_iDotLeaders = iNew
End Property

Public Property Get DotLeaders() As MPTOC90.mpTOCLevels
    DotLeaders = m_iDotLeaders
End Property

Public Property Let UseStepIndents(bNew As Boolean)
    m_bUseStepIndents = bNew
End Property

Public Property Get UseStepIndents() As Boolean
    UseStepIndents = m_bUseStepIndents
End Property

Public Property Let CenterLevel1(bNew As Boolean)
    m_bCenterLevel1 = bNew
End Property

Public Property Get CenterLevel1() As Boolean
    CenterLevel1 = m_bCenterLevel1
End Property

Public Property Let SpaceBeforeLevel(sNew As Single)
    m_sSpaceBefore = sNew
End Property

Public Property Get SpaceBeforeLevel() As Single
    SpaceBeforeLevel = m_sSpaceBefore
End Property

Public Property Let SpaceBetweenLevel(sNew As Single)
    m_sSpaceBetween = sNew
End Property

Public Property Get SpaceBetweenLevel() As Single
    SpaceBetweenLevel = m_sSpaceBetween
End Property

'**********************************************************
'METHODS
'**********************************************************
Public Function TOCRange() As Word.Range
    Set TOCRange = rngGetTOC()
End Function


Sub DoStepIndents()
'formats current toc range with hanging step indents
    Dim i As Integer
    Dim iLevel As Integer
    Dim paraTOC As Word.Paragraph
    Dim xTabFlag(8) As String
    Dim sHPos As Single
    Dim xStartRange As String
    
    If (Me.TOCRange Is Nothing) Then
        Err.Number = mpTOCError_NoTOCFound
        RaiseError "MPTOC90.CTOC.DoStepIndents"
        Exit Sub
    End If
    
'---cycle through TOC paragraphs
    For Each paraTOC In Me.TOCRange.Paragraphs
        With paraTOC.Range
'           get level
            iLevel = Right(.Style, 1)

'           step indent - adjust hanging indent of style based on
'           first representative entry for this level

'           determine if it's a numbered entry and
'           whether level has already been checked
            If lCountChrs(.Text, vbTab) > 1 And _
                    xTabFlag(iLevel - 1) = "" Then
'               set flag
                xTabFlag(iLevel - 1) = "X"
'               get start position of text after tab
                .StartOf
                .MoveStartUntil vbTab
                .Move wdCharacter
                .Select
                sHPos = Selection.Information(wdHorizontalPositionRelativeToTextBoundary)
'               if past left indent, needs adjusting
                If sHPos > .ParagraphFormat.LeftIndent Then
                    .Move wdCharacter, -1
                    .Select
'                   locate the .25" sector of document that contains end of number;
'                   tab s/b .5" from start of that sector
                    sHPos = Selection.Information(wdHorizontalPositionRelativeToTextBoundary)
                    xStartRange = LTrim(Partition(sHPos, 0, 612, 18))
                    xStartRange = Left(xStartRange, InStr(xStartRange, ":") - 1)
'                   redefine style
                    With ActiveDocument.Styles(.Style).ParagraphFormat
                        .LeftIndent = CSng(xStartRange) + 36
                        .FirstLineIndent = (36 * (iLevel - 1)) - .LeftIndent
                    End With
                End If
                .Expand wdParagraph
            End If
        End With
    Next paraTOC
End Sub
'**********************************************************
'EVENT PROCS
'**********************************************************
'**********************************************************
'INTERNAL PROCS
'**********************************************************
Private Function rngGetTOC() As Word.Range
'returns location of existing TOC -

    With Word.ActiveDocument
'       first check for existing bookmark, then for
'       placeholder, then for existing TOC -
        If .Bookmarks.Exists("mpTableOfContents") Then
            Set rngGetTOC = .Bookmarks("mpTableOfContents").Range
        ElseIf .Bookmarks.Exists("TableOfContents") Then
            Set rngGetTOC = .Bookmarks("TableOfContents").Range
        ElseIf .Bookmarks.Exists("PTOC") Then
            Set rngGetTOC = .Bookmarks("PTOC").Range
        ElseIf .Bookmarks.Exists("TOC") Then
            Set rngGetTOC = .Bookmarks("TOC").Range
        ElseIf .TablesOfContents.Count Then
'           check for existing TOC
            Set rngGetTOC = .TablesOfContents(1).Range
        Else
            Set rngGetTOC = Nothing
        End If
    End With
End Function

Function EditCustomTOCScheme() As Boolean
'displays TOC customization form.
'if xScheme is "custom", sets defaults in ini;
'otherwise, modifies document scheme.
    Dim i As Integer
    Dim xStyle As String
    Dim styTOC As Word.Style

    Application.ScreenUpdating = False

    With frmEditTOC
        .EditCustomScheme = True
        .Show vbModal

        If .Cancelled Then
            Unload frmEditTOC
            Exit Function
        End If

'       set defaults
        bAppSetUserINIValue "TOC", "LineSpacing", _
            .cmbLineSpacing.Bookmark
        bAppSetUserINIValue "TOC", "PointsBefore", _
            .spnBefore.Value
        bAppSetUserINIValue "TOC", "PointsBetween", _
            .spnBetween.Value
        bAppSetUserINIValue "TOC", "PageNumbers", _
            .cmbPageNums.Bookmark
        bAppSetUserINIValue "TOC", "DotLeaders", _
            .cmbDotLeaders.Bookmark
        bAppSetUserINIValue "TOC", "AllCaps", _
            .cmbAllCaps.Bookmark
        bAppSetUserINIValue "TOC", "CenterLevel1", _
            .chkCenterLevel1
        bAppSetUserINIValue "TOC", "DefaultToCustom", _
            .chkDefaultScheme
    End With

    Unload frmEditTOC
    Application.ScreenUpdating = True
End Function

Function ShowEditTOCDialog() As Boolean
'returns true if not cancelled or erred
    On Error GoTo ProcError
    
    Application.ScreenUpdating = False
    
    With frmEditTOC
        .EditCustomScheme = False
        .Show vbModal
        
        If Not .Cancelled Then
            If Len(.cmbAllCaps) Then
                Me.AllCaps = .cmbAllCaps.Bookmark
            Else
                Me.AllCaps = mpTOCLevels_UseCurrent
            End If
            
            If Len(.cmbDotLeaders) Then
                Me.DotLeaders = .cmbDotLeaders.Bookmark
            Else
                Me.DotLeaders = mpTOCLevels_UseCurrent
            End If
            
            If Len(.cmbLineSpacing) Then
                Me.LineSpacing = .cmbLineSpacing.Bookmark
            Else
                Me.LineSpacing = mpTOCLevels_UseCurrent
            End If
            
            If Len(.cmbPageNums) Then
                Me.PageNumbers = .cmbPageNums.Bookmark
            Else
                Me.PageNumbers = mpTOCLevels_UseCurrent
            End If
            
'            If Len(.txtBefore) Then
'                Me.SpaceBeforeLevel = .txtBefore
'            Else
'                Me.SpaceBeforeLevel = mpTOCLevels_UseCurrent
'            End If
'
'            If Len(.txtBetween) Then
'                Me.SpaceBetweenLevel = .txtBetween
'            Else
'                Me.SpaceBetweenLevel = mpTOCLevels_UseCurrent
'            End If

            Me.SpaceBeforeLevel = .spnBefore.Value
            Me.SpaceBetweenLevel = .spnBetween.Value
            
            Me.CenterLevel1 = (.chkCenterLevel1 = vbChecked)
            
            ShowEditTOCDialog = True
        End If
    End With
    Unload frmEditTOC
    Exit Function
ProcError:
    Unload frmEditTOC
    RaiseError "MPTOC90.CTOC.ShowEditTOCDialog"
    Exit Function
End Function

Sub Modify()
'displays TOC customization form and
'modifies the active document TOC scheme.
    Dim i As Integer
    Dim xStyle As String
    Dim styTOC As Word.Style
    
    On Error GoTo ProcError
    
    If ShowEditTOCDialog() Then
'       modify TOC styles and doc props
        For i = 1 To 9
            xStyle = xTranslateTOCStyle(i)
            Set styTOC = ActiveDocument.Styles(xStyle)
                
'           paragraph
            With styTOC.ParagraphFormat
                If Me.LineSpacing <> mpLineSpacing_UseCurrent Then _
                    .LineSpacingRule = Me.LineSpacing
                If Me.SpaceBetweenLevel <> mpTOCUseCurrent Then _
                    .SpaceAfter = Me.SpaceBetweenLevel
                    
                Select Case Me.DotLeaders
                    Case mpTOCLevels_All
                        .TabStops(1).Leader = 1
                    Case mpTOCLevels_1
                        .TabStops(1).Leader = Abs(i = 1)
                    Case mpTOCLevels_Subsequent
                        .TabStops(1).Leader = Abs(i > 1)
                    Case mpTOCLevels_None
                        .TabStops(1).Leader = 0
                End Select
                
                If Me.CenterLevel1 And (i = 1) Then
                    .Alignment = wdAlignParagraphCenter
                End If
            End With
                
'           font
            Select Case Me.AllCaps
                Case mpTOCLevels_All
                    styTOC.Font.AllCaps = True
                Case mpTOCLevels_1
                    styTOC.Font.AllCaps = (i = 1)
                Case mpTOCLevels_Subsequent
                    styTOC.Font.AllCaps = (i > 1)
                Case mpTOCLevels_None
                    styTOC.Font.AllCaps = False
            End Select
        Next i
        
        Dim paraP As Word.Paragraph
        Dim rngP As Word.Range
        For Each paraP In Me.TOCRange.Paragraphs
            Set rngP = paraP.Range
            Me.DoCentering rngP
            Me.DoPageNumbers rngP
            Me.DoSpaceBeforeLevel rngP
        Next paraP
            
        If Me.UseStepIndents Then
            Me.DoStepIndents
        End If
        
    End If
    Unload frmEditTOC
    Application.ScreenUpdating = True
    Exit Sub
ProcError:
    Unload frmEditTOC
    RaiseError "MPTOC90.mpTOC.Modify"
    Exit Sub
End Sub


Function DoPageNumbers(rngScope As Word.Range)
    Dim iLevel As Integer
    Dim bIsCentered As Boolean
    Dim xTrailingChar As String
    Dim paraTOC As Word.Paragraph
    Dim bOmitPageNumber As Boolean
    Dim xIniValue As String
    
'---cycle through TOC paragraphs
    For Each paraTOC In rngScope.Paragraphs
        With paraTOC.Range
'           get level
            iLevel = Right(.Style, 1)

'           get alignment
            bIsCentered = (.ParagraphFormat.Alignment = wdAlignParagraphCenter)
            
'           delete/format page numbers
            Select Case Me.PageNumbers
                Case mpTOCLevels_All
                    bOmitPageNumber = False
                Case mpTOCLevels_1
                    bOmitPageNumber = (iLevel > 1)
                Case mpTOCLevels_Subsequent
                    bOmitPageNumber = (iLevel = 1)
                Case mpTOCLevels_None
                    bOmitPageNumber = True
            End Select
            
            If bOmitPageNumber Then
'               remove page # and tab
                .EndOf
                .Move wdCharacter, -1
                .MoveStartUntil vbTab, wdBackward
                .MoveStart wdCharacter, -1
                .Delete
            Else
'               if centered with page no, replace last tab with 2 spaces
                If bIsCentered Then
                    .EndOf
                    .MoveUntil vbTab, wdBackward
                    .MoveStart wdCharacter, -1
                    .Text = Space(2)
                Else
'                   if any EOL's are not at the right tab position,
'                   then add a tab using tab set as determination
                    With .Characters.Last
                        .Select
                        If (Int(Selection.Information(wdHorizontalPositionRelativeToPage)) + 1 < _
                            Int(Selection.ParagraphFormat.TabStops(1).Position)) And _
                            Selection.Information(wdHorizontalPositionRelativeToPage) > -1 Then
                                .MoveEndUntil vbTab, wdBackward
                                .InsertAfter vbTab
                        End If
                    End With
                End If
            End If
            .Expand wdParagraph
        End With
    Next paraTOC
End Function

Function DoCentering(rngScope As Word.Range)
    Dim iLevel As Integer
    Dim bIsCentered As Boolean
    Dim xTrailingChar As String
    Dim paraTOC As Word.Paragraph
    
'---cycle through paragraphs
    For Each paraTOC In rngScope.Paragraphs
        With paraTOC.Range
'           get level
            iLevel = Right(.Style, 1)

'           get alignment
            bIsCentered = .ParagraphFormat.Alignment
            
            If iLevel = 1 And bIsCentered Then
'               base # of shift-returns on line spacing
                With .ParagraphFormat
                    If .LineSpacingRule = wdLineSpaceDouble Or _
                            .LineSpacing > 14 Then
                        xTrailingChar = Chr(11)
                    Else
                        xTrailingChar = String(2, 11)
                    End If
                End With
'               replace remaining tab with shift-return(s)
                .MoveEnd wdCharacter, -1
                .Text = xSubstitute(.Text, vbTab, xTrailingChar)
            End If
        End With
    Next paraTOC
End Function

Function DoSpaceBeforeLevel(rngScope As Word.Range)
    Dim iLevel As Integer
    Dim paraTOC As Word.Paragraph
    Dim xSpaceBefore As String
    Dim bAtStartOfTOC As Boolean
    Dim bNewGroup As Boolean
    
    If Me.SpaceBeforeLevel <> mpTOCUseCurrent Then
'       cycle through paragraphs
        For Each paraTOC In rngScope.Paragraphs
            With paraTOC.Range
'               get level
                iLevel = Right(.Style, 1)
    
'               if first para of new level,
'               adjust space after previous paragraph
                bAtStartOfTOC = (.Start = Me.TOCRange.Start)
                bNewGroup = (.Style <> .Previous(wdParagraph).Style)
                
                If (Not bAtStartOfTOC) And bNewGroup Then
                    .Previous(wdParagraph).ParagraphFormat _
                        .SpaceAfter = Me.SpaceBeforeLevel
                End If
            End With
        Next paraTOC
    End If
End Function


