Attribute VB_Name = "mdlRange"
Option Explicit

Function rngGetField(rngFieldCode As Word.Range) As Word.Range
'returns the range of the field
'whose code is rngFieldCode

    Dim lStartPos As Long
    Dim lEndPos As Long
    
    With rngFieldCode
        lStartPos = .Start
        lEndPos = .End
    End With
    
    rngFieldCode.SetRange lStartPos - 1, lEndPos + 1
    Set rngGetField = rngFieldCode
End Function

Public Function bUnderLineHeadingToggle() As Boolean
' converted from WordBasic code in MacPac 6/7.
' eventually should be rewritten -
' partially rewritten 1/11/98 - df

    Dim ShowAllStart As Boolean
    Dim IsUnderlined As Boolean
    Dim CurPosInInches As Single
    Dim bUnderlined As Boolean
    Dim bSelectEOPara As Boolean
    Dim rngLocation As Word.Range
    Dim bIsOneLineHanging As Boolean
    Dim iTabPosition As Integer
    Dim iFirstTabStop As Integer
    Dim iNumTabStops As Integer
    Dim tabExisting As TabStop
    Dim rngStart As Word.Range
    Dim i As Integer
    Dim rngShiftReturn As Word.Range
    Dim iVPos
    Dim xHiddenText As String
    Dim bIsField As Boolean
    Dim oPara As Word.Paragraph
    Dim bShowHidden As Boolean 'GLOG 5205
    Dim bParaAdded As Boolean
    
    Application.ScreenUpdating = False
    
    'GLOG 5205
    bShowHidden = ActiveWindow.View.ShowHiddenText
    ActiveWindow.View.ShowHiddenText = False
    
    Set rngStart = Selection.Range
    
    'GLOG 5273 - prevent issues at end of content control
    If g_iWordVersion >= mpWordVersion_2007 Then _
        bParaAdded = mdlCC.AddTempParaToContentControl(rngStart)
    
    With rngStart.Paragraphs(1).Range
        If rngStart.Start = .End - 1 And _
            rngStart.End = .End - 1 Then
            bSelectEOPara = True
        End If
    End With
    
    With WordBasic
    '   get showall
        ShowAllStart = .ShowAll()
    
        'GLOG 5204 - if in style separator paragraph, move to after style separator
        If g_iWordVersion > mpWordVersion_XP Then
            ActiveWindow.View.ShowAll = True
            If mdlWordXML.IsStyleSeparator(Selection.Paragraphs(1)) Then
                Selection.MoveStart wdParagraph
            End If
            ActiveWindow.View.ShowAll = False
        End If
    
    '   test for current underlining -
    '   remove if it exists
        .WW7_EditGoTo "\Para"
    
        'if centered, remove any hidden text at the end of the paragraph to
        'avoid errors in bUnderlineToLengthOfLongest(), which needs to be run with
        'ShowAll=false, and ensure that hard spaces get removed with underlining
        Set oPara = Selection.Paragraphs(1)
        If Selection.ParagraphFormat.Alignment = wdAlignParagraphCenter Then _
            RemoveHiddenEndOfPara oPara, xHiddenText, bIsField
                
    '   trim trailing para from selection if necessary
        If Right(Selection, 1) = vbCr Or _
                Asc(Right(Selection, 1)) = 7 Then '*cell marker
            .CharLeft 1, 1
        End If
    
    '   select last line
        .CharRight
        .StartOfLine 1
        
    '   check for one-line hanging indent (e.g. reline)
        With Selection
            If .Characters.Last <> vbTab And _
                    .ParagraphFormat.FirstLineIndent < 0 And _
                    iTabPosition > 0 And Left(.Text, 1) = vbTab Then
                bIsOneLineHanging = True
                .MoveStart wdCharacter, iTabPosition
            End If
        End With
        
    '   remove underlining, hard spaces,
    '   trailing tab and last tab marker'  if they exist - then exit
        IsUnderlined = Selection.Range.Underline
        If IsUnderlined Then
            bEditFindReset
            .EditReplace Find:="^s", Replace:="", ReplaceAll:=1
            .EndOfLine
            .CharLeft 1, 1
            If Selection = vbTab Then
                .WW6_EditClear
                CurPosInInches = (.SelInfo(7) / 1440)
                Selection.Move wdCharacter, -1
                
                While Right(Selection, 1) = vbTab
                    Selection.Delete
                    Selection.Move wdCharacter, -1
                Wend

'--             clear last tab the VBA way
                With Selection.ParagraphFormat
                    For i = .TabStops.Count To 1 Step -1
                        With .TabStops(i)
                            If .CustomTab And .Alignment = wdAlignTabRight Then
                                .Clear
                                Exit For
                            End If
                        End With
                    Next i
                    
'                   check for numbering - if trailing tab
'                   for number, skip first tab stop
                    iFirstTabStop = 1
                    With Selection.Range.ListFormat
                        If .ListType <> wdListNoNumbering Then
                            'GLOG 5205 - prevent error when there's a list num in
                            'non-numbered paragraph
                            If Not .ListTemplate Is Nothing Then
                                If .ListTemplate.ListLevels(.ListLevelNumber) _
                                        .TrailingCharacter = wdTrailingTab Then
                                    iFirstTabStop = 2
                                End If
                            End If
                        End If
                    End With
                    
                    iNumTabStops = .TabStops.Count
                    On Error Resume Next
                    For i = iNumTabStops To iFirstTabStop Step -1
                        If (.TabStops(i).Position Mod 36 = 0) And _
                                .TabStops(i).Position <> .LeftIndent Then
                            .TabStops(i).Clear
                        End If
                    Next i
                    On Error GoTo 0
                End With
            End If
            
            'remove shift-return added by us to preserve correct wrapping
            'in Asian/complex script environments, which is identifiable by
            'the space preceding it, ensuring that this has no effect on
            'how the text wraps
            With Selection
                Set rngLocation = .Range
                .Expand wdParagraph
                .MoveEnd wdCharacter, -1
                .Collapse wdCollapseEnd
                .MoveUp wdLine
                .Expand wdLine
                With .Characters.Last
                    If .Text = vbVerticalTab Then
                        If .Previous(wdCharacter) = " " Then
                            iVPos = .Next(wdCharacter) _
                                .Information(wdVerticalPositionRelativeToPage)
                            .Delete
                            If .Next(wdCharacter) _
                                .Information(wdVerticalPositionRelativeToPage) < iVPos Then
                                'next character has moved up - restore the shift-return
                                .InsertAfter vbVerticalTab
                            End If
                        End If
                    End If
                End With
            End With
            
            '2/10/09 - restore selection - we were previously removing underlining
            'from the previous paragraph when the target paragraph was just one line
            rngLocation.Select
                        
            'remove underline
            Selection.Paragraphs(1).Range.Underline = wdUnderlineNone
        Else
            '*c
            If Selection.ParagraphFormat.Alignment = wdAlignParagraphRight And _
                    Selection.Information(wdWithInTable) Then
                Selection.Font.Underline = wdUnderlineSingle
            Else
                If bIsOneLineHanging Then
                    Set rngLocation = Selection.Range
                Else
                    Set rngLocation = Selection.Paragraphs(1).Range
                    rngLocation.MoveEnd wdCharacter, -1
                End If
                
                bUnderlined = bUnderlineToLengthOfLongest(rngLocation)
            End If
        End If
    
                
        'restore hidden text
        If xHiddenText <> "" Then _
            RestoreHiddenEndOfPara oPara, xHiddenText, bIsField
    
    '   ensure insertion point is at end of paragraph
        .WW7_EditGoTo "\Para"
    
    '   trim trailing para from selection if necessary
        If Right(Selection, 1) = vbCr Then _
            .CharLeft 1, 1
        .CharRight
        .Underline 0
    
    '   set environment
        .ShowAll ShowAllStart
        bEditFindReset
        
        'GLOG 5205 - restore hidden text display
        ActiveWindow.View.ShowHiddenText = bShowHidden

'       redefine selection range if eo para
'       should be selected - see top
        If bSelectEOPara Then
            Set rngStart = rngStart.Paragraphs(1).Range
            If rngStart.Characters.Last = vbCr Then _
                rngStart.MoveEnd wdCharacter, -1
            rngStart.EndOf
        End If
                        
        'GLOG 5273 - delete temp paragraph
        If bParaAdded Then
            If (rngStart.Paragraphs.Count > 1) And (rngStart.Characters.Last = vbCr) Then
                rngStart.Paragraphs.Last.Range.Delete
            Else
                rngStart.Next(wdParagraph).Delete
            End If
        End If
        
        rngStart.Select
    End With
End Function

Function bUnderlineToLengthOfLongest(rngLocation As Word.Range, _
                                     Optional sDefaultTabStop As Single = 36) As Boolean
    
    Const mpHPos = wdHorizontalPositionRelativeToTextBoundary
    Const mpVPos = wdVerticalPositionRelativeToPage
    
    Dim iPosition
    Dim iLongestLine
    Dim iLeftEdge
    Dim iRightEdge
    Dim iTabStops As Integer
    Dim sRightIndentPos As Single
    Dim lTabPos As Long
    Dim iNumTabs As Integer
    Dim bIsHangingIndent As Boolean
    Dim i As Integer
    Dim tsExisting As Word.TabStop
    Dim iListLevel As Integer
    Dim rngLineEnd As Word.Range
    Dim iVPosition
    Dim iVPrevCharPos
    Dim cS As CStrings
    Dim rngTest As Word.Range
    Dim iLeftSpaces As Integer
    Dim iRightSpaces As Integer
    Dim iCompatibilityMode As Integer
    
    Set cS = New CStrings
    Application.ScreenUpdating = False
    
    ActiveWindow.View.ShowAll = False
    
    rngLocation.Select
    With Selection
        lTabPos = InStr(.Text, vbTab)
        iNumTabs = cS.lCountChrs(.Text, vbTab)
        .Collapse wdCollapseStart
        .EndKey wdLine, wdMove
        If .Text = vbCr Then
            If lTabPos And .ParagraphFormat.FirstLineIndent < 0 Then
                rngLocation.MoveStart wdCharacter, lTabPos
            End If
            rngLocation.Underline = wdUnderlineSingle
            Exit Function
        End If
        
        If .ParagraphFormat.Alignment = wdAlignParagraphCenter Then
            iRightEdge = .Information(mpHPos)
            .HomeKey wdLine, wdMove
            iLeftEdge = .Information(mpHPos)
            .MoveDown wdLine, 1, wdMove
            .EndKey wdLine, wdMove
            While Selection.InRange(rngLocation)
                iPosition = .Information(mpHPos)
                If iPosition > iRightEdge Then _
                    iRightEdge = iPosition
                .HomeKey wdLine, wdMove
                iPosition = .Information(mpHPos)
                If iPosition < iLeftEdge Then _
                    iLeftEdge = iPosition
                .MoveDown wdLine, 1, wdMove
                .EndKey wdLine, wdMove
            Wend
            .HomeKey wdLine, wdExtend
            Set rngLineEnd = ActiveDocument.Range(.End, .End)
            
            If .Next(wdCharacter).Information(mpHPos) = iRightEdge Then
'               lines are the same length - add no spaces
                GoTo skipHardSpaces
            End If
                        
'           get the vertical position of the last character on the second to
'           last line - we'll use this to test whether hard spaces are
'           affecting the way the paragraph wraps
            Set rngTest = .Previous(wdCharacter).Previous(wdCharacter)
            iVPrevCharPos = rngTest.Information(mpVPos)
            
            iVPosition = .Information(mpVPos)
            .Collapse wdCollapseEnd
            While (.Information(mpHPos) < iRightEdge) And _
                    (.Information(mpVPos) = iVPosition)
                'add space to end
                .InsertAfter ChrW(&HA0)
                .Collapse wdCollapseEnd
                If .Information(mpVPos) = iVPosition Then
                    'still on same line - add space to start
                    .HomeKey wdLine, wdMove
                    .InsertAfter ChrW(&HA0)
                    If rngTest.Information(mpVPos) > iVPrevCharPos Then
                        'if the document is set to support complex scripts,
                        'a hard space added at the start of a line will attach
                        'to the last word on the previous line, causing that word
                        'to move down to the start of the current line - add a
                        'shift-return to restore the correct wrap
                        .Collapse wdCollapseEnd
                        .Previous(wdCharacter).InsertBefore vbVerticalTab
                    End If
                    .Expand wdParagraph
                    .MoveEnd wdCharacter, -1
                    .Collapse wdCollapseEnd
                End If
            Wend
            
            If .Information(mpVPos) > iVPosition Then
'               last line wrapped - delete responsible spaces
                .HomeKey wdLine, wdMove
                While .Characters(1) = ChrW(&HA0)
                    .Delete
                Wend
                .EndKey wdLine, wdMove
                While .Information(mpVPos) > iVPosition
                    .Move wdCharacter, -1
                    If .Characters(1) = ChrW(&HA0) Then _
                        .Delete
                    .Expand wdParagraph
                    .MoveEnd wdCharacter, -1
                    .Collapse wdCollapseEnd
                Wend
            End If
            
            'ensure that there are the same number of spaces on both sides
            .Expand wdParagraph
            .MoveEnd wdCharacter, -1
            .Collapse wdCollapseEnd
            
            'measure right-hand side
            .MoveStartWhile ChrW(&HA0), wdBackward
            If .Characters(1) = ChrW(&HA0) Then _
                iRightSpaces = Len(.Text)
                        
            'measure left-hand side
            .Collapse wdCollapseEnd
            .HomeKey wdLine, wdMove
            .MoveEndWhile ChrW(&HA0)
            If .Characters(1) = ChrW(&HA0) Then _
                iLeftSpaces = Len(.Text)
                
            'delete left space if necessary
            If iLeftSpaces > iRightSpaces Then _
                .Characters.First.Delete
                
            .Expand wdParagraph
            .MoveEnd wdCharacter, -1
            .Collapse wdCollapseEnd
            
            'delete right space if necessary
            If iRightSpaces > iLeftSpaces Then _
                .Previous(wdCharacter).Delete
skipHardSpaces:
            i = i
        Else
            '*c
            iLongestLine = .Information(mpHPos)
            If Str(Asc(Selection.Text)) <> 13 Then
                .MoveDown wdLine, 1, wdMove
                .EndKey wdLine, wdMove
            End If
            While Selection.InRange(rngLocation)
                .MoveWhile Chr(32), wdBackward
                iPosition = .Information(mpHPos)
                If iPosition > iLongestLine Then _
                    iLongestLine = iPosition
                .MoveDown wdLine, 1, wdMove
                .EndKey wdLine, wdMove
            Wend
            iPosition = .Information(mpHPos)

            If iPosition < iLongestLine Then
'               add tab stops every x points if hanging indent
'               and >1 tab or not hanging and > 0 tabs.
                With .ParagraphFormat
                    bIsHangingIndent = (.FirstLineIndent < .LeftIndent)
                    If bIsHangingIndent Then
'                       added 6/6/01 - ensure expected tab stop at left indent
                        .TabStops.Add .LeftIndent
                    End If
                    With rngLocation
                        'GLOG 5205 - tab count was inaccurate in partner paragraph and
                        'list format code was erring when the paragraph contained list nums -
                        'adjust the range to target style separator paragraph
                        If g_iWordVersion > mpWordVersion_XP Then
                            Dim bShowAll As Boolean
                            bShowAll = ActiveWindow.View.ShowAll
                            ActiveWindow.View.ShowAll = True
                            If mdlWordXML.IsStyleSeparator(.Paragraphs(1)) Then
                                .SetRange .Paragraphs(1).Range.Start, .Paragraphs(1).Range.End
                            End If
                            ActiveWindow.View.ShowAll = bShowAll
                        End If
                        
                        iNumTabs = cS.lCountChrs(.Text, vbTab)
                        With .ListFormat
                            If .ListType <> wdListNoNumbering Then
                                iListLevel = .ListLevelNumber
                                If .ListTemplate.ListLevels(iListLevel).TrailingCharacter = wdTrailingTab Then
                                    iNumTabs = iNumTabs + 1
                                End If
                            End If
                        End With
                    End With
                    If bIsHangingIndent + iNumTabs Then
                        With ActiveDocument.PageSetup
                            sRightIndentPos = .PageWidth - _
                                (.LeftMargin + .RightMargin)
                        End With
                        
'                       get position of 1st custom tab stop,
'                       then specify next half -inch
                        For Each tsExisting In .TabStops
                            If tsExisting.CustomTab Then
                                i = tsExisting.Position
'                               specify next half inch
                                i = i + (36 - (i Mod 36))
                                Exit For
                            End If
                        Next tsExisting
                        
                        If i = Empty Then _
                            i = sDefaultTabStop
                            
                        While i <= iLongestLine
                            .TabStops.Add i
                            i = i + sDefaultTabStop
                        Wend
                    End If
                End With
                With .ParagraphFormat.TabStops
                    .Add iLongestLine, wdAlignTabRight
                End With
                
                'GLOG 15771 (dm) - Word 2013/2016 compatibility mode does not allow
                'tabbing past the right margin - account for this in the conditional
                'below to avoid an infinite loop in justified paragraphs
                If g_iWordVersion >= mpWordVersion_2010 Then
                    iCompatibilityMode = mdlWord14.GetCompatibilityMode(ActiveDocument)
                End If
                With ActiveDocument.Sections(.Sections.First.Index).PageSetup
                    sRightIndentPos = .PageWidth - (.LeftMargin + .RightMargin)
                End With

                While (.Information(mpHPos) < Int(iLongestLine)) And _
                    ((iCompatibilityMode < 15) Or (.Information(mpHPos) < sRightIndentPos))
                    .InsertAfter vbTab
                    .EndKey wdLine, wdMove
                Wend
            End If
        End If
        .HomeKey wdLine, wdExtend
        .Font.Underline = wdUnderlineSingle
    End With
                
    Set cS = Nothing
End Function

Private Sub RemoveHiddenEndOfPara(ByVal oPara As Word.Paragraph, _
                                  ByRef xHiddenText As String, _
                                  ByRef bIsField As Boolean)
    Dim rngLocation As Word.Range
    Dim bContinue As Boolean
    Dim oResult As Word.Range
    
    ActiveWindow.View.ShowAll = True
    
    Set rngLocation = oPara.Range
    With rngLocation
        If .Text = vbCr Then Exit Sub
        .EndOf
        If .Paragraphs(1).Range.Start <> oPara.Range.Start Then _
            .Move wdCharacter, -1
        .MoveStart wdCharacter, -1
        If .Fields.Count = 1 Then
            'there's a field code at end of para - remove only if
            'it has no result, i.e. it's hidden when ShowAll is off
            On Error Resume Next
            Set oResult = .Fields(1).Result
            On Error GoTo 0
            If oResult Is Nothing Then
                bIsField = True
                xHiddenText = .Fields(1).Code.Text
                .Fields(1).Delete
            End If
        ElseIf .Font.Hidden Then
            'there's hidden text at the end of the para
            bIsField = False
            bContinue = True
            While bContinue
                .MoveStart wdCharacter, -1
                If .Start <> oPara.Range.Start Then
                    bContinue = .Previous(wdCharacter).Font.Hidden
                Else
                    bContinue = False
                End If
            Wend
            xHiddenText = .Text
            .Text = ""
        Else
            Exit Sub
        End If
    End With
End Sub

Private Sub RestoreHiddenEndOfPara(ByVal oPara As Word.Paragraph, _
                                   ByVal xHiddenText As String, _
                                   ByVal bIsField As Boolean)
    Dim rngLocation As Word.Range
    Dim oField As Word.Field
    
    If xHiddenText = "" Then Exit Sub
    
    Set rngLocation = oPara.Range
    With rngLocation
        .EndOf
        If .Paragraphs(1).Range.Start <> oPara.Range.Start Then _
            .Move wdCharacter, -1
        If bIsField Then
            'restore field code
            Set oField = .Fields.Add(rngLocation, , xHiddenText, False)
            
            'trim extra spaces
            With oField.Code
                If .Characters.First = " " Then _
                    .Characters.First.Delete
                If .Characters.Last = " " Then _
                    .Characters.Last.Delete
            End With
        Else
            'restore hidden text
            .InsertAfter xHiddenText
            .Font.Hidden = 1
        End If
    End With
End Sub
