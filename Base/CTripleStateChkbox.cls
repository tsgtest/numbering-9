VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CTripleStateChkbox"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Function ConvToTripleState(lValue As Long) As Integer
'converts a value into a valid
'value for a VB.chkbox
    Select Case lValue
        Case -1, 1, 2
'           value = 2 is considered checked
'           because it represents wdUnderlineWords
            ConvToTripleState = vbChecked
        Case 0
            ConvToTripleState = vbUnchecked
        Case Else
            ConvToTripleState = vbGrayed
    End Select
End Function

Function ConvFromTripleState(lValue As Long) As Long
'converts a value into a valid
'value for a VB.chkbox
    Select Case lValue
        Case 0
            ConvFromTripleState = 0
        Case 1
            ConvFromTripleState = -1
        Case Else
            ConvFromTripleState = wdUndefined
    End Select
End Function

Sub CycleTripleState(chkP As Object, ByVal iValue As Integer)
    If iValue = vbChecked Then
        chkP.Value = vbGrayed
    ElseIf iValue = vbGrayed Then
        chkP.Value = vbUnchecked
    Else
        chkP.Value = vbChecked
    End If
    DoEvents
End Sub


