Attribute VB_Name = "mpTypes"
Option Explicit

Type mpDocEnvironment
    bShowAll As Boolean
    sVScroll As Single
    sHScroll As Single
    iView As Integer
    bFieldCodes As Boolean
    bBookmarks As Boolean
    bTabs As Boolean
    bSpaces As Boolean
    bHyphens As Boolean
    bHiddenText As Boolean
    bParagraphs As Boolean
    bTextBoundaries As Boolean
    iProtectionType As Integer
    iSelectionStory As Integer
    lSelectionStartPos As Long
    lSelectionEndPos As Long
    bTrackChanges As Boolean
End Type

Type mpAppEnvironment
    lBrowserTarget As Long
End Type
