Attribute VB_Name = "mdlVariables"
Public Enum mpPleadingFirmNameTypes
     mpPleadingFirmNameNone = 0
     mpPleadingFirmNameInFooter = 1
     mpPleadingFirmNameLandscaped = 2
End Enum

'pleading paper types
Public Enum mpPleadingPaperTypes
     mpPleadingPaperNone = 0
     mpPleadingPaper26Line = 26
     mpPleadingPaper28Line = 28
     mpPleadingPaperNoneFNPort = 100
     mpPleadingPaper26LineFNPort = 126
     mpPleadingPaper28LineFNPort = 128
     mpPleadingPaperNoneFNLand = 200
     mpPleadingPaper26LineFNLand = 226
     mpPleadingPaper28LineFNLand = 228
End Enum

Public Const mpTopMarginTOC As Single = 1
Public Const mpBottomMarginTOC As Single = 1
Public Const mpLeftMarginTOC As Single = 1
Public Const mpRightMarginTOC As Single = 1
Public Const mpHeaderDistTOC As Single = 0.75
Public Const mpFooterDistTOC As Single = 0.5

Public Const mpPleadingTOCTopMargin As String = "-1.58"
Public Const mpPleading26TOCTopMargin As String = "-2.02"

Public Const mpHeaderDistDefault As String = ".5"
Public Const mpFooterDistDefault As String = ".5"

Public Const mpPleadingHeaderDistance As String = ".3"
Public Const mpPleadingFooterDistance As String = ".25"

Public Const mpTopMarginDefault As String = "1"
Public Const mpBottomMarginDefault As String = "1"
Public Const mpLeftMarginDefault As String = "1"
Public Const mpRightMarginDefault As String = "1"

Public Const mpPleading26TopMargin As String = "-1.35"
Public Const mpPleading26BotMargin As String = "1.12"
Public Const mpPleading26LeftMargin As String = "1.45"
Public Const mpPleading26RightMargin As String = ".5"

Public Const mpPleading28LeftMargin As String = "1.45"
Public Const mpPleading28RightMargin As String = ".5"
Public Const mpPleading28TopMargin As String = "-.92"
Public Const mpPleading28BotMargin As String = ".75"

'MacPac 2000
Public g_xMP90Dir As String
Public g_iSeed As Integer
Public g_oPleadingPaper As MPO.CPleadingPaper '9.9.5005

Public Const SYNCHRONIZE = &H100000
Public Const STANDARD_RIGHTS_READ = &H20000
Public Const STANDARD_RIGHTS_WRITE = &H20000
Public Const STANDARD_RIGHTS_EXECUTE = &H20000
Public Const STANDARD_RIGHTS_REQUIRED = &HF0000
Public Const STANDARD_RIGHTS_ALL = &H1F0000
Public Const KEY_QUERY_VALUE = &H1
Public Const KEY_SET_VALUE = &H2
Public Const KEY_CREATE_SUB_KEY = &H4
Public Const KEY_ENUMERATE_SUB_KEYS = &H8
Public Const KEY_NOTIFY = &H10
Public Const KEY_CREATE_LINK = &H20
Public Const KEY_READ = ((STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE _
 Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))
Public Const KEY_WRITE = ((STANDARD_RIGHTS_WRITE Or KEY_SET_VALUE _
 Or KEY_CREATE_SUB_KEY) And (Not SYNCHRONIZE))
Public Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL _
 Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY _
 Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) _
 And (Not SYNCHRONIZE))


