Attribute VB_Name = "mpSchemeRecord"
Option Explicit

'**********************************************************
'   CSchemeRecord Collection Class
'   created 6/3/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   SchemeRecord class - the class that manages Scheme
'   Records. Scheme Records are held as doc vars in the
'   source file of a scheme
'**********************************************************

Enum mpRecordFields
    mpRecField_Name = 1
    mpRecField_Alias = 2
    mpRecField_Origin = 3
    mpRecField_TOCScheme = 4
End Enum

Public Function AddRecord(udtScheme As mpScheme) As Long
'adds scheme to document properties "table"
'and refills xPSchemes() array
    Dim iNumSchemes As Integer
    Dim i As Integer
    Dim j As Integer
    Dim oTarget As Object
    
'   set target and get the number of schemes in target
    Select Case udtScheme.Type
        Case mpSchemeType_Document
            Set oTarget = ActiveDocument
        Case mpSchemeType_Firm
            Set oTarget = Application.Templates(xFNumSty)
        Case Else
            Set oTarget = Application.Templates(xPNumSty)
    End Select
    
    iNumSchemes = RecordCount(oTarget)
    
'   add document property for new scheme
    oTarget.CustomDocumentProperties.Add _
                "Scheme" & (iNumSchemes + 1), _
                False, _
                msoPropertyTypeString, _
                ""
'   fill new doc prop with values in udtSchemes
    SetRecord oTarget, _
              iNumSchemes + 1, _
              udtScheme
    
'   update array
     Select Case udtScheme.Type
        Case mpSchemeType_Firm
            lRet = iGetSchemes(xFSchemes(), mpSchemeType_Firm)
        Case mpSchemeType_Document
            lRet = iGetSchemes(xDSchemes(), mpSchemeType_Document)
        Case Else
            lRet = iGetSchemes(xPSchemes(), mpSchemeType_Personal)
    End Select
    
    AddRecord = lRet
End Function

Public Function DeleteRecord(xScheme As String, _
    Optional iType As mpSchemeTypes = mpSchemeType_Personal) As Long
'adds/deletes schemes from document properties "table"
'and refills xPSchemes() array
    Dim iNumSchemes As Integer
    Dim i As Integer
    Dim j As Integer
    Dim oTarget As Object
    Dim iID As Integer
    Dim udtScheme As mpScheme
    
'   set target
    Select Case iType
        Case mpSchemeType_Document
            Set oTarget = ActiveDocument
        Case mpSchemeType_Firm
            Set oTarget = Application.Templates(xFNumSty)
        Case Else
            Set oTarget = Application.Templates(xPNumSty)
    End Select
    
    With oTarget
'       get # schemes in target
        iNumSchemes = RecordCount(oTarget)
        iID = GetRecordID(oTarget, xScheme)
        
'       move all doc props after the one that
'       is to be deleted down one - eg
'       the value for "Scheme12" becomes the
'       value for "Scheme11", etc
        For i = iID + 1 To iNumSchemes
            udtScheme = GetRecord(oTarget, i)
            SetRecord oTarget, i - 1, udtScheme
        Next i
            
'       delete last indexed document property whose
'       value has been moved to the penultimate prop
        .CustomDocumentProperties _
            .Item("Scheme" & iNumSchemes).Delete
    End With
    
'   update array
     Select Case udtScheme.Type
        Case mpSchemeType_Firm
            lRet = iGetSchemes(xFSchemes(), _
                        mpSchemeType_Firm)
        Case mpSchemeType_Document
            lRet = iGetSchemes(xDSchemes(), _
                    mpSchemeType_Document)
        Case Else
            lRet = iGetSchemes(xPSchemes(), _
                    mpSchemeType_Personal)
    End Select
    
    DeleteRecord = lRet
End Function

Public Function GetRecordID(oSource As Object, _
                   ByVal xScheme As String) As Integer
'returns the record id
'for the scheme xScheme

    Dim xTemp As String
    Dim i As Integer

'   cycle through docprops
'   looking for a match of name
    i = 1
            
    Do
        xTemp = GetRecordField(oSource, i, mpRecField_Name)
        If xTemp = xScheme Then
            GetRecordID = i
            Exit Function
        End If
        i = i + 1
    Loop While Len(xTemp)
End Function

Public Function RecordCount(oSource As Object) As Integer
'returns the number of records in oSource

    Dim xTemp As String
    Dim i As Integer

'   cycle through docprops, counting
    i = 1
    
    xTemp = GetRecordField(oSource, i, mpRecField_Name)
                     
    While Len(xTemp)
        i = i + 1
        xTemp = GetRecordField(oSource, i, mpRecField_Name)
    Wend
    RecordCount = i - 1
End Function

Public Function GetRecord(oSource As Object, _
                     ByVal vID As Variant) As mpScheme
'returns a mpScheme var with sub vars
'filled from doc prop with name xName
'in source template/document oSource
    Dim udtScheme As mpScheme
    Dim iID As Integer
    
    If IsNumeric(vID) Then
        iID = vID
    Else
'       supplied vID is scheme name - get record name
        iID = GetRecordID(oSource, vID)
    End If
    
    udtScheme.Name = GetRecordField(oSource, _
                                iID, _
                                mpRecField_Name)
    udtScheme.Alias = GetRecordField(oSource, _
                                iID, _
                                mpRecField_Alias)
    udtScheme.Origin = GetRecordField(oSource, _
                                iID, _
                                mpRecField_Origin)
    udtScheme.TOCScheme = GetRecordField(oSource, _
                                iID, _
                                mpRecField_TOCScheme)
    
'   get type from source
    If oSource.FullName = xFNumSty Then
        udtScheme.Type = mpSchemeType_Firm
    ElseIf oSource.FullName = xPNumSty Then
        udtScheme.Type = mpSchemeType_Personal
    Else
        udtScheme.Type = mpSchemeType_Document
    End If
    GetRecord = udtScheme
End Function

Public Function SetRecord(oSource As Object, _
                     ByVal vID As Variant, _
                     udtScheme As mpScheme) As String
'sets the value of the doc prop with name
'xName in template/document oSource based
'on the values in udtScheme
    Dim xValue As String
    Dim xDocProp As String
    
    If IsNumeric(vID) Then
        xDocProp = "Scheme" & vID
    Else
'       supplied vID is scheme name - get record name
        xDocProp = "Scheme" & GetRecordID(oSource, vID)
    End If
        
    xValue = "|" & udtScheme.Name & "|" & udtScheme.Alias & "|" & _
             udtScheme.Origin & "|" & udtScheme.TOCScheme & "|"
    oSource.CustomDocumentProperties(xDocProp).Value = xValue
End Function

Public Function GetRecordField(oSource As Object, _
                               ByVal vID As Variant, _
                               ByVal iField As mpRecordFields) As String
'returns specified field from doc prop with
'name xName in template/document oSource
    Dim xTemp As String
    Dim iPos1 As Integer
    Dim iPos2 As Integer
    Dim i As Integer
    Dim xDocProp As String
    
    If IsNumeric(vID) Then
        xDocProp = "Scheme" & vID
    Else
'       supplied vID is scheme name - get record name
        xDocProp = "Scheme" & GetRecordID(oSource, vID)
    End If
    
'   get value of doc prop
    On Error Resume Next
    xTemp = oSource.CustomDocumentProperties(xDocProp).Value
    On Error GoTo 0
    
    If Len(xTemp) Then
'       get pipe that precedes the requested field
        For i = 1 To iField
            iPos1 = InStr(iPos1 + 1, xTemp, "|")
        Next i
    
'       get pipe that succeeds the requested field
        iPos2 = InStr(iPos1 + 1, xTemp, "|")
    
'       return the contents in between the 2 pipes
        GetRecordField = Mid(xTemp, iPos1 + 1, iPos2 - iPos1 - 1)
    End If
End Function

Public Sub SetRecordField(oSource As Object, _
                          ByVal vID As Variant, _
                          ByVal iField As mpRecordFields, _
                          ByVal xValue As String)
'sets field iField in doc prop with
'name xName in template/document oSource to
'value xValue
    Dim xTemp As String
    Dim iPos1 As Integer
    Dim iPos2 As Integer
    Dim i As Integer
    Dim xDocProp As String
    
    If IsNumeric(vID) Then
        xDocProp = "Scheme" & vID
    Else
'       supplied vID is scheme name - get record name
        xDocProp = "Scheme" & GetRecordID(oSource, vID)
    End If
    
'   get value of doc prop
    xTemp = oSource.CustomDocumentProperties(xDocProp).Value
    
'   get pipe that precedes the requested field
    For i = 1 To iField
        iPos1 = InStr(iPos1 + 1, xTemp, "|")
    Next i
    
'   get pipe that succeeds the requested field
    iPos2 = InStr(iPos1 + 1, xTemp, "|")
    
'   return the contents in between the 2 pipes
    xValue = Left(xTemp, iPos1) & xValue & Mid(xTemp, iPos2)
    
'   set doc prop to new value
    oSource.CustomDocumentProperties(xDocProp).Value = xValue
End Sub





