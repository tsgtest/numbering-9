Attribute VB_Name = "mdlCC"
Option Explicit

Public Function AddUnlinkedParagraphStyle(ByVal oDoc As Word.Document, _
                                          ByVal xName As String) As Word.Style
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    Set AddUnlinkedParagraphStyle = oCC.AddUnlinkedParagraphStyle(oDoc, xName)
End Function

Public Function COMAddInIsLoaded(ByVal xName As String) As Boolean
    Dim oCC As mpnCC.cContentControls
    Set oCC = New mpnCC.cContentControls
    COMAddInIsLoaded = oCC.COMAddInIsLoaded(xName)
End Function

