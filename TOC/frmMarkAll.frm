VERSION 5.00
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmMarkAll 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Mark/Format All Headings"
   ClientHeight    =   5370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6465
   Icon            =   "frmMarkAll.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   6465
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   4620
      Left            =   120
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   8149
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   609
      TabCaption(0)   =   "&Main"
      TabPicture(0)   =   "frmMarkAll.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "pnlScope"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "pnlApplyTo"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "pnlLevels"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "pnlDo"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "pnlUndo"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "&Formatting"
      TabPicture(1)   =   "frmMarkAll.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdFormatting"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "cbxFormatting"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Label2"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Label4"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Label1"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      Begin VB.Frame pnlUndo 
         Caption         =   "Reverse Actions"
         Height          =   1655
         Left            =   3855
         TabIndex        =   16
         Top             =   585
         Width           =   2100
         Begin VB.CheckBox chkUnformat 
            Caption         =   "&Unformat Headings"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   135
            TabIndex        =   17
            Top             =   280
            Width           =   1700
         End
         Begin VB.CheckBox chkUnmark 
            Caption         =   "U&nmark for TOC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   135
            TabIndex        =   18
            Top             =   880
            Width           =   1700
         End
      End
      Begin VB.Frame pnlDo 
         Caption         =   "Actions"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1655
         Left            =   1305
         TabIndex        =   11
         Top             =   585
         Width           =   2380
         Begin VB.CheckBox chkMarkTC 
            Caption         =   "Mark for &TOC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   135
            TabIndex        =   14
            Top             =   910
            Width           =   1800
         End
         Begin VB.CheckBox chkFormat 
            Caption         =   "Format &Headings"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   135
            TabIndex        =   12
            Top             =   280
            Width           =   1830
         End
         Begin VB.ComboBox cbxUse 
            Enabled         =   0   'False
            Height          =   315
            ItemData        =   "frmMarkAll.frx":05C2
            Left            =   400
            List            =   "frmMarkAll.frx":05CC
            Style           =   2  'Dropdown List
            TabIndex        =   15
            Top             =   2245
            Visible         =   0   'False
            Width           =   1850
         End
         Begin VB.Label lblMode 
            Caption         =   "(Mode)"
            Height          =   225
            Left            =   390
            TabIndex        =   34
            Top             =   1215
            Width           =   1755
         End
         Begin VB.Label Label3 
            Caption         =   "(Format is Directly Applied)"
            Height          =   225
            Left            =   390
            TabIndex        =   13
            Top             =   585
            Width           =   1905
         End
      End
      Begin VB.Frame pnlLevels 
         Caption         =   "Levels"
         Height          =   3670
         Left            =   195
         TabIndex        =   1
         Top             =   585
         Width           =   975
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   0
            Left            =   200
            TabIndex        =   2
            ToolTipText     =   "Click to check level 1."
            Top             =   280
            Width           =   450
         End
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&2"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   1
            Left            =   200
            TabIndex        =   3
            ToolTipText     =   "Press the shift key and click to check levels 1-2."
            Top             =   630
            Width           =   450
         End
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&3"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   2
            Left            =   200
            TabIndex        =   4
            ToolTipText     =   "Press the shift key and click to check levels 1-3."
            Top             =   980
            Width           =   450
         End
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&4"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   3
            Left            =   200
            TabIndex        =   5
            ToolTipText     =   "Press the shift key and click to check levels 1-4."
            Top             =   1330
            Width           =   450
         End
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&5"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   4
            Left            =   200
            TabIndex        =   6
            ToolTipText     =   "Press the shift key and click to check levels 1-5."
            Top             =   1680
            Width           =   450
         End
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&6"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   5
            Left            =   200
            TabIndex        =   7
            ToolTipText     =   "Press the shift key and click to check levels 1-6."
            Top             =   2030
            Width           =   450
         End
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&7"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   6
            Left            =   200
            TabIndex        =   8
            ToolTipText     =   "Press the shift key and click to check levels 1-7."
            Top             =   2380
            Width           =   450
         End
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&8"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   7
            Left            =   200
            TabIndex        =   9
            ToolTipText     =   "Press the shift key and click to check levels 1-8."
            Top             =   2730
            Width           =   450
         End
         Begin VB.CheckBox chkLevel 
            Alignment       =   1  'Right Justify
            Caption         =   "&9"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   8
            Left            =   200
            TabIndex        =   10
            ToolTipText     =   "Press the shift key and click to check levels 1-9."
            Top             =   3050
            Width           =   450
         End
      End
      Begin VB.Frame pnlApplyTo 
         Caption         =   "Apply To"
         Height          =   1900
         Left            =   1305
         TabIndex        =   19
         Top             =   2360
         Width           =   2380
         Begin VB.OptionButton optAllSchemes 
            Caption         =   "&All Schemes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   160
            TabIndex        =   20
            Top             =   435
            Value           =   -1  'True
            Width           =   1515
         End
         Begin VB.OptionButton optSelectedScheme 
            Caption         =   "S&elect Scheme:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   160
            TabIndex        =   21
            Top             =   915
            Width           =   1930
         End
         Begin TrueDBList60.TDBCombo cbxScheme 
            Height          =   564
            Left            =   160
            OleObjectBlob   =   "frmMarkAll.frx":05EC
            TabIndex        =   22
            Top             =   1230
            Width           =   1800
         End
      End
      Begin VB.Frame pnlScope 
         Caption         =   "Other"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1900
         Left            =   3855
         TabIndex        =   23
         Top             =   2360
         Width           =   2100
         Begin VB.TextBox txtDefinition 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   135
            TabIndex        =   25
            Top             =   565
            Width           =   1850
         End
         Begin VB.ComboBox cbxDefinition 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmMarkAll.frx":2511
            Left            =   135
            List            =   "frmMarkAll.frx":2513
            Style           =   2  'Dropdown List
            TabIndex        =   33
            Top             =   2000
            Visible         =   0   'False
            Width           =   1850
         End
         Begin TrueDBList60.TDBCombo cbxScope 
            Height          =   564
            Left            =   135
            OleObjectBlob   =   "frmMarkAll.frx":2515
            TabIndex        =   27
            Top             =   1300
            Width           =   1850
         End
         Begin VB.Label lblScope 
            Caption         =   "&Scope:"
            Height          =   210
            Left            =   165
            TabIndex        =   26
            Top             =   1075
            Width           =   1700
         End
         Begin VB.Label lblDefinition 
            Caption         =   "Heading &Definition:"
            Height          =   210
            Left            =   165
            TabIndex        =   24
            Top             =   335
            Width           =   1700
         End
      End
      Begin TrueDBGrid60.TDBGrid grdFormatting 
         Height          =   2955
         Left            =   -74865
         OleObjectBlob   =   "frmMarkAll.frx":4439
         TabIndex        =   30
         Top             =   1350
         Width           =   5895
      End
      Begin TrueDBList60.TDBCombo cbxFormatting 
         Height          =   564
         Left            =   -71850
         OleObjectBlob   =   "frmMarkAll.frx":8294
         TabIndex        =   29
         Top             =   630
         Width           =   1800
      End
      Begin VB.Label Label2 
         Caption         =   "scheme"
         Height          =   300
         Left            =   -69985
         TabIndex        =   36
         Top             =   675
         Width           =   810
      End
      Begin VB.Label Label4 
         Caption         =   "selected on Main tab:"
         Height          =   300
         Left            =   -74600
         TabIndex        =   35
         Top             =   950
         Width           =   2500
      End
      Begin VB.Label Label1 
         Caption         =   "Show direct formatting for run-in levels of"
         Height          =   300
         Left            =   -74800
         TabIndex        =   28
         Top             =   675
         Width           =   2950
      End
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   393
      Left            =   3975
      TabIndex        =   31
      Top             =   4875
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   393
      Left            =   5175
      TabIndex        =   32
      Top             =   4875
      Width           =   1100
   End
End
Attribute VB_Name = "frmMarkAll"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_bFinished As Boolean
Private m_bClickingLevels As Boolean
Private m_bInitializing As Boolean
Private m_oFormattingChanges As Scripting.Dictionary
Private m_bLoadingDefaults As Boolean
Private oGridArray(5, 8) As Variant
Private m_iActive As Integer
Private m_xarScheme As xArray



Private Enum mpFormattingColumns
    Level = 0
    Bold = 1
    Italic = 2
    Underline = 3
    SmallCaps = 4
    AllCaps = 5
End Enum

Private Sub btnCancel_Click()
    Me.Hide
End Sub

Private Sub btnOK_Click()
    Dim i As Integer
    
    If bIsValid() Then
'       sticky fields
        For i = 0 To 8
            bAppSetUserINIValue "MarkAll", _
                Str(i + 1), Me.chkLevel(i)
        Next i
        bAppSetUserINIValue "MarkAll", _
            "ApplyToSelected", Me.optSelectedScheme
        bAppSetUserINIValue "MarkAll", _
            "SentenceDefinition", Me.txtDefinition
        bAppSetUserINIValue "MarkAll", "Format", Me.chkFormat
        bAppSetUserINIValue "MarkAll", "MarkTC", Me.chkMarkTC
        bAppSetUserINIValue "MarkAll", "Unformat", Me.chkUnformat
        bAppSetUserINIValue "MarkAll", "Unmark", Me.chkUnmark
        bAppSetUserINIValue "MarkAll", "Scheme", Me.cbxScheme.Text
        bAppSetUserINIValue "MarkAll", "Scope", Me.cbxScope.Text
        
        m_bFinished = True
        Me.Hide
        DoEvents
    End If
End Sub

Private Sub cbxFormatting_ItemChange()
    If Not m_bInitializing Then
        LoadFormattingDefaults
    End If
End Sub

Private Sub cbxFormatting_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxFormatting, Reposition
    Exit Sub
ProcError:
    RaiseError "frmMarkAll.cbxFormatting_Mismatch"
    Exit Sub
End Sub

Private Sub cbxScheme_ItemChange()
    If Not m_bInitializing Then
        Me.cbxFormatting.SelectedItem = Me.cbxScheme.SelectedItem
        LoadFormattingDefaults
    End If
End Sub

Private Sub cbxScheme_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxScheme, Reposition
    Exit Sub
ProcError:
    RaiseError "frmMarkAll.cbxScheme_Mismatch"
    Exit Sub
End Sub

Private Sub cbxScope_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cbxScope, Reposition
    Exit Sub
ProcError:
    RaiseError "frmMarkAll.cbxScope_Mismatch"
    Exit Sub
End Sub

Private Sub chkFormat_Click()
    Dim bEnabled As Boolean
    Dim i As Integer
    
    If chkFormat.Value = 1 Then
        chkUnformat.Value = 0
        
        'enable formatting tab only if formatting is checked and
        'at least one level is selected
        For i = 0 To 8
            If Me.chkLevel(i).Value = 1 Then
                bEnabled = True
                Exit For
            End If
        Next i
    End If
    
    Me.SSTab1.TabEnabled(1) = bEnabled
End Sub

Private Sub chkLevel_Click(Index As Integer)
    Dim i As Integer
    Dim bValue As Boolean
    Dim bEnabled As Boolean
    
'   if shift is pressed, make all previous
'   levels the same value as the clicked level
    If Not m_bClickingLevels Then
        m_bClickingLevels = True
        If IsPressed(KEY_SHIFT) Then
            For i = 0 To Index - 1
                Me.chkLevel(i).Value = Me.chkLevel(Index).Value
            Next i
        End If
        m_bClickingLevels = False
    End If
    
    'load formatting defaults
    If Not m_bInitializing Then _
        LoadFormattingDefaults
    
    'enable formatting tab only if formatting is checked and
    'at least one level is selected
    If chkFormat.Value = 1 Then
        For i = 0 To 8
            If Me.chkLevel(i).Value = 1 Then
                bEnabled = True
                Exit For
            End If
        Next i
    End If
    Me.SSTab1.TabEnabled(1) = bEnabled
End Sub

Private Sub chkMarkTC_Click()
    If chkMarkTC.Value = 1 Then _
        chkUnmark.Value = 0
    Me.cbxUse.Enabled = CBool(Me.chkMarkTC.Value)
End Sub

Private Sub chkUnformat_Click()
    If chkUnformat.Value = 1 Then _
        chkFormat.Value = 0
End Sub

Private Sub chkUnmark_Click()
    If chkUnmark.Value = 1 Then _
        chkMarkTC.Value = 0
End Sub

Private Sub Form_Activate()
    Dim xScope As String
    Dim xScheme As String
    Dim i As Integer
    
'   initialize scope
    xScope = xAppGetUserINIValue("MarkAll", "Scope")
    If Selection.Type = wdSelectionNormal Then
        Me.cbxScope.SelectedItem = 2
    Else
        If xScope = "Current Section" Then
            Me.cbxScope.SelectedItem = 1
        Else
            Me.cbxScope.SelectedItem = 0
        End If
    End If

'   if last selected scheme is not in doc, default to active scheme
    xScheme = xAppGetUserINIValue("MarkAll", "Scheme")
    
    For i = 0 To m_xarScheme.UpperBound(1)
        If m_xarScheme(i, 0) = xScheme Then
            Me.cbxScheme.SelectedItem = m_xarScheme(i, 1)
            Exit For
        End If
    Next i

    If Me.cbxScheme.BoundText = "" Then _
        Me.cbxScheme.SelectedItem = m_iActive

    Me.cbxFormatting.SelectedItem = Me.cbxScheme.SelectedItem
    
    LoadFormattingDefaults
    
    m_bInitializing = False

End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim iChecked As Integer
    Dim xActive As String
    Dim xDef As String
    Dim xScope As String
    Dim xarScope As xArray
    
    m_bInitializing = True
    m_bFinished = False
    
    Set m_oFormattingChanges = New Scripting.Dictionary
    
'   formatting tab should start disabled
    Me.SSTab1.TabEnabled(1) = False

'   load scopes
    Set xarScope = New xArray
    
    xarScope.ReDim 0, 2, 0, 1
    xarScope(0, 0) = "Current Document"
    xarScope(0, 1) = "0"
    xarScope(1, 0) = "Current Section"
    xarScope(1, 1) = "1"
    xarScope(2, 0) = "Current Selection"
    xarScope(2, 1) = "2"
    
    Me.cbxScope.Array = xarScope
    ResizeTDBCombo Me.cbxScope, 3
    
'   sticky checkboxes
    On Error Resume Next
    For i = 0 To 8
        iChecked = Val(xAppGetUserINIValue("MarkAll", Str(i + 1)))
        Me.chkLevel(i) = iChecked
    Next i
    Me.chkFormat = Val(xAppGetUserINIValue("MarkAll", "Format"))
    Me.chkMarkTC = Val(xAppGetUserINIValue("MarkAll", "MarkTC"))
    Me.chkUnformat = Val(xAppGetUserINIValue("MarkAll", "Unformat"))
    Me.chkUnmark = Val(xAppGetUserINIValue("MarkAll", "Unmark"))
    On Error GoTo 0
    
'   ensure that at least one action is checked
    If Me.chkFormat + Me.chkMarkTC + Me.chkUnformat + Me.chkUnmark = 0 Then
        Me.chkFormat = 1
    End If
    
    Me.optSelectedScheme = (xAppGetUserINIValue("MarkAll", _
        "ApplyToSelected") = "True")
    
    
'   load schemes
    On Error Resume Next
    xActive = ActiveDocument _
        .Variables("zzmpFixedCurScheme_9.0").Value
    On Error GoTo 0
    
    If xActive <> "" Then _
        xActive = Mid(xTrimSpaces(xActive), 2)
    
    Set m_xarScheme = New xArray
    m_xarScheme.ReDim 0, UBound(g_xDSchemes), 0, 1

    m_iActive = 0
    
    For i = 0 To UBound(g_xDSchemes)
        If g_xDSchemes(i, 1) = "" Then
            m_xarScheme(i, 0) = xGetStyleRoot(g_xDSchemes(i, 0))
            m_xarScheme(i, 1) = i
        Else
            m_xarScheme(i, 0) = g_xDSchemes(i, 1)
            m_xarScheme(i, 1) = i
        End If
        
        If g_xDSchemes(i, 0) = xActive Then _
            m_iActive = i
    Next i
    
    Me.cbxScheme.Array = m_xarScheme
    ResizeTDBCombo Me.cbxScheme, UBound(g_xDSchemes) + 1
        
'   sentence definition
    xDef = xAppGetUserINIValue("MarkAll", "SentenceDefinition")
    If xDef = "" Then _
        xDef = "." & g_xBullet & g_xBullet
    With Me.txtDefinition
        .Text = xDef
        .SelStart = Len(xDef)
    End With
            
    'marking mode
    If g_bMarkWithStyleSeparators Then
        Me.lblMode.Caption = "(Using Style Separators)"
    Else
        Me.lblMode.Caption = "(Using TC Codes)"
    End If
    
'    If g_iWordVersion = mpWordVersion_2007 Then
'        If g_bMarkWithStyleSeparators Then
'            Me.cbxUse.ListIndex = 0
'        Else
'            Me.cbxUse.ListIndex = 1
'        End If
'    Else
'        'we're only offering style separator option in Word 2007 and 2010
'        Me.chkMarkTC.Caption = "&Mark for TOC"
'        Me.cbxUse.ListIndex = 1
'        Me.cbxUse.Visible = False
'        Me.chkMarkTC.Top = Me.chkMarkTC.Top + 75
'        Me.chkFormat.Top = Me.chkFormat.Top + 75
'        Me.chkUnmark.Top = Me.chkUnmark.Top + 75
'        Me.chkUnformat.Top = Me.chkUnformat.Top + 75
'    End If
    
    'set formatting list and defaults
    Me.cbxFormatting.Array = m_xarScheme
    ResizeTDBCombo Me.cbxScheme, UBound(g_xDSchemes) + 1

'    With Me.cbxFormatting
'        For i = 1 To Me.cbxScheme.ListCount
'            .AddItem Me.cbxScheme.List(i - 1)
'        Next i
'        .ListIndex = Me.cbxScheme.ListIndex
'    End With
    
    For i = 0 To 8
        oGridArray(0, i) = CStr(i + 1)
    Next i
    Me.grdFormatting.ApproxCount = 9
    
End Sub

Private Function bIsValid() As Boolean
'returns TRUE only if user has
'selected valid choices to run the
'"Mark all" function
    Dim bLevelsOK As Boolean
    Dim bActionsOK As Boolean
    
    With Me.chkLevel
        bLevelsOK = (.Item(1) + _
                    .Item(2) + _
                    .Item(3) + _
                    .Item(4) + _
                    .Item(5) + _
                    .Item(6) + _
                    .Item(7) + _
                    .Item(8) + _
                    .Item(0)) > 0
    End With
    
    bActionsOK = (Me.chkFormat + Me.chkMarkTC + _
        Me.chkUnformat + Me.chkUnmark) > 0
    
    If Not bDefinitionIsValid() Then
        Me.txtDefinition.SetFocus
        If Left(Me.txtDefinition, 1) = g_xBullet Then
'           move cursor to start
            Me.txtDefinition.SelStart = 0
        End If
    ElseIf bLevelsOK And bActionsOK Then
        bIsValid = True
    ElseIf Not bLevelsOK Then
        xMsg = "You must select at least one level to " & _
               "mark and/or format."
        MsgBox xMsg, vbExclamation, AppName
        Me.chkLevel(0).SetFocus
    Else
        xMsg = "You must choose to " & _
               "perform some action.  Currently, " & _
               "nothing is selected."
        MsgBox xMsg, vbExclamation, AppName
        Me.chkFormat.SetFocus
    End If
End Function

Private Sub grdFormatting_AfterColUpdate(ByVal ColIndex As Integer)
    Dim iOldFormat As Integer
    Dim iNewFormat As Integer
    Dim xScheme As String
    Dim iRow As Integer
    Dim iField As mpTCFormatFields
    Dim xKey As String
    
    'get current heading format
    xScheme = g_xDSchemes(Me.cbxFormatting.Bookmark, 0)
    iRow = Me.grdFormatting.Row
    xKey = xScheme & CStr(iRow + 1)
    If m_oFormattingChanges.Exists(xKey) Then
        'from dictionary key
        iOldFormat = CInt(m_oFormattingChanges(xKey))
    Else
        'from list template
        iOldFormat = CInt(xGetLevelProp(xScheme, iRow + 1, _
            mpNumLevelProp_HeadingFormat, mpSchemeType_Document))
    End If
    
    'get target field
    Select Case ColIndex
        Case mpFormattingColumns.Bold
            iField = mpTCFormatField_Bold
        Case mpFormattingColumns.Italic
            iField = mpTCFormatField_Italic
        Case mpFormattingColumns.Underline
            iField = mpTCFormatField_Underline
        Case mpFormattingColumns.SmallCaps
            iField = mpTCFormatField_SmallCaps
        Case mpFormattingColumns.AllCaps
            iField = mpTCFormatField_AllCaps
    End Select
    
    'toggle target bit
    If iHasTCFormat(iOldFormat, iField) = 0 Then _
        iNewFormat = iField
        
    'restore other bits
    If (iHasTCFormat(iOldFormat, mpTCFormatField_Bold) = 1) And _
            (iField <> mpTCFormatField_Bold) Then
        iNewFormat = iNewFormat Or mpTCFormatField_Bold
    End If
    If (iHasTCFormat(iOldFormat, mpTCFormatField_Italic) = 1) And _
            (iField <> mpTCFormatField_Italic) Then
        iNewFormat = iNewFormat Or mpTCFormatField_Italic
    End If
    If (iHasTCFormat(iOldFormat, mpTCFormatField_Underline) = 1) And _
            (iField <> mpTCFormatField_Underline) Then
        iNewFormat = iNewFormat Or mpTCFormatField_Underline
    End If
    If (iHasTCFormat(iOldFormat, mpTCFormatField_SmallCaps) = 1) And _
            (iField <> mpTCFormatField_SmallCaps) Then
        iNewFormat = iNewFormat Or mpTCFormatField_SmallCaps
    End If
    If (iHasTCFormat(iOldFormat, mpTCFormatField_AllCaps) = 1) And _
            (iField <> mpTCFormatField_AllCaps) Then
        iNewFormat = iNewFormat Or mpTCFormatField_AllCaps
    End If
        
    'update dictionary key
    m_oFormattingChanges(xScheme & CStr(iRow + 1)) = CStr(iNewFormat)
End Sub

Private Sub grdFormatting_ClassicRead(Bookmark As Variant, ByVal Col As Integer, Value As Variant)
    Value = GetUserData(Bookmark, Col)
End Sub

Private Sub grdFormatting_ClassicWrite(Bookmark As Variant, ByVal Col As Integer, Value As Variant)
    If Not StoreUserData(Bookmark, Col, Value) Then
        Bookmark = Null   ' Tell the grid the update failed.
    End If
End Sub

Private Sub grdFormatting_FetchCellStyle(ByVal Condition As Integer, ByVal Split As Integer, Bookmark As Variant, ByVal Col As Integer, ByVal CellStyle As TrueDBGrid60.StyleDisp)
'disable checkboxes if level isn't specified, doesn't exist, or is style based
    Dim xScheme As String
    Dim bExists As Boolean
    Dim bIsStyleBased As Boolean
    Dim iLevel As Integer
    
    If Col > 0 Then
        xScheme = g_xDSchemes(Me.cbxFormatting.Bookmark, 0)
        iLevel = CInt(Bookmark) + 1
        bExists = bLevelExists(xScheme, iLevel, mpSchemeType_Document)
        If bExists Then _
            bIsStyleBased = bTOCLevelIsStyleBased(xScheme, iLevel)
        With CellStyle
            If (Not bExists) Or bIsStyleBased Or _
                    (Me.chkLevel(iLevel - 1).Value = 0) Then
                .Locked = True
                .BackColor = RGB(230, 230, 230)
'                .BackColor = Me.grdFormatting.Columns.Item(0).BackColor
            Else
                .Locked = False
                .BackColor = SystemColorConstants.vbWindowBackground
            End If
        End With
    End If
    
End Sub

Private Sub grdFormatting_UnboundGetRelativeBookmark(StartLocation As Variant, ByVal Offset As Long, NewLocation As Variant, ApproximatePosition As Long)
    NewLocation = GetRelativeBookmark(StartLocation, Offset)
    If Not IsNull(NewLocation) Then
       ApproximatePosition = IndexFromBookmark(NewLocation, 0)
    End If
End Sub

Private Function MakeBookmark(Index As Long) As Variant
    MakeBookmark = Str$(Index)
End Function

Private Function IndexFromBookmark(Bookmark As Variant, Offset As Long) As Long
    Dim Index As Long

    If IsNull(Bookmark) Then
        If Offset < 0 Then
            ' Bookmark refers to EOF. Since (MaxRow - 1)
            ' is the index of the last record, we can use
            ' an index of (MaxRow) to represent EOF.
            Index = 9 + Offset
        Else
            ' Bookmark refers to BOF. Since 0 is the index
            ' of the first record, we can use an index of
            ' -1 to represent BOF.
            Index = -1 + Offset
        End If
    Else
        ' Convert string to long integer
        Index = Val(Bookmark) + Offset
    End If

    If Index >= 0 And Index < 9 Then
       IndexFromBookmark = Index
    Else
       IndexFromBookmark = -9999
    End If
End Function

Private Function GetRelativeBookmark(Bookmark As Variant, Offset As Long) As Variant
    Dim Index As Long

    Index = IndexFromBookmark(Bookmark, Offset)

    If Index < 0 Or Index >= 9 Then
        ' Index refers to a row before the first or
        ' after the last, so just return Null.
        GetRelativeBookmark = Null
    Else
        GetRelativeBookmark = MakeBookmark(Index)
    End If
End Function

Private Function GetUserData(Bookmark As Variant, Col As Integer) As Variant
    Dim Index As Long

    Index = IndexFromBookmark(Bookmark, 0)
    If Index < 0 Or Index >= 9 Or Col < 0 Or Col >= 6 Then
        ' Cell position is invalid, so just return null
        ' to indicate failure
        GetUserData = Null
    Else
        GetUserData = oGridArray(Col, Index)
    End If
End Function

Private Function StoreUserData(Bookmark As Variant, Col As Integer, Userval As Variant) As Boolean
    Dim Index As Long
   
    Index = IndexFromBookmark(Bookmark, 0)
    
    If Index < 0 Or Index >= 9 Or Col < 0 Or Col >= 6 Then
        ' Cell position is invalid, so just return null
        ' to indicate failure
        StoreUserData = False
    Else
        StoreUserData = True
        
        'store to grid array
        oGridArray(Col, Index) = Userval
    End If
End Function

Private Sub optSelectedScheme_Click()
    On Error Resume Next
    Me.cbxScheme.SetFocus
End Sub

Private Function bDefinitionIsValid() As Boolean
    Dim i As Integer
    Dim iLen As Integer
    
'   ensure that sentence is defined by more than just spaces
    iLen = Len(Me.txtDefinition)
    For i = 1 To iLen
        If Mid(Me.txtDefinition, i, 1) <> g_xBullet Then
            Exit For
        ElseIf i = iLen Then
            MsgBox "A sentence cannot be defined solely by spaces.  " & _
                "Please enter at least one additional character.", _
                    vbExclamation, AppName
            Exit Function
        End If
    Next i
    
'   check for other invalid definitions
    Select Case Me.txtDefinition
        Case ""
'           no definition
            MsgBox "Please enter a sentence definition.", _
                vbExclamation, AppName
            Exit Function
        Case "."
'           period only
            MsgBox "This is not a valid sentence definition.  " & _
                "Please enter at least one space after the period.", _
                vbExclamation, AppName
            Exit Function
    End Select
    
    bDefinitionIsValid = True
End Function

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab = 0 Then
        Me.chkLevel(0).SetFocus
    Else
        Me.cbxFormatting.SetFocus
    End If
End Sub

Private Sub txtDefinition_KeyUp(KeyCode As Integer, Shift As Integer)
'display space as bullet
    If KeyCode = 32 Then
        With txtDefinition
            If .SelStart > 0 Then
                'conditional accounts for spacebar being pressed before IME input has been accepted
                .SelStart = .SelStart - 1
                .SelLength = 1
                .SelText = g_xBullet
            End If
        End With
    End If
End Sub

Private Sub LoadFormattingDefaults()
'sets formatting defaults based on selected scheme
    Dim xScheme As String
    Dim iFormat As Integer
    Dim iLevel As Integer
    Dim i As Integer
    Dim bSetDefaults As Boolean
    Dim xKey As String
    Dim j As Integer
    Dim oStyle As TrueDBGrid60.StyleDisp

    m_bLoadingDefaults = True

    xScheme = g_xDSchemes(Me.cbxFormatting.Bookmark, 0)
    For iLevel = 0 To 8
        '1/29/13 - set defaults only for enabled rows
        bSetDefaults = False
        If (Me.chkLevel(iLevel).Value = 1) Then
            If bLevelExists(xScheme, iLevel + 1, mpSchemeType_Document) Then
                bSetDefaults = Not bTOCLevelIsStyleBased(xScheme, iLevel + 1)
            End If
        End If
        
        If bSetDefaults Then
            'set defaults
            xKey = xScheme & CStr(iLevel + 1)
            If m_oFormattingChanges.Exists(xKey) Then
                'use updated values
                iFormat = CInt(m_oFormattingChanges(xKey))
            Else
                iFormat = CInt(xGetLevelProp(xScheme, iLevel + 1, _
                    mpNumLevelProp_HeadingFormat, mpSchemeType_Document))
            End If
            oGridArray(mpFormattingColumns.Bold, iLevel) = iHasTCFormat(iFormat, mpTCFormatField_Bold)
            oGridArray(mpFormattingColumns.Italic, iLevel) = iHasTCFormat(iFormat, mpTCFormatField_Italic)
            oGridArray(mpFormattingColumns.Underline, iLevel) = iHasTCFormat(iFormat, mpTCFormatField_Underline)
            oGridArray(mpFormattingColumns.SmallCaps, iLevel) = iHasTCFormat(iFormat, mpTCFormatField_SmallCaps)
            oGridArray(mpFormattingColumns.AllCaps, iLevel) = iHasTCFormat(iFormat, mpTCFormatField_AllCaps)
        Else
            'level doesn't exist
            oGridArray(mpFormattingColumns.Bold, iLevel) = 0
            oGridArray(mpFormattingColumns.Italic, iLevel) = 0
            oGridArray(mpFormattingColumns.Underline, iLevel) = 0
            oGridArray(mpFormattingColumns.SmallCaps, iLevel) = 0
            oGridArray(mpFormattingColumns.AllCaps, iLevel) = 0
        End If
    Next iLevel
    
    Me.grdFormatting.Rebind
    Me.grdFormatting.Refresh
    
    m_bLoadingDefaults = False
End Sub

Public Property Get FormattingChanges() As Scripting.Dictionary
    Set FormattingChanges = m_oFormattingChanges
End Property
