VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cWordXML"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function SetXMLMarkupState(ByVal oDocument As Word.Document, _
                                  ByVal bShowXML As Boolean) As Long
    SetXMLMarkupState = mdlGlobal.SetXMLMarkupState(oDocument, bShowXML)
End Function

Public Function GetTagSafeParagraphStart(ByVal oInsertionRange As Word.Range, _
                                         Optional ByRef iParagraphLevelTagsAtStart As Integer) As Long
'gets "start of paragraph" insertion location, ensuring that inside any multiparagraph
'tags that would otherwise force a new paragraph to be created above the tag(s)
    Const mpThisFunction As String = "mpnWordXML.cWordXML.MoveToTagSafeParagraphStart"
    Dim i As Integer
    Dim rngPara As Word.Range
    Dim iCount As Integer
    Dim oTag As Word.XMLNode
    
    On Error GoTo ProcError
    
    Set rngPara = oInsertionRange.Paragraphs(1).Range
    
    'count multiparagraph tags at start of paragraph -
    'mp10 GLOG 3914 - instead of # of paragraphs, check tag level - we
    'should be staying inside of and preserving all block tags
    For i = 1 To rngPara.XMLNodes.Count
        Set oTag = rngPara.XMLNodes(i)
        If (oTag.Range.Start = rngPara.Start + i) And _
                (oTag.Level <> wdXMLNodeLevelInline) Then
            iCount = iCount + 1
        Else
            Exit For
        End If
    Next i
    
    '9.9.4009
    iParagraphLevelTagsAtStart = iCount
    
    GetTagSafeParagraphStart = rngPara.Start + iCount
    
    Exit Function
ProcError:
    mdlGlobal.RaiseError mpThisFunction
End Function

Public Function GetTagSafeParagraphEnd(ByVal oInsertionRange As Word.Range, _
                                       Optional ByRef iParagraphLevelTagsAtEnd As Integer) As Long
'gets "end of paragraph" insertion location, ensuring that inside any multiparagraph
'tags that would otherwise force a new paragraph to be created below the tag(s)
    Const mpThisFunction As String = "mpnWordXML.cWordXML.MoveToTagSafeParagraphEnd"
    Dim i As Integer
    Dim rngPara As Word.Range
    Dim iCount As Integer
    Dim oTag As Word.XMLNode
    
    On Error GoTo ProcError
    
    Set rngPara = oInsertionRange.Paragraphs(1).Range
    With rngPara
'        'move to end of paragraph
        .MoveEnd wdCharacter, -1
    
        'count multiparagraph tags at start of paragraph
        'mp10 GLOG 3914 - instead of # of paragraphs, check tag level - we
        'should be staying inside of and preserving all block tags
        For i = 1 To .XMLNodes.Count
            Set oTag = .XMLNodes(i)
            If (oTag.Range.End = .End - i) And _
                    (oTag.Level <> wdXMLNodeLevelInline) Then
                iCount = iCount + 1
            Else
                Exit For
            End If
        Next i
    End With
    
    '9.9.4009
    iParagraphLevelTagsAtEnd = iCount
    
    GetTagSafeParagraphEnd = rngPara.End - iCount
    
    Exit Function
ProcError:
    mdlGlobal.RaiseError mpThisFunction
End Function

Private Function TagIsBlockable(ByVal oTag As Word.XMLNode) As Boolean
'returns TRUE if oTag can contain multiple paragraphs -
'converts inline tag if possible
    Const mpThisFunction As String = "mpnWordXML.cWordXML.TagIsBlockable"
    Dim oRange As Word.Range
    Dim lEnd As Long
    Dim bIsBlockable As Boolean
    
    On Error GoTo ProcError
    
    Set oRange = oTag.Range
    With oRange
        If .Paragraphs.Count > 1 Then
            'tag already contains multiple paragraphs
            bIsBlockable = True
        Else
            'test blockability by seeing whether tag expands when a paragraph mark
            'is inserted at the end of its range - note that this will automatically
            'convert inline tags if there's nothing else in the paragraph
            lEnd = .End
            
            On Error Resume Next
            .InsertAfter vbCr
            If Err = 5904 Then
                'cannot edit range - tag spans table,
                'which means it's already a block tag
                TagIsBlockable = True
                Err = 0
                Exit Function
            ElseIf Err > 0 Then
                GoTo ProcError
            End If
            On Error GoTo ProcError
            
            bIsBlockable = (oTag.Range.End > lEnd)
            
            If .Characters.Last = vbCr & Chr(7) Then _
                .MoveEnd wdCharacter, -1
            .Characters.Last.Delete
        End If
    End With
    
    TagIsBlockable = bIsBlockable
    
    Exit Function
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Function
End Function

Public Function CursorIsAtStartOfBlockTag() As Boolean
    Const mpThisFunction As String = "mpnWordXML.cWordXML.CursorIsAtStartOfBlockTag"
    Dim oTag As Word.XMLNode
    Dim bIsAtStart As Boolean
    
    On Error GoTo ProcError
    
    Set oTag = Selection.XMLParentNode
    If Not oTag Is Nothing Then
        If TagIsBlockable(oTag) Then
            bIsAtStart = (Selection.Start = oTag.Range.Start)
        End If
    End If
    
    CursorIsAtStartOfBlockTag = bIsAtStart
    
    Exit Function
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Function
End Function

Public Sub SetControlFaceID(ByVal oControl As CommandBarControl, ByVal lFaceID As Long)
'this method is in this dll and class to avoid a compile error in Word 2000 (GLOG 4780)
    Const mpThisFunction As String = "mpnWordXML.cWordXML.SetControlFaceID"
    On Error GoTo ProcError
    oControl.FaceId = lFaceID
    Exit Sub
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub

Public Function CountTagsEndingInRange(ByVal oRange As Word.Range) As Integer
'returns the number of XML tags in the specified range that end before the end of the range
    Const mpThisFunction As String = "mpnWordXML.cWordXML.CountTagsEndingInRange"
    Dim i As Integer
    Dim iCount As Integer

    On Error GoTo ProcError
    
    For i = 1 To oRange.XMLNodes.Count
        If oRange.XMLNodes(i).Range.End < oRange.End Then _
            iCount = iCount + 1
    Next i
    
    CountTagsEndingInRange = iCount
    
    Exit Function
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Function
End Function

Public Sub InsertStyleSeparator()
    Const mpThisFunction As String = "mpnWordXML.cWordXML.InsertStyleSeparator"
    On Error GoTo ProcError
    Selection.InsertStyleSeparator
    Exit Sub
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub

Public Function IsStyleSeparator(ByVal oPara As Word.Paragraph) As Boolean
    Const mpThisFunction As String = "mpnWordXML.cWordXML.IsStyleSeparator"
    On Error GoTo ProcError
    IsStyleSeparator = oPara.IsStyleSeparator
    Exit Function
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Function
End Function

Public Function GetPositionAfterInlineTag(ByVal oRange As Word.Range) As Long
'if specified range is inside an inline tag, returns the position immediately after it
    Const mpThisFunction As String = "mpnWordXML.cWordXML.GetPositionAfterInlineTag"
    Dim oTag As Word.XMLNode
    Dim lPos As Integer
    
    On Error GoTo ProcError
    
    Set oTag = oRange.XMLParentNode
    If Not oTag Is Nothing Then
        If oTag.Level = wdXMLNodeLevelInline Then
            lPos = oTag.Range.End + 1
        End If
    End If
    
    GetPositionAfterInlineTag = lPos
    
    Exit Function
ProcError:
    Err.Raise Err.Number, mpThisFunction, Err.Description
    Exit Function
End Function

