VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPublicSchemes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Shared Schemes"
   ClientHeight    =   5085
   ClientLeft      =   17670
   ClientTop       =   8625
   ClientWidth     =   6315
   Icon            =   "frmPublicSchemes.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5085
   ScaleWidth      =   6315
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.TreeView tvwSchemes 
      Height          =   4080
      Left            =   60
      TabIndex        =   1
      Top             =   390
      Width           =   2145
      _ExtentX        =   3784
      _ExtentY        =   7197
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   0
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin VB.VScrollBar vsbPreview 
      Height          =   4080
      LargeChange     =   1025
      Left            =   6015
      Max             =   4100
      SmallChange     =   410
      TabIndex        =   7
      Top             =   420
      Value           =   375
      Width           =   225
   End
   Begin VB.CommandButton btnViewProfile 
      Caption         =   "&View Profile"
      Height          =   380
      Left            =   120
      TabIndex        =   6
      Top             =   4620
      Width           =   2025
   End
   Begin VB.CommandButton btnImport 
      Caption         =   "&Import"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   380
      Left            =   4320
      TabIndex        =   2
      Top             =   4620
      Width           =   880
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   380
      Left            =   5280
      TabIndex        =   5
      Top             =   4620
      Width           =   880
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "&Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   380
      Left            =   4320
      TabIndex        =   3
      Top             =   4620
      Width           =   880
   End
   Begin VB.PictureBox imgContainer 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      Height          =   4080
      Left            =   2280
      ScaleHeight     =   4020
      ScaleWidth      =   3930
      TabIndex        =   8
      Top             =   390
      Width           =   3990
      Begin VB.Image Image1 
         Height          =   375
         Left            =   -15
         Picture         =   "frmPublicSchemes.frx":058A
         Top             =   0
         Width           =   4365
      End
      Begin VB.Image imgPreview 
         Height          =   7995
         Left            =   0
         Top             =   0
         Width           =   3705
      End
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   380
      Left            =   4320
      TabIndex        =   4
      ToolTipText     =   "Use selected scheme to insert numbers."
      Top             =   4620
      Visible         =   0   'False
      Width           =   880
   End
   Begin VB.Label lblPublicSchemes 
      Caption         =   "Available Shared &Schemes:"
      Height          =   300
      Left            =   105
      TabIndex        =   0
      Top             =   165
      Width           =   2190
   End
End
Attribute VB_Name = "frmPublicSchemes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_xDir As String
Private m_bCancelled As Boolean
Private m_xarFiles As xArray
Private m_xDesc As String

Public Property Let PublicDir(xNew As String)
    If Right(xNew, 1) <> "\" Then
        xNew = xNew & "\"
    End If
    m_xDir = xNew
End Property

Public Property Get PublicDir() As String
    PublicDir = m_xDir
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    Unload Me
End Sub

Private Sub btnDelete_Click()
    Dim lRet As Long
    Dim oShare As CShare
    Set oShare = New CShare
    Dim xFile As String
    
    With Me.tvwSchemes
        xMsg = "Delete the " & .SelectedItem & " scheme?"
        iUserChoice = MsgBox(xMsg, vbYesNo, g_xAppName)
        If iUserChoice = vbYes Then
            Me.MousePointer = vbHourglass
            xFile = PublicSchemesDir() & _
                .SelectedItem.Parent & "\" & _
                .SelectedItem.Key
            lRet = oShare.DeletePublicScheme(xFile)
            If lRet Then
                On Error Resume Next
                Err.Raise lRet
                RaiseError "frmPubliceSchemes.Delete_Click"
            Else
                .Nodes.Remove .SelectedItem.Index
            End If
            tvwSchemes_NodeClick .SelectedItem
            Me.MousePointer = vbDefault
        End If
    End With
End Sub

Private Sub btnImport_Click()
    Dim oShare As CShare
    Dim xScheme As String
    Set oShare = New CShare
        
    Me.Hide
    xScheme = oShare.ImportPublicScheme()
    If xScheme <> "" Then
        Set g_oCurScheme = GetRecord(xScheme, mpSchemeType_Private)
        Cancelled = False
    End If
End Sub

Private Sub btnOK_Click()
    Me.Hide
    Me.Cancelled = False
    DoEvents
End Sub

Private Sub btnViewProfile_Click()
    ShowProfile
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    On Error GoTo Proc_Error
    
    Me.Cancelled = True
    
'   add categories to tree
    GetPublicCategories
    
'   load schemes - this is necessary to get + signs
'   next to categories
    With Me.tvwSchemes
        For i = 1 To .Nodes.Count
            GetPublicSchemes .Nodes(i).Key
        Next i
    End With
    
    DoEvents
    
'   select first category
    On Error Resume Next
    Me.tvwSchemes.Nodes(1).FirstSibling.Selected = True
    
'   if err is generated, it's due to lack
'   of shared scheme categories - alert
    If Err.Number Then
        On Error GoTo Proc_Error
        Unload Me
        Err.Raise mpError_NoSharedSchemeCategories
    Else
        On Error GoTo Proc_Error
    End If
    
    tvwSchemes_NodeClick Me.tvwSchemes.SelectedItem
    
    Exit Sub
Proc_Error:
    Select Case Err.Number
        Case Else
            RaiseError "frmPublicSchemes.Form_Load"
    End Select
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_xarFiles = Nothing
    Set frmPublicSchemes = Nothing
End Sub

Private Function GetPublicCategories()
'   adds all scheme categories to tree
    Dim xDir As String
    Dim bFound As Boolean
    
'   cycle through all dirs in shared schemes dir
    xDir = Dir(PublicSchemesDir() & "*.*", vbDirectory)
    While xDir <> ""
        If (xDir <> ".") And (xDir <> "..") Then
'           add
            Me.tvwSchemes.Nodes.Add , , xDir, xDir
        End If
        xDir = Dir()
    Wend
End Function

Private Function GetPublicSchemes(xCategory As String)
'   add shared schemes in Category
'   xCategory to the tree
    Dim xFile As String
    Dim xDisplayName As String
    Dim nodeCat As MSComctlLib.Node
    Dim xOld As String
    Dim xNew As String
    Dim xRnd As String
    Dim xPublicDir As String
    
    Set nodeCat = Me.tvwSchemes.Nodes(xCategory)
    
 '  cycle through all mpn files in shared schemes dir
    xPublicDir = PublicSchemesDir()
    xFile = Dir(xPublicDir & xCategory & "\" & "*.mpn")
    While xFile <> ""
'       trim extension from file
        xDisplayName = Left(xFile, InStr(xFile, "@@_") - 1)
        xDisplayName = xSubstitute(xDisplayName, "=", "/")
        On Error Resume Next
        Me.tvwSchemes.Nodes.Add nodeCat, tvwChild, xFile, xDisplayName
        If Err = 35602 Then
'           this is an exact duplicate of a scheme in another folder - client
'           has manually copied files - rename doc and bitmap
            Randomize
            xRnd = Mid(Rnd(), 3)
            
'           rename doc
            xOld = xPublicDir & xCategory & "\" & xFile
            xNew = xPublicDir & xCategory & "\" & xDisplayName & _
                "@@_" & xRnd & ".mpn"
            Name xOld As xNew
            
'           add to tree
            Me.tvwSchemes.Nodes.Add nodeCat, tvwChild, _
                xDisplayName & "@@_" & xRnd & ".mpn", xDisplayName
            
'           rename bitmap
            xOld = xPublicDir & xCategory & "\" & _
                xSubstitute(xFile, ".mpn", ".mpb")
            xNew = xPublicDir & xCategory & "\" & xDisplayName & _
                "@@_" & xRnd & ".mpb"
            Name xOld As xNew
        End If
        On Error GoTo 0
        xFile = Dir()
    Wend
End Function

Private Function ShowProfile(Optional bVisible As Boolean = True) As String
    Dim xFile As String
    Dim tplP As Word.Template
    Dim dpsP As Office.DocumentProperties
    Dim xPostedBy As String
    Dim xPostedOn As String
    Dim xDescription As String
    Dim oShare As CShare
    
    If bVisible Then
        Me.MousePointer = vbHourglass
        
'       add as template
        With Me.tvwSchemes.SelectedItem
            If (.Parent Is Nothing) Then
'               a category is selected
                Me.MousePointer = vbDefault
                Exit Function
            End If
            xFile = PublicSchemesDir() & .Parent.Text & _
                    "\" & .Key
        End With
        Word.AddIns.Add xFile, True
        Set tplP = mdlApplication.GetTemplate(xFile)
        
'       get doc properties
        With tplP.CustomDocumentProperties
            xDescription = .Item("Description").Value
            xPostedBy = .Item("PostedBy").Value
            xPostedOn = Format(.Item("PostedOn").Value, "mmmm d, yyyy")
        End With
        
        tplP.Saved = True
    
'       remove as template
        Word.AddIns.Item(xFile).Delete
        
        With frmSchemeProfile
            .btnOK.Visible = False
            .btnCancel.Caption = "Close"
            .btnCancel.Default = True
            .btnCancel.Cancel = True
            .txtDescription = xDescription
            .txtDescription.Locked = True
            .txtPostedBy = xPostedBy
            .txtPostedBy.Locked = True
            .lblPostedOnText = xPostedOn
            .cbxCategories = Me.tvwSchemes.SelectedItem.Parent.Text
            .cbxCategories.Enabled = False
            .lblSchemeText = Me.tvwSchemes.SelectedItem.Text
            .cbxCategories.BackColor = vbButtonFace
            .txtDescription.BackColor = vbButtonFace
            .txtPostedBy.BackColor = vbButtonFace
            .Show vbModal
        End With
        ShowProfile = "Posted By: " & xPostedBy & vbCr & _
                           "Posted On: " & xPostedOn & vbCr & _
                           "Description:  " & xDescription
        Unload frmSchemeProfile
    End If
    Me.MousePointer = vbDefault
    Exit Function
ProcError:
    Me.MousePointer = vbDefault
    RaiseError "frmPublicSchemes.ShowProfile"
    Exit Function
End Function

Private Function GetSchemeDescription(lRowIndex As Long) As String
    Dim xFile As String
    Dim tplP As Word.Template
    Dim dpsP As Office.DocumentProperties
    Dim oShare As CShare
    
'   add as template
    xFile = PublicSchemesDir() & m_xarFiles(lRowIndex, 1)
'    xFile = PublicSchemesDir() & Me.lstSchemes.Columns("fldFileName")
    Word.AddIns.Add xFile, True
    Set tplP = mdlApplication.GetTemplate(xFile)
    
'   get doc properties
    GetSchemeDescription = tplP.CustomDocumentProperties _
                                .Item("Description").Value
    
    tplP.Saved = True
    
'   remove as template
    Word.AddIns.Item(xFile).Delete
End Function

Private Sub tvwSchemes_Click()
    Dim xFile As String
    Dim bIsScheme As Boolean
    xFile = tvwSchemes.SelectedItem.Key
    bIsScheme = Right(xFile, 4) = ".mpn"
    Me.btnOK.Enabled = bIsScheme
    Me.btnDelete.Enabled = bIsScheme
    Me.btnImport.Enabled = bIsScheme
    Me.btnViewProfile.Enabled = bIsScheme
    
End Sub

Private Sub tvwSchemes_DblClick()
    tvwSchemes_NodeClick Me.tvwSchemes.SelectedItem
    If InStr(Me.Caption, "Import") Then
        If btnImport.Enabled Then _
            btnImport_Click
    Else
        If btnDelete.Enabled Then _
            btnDelete_Click
    End If
End Sub

Private Sub tvwSchemes_NodeClick(ByVal Node As MSComctlLib.Node)
    Dim xBitmap As String
    Dim xFile As String
    Dim xCategory As String
    Dim bIsScheme As Boolean
    
    On Error GoTo ProcError
    Me.MousePointer = vbHourglass
    With Me.tvwSchemes
        xFile = .SelectedItem.Key
        bIsScheme = Right(xFile, 4) = ".mpn"
        Me.btnOK.Enabled = bIsScheme
        Me.btnDelete.Enabled = bIsScheme
        Me.btnImport.Enabled = bIsScheme
        Me.btnViewProfile.Enabled = bIsScheme
        
        If bIsScheme Then
'           selected node is a scheme
            xCategory = .SelectedItem.Parent.Text
            xBitmap = PublicSchemesDir() & xCategory & _
                "\" & xSubstitute(Left(xFile, Len(xFile) - 4), _
                "/", "=") & ".mpb"
        Else
            xBitmap = mpBase.GetAppPath & "\" & "PubCat.mpb"
            ShowProfile False
            If .SelectedItem.Children = 0 Then
                GetPublicSchemes .SelectedItem.Key
            End If
        
            .SelectedItem.Expanded = Not .SelectedItem.Expanded
        End If
    End With
    Me.Image1.Visible = bIsScheme
    Me.imgPreview.Picture = LoadPicture(xBitmap)
    
    If bIsScheme Then
        Me.vsbPreview.Value = 0
    Else
        Me.vsbPreview.Value = 375
    End If
    
    Me.MousePointer = vbDefault
    Exit Sub
ProcError:
    Me.MousePointer = vbDefault
    Select Case Err.Number
        Case 53 'file not found
            Me.imgPreview.Picture = LoadPicture(mpBase.GetAppPath & "\NoPreview.mpb")
        Case Else
            MsgBox Err.Number & " :: " & _
                   Err.Description & _
                   " :: frmPublicSchemes.tvwSchemes_NodeClick"
    End Select
    Exit Sub
End Sub

Private Sub vsbPreview_Change()
    ScrollPreview
End Sub

Private Sub ScrollPreview()
    EchoOff
    Me.imgPreview.Top = Me.imgContainer - Me.vsbPreview.Value + 375
    DoEvents
    EchoOn
End Sub

Private Sub vsbPreview_Scroll()
    ScrollPreview
End Sub
