VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPreview"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**********************************************************
'   CPreview Collection Class
'   created 3/01/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   CPreview object - the numbering Preview - the preview
'   is the visual representation of the numbering scheme
'   that is displayed when the edit scheme dialog is active.
'   numbering bitmaps are created by using bit block transfers
'   of the screen representation of the preview.
'**********************************************************
Private Const GW_CHILD = 5
Private Const GW_HWNDFIRST = 0
Private Const GW_HWNDLAST = 1
Private Const GW_HWNDNEXT = 2
Private Const GW_HWNDPREV = 3
Private Const GW_MAX = 5

Private Declare Function GetWindow Lib "user32" ( _
    ByVal hwnd As Long, ByVal wCmd As Long) As Long
Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" ( _
    ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" ( _
    ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function IsWindowVisible Lib "user32" ( _
    ByVal hwnd As Long) As Long

Private Type udtWindowProps
    iZoom As Integer
    bShowAll As Boolean
    bShowTabs As Boolean
    bShowSpaces As Boolean
    bShowParas As Boolean
    bShowTableGrids As Boolean
    iView As Integer
End Type

Private m_lRefreshes As Long

Private Declare Function GetWindowPlacement Lib "user32" ( _
    ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
    
Private Type POINTAPI
    X As Long
    Y As Long
End Type

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type WINDOWPLACEMENT
    Length As Long
    flags As Long
    showCmd As Long
    ptMinPosition As POINTAPI
    ptMaxPosition As POINTAPI
    rcNormalPosition As RECT
End Type

Public Function ShowPreview(ByVal xScheme As String, ByVal iSchemeType As mpSchemeTypes) As IPictureDisp
    Dim rngLocation As Word.Range
    Dim xAutoText As String
    Dim oAutoText As Word.AutoTextEntry
    Dim WinProps As udtWindowProps
    Dim rngP As Word.Range
    Dim xSource As String
    Dim iZoom As Integer
    
    EchoOff
    Word.Application.ScreenUpdating = False
    
'   get current zoom
    On Error Resume Next
    iZoom = xGetUserIni("Numbering", "Zoom")
    On Error GoTo 0
    
    With Word.ActiveDocument
'       set margins/pageheight only if necessary - there's a Word quirk
'       such that setting page setup programmatically the first time
'       in a session takes an inordinate amount of time - this solution
'       is the best we can do.
        With .Content.Sections(1).PageSetup
            If .Orientation = wdOrientLandscape Then
                .Orientation = wdOrientPortrait
            End If
            If (.LeftMargin <> 90) Then
                .LeftMargin = 90
            End If
            If (.RightMargin <> 90) Then
                .RightMargin = 90
            End If
            If .PageHeight < (22 * 72) Then
                .PageHeight = (22 * 72)
            End If
        End With
        
        Set rngLocation = .Content
        rngLocation.StartOf
        
'       9.7.4 - decided to always create the preview on the fly, after three
'       different XP clients experienced the same problem with corrupt autotext;
'       specifically, some of the numbered paragraphs in the preview table are
'       winding up with simple numbering, causing errors in AdjustLines
        
'        If iSchemeType = mpSchemeType_Private Then
''           insert preview autotext
'            xAutoText = xScheme & "Preview"
'            On Error Resume Next
'            Set oAutoText = Templates(g_xPNumSty) _
'                        .AutoTextEntries(xAutoText)
'            On Error GoTo 0
'
'            If Not (oAutoText Is Nothing) Then
'                On Error Resume Next
'                oAutoText.Insert rngLocation, True
'                Zoom CBool(iZoom), False
'                If Err.Number Then
'                    On Error GoTo 0
'                    InsertTable rngLocation, xScheme
'                Else
''                   relink list template to level 1 style this
''                   guarantees that list levels display correctly
'                    On Error GoTo 0
'                    Set rngP = Word.ActiveDocument.Tables(1).Cell(1, 1).Range
'                    With rngP.ListFormat
'                        If Not g_bIsXP Then
''                           not even sure whether this relinking is necessary anymore,
''                           but in XP, it causes all style indents to go to 0
'                            Word.ActiveDocument.Styles(rngP.Style) _
'                                .LinkToListTemplate .ListTemplate, .ListLevelNumber
'                        End If
'                    End With
'                End If
'            Else
'                InsertTable rngLocation, xScheme
'            End If
'        Else
            InsertTable rngLocation, xScheme
'        End If
        
'       add trailing returns to move
'       end of file line below screen
        Set rngLocation = Word.ActiveDocument.Content
        With rngLocation
            .EndOf
            .InsertAfter String(160, vbCr)
            .Style = wdStyleNormal
            .EndOf
            .Select
        End With
    End With
        
    On Error Resume Next
    With Word.ActiveWindow
        .VerticalPercentScrolled = 0
        With .View
            .TableGridlines = False
            .ShowAll = False
            .ShowTabs = False
            .ShowSpaces = False
            .ShowParagraphs = False
            .Type = wdNormalView
        End With
        .StyleAreaWidth = 0
    End With
    EchoOn
End Function

Private Sub InsertTable(rngLocation As Word.Range, xScheme As String)
    Dim i As Integer
    Dim iLevels As Integer
    Dim xStyle As String
    Dim rngParaStart As Word.Range
    Dim tblPreview As Word.Table
    Dim rngLevel As Word.Range
    Dim rngHeading As Word.Range
    Dim iZoom As Integer
    
'    Application.StatusBar = "Updating Preview..."
    
    On Error Resume Next
    iZoom = xGetUserIni("Numbering", "Zoom")
    On Error GoTo 0
    Zoom iZoom, False
    
    With Word.ActiveDocument
        On Error Resume Next
'       add a table at the beginning of document
        Set tblPreview = rngLocation.Tables.Add(rngLocation, 9, 1)
        
        tblPreview.Borders.Enable = False
        tblPreview.Rows.HeightRule = wdRowHeightAuto

        iLevels = iGetLevels(xScheme, mpSchemeType_Document)
        
'       insert paras for all levels
        For i = 1 To iLevels
            On Error Resume Next
            If xScheme = "HeadingStyles" Then
                xStyle = xTranslateHeadingStyle(i)
            Else
                xStyle = xGetStyleRoot(xScheme) & "_L" & i
            End If
            If .Styles(xStyle) Is Nothing Then
                Exit For
            End If
            On Error GoTo 0
            Set rngLevel = InsertLevel(tblPreview, xScheme, i)
        Next i
    End With
    
    Word.ActiveDocument.UndoClear
'    Application.StatusBar = ""
End Sub

Sub RefreshPreviewLevel(xScheme As String, iLevel As Integer)
    Dim rngP As Word.Range
    Dim i As Integer
    Dim iLineSpaceRule As WdLineSpacing
    Dim iLineSpacing As WdLineSpacing
    Dim iPreLineSpacing As Integer
    Dim iPostLineSpacing As Integer
    Dim xPath As String
    
    On Error Resume Next
    With Word.ActiveDocument
        Set rngP = .Tables(1).Cell(iLevel, 1).Range
        If rngP Is Nothing Then
            Exit Sub
        End If
        
'       get spacing before refresh of level
        iPreLineSpacing = rngP.ParagraphFormat.LineSpacing

        'GLOG 5463 - removing this screen refresh prevents Word
        'from crashing when editing with multiple Bingham documents
        'of a certain type open in print layout view - what reason
        'could there be for a screen refresh here?
'        Application.ScreenRefresh
        
'       remove number
        rngP.Delete
        rngP.Style = wdStyleNormal
        InsertLevel .Tables(1), xScheme, iLevel
        
'       increment the number of refreshes executed-
'       when this number reaches a threshold, save
'       preview - this prevents "Formatting is too
'       complex" message from Word
        m_lRefreshes = m_lRefreshes + 1
        If m_lRefreshes Mod 28 = 0 Then
            xPath = g_xUserPath & "\Refresh.mpf"
            .SaveAs xPath, , , , False
            'there were circumstances in which there was
            'no active doc after resaving (see GLOG 4756)
            Documents(xPath).Activate
            If g_lUILanguage = wdFrenchCanadian Then
                .ActiveWindow.Caption = "Aper�u Th�me"
            Else
                .ActiveWindow.Caption = "Scheme Preview"
            End If
        End If
        
'       show change now
        EchoOn
    End With
End Sub

Private Function InsertLevel(tblPreview As Word.Table, _
                             ByVal xScheme As String, _
                             ByVal iLevel As Integer) As Word.Range
    Dim xStyle As String
    Dim rngParaStart As Word.Range
    Dim i As Integer
    Dim bHeadingIsPara  As Boolean
    Dim bLinesInserted As Boolean
    Dim llCur As Word.ListLevel
    Dim iHeadingFormat As Integer
    Dim lDelStart As Long
    Dim lDelEnd As Long
    Dim iLineSpaceRule As WdLineSpacing
    Dim iLineSpacing As Single
    Dim iAlignment As WdParagraphAlignment
    Dim iSpaceAfter As Integer
    Dim iMove As Integer
    Dim iTrailUnderline As Integer
                            
'   get style name
    xStyle = xGetStyleName(xScheme, iLevel)
    
'   set range as appropriate row of table
    Set rngParaStart = tblPreview.Range.Cells(iLevel) _
                       .Range.Paragraphs(1).Range

'   insert number
    Set rngParaStart = rngInsertNumWordNum(rngParaStart, _
                                           xScheme, _
                                           iLevel)
    Set llCur = rngParaStart.ListFormat _
                .ListTemplate.ListLevels(iLevel)
    
    With rngParaStart
        With .ParagraphFormat
'            .LeftIndent = llCur.TextPosition
'            .FirstLineIndent = llCur.NumberPosition - _
'                               llCur.TextPosition
            On Error Resume Next
            .TabStops(1).Position = llCur.TabPosition
            
'           delete extra tab stops (12/21/00)
            For i = 2 To .TabStops.Count
                With .TabStops(i)
                    If .Position <> llCur.TabPosition Then
                        .Clear
                    End If
                End With
            Next i
        End With
        .StartOf
        .InsertAfter g_xPreviewLevelText & iLevel
            
'       format info is held as bits
'       in an integer - get integer
        iHeadingFormat = xGetLevelProp(xScheme, _
                                  iLevel, _
                                  mpNumLevelProp_HeadingFormat, _
                                  mpSchemeType_Document)
        With rngParaStart.Font
'           test bits for format value
            .Bold = iHasTCFormat(iHeadingFormat, mpTCFormatField_Bold)
            .Italic = iHasTCFormat(iHeadingFormat, mpTCFormatField_Italic)
            .AllCaps = iHasTCFormat(iHeadingFormat, mpTCFormatField_AllCaps)
            .Underline = iHasTCFormat(iHeadingFormat, mpTCFormatField_Underline)
            .SmallCaps = iHasTCFormat(iHeadingFormat, mpTCFormatField_SmallCaps)
            .ColorIndex = wdBlue
        End With

        .EndOf
            
        With .ParagraphFormat
'           guarantee that left indent and first line indent
'           show the indents of the list template by setting
'           the para formats to the values of the corresponding
'           list templates - there are instances where the
'           autotext previews lose there 'number indent' and
'           'text indent' values - i suspect that the para values
'           are overriding them
'            .LeftIndent = llCur.TextPosition
'            .FirstLineIndent = llCur.NumberPosition - .LeftIndent
                        
'           if user has chosen to base para on host doc
'           "normal" style, preview as left aligned;
'           right alignment vs. base on normal is now held in
'           trailing underline level prop
            iTrailUnderline = xGetLevelProp(xScheme, _
                                            iLevel, _
                                            mpNumLevelProp_TrailUnderline, _
                                            mpSchemeType_Document)
            If ((.Alignment = wdAlignParagraphRight) And (iTrailUnderline < 2)) Or _
                    bBitwisePropIsTrue(iTrailUnderline, _
                    mpTrailUnderlineField_AdjustToNormal) Then
                .Alignment = wdAlignParagraphLeft
            End If
            iAlignment = .Alignment
            .PageBreakBefore = False
        End With
        
'       insert lines if not centered
        If Not iAlignment = wdAlignParagraphCenter Then
            .InsertAfter String(24, vbTab)
            .Underline = wdUnderlineSingle
            
            bHeadingIsPara = (iHeadingFormat And _
                              mpTCFormatField_Type)
                              
            If bHeadingIsPara Then
                .Font.ColorIndex = wdBlue
            Else
                .Font.ColorIndex = wdAuto
            End If
            bLinesInserted = True
        End If

        With rngParaStart
            .Expand wdParagraph

'           set font size
            If bIsZoomedOut() Then
                .Font.Size = 20
            Else
                .Font.Size = 12
            End If
            
'           get eo para
            lDelEnd = .End
            
            .Select
        End With
        
'       delete all text after first two lines
        Selection.StartOf
        If InStr(rngParaStart, String(2, 11)) Then
            iMove = 3
        Else
            iMove = 2
        End If

        lRet = Selection.Move(wdLine, iMove)
        lDelStart = Selection.Start

        If iAlignment <> wdAlignParagraphJustify Then
'           delete one extra tab at start of range-
'           this will mimic left or center alignment
            lDelStart = lDelStart - 1
        End If
        ActiveDocument.Range(lDelStart, lDelEnd).Text = ""
        If iAlignment = wdAlignParagraphLeft Then
            rngParaStart.InsertAfter "__"
        End If
        With rngParaStart.ParagraphFormat
'           set line spacing
            If .LineSpacing < 16 Then
                iLineSpacing = 17.5
            ElseIf .LineSpacing < 22 Then
                iLineSpacing = 24
            ElseIf .LineSpacing < 28 Then
                iLineSpacing = 30
            Else
                iLineSpacing = 36
            End If
            .LineSpacingRule = wdLineSpaceExactly
            .LineSpacing = iLineSpacing
            
'            iLineSpaceRule = ConvertLineSpace(.LineSpacingRule, .LineSpacing)
'            .LineSpacingRule = wdLineSpaceExactly
'            If iLineSpaceRule = wdLineSpace1pt5 Then
'                iLineSpacing = 24
'            ElseIf iLineSpaceRule = wdLineSpaceDouble Then
'                iLineSpacing = 30
'            Else
'                iLineSpacing = 17.5
'            End If
'            .LineSpacing = iLineSpacing
        End With
        .EndOf
        Set InsertLevel = rngParaStart
    End With

'   move insertion point out of the way
    Word.ActiveDocument.Paragraphs.Last.Range.Select
    Word.ActiveWindow.VerticalPercentScrolled = 100
    Word.ActiveWindow.VerticalPercentScrolled = 0
End Function

Private Function CapturePreviewWindow(ByVal xFile As String) As Picture
    Dim sHeight As Single
    Dim xText As String
    Dim lText As Long
    Dim hWndActive As Long
    Dim hWndTop As Long
    Dim oBitmap As CBitmap
    Dim iCharsRet As Integer
    Dim oPlacement As WINDOWPLACEMENT
    Dim lRet As Long
    Dim lLeft As Long
    Dim lTop As Long
    
    xText = String(100, " ")
    lText = Len(xText)

    On Error GoTo 0
    
'   get Word app window - conditional reworked for Word 10
    If InStr(Word.Application.Version, "8.") > 0 Then
        hWndTop = FindWindow(vbNullString, _
                             Word.Application.Caption)
    Else
        hWndTop = FindWindow(vbNullString, Word.ActiveWindow.Caption & _
            " - " & Word.Application.Caption)
    End If
    
    If hWndTop = 0 Then
        If g_lUILanguage = wdFrenchCanadian Then
            MsgBox "Ne peut trouver la fen�tre de l'aper�u.  L'image ne peut pas �tre cr�e/mise � jour.", vbCritical, g_xAppName
        Else
            MsgBox "Unable to locate preview window.  The bitmap cannot not be " & _
                "created/updated.", vbCritical, g_xAppName
        End If
        Exit Function
    End If

'   get first Word child window
    hWndActive = GetWindow(hWndTop, GW_CHILD)
    
'   get preview doc window, which is the first
'   visible child window that has no window text
    iCharsRet = GetWindowText(hWndActive, xText, lText)
    
    If InStr(Word.Application.Version, "8.") > 0 Then
        While iCharsRet Or Not (IsWindowVisible(hWndActive) = 1)
            hWndActive = GetWindow(hWndActive, GW_HWNDNEXT)
            iCharsRet = GetWindowText(hWndActive, xText, lText)
        Wend
    Else
        While InStr(xText, "MsoDockLeft") = 0
            hWndActive = GetWindow(hWndActive, GW_HWNDNEXT)
            iCharsRet = GetWindowText(hWndActive, xText, lText)
        Wend
    End If

    '9.9.5001 - since we've been unable to successfully capture the document body
    'window directly in Word 2013 on Windows 8, instead just get its
    'position and trim a screenshot of the entire desktop accordingly -
    'also account for the ruler that's not displaying in draft view
    If g_iWordVersion >= mpWordVersion_2013 Then
        oPlacement.Length = Len(oPlacement)
        lRet = GetWindowPlacement(hWndActive, oPlacement)
        lLeft = oPlacement.rcNormalPosition.Left + 19
        lTop = oPlacement.rcNormalPosition.Top + 22
        hWndActive = 0
    Else
        lLeft = 19
        lTop = 26
    End If
    
'   capture the active window given its handle
'   and return the Resulting Picture object.
    sHeight = mpMin(400, Word.ActiveWindow.Height + 40)
    Set oBitmap = New CBitmap
    Set CapturePreviewWindow = _
        oBitmap.CaptureWindow(hWndActive, lLeft, lTop, 240, sHeight, xFile)
End Function

Sub CreateBitmap(ByVal xScheme As String, ByVal xAlias As String)
    Dim oPicture As Picture
    Dim rngP As Word.Range
    Dim i As Long
    Dim xBitmap As String
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If

'   remove highlight - on some machines, it shows up in snapshot
    '3/22/13 - entire preview was turning black on a VM machine with Word 2013
    If g_iWordVersion < mpWordVersion_2013 Then _
        RemoveHighlight
    
    If Not bIsZoomedOut() Then
'       ensure zoom out
        EchoOff
        ZoomOut
        
        '9.9.6006 (8/27/15)
        If g_iWordVersion = mpWordVersion_2013 Then
            Dim l As Long
            For l = 1 To 100
                DoEvents
            Next l
        End If
        
        EchoOn
    End If

    With Word.Application
        Set rngP = .ActiveDocument.Tables(1).Range

        With Application
            .ScreenUpdating = True
            .ScreenRefresh
            .ScreenUpdating = False
            .Activate
        End With
    
        xBitmap = xSubstitute(xAlias, "/", "=")
        Set oPicture = CapturePreviewWindow( _
                    g_xMPBPath & "\" & xBitmap & ".mpb")
                
'9.8.1007 - removed code below because we're now keeping sty files loaded
'and we stopped using autotext entries in 9.7.4
''       attempt to add autotext entry - ignore
''       if not successful - scheme preview will
''       then be created on the fly
'        On Error Resume Next
''       store preview table as autotext
'        .Templates(g_xPNumSty).AutoTextEntries.Add _
'                    xScheme & "Preview", rngP
'        If Err.Number Then
'            Err.Clear
'            .Templates(g_xPNumSty) _
'                .AutoTextEntries(xScheme & "Preview").Delete
'        End If
'
'        #If compHandleErrors Then
'            On Error GoTo ProcError
'        #End If
    End With
    
    Exit Sub
ProcError:
    EchoOn
    RaiseError "CPreview.CreateBitmap"
    Exit Sub
End Sub

Function GetFile(ByVal xAlias As String, ByVal iSchemeType As mpSchemeTypes) As String
    Dim xPreviewFilePath As String
    Dim xBitmap As String
    Dim xAppPath As String
    
'   get appropriate preview file
    xAppPath = mpBase.GetAppPath
    xBitmap = xSubstitute(xAlias, "/", "=")
    Select Case iSchemeType
        Case mpSchemeType_Document
            xPreviewFilePath = xAppPath & "\DocScheme" & ".mpb"
        Case mpSchemeType_Private
            xPreviewFilePath = g_xMPBPath & "\" & xBitmap & ".mpb"
            
            If Dir(xPreviewFilePath) = "" Then
                xPreviewFilePath = xAppPath & "\NoPreview.mpb"
            End If
        Case mpSchemeType_Public
            xPreviewFilePath = xAppPath & "\" & xBitmap & ".mpb"
            
            If Dir(xPreviewFilePath) = "" Then
                xPreviewFilePath = xAppPath & "\NoPreview.mpb"
            End If
        Case Else
'           scheme type is "category"
            Select Case xAlias
                Case mpPersonalSchemes
                    xPreviewFilePath = xAppPath & "\PersonalSchemes.mpb"
                Case mpPublicSchemes
                    If g_bIsAdmin Then
'                       we're now calling this node "Public Schemes"
                        xPreviewFilePath = xAppPath & "\AdminSchemes.mpb"
                    Else
                        xPreviewFilePath = xAppPath & "\FirmSchemes.mpb"
                    End If
                Case mpDocumentSchemes
                    xPreviewFilePath = xAppPath & "\DocumentSchemes.mpb"
                Case mpAdminSchemes
                    xPreviewFilePath = xAppPath & "\AdminSchemes.mpb"
                Case mpFavoriteSchemes
                    xPreviewFilePath = xAppPath & "\FavoriteSchemes.mpb"
                Case Else
                    xPreviewFilePath = xAppPath & "\Blank.mpb"
            End Select
    End Select
    GetFile = xPreviewFilePath
End Function

Sub HighlightLevel(iLevel As Integer)
'Highlights the cell containing the specified level -
'removes highlight from all other cells
    Dim rngP As Word.Range
    Dim rngQ As Word.Range
    Dim sVScroll As Single
    
    Word.ActiveDocument.UndoClear
    On Error Resume Next
    Set rngP = Word.ActiveDocument _
        .Tables(1).Range.Cells(iLevel).Range
    On Error GoTo 0
    If rngP Is Nothing Then
        Exit Sub
    End If
    On Error Resume Next
    
'   clear current highlight
    RemoveHighlight
    
'   highlight current level
    rngP.Shading.Texture = wdTexture7Pt5Percent
    
    rngP.StartOf
    rngP.Select
    Set rngQ = ActiveDocument.Content
    rngQ.EndOf
    
    With ActiveWindow
        sVScroll = .VerticalPercentScrolled
        rngQ.Select
        .VerticalPercentScrolled = 100
'       ensure that highlighted level is visible
'       on screen if not currently visible
        If iLevel = 1 Then
            .VerticalPercentScrolled = 0
        Else
            rngP.Select
            If Selection.Information( _
                wdHorizontalPositionRelativeToPage) = -1 Then
                .VerticalPercentScrolled = sVScroll + 10
                sVScroll = .VerticalPercentScrolled
            Else
                .VerticalPercentScrolled = 0
                sVScroll = .VerticalPercentScrolled
            End If
            rngQ.Select
            .VerticalPercentScrolled = sVScroll
        End If
    End With
End Sub

Public Function BitmapExists(xSchemeDisplayName As String, _
                             iSchemeType As mpSchemeTypes) As Boolean
'returns TRUE if the bitmap for the specified scheme name exists
    Select Case iSchemeType
        Case mpSchemeType_Private
            BitmapExists = (Dir(g_xUserPath & "\" & _
                        xSchemeDisplayName) <> "")
        Case mpSchemeType_Public
            BitmapExists = (Dir(mpBase.GetAppPath & "\" & _
                        xSchemeDisplayName) <> "")
        Case mpSchemeType_Document
            BitmapExists = False
    End Select
End Function

Private Function GetCurWindowProps() As udtWindowProps
    Dim WinProps As udtWindowProps
    With Word.ActiveWindow.View
        WinProps.bShowAll = .ShowAll
        WinProps.bShowTabs = .ShowTabs
        WinProps.bShowSpaces = .ShowSpaces
        WinProps.bShowParas = .ShowParagraphs
        WinProps.bShowTableGrids = .TableGridlines
        WinProps.iView = .Type
        WinProps.iZoom = .Zoom
    End With
End Function

Private Sub SetCurWindowProps(WinProps As udtWindowProps)
    With Word.ActiveWindow.View
        .ShowAll = WinProps.bShowAll
        .ShowTabs = WinProps.bShowTabs
        .ShowSpaces = WinProps.bShowSpaces
        .ShowParagraphs = WinProps.bShowParas
        .TableGridlines = WinProps.bShowTableGrids
        .Type = WinProps.iView
        .Zoom = WinProps.iZoom
    End With
End Sub

Public Sub Zoom(ByVal bIn As Boolean, _
                Optional ByVal bAdjustLines As Boolean = True, _
                Optional ByVal bRemoveHighlight As Boolean = False)
    If bIn Then
        ZoomIn bAdjustLines
    Else
        ZoomOut bRemoveHighlight, bAdjustLines
    End If
    
'   refresh ruler to reflect zoom  - tweaked for Word 10
    If InStr(Word.Application.Version, "8.") = 0 Then
        With Word.ActiveWindow
            .DisplayRulers = False
            .DisplayRulers = True
        End With
    End If
End Sub

Public Sub ZoomIn(Optional ByVal bAdjustLines As Boolean = True)
'makes preview 100% and changes font to 12 pt
    Dim llCur As Word.ListLevel
    Dim xLT As String
    
    DoEvents
    xLT = xGetFullLTName(g_oCurScheme.Name)
    Word.ActiveDocument.ActiveWindow.View.Zoom.Percentage = 70
    ActiveDocument.Content.Font.Size = 12
    For Each llCur In ActiveDocument.ListTemplates(xLT).ListLevels
        If llCur.Font.Size <> wdUndefined Then _
            llCur.Font.Size = 12
    Next llCur
        
    If bAdjustLines Then
        AdjustLines
    End If
End Sub

Public Sub ZoomOut(Optional ByVal bRemoveHighlight As Boolean = False, _
                   Optional ByVal bAdjustLines As Boolean = True)
    Dim llCur As Word.ListLevel
    Dim xLT As String
    
    If bRemoveHighlight Then
        RemoveHighlight
    End If
    DoEvents
    xLT = xGetFullLTName(g_oCurScheme.Name)
    Word.ActiveDocument.ActiveWindow.View.Zoom.Percentage = 41
    ActiveDocument.Content.Font.Size = 20
    For Each llCur In ActiveDocument.ListTemplates(xLT).ListLevels
        If llCur.Font.Size <> wdUndefined Then _
            llCur.Font.Size = 20
    Next llCur
    If bAdjustLines Then
        AdjustLines
    End If
End Sub

Public Function bIsZoomedOut() As Boolean
    bIsZoomedOut = (Word.ActiveWindow.View.Zoom.Percentage = 41)
End Function

Public Sub AdjustLines()
    Dim rngTable As Word.Range
    Dim rngP As Word.Range
    Dim rngQ As Word.Range
    Dim paraP As Word.Paragraph
    Dim iAlignment As Integer
    Dim bMakeBlue As Boolean
    Dim bTabExists As Boolean
    Dim lDelEnd As Long
    Dim lDelStart As Long
    Dim iMove As Integer
    Dim iFont As Integer
    Dim llp As Word.ListLevel
    
    Set rngTable = Word.ActiveDocument.Tables(1).Range
'   set font size
    If bIsZoomedOut() Then
        iFont = 20
    Else
        iFont = 12
    End If
    
    For Each paraP In rngTable.Paragraphs
        Set rngP = paraP.Range
        
        With rngP
            bTabExists = (InStr(rngP, vbTab))
            
            If Not bTabExists Then
                GoTo NextPara
            End If
            
'           find first tab
            .MoveStartUntil vbTab
            
'           exclude trailing para mark
            If .Characters.Last = vbCr Then
                .MoveEnd wdCharacter, -1
            End If
            
'           get whether to make blue
            bMakeBlue = (.Font.ColorIndex = wdBlue)
            
'           delete all underlines
            .Text = String(24, vbTab)
            
            iAlignment = paraP.Alignment
            
'           if list level underlining is undefined, number will
'           be inadvertently underlined when we insert lines below;
'           since underlining will be defined anyway as soon as ANY change
'           is made to list level, we may as well do it now
            With .ListFormat
                Set llp = .ListTemplate.ListLevels(.ListLevelNumber)
            End With
            If llp.Font.Underline = wdUndefined Then _
                llp.Font.Underline = wdUnderlineNone
            
'           insert lines
            .Underline = wdUnderlineSingle
                                             
'           set color on underlines
            Set rngQ = rngP.Duplicate
            rngQ.MoveEnd wdCharacter, -1

            If bMakeBlue Then
                rngQ.Font.ColorIndex = wdBlue
            Else
                rngQ.Font.ColorIndex = wdAuto
            End If
    
            .Expand wdParagraph
    
            .Font.Size = iFont
            
'           get eo para
            lDelEnd = .End
            
            .Select
            
'           delete all text after first two lines
            Selection.StartOf
            If InStr(rngP, String(2, 11)) Then
                iMove = 3
            Else
                iMove = 2
            End If
    
            lRet = Selection.Move(wdLine, iMove)
            If Selection.Information(wdAtEndOfRowMarker) Then _
                Selection.Move wdCharacter, -1
            lDelStart = Selection.Start
                
            If iAlignment <> wdAlignParagraphJustify Then
'               delete one extra tab at start of range-
'               this will mimic left or center alignment
                lDelStart = lDelStart - 1
            End If
            
'           added in 9.8.1004 to prevent crashing in
'           ocr scanned documents in Word 2003
            DoEvents
            
            If lDelEnd > lDelStart Then
                ActiveDocument.Range(lDelStart, lDelEnd).Text = ""
                If iAlignment = wdAlignParagraphLeft Then
                    rngP.InsertAfter "__"
                End If
            End If
        End With
NextPara:
    Next paraP
'   move insertion point out of the way
    Word.ActiveDocument.Paragraphs.Last.Range.Select
    Word.ActiveWindow.VerticalPercentScrolled = 0
End Sub

Public Sub ArrangeWindows(docPreview As Word.Document, _
                          docStart As Word.Document)
'arranges Word windows to display preview
'in the same location as the original document
    Dim sWindowHeight As Single
    Dim sWindowWidth As Single
    Dim iWindowState As Integer
    
    With docStart.ActiveWindow
        sWindowHeight = .Height
        sWindowWidth = .Width
        iWindowState = .WindowState
    End With
    
    With docPreview.ActiveWindow
        .WindowState = iWindowState
        If .WindowState = wdWindowStateNormal Then
            .Height = sWindowHeight
            .Width = sWindowWidth
            .Left = docStart.ActiveWindow.Left
            .Top = docStart.ActiveWindow.Top
        End If
    End With
    
    With docStart.ActiveWindow
        .WindowState = wdWindowStateMinimize
    End With
End Sub

Public Sub RemoveHighlight()
'removes shading from entire table
    On Error Resume Next
    With Word.ActiveDocument.Tables(1).Range.Shading
        .Texture = wdTextureNone
        .ForegroundPatternColorIndex = wdAuto
        .BackgroundPatternColorIndex = wdAuto
    End With
End Sub
